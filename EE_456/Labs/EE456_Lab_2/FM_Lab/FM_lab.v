module FM_lab(
	//////////// CLOCK //////////
	input CLOCK_50,
	
	//////////// LED //////////
	output reg [8:0] 	LEDG,
	output reg [17:0] LEDR,

	//////////// KEY //////////
	input [3:0] KEY,

	//////////// SW //////////
	input [17:0] SW,

	//////////// Audio //////////
	input 	AUD_ADCDAT,
	inout 	AUD_ADCLRCK,
	inout 	AUD_BCLK,
	output 	AUD_DACDAT,
	inout 	AUD_DACLRCK,
	output 	AUD_XCK,

	//////////// I2C for Audio  //////////
	output 	I2C_SCLK,
	inout 	I2C_SDAT, 
	
	//////////// ADC and DAC   ///////////
	input [13:0]		ADC_DA,
	input [13:0]		ADC_DB,
	output reg[13:0]	DAC_DA,
	output reg [13:0]	DAC_DB,
	output				ADC_CLK_A,
	output				ADC_CLK_B,
	output				ADC_OEB_A,
	output				ADC_OEB_B,
	output				DAC_CLK_A,
	output				DAC_CLK_B,
	output				DAC_MODE,
	output				DAC_WRT_A,
	output				DAC_WRT_B
);

//=======================================================
//  PARAMETER declarations
//=======================================================

//the length of the AUDIO CODEC data is this many bits
localparam AUDIO_DATA_WIDTH	= 32;

//=======================================================
//  REG/WIRE declarations
//=======================================================

//HARDWARE -------------------------------------------
wire reset;

//Audio ----------------------------------------------
(* keep *)wire read_audio_in;
(* keep *)wire write_audio_out;
(* keep *)wire clear_audio_in_memory;
(* keep *)wire clear_audio_out_memory;
(* noprune *) reg signed [AUDIO_DATA_WIDTH-1:0] left_channel_audio_out;
(* noprune *) reg signed [AUDIO_DATA_WIDTH-1:0] right_channel_audio_out;
(* keep *)wire signed [AUDIO_DATA_WIDTH-1:0] left_channel_audio_in;
(* keep *)wire signed [AUDIO_DATA_WIDTH-1:0] right_channel_audio_in;
(* keep *)wire audio_in_available;
(* keep *)wire audio_out_allowed;

//clocks ---------------------------------------------
// you will have to generate these clocks
(* keep, noprune *) wire clk_32M, clk_8M, clk_42k ,clk_4M, clk_2M, clk_1M ;

clock_gen pll_1(
			.inclk0(CLOCK_50),
			.c0(clk_32M),
			.c1(clk_42k));
			
reg clk_16M;
always@(posedge clk_32M)
	clk_16M <= ~clk_16M;
			
//clocks
//(* keep, noprune *) reg [10:0] clk_cnt,clk_phase;
//always@ (posedge clk_32M)
//	clk_cnt <= clk_cnt - 11'd1;			
//
//always@*
//	clk_phase = ~clk_cnt;
	

//reset  when changing frequencies
reg chan_reset;
always@ (posedge clk_32M)
	if((KEY[0] == 1'b0) || (KEY[1] == 1'b0) || (KEY[2] == 1'b0))
		chan_reset = 1'b0;
	else
		chan_reset = 1'b1;			

//=======================================================
//	Set up lights and switches
//=======================================================
// You don't need this but it is nice to have
always @ *
	LEDR = SW;

always @ *
	LEDG[3:0] = KEY;
	
always @ *
	LEDG[7:4] = KEY;
	
always @ *
	LEDG[8] = KEY[0];


//=======================================================
//	Set up DACs and ADCs
//=======================================================

/************************
Set up DACs @ 32 MHz  */
(* keep,noprune *) wire signed [17:0] signal_to_DAC_A ;
(* keep,noprune *) wire signed [17:0] signal_to_DAC_B ;
assign DAC_CLK_A = clk_32M;
assign DAC_CLK_B = clk_32M;

assign DAC_MODE = 1'b1; //treat DACs seperately

assign DAC_WRT_A = ~clk_32M;
assign DAC_WRT_B = ~clk_32M;

always@ (posedge clk_32M)// convert 1s13 format to 0u14
	DAC_DA = {~signal_to_DAC_A[17], signal_to_DAC_A[16:4]};		

always@ (posedge clk_32M) 	 
	DAC_DB = {~signal_to_DAC_B[17], signal_to_DAC_B[16:4]};	
	
/*  End DAC setup
************************/

/********************
*	set up ADCs @ 32 MHz
*/
(* keep, noprune *) reg [17:0] registered_ADC_A, registered_ADC_B;

assign ADC_CLK_A = ~clk_32M;
assign ADC_CLK_B = ~clk_32M;

assign ADC_OEB_A = 1'b0;
assign ADC_OEB_B = 1'b0;

always@ (posedge clk_32M)
	registered_ADC_A <= {~ADC_DA[13],ADC_DA[12:0],4'd0};
	
always@ (posedge clk_32M)
	registered_ADC_B <= {~ADC_DB[13],ADC_DB[12:0],4'd0};
			
/*  End ADC setup
************************/	
		
		
//======================================================
// Create FM Transmitter
//======================================================

// get carrier frequency from user
reg signed [17:0] carrier_freq;
always@ (posedge clk_32M)
	if(KEY[0] == 1'b0)
		carrier_freq <= { 1'd0, SW[10:0], 6'd0 }; 
	else
		carrier_freq <= carrier_freq;
		
//get user k_f - 1s17 format
reg [17:0] user_k;
always@ (posedge clk_32M)
	if(KEY[1] == 1'b0)
		user_k <= {7'd0, SW[10:0]};
	else
		user_k <= user_k;		
		
FM_Transmitter fm_tx1(
	.clk_32M(clk_32M),
	.reset_n(chan_reset),
	.mess_sel(SW[17:16]),
	.user_k(user_k),
	.kf_sel(SW[15]),
	.dac_sel(SW[14]),
	.aud_in(aud_in),
	.carrier_freq(carrier_freq),
	.tx_out(signal_to_DAC_B) );
				
//========================================================
// FM Receiver
//========================================================
//FM Receiver ----------------------------------------
// declare your wires/registers for the FM Receiver
// here

reg signed [17:0] recv_freq;
always@ (posedge clk_32M)
	if (KEY[2] == 1'b0)
			recv_freq <= {1'd0 ,SW[10:0], 6'd0 };
	else
			recv_freq <= recv_freq;

reg [3:0] gain;
always@ (posedge clk_32M)
	if(KEY[3] == 1'b0)
		gain = SW[3:0];
	else
		gain = gain;
			
(* keep *) wire signed [17:0] aud_out, diff_in_sin, diff_in_cos, diff_out_sin, diff_out_cos, diff_cent_sin, diff_cent_cos ;	
			
FM_Receiver fm_recv1(
	.clk_32M(clk_32M),
	.clk_1M(clk_1M),
	.reset_n(chan_reset),
	.rf_in(registered_ADC_B),
	.gain(gain),
	.recv_freq(recv_freq),
	.dac_sel(SW[13:12]),
	.diff_in_sin(diff_in_sin),
	.diff_in_cos(diff_in_cos),
	
	.diff_out_sin(diff_out_sin),
	.diff_out_cos(diff_out_cos),
	
	.diff_cent_sin(diff_cent_sin),
	.diff_cent_cos(diff_cent_cos),
	
	.aud_out(aud_out),
	.dac_out(signal_to_DAC_A) );
	
	
//create differentiators
//==================================
// STUDENT CODE SHOULD GO HERE
//==================================
Differentiator Diff_Cos( .clk(clk_1M),
						.sig_in(diff_in_cos),
						.sig_out(diff_out_cos),
						.center_tap(diff_cent_cos));
						
Differentiator Diff_Sin( .clk(clk_1M),
						.sig_in(diff_in_sin),
						.sig_out(diff_out_sin),
						.center_tap(diff_cent_sin));
//==================================
// STUDENT CODE ENDS HERE
//==================================
	
//=======================================================
// Audio Controller
//=======================================================
avconf avconf1(
			.clk(clk_32M),
			.reset(1'b0),
			.I2C_SCLK(I2C_SCLK),
			.I2C_SDAT(I2C_SDAT)
			);
			
assign AUD_XCK = clk_16M ; // Audio chip clocked at 16 MHz		
	
//interface to use the audio chip.
Audio_Controller Audio_Controller_1(
			//inputs
			.clk(clk_32M),
			.reset(1'b0),
			.clear_audio_in_memory(),
			.read_audio_in(read_audio_in),
			.clear_audio_out_memory(),
			.left_channel_audio_out(left_channel_audio_out),
			.right_channel_audio_out(right_channel_audio_out),
			.write_audio_out(write_audio_out),
			.AUD_ADCDAT(AUD_ADCDAT),
			//Bidirectionals
			.AUD_BCLK(AUD_BCLK),
			.AUD_ADCLRCK(AUD_ADCLRCK),
			.AUD_DACLRCK(AUD_DACLRCK),
			//outputs
			.left_channel_audio_in(left_channel_audio_in),
			.right_channel_audio_in(right_channel_audio_in),
			.audio_in_available(audio_in_available),
			.audio_out_allowed(audio_out_allowed),
			.AUD_DACDAT(AUD_DACDAT)
			);

//ouput audio signal to the Line out port			
always@ (posedge AUD_DACLRCK)
	left_channel_audio_out <= {aud_out,14'd0};
always@ (posedge AUD_DACLRCK)
	right_channel_audio_out <= {aud_out,14'd0};
	
assign write_audio_out = ~AUD_DACLRCK;
assign read_audio_in	= ~AUD_ADCLRCK;		

//message signal
(* noprune,keep *) reg signed [17:0] aud_in_left, aud_in_right;
always@ (negedge AUD_ADCLRCK)
	begin
		aud_in_left <= left_channel_audio_in[31:14];
		aud_in_right <= right_channel_audio_in[31:14];
	end
	
always@ (negedge AUD_ADCLRCK)
	aud_in <= {aud_in_left[17],aud_in_left[17:1]} + 
				 {aud_in_right[17],aud_in_right[17:1]} ;
				 
reg signed [17:0] aud_in;

endmodule
