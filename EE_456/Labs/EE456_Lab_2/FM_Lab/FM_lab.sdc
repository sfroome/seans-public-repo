# Clock constraints

create_clock -name "sys_clk" -period 20.000ns [get_ports {CLOCK_50}]
create_generated_clock -divide_by 25 -multiply_by 16 -source [get_ports {CLOCK_50}] -name clk_32M [get_nodes {clk_32M}]

create_generated_clock -divide_by 2 -source [get_nodes {clk_32M}] -name clk_16M [get_nodes {clk_16M}]
#create_generated_clock -divide_by 4 -source [get_nodes {clk_32M}] -name clk_8M [get_nodes {clk_8M}]
#create_generated_clock -divide_by 8 -source [get_nodes {clk_32M}] -name clk_4M [get_nodes {clk_4M}]
#create_generated_clock -divide_by 16 -source [get_nodes {clk_32M}] -name clk_2M [get_nodes {clk_2M}]
#create_generated_clock -divide_by 32 -source [get_nodes {clk_32M}] -name clk_1M [get_nodes {clk_1M}]
#create_generated_clock -divide_by 64 -source [get_nodes {clk_32M}] -name clk_500k [get_nodes {clk_500k}]

# Automatically constrain PLL and other generated clocks
derive_pll_clocks -create_base_clocks

# Automatically calculate clock uncertainty to jitter and other effects.
derive_clock_uncertainty

# tsu/th constraints

# tco constraints

# tpd constraints

