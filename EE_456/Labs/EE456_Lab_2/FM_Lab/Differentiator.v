module Differentiator( input clk,
								input signed [17:0]sig_in, //1s17
								output reg signed [17:0]sig_out, //1s17
								output reg signed [17:0]center_tap);

		//----------------------------------------------------
		//remove these lines of verilog code before you begin
//		assign sig_out = 18'd0; // Read Line above
//		assign center_tap = sig_in;
		//end of lines to remove
		//----------------------------------------------------
reg signed [17:0]  add_in;

always @ (posedge clk)
center_tap = sig_in;

always @ (posedge clk)		
add_in = center_tap; // Should probably be called subtract_in
//If you don't subtract it, all you're doing is delaying the
//output by two clock cycles.

always @ *
sig_out = sig_in - add_in;

endmodule
