module NCO( input [15:0] phi_inc_i, // 3.125 Mhz
				input [15:0] phi_adj,
				input clk,
				input reset_n,
				input	clken,
				output reg signed [17:0] fcos_o,
				output reg signed [17:0] fsin_o);

reg [15:0] phi_acc, phi_out, phi_out_cos, phi_out_sin;
always@(posedge clk)
	if(~reset_n)
		phi_acc <= 16'd0;
	else if(~clken)
		phi_acc <= phi_acc;
	else 
		phi_acc <= phi_acc + phi_inc_i ;
			
always@(posedge clk)
	phi_out <= phi_acc - phi_adj ;
	
always@(posedge clk)
	phi_out_cos <= phi_out ;
always@(posedge clk)
	phi_out_sin <= phi_out - 16'd16384; // 1/4 cycle

// dual port rom
reg [13:0] rom_add_cos, rom_add_sin;
wire signed[17:0] rom_out_cos,rom_out_sin;
nco_rom rom1(.address_a(rom_add_cos),
				 .address_b(rom_add_sin),
				 .clock(clk),
				 .q_a(rom_out_cos[16:0]),
				 .q_b(rom_out_sin[16:0]));	
	
// cosine output	
always@(posedge clk)
	if(phi_out_cos[14] == 1'b0)
		rom_add_cos <= phi_out_cos[13:0];
	else
		rom_add_cos <= ~phi_out_cos[13:0] ;
		
reg cos_out_sel[1:0]; // sync input and output quadrant selection
always @(posedge clk)
	cos_out_sel[0] <= phi_out_cos[15]^phi_out_cos[14];
always @(posedge clk)
	cos_out_sel[1]  <= cos_out_sel[0];
	
always@(posedge clk)
	if( cos_out_sel[1] )
		fcos_o <= -rom_out_cos;
	else 
		fcos_o <= rom_out_cos;

// sine output
always@(posedge clk)
	if(phi_out_sin[14] == 1'b0)
		rom_add_sin <= phi_out_sin[13:0];
	else
		rom_add_sin <= ~phi_out_sin[13:0] ;

reg sin_out_sel[1:0]; // sync input and output quadrant selection
always @(posedge clk)
	sin_out_sel[0] <= phi_out_sin[15]^phi_out_sin[14];
always @(posedge clk)
	sin_out_sel[1]  <= sin_out_sel[0];
	
always@(posedge clk)
	if( sin_out_sel[1] )
		fsin_o <= -rom_out_sin;
	else 
		fsin_o <= rom_out_sin;
				

endmodule
	