module pulse_shaping_filter (
         input clk,
		   input signed [17:0] x_in,
		   output reg signed [17:0] y   ); // Inputs and outputs will be 1s17. 
integer i; // Clearly it's assumed one will not be lazy and actually use a for loop.

// Smart Compilation is Dumb Compilation. Why does it even exist?! It just ignores everything.
//---------------------------------------------
// Comment these lines of code before you begin
//	always@*
//	y <= x_in;
	
/*	
//	FOR STEP 12
	reg signed [17:0] x_1, x_2, x_3, x_4, x_5, x_6, x_7;
// I guess I could always use a for loop but I'm lazy.
// H(z) = 1+z1 + z2 + ... + z7.
// Y(z) = X(z) + X(z)*z1 + ... + X(z)*z7.
	
	always @ (posedge clk)
		x_7 = x_6;
	always @ (posedge clk)
		x_6 = x_5;
	always @ (posedge clk)
		x_5 = x_4;
	always @ (posedge clk)
		x_4 = x_3;
	always @ (posedge clk)
		x_3 = x_2;
	always @ (posedge clk)
		x_2 = x_1;
	always @ (posedge clk)
		x_1 = x_in;		
		
	always @ *
		y = x_in + x_1 + x_2 + x_3 + x_4 + x_5 + x_6 + x_7;
	// There is no multiplication, so... there is no trimming or padding required. Although surely there'll be overflow given how many terms are being added together??
*/


// FOR STEP 18
reg signed [17:0] x[7:0];
reg signed [17:0] b[3:0];
reg signed [35:0] mult_out[3:0];
reg signed [17:0] sum_level_1[3:0];
reg signed [17:0] sum_level_2[1:0];
reg signed [17:0] sum_level_3;




// AIGHT THey should be correct
always @ (posedge clk)
	x[0] = x_in;
	
always @ (posedge clk)
	begin
		for(i = 1; i < 8; i = i +1) // x1 to x7
			x[i] <= x[i-1];
	end
// This is the top layer.

always @ *
	for(i = 0; i <= 3; i = i + 1)
		sum_level_1[i] = x[i] + x[7 -i]; // ie: x0 + x7, x1 + x6, x2 + x5 ,x3+x4 
//always @ *
//		sum_level_1[3] = x[3];
always @ *
	for (i = 0; i <= 3; i = i + 1)
		mult_out[i] = sum_level_1[i] * b[i];
always @ * 
	for(i = 0; i <= 1; i = i + 1)
//		sum_level_2[i] = mult_out[2*i][35:18] + mult_out[2*i+1][35:18]; // Need to truncate MSB since this is currently
																							// 2s16 after removing the 18 LSB's, but not the MSB.
		sum_level_2[i] = {mult_out[2*i][34:17]} + {mult_out[2*i+1][34:17]}; // Try hacking off the most significant bit. Ohai it worked.
																							
// mult0+mult1, mult2+mult3
//	sum_level_2[i] = {mult_out[2*i][35], mult_out[2*i][35], mult_out[2*i][35:20]} 
//	+ {mult_out[2*i+1][35], mult_out[2*i+1][35], mult_out[2*i+1][35:20]};

always @ *
		sum_level_3 = sum_level_2[0] + sum_level_2[1];
	
always @ *
		y = sum_level_3;
		
always @ *
   begin
		b[0] =  18'sd  19955; // These may be 2s16 coefficients, but they were already scaled
		b[1] =  18'sd	56826; // in MATLAB... which is why you don't truncate them here.
		b[2] =  18'sd  85046;  
		b[3] =  18'sd 100319;
	end		



endmodule



