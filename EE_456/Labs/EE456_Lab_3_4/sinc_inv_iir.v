module sinc_inv_iir (
          input clk,
			 input en, // enable
		   input signed [17:0] x_in,
		   output reg signed [17:0] y   );
			
reg signed [17:0] x_reg, delay_in;

always@(posedge clk)
	x_reg <= x_in;
	
always@(posedge clk)
	y <= delay_in;
	

reg signed [35:0] mult_out;
always@*
	if(en)
		mult_out <= y * 18'sd13985;
	else
		mult_out <= 18'd0;
	
always@*
	delay_in <= x_reg - mult_out[34:17];		
			
endmodule
			