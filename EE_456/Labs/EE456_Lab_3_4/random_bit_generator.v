module random_bit_generator (
          input clk,
		   input rst,
		   output reg y   );
integer i;
reg delay [11:0];	

initial begin
delay[11:0] = 1'b1011 1111 1111;
end		
			
always @ (posedge clk)
for(i=1;i<=11;i=i+1)
delay[i+1] = delay[i];

always @ (posedge clk)
delay[1] = xor({delay[2], delay[3], delay[6], delay[11]}  );		
			
always @ (posedge clk)
y = delay[11];
end module			