module slicer_th_cal (
          input clk,
		   input signed [17:0] x,
		   output reg signed [17:0] y   );
			
// Tung Nguyen 
// this module finds decision threshold for 4PAM constellation
// based on baseband down-sampled signals

reg signed [17:0] x_abs; // absolute value of x

always@(posedge clk) 	
	if(x[17]) // negative number
		x_abs <= -x;
	else 
	   x_abs <= x;

// pass absolute value through iir ac-block filter
// with single pole at 2^17-1, the DC gain is 2^17		
reg signed [34:0] avg_reg, avg_in;
always@*
	avg_in <= mult_out[51:17] + x_abs;
	
always@(posedge clk)
	avg_reg <= avg_in ;

reg signed [52:0] mult_out; 	
always@*
	mult_out <= avg_reg * 18'sd131071; 


always@(posedge clk)
	y <= avg_reg[34:17];
	
endmodule
