module EE456_Lab_4(
			input CLOCK_50,
			
			////////////   KEY,SW,LED    //////////
			input [17:0]SW,
			input [3:0] KEY,
			output reg [3:0] LEDG,
			output reg [17:0] LEDR,
			
			//////////// ADC and DAC   ///////////
			output reg [13:0]DAC_DA,
			output reg [13:0]DAC_DB,
			input [13:0]ADC_DA,
			input [13:0]ADC_DB,
			output	ADC_CLK_A,
			output	ADC_CLK_B,
			output	ADC_OEB_A,
			output	ADC_OEB_B,
			input 	ADC_OTR_A,
			input 	ADC_OTR_B,
			output	DAC_CLK_A,
			output	DAC_CLK_B,
			output	DAC_MODE,
			output	DAC_WRT_A,
			output	DAC_WRT_B,
			input 	OSC_SMA_ADC4,
			input 	SMA_DAC4,
			////////////   Audio   //////////
			input 	AUD_ADCDAT,
			inout 	AUD_ADCLRCK,
			inout 	AUD_BCLK,
			output reg	AUD_DACDAT,
			inout 	AUD_DACLRCK,
			output 	AUD_XCK,

			//////////// I2C for Audio  //////////
			output 	I2C_SCLK,
			inout 	I2C_SDAT
			 );
	
	integer i;
   
	//the length of the AUDIO CODEC data is this many bits
	localparam AUDIO_DATA_WIDTH	= 32;
	
	//=======================================================
	// Audio Configuration
	//=======================================================
	avconf avconf1(
				.CLOCK_50(CLOCK_50),
				.reset(1'b0),
				.I2C_SCLK(I2C_SCLK),
				.I2C_SDAT(I2C_SDAT)
				);
				

	assign AUD_XCK = clk_d2; // 12.5Mhz
	always@(negedge AUD_BCLK)
		if(SW[14:12] == 3'd3) // digital
			AUD_DACDAT <= aud_shift_reg[54];
		else if(SW[14:12] == 3'd7) // analog
			AUD_DACDAT <= aud_shift_reg[52];
		else
			AUD_DACDAT <= 1'b0; 
			
//	reg [5:0] aud_bit_cnt_L,aud_bit_cnt_R;	
//	reg AUD_ADCLRCK_D1; // delay by 1; 	
//	always@(posedge AUD_BCLK)
//		AUD_ADCLRCK_D1 <= AUD_ADCLRCK;
//		
//	always@(posedge AUD_BCLK)	
//		if( {AUD_ADCLRCK,AUD_ADCLRCK_D1} == 2'b10) // posedege AUD_ADCLRCK
//			aud_bit_cnt_R <= 0;
//		else if(aud_bit_cnt_R[5] == 1'b0)
//			aud_bit_cnt_R <= aud_bit_cnt_R+1;
//		else
//			aud_bit_cnt_R <= aud_bit_cnt_R;
//	
//	reg [AUDIO_DATA_WIDTH-1:0] aud_buff_R_shift;
//	always@(posedge AUD_BCLK)
//			aud_buff_R_shift <= {aud_buff_R_shift[AUDIO_DATA_WIDTH-2:0],AUD_ADCDAT};
//	
//	(* noprune *) reg [AUDIO_DATA_WIDTH-1:0] aud_buff_R_D1,aud_buff_R_D2;
//	always@(posedge aud_bit_cnt_R[5]) // end of serial -> parallel
//		aud_buff_R_D1 <= aud_buff_R_shift;
//		
//	always@(posedge AUD_ADCLRCK) // end of serial -> parallel
//		aud_buff_R_D2 <= aud_buff_R_D1;
	
	
	/******************************
	*	generate the clock used for sampling ADC and
	*  driving the DAC, i.e. generate the sampling clock
	*/
	(* noprune *) wire clk, clk_d2,clk_d4,clk_d8;
	reg [3:0] cnt; // a counter to divide clock rate
	always @ (posedge CLOCK_50)
	cnt = cnt + 1'b1;
	
	assign clk = cnt[0];
	assign clk_d2 = cnt[1]; // clock divide by 2;
	assign clk_d4 = cnt[2];
	assign clk_d8 = cnt[3];
   
	
	// end generatating sampling clock
	
	/************************
		  Set up DACs			*/
			wire signed [17:0] signal_to_DAC_A,signal_to_DAC_B ;
			assign DAC_CLK_A = clk;
			assign DAC_CLK_B = clk;
			
			assign DAC_MODE = 1'b1; //treat DACs seperately
			
			assign DAC_WRT_A = ~clk;
			assign DAC_WRT_B = ~clk;
			
		always@ (posedge clk)// convert 1s13 format to 0u14
								//format and send it to DAC A
		DAC_DA = {~signal_to_DAC_A[17],
						signal_to_DAC_A[16:4]};		
		
				
		always@ (posedge clk) // DAC B is not used in this
					 // lab so makes it the same as DAC A
				 
			DAC_DB = {~signal_to_DAC_B[17],
						signal_to_DAC_B[16:4]} ;	
	/*  End DAC setup
	************************/
	
	/********************
	*	set up ADCs
	*/
	(* noprune *) reg [13:0] registered_ADC_A;
	(* noprune *) reg [13:0] registered_ADC_B;
	
			assign ADC_CLK_A = ~clk;
			assign ADC_CLK_B = ~clk;
			
			assign ADC_OEB_A = 1'b0;
			assign ADC_OEB_B = 1'b0;

			
			always@ (posedge clk)
				registered_ADC_A <= ADC_DA;
				
			always@ (posedge clk)
				registered_ADC_B <= ADC_DB;
				
			/*  End ADC setup
			************************/	
	
	
	/******************************
			Set up switches and LEDs
			*/
			always @ *
			LEDR = SW;
			
			
			always @ *
			LEDG[0] = KEY[0];
			
			reg [2:0] key_reg;
			always @(posedge clk)
			if(~KEY[1])
				key_reg = 3'b001;
			else if(~KEY[2])
				key_reg = 3'b010;
			else if(~KEY[3])
				key_reg = 3'b100;
			
			
			always @ *
				LEDG[3:1] = key_reg;
			
	//end setting up switches and LEDs
	
	// Master reset signal to synchronize all modules
  wire reset;
  reset_gen reset1(.clk(clk),.rst(reset));
				
				
  (* keep,noprune *) wire signed [17:0] g_out,u_out;
awgn_gen awgn1( .clk(clk),
					 .reset(reset),
					 .seed(55'hF_FF_FF_FF),
					 .gain(SW[10:7]),
					 .uniform_out(u_out),
					 .gaussian_out(g_out) );
 
(* keep *) wire [3:0] x;		
data_gen dat1(.clk(clk_d8),
				  .reset(reset),
				  .seed(49'hF_FF_FF_FF),
			     .x(x) );	

// bits to BPSK symbols				  
reg signed [17:0] sym_bpsk ;				  
always@*
	if(x[1] == 1'b0)
	    sym_bpsk = -18'd131071; 
	else
		 sym_bpsk = 18'd131071;
		 
// bits to 4PAM symbols			
reg signed [17:0] sym_4pam,sym ;				  
always@*
	case(x[1:0])
	2'd0: sym_4pam = -18'd131071; //  -1
	2'd1: sym_4pam = -18'd43691; // -1/3
	2'd2: sym_4pam = 18'd131071; //  1
	2'd3: sym_4pam = 18'd43691; // 1/3
	endcase

reg signed [17:0] sym_aud;
always@(posedge AUD_BCLK) // audio signal with BPSK encoded
	if(AUD_ADCDAT)
		sym_aud <= 18'd131071 ;  // 1
	else 
		sym_aud <= -18'd131071 ; //-1
	
always@*
	case(SW[13:12])
		2'd0: sym <= sym_bpsk;
		2'd1: sym <= sym_4pam; 
		2'd2: sym <= 18'd131071; //  1
		2'd3: sym <= sym_aud ;
	endcase
	
	
		
	
// Upsampling by 8
reg signed [17:0] up8_out;
always@(posedge clk)
		if(cnt[3:1] == 3'b000)
			up8_out <= sym;
		else
	      up8_out <= 18'd0;	

// Pulse shaping filter
wire signed [17:0] srrc_out, psf_out;
srrc_filter flt1(.clk(clk),
				     .x_in(up8_out),
		           .y(srrc_out) );
//					  
pulse_shaping_filter psf1(.clk(clk),
				     .x_in(up8_out),
		           .y(psf_out) );

//reg signed [17:0] psf_out_delay[7:0];
//always@*
//	psf_out_delay[0] <= psf_out;
//
//always@(posedge clk)
//	for(i=1;i<=7;i=i+1)
//	psf_out_delay[i] <= psf_out_delay[i-1];
					  
reg signed [17:0] flt_out;
always@*
	if(SW[11])
		//flt_out <= psf_out_delay[5];
		flt_out <= psf_out ;
	else
		flt_out <= srrc_out;

 //modulate the signal
(* keep *) wire signed [17:0] tx_cos_out;
NCO TX_NCO(		.phi_inc_i(16'd8192), // 3.125 Mhz
					.phi_adj(16'd0),
					.clk(clk),
					.reset_n(~reset),
					.clken(1'b1),
					.fcos_o(tx_cos_out));
					
reg signed [35:0] tx_mult_out;
always@(posedge clk)
	tx_mult_out <= tx_cos_out * flt_out;
	
wire signed [17:0] tx_out= tx_mult_out[34:17];
 
reg signed [35:0] tx_out_3db; // 3dB down to make the power equals to analog channel
always@(posedge clk)
	tx_out_3db <= tx_out * 18'sd92682; // sqrt(2)*2^17
 
// Add AWGN
reg signed [17:0] rx_in;
always@(posedge clk)
	if(SW[14] == 1'b1)
	   rx_in <= {~registered_ADC_B[13],~registered_ADC_B[13],
					  registered_ADC_B[12:0],3'b0} + g_out ; // 6dB attenuation and noise
	else 
      rx_in <= {{2{tx_out_3db[34]}},tx_out_3db[34:19]} + g_out ; // -12dB gain digitally connected

// store timing synchronization value		
reg [5:0] time_sync_digital, time_sync_analog,time_sync;		

always@(posedge clk)
	if( SW[14] & ~KEY[0] )
		time_sync_analog <= SW[5:0];
	else
		time_sync_analog <= time_sync_analog ;
		
always@(posedge clk)
	if( ~SW[14] & ~KEY[0] )
		time_sync_digital <= SW[5:0];
	else
		time_sync_digital <= time_sync_digital ;

always@(posedge clk)
	if ( SW[14] )	
		time_sync <= time_sync_analog ;
	else 
		time_sync <= time_sync_digital;
		
		
// demodulate signal
wire signed [17:0] rx_sin_out,rx_cos_out;
NCO RX_NCO(		.phi_inc_i(16'd8192), // 3.125 Mhz
					.phi_adj({time_sync[2:0],13'd0}),
					.clk(clk),
					.reset_n(~reset),
					.clken(1'b1),
					.fsin_o(rx_sin_out),
					.fcos_o(rx_cos_out));
			
					
reg signed [35:0] rx_mult_sin, rx_mult_cos ;
always@(posedge clk)
	rx_mult_sin <= rx_sin_out * rx_in; 
	
always@(posedge clk)
	rx_mult_cos <= rx_cos_out * rx_in;

	
// Matching LP filter
wire signed [17:0] rx_I_srrc, rx_Q_srrc, rx_I_rect, rx_Q_rect, rx_I_sine, rx_Q_sine;
// SRRC pulse
srrc_filter rx_flt_sin(.clk(clk),
				     .x_in( {{1{rx_mult_sin[35]}},rx_mult_sin[35:18]} ),
		           .y(rx_Q_srrc) );					

srrc_filter rx_flt_cos(.clk(clk),
				     .x_in( {{1{rx_mult_cos[35]}},rx_mult_cos[35:18]} ),
		           .y(rx_I_srrc) );		

// Rectangular pulse					  
rect_pulse rx_flt_sin1(.clk(clk),
				     .x_in( {{1{rx_mult_sin[35]}},rx_mult_sin[35:18]} ),
		           .y(rx_Q_rect) );					

rect_pulse rx_flt_cos1(.clk(clk),
				     .x_in( {{1{rx_mult_cos[35]}},rx_mult_cos[35:18]} ),
		           .y(rx_I_rect) );	

// Half Sine Pulse					  
half_sine_pulse rx_flt_sin2(.clk(clk),
				     .x_in( {{1{rx_mult_sin[35]}},rx_mult_sin[35:18]} ),
		           .y(rx_Q_sine) );					

half_sine_pulse rx_flt_cos2(.clk(clk),
				     .x_in( {{1{rx_mult_cos[35]}},rx_mult_cos[35:18]} ),
		           .y(rx_I_sine) );		

(* keep *) reg [17:0] rx_I,rx_Q;
always @*
	case(key_reg)
		3'b001: rx_I <= rx_I_srrc;
		3'b010: rx_I <= rx_I_rect;
		3'b100: rx_I <= rx_I_sine;
		default: rx_I <= 18'd0;
	endcase	

always @*
	case(key_reg)
		3'b001: rx_Q <= rx_Q_srrc;
		3'b010: rx_Q <= rx_Q_rect;
		3'b100: rx_Q <= rx_Q_sine;
		default: rx_Q <= 18'd0;
	endcase			

	
reg signed [17:0] slicer [7:0];
always@*
		slicer[0] <= rx_I ;
always@(posedge clk)
		for(i=1; i<=7; i= i+1)
		slicer[i] <= slicer[i-1];

wire signed [17:0] rx_I_down = slicer[time_sync[5:3]]; // adjust the index for perfect sampling moment
		
	(* noprune *)reg [1:0] y;
	always@(posedge clk_d8)    // BPSK slicer
		y[1] = ~rx_I_down[17];			 

(* keep *)wire signed [17:0] pam_2_by_3 ;	
slicer_th_cal sthc(.clk(clk_d8),
						 .x(rx_I_down),
						 .y(pam_2_by_3));

	always@(posedge clk_d8)    // extra slicer for 4-PAM, threshold is at 2/3
	if( (rx_I_down >= -pam_2_by_3) & (rx_I_down <= pam_2_by_3) )	
		y[0] <= 1'b1; 
	else 
		y[0] <= 1'b0; 
		
	reg [127:0]	aud_shift_reg;
	
	always@(posedge clk_d8)
		aud_shift_reg <= {aud_shift_reg[126:0],y[1]};
		
// hold to see the output magnitude, important to implement the 4-PAM slicer
(* noprune *)reg signed [17:0] rx_I_down_staggered;
always@(posedge clk_d8)    
	rx_I_down_staggered <= rx_I_down;		
 
// delay original bit stream for errors counting
(* keep,noprune *)reg [3:0] x_delay_regs [15:0];
always@*
	  x_delay_regs[0] = x;
	  
always@(posedge clk_d8)
	  for(i = 1;i<=15;i=i+1)
	  x_delay_regs[i] <= x_delay_regs[i-1];

// compared bit stream 
reg [3:0] symbol_delay;
always@*
	if(SW[14]) // analog
		symbol_delay <= 4'd8;
	else
		symbol_delay <= 4'd6;


(* noprune *)reg [1:0] x_delay;
always@(posedge clk_d8)
     x_delay = x_delay_regs[symbol_delay][1:0];


(* noprune *)reg [23:0] data_cnt, error_cnt, error_staggered;
always@(posedge clk_d8)
		if(data_cnt < 24'd1000000 ) 
			if(SW[13:12] == 2'd1) // 4-PAM
				data_cnt <= data_cnt + 24'd2;
			else  // BPSK
				data_cnt <= data_cnt + 24'd1;
		else 
			data_cnt <= 24'b0;
		
always@(posedge clk_d8)
		if(data_cnt > 24'd0 )
			if(SW[13:12] == 2'd1) // 4-PAM
				error_cnt <= error_cnt + (y[1]^x_delay[1]) + (y[0]^x_delay[0]);
			else // BPSK
				error_cnt <= error_cnt + (y[1]^x_delay[1]);
		else
			error_cnt <= 24'd0;

always@(posedge clk_d8)
		if(data_cnt == 24'd1000000 )
			error_staggered <= error_cnt ; 		
		else 
			error_staggered <= error_staggered;

reg signed [17:0] probe_signal;			
always@*
		case(SW[17:15])
        3'd0: probe_signal <= u_out ;
        3'd1: probe_signal <= g_out ; 
		3'd2: probe_signal <= flt_out ;
		3'd3: probe_signal <= rx_I_down_staggered ;
		3'd4: probe_signal <= tx_out;
		3'd5: probe_signal <= rx_in;
		3'd6: probe_signal <= rx_Q;
		3'd7: probe_signal <= rx_I;
		endcase

		
		
sinc_inv_fir sinc_inv_A (.clk(clk),
							.en(SW[6]),
						  .x_in(tx_out),
						  .y(signal_to_DAC_A));		
						  
sinc_inv_fir sinc_inv_B (.clk(clk),
							.en(SW[6]),
						  .x_in(probe_signal),
						  .y(signal_to_DAC_B));								

// debug only						
//always@(posedge clk)
//	if(~KEY[2] && ~KEY[3])
//	symbol_delay <= SW[3:0];	
// d33 a51
//reg [15:0] teta;
//always@(posedge clk)
//	if(reset)
//		teta <= 16'd0;
//	else if(~KEY[1]) 
//		teta <= {3'd0,SW[5:0],7'd0};
//	else
//		teta <= teta;	

				
endmodule	