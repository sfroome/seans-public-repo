%% noise gain             15   12    8    6    4    3    2    1
SNR_measured_bpsk = [0 0 0 0 0 0 0 0]; % replace with your measurements
error_staggered_bpsk = [0 0 0 0 0 0 0 0]/1e6; % replace with your measurements

SNR_measured_4ask = [0 0 0 0 0 0 0 0]; % replace with your measurements
error_staggered_4ask = [0 0 0 0 0 0 0 0]/1e6; % replace with your measurements

figure(1)
SNR = -5:1:20;

snr=10.^(SNR/10);
x=snr/5; %Es=1.25*\Delta^2

bpsk_ber_theoretical = qfunc(sqrt(10.^(SNR/10)));

f4ask_ber_theoretical = (3/4)*qfunc(sqrt(x))+(1/2)*qfunc(3*sqrt(x))-(1/4)*qfunc(5*sqrt(x));

semilogy(SNR_measured_bpsk,error_staggered_bpsk,'-*r',...
        SNR_measured_4ask,error_staggered_4ask,'-ob',...
        SNR,bpsk_ber_theoretical, '-sm', SNR, f4ask_ber_theoretical, '-xk');
legend('BPSK - simulation',...
    '4-ASK simulation','BPSK - theory','4-ASK - theory','Location','SW');

xlabel('Signal-to-noise ratio (dB)','FontName','Times New Roman','FontSize',16,'Interpreter','latex');
ylabel('Bit error rate','FontName','Times New Roman','FontSize',16,'Interpreter','latex');
%title('BER Performance of BPSK and 4-ASK','FontName','Times New Roman','FontSize',16,'Interpreter','latex');
set(gca,'FontSize',16,'XGrid','on','YGrid','on','GridLineStyle',':','MinorGridLineStyle','none',...
   'FontName','Times New Roman'); grid on;
axis([-10 20 10^(-5) 1]);

    
