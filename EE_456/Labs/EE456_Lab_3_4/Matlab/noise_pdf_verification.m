close all;
fileID = fopen('stp1_auto_signaltap_0.txt','r');

%determine if u_out is first or if g_out is first.
while(1)
    header = textscan(fileID,'%s',1,'delimiter','\n');
    test = strfind(header{1},'0');
    if( isempty(test{1}) == 0)
       u_test = 1;
       test = strfind(header{1},'u_out');
       if(isempty(test{1}) == 0)
             u_test = 0;
       end
       break;
    end
end

while(1)
    header = textscan(fileID,'%s',1,'delimiter','\n');
    if( strcmp(header{1},'sample')) 
        break; 
    end;
end
header = textscan(fileID,'%s',1,'delimiter','\n')
data = textscan(fileID,'%f %f %f %s',inf,'delimiter','\n');
fclose(fileID);

%put arrays in the correct order
if (u_test == 0)
    u_out = data{2} / 2^18;
    g_out = data{3} / 2^18;
else
    u_out = data{3} / 2^18;
    g_out = data{2} / 2^18;
end

N_bins=100;

for k=1:2
    if k==1
        x=u_out;
    else
        x=g_out;
    end
    sigma=std(x);
    %Histogram and pdf fit

    [y,x_center]=hist(x,N_bins);  % This bins the elements of x into N_bins equally spaced containers
                % and returns the number of elements in each container in y,
                % while the bin centers are stored in x_center.
    dx=(max(x)-min(x))/N_bins; % This gives the width of each bin.
    hist_pdf= (y/length(x))/dx; % This approximate the pdf to be a constant over each bin;
    figure(k);
    pl(1)=plot(x_center,hist_pdf,'color','blue','linestyle','--','linewidth',1.0);
    hold on;
    x0=[min(x):0.001:max(x)]; % this specifies the range of random variable x
    
    if k==1
        true_pdf=1/(max(x)-min(x))*ones(1,length(x0));
    else
        true_pdf=1/(sqrt(2*pi)*sigma)*exp(-x0.^2/(2*sigma^2));
    end
    pl(2)=plot(x0,true_pdf,'color','r','linestyle','-','linewidth',1.0);
    xlabel('{\itx}','FontName','Times New Roman','FontSize',16);
    ylabel('{\itf}_{\bf x}({\itx})','FontName','Times New Roman','FontSize',16);
    if k==1
        title('True pdf and histogram pdf for uniform noise','FontName','Times New Roman','FontSize',16);
    else
        title('True pdf and histogram pdf for Gaussian noise','FontName','Times New Roman','FontSize',16);
    end
    legend(pl,'pdf from histogram','true pdf',+1);
    set(gca,'FontSize',16,'XGrid','on','YGrid','on','GridLineStyle',':','MinorGridLineStyle','none','FontName','Times New Roman');
end

