module sinc_inv_fir (
          input clk,
			 input en, // enable
		   input signed [17:0] x_in,
		   output reg signed [17:0] y   );
			
reg signed [17:0] x_reg [2:0] ;

reg [35:0] x_scaled;
always @* 
	x_scaled = x_in * 18'sd119711; // gain 1/(1+b)

always@(posedge clk)
	x_reg[0] <= x_scaled[34:17];
	
integer i;
always@(posedge clk)
	for(i=1;i<=2;i=i+1)
	x_reg[i] <= x_reg[i-1];

reg signed [17:0] sum_level_1[1:0],sum_level_2;
always@(posedge clk)
	sum_level_1[0] <= {x_reg[0][17],x_reg[0][17:1]} +
							{x_reg[2][17],x_reg[2][17:1]}; // (x[0]+x[2])/2 in 1s17 format
	
always@(posedge clk)
	sum_level_1[1] <= x_reg[1]; //1s17 format
	
always@(posedge clk)
	sum_level_2 <= sum_level_1[1] ;// 1s17 format
	
reg signed [35:0] mult_out;
always@(posedge clk)
	if(en)
		mult_out <= sum_level_1[0] * 18'sd 99510; // b in -2s20 format
	else
		mult_out <= 36'sd0;
		
always@(posedge clk)
	y <= sum_level_2 - { {2{mult_out[35]}}, mult_out[35:20]};

			
endmodule
			