module data_gen(
         input clk,
			input reset,
			input [48:0] seed,
			output reg [3:0] x );
			
// generate uniform random number			
reg [48:0] lfsr; 

always @ (posedge clk)
	if(reset == 1'b1)
		lfsr <= seed;
	else
		lfsr <= { lfsr[47:0], lfsr[48] ^ lfsr[39] };	

always @ (posedge clk)	
	x <= { lfsr[0], lfsr[24], lfsr[12], lfsr[36]} ; 
	
endmodule
