module reset_gen(input clk,
			output reg rst);
			
reg [7:0] cnt;

always@(posedge clk)
	if(cnt < 8'hFF)
	cnt <= cnt + 8'b1;

always@(posedge clk)
    if(cnt == 8'hFF)
	    rst = 1'b0;
	 else rst = 1'b1;

endmodule
	