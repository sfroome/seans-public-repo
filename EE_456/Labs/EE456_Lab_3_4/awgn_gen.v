module awgn_gen(
         input clk,
			input reset,
			input [54:0] seed,
			input [3:0] gain,
			output reg signed [17:0] uniform_out, 
		   output reg signed [17:0] gaussian_out );

reg [54:0] lfsr1; // first lfsr, generate uniform random number

always @ (posedge clk)
	if(reset == 1'b1)
		lfsr1 <= seed;
	else
		lfsr1 <= { lfsr1[53:0], lfsr1[54] ^ lfsr1[30] };	

reg [51:0] lfsr2; // second lfsr, generate sign bit

always @ (posedge clk)
	if(reset == 1'b1)
		lfsr2 <= seed[51:0];
	else
		lfsr2 <= { lfsr2[50:0], lfsr2[51] ^ lfsr2[48] };	

// 13 bits uniform distributed random number		
wire [12:0] rand_uni13 = {lfsr1[30], lfsr1[0], lfsr1[24], lfsr1[4], lfsr1[28], 
							lfsr1[8], lfsr1[32], lfsr1[12], lfsr1[36], 
							lfsr1[16], lfsr1[40], lfsr1[20], lfsr1[44]};			

// convert uniform dist. to positive Gaussian dist. by CDF inversion
wire [16:0] awgn_out;	// positive awgn number				
awgn_rom rom1( .address(rand_uni13),
					.clock(clk),
					.q(awgn_out)); // one-sided AWGN

reg [20:0] scaled_awgn;
always@(posedge clk)
	scaled_awgn <= awgn_out * gain ;
	
reg signed [17:0] pos_awgn_out;
always@*
	pos_awgn_out <= {1'b0,scaled_awgn[20:4]};
					
// gaussian distribution output					
always@(posedge clk)
	if(lfsr2[0] == 1'b0)
		gaussian_out <= pos_awgn_out ;
	else
		gaussian_out <= -pos_awgn_out;				
	
// uniform distribution output
wire [16:0] rand_uni17 = {lfsr1[33], lfsr1[1],lfsr1[4], lfsr1[31], lfsr1[7], 
						   lfsr1[34], lfsr1[10], lfsr1[37], lfsr1[13], 
							lfsr1[40], lfsr1[16], lfsr1[43], lfsr1[19], 
							lfsr1[46], lfsr1[22], lfsr1[49], lfsr1[25]};			

reg [20:0] scaled_uni;
always@(posedge clk)
	scaled_uni <= rand_uni17 * gain ;
	
reg signed [17:0] pos_uni_out;
always@*
	pos_uni_out <= {1'b0,scaled_uni[20:4]};							
							
always@(posedge clk)
	if(lfsr2[0] == 1'b0)
		uniform_out <= pos_uni_out ;
	else
		uniform_out <= -pos_uni_out;			

	
endmodule
