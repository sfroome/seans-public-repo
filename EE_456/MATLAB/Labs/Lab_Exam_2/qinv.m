function y = qinv(x)

% Ha Nguyen
% University of Saskatchewan
% 2002

%QINV Inverse Q function.
%   X = QINV(Y) is the inverse Q function for each element of X.
%   The inverse Q functions satisfies y = Q(x), for -1 <= y < 1
%   and -inf <= x <= inf.
%
%   See also ERF, ERFC, ERFCX, ERFINV, Q

% Derived from ERFINV function!

y=sqrt(2)*erfinv(1-2*x);