function bin= nat2bin(int)

% converts an integer into a binary form 
% int should not be 0
if int==0
   bin=[0];
else
   m=ceil(log2(int+1));
   for i = m:-1:1
      state(m-i+1) = fix( int/ (2^(i-1)) );
      int = int - state(m-i+1)*2^(i-1);
   end
   bin = state;
end