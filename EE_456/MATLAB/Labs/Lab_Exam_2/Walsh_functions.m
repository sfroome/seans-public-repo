% This is to generate and plot Walsh functions
close all; clear all;
Kw=2^3; % number of orthonormal Walsh functions
Tb=1; % bit duration normalized to 1
N=10^4; % number of samples for each Walsh function
Delta=Tb/N;
t=[Delta:Delta:Tb];
WF=ones(Kw,length(t)); % initialize the Walsh functions
for k=1:Kw    
   yname=['{\it\phi}_',num2str(k-1)+1,'({\itt})'];
   k_bin=nat2bin(k-1);
   D=length(k_bin);
   for d=1:D
      WF(k,:)=WF(k,:).*sign(cos(k_bin(D-d+1)*2^(d-1)*pi*(t/Tb)));      
   end
   subplot(Kw,1,k);plot(t,WF(k,:),'color','black','linewidth',1.5);
   box off;
   axis([0 1 -1 1]);
   if k<Kw
      ylabel(yname,'Rotation',0,'FontName','Times New Roman','FontSize',12);
      set(gca,'Color','none','XTick',[0 1],'YTick',[-1 1],'FontName','Times New Roman','FontSize',12);      
   else
      ylabel(yname,'Rotation',0,'FontName','Times New Roman','FontSize',12);
   end
   if k==1
       title('The First 8 Walsh Functions','FontName','Times New Roman','FontSize',12);
   end  
end
xlabel('{\itt} (sec)','FontName','Times New Roman','FontSize',12);
set(gca,'Color','none','YTick',[-1 1],'FontName','Times New Roman','FontSize',12);
