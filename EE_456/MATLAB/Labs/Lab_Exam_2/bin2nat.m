function y=bin2nat(x)

% Ha Nguyen
% University of Saskatchewan
% 2002
% Converts a binary representation of binary digits into a decimal number
%   Example
%      bin2nat([1 0 1 1 1]) returns 23

% Input checking
%
if nargin<1, error('Not enough input arguments.'); end
if isempty(x), y = ''; return, end

y=x*(2.^[(length(x)-1):-1:0])';