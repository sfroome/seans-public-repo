function y = q(x)

% Ha Nguyen
% University of Saskatchewan
% 2002

%Q  Q function.
%   Y = Q(X) is the function that calculates the area from X to infinity
%   under the tail of a normalized Gaussian density.
%	 X must be real.  The Q function is defined as:
%
%     Q(x) = 1/sqrt(2*pi) * integral from x to inf of exp(-t^2/2) dt.
%
%   See also ERF, ERFCX, ERFINV, Qinv

% Derived from ERFC function!

y=0.5*erfc(x/sqrt(2)); 