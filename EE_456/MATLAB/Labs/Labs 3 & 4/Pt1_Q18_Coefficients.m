% Calculations for Step 18 
% INPUTS AND OUTPUTS "SHOULD" BE 1S17
% COEFFICIENTS ARE 2s16
clf
clear all
N = 7;
M = N - 1;
n= 0:N;
%m = -1:10;
h = (8/5.1258)*sin(pi*n/8+pi/16);

%plot(H)

plot(20*log10(abs(h)))
%w = [.5:1:1000]/1000*pi;
%numerator_poly = [0, sin((pi/8)*n + (pi/16))];
%denominator_poly = [1, -2*cos((pi/8)*n + pi/16), 1];


%H = freqz(numerator_poly, denominator_poly, w);
%H = freqz(h, w);
%plot(20*log10(abs(H)));

stem(n, h)

h_verilog = h*2^16;
h_verilog = round(h_verilog);
h_verilog'


% yeah I needed to read the lab manual more closely. Everything below this
% can safely be nuked.
% You can tell from the graph that hT[n] is going to be AT LEAST a 2s16
% number.
%c_decimal = 8/5.1258; 
% Will need to be a 2s16 number.
%c_verilog = round(c_decimal*2^16); %2s16
%b_decimal = 2; % Clearly the values need to be greater than 1s17.
% It will have to be at least a 3s15 number in order to multiply it.
%b_verilog = round(b_decimal*2^17);
%a_decimal = pi/8 + pi/16; 
% This one can be 1s17.


%sina_decimal = sin(a_decimal); 
%cosa_decimal = cos(a_decimal);
% Alright....

%sina_decimal = sina_decimal*8/5.1258;
%cosa_decimal = cosa_decimal * 2; %b_decimal; 
% You don't need to use 2 as a coefficient so why bother. You can save
% yourself pain later by just computing it now and scaling after.

%sina_verilog = round(sina_decimal * 2^16); % 2s16
%cosa_verilog = round(cosa_decimal * 2^16); % 2s16


