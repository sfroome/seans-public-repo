% LAB 3/4 ROUGHT NOTES PART II

% Q1, Q2, Q3)

% Yes messing with the gain makes the noise amplitude shoot up.
% But the waveform mostly doesn't change ( or well... it changes because
% it's random but anyway).

% And on the spectrum analyzer the noise is of course a flat line (mostly
% flat.) Changing the gain simply raises the noise floor.

% Q4)
% ie: set SW[17:15] = 3'b001. With Trac average on, it essentially looks
% the same. The dB values change, but raising the gain simply raises the
% noise floor, same as before. Gaussian's average with gain maxed out is
% the game. 
% On the scope, Gaussian seems to flatten the noise out more than uniform
% does....

% Q5) Already done? (This question is basically repeating the first four...
% You can't have Switches 16 and 17 set or else you're getting filter
% outputs...?!)

% Q6) From 250 KHz to 12.5 MHz, the total power in the band is -12.48 dBm
% (roughly). This was calculated using Trace Average as requested in the
% manual. (SW[6] was also set to 1'b1  as required)
 % Gain was also maxed out (SW[10:7] = 4'b1111).
 
 
% Q7), Q8), see tables.

% Q9) SIGNAL TAP PARTY

% Q10)

% Q11)


% Q12) The MATLAB scripts from the lab file download have been copied into
% this folder. You need the Signal tap list file in this folder to run the
% premade script.

% See that file for the relevant figures. And be sure to review later.



 