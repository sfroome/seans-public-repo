% LAB 3 ROUGHT NOTES PART IV

% Q1, Q2, Q3) Assumed Electro-Mechanical Attenuation was just
% a fancy way of saying set the Spectrum Analyzer's attentuation to 10 dB.
% 


% Q4) 
% Q5) Set SW[14] =1'b0. "DIGITTALLY CONNECTED NUMBNUTS"
% Also make sure you set SW[17:15] accordingly (see next question).

% Q6) Oh wait. This is where you set SW[17:15]. SW[17:15] = 3'd5.
% Reminder noise gains are on SW[10:7]. 
% by setting the noise high enough, it completely kills the signal.

% Q7, Q8, Q10)
% Rx_I is max with SW[1:0] = 2'd2;

%rx_ Q is minimum set to SW[1:0] = 2'd2 as well.

% Well, flipping SW[0] does cause a 3dB drop in rx_I I think so is should
% be correct.

%Q12) So yeah just leave SW[2] set to zero.

% Q13) SW[13:12] = 0 for BPSK
%ASSUMING SW[13:12] = 2'd1 for 4-ASK (ie: SW[12] = 1 and SW[13] = 0) 
%(although despite being fine earlier, it does have errors it would seem)
% There is zero error for BPSK, but some if I set the switches for M-ASK.

%SW[6:0] = 1011011 (probably the correct input)

% Q14) Error staggered is 312798 
Estimated_Bit_Error = 312798/1e6; % = 0.3128 
%So I guess that means that the error is over 30 percent, with the noise
%gain maximized.

% Q15) % with sampling rate correction SW[5:3] = 3'd3, it's minimiized to
Corrected_Estimated_Bit_Error =  249662/1e6; % Down to 24.97%
% Also note that x and y output correctly fully with SW[12] set 
% AFTER the sampling time is corrected for.


% Q16, 17, 18)

% MARKER 1: From 250 KHz to 12.5 MHz (Trace Average for Trace 1)
% -25.31 dBm.
% -96.19 dBm/Hz (PSD)

% Q19
% TRACE 2 (Marker 2 measuring from 250KHz to 12.5 MHz)
%  SW[13] = 1'b1 
% -67.177 dBm
% -138.080 dBm/Hz (PSD)

% I get a difference of 17.530 dB
 
% Q21, 22) 
% The curve is close enough


% Q23)
% There is nothing visible yet.


% Q24)
% Ohai it's rx_in.

% Q25) OH YAY!
% maxed out error_staggered is 464136 (maxed out noise gain)
% CORRECT Switch settings
%SW[6:0] = 7'b1101001 
% And now the error is zero when noise is eliminated.


% Q26) w00t. It's set correctly.
%Remember to switch 4-ASK back on when done synchronizing)

% Q27, 28) DONE. See tables and MATLAB script.

% Q29) 

% Q30) Yes. Very nicely.  Should look at that assignment question.

% Q31, 32) Not going deaf is indeed preferable.

% Q32) Past SW[10:7] = 4'd2, it's useless. Even 4'd2 is extremely
% crackly.4'd1 is largely unaffected. Yes, the hissing is very similar to
% the noise you hear from an AM/FM radio when it isn't tuned to a station.
% 
% Q33) KEY[3] has a buzzing sound and hissing, but the audio signal can be
%       heard
%      KEY[2] has a slight buzzing sound, but the audio does 
%       come through properly

% Oh dear. Switching SW[2] does nothing as far as I can tell. But I suppose
%
% What a surprise that messing with the timings will cause Intersymbol
% interference
%
