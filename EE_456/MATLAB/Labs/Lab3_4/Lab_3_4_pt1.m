% LAB 3/4 ROUGHT NOTES PART I
% Tfw 156 warnings.... wat?!


% Q 7)

% For BPSK, the SW[13:12] is set to 0.

% Q8) SW[11] = 1'b1.

% Q9) aka SW[17:15] = 3'b010. 

% Q10)  Wide band signal. And yes the random sequence of binary amplitude
%levels is a white random sequence... I think. (It trails off slightly
%around 5MHz but is basically a flat line of noise peaking around -20 dBm)

% Q11) Yes...? It explains the space between the positive and negative
% peaks?

% Q12, Q13) See Verilog code. As stated in the lab manual, 
% Since it's upsampled with a gain of 1/8, sign
% extension to compensate the gain of 8 becomes unnecessary.


% Q14) Yes. It's antipodal. You either have amplitude +1 or -1.

% Q15)
% So... taking the reference lobe value at 500 KHz since measuring at 150
% KHz is just pointless.
% Marker 1 is at 500 KHz and -.875 dBm.
% Marker 2 is relative to Marker 1 and is at 4.2 MHz away and -10.374 dB 
% down from Marker 1. 
% Marker 3 is relative to Marker 1 and is at 7.325 MHz away and -14.008 dB 
% down from Marker 1. 
% Compared to Figure 5, the locations of the side lobe peaks are definitely
% correct, and the magnitudes are reasonably close.

% Q16 is optional. If I have time, I'll come back and do it.

% Q18) God Dammit.
% Welp that was painfully annoying, but appears to be done.

% Q19. God dammit you should have read this one more closely. Oh well. It's
% done now.
% READING IS THE GIFT OF KNOWLEDGE SEAN!
% Q20. Close enough.

% Q21. um.... yes. +1/-1 (or whatever the amplitude value actually is). 
% Yes it's antipodal. 
% Assuming I'm looking at it correctly, then yes there is a unique bit for
% negative and a unique bit for positive (ie: s1 and s2).

% Q22. Do later.

% Q23) 

% After switching the mult_out values to take [34:17] instead of [35:18],
% ([34:17] is in the 461 default code incidentally), my values are closer
% to where they should probably sit.
% Marker 4 at 500 KHz is -17.68 dBm
% Marker 5 (side lobe 1)is 5.250 away (so 5.750 MHz actual) and -24.24 dB down
% Marker 6 (side lobe 2) is 8.3375 MHz away (8.8375 MHz actual)
% and -36.98 dB down. The relative dB values don't (and shouldn't change
% much), but the gain of the filter does (by 6 dB which is what I wanted it
% to do).

% IGNORE (used [35:18], no truncation)
% At 500 MHz, since 150 is impossible to measure properly.
% -24.14 dBm (Might need to sign extend it or something.... 
%  At first side lobe. (Marker 5 relative to Reference Marker 4 @ 500 KHz:
% 5.550 MHz away from reference, and -25.09 dB down. (So basically at 5MHz
% which is correct)
% Second side lobe is 8.3375 MHz away from reference and -36.31 dB down (so
% at about 8.8 MHz which again matches the graph in the lab manual).
% dB values are reasonably close.


% Q24) ONCE AGAIN, USING 500 KHz as a reference since 150 KHz is dumb
% Marker 7 at 500 KHz is -18.59 dBm. 
% First Side Lobe is at about 2.312 MHz away (so 2.812 MHz), and -29.52 dB
% down (which is accurate)
% Second sidelobe is 3.1125 MHz away (so about 3.6 MHz), and -34.3 dB down.
% (Again accurate).
 
% Q25) I cannot really see the impulse response if that's what is being
% asked here. 


% Q26 see Q24 since I already did the measurements.

% Q27) The SRRC filter is probably the best at filtering out noise and so
% on, making it probably the best suited if your application needs to be
% bandwidth efficient. The rectangular filter filters out the least amount
% of noise, but also has the lowest power requirements.

% A 31 tap filter is clearly more complex than either of the other two. The
% rectangular filter is clearly the simplest to implement.




