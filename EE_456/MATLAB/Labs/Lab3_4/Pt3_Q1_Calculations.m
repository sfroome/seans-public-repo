% For Rough Calculations.
clear all
clf

alpha = 0.1;
w = 0:pi/64:2*pi;
f = w/(2*pi);



Fs = 25e6;
f_hat = f/Fs;
w_hat = 2*pi*f_hat;


%H1 = -0.5*alpha + (1+alpha)*exp(-j*w) -0.5*alpha*exp(-j*2*w);
%plot(f, H1, 'black')
hold on
grid on

H2 = (1+alpha) - alpha*cos(w);
plot(f, H2, '--r') % This is the correction filter.


% given that the absolute value of H1 and H2 end up being the same, this
% basically proves it.


H3 = sinc(f); %(1/Fs)

plot(f, H3) % This be the DAC

%Fs = 25e6;
%f = 0:1/64:1;
%f_hat = f/Fs;

%H = (1/Fs)*sinc(f_hat);
%
%H = (1/Fs)*sinc(f);
%alpha  = 0.1;
%N = 3;
%M = n-1;
%n = 0:M;
%w_hat = 2*pi*f_hat;
%w = 2*pi*f;


%
%H = -0.5*alpha + (1+alpha)*exp(-j*w_hat) -0.5*exp(-j*2*w_hat);
%plot(w, abs(H))
%plot(w_hat/2*pi, abs(H))
%%grid on

%H_actual = (1+alpha)- alpha*cos(w_hat/(2*pi));
%H_actual = (1+alpha)- alpha*cos(w);

%plot(w_hat/(2*pi), abs(H_actual), 'r')
%plot(w, abs(H_actual), 'r')