M=48; % number of taps on each side of the peak value
L = 4 ; % upsampling factor
Rs = 1; % normalized symbol rate
fc = Rs/2/L; % normalized cutoff frequency of the shaping filter
beta = .2 ; % rolloff factor of the shaping filter
h_srrc = firrcos(2*M,fc,beta,Rs,'rolloff','sqrt'); % SRRC filter
n = 0:2*M;

subplot(2,1,1)
plot(n,h_srrc)

h_rc = conv(h_srrc,h_srrc);
% this gives the overall impulse response
[max_value,max_id] = max(h_rc);
% locate the max value and its index
h_rc_downsampled_left=h_rc(max_id-L:-L:1);
% downsample from the peak position to the left
h_rc_downsampled_right=h_rc(max_id+L:L:end);
% downsample from the peak position to the right
h_rc_downsampled = ...
[h_rc_downsampled_left(end:-1:1),max_value,h_rc_downsampled_right];
% put the downsampled values in the correct order
h_rc_downsampled = h_rc_downsampled/max_value;
% normalize so that the main tap is 1;
h_rc_downsampled(h_rc_downsampled==1)=0; % remove the main tap

n1 = 0:M;
subplot(2,1,2)
stem(n1, h_rc_downsampled)