%(a) Generate 10^6 samples of Laplacian Random Variable

L=10^6; % Length of the noise vector
%sigma=sqrt(10^(-8)); % Standard deviation of the noise
sigma = 1; % As required by the question.
A=-10^(-4);


u = rand(1,L); %Uniform Random Variable over (0,1)
%x=sigma*randn(1,L);
x = zeros(1,L); % Initialize x
id1 = (u<1/2);
id2 = (u>=1/2);
x(id1) = sqrt(sigma^(2)/2)*log(2*u(id1));
x(id2) = sqrt(sigma^(2)/2)*log(1./(2*(1-u(id2))));

%[2] (a) Verify mean and variance:
mean_x=sum(x)/L % this is the same as mean(x)
variance_x=sum((x-mean_x).^2)/L % this is the same as var(x);


mx = mean(x);

vx = var(x);

% mean_x = -3.7696e-008
% variance_x = 1.0028e-008

%[3] (b) Compute P(x>A)
P=length(find(x>A))/L

%[5] (c) Histogram and Gaussian pdf fit
N_bins=100;
[y,x_center]=hist(x,100);  % This bins the elements of x into N_bins equally spaced containers
                % and returns the number of elements in each container in y,
                % while the bin centers are stored in x_center.
dx=(max(x)-min(x))/N_bins; % This gives the width of each bin.
hist_pdf= (y/L)/dx; % This approximate the pdf to be a constant over each bin;



pl(1)=plot(x_center,hist_pdf,'color','blue','linestyle','--','linewidth',1.0);
hold on;

x0=[-5:0.001:5]*sigma; % this specifies the range of random variable x
true_pdf=1/(sqrt(2*sigma))*exp(-abs(x0).*sqrt(2/sigma));
pl(2)=plot(x0,true_pdf,'color','r','linestyle','-','linewidth',1.0);

xlabel('{\itx}','FontName','Times New Roman','FontSize',16);
ylabel('{\itf}_{\bf x}({\itx})','FontName','Times New Roman','FontSize',16);
legend(pl,'pdf from histogram','true pdf',+1);
set(gca,'FontSize',16,'XGrid','on','YGrid','on','GridLineStyle',':','MinorGridLineStyle','none','FontName','Times New Roman');
