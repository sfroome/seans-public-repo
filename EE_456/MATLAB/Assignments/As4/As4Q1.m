L = 10^6;
%p = 0.75; % # of heads should be close to 3/4 (Loaded coin)
p = 0.5; % A Fair Coin

number = zeros(1,L); % keep number of head counts in each experiment

%x = rand(1,1);
for k = 1:L;
    for i=1:4 % set up simulation for 4 coin tosses
        if rand(1,1)< p % toss coin with p
             x(i,1)=1; % head
        else
             x(i,1)=0; % tail
        end
        number(k) = number(k) + x(i,1); % Count # of Heads.
    end
    
end
probabilty_3heads = sum(number==3)/L;

Computed_P75 = (3/4)*(3/4)*(3/4);
Computed_P50 = (2/4)*(2/4);