clear all;
load a2_data_f13.mat;

L = length(x_data);
M = 1000; % # of Samples to Plot

% (a) Correlation between x_data and y_data - it should practically be zero
rho_xy=(sum((x_data - mean(x_data)).*(y_data - mean(y_data)))/L)/(std(x_data)*std(y_data));

% (b) Correlation between x_data and z_data - it should be close to -0.75
rho_xz=(sum((x_data-mean(x_data)).*(z_data-mean(z_data)))/L)/(std(x_data)*std(z_data));

% (c) Plotting - the first plot indicates that x_data and y_data are
% uncorrelated, while the second plot clearly show a negative linear
% dependence between x_data and z_data. These plots agree with
% correlation coefficients computed in Parts (a) and (b)
subplot(1,2,1);
plot(x_data(1,1:M),y_data(1,1:M),'color','r','linestyle','none','marker','o');
axis equal
xlabel('x\_data','FontName','Times New Roman','FontSize',16);
ylabel('y\_data','FontName','Times New Roman','FontSize',16);
set(gca,'FontSize',16,'FontName', 'Times New Roman');

subplot(1,2,2);
plot(x_data(1,1:M),z_data(1,1:M),'color','b','linestyle','none','marker','o');
axis equal
xlabel('x\_data','FontName','Times New Roman','FontSize',16);
ylabel('z\_data','FontName','Times New Roman','FontSize',16);
set(gca,'FontSize',16,'FontName','Times New Roman');