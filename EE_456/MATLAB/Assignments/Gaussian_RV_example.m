clf

clear all

L=10^6; % Length of the noise vector
sigma=sqrt(10^(-8)); % Standard deviation of the noise
A=-10^(-4);
x=sigma*randn(1,L);

%[2] (a) Verify mean and variance:
mean_x=sum(x)/L % this is the same as mean(x)
variance_x=sum((x-mean_x).^2)/L % this is the same as var(x);

% mean_x = -3.7696e-008
% variance_x = 1.0028e-008

%[3] (b) Compute P(x>A)
P=length(find(x>A))/L

%[5] (c) Histogram and Gaussian pdf fit
N_bins=100;
[y,x_center]=hist(x,100);  % This bins the elements of x into N_bins equally spaced containers
                % and returns the number of elements in each container in y,
                % while the bin centers are stored in x_center.
dx=(max(x)-min(x))/N_bins; % This gives the width of each bin.
hist_pdf= (y/L)/dx; % This approximate the pdf to be a constant over each bin;

pl(1)=plot(x_center,hist_pdf,'color','blue','linestyle','--','linewidth',1.0);
hold on;

x0=[-5:0.001:5]*sigma; % this specifies the range of random variable x
true_pdf=1/(sqrt(2*pi)*sigma)*exp(-x0.^2/(2*sigma^2));
pl(2)=plot(x0,true_pdf,'color','r','linestyle','-','linewidth',1.0);

xlabel('{\itx}','FontName','Times New Roman','FontSize',16);
ylabel('{\itf}_{\bf x}({\itx})','FontName','Times New Roman','FontSize',16);
legend(pl,'pdf from histogram','true pdf',+1);
set(gca,'FontSize',16,'XGrid','on','YGrid','on','GridLineStyle',':','MinorGridLineStyle','none','FontName','Times New Roman');
