module basic_BCD_counter(
input clk, sync_clr, count_enable, 
(* preserve *) output reg [3:0] BCD_count,
(* keep *) output reg look_ahead_roll_over);


always @ (posedge clk)
if(sync_clr)
BCD_count = 4'b0;
else if (count_enable == 1'b1 && BCD_count == 4'b1001)
BCD_count = 4'b0;
else if(count_enable)
BCD_count = BCD_count + 4'd1;
else
BCD_count = BCD_count;

always @ *
if (BCD_count == 4'b1001 && count_enable == 1)
look_ahead_roll_over = 1'b1;
else
look_ahead_roll_over = 1'b0;


endmodule
