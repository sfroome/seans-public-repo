module BCD_counter(
input clk, sync_clr, count_enable, 
output look_ahead_roll_over_1, look_ahead_roll_over_2,
output [3:0] dg_1, dg_10, dg_100);
//basic_BCD_counter BCD_001(.clk(clk), .sync_clr(sync_clr), .count_enable(count_enable), .BCD_count(dg_1), .look_ahead_roll_over(look_ahead_roll_over_1) );
basic_BCD_counter BCD_001(.clk(clk), .sync_clr(sync_clr), .count_enable(1'b1), .BCD_count(dg_1), .look_ahead_roll_over(look_ahead_roll_over_1) );
basic_BCD_counter BCD_010(.clk(clk), .sync_clr(sync_clr), .count_enable(look_ahead_roll_over_1), .BCD_count(dg_10), .look_ahead_roll_over(look_ahead_roll_over_2) );
basic_BCD_counter BCD_100(.clk(clk), .sync_clr(sync_clr), .count_enable(look_ahead_roll_over_2), .BCD_count(dg_100), .look_ahead_roll_over() );
endmodule
