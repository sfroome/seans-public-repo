module set_clear_latch_nor(
input set, clear, 
output Q, n_Q); 

// SW0 is clear, SW17 is set
// LEDR0 is n_Q, LEDR 17 is Q

nor nor_1(Q, clear, n_Q);
nor nor_2(n_Q, Q, set);

endmodule