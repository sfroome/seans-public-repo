module B_to_BCD_converter_with_hex_driver(
input[7:0]binary_input, 
//output reg[7:0] remainder_under_100,
output reg[7:0] remainder_under_010,
output wire[6:0]  seven_segments_10 //,seven_segments_100
);

always @ *
if(binary_input >= 8'd90)
	begin
		digit_10 = 4'd90;
		remainder_under_10 = binary_input - 8'd90;
	end
else if(binary_input >= 8'd80 )
	begin
		digit_10 = 4'd80;
		remainder_under_10 = binary_input - 8'd80; 
	end
else if (binary_input >= 8'd70)
	begin
		digit_10 = 4'd70;
		remainder_under_10 = binary_input - 8'd70;
	end
else if (binary_input >= 8'd60)
	begin
		digit_10 = 4'd60;
		remainder_under_10 = binary_input - 8'd60;
	end	
else if (binary_input >= 8'd50)
	begin
		digit_10 = 4'd50;
		remainder_under_10 = binary_input - 8'd50;
	end	
else if (binary_input >= 8'd40)
	begin
		digit_10 = 4'd40;
		remainder_under_10 = binary_input - 8'd40;
	end	
else if (binary_input >= 8'd30)
	begin
		digit_10 = 4'd30;
		remainder_under_10 = binary_input - 8'd30;
	end	
else if (binary_input >= 8'd20)
	begin
		digit_10 = 4'd20;
		remainder_under_10 = binary_input - 8'd20;
	end	
else if (binary_input >= 8'd10)
	begin
		digit_10 = 4'd10;
		remainder_under_10 = binary_input - 8'd10;
	end	
else
	begin
		digit_10 = 4'd00;
		remainder_under_10 = binary_input - 8'd00;
		
	end	
	
hex_display_driver hex_driver_10(
.hex_digit(digit_010),
.hex_segments(seven_segments_10)
);

endmodule
