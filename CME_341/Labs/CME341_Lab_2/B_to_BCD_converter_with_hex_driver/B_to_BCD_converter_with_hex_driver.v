module B_to_BCD_converter_with_hex_driver(
input[7:0]binary_input, 
output reg[7:0] remainder_under_100,
output reg[7:0] remainder_under_10,
output wire[6:0]  seven_segments_100,
output wire[6:0]  seven_segments_10,
output wire[6:0]  seven_segments_1
);

reg[3:0]digit_100;
reg[3:0]digit_010;
reg[3:0]digit_001;

always @ *
if(binary_input>=8'd200)
	begin
			digit_100= 4'd2;
			remainder_under_100 = binary_input - 8'd200;
	end
else if(binary_input >= 8'd100)
	begin
		digit_100 = 4'd1;
		remainder_under_100 = binary_input - 8'd100;
	end
else
	begin
		digit_100 = 4'd0;
		remainder_under_100 = binary_input - 8'd000;
	end 

always @ *
if( remainder_under_100 >= 8'd90)
	begin
		digit_010 = 4'd9;
		remainder_under_10 = remainder_under_100 - 8'd90;
		digit_001 = remainder_under_10;
	end
else if(remainder_under_100 >= 8'd80 )
	begin
		digit_010 = 4'd8;
		remainder_under_10 = remainder_under_100 - 8'd80; 
		digit_001 = remainder_under_10;
	end
else if (remainder_under_100 >= 8'd70)
	begin
		digit_010 = 4'd7;
		remainder_under_10 = remainder_under_100 - 8'd70;
		digit_001 = remainder_under_10;
	end
else if (remainder_under_100 >= 8'd60)
	begin
		digit_010 = 4'd6;
		remainder_under_10 = remainder_under_100 - 8'd60;
		digit_001 = remainder_under_10;
	end	
else if (remainder_under_100 >= 8'd50)
	begin
		digit_010 = 4'd5;
		remainder_under_10 = remainder_under_100 - 8'd50;
		digit_001 = remainder_under_10;
	end	
else if (remainder_under_100 >= 8'd40)
	begin
		digit_010 = 4'd4;
		remainder_under_10 = remainder_under_100 - 8'd40;
		digit_001 = remainder_under_10;
	end	
else if (remainder_under_100 >= 8'd30)
	begin
		digit_010 = 4'd3;
		remainder_under_10 = remainder_under_100 - 8'd30;
		digit_001 = remainder_under_10;
	end	
else if (remainder_under_100 >= 8'd20)
	begin
		digit_010 = 4'd2;
		remainder_under_10 = remainder_under_100 - 8'd20;
		digit_001 = remainder_under_10;
	end	
else if (remainder_under_100 >= 8'd10)
	begin
		digit_010 = 4'd1;
		remainder_under_10 = remainder_under_100 - 8'd10;
		digit_001 = remainder_under_10;
	end	
else
	begin
		digit_010 = 4'd0;
		remainder_under_10 = remainder_under_100 - 8'd00;	
		digit_001 = remainder_under_10;
	end	
	
/*always @ *
if(remainder_under_10 >= 8'd9)
	begin
		digit_001 = 4'd9;
	end
else if(remainder_under_10 >= 8'd8 )
	begin
		digit_001 = 4'd8;
	end
else if (remainder_under_10 >= 8'd7)
	begin
		digit_001 = 4'd7;
	end
else if (remainder_under_10 >= 8'd6)
	begin
		digit_001 = 4'd6;
	end	
else if (remainder_under_10 >= 8'd5)
	begin
		digit_001= 4'd5;
	end	
else if (remainder_under_10 >= 8'd4)
	begin
		digit_001 = 4'd4;
	end	
else if (remainder_under_10 >= 8'd3)
	begin
		digit_001 = 4'd3;
	end	
else if (remainder_under_10>= 8'd2)
	begin
		digit_001 = 4'd2;
	end	
else if (remainder_under_10 >= 8'd1)
	begin
		digit_001 = 4'd1;
	end	
else
	begin
		digit_001 = 4'd00;		
	end*/	
	
	
hex_display_driver hex_driver_1(
.hex_digit(digit_001),
.hex_segments(seven_segments_1)
);
hex_display_driver hex_driver_10(
.hex_digit(digit_010),
.hex_segments(seven_segments_10)
);	

hex_display_driver hex_driver_100(
.hex_digit(digit_100),
.hex_segments(seven_segments_100)
); 


endmodule 