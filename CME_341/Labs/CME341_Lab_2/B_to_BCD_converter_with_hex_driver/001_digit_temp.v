module B_to_BCD_converter_with_hex_driver(
input[7:0]binary_input, 
output reg[7:0] remainder_under_100,
output reg[7:0] remainder_under_010,
output wire[6:0]  seven_segments_100,
output wire[6:0]  seven_segments_10,
output wire[6:0] seven_segmements_1
);
reg [3:0]digit_001;

always @ *
if(binary_input >= 8'd9)
	begin
		digit_001 = 4'd9;
	end
else if(binary_input >= 8'd8 )
	begin
		digit_001 = 4'd8;
	end
else if (binary_input >= 8'd7)
	begin
		digit_001 = 4'd7;
	end
else if (binary_input >= 8'd6)
	begin
		digit_001 = 4'd6;
	end	
else if (binary_input >= 8'd5)
	begin
		digit_001= 4'd5;
	end	
else if (binary_input >= 8'd4)
	begin
		digit_001 = 4'd4;
	end	
else if (binary_input >= 8'd3)
	begin
		digit_001 = 4'd3;
	end	
else if (binary_input >= 8'd2)
	begin
		digit_001 = 4'd2;
	end	
else if (binary_input >= 8'd1)
	begin
		digit_001 = 4'd1;
	end	
else
	begin
		digit_001 = 4'd00;		
	end	
	
hex_display_driver hex_driver_1(
.hex_digit(digit_001),
.hex_segments(seven_segments_1)
);

endmodule
