module CME341_Lab5A(
input clk_27,
input reset_bar, enable_fast_clk,

output reg northbound_green, northbound_amber, northbound_red,
output reg southbound_green, southbound_amber, southbound_red,
output reg eastbound_green, eastbound_amber, eastbound_red,
output reg westbound_green, westbound_amber, westbound_red
);

reg entering_state_1, entering_state_2, entering_state_3;
reg entering_state_4, entering_state_5, entering_state_6;

reg staying_in_state_1, staying_in_state_2, staying_in_state_3;
reg staying_in_state_4, staying_in_state_5, staying_in_state_6;

reg clk;
reg [23:0] slow_clk;
reg [23:0] fast_clk;

reg [5:0] timer;

reg state_1, state_1_d, state_2, state_2_d, state_3, state_3_d;
reg state_4, state_4_d, state_5, state_5_d, state_6, state_6_d;

// Making a 1Hz clock
always @ (posedge clk_27)
if(slow_clk >= 24'd0 && slow_clk <= 24'd13500000) // 1100 1101 1111 1110 0110 0000
slow_clk = slow_clk + 24'd1;
else
slow_clk = 24'd0;

always @ (posedge clk_27)
if( fast_clk >= 24'd0 && fast_clk <= 24'd1350000)
fast_clk = fast_clk + 24'd1;
else 
fast_clk = 24'd0;

always @ (posedge clk_27)
if (slow_clk == 24'd13500000 && enable_fast_clk == 0)
clk = ~clk;
else if (fast_clk == 24'd1350000 && enable_fast_clk == 1)
clk = ~clk;
else 
clk = clk;




// Register for Changing States
// omgwtfbbq
always @ (posedge clk or negedge reset_bar)
if (reset_bar == 1'b0)
	timer <= 6'd60;
else if (entering_state_1 == 1'b1)
	timer <= 6'd60;
else if (entering_state_2 == 1'b1)
	timer <= 6'd6;
else if (entering_state_3 == 1'b1)
	timer <= 6'd2;
else if (entering_state_4 == 1'b1)
	timer <= 6'd60;
else if (entering_state_5 == 1'b1)
	timer <= 6'd6;
else if (entering_state_6 == 1'b1)
	timer <= 6'd2; 
else if (timer == 6'd1)
	timer <= timer;
else
	timer <= timer - 6'd1;

// State 1 Flippy Floppy
always @ (posedge clk or negedge reset_bar)
if (reset_bar == 1'b0)
	state_1 <= 1'b1;
else state_1 <= state_1_d;
// Logic for entering State_1
always @ *
if ((state_6 == 1'b1) && (timer == 6'd1))
	entering_state_1 <= 1'b1;
else entering_state_1 <= 1'b0;
// Logic for staying in State_1
always @ * 
if ((state_1 == 1'b1) && (timer != 6'd1))
	staying_in_state_1 <= 1'b1;
else staying_in_state_1 <= 1'b0;
// d-input for state 1 Flippy Floppy
always @ * 
if ( entering_state_1 == 1'b1)
	state_1_d <= 1'b1;
else if (staying_in_state_1 == 1'b1)
	state_1_d <= 1'b1;
else
	state_1_d <= 1'b0;

// State 2 Flippy Floppy
always @ (posedge clk or negedge reset_bar)
if(reset_bar == 1'b0)
	state_2 <= 1'b0;
else state_2 <= state_2_d;
//Enter State 2 Logics
always @ * 
if ( (state_1 == 1'b1) && (timer == 6'd1))
	entering_state_2 <= 1'b1;
else entering_state_2 <= 1'b0;
//Logics for staying in state 2
always @ *
if( (state_2 == 1'b1) && (timer != 6'd1))
	staying_in_state_2 <= 1'b1;
else staying_in_state_2 <= 1'b0;
// dinput for state_2 flippy floppy
always @ *
if ( entering_state_2 == 1'b1)
	state_2_d <= 1'b1;
else if(staying_in_state_2 == 1'b1)
	state_2_d <= 1'b1;
else
	state_2_d <= 1'b0;
	
// State 3 Flippy Floppy
always @ (posedge clk or negedge reset_bar)
if(reset_bar == 1'b0)
	state_3 <= 1'b0;
else state_3 <= state_3_d;
//Enter State 3 Logics
always @ * 
if ( (state_2 == 1'b1) && (timer == 6'd1))
	entering_state_3 <= 1'b1;
else entering_state_3 <= 1'b0;
//Logics for staying in state 3
always @ *
if( (state_3 == 1'b1) && (timer != 6'd1))
	staying_in_state_3 <= 1'b1;
else staying_in_state_3 <= 1'b0;
// dinput for state_3 flippy floppy
always @ *
if ( entering_state_3 == 1'b1)
	state_3_d <= 1'b1;
else if(staying_in_state_3 == 1'b1)
	state_3_d <= 1'b1;
else
	state_3_d <= 1'b0;

// State 4 Flippy Floppy
always @ (posedge clk or negedge reset_bar)
if(reset_bar == 1'b0)
	state_4 <= 1'b0;
else state_4 <= state_4_d;
//Enter State 4 Logics
always @ * 
if ( (state_3 == 1'b1) && (timer == 6'd1))
	entering_state_4 <= 1'b1;
else entering_state_4 <= 1'b0;
//Logics for staying in state 4
always @ *
if( (state_4 == 1'b1) && (timer != 6'd1))
	staying_in_state_4 <= 1'b1;
else staying_in_state_4 <= 1'b0;
// dinput for state_4 flippy floppy
always @ *
if ( entering_state_4 == 1'b1)
	state_4_d <= 1'b1;
else if(staying_in_state_4 == 1'b1)
	state_4_d <= 1'b1;
else
	state_4_d <= 1'b0;


// State 5 Flippy Floppy
always @ (posedge clk or negedge reset_bar)
if(reset_bar == 1'b0)
	state_5 <= 1'b0;
else state_5 <= state_5_d;
//Enter State 5 Logics
always @ * 
if ( (state_4 == 1'b1) && (timer == 6'd1))
	entering_state_5 <= 1'b1;
else entering_state_5 <= 1'b0;
//Logics for staying in state 5
always @ *
if( (state_5 == 1'b1) && (timer != 6'd1))
	staying_in_state_5 <= 1'b1;
else staying_in_state_5 <= 1'b0;
// dinput for state_5 flippy floppy
always @ *
if ( entering_state_5 == 1'b1)
	state_5_d <= 1'b1;
else if(staying_in_state_5 == 1'b1)
	state_5_d <= 1'b1;
else
	state_5_d <= 1'b0;

// State 6 Flippy Floppy
always @ (posedge clk or negedge reset_bar)
if(reset_bar == 1'b0)
	state_6 <= 1'b0;
else state_6 <= state_6_d;
//Enter State 6 Logics
always @ * 
if ( (state_5 == 1'b1) && (timer == 6'd1))
	entering_state_6 <= 1'b1;
else entering_state_6 <= 1'b0;
//Logics for staying in state 6
always @ *
if( (state_6 == 1'b1) && (timer != 6'd1))
	staying_in_state_6 <= 1'b1;
else staying_in_state_6<= 1'b0;
// dinput for state_6 flippy floppy
always @ *
if ( entering_state_6 == 1'b1)
	state_6_d <= 1'b1;
else if(staying_in_state_6 == 1'b1)
	state_6_d <= 1'b1;
else
	state_6_d <= 1'b0;
	
// Outputs based on the Status of Each State
always @ *
if( (state_4) == 1'b1)	
	northbound_green = ~1'b1;
else northbound_green = ~1'b0;
always @ *
if( (state_5) == 1'b1)	
	northbound_amber = ~1'b1;
else northbound_amber = ~1'b0;
always @ *
if( ((state_1 | state_2 | state_3 | state_6) == 1'b1))	
	northbound_red = ~1'b1;
else northbound_red = ~1'b0;

always @ *
if( (state_4) == 1'b1)	
	southbound_green = ~1'b1;
else southbound_green = ~1'b0;
always @ *
if( (state_5) == 1'b1)	
	southbound_amber = ~1'b1;
else southbound_amber = ~1'b0;
always @ *
if( ((state_1 | state_2 | state_3 | state_6) == 1'b1))	
	southbound_red = ~1'b1;
else southbound_red = ~1'b0;

always @ *
if( (state_1) == 1'b1)	
	eastbound_green = ~1'b1;
else eastbound_green = ~1'b0;
always @ *
if( (state_2) == 1'b1)	
	eastbound_amber = ~1'b1;
else eastbound_amber = ~1'b0;
always @ *
if( (state_3 | state_4 | state_5 | state_6) == 1'b1)	
	eastbound_red = ~1'b1;
else eastbound_red = ~1'b0;

always @ *
if( (state_1) == 1'b1)	
	westbound_green = ~1'b1;
else westbound_green = ~1'b0;
always @ *
if( (state_2) == 1'b1)	
	westbound_amber = ~1'b1;
else westbound_amber = ~1'b0;
always @ *
if( (state_3 | state_4 | state_5 | state_6) == 1'b1)
	westbound_red = ~1'b1;
else westbound_red = ~1'b0;
	
endmodule
