module sequential_B_to_BCD_converter(
input[10:0]sw,//switchesSW[15:8],binary num to be converted
input clk,
output wire [6:0] hex0, hex1, hex2, hex3//segmentdrivers
);
reg[10:0] counter;//the binary counter
reg[10:0] bin_num;//the register holding the 
					 // binary number being converted
reg[10:0] digit_0001, digit_0010, digit_0100, digit_1000;
//registers holding the BCD digits

(*keep*)reg right_number;//useful for debugging
//so don't let the compiler optimize it out
//declaring it a reg means the comparator
//must be built with the "always*" always @ * 

wire look_ahead_roll_over_1, look_ahead_roll_over_2, look_ahead_roll_over_3;
wire [3:0] dg_1, dg_10, dg_100, dg_1000;//outputs of "basic_BCD_counter"

//Binary Counter
always @ (posedge clk)
if(right_number == 1'b1)
counter = 11'b0;
else
counter = counter + 11'b1;

//bin_num register
always @ (posedge clk)
if(right_number)
bin_num = sw;
else
bin_num = bin_num;

//comparator
always @ *
if (bin_num == counter)
right_number = 1'b1;
else 
right_number = 1'b0;

//registers digit_001, digit_010 and digit_100
always @ (posedge clk)
if (right_number == 1'b1)
digit_0001 = dg_1;
else
digit_0001 = digit_0001;

always @ (posedge clk)
if (right_number == 1'b1)
digit_0010 = dg_10;
else
digit_0010 = digit_0010;

always @ (posedge clk)
if (right_number == 1'b1)
digit_0100 = dg_100;
else
digit_0100 = digit_0100;

always @ (posedge clk)
if (right_number == 1'b1)
digit_1000 = dg_1000;
else
digit_1000 = digit_1000;

//make the 3 digit BCD by the
//instantiation of the basic BCD counter 3 times
basic_BCD_counter BCD_0001(.clk(clk), .sync_clr(right_number), .count_enable(1'b1), .BCD_count(dg_1), .look_ahead_roll_over(look_ahead_roll_over_1) );
basic_BCD_counter BCD_0010(.clk(clk), .sync_clr(right_number), .count_enable(look_ahead_roll_over_1), .BCD_count(dg_10), .look_ahead_roll_over(look_ahead_roll_over_2) );
basic_BCD_counter BCD_0100(.clk(clk), .sync_clr(right_number), .count_enable(look_ahead_roll_over_2), .BCD_count(dg_100), .look_ahead_roll_over(look_ahead_roll_over_3) );
basic_BCD_counter BCD_1000(.clk(clk), .sync_clr(right_number), .count_enable(look_ahead_roll_over_3), .BCD_count(dg_1000), .look_ahead_roll_over());
//instantiation of the hex display drivers
hex_display_driver hex_display_0(
.hex_digit(digit_0001),
.hex_segments(hex0));

hex_display_driver hex_display_1(
.hex_digit(digit_0010),
.hex_segments(hex1));

hex_display_driver hex_display_2(
.hex_digit(digit_0100),
.hex_segments(hex2));

hex_display_driver hex_display_3(
.hex_digit(digit_1000), 
.hex_segments(hex3));
endmodule


// 838 logic elements 
// 476 Combinational Functions
// 685 Dedicated Logic Registers
