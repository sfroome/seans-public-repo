module lab_exam_1_2015 (input CLOCK_27,
								input [17:0]SW,
								input [3:0]KEY,
								output [17:0]LEDR,
								output [6:0]HEX0,
								output [6:0]HEX1,
								output [6:0]HEX2,
								output [6:0]HEX3,
								output [6:0]HEX4,
								output [6:0]HEX5,
								output [6:0]HEX6,
								output [6:0]HEX7);

			
			`define MAX_CLOCK_CNT_SL 23'd6750000
			`define MAX_CLOCK_CNT_FST 23'd337500
			
			wire [6:0] to_hex0, to_hex1, to_hex2, to_hex3, to_hex4, to_hex5, to_hex6, to_hex7;
			
			assign LEDR = SW;
			
			//create a slow clock pulse
			reg [22:0]sl_count;
			reg [22:0]clk_cnt_val;
			
			always@ *
				if (SW[17] == 1'b1)
					clk_cnt_val <= `MAX_CLOCK_CNT_FST;
				else
					clk_cnt_val <= `MAX_CLOCK_CNT_SL;
			
			always@ (posedge CLOCK_27)
					begin
						if (sl_count < clk_cnt_val)
							sl_count <= sl_count + 23'd1;
						else
							sl_count <= 23'd0;
					end
					
			(*keep*) reg clk_8Hz;
			always@ (posedge CLOCK_27)
				if (sl_count == 24'd0)
					clk_8Hz <= 1'b1;
				else
					clk_8Hz <= 1'b0;
					
			reg [10:0]value;
			
			always@ (posedge clk_8Hz)
					begin
							value <= value + 11'd1;
					end
			
	
			assign HEX0 = to_hex0;
			assign HEX1 = to_hex1;
			assign HEX2 = to_hex2;
			assign HEX3 = to_hex3;
			assign HEX4 = to_hex4;
			assign HEX5 = to_hex5;
			assign HEX6 = to_hex6;
			assign HEX7 = to_hex7;
	
			//Begin your coding here ============================
			//===================================================
			
			sequential_B_to_BCD_converter sbcd1(.sw(value),
														 .clk(CLOCK_27),
														 .hex0(to_hex0),
														 .hex1(to_hex1),
														 .hex2(to_hex2),
														 .hex3(to_hex3)
														 );
														 
			Magic_Exam exam_1(.clk(clk_8Hz),
											.v1(value),
											.v2(to_hex0),
											.v3(to_hex1),
											.v4(to_hex2),
											.v5(to_hex3),
											.check(KEY[3]),
											.a1(to_hex7));
								
								
endmodule