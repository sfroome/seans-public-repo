module hex_display_driver( input [3:0]hex_digit,
								 output reg [6:0] hex_segments);


	always@ *
	begin
			case (hex_digit)
				//                          gfe_dcba
				4'd0: hex_segments <= ~7'b011_1111;
				4'd1: hex_segments <= ~7'b000_0110;
				4'd2: hex_segments <= ~7'b101_1011;
				4'd3: hex_segments <= ~7'b100_1111;
				4'd4: hex_segments <= ~7'b110_0110;
				4'd5: hex_segments <= ~7'b110_1101;
				4'd6: hex_segments <= ~7'b111_1101;
				4'd7: hex_segments <= ~7'b000_0111;
				4'd8: hex_segments <= ~7'b111_1111;
				4'd9: hex_segments <= ~7'b110_0111;
				4'd10: hex_segments <= ~7'b111_0111;
				4'd11: hex_segments <= ~7'b111_1100;
				4'd12: hex_segments <= ~7'b011_1001;
				4'd13: hex_segments <= ~7'b101_1110;
				4'd14: hex_segments <= ~7'b111_1001;
				4'd15: hex_segments <= ~7'b111_0001;
			endcase
	end
	
								 
								 
endmodule