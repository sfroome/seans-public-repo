/**********************************
CME 341 Lab 7C 
Electronic Card Lock
Sean Froome
***********************************/
module electronic_card_lock (
input wire key_0, key_1, // Button presses aka latch determine card_read state
input wire[15:0] entry_code_on_card, // switches sw[15:0]
input wire[1:0] card_type, // switces sw[17:16]
input clk,
output reg card_read,
output reg trip_lock_for_guest // the output that trips the lock
);

wire [15:0] reset_for_guest_card, reset_for_maid_card;

reg d0, d1;

reg [15:0] guest_LFSR;
reg [15:0] maid_LFSR;

reg [15:0] current_guest_combination;
reg [15:0] current_maid_combination;

assign reset_for_guest_card = 16'H8001;
assign reset_for_maid_card = 16'H8001;

/*********************
Card Read Logic
Aka Card Read Latch
*********************/
	always @ *
	if(key_1 == 1'b0)
		card_read = 1'b1;
	else if (key_0 == 1'b0)
		card_read = 1'b0;
	else
		card_read = card_read;


/************************************************
	UNLOCKING CONDITIONS	FOR GUEST AND MAID CARDS
*************************************************/
	always @ *
	if((card_type == 2'b00) &&(entry_code_on_card == current_guest_combination)&&(current_guest_combination != 16'd0) && (card_read == 1'b1))
		trip_lock_for_guest = 1'b1;
	else if((card_type == 2'b00) && (guest_LFSR == entry_code_on_card) && (card_read == 1'b1))
		trip_lock_for_guest = 1'b1;	
	else if((card_type == 2'b01) &&(entry_code_on_card == current_maid_combination)&&(current_maid_combination != 16'd0) && (card_read == 1'b1))
		trip_lock_for_guest = 1'b1;
	else if((card_type == 2'b01) && (maid_LFSR == entry_code_on_card) && (card_read == 1'b1))
		trip_lock_for_guest = 1'b1;	
	else
		trip_lock_for_guest = 1'b0;
		
/***********************************
Guest Door Lock Logic
************************************/
always @ (posedge card_read)
	if((card_type == 2'b00) && (guest_LFSR == entry_code_on_card))
		current_guest_combination = guest_LFSR;
	else
		current_guest_combination = current_guest_combination;

/****************************************
GUEST LFSR LOGIC
*****************************************/		
always @ *
	d0 = guest_LFSR[1]^(guest_LFSR[2]^(guest_LFSR[4]^guest_LFSR[15]));		

always @ (posedge card_read)
if((card_type == 2'b10) && (entry_code_on_card == reset_for_guest_card))
	guest_LFSR = 16'H0;
else if ((card_type == 2'b00) && (guest_LFSR == 16'd0)) //May not be necessary?
	guest_LFSR = entry_code_on_card;
else if(guest_LFSR == current_guest_combination)
	begin
		guest_LFSR <= {guest_LFSR[14:0],d0};
	end
else
	guest_LFSR = guest_LFSR;
	
/***********************************		
MAID DOOR LOCK LOGIC
***********************************/

always @ (posedge card_read)
	if((card_type == 2'b01) && (maid_LFSR == entry_code_on_card))
		current_maid_combination = maid_LFSR;
	else
		current_maid_combination = current_maid_combination;
	
/***********************************
MAID LFSR LOGIC
***********************************/
always @ *
	d1 = maid_LFSR[1]^(maid_LFSR[2]^(maid_LFSR[4]^maid_LFSR[15]));		

always @ (posedge card_read)
if((card_type == 2'b11) && (entry_code_on_card == reset_for_maid_card))
	maid_LFSR = 16'H0;
else if ((card_type == 2'b01) && (maid_LFSR == 16'd0)) //May not be necessary?
	maid_LFSR = entry_code_on_card;
else if(maid_LFSR == current_maid_combination)
	begin
		maid_LFSR <= {maid_LFSR[14:0],d1};
	end
else
	maid_LFSR = maid_LFSR;

endmodule
