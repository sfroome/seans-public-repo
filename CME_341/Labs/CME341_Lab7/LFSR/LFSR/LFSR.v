module LFSR(
input clk,
input load_data_n,
output reg [15:0] q
);

reg load_data;
reg d0;
wire [15:0] data;
assign data = 16'HFFFF;

(* noprune *) reg [15:0] counter;

always @ *
load_data = ~load_data_n;

always @ *
d0 = q[1]^(q[2]^(q[4]^q[15]));  // Is it really q4??!
always @ (posedge clk)
q[0] = ((data[0] & load_data)|(~load_data & d0));
always @ (posedge clk)
q[1] = ((data[1] & load_data)|(~load_data & q[0]));
always @ (posedge clk)
q[2] = ((data[2] & load_data)|(~load_data & q[1]));
always @ (posedge clk)
q[3] = ((data[3] & load_data)|(~load_data & q[2]));
always @ (posedge clk)
q[4] = ((data[4] & load_data)|(~load_data & q[3]));
always @ (posedge clk)
q[5] = ((data[5] & load_data)|(~load_data & q[4]));
always @ (posedge clk)
q[6] = ((data[6] & load_data)|(~load_data & q[5]));
always @ (posedge clk)
q[7] = ((data[7] & load_data)|(~load_data & q[6]));
always @ (posedge clk)
q[8] = ((data[8] & load_data)|(~load_data & q[7]));
always @ (posedge clk)
q[9] = ((data[9] & load_data)|(~load_data & q[8]));
always @ (posedge clk)
q[10] = ((data[10] & load_data)|(~load_data & q[9]));
always @ (posedge clk)
q[11] = ((data[11] & load_data)|(~load_data & q[10]));
always @ (posedge clk)
q[12] = ((data[12] & load_data)|(~load_data & q[11]));
always @ (posedge clk)
q[13] = ((data[13] & load_data)|(~load_data & q[12]));
always @ (posedge clk)
q[14] = ((data[14] & load_data)|(~load_data & q[13]));
always @ (posedge clk)
q[15] = ((data[15] & load_data)|(~load_data & q[14]));

always @ (posedge clk)
if(q == 16'HFFFF)
counter = 16'H0001;
else
counter = counter + 16'H0001;


endmodule
