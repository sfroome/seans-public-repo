module counter(up_down, ena, clk, cnt, clk_out);
input up_down, ena, clk;
output clk_out;
output [3:0]cnt;
assign 
clk_out = clk;
reg [3:0] cnt;
always @ (posedge clk)
if (ena == 1'b0)
cnt = cnt;
else if (up_down == 1'b1)
cnt = cnt + 4'd1;
else
cnt = cnt - 4'd1;
endmodule

// 5 logic elements before signal tap 
// 4 logical Register
// 488 logic elements with signal tap @ 128 samples
// 558 logic elements with signal tap @ 1000 samples
// 583 logic elements with 2 triggers, 1000 samples. 