/*****************
Sean Froome
CME 341 Assignment 6
October 27, 2015
Question 2
******************/
module program_sequencer(
input clk, sync_reset, jmp, jmp_nz, dont_jmp,
input [3:0] jmp_addr,
output reg [7:0] pm_addr, pc, from_PS
);
//reg [7:0] dummy;

always @ (posedge clk)
pc = pm_addr;

always @ *
from_PS = pc;


//always @ *
//dummy = pm_addr;

always @ *
	if(sync_reset) // Reset trumps everything because reset.
			pm_addr = 8'H00;
	else if(jmp) // Jump should have higher precedence than a Conditional Jump
			//pm_addr = {jmp_addr, 4'H0};
			pm_addr = (pc + {4'H0,jmp_addr }); // hehehe wouldn't work for two consecutive jumps... but hey it works!
	else if (jmp_nz  && dont_jmp == 1'b0 )
		//	pm_addr = {jmp_addr, 4'H0};
			pm_addr = (pc + {4'H0,jmp_addr });
	else //(dont_jmp || (jmp == 1'b0) && (jmp_nz == 1'b0))
			pm_addr = pc + 8'H01;
endmodule
