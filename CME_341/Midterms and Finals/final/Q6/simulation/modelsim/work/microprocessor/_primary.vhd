library verilog;
use verilog.vl_types.all;
entity microprocessor is
    port(
        i_pins          : in     vl_logic_vector(3 downto 0);
        reset           : in     vl_logic;
        clk             : in     vl_logic;
        o_reg           : out    vl_logic_vector(3 downto 0);
        pm_data         : out    vl_logic;
        pc              : out    vl_logic;
        from_PS         : out    vl_logic;
        pm_address      : out    vl_logic;
        ir              : out    vl_logic;
        from_ID         : out    vl_logic;
        from_CU         : out    vl_logic;
        x0              : out    vl_logic_vector(3 downto 0);
        x1              : out    vl_logic_vector(3 downto 0);
        y0              : out    vl_logic_vector(3 downto 0);
        y1              : out    vl_logic_vector(3 downto 0);
        m               : out    vl_logic_vector(3 downto 0);
        r               : out    vl_logic_vector(3 downto 0);
        i               : out    vl_logic_vector(3 downto 0);
        dm              : out    vl_logic_vector(3 downto 0);
        zero_flag       : out    vl_logic
    );
end microprocessor;
