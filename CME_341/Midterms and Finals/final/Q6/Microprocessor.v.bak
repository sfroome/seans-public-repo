/****************************
CME 341 Microprocessor
Sean Froome


****************************/

module Microprocessor(
input [3:0] i_pins,
input reset, clk,
output [3:0] o_reg,
output pm_data,
output pc, from_PS,
output pm_address,
output ir, from_ID,
output from_CU,
output [3:0] x0, x1, y0, y1, m, r, i, dm,
output zero_flag
);
wire sync_reset;


/*****************************************
NOTE ABOUT DEBUGGING:
ANYTHING COMMENTED OUT BELOW IS LIKELY AN
OUTPUT IN PORT LIST FOR DEBUGGING PURPOSES
REMEMBER TO REMOVE FROM PORT LIST 
AFTERWARDS.
*****************************************/

/****************************************
Program Sequencer --> Program Memory 
****************************************/
//wire [7:0] pm_address;

/****************************************
Program Memory --> Instruction Decoder
****************************************/
//wire [7:0] pm_data; 

/****************************************
Instruction Decoder --> Program Sequencer
*****************************************/
wire jump; 
wire conditional_jump; 

/*********************************************
Instruction Decoder --> Program Sequencer AND
Instruction Decoder --> Computational Unit
*********************************************/
wire [3:0] LS_nibble_ir; 

/****************************************
Instruction Decoder --> Computational Unit
*****************************************/
wire i_mux_select; 
wire y_reg_select; 
wire x_reg_select; 
wire [3:0] source_select; 
wire [8:0] reg_enables;

/****************************************
Computational Unit --> Program Sequencer
*****************************************/
//wire zero_flag; 

/****************************************
Computational Unit --> Data Memory
*****************************************/
//wire [3:0] data_mem_addr;
wire [3:0] data_bus;
//wire [3:0] dm;


dff reset_line(.clk(clk), .d(reset), .q(sync_reset));

program_sequencer prog_sequencer(.clk(clk), .sync_reset(sync_reset), .jmp(jump), .jmp_nz(conditional_jump), .jmp_addr(LS_nibble_ir), .dont_jmp(zero_flag), .pm_addr(pm_address), .pc(pc), .from_PS(from_PS));

program_memory prog_mem(.clk(~clk), .addr(pm_address), .data(pm_data));

instruction_decoder instr_decoder(.clk(clk),.sync_reset(sync_reset), .next_instr(pm_data), .jmp(jump), .jmp_nz(conditional_jump), .ir_nibble(LS_nibble_ir), 
.i_sel(i_mux_select), .y_sel(y_reg_select), .x_sel(x_reg_select), .source_sel(source_select), .reg_en(reg_enables), .from_ID(from_ID), .ir(ir) );

computational_unit comp_unit(.clk(clk), .sync_reset(sync_reset), .nibble_ir(LS_nibble_ir), .i_sel(i_mux_select), .y_sel(y_reg_select), .x_sel(x_reg_select), .source_sel(source_select), .reg_en(reg_enables),
.i_pins(i_pins), .i(i) , .data_bus(data_bus), .from_CU(from_CU), .dm(dm), .o_reg(o_reg), .x0(x0), .x1(x1), .y0(y0), .y1(y1), .r(r), .m(m));
//.i(data_mem_addr)
data_memory data_mem(.clk(~clk), .addr(data_mem_addr), .data_in(data_bus), .w_en(reg_enables[7]), .data_out(dm));

endmodule
