module data_memory(
input w_en, clk,
input [3:0] data_in, addr,
output [3:0] data_out
);

ram ram1(.clock(clk), .wren(w_en), .address(addr), .data(data_in),  .q(data_out) );

endmodule

