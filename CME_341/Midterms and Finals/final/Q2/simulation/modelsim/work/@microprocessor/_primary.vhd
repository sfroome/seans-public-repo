library verilog;
use verilog.vl_types.all;
entity Microprocessor is
    port(
        i_pins          : in     vl_logic_vector(3 downto 0);
        reset           : in     vl_logic;
        clk             : in     vl_logic;
        o_reg           : out    vl_logic_vector(3 downto 0);
        pm_data         : out    vl_logic_vector(7 downto 0);
        pc              : out    vl_logic_vector(7 downto 0);
        from_PS         : out    vl_logic_vector(7 downto 0);
        pm_address      : out    vl_logic_vector(7 downto 0);
        ir              : out    vl_logic_vector(7 downto 0);
        from_ID         : out    vl_logic_vector(7 downto 0);
        from_CU         : out    vl_logic_vector(7 downto 0);
        x0              : out    vl_logic_vector(3 downto 0);
        x1              : out    vl_logic_vector(3 downto 0);
        y0              : out    vl_logic_vector(3 downto 0);
        y1              : out    vl_logic_vector(3 downto 0);
        m               : out    vl_logic_vector(3 downto 0);
        r               : out    vl_logic_vector(3 downto 0);
        i               : out    vl_logic_vector(3 downto 0);
        dm              : out    vl_logic_vector(3 downto 0);
        zero_flag       : out    vl_logic
    );
end Microprocessor;
