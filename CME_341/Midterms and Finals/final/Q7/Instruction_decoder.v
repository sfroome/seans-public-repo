/*************************************
CME 341 Assignment 8
Sean Froome
November 12, 2015

Hopefully this works as intended....
Tested in Midterm 2 Preamble, and in the Course Website's testbench.
**************************************/

module instruction_decoder(
input [7:0] next_instr,
input sync_reset, clk,

output reg jmp, jmp_nz, i_sel, y_sel, x_sel,
output reg [3:0] ir_nibble, source_sel,
output reg  [8:0] reg_en, 
output reg [7:0] ir, from_ID

);


//reg [7:0] ir;

always @ *
from_ID = reg_en[7:0];

always @ (posedge clk)
ir = next_instr;

always @ *
ir_nibble = ir[3:0];

	
//always  @ *
//ir_nibble = ir[3:0];

// For Register Enables
// For x0
always @ * 
	if(sync_reset == 1'b1)
		reg_en[0] = 1'b1; 
	else if((ir[7] == 1'b0)&&(ir[6:4] == 3'b0)) // load instruction
		reg_en[0] = 1'b1;
	else if((ir[7:6] == 2'b10) && (ir[5:3] ==3'd0)) // move instruction
		reg_en[0] = 1'b1;
	else 
		reg_en[0] = 1'b0;
		
//for x1
always @ *
	if(sync_reset == 1'b1)
		reg_en[1] = 1'b1;
	else if((ir[7] == 1'b0) && (ir[6:4] == 3'd1)) // load instruction
		reg_en[1] <= 1'b1;
	else if((ir[7:6] == 2'b10) && (ir[5:3] == 3'd1)) // move instruction
		reg_en[1] = 1'b1;
	else 
		reg_en[1] = 1'b0;
		
//For y0
always @ *
	if(sync_reset == 1'b1)
		reg_en[2] = 1'b1;
	else if((ir[7] == 1'b0) && (ir[6:4] == 3'd2)) // load instruction
		reg_en[2] <= 1'b1;
	else if((ir[7:6] == 2'b10) && (ir[5:3] == 3'd2))// move instruction
		reg_en[2] = 1'b1;
	else 
		reg_en[2] = 1'b0;
		
//For y1
always @ *
	if(sync_reset == 1'b1)
		reg_en[3] = 1'b1;
	else if((ir[7] == 1'b0) && (ir[6:4] == 3'd3)) // load instruction
		reg_en[3] <= 1'b1;
	else if((ir[7:6] == 2'b10) && (ir[5:3] == 3'd3))// move instruction
		reg_en[3] = 1'b1;
	else 
		reg_en[3] = 1'b0;
		
//For zero_flag/r aka it should be an ALU function???? since r can't be an actual destination....
always @ *
	if(sync_reset == 1'b1)
		reg_en[4] = 1'b1;
	else if((ir[7:5] == 3'b110)) // If it's an ALU function?????
		reg_en[4] = 1'b1; 
	else 
		reg_en[4] = 1'b0; 

//For m
always @ *
	if(sync_reset == 1'b1)
		reg_en[5] = 1'b1;
	else if((ir[7] == 1'b0) && (ir[6:4] == 3'd5)) // load instruction
		reg_en[5] <= 1'b1;
	else if((ir[7:6] == 2'b10) && (ir[5:3] == 3'd5))// move instruction
		reg_en[5] = 1'b1;
	else 
		reg_en[5] = 1'b0;
		
//For i
always @ *
	if(sync_reset == 1'b1)
		reg_en[6] = 1'b1;
	else if((ir[7] == 1'b0) && (ir[6:4] == 3'd6)) // i is dst in a load instruction
		reg_en[6] <= 1'b1;
	else if((ir[7:6] == 2'b10) && (ir[5:3] == 3'd6))//i is dst in a  move instruction
		reg_en[6] = 1'b1;
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'd7) && (ir[5:3] == 3'd6))// dm is source in a move but i isn't the destination
		reg_en[6] <= 1'b0;
	else if((ir[7:6] == 2'b10) && ((ir[2:0] == 3'd7)|| (ir[5:3]== 3'd7)))// dm is source in a move but i isn't the destination
		reg_en[6] <= 1'b1;
	else if((ir[7] == 1'b0) && (ir[6:4] == 3'd7) ) // dm a source in a load instruction
		reg_en[6] = 1'b1;
	else 
		reg_en[6] = 1'b0;
		
// For dm (aka w_en???)
always @ *
	if(sync_reset == 1'b1)
		reg_en[7] = 1'b1;
	else if((ir[7] == 1'b0) && (ir[6:4] == 3'd7)) // load instruction
		reg_en[7] <= 1'b1;
	else if((ir[7:6] == 2'b10) && (ir[5:3] == 3'd7))// move instruction
		reg_en[7] = 1'b1;
	else 
		reg_en[7] = 1'b0;		
		
//For o_reg
always @ *
	if(sync_reset == 1'b1)
		reg_en[8] = 1'b1;
	else if((ir[7] == 1'b0) && (ir[6:4] == 3'd4)) // load instruction
		reg_en[8] = 1'b1;
	else if((ir[7:6] == 2'b10) && (ir[5:3] == 3'd4))// move instruction
		reg_en[8] = 1'b1;
	else 
		reg_en[8] = 1'b0; 

// For Decoding Source Registers
always @ * 

	if(sync_reset == 1'b1)
		source_sel = 4'd10;
	else if ((ir[7:6] == 2'b10) && (ir[2:0] == ir[5:3]) && ( ir[2:0] != 3'd4) && ( ir[5:3] != 3'd4) ) // i_pins
		source_sel = 4'd9;
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'd0)) // x0
		source_sel = 4'd0;
	else if((ir[7:4] == 4'b1111) && (ir[2:0] == 3'd0))
		source_sel = 4'd0;
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'b001)) // x1
		source_sel = 4'd1;	
	else if((ir[7:4] == 4'b1111) && (ir[2:0] == 3'd1))
		source_sel = 4'd1;		
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'b010)) //y0
		source_sel = 4'd2;
	else if((ir[7:4] == 4'b1111) && (ir[2:0] == 3'd2))
		source_sel = 4'd2;
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'b011)) //y1
		source_sel = 4'd3;
	else if((ir[7:4] == 4'b1111) && (ir[2:0] == 3'd3))
		source_sel = 4'd3;	
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'b100)) // r aka 3'd4
		source_sel <= 4'd4;
	else if((ir[7:4] == 4'b1111) && (ir[2:0] == 3'd4))
		source_sel = 4'd4;		
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'b101)) //m
		source_sel = 4'd5;
	else if((ir[7:4] == 4'b1111) && (ir[2:0] == 3'd5))
		source_sel = 4'd5;		
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'b110)) // i
		source_sel = 4'd6;	
	else if((ir[7:4] == 4'b1111) && (ir[2:0] == 3'd6))
		source_sel = 4'd6;		
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'b111)) // dm
		source_sel = 4'd7; 
	else if((ir[7:4] == 4'b1111) && (ir[2:0] == 3'd7))
		source_sel = 4'd7;		
	else if((ir[7] == 1'b0)) // pm_data used in a load instruction
		source_sel = 4'd8;
	else
		source_sel = 4'd10; // Not sure what default should be....	

//x_sel
always @ *
	if(sync_reset == 1'b1)
		x_sel <= 1'b0;
	else if (ir[4] == 1'b0)	
		x_sel <= 1'b0;
	else if(ir[4] == 1'b1)
		x_sel <= 1'b1;
	else
		x_sel <= 1'b0;
		
//y_sel
always @ *
	if(sync_reset == 1'b1)
		y_sel <= 1'b0;
	else if (ir[3] == 1'b0)
		y_sel <= 1'b0;
	else if(ir[3] == 1'b1)
		y_sel <= 1'b1;
	else
		y_sel <= 1'b0;
	
// i_sel (determines auto increment condition???)
always @ *
	if(sync_reset == 1'b1)
		i_sel <= 1'b0;
	/*	else if((ir[7:6] == 2'b10) && (ir[5:3] == 3'd6) && (ir[2:0] == 3'd7)) // i is dst and dm is src
		i_sel <= 1'b0;
	else if((ir[7:6] == 2'b10) && (ir[2:0] == 3'd7) && (ir[5:3] != 3'd6)) // ie: dm is the source in a move i is not dst
		i_sel <= 1'b1;
		*/
	else if((ir[7] == 1'b0) && (ir[6:4] == 3'd6)) //ie: dm is the destination in a load
		i_sel <= 1'b0;
	else if((ir[7:6] == 2'b10) && (ir[5:3] == 3'd6)) //ie: dm is the destination in a move
		i_sel <= 1'b0; 
	else
		//i_sel <= i_sel; // WAT THE F SOULD THIS DEFAULT TO?!
		i_sel <= 1'b1; // For the testbench, it should be set to 1. In general it is not going to matter what i_sel is equal to 
		// by default because it shouldn't affect the i register unless it is enabled (and that isn't going to be the default case anyways)
	
	
always @ * // For Decoding Instruction Types aka jump
	if(sync_reset == 1'b1)
		begin
			jmp <= 1'b0;
			jmp_nz <= 1'b0;
		end
	else if (ir[7:4] == 4'HE)
		begin
			jmp <= 1'b1;
			jmp_nz <= 1'b0;
		end
	else if (ir[7:4] == 4'HF)
		begin
			jmp <= 1'b0;
			jmp_nz <= 1'b1;
		end
	else
		begin
			jmp <= 1'b0;
			jmp_nz <= 1'b0;
		end
		
endmodule
