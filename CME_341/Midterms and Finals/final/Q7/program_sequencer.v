/*****************
Sean Froome
CME 341 Assignment 6
October 27, 2015
Question 2
******************/
module program_sequencer(
input clk, sync_reset, jmp, jmp_nz, dont_jmp,
input [3:0] jmp_addr, 
input[7:0] ir,
output reg [7:0] pm_addr, pc, from_PS,
output reg interrupt_flag
);


always @ *
if(ir == 8'HC8)
		interrupt_flag <= 1'b1;
else	
		interrupt_flag <= 1'b0;	
		
reg [7:0]  return_addr;

always @ (posedge clk)
	if((jmp) && (jmp_addr == 4'HF))
			return_addr <= {jmp_addr, 4'H0};
	else
			return_addr = return_addr;
		
always @ (posedge clk)
	pc = pm_addr;

always @ *
	from_PS = pc;

always @ *
	if(sync_reset) // Reset trumps everything because reset.
			pm_addr = 8'H00;
//	else if(jmp_addr == 4'H8)
//			pm_addr = return_addr;
	else if((jmp) && (jmp_addr == 4'HF) && (interrupt_flag == 1'b1))
//else if(jmp_addr == 4'H8)
			pm_addr = return_addr;
	else if((jmp) && (jmp_addr == 4'HF) && (interrupt_flag == 1'b0))
			pm_addr = 8'HFC;
	else if(jmp) // Jump should have higher precedence than a Conditional Jump
			pm_addr = {jmp_addr, 4'H0};
	else if (jmp_nz  && dont_jmp == 1'b0 )
			pm_addr = {jmp_addr, 4'H0};
	else //(dont_jmp || (jmp == 1'b0) && (jmp_nz == 1'b0))
			pm_addr = pc + 8'H01;
endmodule
