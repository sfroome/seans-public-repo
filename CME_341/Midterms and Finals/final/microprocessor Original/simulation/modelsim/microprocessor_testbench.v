`timescale 1us / 1ns
module test_program_sequencer();
reg clk, reset;
reg [3:0] i_pins;
output [3:0] o_reg;



initial clk = 1'b0;
always
begin
#0.5 clk = ~clk;
end



microprocessor micro_1(
.clk(clk),
.reset(reset), 
.i_pins(i_pins),
.o_reg(o_reg));

endmodule
