module computational_unit(
input clk, sync_reset,
input i_sel, x_sel, y_sel, 
input [3:0] nibble_ir, source_sel, dm, i_pins,
input [8:0] reg_en, // SHOULD PROBABLY MAKE THIS 8 BITS SINCE ONE OF THEM IS NOT USED AT ALL... Probably won't matter??? OKAY IT WILL 
output reg [3:0] i, data_bus, o_reg, 
output reg zero_flag,
output reg [7:0] from_CU,
output reg [3:0] x0, x1, y0, y1,
output reg [3:0] m,
output reg [3:0] r
);

//reg [3:0] x0, x1, y0, y1;
//reg [3:0] m;
//reg [3:0] r;
reg alu_out_eq_0;
reg [3:0] pm_data;
reg [2:0] alu_function;
reg [3:0] alu_out;

reg [7:0] multi_out;

always @ *
	pm_data = nibble_ir;
always @ *
	alu_function = nibble_ir[2:0];
	
always @ *
from_CU = {x1, x0};
	
/*****************
Source Selector
******************/
always @ *
case(source_sel)
4'd0: data_bus = x0;
4'd1: data_bus = x1;
4'd2: data_bus = y0;
4'd3: data_bus = y1;
4'd4: data_bus = r;
4'd5:	data_bus = m;
4'd6: data_bus = i;
4'd7: data_bus = dm;
4'd8: data_bus = pm_data;
4'd9: data_bus = i_pins;
4'd10: data_bus = 4'H0;
4'd11: data_bus = 4'H0;
4'd12: data_bus = 4'H0;
4'd13: data_bus = 4'H0;
4'd14: data_bus = 4'H0;
4'd15: data_bus = 4'H0;
endcase

/**********************
ALU Logic
***********************/
always @ *
if(sync_reset == 1'b1)
	alu_out = 4'd0;
else
	case(alu_function)
	3'b000: if((x_sel == 1'b0) && (y_sel == 1'b0))
				alu_out = -x0;
			 else if((x_sel == 1'b0) && (y_sel == 1'b1))
			 // alu_out = alu_out;
			 alu_out = r;
			 else if((x_sel == 1'b1) && (y_sel == 1'b0))
			  alu_out = -x1;
			 else //if((x_sel == 1'b1) && (y_sel == 1b1))
			 // alu_out = alu_out;
			 alu_out = r;
	3'b001: if((x_sel == 1'b0) && (y_sel == 1'b0))
				alu_out = x0- y0;
			 else if((x_sel == 1'b0) && (y_sel == 1'b1))
				alu_out = x0 - y1;
			 else if((x_sel == 1'b1) && (y_sel == 1'b0))
				alu_out = x1 - y0;
			 else //if((x_sel == 1'b1) && (y_sel == 1b1))
				alu_out = x1 - y1;
	3'b010: if((x_sel == 1'b0) && (y_sel == 1'b0))
					alu_out = x0 + y0;
			 else if((x_sel == 1'b0) && (y_sel == 1'b1))
					alu_out = x0 + y1;
			 else if((x_sel == 1'b1) && (y_sel == 1'b0))
					alu_out = x1 + y0;
			 else //if((x_sel == 1'b1) && (y_sel == 1b1))
					alu_out = x1 + y1;
	3'b011:  alu_out = multi_out[7:4];
	3'b100:  alu_out = multi_out[3:0];
	3'b101: if((x_sel == 1'b0) && (y_sel == 1'b0))
				alu_out = x0^y0;
			 else if((x_sel == 1'b0) && (y_sel == 1'b1))
				alu_out = x0^y1;
			 else if((x_sel == 1'b1) && (y_sel == 1'b0))
				alu_out = x1^y0;
			 else //if((x_sel == 1'b1) && (y_sel == 1b1))
				alu_out = x1^y1;
	3'b110: if((x_sel == 1'b0) && (y_sel == 1'b0))
				alu_out = x0&y0;
			 else if((x_sel == 1'b0) && (y_sel == 1'b1))
			  alu_out = x0&y1;
			 else if((x_sel == 1'b1) && (y_sel == 1'b0))
			  alu_out = x1&y0;
			 else //if((x_sel == 1'b1) && (y_sel == 1b1))
			  alu_out = x1&y1;
	3'b111: if((x_sel == 1'b0) && (y_sel == 1'b0))
				alu_out = ~x0;
			 else if((x_sel == 1'b0) && (y_sel == 1'b1))
				//alu_out = alu_out;
				alu_out = r;
			 else if((x_sel == 1'b1) && (y_sel == 1'b0))
				alu_out = ~x1;
			 else //if((x_sel == 1'b1) && (y_sel == 1b1))
				//alu_out = alu_out;
				alu_out = r;
	//default:	alu_out = 4'd0;
	endcase

always @ *
	if(sync_reset == 1'b1 || alu_out == 4'd0)
		alu_out_eq_0 = 1'b1;
	else
		alu_out_eq_0 = 1'b0;

/*******************************************************
Logic for Multiplication
*******************************************************/
always @ *
if((x_sel == 1'b0) && (y_sel == 1'b0))
	multi_out = x0*y0;
else if((x_sel == 1'b0) && (y_sel == 1'b1))
	multi_out =x0*y1;
else if((x_sel == 1'b1) && (y_sel == 1'b0))
	multi_out = x1*y0;
else if((x_sel == 1'b1) && (y_sel == 1'b1))
	multi_out = x1*y1;
else
	multi_out = 8'b0;
 
/*******************************************************
	Registers 
********************************************************/
always @ (posedge clk)
if((sync_reset == 1'b1) && (reg_en[0]))
	x0 = 4'H0;
else if(reg_en[0] == 1'b1)
	x0 = data_bus;
else
	x0 = x0;
always @ (posedge clk)
if((sync_reset == 1'b1) && (reg_en[1]))
	x1 = 4'H0;
else if(reg_en[1] == 1'b1)
	x1 = data_bus;
else
	x1 = x1;
always @ (posedge clk)
if((sync_reset == 1'b1) && (reg_en[2]))
	y0 = 4'H0;
else if(reg_en[2] == 1'b1)
	y0 = data_bus;
else
	y0 = y0;
always @ (posedge clk)
if((sync_reset == 1'b1) && (reg_en[3]))
	y1 = 4'H0;
else if(reg_en[3] == 1'b1)
	y1 = data_bus;
else
	y1 = y1;
always @ (posedge clk)
if((sync_reset == 1'b1) && (reg_en[4]))
	r = 4'H0;
else if(reg_en[4] == 1'b1)
	r = alu_out;
else
	r = r;
always @ (posedge clk)
	//if((sync_reset == 1'b1) && (reg_en[5] == 1'b1))
	//	m = 4'H0;
	if(reg_en[5] == 1'b1)
		m = data_bus;
	else
		m = m;
always @ (posedge clk)
	if(( i_sel == 1'b0) && reg_en[6] == 1'b1)
		i = m+data_bus;
	else if((i_sel == 1'b1) && (reg_en[6] == 1'b1))
		i = i + m;
	else
		i = i;
always @ (posedge clk)
	if(reg_en[8] == 1'b1)
		o_reg = data_bus;
	else
		o_reg = o_reg;
always @ (posedge clk)
	if(reg_en[4] == 1'b1)
		zero_flag = alu_out_eq_0;
	else
		zero_flag = zero_flag;



/*
case({alu_function, x_sel, y_sel})
	// r = -x and r = r 
	5'b00000: alu_out = -x0;
	5'b00001: alu_out = 	alu_out;
	5'b00010: alu_out = -x1;
	5'b00011: alu_out = 	alu_out;
	// r = x - y
	5'b00100: alu_out = x0 - y0;
	5'b00101: alu_out = x0 - y1;
	5'b00110: alu_out = x1 - y0;
	5'b00111: alu_out = x1 - y1;
	// r = x + y
	5'b01000: alu_out = x0 + y0;
	5'b01001: alu_out = x0 + y1;
	5'b01010: alu_out = x1 + y0;
	5'b01011: alu_out = x1 + y1;
 	// r = x*y (MSB)
	5'b01100: alu_out = multi_out[7:4];
	5'b01101: alu_out = multi_out[7:4];
	5'b01110: alu_out = multi_out[7:4];
	5'b01111: alu_out = multi_out[7:4];
	// r = x*y (LSB)
	5'b10000: alu_out = multi_out[3:0];
	5'b10001: alu_out = multi_out[3:0];
	5'b10010: alu_out = multi_out[3:0];
	5'b10011: alu_out = multi_out[3:0];
	// r = x^y
	5'b10100: alu_out = x0^y0;
	5'b10101: alu_out = x0^y1;
	5'b10110: alu_out = x1^y0;
	5'b10111: alu_out = x1^y1;
	// r = x&y
	5'b11000: alu_out = x0&y0;
	5'b11001: alu_out = x0&y1;
	5'b11010: alu_out = x1&y0;
	5'b11011: alu_out = x1&y1;
	//r = ~x and r = r 
	5'b11100: alu_out = ~x0;
	5'b11101: alu_out = alu_out;
	5'b11110: alu_out = ~x1;
	5'b11111: alu_out = alu_out;
endcase
*/

endmodule
