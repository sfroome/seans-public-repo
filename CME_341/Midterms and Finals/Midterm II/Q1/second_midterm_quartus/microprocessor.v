module microprocessor(
input [3:0] i_pins,
input reset,
output reg [3:0] o_reg);

wire sync_reset;

//Program Memory
wire [7:0] pm_address;

// Wires out of Instruction Decoder
wire jump;
wire conditional_jump;
wire [3:0] LS_nibble_ir;
wire [7:0] pm_data;
wire i_mux_select;
wire y_reg_select;
wire x_reg_select;
wire [3:0] source_select;
wire [8:0] reg_enables;

// Wires out of Computational Unit
wire zero_flag;
wire [3:0] data_mem_addr;
wire [3:0] data_bus;
wire [3:0] dm;

//wire not_clk;
//not n_clk(not_clk, clk);


program_memory prog_mem(.clk(clk), .addr(pm_address), .data(pm_data));

program_sequencer prog_sequencer(.clk(clk), .sync_reset(sync_reset), .jmp(jump), .jmp_nz(conditional_jump), .jmp_addr(LS_nibble_ir), .dont_jmp(zero_flag), .pm_addr(pm_address));

//Instruction_decoder instr_decoder(.clk(clk),.sync_reset(sync_reset), .next_instr(pm_data), .jmp(jump), .jmp_nz(conditional_jump), .ir_nibble(LS_nibble_ir), 
//.i_sel(i_mux_select), .y_sel(y_reg_select), .x_sel(x_reg_select), .source_sel(source_select), .reg_en(reg_enables) );

//Computational_unit comp_unit(.clk(clk), .sync_reset(sync_reset), .nibble_ir(LS_nibble_ir), .i_sel(i_mux_select), .y_sel(y_reg_select), .x_sel(x_reg_select), .source_sel(source_select), .reg_en(reg_enables),
//.i_pins(i_pins), .i(data_mem_addr), .data_bus(data_bus), .dm(dm), .o_reg(o_reg));

//data_memory data_mem(.clk(clk), .addr(data_mem_addr), .data_in(data_bus), .w_en(reg_enables[7]), .data_out(dm));

endmodule
