/*****************
Sean Froome
CME 341 Assignment 6
October 27, 2015
Question 2
******************/
module program_sequencer(
input clk, sync_reset, jmp, jmp_nz, dont_jmp,
input [3:0] jmp_addr,
output reg [7:0] pm_addr, pc
);

//reg [7:0] pc;

/* These are wrong but I kept them in case they were at any point less wrong that what I have now.
always @(posedge clk)
	if(sync_reset)
		pm_addr = 8'H00;
	else if(jmp)
		pm_addr = {jmp_addr, 4'H0};
	else if (jmp_nz  && dont_jmp == 1'b0 )
		pm_addr = {jmp_addr, 4'H0};
	else //(dont_jmp || (jmp == 1'b0 && jmp_nz == 1'b0))
		pm_addr = pc + 8'H01;
// pc should roll over to 8'H00 from 8'HFF
always @ (posedge clk)
	pc = pm_addr;
*/


// begin end wrappers because I can't figure out
// how to get pc to actually get the value in pm_addr and not
// be a clock cycle behind where it should be (based on the original testbench from q3).
/*
always @(posedge clk)
	if(sync_reset) // Reset trumps everything because reset.
		begin
			pm_addr = 8'H00;
			pc = pm_addr;
		end
	else if(jmp) // Jump should have higher precedence than a Conditional Jump
		begin
			pm_addr = {jmp_addr, 4'H0};
			pc = pm_addr;
		end
	else if (jmp_nz  && dont_jmp == 1'b0 )
		begin
			pm_addr = {jmp_addr, 4'H0};
			pc = pm_addr;
		end
	else //(dont_jmp || (jmp == 1'b0) && (jmp_nz == 1'b0))
		begin
			pm_addr = pc + 8'H01;
			pc = pm_addr;
		end 
		*/
// pc should roll over to 8'H00 from 8'HFF


always @ (posedge clk)
pc = pm_addr;

always @ *
	if(sync_reset) // Reset trumps everything because reset.
			pm_addr = 8'H00;
	else if(jmp) // Jump should have higher precedence than a Conditional Jump
			pm_addr = {jmp_addr, 4'H0};
	else if (jmp_nz  && dont_jmp == 1'b0 )
			pm_addr = {jmp_addr, 4'H0};
	else //(dont_jmp || (jmp == 1'b0) && (jmp_nz == 1'b0))
			pm_addr = pc + 8'H01;
endmodule
