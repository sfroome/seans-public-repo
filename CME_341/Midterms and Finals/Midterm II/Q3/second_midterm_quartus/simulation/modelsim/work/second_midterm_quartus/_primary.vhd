library verilog;
use verilog.vl_types.all;
entity second_midterm_quartus is
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        zero_flag       : in     vl_logic;
        sync_reset      : out    vl_logic;
        pm_address      : out    vl_logic_vector(7 downto 0);
        pm_data         : out    vl_logic_vector(7 downto 0);
        jump            : out    vl_logic;
        conditional_jump: out    vl_logic;
        x_mux_select    : out    vl_logic;
        y_mux_select    : out    vl_logic;
        i_mux_select    : out    vl_logic;
        source_register_select: out    vl_logic_vector(3 downto 0);
        LS_nibble_of_ir : out    vl_logic_vector(3 downto 0);
        register_enables: out    vl_logic_vector(8 downto 0);
        pc              : out    vl_logic_vector(7 downto 0);
        instr_register  : out    vl_logic_vector(7 downto 0)
    );
end second_midterm_quartus;
