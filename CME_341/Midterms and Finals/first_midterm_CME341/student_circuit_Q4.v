module student_circuit_Q4(
input clk, clear,
input [7:0] cct_input,
output reg [7:0] cct_output);

reg [7:0] delay;

always @(posedge clk)
if(clear == 1'b1)
delay = 8'b0;
else 
delay = delay;

always @ *
cct_output = cct_input ^ delay;

endmodule
