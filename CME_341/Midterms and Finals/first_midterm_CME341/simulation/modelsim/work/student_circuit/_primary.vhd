library verilog;
use verilog.vl_types.all;
entity student_circuit is
    port(
        clk             : in     vl_logic;
        clear           : in     vl_logic;
        cct_input       : in     vl_logic_vector(7 downto 0);
        cct_output      : out    vl_logic_vector(7 downto 0)
    );
end student_circuit;
