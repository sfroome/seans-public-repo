module student_circuit_Q1(
input clk, clear,
input [7:0] cct_input,
output reg [7:0] cct_output);

reg [2:0] count;

//wire [7:0] not_cct_input;
//not n1(not_cct_input, cct_input);

always @ (posedge clk)
if(clear == 1'b1)
count = 3'b0;
else 
count = count + 3'b1;

always @ *
if (count < 3'b111)
cct_output = cct_input;
else //if (count >= 3'b111)
//begin
//cct_output[0] = ~cct_input[0];
//cct_output[1] = ~cct_input[1];
//cct_output[2] = ~cct_input[2];
//cct_output[3] = ~cct_input[3];
//cct_output[4] = ~cct_input[4];
//cct_output[5] = ~cct_input[5];
//cct_output[6] = ~cct_input[6];
//cct_output[7] = ~cct_input[7];
cct_output = ~cct_input;
//end
//else 
//cct_output = cct_output;
endmodule
