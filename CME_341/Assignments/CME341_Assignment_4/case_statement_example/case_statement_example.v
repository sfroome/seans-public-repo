module case_statement_example (select,
data_0, data_1, data_2, data_3,
data_out);
/******** IMPORTANT NOTE **********
Some compilers, for example MAX PLUS II, do not handle "don’t cares"
properly - but some do, for example Leonardo Spectrum */
input [3:0] select;
input [1:0] data_0, data_1, data_2, data_3;
output [1:0] data_out;
reg [1:0] data_out;
always @ (select or data_0 or
data_1 or data_2 or data_3)
begin
casex (select) // case x is used when don’t cares are
// used in the case item
4'b1xxx : data_out = data_0; // if (select[3]==1)
4'bx1xx : data_out = data_1; // else if (select[2]==1)
4'bxx1x : data_out = data_2; // else if (select[1]==1)
4'bxxx1 : data_out = data_3; // else if (select[0]==1)
default : data_out = 2'b00; // else
// Really it's more like
// 1xxx, it will always enter that first so.
//01xx is only possible
//001x is only possible given the order of the case statement
//0001 Only one case.
//Actually, it doesn't work. It doesn't handle the don't cares correctly.
endcase
end
endmodule
