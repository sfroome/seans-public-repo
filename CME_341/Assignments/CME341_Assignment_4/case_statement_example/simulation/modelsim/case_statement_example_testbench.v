`timescale 1 ns / 100 ps
module case_statement_example_testbench();
reg [3:0] select;
reg [1:0] data_0, data_1, data_2, data_3;
wire [1:0] data_out;
initial
#16000 $stop;
initial
data_0 = 2'b01;
initial
data_1 = 2'b10;
initial
data_2 = 2'b11;
initial
data_3 = 2'b11;
initial
select = 4'b0000;
always
#1000 select = select + 4'b0001;
case_statement_example cse(.select(select), .data_0(data_0), 
.data_1(data_1), .data_2(data_2), .data_3(data_3));
endmodule
