module literal_ex (b, a);
input [3:0] b;
output [3:0] a;
reg [3:0] a;
always @ *
if ( (&b)==1'b1 )
	a=3'b1001;  // It won't depend on b because both the if and the else will hold the same value \
					//(the MSB gets chopped off making them both equal to 1)
else a=4'b0001;
//Should also note the Compiler SPECIFICALLY states that 4 input pins aren't driving logic aka b[3:0].
// And the fact that line 7 truncates a to 3 bits. Morale of the story: Read the warnings.
endmodule
