`timescale 1 ns / 100 ps
module literal_ex_testbench();
reg [3:0] b;
wire [3:0] a;
initial
#16000 $stop; 
initial
b=4'H0;
always
#1000 b = b + 4'H1; // the delay of 1000 assumes
// a timescale of 1 ns
literal_ex lel(.a(a), .b(b));
endmodule

