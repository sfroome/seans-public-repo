`timescale 1 ns / 100 ps
module q8b_testbench();
reg clk;
reg d;
reg clk_enable;
wire q;

initial
#16000 $stop; 

initial clk = 1'b0;
always
#1000 clk = ~clk;

initial clk_enable = 1'b0;
initial
begin
#8000 clk_enable = 1'b1;
end
initial d = 1'b0;
initial
begin
#1500 d = 1'b1;
#1500 d = 1'b0;
#1500 d = 1'b1;
#1500 d = 1'b0;
#2500 d = 1'b1;
#2500 d = 1'b0;
#3500 d = 1'b1;
#1500 d = 1'b0;

end


q8b q8b1(.clk_enable(clk_enable), .d(d), .clk(clk), .q(q));
endmodule

