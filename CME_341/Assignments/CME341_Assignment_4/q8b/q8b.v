module q8b(d, clk_enable, clk, q);

input d, clk_enable, clk; 
output reg q;

/*
always @ (posedge clk)
if (clk_enable == 1'b1) q = d;
else q = q; 
*/
wire case1;
wire case2;
wire not_clk_enable;
wire d1;

not not1(not_clk_enable, clk_enable);

and and1(case1, d, clk_enable);
and and2(case2, not_clk_enable, q);

or or1(d1, case1, case2);

DFFE flop_1(.d(d1), .clk(clk), .ena(), 
.clrn(), .prn(), .q(q));




endmodule
