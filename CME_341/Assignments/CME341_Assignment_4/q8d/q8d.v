module q8d(d, set, clear, clk, q);
input d, set, clear, clk; 
output reg q;

/*input d, set, clear, clk; output q;
always @ (posedge clk or posedge clear or posedge set)
if (set == 1’b1) q = 1’b1;
else if (clear == 1’b1) q = 1’b0;
else q = d;*/

wire case1;
wire case2;
wire case3;
wire not_set;
wire not_clear;
wire not_q;
wire not_case2;
wire d1;
wire clk1;

not ns(not_set, set);
not nc(not_clear, clear);
//not nq(not_q, q);
//not nc2(not_case2, case2);

and and1(case1, set, 1'b1);
and and2(case2, not_set, clear, 1'b0); 
and and3(case3, not_clear, d);

or or1(d1, case1, case2, case3);
or or2(clk1, clk, set, clear);

DFFE flop_1(
.d(d1), .clk(clk1), .ena(), 
.clrn(), .prn(), .q(q));

endmodule
