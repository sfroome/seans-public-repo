module if_else_combinational_logic(
input [3:0] a, b,
output reg [3:0] y );
always @ *
if (a==4'b1111) 
y=b;
else
y=~b;
// Keeping both if-else statements in the always block
//violates having one argument in the always block
// Error is "Expecting 'endmodule'". This doesn't really explain it
// But it sort of does in that the second argument shouldn't be there period.


// 2C) Both are valid reasons, but since begin-end wrappers are discouraged
// More likely people are forgetting the second always line
// 2D) The problem with begin-end wrappers is that they can make the FPGA behave in 
// very unexpected ways. Instead of two d flip flops that shift the value of q along 
// at each clock cycle you can end up with both q1 and q2 being equal to D at the same time.

if (a==4'b0111) 
y=b;
else
y=~b;

endmodule