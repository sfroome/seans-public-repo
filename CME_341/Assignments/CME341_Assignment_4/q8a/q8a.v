module q8a(clk, toggle, q);
input clk, toggle; 
output reg q;
/*always @ (posedge clk)
if (toggle == 1'b1) q = ~q;
else q = q;*/
wire not_q;
wire not_toggle;
wire case1;
wire case2;
wire d;
not nq(not_q, q);
not nt(not_toggle, toggle);
and and1(case1, toggle, not_q);
and and2(case2, q, not_toggle);
or or1(d,case1,case2);
DFFE dff1(.d(d), .clk(clk),.ena(),.clrn(),.prn(),.q(q));
endmodule

