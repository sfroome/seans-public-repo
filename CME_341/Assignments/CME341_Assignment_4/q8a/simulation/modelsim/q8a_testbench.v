`timescale 1 ns / 100 ps
module q8a_testbench();
reg clk;
reg toggle;
wire q;

initial
#16000 $stop; 

initial clk = 1'b0;
always
#1000 clk = ~clk;

initial toggle = 1'b0;
initial
begin
#8000 toggle = 1'b1;
end


q8a q8a1(.toggle(toggle), .clk(clk), .q(q));
endmodule
