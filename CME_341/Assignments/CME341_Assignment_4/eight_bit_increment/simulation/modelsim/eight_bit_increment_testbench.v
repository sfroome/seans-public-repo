`timescale 1 ns / 100 ps
module eight_bit_increment_testbench();
reg [7:0] low_number;
reg [1:0] select;
wire [7:0] high_number;
initial
#16000 $stop;
initial
low_number = 8'b0;
initial 
select = 2'b0;
always
#1000 select = select + 2'b01;
eight_bit_increment ebi(.low_number(low_number), 
		.select(select), .high_number(high_number));
endmodule
