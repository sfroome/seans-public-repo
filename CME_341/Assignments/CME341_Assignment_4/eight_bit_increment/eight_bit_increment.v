module eight_bit_increment(
input [7:0] low_number,
input [1:0] select,
output reg [7:0] high_number
);
always @ *
case(select)
2'b00: high_number = low_number + 8'd1;
2'b01: high_number = low_number + 8'd7;
2'b10: high_number = low_number + 8'd255;
2'b11: high_number = low_number + 8'd249; 
default: high_number = high_number;
endcase
endmodule
