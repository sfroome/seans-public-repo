`timescale 1 ns / 100 ps

module if_else_combinational_logic_testbench();

reg [3:0] a;
reg [3:0] b;
wire [3:0] y;

initial
#16000 $stop; 

initial 
a = 4'b0000;
initial
b = 4'b0001;
always
#1000 a = a + 4'b0001;

if_else_combinational_logic iecl(.a(a), .b(b), .y(y));

endmodule
