module if_else_combinational_logic(
input [3:0] a, b,
output reg [3:0] y );
always @ *
begin
if (a==4'b1111) // Compiler does not mention ignoring this if-else
y=b;
else
y=~b;
if (a==4'b0111)
y=b;
else
y=~b;
end
endmodule