module q8c(d, set, clear, clk, q);
input d, set, clear, clk; 
output reg q;

/*
always @ (posedge clk)
if (set == 1'b1) 
	q = 1'b1;
else if (clear == 1'b1) 
	q = 1'b0;
else q = d;  
*/

wire case1;
wire case2;
wire case3;
wire not_set;
wire not_clear;
wire not_q;
wire not_case2;
wire d1;

not ns(not_set, set);
not nc(not_clear, clear);
//not nq(not_q, q);
//not nc2(not_case2, case2);

and and1(case1, set, 1'b1);
and and2(case2, not_set, clear, 1'b0); 
and and3(case3, not_clear, d);

or or1(d1, case1, case2, case3);

DFFE flop_1(
.d(d1), .clk(clk), .ena(), 
.clrn(), .prn(), .q(q));

endmodule
