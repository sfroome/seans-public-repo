`timescale 1 ns / 100 ps
module q8c_testbench();
reg clk;
reg d;
reg set;
reg clear;
wire q;

initial
#16000 $stop; 

initial clk = 1'b0;
always
#250 clk = ~clk;

initial d = 1'b0;
initial
begin
#1500 d = 1'b1;
#1500 d = 1'b0;
#1500 d = 1'b1;
#1500 d = 1'b0;
#2500 d = 1'b1;
#2500 d = 1'b0;
#3500 d = 1'b1;
#1500 d = 1'b0;
end

initial set = 1'b0;
initial
begin
#750 set = 1'b1;
#2250 set = 1'b0;
#2750 set = 1'b1;
#500 set = 1'b0;
#750 set = 1'b1;
#500 set = 1'b0;
#500 set = 1'b1;
#2000 set = 1'b0;
#2500 set = 1'b1;
#2500 set = 1'b0; 
end

initial clear = 1'b0;
initial
begin
#2250 clear = 1'b1;
#2750 clear = 1'b0;
#500 clear = 1'b1;
#750 clear = 1'b0;
#500 clear = 1'b1;
#500 clear = 1'b0;
#2000 clear = 1'b1;
#2500 clear = 1'b0;
#2500 clear = 1'b1; 
#750 clear = 1'b0;
end

q8c q8c1(.set(set), .clear(clear), .clk(clk), .d(d), .q(q));
endmodule

//NOTE: At 6.25us in the testbench, for some reason, despite d being low, set being low, and clear being low, 
//q manages to go high on the posedge for some stupid reason that eludes me. It appears to work 
// in every other case I can think of. 
// NOTE REGARDING NOTE: Unless... because the inputs (specifically set) are going low at the same time
// at the clock edge, it takes it as a high.

