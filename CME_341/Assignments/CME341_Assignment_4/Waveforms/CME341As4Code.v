// CME 341 Assignment 4
// Sean Froome
// October 5, 2015
// All code, inclduing testbenches for each question
// are included in this file, sorted by question number,
// and starting with question 1.


// Question 1
module if_else_combinational_logic(
input [3:0] a, b,
output reg [3:0] y );
always @ *
begin
if (a==4'b1111) // Compiler does not mention ignoring this if-else
y=b;
else
y=~b;
if (a==4'b0111)
y=b;
else
y=~b;
end
endmodule

`timescale 1 ns / 100 ps
module if_else_combinational_logic_testbench();
reg [3:0] a;
reg [3:0] b;
wire [3:0] y;
initial
#16000 $stop; 
initial 
a = 4'b0000;
initial
b = 4'b0001;
always
#1000 a = a + 4'b0001; // Increase a by 1 every 1000ns
if_else_combinational_logic iecl(.a(a), .b(b), .y(y));
endmodule


// Question 2

module if_else_combinational_logic(
input [3:0] a, b,
output reg [3:0] y );
always @ *
if (a==4'b1111) 
y=b;
else
y=~b;
// Keeping both if-else statements in the always block
//violates having one argument in the always block
// Error is "Expecting 'endmodule'". This doesn't really explain it
// But it sort of does in that the second argument shouldn't be there period.


// 2C) Both are valid reasons, but since begin-end wrappers are discouraged
// More likely people are forgetting the second always line

// 2D) The problem with begin-end wrappers is that they can make the FPGA behave in 
// very unexpected ways. Instead of two d flip flops that shift the value of q along 
// at each clock cycle you can end up with both q1 and q2 being equal to D at the same time.

if (a==4'b0111) 
y=b;
else
y=~b;

endmodule

// Question 3-4

module case_statement_example (select,
data_0, data_1, data_2, data_3,
data_out);
/******** IMPORTANT NOTE **********
Some compilers, for example MAX PLUS II, do not handle "don’t cares"
properly - but some do, for example Leonardo Spectrum */
input [3:0] select;
input [1:0] data_0, data_1, data_2, data_3;
output [1:0] data_out;
reg [1:0] data_out;
always @ (select or data_0 or
data_1 or data_2 or data_3)
begin
casex (select) // case x is used when don’t cares are
// used in the case item
4'b1xxx : data_out = data_0; // if (select[3]==1)
4'bx1xx : data_out = data_1; // else if (select[2]==1)
4'bxx1x : data_out = data_2; // else if (select[1]==1)
4'bxxx1 : data_out = data_3; // else if (select[0]==1)
default : data_out = 2'b00; // else
// Really it's more like
// 1xxx, it will always enter that first so.
//01xx is only possible
//001x is only possible given the order of the case statement
//0001 Only one case.
//Actually, it doesn't work. It doesn't handle the don't cares correctly.
endcase
end
endmodule

`timescale 1 ns / 100 ps
module case_statement_example_testbench();
reg [3:0] select;
reg [1:0] data_0, data_1, data_2, data_3;
wire [1:0] data_out;
initial
#16000 $stop;
initial
data_0 = 2'b01;
initial
data_1 = 2'b10;
initial
data_2 = 2'b11;
initial
data_3 = 2'b11;
initial
select = 4'b0000;
always
#1000 select = select + 4'b0001;
case_statement_example cse(.select(select), .data_0(data_0), 
.data_1(data_1), .data_2(data_2), .data_3(data_3));
endmodule


// Question 5

module eight_bit_increment(
input [7:0] low_number,
input [1:0] select,
output reg [7:0] high_number
);
always @ *
case(select)
2'b00: high_number = low_number + 8'd1;
2'b01: high_number = low_number + 8'd7;
2'b10: high_number = low_number + 8'd255;
2'b11: high_number = low_number + 8'd249; 
default: high_number = high_number;
endcase
endmodule

`timescale 1 ns / 100 ps
module eight_bit_increment_testbench();
reg [7:0] low_number;
reg [1:0] select;
wire [7:0] high_number;
initial
#16000 $stop;
initial
low_number = 8'b0;
initial 
select = 2'b0;
always
#1000 select = select + 2'b01;
eight_bit_increment ebi(.low_number(low_number), 
		.select(select), .high_number(high_number));
endmodule


// Question 6

module vector_combiner(
input [15:0] A, 
input [15:0] B,
output reg [31:0] C
);
integer i;
integer j;
always @ *
begin
for(i = 1; i <=31; i= i+2)
C[i] = B[i/2];
for(j = 0; j <=30; j = j+2)
C[j] = A[j/2];
end
endmodule

`timescale 1 ns / 100 ps
module vector_combiner_testbench();
reg [15:0] A;
reg [15:0] B;
wire [31:0] C;
initial
#16000 $stop; 
initial 
A = 16'b1111111111111111;
initial
B = 16'b0000000000000000;
vector_combiner vcl(.A(A), .B(B), .C(C));
endmodule


// Question 7

module literal_ex (b, a);
input [3:0] b;
output [3:0] a;
reg [3:0] a;
always @ *
if ( (&b)==1'b1 )
	a=3'b1001;  // It won't depend on b because both the if and the else will hold the same value \
					//(the MSB gets chopped off making them both equal to 1)
else a=4'b0001;
//Should also note the Compiler SPECIFICALLY states that 4 input pins aren't driving logic aka b[3:0].
// And the fact that line 7 truncates a to 3 bits. Morale of the story: Read the warnings.
endmodule

`timescale 1 ns / 100 ps
module literal_ex_testbench();
reg [3:0] b;
wire [3:0] a;
initial
#16000 $stop; 
initial
b=4'H0;
always
#1000 b = b + 4'H1; // the delay of 1000 assumes
// a timescale of 1 ns
literal_ex lel(.a(a), .b(b));
endmodule

// Question 8-10

8A)

module q8a(clk, toggle, q);
input clk, toggle; 
output reg q;
/*always @ (posedge clk)
if (toggle == 1'b1) q = ~q;
else q = q;*/
wire not_q;
wire not_toggle;
wire case1;
wire case2;
wire d;
not nq(not_q, q);
not nt(not_toggle, toggle);
and and1(case1, toggle, not_q);
and and2(case2, q, not_toggle);
or or1(d,case1,case2);
DFFE dff1(.d(d), .clk(clk),.ena(),.clrn(),.prn(),.q(q));
endmodule

`timescale 1 ns / 100 ps
module q8a_testbench();
reg clk;
reg toggle;
wire q;
initial
#16000 $stop; 
initial clk = 1'b0;
always
#1000 clk = ~clk;
initial toggle = 1'b0;
initial
begin
#8000 toggle = 1'b1;
end
q8a q8a1(.toggle(toggle), .clk(clk), .q(q));
endmodule


8B)

module q8b(d, clk_enable, clk, q);

input d, clk_enable, clk; 
output reg q;
/*
always @ (posedge clk)
if (clk_enable == 1'b1) q = d;
else q = q; 
*/
wire case1;
wire case2;
wire not_clk_enable;
wire d1;
not not1(not_clk_enable, clk_enable);
and and1(case1, d, clk_enable);
and and2(case2, not_clk_enable, q);
or or1(d1, case1, case2);
DFFE flop_1(.d(d1), .clk(clk), .ena(), 
.clrn(), .prn(), .q(q));
endmodule

`timescale 1 ns / 100 ps
module q8b_testbench();
reg clk;
reg d;
reg clk_enable;
wire q;
initial
#16000 $stop; 
initial clk = 1'b0;
always
#1000 clk = ~clk;
initial clk_enable = 1'b0;
initial
begin
#8000 clk_enable = 1'b1;
end
initial d = 1'b0;
initial
begin
#1500 d = 1'b1;
#1500 d = 1'b0;
#1500 d = 1'b1;
#1500 d = 1'b0;
#2500 d = 1'b1;
#2500 d = 1'b0;
#3500 d = 1'b1;
#1500 d = 1'b0;
end
q8b q8b1(.clk_enable(clk_enable), .d(d), .clk(clk), .q(q));
endmodule

8C)
`timescale 1 ns / 100 ps
module q8c_testbench();
reg clk;
reg d;
reg set;
reg clear;
wire q;
initial
#16000 $stop; 
initial clk = 1'b0;
always
#250 clk = ~clk;
initial d = 1'b0;
initial
begin
#1500 d = 1'b1;
#1500 d = 1'b0;
#1500 d = 1'b1;
#1500 d = 1'b0;
#2500 d = 1'b1;
#2500 d = 1'b0;
#3500 d = 1'b1;
#1500 d = 1'b0;
end
initial set = 1'b0;
initial
begin
#750 set = 1'b1;
#2250 set = 1'b0;
#2750 set = 1'b1;
#500 set = 1'b0;
#750 set = 1'b1;
#500 set = 1'b0;
#500 set = 1'b1;
#2000 set = 1'b0;
#2500 set = 1'b1;
#2500 set = 1'b0; 
end
initial clear = 1'b0;
initial
begin
#2250 clear = 1'b1;
#2750 clear = 1'b0;
#500 clear = 1'b1;
#750 clear = 1'b0;
#500 clear = 1'b1;
#500 clear = 1'b0;
#2000 clear = 1'b1;
#2500 clear = 1'b0;
#2500 clear = 1'b1; 
#750 clear = 1'b0;
end
q8c q8c1(.set(set), .clear(clear), .clk(clk), .d(d), .q(q));
endmodule
//NOTE: At 6.25us in the testbench, for some reason, despite d being low, set being low, and clear being low, 
//q manages to go high on the posedge for some stupid reason that eludes me. It appears to work 
// in every other case I can think of. 
// NOTE REGARDING NOTE: Unless... because the inputs (specifically set) are going low at the same time
// at the clock edge, it takes it as a high.


8D)

module q8d(d, set, clear, clk, q);
input d, set, clear, clk; 
output reg q;
/*input d, set, clear, clk; output q;
always @ (posedge clk or posedge clear or posedge set)
if (set == 1’b1) q = 1’b1;
else if (clear == 1’b1) q = 1’b0;
else q = d;*/
wire case1;
wire case2;
wire case3;
wire not_set;
wire not_clear;
wire not_q;
wire not_case2;
wire d1;
wire clk1;
not ns(not_set, set);
not nc(not_clear, clear);
//not nq(not_q, q);
//not nc2(not_case2, case2);
and and1(case1, set, 1'b1);
and and2(case2, not_set, clear, 1'b0); 
and and3(case3, not_clear, d);
or or1(d1, case1, case2, case3);
or or2(clk1, clk, set, clear);
DFFE flop_1(
.d(d1), .clk(clk1), .ena(), 
.clrn(), .prn(), .q(q));
endmodule

`timescale 1 ns / 100 ps
module q8d_testbench();
reg clk;
reg d;
reg set;
reg clear;
wire q;
initial
#16000 $stop; 
initial clk = 1'b0;
always
#250 clk = ~clk;
initial d = 1'b0;
initial
begin
#1500 d = 1'b1;
#1500 d = 1'b0;
#1500 d = 1'b1;
#1500 d = 1'b0;
#2500 d = 1'b1;
#2500 d = 1'b0;
#3500 d = 1'b1;
#1500 d = 1'b0;
end
initial set = 1'b0;
initial
begin
#750 set = 1'b1;
#2250 set = 1'b0;
#2750 set = 1'b1;
#500 set = 1'b0;
#750 set = 1'b1;
#500 set = 1'b0;
#500 set = 1'b1;
#2000 set = 1'b0;
#2500 set = 1'b1;
#2500 set = 1'b0; 
end
initial clear = 1'b0;
initial
begin
#2250 clear = 1'b1;
#2750 clear = 1'b0;
#500 clear = 1'b1;
#750 clear = 1'b0;
#500 clear = 1'b1;
#500 clear = 1'b0;
#2000 clear = 1'b1;
#2500 clear = 1'b0;
#2500 clear = 1'b1; 
#750 clear = 1'b0;
end
q8d q8c1(.set(set), .clear(clear), .clk(clk), .d(d), .q(q));
endmodule



