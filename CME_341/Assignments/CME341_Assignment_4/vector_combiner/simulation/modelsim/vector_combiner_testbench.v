`timescale 1 ns / 100 ps
module vector_combiner_testbench();
reg [15:0] A;
reg [15:0] B;
wire [31:0] C;
initial
#32000 $stop; 
initial 
A = 16'b1111111111111111;
initial
B = 16'b0000000000000000;
vector_combiner vcl(.A(A), .B(B), .C(C));
endmodule
