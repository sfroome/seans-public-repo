module vector_combiner(
input [15:0] A, 
input [15:0] B,
output reg [31:0] C
);
integer i;
integer j;
always @ *
begin
for(i = 1; i <=31; i= i+2)
C[i] = B[i/2];
for(j = 0; j <=30; j = j+2)
C[j] = A[j/2];
end
endmodule
