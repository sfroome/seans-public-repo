module q8e(
input clk,
input [3:0] d,
output reg [3:0] q);

//input clk;
//input [3:0] d;
//output [3:0] q;
//reg [3:0] q;
always @ (posedge clk)
casex({d[3],d[1]})
2'b00 : begin q[3]=1'b0; q[2:0] = q[2:0]; end
2'b1x : begin q[3] = ~q[3]; q[2:0] = 3'b000; end
default: q = d;
endcase

endmodule

