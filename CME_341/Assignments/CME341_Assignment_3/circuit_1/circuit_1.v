module circuit_1 ( a, b, c);
input [7:0] a,b;
output [5:0] c;
reg [5:0] c;
always @ (a or b)
begin
if (a == 8'b111x_xxxx) // Bits  0 through 4 would be unused in the circuit design? <---- Q3
c = b[5:0];			 	// Schematic will look something like pg40 of notes (example is case statement but)
else if (a == 8'b11xx_xxxx)
c = b[6:1];
else c = b[7:2];
end
endmodule