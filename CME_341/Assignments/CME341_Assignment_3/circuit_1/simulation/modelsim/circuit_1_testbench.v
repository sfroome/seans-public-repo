`timescale 1 ns / 100 ps
module circuit_1_testbench();
reg [7:0] a;
reg [7:0] b;
wire [6:0] c;
initial
#16000 $stop;
initial
begin
b = 8'b11111101; // should be either 11111, 11110, 11101
a = 8'b11110000;
#1000
a = 8'b11111111;
#1000
a = 8'b11000000;
#1000
a = 8'b11011111;
#1000
a = 8'b10011111;
#1000
a = 8'b10000000;
#1000
a = 8'b01110000;
#1000
a = 8'b01100000;
end 
circuit_1 c1(.a(a), .b(b), .c(c));
endmodule