module number_typo (x , y); 
input [3:0] x; 
output y; 
reg y;
always @ x 
if (x == 3'b1100) // should be 4’b1100
						// Amazingly, the Compiler doesn't even notice this error.
	y = 1;
else y = 0;
endmodule
