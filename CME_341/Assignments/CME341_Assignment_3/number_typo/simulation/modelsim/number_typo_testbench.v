`timescale 1 ns / 100 ps
module number_typo_testbench();
reg [3:0] x;
wire y;
initial
#16000 $stop;
initial
begin

x = 4'b0000;
#1000
x = 4'b0001;
#1000
x = 4'b0010;
#1000
x = 4'b0011;
#1000
x = 4'b0100; // Because of the typo, this one will activate the if statement 
#1000
x = 4'b0101;
#1000
x = 4'b0110;
#1000
x = 4'b0111;
#1000
x = 4'b1000;
#1000
x = 4'b1001;
#1000
x = 4'b1010;
#1000
x = 4'b1011;
#1000
x = 4'b1100; // Won't fire the correct output because of the typo
#1000
x = 4'b1101;
#1000
x = 4'b1111;
end 
number_typo nt1(.x(x), .y(y));
endmodule
