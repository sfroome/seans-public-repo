module circuit_3 ( gate,d,q);
input gate,d;
output q;
reg q;
always @ (gate or d)
begin
if ( gate == 1'b1 )
q = d;
else q = q;
end
endmodule


//See page 33 of chapter 3.