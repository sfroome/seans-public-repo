`timescale 1 ns/ 1 ps
module circuit_3_testbench();
reg gate, d;
wire q;
initial #10000 $stop;

initial d = 1'b0;
always 
#100 d = ~d;

initial
begin
gate = 1'b0;
#250 gate = 1'b1;
#500 gate = 1'b0;
#750 gate = 1'b1;
#1000 gate = 1'b0;

#1300 gate =1'b1;
#2600 gate = 1'b0;


end





circuit_3 c(.gate(gate), .d(d), .q(q));
endmodule
