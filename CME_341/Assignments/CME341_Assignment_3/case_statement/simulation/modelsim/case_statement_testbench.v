`timescale 1 ns / 100 ps 

// This testbench will check every single possible eight bit value starting at 8'b0 (8'd0) to 8'b11111111 (8'd255)

module eight_bit_vector_testbench();
// no inputs or outputs
reg a,b,c;
reg [2:0] sel;
wire d;

initial
#10000 $stop; //Stop at 256 microseconds. Don't need any more than that.
initial
begin
a = 0;
b = 0;
c = 0;
#100
sel = 3'b000;
#100
sel = 3'b001;
#100
sel = 3'b010;
#100
sel = 3'b011;
#100
sel = 3'b100;
#100
sel = 3'b101;
#100
sel = 3'b110;
#100
sel = 3'b111;
#200
a = 0;
b = 0;
c = 1;
#100
sel = 3'b000;
#100
sel = 3'b001;
#100
sel = 3'b010;
#100
sel = 3'b011;
#100
sel = 3'b100;
#100
sel = 3'b101;
#100
sel = 3'b110;
#100
sel = 3'b111;
#200
a = 0;
b = 1;
c = 0;
#100
sel = 3'b000;
#100
sel = 3'b001;
#100
sel = 3'b010;
#100
sel = 3'b011;
#100
sel = 3'b100;
#100
sel = 3'b101;
#100
sel = 3'b110;
#100
sel = 3'b111;
#200
a = 0;
b = 1; 
c = 1;
#100
sel = 3'b000;
#100
sel = 3'b001;
#100
sel = 3'b010;
#100
sel = 3'b011;
#100
sel = 3'b100;
#100
sel = 3'b101;
#100
sel = 3'b110;
#100
sel = 3'b111;
#200
a = 1;
b = 0;
c = 0;
#100
sel = 3'b000;
#100
sel = 3'b001;
#100
sel = 3'b010;
#100
sel = 3'b011;
#100
sel = 3'b100;
#100
sel = 3'b101;
#100
sel = 3'b110;
#100
sel = 3'b111;
#200
a = 1;
b = 0;
c = 1;
#100
sel = 3'b000;
#100
sel = 3'b001;
#100
sel = 3'b010;
#100
sel = 3'b011;
#100
sel = 3'b100;
#100
sel = 3'b101;
#100
sel = 3'b110;
#100
sel = 3'b111;
#200
a = 1;
b = 1;
c = 0;
#100
sel = 3'b000;
#100
sel = 3'b001;
#100
sel = 3'b010;
#100
sel = 3'b011;
#100
sel = 3'b100;
#100
sel = 3'b101;
#100
sel = 3'b110;
#100
sel = 3'b111;
#200
a = 1;
b = 1;
c = 1;
#100
sel = 3'b000;
#100
sel = 3'b001;
#100
sel = 3'b010;
#100
sel = 3'b011;
#100
sel = 3'b100;
#100
sel = 3'b101;
#100
sel = 3'b110;
#100
sel = 3'b111;
end
case_statement cs1(.a(a), .b(b), .c(c), .sel(sel), .d(d));

endmodule
