module case_statement(
input a,b,c,
input [2:0]sel,
output reg d
);

always @ *
case(sel)
0: d = a&b;
1: d = a|b;
2: d = b&c;
3: d = b|c;
4: d = a&c;
5: d = a|c;
6: d = a&b&c;
7: d = a|b|c;
default: d = 1'b0;
endcase

endmodule

