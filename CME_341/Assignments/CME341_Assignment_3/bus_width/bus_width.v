//Q9B 
// Wire either wasn't declared as a wire vector
//OR Wire vector is incorrect size

module bus_width (x , y); 
input [3:0] x; 
output [3:0] y;

//Remember to recompile this verilog file before 
// simulating in ModelSim in order to see the changes in W1

//wire [3:0] W1; 

// this is correct but will be 
// changed in a subsequent question 
// Le me after reading the above comment: oh. gotcha.

// For 9 ii)
//wire [1:0] W1; // YAY INTENTIONALLY BREAKING THINGS :) 
// SO I CAN AVOID ACCIDENTALLY BREAKING THINGS IN THE FUTURE!
// It compiles successfully, but you get an error saying
// "2 hierarchies have connectivity warnings" 
// The "hierarchies are the adders that are being called. Since x is too small to connect
// to all of the pins on the adder inputs (4 bits)
// It spits that warning.
// Also that output pins are stuck at VCC or GND
// And that "Design contains 2 input pins that do not drive logic"
// Basically, you get several warnings for missing this, so 
// It SHOULD be quite easy to catch this if someone checks their warnings.


 //For 9 iii)
 // The Compiler will create a wire W1 because it sees it needs it
 // But it makes a single wire, and not a 4 bit wire vector that is 
 // actually required.
 // so you get the same warnings as you do for 9 ii)
 // But now there are three input pins that don't drive logic because they don't get hooked up to the adders.
 
 
adder adder_1(.in_1(x), .in_2(4'b0001), .out_1(W1) ); 
adder adder_2(.in_1(W1), .in_2(4'b0100), .out_1(y) );

endmodule


module adder(in_1, in_2, out_1); 

input [3:0] in_1, in_2; 
output [3:0] out_1; 

reg [3:0] out_1;

always @ (in_1 or in_2) 
out_1 = in_1 + in_2; 
endmodule
