module eight_bit_vector(
input [7:0] eight_bit_vector,
output reg is_vector);

always @ * // Because Combinational Logic
if(eight_bit_vector == 8'b11010011) // Simple enough... check for the desired value
	is_vector = 1'b1;	// If it's that value, make the output high
else
	is_vector = 1'b0; // If not, the output is zero for false.
endmodule
