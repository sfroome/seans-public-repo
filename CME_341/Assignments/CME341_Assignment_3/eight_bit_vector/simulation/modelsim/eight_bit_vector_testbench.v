`timescale 1 ns / 100 ps 

// This testbench will check every single possible eight bit value starting at 8'b0 (8'd0) to 8'b11111111 (8'd255)

module eight_bit_vector_testbench();
// no inputs or outputs
reg [7:0] eight_bit_vector;
wire is_vector;

initial
#256000 $stop; //Stop at 256 microseconds. Don't need any more than that.
initial eight_bit_vector = 8'b00000000; // Make sure eight bit vector starts at zero
always
#1000 eight_bit_vector = eight_bit_vector + 8'b00000001; //Increases the vector by 1 every microsecond


eight_bit_vector ebv(.eight_bit_vector(eight_bit_vector),
.is_vector(is_vector));

endmodule