`timescale 1 ns / 100 ps 

// This testbench will check every single possible eight bit value starting at 8'b0 (8'd0) to 8'b11111111 (8'd255)

module and_or_invert_testbench();
// no inputs or outputs
reg in_0,in_1,select;

wire out;


initial
#80000 $stop; //Stop at 256 microseconds. Don't need any more than that

initial
begin
in_0 = 1'b0;
in_1 = 1'b0;
select = 1'b0;

#10000
in_0 = 1'b0;
in_1 = 1'b0;
select = 1'b1;

#10000
in_0 = 1'b0;
in_1 = 1'b1;
select = 1'b0;

#10000
in_0 = 1'b0;
in_1 = 1'b1;
select = 1'b1;

#10000
in_0 = 1'b1;
in_1 = 1'b0;
select = 1'b0;

#10000
in_0 = 1'b1;
in_1 = 1'b0;
select = 1'b1;

#10000
in_0 = 1'b1;
in_1 = 1'b1;
select = 1'b0;

#10000
in_0 = 1'b1;
in_1 = 1'b1;
select = 1'b1;

end

and_or_invert aoi_1(.in_0(in_0),.in_1(in_1), 
.select(select), .out(out));

endmodule
