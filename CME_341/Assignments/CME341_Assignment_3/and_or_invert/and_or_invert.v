/*module and_or_invert (in_0, in_1, select, out);
input in_0, in_1, select;
output out;
reg out;
always @ (in_0 or in_1 or select)
if (select == 1'b0) out = ~in_0;
else out = ~in_1;
endmodule*/

module and_or_invert(
input in_0, in_1, select,
output out);

wire select_not;
wire and_1_to_nor;
wire and_2_to_nor;


not not_1(select_not, select);

and and_1(and_1_to_nor, in_0, select_not);
and and_2(and_2_to_nor, in_1, select);

nor nor_1(out, and_1_to_nor, and_2_to_nor);

endmodule
