module circuit_2 ( a, b, c);
input [1:0] a,b;
output c;
reg c;
always @ (a or b)
begin
	if ( b == (2'b10 | 2'b01) )
		c = &(b ^ a); //reduction operator & ie: Produces one bit output.
	else
		begin 
			c = c ^ ~&a; 
		end
end

endmodule