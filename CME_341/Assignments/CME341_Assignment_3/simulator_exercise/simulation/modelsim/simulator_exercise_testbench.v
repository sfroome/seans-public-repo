
`timescale 1 ns/ 1 ps
module simulator_exercise_testbench ();
reg [7:0] x;
wire [7:0] y;
initial #100000 $stop;
initial
x = 8'h00;
always
#100 x = x + 8'h01;
simulator_exercise cct_1(.x(x), .y(y));
endmodule