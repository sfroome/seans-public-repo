module simulator_exercise (
input [7:0] x,
output reg [7:0] y );
always @ *
y=x;
endmodule