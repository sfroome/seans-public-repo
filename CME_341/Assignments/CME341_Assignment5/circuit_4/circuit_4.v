/*
module circuit_4 (clk, data_in, data_out);
input clk;
input [1:0] data_in;
output [2:0] data_out;
wire [3:0] internal_signal;
assign internal_signal[0] = & data_in;
assign internal_signal[1] = internal_signal[0] ^ data_out[2];
assign internal_signal[2] = internal_signal[1] ^ data_out[0];
assign internal_signal[3] = internal_signal[2] ^ data_out[1];

dff dff_1(.clk(clk),.d(internal_signal[1]),.q(data_out[0]));
dff dff_2(.clk(clk),.d(internal_signal[2]),.q(data_out[1]));
dff dff_3(.clk(clk),.d(internal_signal[3]),.q(data_out[2]));
endmodule 
*/

/* Question 4b */

/*
module circuit_4(
input clk,
input [1:0] data_in,
output reg [2:0] data_out);

reg [3:0] internal_signal;

always @(posedge clk)
data_out[0] = internal_signal[1];
always @ (posedge clk)
data_out[1] = internal_signal[2];
always @ (posedge clk)
data_out[2] = internal_signal[3];

always @ *
internal_signal[0] = data_in[0] & data_in[1];
always @ *
internal_signal[1] = data_out[2] ^ internal_signal[0];
always @ *
internal_signal[2] = data_out[0] ^ internal_signal[1];
always @ *
internal_signal[3] = data_out[1] ^ internal_signal[2];

endmodule 

*/


/* Question 4c */

/*
module circuit_4(
input clk,
input [1:0] data_in,
output reg [2:0] data_out);
reg [3:0] internal_signal;

always @ (posedge clk)
begin
data_out[0] <= internal_signal[1];
data_out[1] <= internal_signal[2];
data_out[2] <= internal_signal[3];
end

always @ *
begin
internal_signal[0] <= data_in[0] & data_in[1];
internal_signal[1] <= data_out[2] ^ internal_signal[0];
internal_signal[2] <= data_out[0] ^ internal_signal[1];
internal_signal[3] <= data_out[1] ^ internal_signal[2];
end

endmodule


*/

// Structural Behaviour
module circuit_4(
input clk,
input [1:0] data_in,
output reg [2:0] data_out);
wire [3:0] internal_signal;

and is0(internal_signal[0], data_in[1], data_in[0]);
xor is1(internal_signal[1], data_out[2], internal_signal[0]);
xor is2(internal_signal[2], data_out[0], internal_signal[1]);
xor is3(internal_signal[3], data_out[1], internal_signal[2]);

dff dff0(.clk(clk), .d(internal_signal[1]), .q(data_out[0]));
dff dff1(.clk(clk), .d(internal_signal[2]), .q(data_out[1]));
dff dff2(.clk(clk), .d(internal_signal[3]), .q(data_out[2]));

endmodule






