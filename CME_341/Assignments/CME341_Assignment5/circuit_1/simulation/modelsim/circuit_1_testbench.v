`timescale 1 ns / 100 ps
module circuit_1_testbench();
reg clk;
reg [1:0] data_in;
wire [2:0] data_out;

initial
#32000 $stop; 

initial clk = 1'b0;
always
#250 clk = ~clk;

initial
begin
data_in[0] = 1'b0;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b0;
data_in[1] = 1'b1;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b0;
#1000
data_in[0] = 1'b1;
data_in[1] = 1'b1;
end

circuit_1 c1a(.clk(clk), .data_in(data_in), .data_out(data_out));
endmodule

