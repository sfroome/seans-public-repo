/*

module circuit_1 (clk, data_in, data_out);
input clk;
input [1:0] data_in;
output [2:0] data_out;
reg [2:0] data_out;
always @ (posedge clk)
begin
data_out[0] = data_in[0]^data_out[0]^data_out[1];
data_out[1] = data_out[0] & data_out[2];
data_out[2] = data_out[1] | data_in[1];
end
endmodule

*/

// Structural Description
module circuit_1(
input clk, 
input [1:0] data_in,
output reg [2:0] data_out);

wire d0, d1, d2;

xor x1( d0, data_in[0], data_out[0], data_out[1]);
and a1(d1, d0, data_out[2]);
or o1(d2, d1, data_in[1]);

dff dff0(.q(data_out[0]), .d(d0), .clk(clk));
dff dff1(.q(data_out[1]), .d(d1), .clk(clk));
dff dff2(.q(data_out[2]), .d(d2), .clk(clk));

endmodule
