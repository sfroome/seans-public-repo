
module circuit_1n (clk, data_in, data_out);
input clk;
input [1:0] data_in;
output [2:0] data_out;
reg [2:0] data_out;
always @ (posedge clk)
begin
data_out[0] <= data_in[0]^data_out[0]^data_out[1];
data_out[1] <= data_out[0] & data_out[2];
data_out[2] <= data_out[1] | data_in[1];
end
endmodule


/*
module circuit_1n(
input clk,
input [1:0] data_in,
output reg [2:0] data_out);

wire data0, data1, data2;

xor xor1(data0, data_out[0], data_out[1], data_in[0]);
and and1(data1, data_out[0], data_out[2]);
or or1(data2, data_out[1], data_in[1]);

dff dff0(.d(data0), .clk(clk), .q(data_out[0]));
dff dff1(.d(data1), .clk(clk), .q(data_out[1]));
dff dff2(.d(data2), .clk(clk), .q(data_out[2]));

endmodule
*/