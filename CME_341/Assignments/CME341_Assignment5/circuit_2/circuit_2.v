
module circuit_2 (clk, data_in, data_out);
input clk;
input [1:0] data_in;
output [1:0] data_out;
reg [1:0] data_out;
always @ (posedge clk)
begin
case (data_in[1])
1'b0 : data_out = data_in;
1'b1 : data_out = data_out;

endcase
end
endmodule
/*
if(data_in[1] = 1'b0)
	data_out = data_out;
else 
	data_out = data_in;


//Structural Description

module circuit_2(
input clk,
input [1:0] data_in,
output reg [1:0] data_out);

wire [1:0] and1_to_or;
wire [1:0] and2_to_or;
wire not_data_in1;
wire d [1:0];

not notd(not_data_in1, data_in[1]);
and and00(and1_to_or[0], data_out[0], data_in[1]);
and and01(and2_to_or[0], data_in[0], not_data_in1);
or or10(d[0], and1_to_or[0], and2_to_or[0]);

and and10(and1_to_or[1], data_out[1], data_in[1]);
and and11(and2_to_or[1], data_in[1], not_data_in1);
or or11(d[1], and1_to_or[1], and2_to_or[1]);

dff dff1(.d(d[0]), .clk(clk), .q(data_out[0]));
dff dff2(.d(d[1]), .clk(clk), .q(data_out[1]));
endmodule
*/