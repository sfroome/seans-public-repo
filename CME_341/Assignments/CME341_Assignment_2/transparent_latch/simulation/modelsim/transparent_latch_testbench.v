`timescale 1 ns / 100 ps // compiler directive that must be included
// the first time listed is used for the unit
// of time on waveform plots
// the second time listed indicates precision.
// All delays are rounded to the ??precision time??.
// allowable units of time are ms, us, ns, and ps
// allowable numbers are 1, 10, 100

module set_clear_latch_testbench();
// no inputs or outputs
reg D, gate;
wire Q, Q_bar;


initial
#10000 $stop;

initial D=1'b1;
always
#250 D = ~D;

initial
begin
gate = 1'b1;
#850 gate = 1'b0;
#3250 gate = 1'b1; 
#1000 gate = 1'b0;
end


transparent_latch latch(.D(D),
.gate(gate),
.Q(Q),
.Q_bar(Q_bar)
);
endmodule