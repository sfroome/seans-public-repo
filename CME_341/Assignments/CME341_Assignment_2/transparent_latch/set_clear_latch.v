module set_clear_latch(
input set, clear, 
output Q, n_Q); 


nor nor_1(Q, clear, n_Q);
nor nor_2(n_Q, Q, set);

endmodule