`timescale 1 ns / 100 ps // compiler directive that must be included
module set_clear_latch_testbench();
reg set, clear;
wire Q, n_Q;


initial
#5000 $stop;


initial
begin
 set = 1'b1;
 clear = 1'b1;
#1000 clear = 1'b0; 
#1000 set = 1'b0; 
#1000 clear = ~clear;
#500
clear = ~clear;
#500
set = ~set;
#500
set = ~set;
end
set_clear_latch latch(.set(set),
.clear(clear),
.Q(Q),
.n_Q(n_Q)
);
endmodule