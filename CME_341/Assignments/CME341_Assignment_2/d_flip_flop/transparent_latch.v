module transparent_latch (D, gate, Q, Q_bar);
input D, gate; 
output Q, Q_bar; 
wire set, clear, D_bar;
and and_1 (set, gate, D); 
and and_2 (clear, gate, D_bar); 
not not_1 (D_bar, D);
// instantiate set/clear latch 

set_clear_latch sc_latch_1 (.set(set),
.clear(clear), .Q(Q), .n_Q(Q_bar)); 
endmodule

