`timescale 1 ns / 100 ps // compiler directive that must be included
// the first time listed is used for the unit
// of time on waveform plots
// the second time listed indicates precision.
// All delays are rounded to the ??precision time??.
// allowable units of time are ms, us, ns, and ps
// allowable numbers are 1, 10, 100

module set_clear_latch_testbench();
// no inputs or outputs
reg D, clk;
wire Q;


initial
#2000 $stop;

initial D=1'b1;
always
#100 clk = ~clk;

initial
begin
clk = 1'b1;
D = 1'b1;
#300 D = 1'b0;
#120 D = 1'b1; 
#130 D = 1'b0;
#1070 D= 1'b1;
end


d_flip_flop dff1(.D(D),
.clk(clk),
.Q(Q)
);

endmodule