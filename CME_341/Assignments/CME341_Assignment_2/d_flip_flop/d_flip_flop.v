module d_flip_flop( 
input clk, D,
output Q);

wire not_clk; // Aka inverse Clock Signal
wire Q1_to_D; // aka output of Latch 1
//Q1_to_D is also the input of Latch 2

//Instantiation of clock inverter
not not_1 (not_clk, clk);

//Latch with module inputs
transparent_latch t_latch_1(
.D(D),.gate(not_clk),
.Q(Q1_to_D),.Q_bar());

//Latch with module output
transparent_latch t_latch_2(
.D(Q1_to_D),.gate(clk),
.Q(Q),.Q_bar());

endmodule
