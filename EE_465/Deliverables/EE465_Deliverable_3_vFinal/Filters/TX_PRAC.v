/*************************************************************************************************************************
EE 465 Deliverable 3 Practical Pulse Shaping Filter
Sean Froome

Multiplierless output, also takes a 2-bit instead of an 18-bit
input, increasing efficiency.
*************************************************************************************************************************/
`default_nettype none
module TX_PRAC(
input wire sys_clk,
input wire reset,
input wire sam_clk_enable,
input wire sym_clk_enable,
input wire signed [1:0] x_in,
output reg signed [17:0] y_out);

/************************************************************************************************************************
REGISTER DECLARATIONS
************************************************************************************************************************
Multiplier Outputs
************************************************************************************************************************/
(* noprune *) reg signed [17:0] multiplier_out0,   multiplier_out1,  multiplier_out2,  multiplier_out3,  multiplier_out4;
(* noprune *) reg signed [17:0] multiplier_out5,   multiplier_out6,  multiplier_out7,  multiplier_out8;
(* noprune *) reg signed [17:0] multiplier_out9,   multiplier_out10, multiplier_out11, multiplier_out12, multiplier_out13;
(* noprune *) reg signed [17:0] multiplier_out14,  multiplier_out15, multiplier_out16, multiplier_out17, multiplier_out18;
(* noprune *) reg signed [17:0] multiplier_out19,  multiplier_out20, multiplier_out21, multiplier_out22, multiplier_out23;
(* noprune *) reg signed [17:0] multiplier_out24,  multiplier_out25, multiplier_out26, multiplier_out27, multiplier_out28;
(* noprune *) reg signed [17:0] multiplier_out29,  multiplier_out30, multiplier_out31, multiplier_out32, multiplier_out33;
(* noprune *) reg signed [17:0] multiplier_out34,  multiplier_out35, multiplier_out36, multiplier_out37, multiplier_out38;
(* noprune *) reg signed [17:0] multiplier_out39,  multiplier_out40, multiplier_out41, multiplier_out42, multiplier_out43;
(* noprune *) reg signed [17:0] multiplier_out44,  multiplier_out45, multiplier_out46, multiplier_out47, multiplier_out48;
(* noprune *) reg signed [17:0] multiplier_out49,  multiplier_out50, multiplier_out51, multiplier_out52, multiplier_out53;
(* noprune *) reg signed [17:0] multiplier_out54,  multiplier_out55, multiplier_out56, multiplier_out57, multiplier_out58;
(* noprune *) reg signed [17:0] multiplier_out59,  multiplier_out60, multiplier_out61, multiplier_out62, multiplier_out63;
(* noprune *) reg signed [17:0] multiplier_out64,  multiplier_out65, multiplier_out66, multiplier_out67, multiplier_out68;
(* noprune *) reg signed [17:0] multiplier_out69,  multiplier_out70, multiplier_out71, multiplier_out72, multiplier_out73;
(* noprune *) reg signed [17:0] multiplier_out74,  multiplier_out75, multiplier_out76, multiplier_out77, multiplier_out78;
(* noprune *) reg signed [17:0] multiplier_out79,  multiplier_out80, multiplier_out81, multiplier_out82, multiplier_out83;
(* noprune *) reg signed [17:0] multiplier_out84,  multiplier_out85, multiplier_out86, multiplier_out87, multiplier_out88;
(* noprune *) reg signed [17:0] multiplier_out89,  multiplier_out90, multiplier_out91, multiplier_out92, multiplier_out93;
(* noprune *) reg signed [17:0] multiplier_out94,  multiplier_out95, multiplier_out96, multiplier_out97, multiplier_out98;
(* noprune *) reg signed [17:0] multiplier_out99,  multiplier_out100, multiplier_out101, multiplier_out102, multiplier_out103;
(* noprune *) reg signed [17:0] multiplier_out104, multiplier_out105, multiplier_out106, multiplier_out107, multiplier_out108;
(* noprune *) reg signed [17:0] multiplier_out109, multiplier_out110, multiplier_out111, multiplier_out112;
/************************************************************************************************************************
Top Level X-Registers
************************************************************************************************************************/
(* noprune *) reg signed [2:0] clk_and_x_in;

(* noprune *) reg signed [2:0] x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16;
(* noprune *) reg signed [2:0] x17,x18,x19,x20,x21,x22,x23,x24,x25,x26,x27,x28,x29,x30,x31,x32,x33;
(* noprune *) reg signed [2:0] x34,x35,x36,x37,x38,x39,x40,x41,x42,x43,x44,x45,x46,x47,x48,x49,x50;
(* noprune *) reg signed [2:0] x51,x52,x53,x54,x55,x56,x57,x58,x59,x60,x61,x62,x63,x64,x65,x66,x67;
(* noprune *) reg signed [2:0] x68,x69,x70,x71,x72,x73,x74,x75,x76,x77,x78,x79,x80,x81,x82,x83,x84;
(* noprune *) reg signed [2:0] x85,x86,x87,x88,x89,x90,x91,x92,x93,x94,x95,x96,x97,x98,x99,x100,x101;
(* noprune *) reg signed [2:0] x102,x103,x104,x105,x106,x107,x108,x109,x110,x111,x112;
/************************************************************************************************************************
Summing Level 1
************************************************************************************************************************/
(* noprune *) reg signed [17:0] sum_level1_1,  sum_level1_2, sum_level1_3, sum_level1_4,sum_level1_5, sum_level1_6;
(* noprune *) reg signed [17:0] sum_level1_7,  sum_level1_8, sum_level1_9, sum_level1_10, sum_level1_11,   sum_level1_11a, sum_level1_12;
(* noprune *) reg signed [17:0] sum_level1_13,  sum_level1_14, sum_level1_15, sum_level1_16,sum_level1_17, sum_level1_18;
(* noprune *) reg signed [17:0] sum_level1_19,  sum_level1_20, sum_level1_21, sum_level1_22, sum_level1_23, sum_level1_24;
(* noprune *) reg signed [17:0] sum_level1_25,  sum_level1_26, sum_level1_27;
/************************************************************************************************************************
Summing Level 2
************************************************************************************************************************/
(* noprune *) reg signed [17:0] sum_level2_1,  sum_level2_2, sum_level2_3, sum_level2_4,sum_level2_5, sum_level2_6;
(* noprune *) reg signed [17:0] sum_level2_7;
/************************************************************************************************************************
Summing Level 3
************************************************************************************************************************/
(* noprune *) reg signed [17:0] sum_level3_1,  sum_level3_2;
/************************************************************************************************************************
Summing Level 4
************************************************************************************************************************/
(* noprune *) reg signed [17:0] sum_level4_1;
/************************************************************************************************************************
What functions as my Mapper
************************************************************************************************************************/
always @ (posedge sys_clk)
  if(reset)
    clk_and_x_in = 3'd0;
  else if(sam_clk_enable == 1'b1)
    clk_and_x_in = {sym_clk_enable, x_in}; //
  else
    clk_and_x_in = clk_and_x_in;

/*
reg [1:0] counter;

always @ (posedge sys_clk)
  if(reset)
    counter = 2'd0;
  else if (sam_clk_enable == 1'b1)
    counter = 2'd0;
  else
    counter = counter + 2'd1;

always @ (posedge sys_clk)
  if(counter == 2'd2) // Maybe on 1, maybe on 0, not sure what works best.
    clk_and_x_in = {1'b1, x_in};
  else
    clk_and_x_in = {1'b0, x_in};
*/


always @ (posedge sys_clk)
  if(reset == 1'b1)
    x0 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    //x0 <= {x_in[17], x_in[17:1]}; // May not need this...
    x0 <= clk_and_x_in;
  else
    x0 <= x0;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x1 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x1 <= x0;
  else
    x1 <= x1;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x2 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x2 <= x1;
  else
    x2 <= x2;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x3 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x3 <= x2;
  else
    x3 <= x3;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x4 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x4 <= x3;
  else
    x4 <= x4;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x5 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x5 <= x4;
  else
    x5 <= x5;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x6 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x6 <= x5;
  else
    x6 <= x6;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x7 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x7 <= x6;
  else
    x7 <= x7;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x8 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x8 <= x7;
  else
    x8 <= x8;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x9 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x9 <= x8;
  else
    x9 <= x9;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x10 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x10 <= x9;
  else
    x10 <= x10;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x11 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x11 <= x10;
  else
    x11 <= x11;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x12 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x12 <= x11;
  else
    x12 <= x12;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x13 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x13 <= x12;
  else
    x13 <= x13;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x14 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x14 <= x13;
  else
    x14 <= x14;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x15 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x15 <= x14;
  else
    x15 <= x15;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x16 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x16 <= x15;
  else
    x16 <= x16;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x17 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x17 <= x16;
  else
    x17 <= x17;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x18 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x18 <= x17;
  else
    x18 <= x18;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x19 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x19 <= x18;
  else
    x19 <= x19;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x20 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x20 <= x19;
  else
    x20 <= x20;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x21 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x21 <= x20;
  else
    x21 <= x21;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x22 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x22 <= x21;
  else
    x22 <= x22;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x23 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x23 <= x22;
  else
    x23 <= x23;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x24 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x24 <= x23;
  else
    x24 <= x24;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x25 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x25 <= x24;
  else
    x25 <= x25;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x26 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x26 <= x25;
  else
    x26 <= x26;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x27 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x27 <= x26;
  else
    x27 <= x27;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x28 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x28 <= x27;
  else
    x28 <= x28;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x29 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x29 <= x28;
  else
    x29 <= x29;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x30 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x30 <= x29;
  else
    x30 <= x30;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x31 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x31 <= x30;
  else
    x31 <= x31;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x32 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x32 <= x31;
  else
    x32 <= x32;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x33 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x33 <= x32;
  else
    x33 <= x33;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x34 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x34 <= x33;
  else
    x34 <= x34;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x35 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x35 <= x34;
  else
    x35 <= x35;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x36 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x36 <= x35;
  else
    x36 <= x36;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x37 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x37 <= x36;
  else
    x37 <=  x37;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x38 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x38 <= x37;
  else
    x38 <=  x38;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x39 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x39 <= x38;
  else
    x39 <=  x39;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x40 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x40 <= x39;
  else
    x40 <= x40;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x41 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x41 <= x40;
  else
    x41 <= x41;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x42 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x42 <= x41;
  else
    x42 <= x42;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x43 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x43 <= x42;
  else
    x43 <=  x43;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x44 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x44 <= x43;
  else
    x44 <= x44;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x45 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x45 <= x44;
  else
    x45 <=  x45;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x46 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x46 <= x45;
  else
    x46 <=  x46;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x47 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x47 <= x46;
  else
    x47 <= x47;
always @ (posedge sys_clk)
  if(reset == 1'b1)
    x48 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x48 <= x47;
  else
    x48 <= x48;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x49 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x49 <= x48;
  else
    x49 <= x49;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x50 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x50 <= x49;
  else
    x50 <= x50;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x51 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x51 <= x50;
  else
    x51 <=  x51;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x52 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x52 <= x51;
  else
    x52 <= x52;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x53 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x53 <= x52;
  else
    x53 <= x53;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x54 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x54 <= x53;
  else
    x54 <= x54;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x55 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x55 <= x54;
  else
    x55 <= x55;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x56 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x56 <= x55;
  else
    x56 <=  x56;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x57 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x57 <= x56;
  else
    x57 <=  x57;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x58 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x58 <= x57;
  else
    x58 <=  x58;
always @ (posedge sys_clk)
  if(reset == 1'b1)
    x59 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x59 <= x58;
  else
    x59 <=  x59;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x60 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x60 <= x59;
  else
    x60 <=  x60;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x61 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x61 <= x60;
  else
    x61 <=  x61;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x62 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x62 <= x61;
  else
    x62 <=  x62;
always @ (posedge sys_clk)
  if(reset == 1'b1)
    x63 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x63 <= x62;
  else
    x63 <=  x63;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x64 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x64 <= x63;
  else
    x64 <=  x64;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x65 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x65 <= x64;
  else
    x65 <=  x65;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x66 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x66 <= x65;
  else
    x66 <=  x66;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x67 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x67 <= x66;
  else
    x67 <= x67;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x68 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x68 <= x67;
  else
    x68 <=  x68;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x69 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x69 <= x68;
  else
    x69 <=  x69;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x70 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x70 <= x69;
  else
    x70 <=  x70;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x71 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x71 <= x70;
  else
    x71 <=  x71;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x72 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x72 <= x71;
  else
    x72 <=  x72;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x73 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x73 <= x72;
  else
    x73 <=  x73;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x74 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x74 <= x73;
  else
    x74 <=  x74;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x75 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x75 <= x74;
  else
    x75 <=  x75;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x76 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x76 <= x75;
  else
    x76 <=  x76;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x77 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x77 <= x76;
  else
    x77 <=  x77;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x78 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x78 <= x77;
  else
    x78 <=  x78;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x79 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x79 <= x78;
  else
    x79 <=  x79;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x80 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x80 <= x79;
  else
    x80 <=  x80;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x81 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x81 <= x80;
  else
    x81 <=  x81;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x82 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x82 <= x81;
  else
    x82 <=  x82;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x83 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x83 <= x82;
  else
    x83 <=  x83;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x84 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x84 <= x83;
  else
    x84 <=  x84;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x85 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x85 <= x84;
  else
    x85 <=  x85;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x86 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x86 <= x85;
  else
    x86 <=  x86;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x87 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x87 <= x86;
  else
    x87 <=  x87;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x88 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x88 <= x87;
  else
    x88 <=  x88;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x89 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x89 <= x88;
  else
    x89 <=  x89;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x90 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x90 <= x89;
  else
    x90 <=  x90;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x91 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x91 <= x90;
  else
    x91 <=  x91;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x92 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x92 <= x91;
  else
    x92 <=  x92;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x93 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x93 <= x92;
  else
    x93 <=  x93;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x94 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x94 <= x93;
  else
    x94 <=  x94;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x95 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x95 <= x94;
  else
    x95 <=  x95;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x96 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x96 <= x95;
  else
    x96 <=  x96;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x97 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x97 <= x96;
  else
    x97 <=  x97;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x98 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x98 <= x97;
  else
    x98 <=  x98;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x99 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x99 <= x98;
  else
    x99 <=  x99;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x100 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x100 <= x99;
  else
    x100 <=  x100;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x101 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x101 <= x100;
  else
    x101 <=  x101;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x102 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x102 <= x101;
  else
    x102 <=  x102;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x103 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x103 <= x102;
  else
    x103 <=  x103;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x104 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x104 <= x103;
  else
    x104 <=  x104;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x105 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x105 <= x104;
  else
    x105 <=  x105;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x106 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x106 <= x105;
  else
    x106 <=  x106;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x107 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x107 <= x106;
  else
    x107 <=  x107;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x108 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x108 <= x107;
  else
    x108 <= x108;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x109 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x109 <= x108;
  else
    x109 <=  x109;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x110 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x110 <= x109;
  else
    x110 <=  x110;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x111 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x111 <= x110;
  else
    x111 <= x111;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    x112 <= 3'd0;
  else if(sam_clk_enable == 1'b1)
    x112 <= x111;
  else
    x112 <= x112;

/*************************************************************************************************************************
Crazy Long LUT
*************************************************************************************************************************/
always @ (posedge sys_clk)
case(x0)
 3'b000: multiplier_out0 = 18'sd0;
 3'b001: multiplier_out0 = 18'sd0;
 3'b011: multiplier_out0 = 18'sd0;
 3'b010: multiplier_out0 = 18'sd0;
 3'b100: multiplier_out0 = -18'sd49;
 3'b101: multiplier_out0 = -18'sd16;
 3'b111: multiplier_out0 = 18'sd16;
 3'b110: multiplier_out0 = 18'sd49;
 default: multiplier_out0 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x1)
 3'b000: multiplier_out1 = 18'sd0;
 3'b001: multiplier_out1 = 18'sd0;
 3'b011: multiplier_out1 = 18'sd0;
 3'b010: multiplier_out1 = 18'sd0;
 3'b100: multiplier_out1 = -18'sd42;
 3'b101: multiplier_out1 = -18'sd14;
 3'b111: multiplier_out1 = 18'sd14;
 3'b110: multiplier_out1 = 18'sd42;
 default: multiplier_out1 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x2)
 3'b000: multiplier_out2 = 18'sd0;
 3'b001: multiplier_out2 = 18'sd0;
 3'b011: multiplier_out2 = 18'sd0;
 3'b010: multiplier_out2 = 18'sd0;
 3'b100: multiplier_out2 = -18'sd2;
 3'b101: multiplier_out2 = -18'sd1;
 3'b111: multiplier_out2 = 18'sd1;
 3'b110: multiplier_out2 = 18'sd2;
 default: multiplier_out2 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x3)
 3'b000: multiplier_out3 = 18'sd0;
 3'b001: multiplier_out3 = 18'sd0;
 3'b011: multiplier_out3 = 18'sd0;
 3'b010: multiplier_out3 = 18'sd0;
 3'b100: multiplier_out3 = 18'sd50;
 3'b101: multiplier_out3 = 18'sd17;
 3'b111: multiplier_out3 = -18'sd17;
 3'b110: multiplier_out3 = -18'sd50;
 default: multiplier_out3 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x4)
 3'b000: multiplier_out4 = 18'sd0;
 3'b001: multiplier_out4 = 18'sd0;
 3'b011: multiplier_out4 = 18'sd0;
 3'b010: multiplier_out4 = 18'sd0;
 3'b100: multiplier_out4 = 18'sd74;
 3'b101: multiplier_out4 = 18'sd25;
 3'b111: multiplier_out4 = -18'sd25;
 3'b110: multiplier_out4 = -18'sd74;
 default: multiplier_out4 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x5)
 3'b000: multiplier_out5 = 18'sd0;
 3'b001: multiplier_out5 = 18'sd0;
 3'b011: multiplier_out5 = 18'sd0;
 3'b010: multiplier_out5 = 18'sd0;
 3'b100: multiplier_out5 = 18'sd47;
 3'b101: multiplier_out5 = 18'sd16;
 3'b111: multiplier_out5 = -18'sd16;
 3'b110: multiplier_out5 = -18'sd47;
 default: multiplier_out5 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x6)
 3'b000: multiplier_out6 = 18'sd0;
 3'b001: multiplier_out6 = 18'sd0;
 3'b011: multiplier_out6 = 18'sd0;
 3'b010: multiplier_out6 = 18'sd0;
 3'b100: multiplier_out6 = -18'sd22;
 3'b101: multiplier_out6 = -18'sd7;
 3'b111: multiplier_out6 = 18'sd7;
 3'b110: multiplier_out6 = 18'sd22;
 default: multiplier_out6 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x7)
 3'b000: multiplier_out7 = 18'sd0;
 3'b001: multiplier_out7 = 18'sd0;
 3'b011: multiplier_out7 = 18'sd0;
 3'b010: multiplier_out7 = 18'sd0;
 3'b100: multiplier_out7 = -18'sd89;
 3'b101: multiplier_out7 = -18'sd30;
 3'b111: multiplier_out7 = 18'sd30;
 3'b110: multiplier_out7 = 18'sd89;
 default: multiplier_out7 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x8)
 3'b000: multiplier_out8 = 18'sd0;
 3'b001: multiplier_out8 = 18'sd0;
 3'b011: multiplier_out8 = 18'sd0;
 3'b010: multiplier_out8 = 18'sd0;
 3'b100: multiplier_out8 = -18'sd104;
 3'b101: multiplier_out8 = -18'sd35;
 3'b111: multiplier_out8 = 18'sd35;
 3'b110: multiplier_out8 = 18'sd104;
 default: multiplier_out8 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x9)
 3'b000: multiplier_out9 = 18'sd0;
 3'b001: multiplier_out9 = 18'sd0;
 3'b011: multiplier_out9 = 18'sd0;
 3'b010: multiplier_out9 = 18'sd0;
 3'b100: multiplier_out9 = -18'sd45;
 3'b101: multiplier_out9 = -18'sd15;
 3'b111: multiplier_out9 = 18'sd15;
 3'b110: multiplier_out9 = 18'sd45;
 default: multiplier_out9 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x10)
 3'b000: multiplier_out10 = 18'sd0;
 3'b001: multiplier_out10 = 18'sd0;
 3'b011: multiplier_out10 = 18'sd0;
 3'b010: multiplier_out10 = 18'sd0;
 3'b100: multiplier_out10 = 18'sd60;
 3'b101: multiplier_out10 = 18'sd20;
 3'b111: multiplier_out10 = -18'sd20;
 3'b110: multiplier_out10 = -18'sd60;
 default: multiplier_out10 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x11)
 3'b000: multiplier_out11 = 18'sd0;
 3'b001: multiplier_out11 = 18'sd0;
 3'b011: multiplier_out11 = 18'sd0;
 3'b010: multiplier_out11 = 18'sd0;
 3'b100: multiplier_out11 = 18'sd145;
 3'b101: multiplier_out11 = 18'sd48;
 3'b111: multiplier_out11 = -18'sd48;
 3'b110: multiplier_out11 = -18'sd145;
 default: multiplier_out11 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x12)
 3'b000: multiplier_out12 = 18'sd0;
 3'b001: multiplier_out12 = 18'sd0;
 3'b011: multiplier_out12 = 18'sd0;
 3'b010: multiplier_out12 = 18'sd0;
 3'b100: multiplier_out12 = 18'sd141;
 3'b101: multiplier_out12 = 18'sd47;
 3'b111: multiplier_out12 = -18'sd47;
 3'b110: multiplier_out12 = -18'sd141;
 default: multiplier_out12 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x13)
 3'b000: multiplier_out13 = 18'sd0;
 3'b001: multiplier_out13 = 18'sd0;
 3'b011: multiplier_out13 = 18'sd0;
 3'b010: multiplier_out13 = 18'sd0;
 3'b100: multiplier_out13 = 18'sd35;
 3'b101: multiplier_out13 = 18'sd12;
 3'b111: multiplier_out13 = -18'sd12;
 3'b110: multiplier_out13 = -18'sd35;
 default: multiplier_out13 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x14)
 3'b000: multiplier_out14 = 18'sd0;
 3'b001: multiplier_out14 = 18'sd0;
 3'b011: multiplier_out14 = 18'sd0;
 3'b010: multiplier_out14 = 18'sd0;
 3'b100: multiplier_out14 = -18'sd118;
 3'b101: multiplier_out14 = -18'sd39;
 3'b111: multiplier_out14 = 18'sd39;
 3'b110: multiplier_out14 = 18'sd118;
 default: multiplier_out14 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x15)
 3'b000: multiplier_out15 = 18'sd0;
 3'b001: multiplier_out15 = 18'sd0;
 3'b011: multiplier_out15 = 18'sd0;
 3'b010: multiplier_out15 = 18'sd0;
 3'b100: multiplier_out15 = -18'sd218;
 3'b101: multiplier_out15 = -18'sd73;
 3'b111: multiplier_out15 = 18'sd73;
 3'b110: multiplier_out15 = 18'sd218;
 default: multiplier_out15 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x16)
 3'b000: multiplier_out16 = 18'sd0;
 3'b001: multiplier_out16 = 18'sd0;
 3'b011: multiplier_out16 = 18'sd0;
 3'b010: multiplier_out16 = 18'sd0;
 3'b100: multiplier_out16 = -18'sd183;
 3'b101: multiplier_out16 = -18'sd61;
 3'b111: multiplier_out16 = 18'sd61;
 3'b110: multiplier_out16 = 18'sd183;
 default: multiplier_out16 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x17)
 3'b000: multiplier_out17 = 18'sd0;
 3'b001: multiplier_out17 = 18'sd0;
 3'b011: multiplier_out17 = 18'sd0;
 3'b010: multiplier_out17 = 18'sd0;
 3'b100: multiplier_out17 = -18'sd13;
 3'b101: multiplier_out17 = -18'sd4;
 3'b111: multiplier_out17 = 18'sd4;
 3'b110: multiplier_out17 = 18'sd13;
 default: multiplier_out17 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x18)
 3'b000: multiplier_out18 = 18'sd0;
 3'b001: multiplier_out18 = 18'sd0;
 3'b011: multiplier_out18 = 18'sd0;
 3'b010: multiplier_out18 = 18'sd0;
 3'b100: multiplier_out18 = 18'sd199;
 3'b101: multiplier_out18 = 18'sd66;
 3'b111: multiplier_out18 = -18'sd66;
 3'b110: multiplier_out18 = -18'sd199;
 default: multiplier_out18 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x19)
 3'b000: multiplier_out19 = 18'sd0;
 3'b001: multiplier_out19 = 18'sd0;
 3'b011: multiplier_out19 = 18'sd0;
 3'b010: multiplier_out19 = 18'sd0;
 3'b100: multiplier_out19 = 18'sd311;
 3'b101: multiplier_out19 = 18'sd104;
 3'b111: multiplier_out19 = -18'sd104;
 3'b110: multiplier_out19 = -18'sd311;
 default: multiplier_out19 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x20)
 3'b000: multiplier_out20 = 18'sd0;
 3'b001: multiplier_out20 = 18'sd0;
 3'b011: multiplier_out20 = 18'sd0;
 3'b010: multiplier_out20 = 18'sd0;
 3'b100: multiplier_out20 = 18'sd228;
 3'b101: multiplier_out20 = 18'sd76;
 3'b111: multiplier_out20 = -18'sd76;
 3'b110: multiplier_out20 = -18'sd228;
 default: multiplier_out20 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x21)
 3'b000: multiplier_out21 = 18'sd0;
 3'b001: multiplier_out21 = 18'sd0;
 3'b011: multiplier_out21 = 18'sd0;
 3'b010: multiplier_out21 = 18'sd0;
 3'b100: multiplier_out21 = -18'sd28;
 3'b101: multiplier_out21 = -18'sd9;
 3'b111: multiplier_out21 = 18'sd9;
 3'b110: multiplier_out21 = 18'sd28;
 default: multiplier_out21 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x22)
 3'b000: multiplier_out22 = 18'sd0;
 3'b001: multiplier_out22 = 18'sd0;
 3'b011: multiplier_out22 = 18'sd0;
 3'b010: multiplier_out22 = 18'sd0;
 3'b100: multiplier_out22 = -18'sd310;
 3'b101: multiplier_out22 = -18'sd103;
 3'b111: multiplier_out22 = 18'sd103;
 3'b110: multiplier_out22 = 18'sd310;
 default: multiplier_out22 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x23)
 3'b000: multiplier_out23 = 18'sd0;
 3'b001: multiplier_out23 = 18'sd0;
 3'b011: multiplier_out23 = 18'sd0;
 3'b010: multiplier_out23 = 18'sd0;
 3'b100: multiplier_out23 = -18'sd430;
 3'b101: multiplier_out23 = -18'sd143;
 3'b111: multiplier_out23 = 18'sd143;
 3'b110: multiplier_out23 = 18'sd430;
 default: multiplier_out23 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x24)
 3'b000: multiplier_out24 = 18'sd0;
 3'b001: multiplier_out24 = 18'sd0;
 3'b011: multiplier_out24 = 18'sd0;
 3'b010: multiplier_out24 = 18'sd0;
 3'b100: multiplier_out24 = -18'sd276;
 3'b101: multiplier_out24 = -18'sd92;
 3'b111: multiplier_out24 = 18'sd92;
 3'b110: multiplier_out24 = 18'sd276;
 default: multiplier_out24 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x25)
 3'b000: multiplier_out25 = 18'sd0;
 3'b001: multiplier_out25 = 18'sd0;
 3'b011: multiplier_out25 = 18'sd0;
 3'b010: multiplier_out25 = 18'sd0;
 3'b100: multiplier_out25 = 18'sd92;
 3'b101: multiplier_out25 = 18'sd31;
 3'b111: multiplier_out25 = -18'sd31;
 3'b110: multiplier_out25 = -18'sd92;
 default: multiplier_out25 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x26)
 3'b000: multiplier_out26 = 18'sd0;
 3'b001: multiplier_out26 = 18'sd0;
 3'b011: multiplier_out26 = 18'sd0;
 3'b010: multiplier_out26 = 18'sd0;
 3'b100: multiplier_out26 = 18'sd461;
 3'b101: multiplier_out26 = 18'sd154;
 3'b111: multiplier_out26 = -18'sd154;
 3'b110: multiplier_out26 = -18'sd461;
 default: multiplier_out26 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x27)
 3'b000: multiplier_out27 = 18'sd0;
 3'b001: multiplier_out27 = 18'sd0;
 3'b011: multiplier_out27 = 18'sd0;
 3'b010: multiplier_out27 = 18'sd0;
 3'b100: multiplier_out27 = 18'sd578;
 3'b101: multiplier_out27 = 18'sd193;
 3'b111: multiplier_out27 = -18'sd193;
 3'b110: multiplier_out27 = -18'sd578;
 default: multiplier_out27 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x28)
 3'b000: multiplier_out28 = 18'sd0;
 3'b001: multiplier_out28 = 18'sd0;
 3'b011: multiplier_out28 = 18'sd0;
 3'b010: multiplier_out28 = 18'sd0;
 3'b100: multiplier_out28 = 18'sd325;
 3'b101: multiplier_out28 = 18'sd108;
 3'b111: multiplier_out28 = -18'sd108;
 3'b110: multiplier_out28 = -18'sd325;
 default: multiplier_out28 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x29)
 3'b000: multiplier_out29 = 18'sd0;
 3'b001: multiplier_out29 = 18'sd0;
 3'b011: multiplier_out29 = 18'sd0;
 3'b010: multiplier_out29 = 18'sd0;
 3'b100: multiplier_out29 = -18'sd190;
 3'b101: multiplier_out29 = -18'sd63;
 3'b111: multiplier_out29 = 18'sd63;
 3'b110: multiplier_out29 = 18'sd190;
 default: multiplier_out29 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x30)
 3'b000: multiplier_out30 = 18'sd0;
 3'b001: multiplier_out30 = 18'sd0;
 3'b011: multiplier_out30 = 18'sd0;
 3'b010: multiplier_out30 = 18'sd0;
 3'b100: multiplier_out30 = -18'sd662;
 3'b101: multiplier_out30 = -18'sd221;
 3'b111: multiplier_out30 = 18'sd221;
 3'b110: multiplier_out30 = 18'sd662;
 default: multiplier_out30 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x31)
 3'b000: multiplier_out31 = 18'sd0;
 3'b001: multiplier_out31 = 18'sd0;
 3'b011: multiplier_out31 = 18'sd0;
 3'b010: multiplier_out31 = 18'sd0;
 3'b100: multiplier_out31 = -18'sd767;
 3'b101: multiplier_out31 = -18'sd256;
 3'b111: multiplier_out31 = 18'sd256;
 3'b110: multiplier_out31 = 18'sd767;
 default: multiplier_out31 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x32)
 3'b000: multiplier_out32 = 18'sd0;
 3'b001: multiplier_out32 = 18'sd0;
 3'b011: multiplier_out32 = 18'sd0;
 3'b010: multiplier_out32 = 18'sd0;
 3'b100: multiplier_out32 = -18'sd373;
 3'b101: multiplier_out32 = -18'sd124;
 3'b111: multiplier_out32 = 18'sd124;
 3'b110: multiplier_out32 = 18'sd373;
 default: multiplier_out32 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x33)
 3'b000: multiplier_out33 = 18'sd0;
 3'b001: multiplier_out33 = 18'sd0;
 3'b011: multiplier_out33 = 18'sd0;
 3'b010: multiplier_out33 = 18'sd0;
 3'b100: multiplier_out33 = 18'sd335;
 3'b101: multiplier_out33 = 18'sd112;
 3'b111: multiplier_out33 = -18'sd112;
 3'b110: multiplier_out33 = -18'sd335;
 default: multiplier_out33 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x34)
 3'b000: multiplier_out34 = 18'sd0;
 3'b001: multiplier_out34 = 18'sd0;
 3'b011: multiplier_out34 = 18'sd0;
 3'b010: multiplier_out34 = 18'sd0;
 3'b100: multiplier_out34 = 18'sd935;
 3'b101: multiplier_out34 = 18'sd312;
 3'b111: multiplier_out34 = -18'sd312;
 3'b110: multiplier_out34 = -18'sd935;
 default: multiplier_out34 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x35)
 3'b000: multiplier_out35 = 18'sd0;
 3'b001: multiplier_out35 = 18'sd0;
 3'b011: multiplier_out35 = 18'sd0;
 3'b010: multiplier_out35 = 18'sd0;
 3'b100: multiplier_out35 = 18'sd1009;
 3'b101: multiplier_out35 = 18'sd336;
 3'b111: multiplier_out35 = -18'sd336;
 3'b110: multiplier_out35 = -18'sd1009;
 default: multiplier_out35 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x36)
 3'b000: multiplier_out36 = 18'sd0;
 3'b001: multiplier_out36 = 18'sd0;
 3'b011: multiplier_out36 = 18'sd0;
 3'b010: multiplier_out36 = 18'sd0;
 3'b100: multiplier_out36 = 18'sd418;
 3'b101: multiplier_out36 = 18'sd139;
 3'b111: multiplier_out36 = -18'sd139;
 3'b110: multiplier_out36 = -18'sd418;
 default: multiplier_out36 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x37)
 3'b000: multiplier_out37 = 18'sd0;
 3'b001: multiplier_out37 = 18'sd0;
 3'b011: multiplier_out37 = 18'sd0;
 3'b010: multiplier_out37 = 18'sd0;
 3'b100: multiplier_out37 = -18'sd553;
 3'b101: multiplier_out37 = -18'sd184;
 3'b111: multiplier_out37 = 18'sd184;
 3'b110: multiplier_out37 = 18'sd553;
 default: multiplier_out37 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x38)
 3'b000: multiplier_out38 = 18'sd0;
 3'b001: multiplier_out38 = 18'sd0;
 3'b011: multiplier_out38 = 18'sd0;
 3'b010: multiplier_out38 = 18'sd0;
 3'b100: multiplier_out38 = -18'sd1319;
 3'b101: multiplier_out38 = -18'sd440;
 3'b111: multiplier_out38 = 18'sd440;
 3'b110: multiplier_out38 = 18'sd1319;
 default: multiplier_out38 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x39)
 3'b000: multiplier_out39 = 18'sd0;
 3'b001: multiplier_out39 = 18'sd0;
 3'b011: multiplier_out39 = 18'sd0;
 3'b010: multiplier_out39 = 18'sd0;
 3'b100: multiplier_out39 = -18'sd1336;
 3'b101: multiplier_out39 = -18'sd445;
 3'b111: multiplier_out39 = 18'sd445;
 3'b110: multiplier_out39 = 18'sd1336;
 default: multiplier_out39 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x40)
 3'b000: multiplier_out40 = 18'sd0;
 3'b001: multiplier_out40 = 18'sd0;
 3'b011: multiplier_out40 = 18'sd0;
 3'b010: multiplier_out40 = 18'sd0;
 3'b100: multiplier_out40 = -18'sd458;
 3'b101: multiplier_out40 = -18'sd153;
 3'b111: multiplier_out40 = 18'sd153;
 3'b110: multiplier_out40 = 18'sd458;
 default: multiplier_out40 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x41)
 3'b000: multiplier_out41 = 18'sd0;
 3'b001: multiplier_out41 = 18'sd0;
 3'b011: multiplier_out41 = 18'sd0;
 3'b010: multiplier_out41 = 18'sd0;
 3'b100: multiplier_out41 = 18'sd890;
 3'b101: multiplier_out41 = 18'sd297;
 3'b111: multiplier_out41 = -18'sd297;
 3'b110: multiplier_out41 = -18'sd890;
 default: multiplier_out41 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x42)
 3'b000: multiplier_out42 = 18'sd0;
 3'b001: multiplier_out42 = 18'sd0;
 3'b011: multiplier_out42 = 18'sd0;
 3'b010: multiplier_out42 = 18'sd0;
 3'b100: multiplier_out42 = 18'sd1896;
 3'b101: multiplier_out42 = 18'sd632;
 3'b111: multiplier_out42 = -18'sd632;
 3'b110: multiplier_out42 = -18'sd1896;
 default: multiplier_out42 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x43)
 3'b000: multiplier_out43 = 18'sd0;
 3'b001: multiplier_out43 = 18'sd0;
 3'b011: multiplier_out43 = 18'sd0;
 3'b010: multiplier_out43 = 18'sd0;
 3'b100: multiplier_out43 = 18'sd1820;
 3'b101: multiplier_out43 = 18'sd607;
 3'b111: multiplier_out43 = -18'sd607;
 3'b110: multiplier_out43 = -18'sd1820;
 default: multiplier_out43 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x44)
 3'b000: multiplier_out44 = 18'sd0;
 3'b001: multiplier_out44 = 18'sd0;
 3'b011: multiplier_out44 = 18'sd0;
 3'b010: multiplier_out44 = 18'sd0;
 3'b100: multiplier_out44 = 18'sd491;
 3'b101: multiplier_out44 = 18'sd164;
 3'b111: multiplier_out44 = -18'sd164;
 3'b110: multiplier_out44 = -18'sd491;
 default: multiplier_out44 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x45)
 3'b000: multiplier_out45 = 18'sd0;
 3'b001: multiplier_out45 = 18'sd0;
 3'b011: multiplier_out45 = 18'sd0;
 3'b010: multiplier_out45 = 18'sd0;
 3'b100: multiplier_out45 = -18'sd1466;
 3'b101: multiplier_out45 = -18'sd489;
 3'b111: multiplier_out45 = 18'sd489;
 3'b110: multiplier_out45 = 18'sd1466;
 default: multiplier_out45 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x46)
 3'b000: multiplier_out46 = 18'sd0;
 3'b001: multiplier_out46 = 18'sd0;
 3'b011: multiplier_out46 = 18'sd0;
 3'b010: multiplier_out46 = 18'sd0;
 3'b100: multiplier_out46 = -18'sd2879;
 3'b101: multiplier_out46 = -18'sd960;
 3'b111: multiplier_out46 = 18'sd960;
 3'b110: multiplier_out46 = 18'sd2879;
 default: multiplier_out46 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x47)
 3'b000: multiplier_out47 = 18'sd0;
 3'b001: multiplier_out47 = 18'sd0;
 3'b011: multiplier_out47 = 18'sd0;
 3'b010: multiplier_out47 = 18'sd0;
 3'b100: multiplier_out47 = -18'sd2660;
 3'b101: multiplier_out47 = -18'sd887;
 3'b111: multiplier_out47 = 18'sd887;
 3'b110: multiplier_out47 = 18'sd2660;
 default: multiplier_out47 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x48)
 3'b000: multiplier_out48 = 18'sd0;
 3'b001: multiplier_out48 = 18'sd0;
 3'b011: multiplier_out48 = 18'sd0;
 3'b010: multiplier_out48 = 18'sd0;
 3'b100: multiplier_out48 = -18'sd517;
 3'b101: multiplier_out48 = -18'sd172;
 3'b111: multiplier_out48 = 18'sd172;
 3'b110: multiplier_out48 = 18'sd517;
 default: multiplier_out48 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x49)
 3'b000: multiplier_out49 = 18'sd0;
 3'b001: multiplier_out49 = 18'sd0;
 3'b011: multiplier_out49 = 18'sd0;
 3'b010: multiplier_out49 = 18'sd0;
 3'b100: multiplier_out49 = 18'sd2666;
 3'b101: multiplier_out49 = 18'sd889;
 3'b111: multiplier_out49 = -18'sd889;
 3'b110: multiplier_out49 = -18'sd2666;
 default: multiplier_out49 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x50)
 3'b000: multiplier_out50 = 18'sd0;
 3'b001: multiplier_out50 = 18'sd0;
 3'b011: multiplier_out50 = 18'sd0;
 3'b010: multiplier_out50 = 18'sd0;
 3'b100: multiplier_out50 = 18'sd5063;
 3'b101: multiplier_out50 = 18'sd1688;
 3'b111: multiplier_out50 = -18'sd1688;
 3'b110: multiplier_out50 = -18'sd5063;
 default: multiplier_out50 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x51)
 3'b000: multiplier_out51 = 18'sd0;
 3'b001: multiplier_out51 = 18'sd0;
 3'b011: multiplier_out51 = 18'sd0;
 3'b010: multiplier_out51 = 18'sd0;
 3'b100: multiplier_out51 = 18'sd4709;
 3'b101: multiplier_out51 = 18'sd1570;
 3'b111: multiplier_out51 = -18'sd1570;
 3'b110: multiplier_out51 = -18'sd4709;
 default: multiplier_out51 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x52)
 3'b000: multiplier_out52 = 18'sd0;
 3'b001: multiplier_out52 = 18'sd0;
 3'b011: multiplier_out52 = 18'sd0;
 3'b010: multiplier_out52 = 18'sd0;
 3'b100: multiplier_out52 = 18'sd532;
 3'b101: multiplier_out52 = 18'sd177;
 3'b111: multiplier_out52 = -18'sd177;
 3'b110: multiplier_out52 = -18'sd532;
 default: multiplier_out52 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x53)
 3'b000: multiplier_out53 = 18'sd0;
 3'b001: multiplier_out53 = 18'sd0;
 3'b011: multiplier_out53 = 18'sd0;
 3'b010: multiplier_out53 = 18'sd0;
 3'b100: multiplier_out53 = -18'sd6943;
 3'b101: multiplier_out53 = -18'sd2314;
 3'b111: multiplier_out53 = 18'sd2314;
 3'b110: multiplier_out53 = 18'sd6943;
 default: multiplier_out53 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x54)
 3'b000: multiplier_out54 = 18'sd0;
 3'b001: multiplier_out54 = 18'sd0;
 3'b011: multiplier_out54 = 18'sd0;
 3'b010: multiplier_out54 = 18'sd0;
 3'b100: multiplier_out54 = -18'sd15594;
 3'b101: multiplier_out54 = -18'sd5198;
 3'b111: multiplier_out54 = 18'sd5198;
 3'b110: multiplier_out54 = 18'sd15594;
 default: multiplier_out54 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x55)
 3'b000: multiplier_out55 = 18'sd0;
 3'b001: multiplier_out55 = 18'sd0;
 3'b011: multiplier_out55 = 18'sd0;
 3'b010: multiplier_out55 = 18'sd0;
 3'b100: multiplier_out55 = -18'sd22487;
 3'b101: multiplier_out55 = -18'sd7496;
 3'b111: multiplier_out55 = 18'sd7496;
 3'b110: multiplier_out55 = 18'sd22487;
 default: multiplier_out55 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x56)
 3'b000: multiplier_out56 = 18'sd0;
 3'b001: multiplier_out56 = 18'sd0;
 3'b011: multiplier_out56 = 18'sd0;
 3'b010: multiplier_out56 = 18'sd0;
 3'b100: multiplier_out56 = -18'sd25113;
 3'b101: multiplier_out56 = -18'sd8371;
 3'b111: multiplier_out56 = 18'sd8371;
 3'b110: multiplier_out56 = 18'sd25113;
 default: multiplier_out56 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x57)
 3'b000: multiplier_out57 = 18'sd0;
 3'b001: multiplier_out57 = 18'sd0;
 3'b011: multiplier_out57 = 18'sd0;
 3'b010: multiplier_out57 = 18'sd0;
 3'b100: multiplier_out57 = -18'sd22487;
 3'b101: multiplier_out57 = -18'sd7496;
 3'b111: multiplier_out57 = 18'sd7496;
 3'b110: multiplier_out57 = 18'sd22487;
 default: multiplier_out57 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x58)
 3'b000: multiplier_out58 = 18'sd0;
 3'b001: multiplier_out58 = 18'sd0;
 3'b011: multiplier_out58 = 18'sd0;
 3'b010: multiplier_out58 = 18'sd0;
 3'b100: multiplier_out58 = -18'sd15594;
 3'b101: multiplier_out58 = -18'sd5198;
 3'b111: multiplier_out58 = 18'sd5198;
 3'b110: multiplier_out58 = 18'sd15594;
 default: multiplier_out58 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x59)
 3'b000: multiplier_out59 = 18'sd0;
 3'b001: multiplier_out59 = 18'sd0;
 3'b011: multiplier_out59 = 18'sd0;
 3'b010: multiplier_out59 = 18'sd0;
 3'b100: multiplier_out59 = -18'sd6943;
 3'b101: multiplier_out59 = -18'sd2314;
 3'b111: multiplier_out59 = 18'sd2314;
 3'b110: multiplier_out59 = 18'sd6943;
 default: multiplier_out59 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x60)
 3'b000: multiplier_out60 = 18'sd0;
 3'b001: multiplier_out60 = 18'sd0;
 3'b011: multiplier_out60 = 18'sd0;
 3'b010: multiplier_out60 = 18'sd0;
 3'b100: multiplier_out60 = 18'sd532;
 3'b101: multiplier_out60 = 18'sd177;
 3'b111: multiplier_out60 = -18'sd177;
 3'b110: multiplier_out60 = -18'sd532;
 default: multiplier_out60 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x61)
 3'b000: multiplier_out61 = 18'sd0;
 3'b001: multiplier_out61 = 18'sd0;
 3'b011: multiplier_out61 = 18'sd0;
 3'b010: multiplier_out61 = 18'sd0;
 3'b100: multiplier_out61 = 18'sd4709;
 3'b101: multiplier_out61 = 18'sd1570;
 3'b111: multiplier_out61 = -18'sd1570;
 3'b110: multiplier_out61 = -18'sd4709;
 default: multiplier_out61 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x62)
 3'b000: multiplier_out62 = 18'sd0;
 3'b001: multiplier_out62 = 18'sd0;
 3'b011: multiplier_out62 = 18'sd0;
 3'b010: multiplier_out62 = 18'sd0;
 3'b100: multiplier_out62 = 18'sd5063;
 3'b101: multiplier_out62 = 18'sd1688;
 3'b111: multiplier_out62 = -18'sd1688;
 3'b110: multiplier_out62 = -18'sd5063;
 default: multiplier_out62 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x63)
 3'b000: multiplier_out63 = 18'sd0;
 3'b001: multiplier_out63 = 18'sd0;
 3'b011: multiplier_out63 = 18'sd0;
 3'b010: multiplier_out63 = 18'sd0;
 3'b100: multiplier_out63 = 18'sd2666;
 3'b101: multiplier_out63 = 18'sd889;
 3'b111: multiplier_out63 = -18'sd889;
 3'b110: multiplier_out63 = -18'sd2666;
 default: multiplier_out63 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x64)
 3'b000: multiplier_out64 = 18'sd0;
 3'b001: multiplier_out64 = 18'sd0;
 3'b011: multiplier_out64 = 18'sd0;
 3'b010: multiplier_out64 = 18'sd0;
 3'b100: multiplier_out64 = -18'sd517;
 3'b101: multiplier_out64 = -18'sd172;
 3'b111: multiplier_out64 = 18'sd172;
 3'b110: multiplier_out64 = 18'sd517;
 default: multiplier_out64 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x65)
 3'b000: multiplier_out65 = 18'sd0;
 3'b001: multiplier_out65 = 18'sd0;
 3'b011: multiplier_out65 = 18'sd0;
 3'b010: multiplier_out65 = 18'sd0;
 3'b100: multiplier_out65 = -18'sd2660;
 3'b101: multiplier_out65 = -18'sd887;
 3'b111: multiplier_out65 = 18'sd887;
 3'b110: multiplier_out65 = 18'sd2660;
 default: multiplier_out65 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x66)
 3'b000: multiplier_out66 = 18'sd0;
 3'b001: multiplier_out66 = 18'sd0;
 3'b011: multiplier_out66 = 18'sd0;
 3'b010: multiplier_out66 = 18'sd0;
 3'b100: multiplier_out66 = -18'sd2879;
 3'b101: multiplier_out66 = -18'sd960;
 3'b111: multiplier_out66 = 18'sd960;
 3'b110: multiplier_out66 = 18'sd2879;
 default: multiplier_out66 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x67)
 3'b000: multiplier_out67 = 18'sd0;
 3'b001: multiplier_out67 = 18'sd0;
 3'b011: multiplier_out67 = 18'sd0;
 3'b010: multiplier_out67 = 18'sd0;
 3'b100: multiplier_out67 = -18'sd1466;
 3'b101: multiplier_out67 = -18'sd489;
 3'b111: multiplier_out67 = 18'sd489;
 3'b110: multiplier_out67 = 18'sd1466;
 default: multiplier_out67 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x68)
 3'b000: multiplier_out68 = 18'sd0;
 3'b001: multiplier_out68 = 18'sd0;
 3'b011: multiplier_out68 = 18'sd0;
 3'b010: multiplier_out68 = 18'sd0;
 3'b100: multiplier_out68 = 18'sd491;
 3'b101: multiplier_out68 = 18'sd164;
 3'b111: multiplier_out68 = -18'sd164;
 3'b110: multiplier_out68 = -18'sd491;
 default: multiplier_out68 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x69)
 3'b000: multiplier_out69 = 18'sd0;
 3'b001: multiplier_out69 = 18'sd0;
 3'b011: multiplier_out69 = 18'sd0;
 3'b010: multiplier_out69 = 18'sd0;
 3'b100: multiplier_out69 = 18'sd1820;
 3'b101: multiplier_out69 = 18'sd607;
 3'b111: multiplier_out69 = -18'sd607;
 3'b110: multiplier_out69 = -18'sd1820;
 default: multiplier_out69 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x70)
 3'b000: multiplier_out70 = 18'sd0;
 3'b001: multiplier_out70 = 18'sd0;
 3'b011: multiplier_out70 = 18'sd0;
 3'b010: multiplier_out70 = 18'sd0;
 3'b100: multiplier_out70 = 18'sd1896;
 3'b101: multiplier_out70 = 18'sd632;
 3'b111: multiplier_out70 = -18'sd632;
 3'b110: multiplier_out70 = -18'sd1896;
 default: multiplier_out70 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x71)
 3'b000: multiplier_out71 = 18'sd0;
 3'b001: multiplier_out71 = 18'sd0;
 3'b011: multiplier_out71 = 18'sd0;
 3'b010: multiplier_out71 = 18'sd0;
 3'b100: multiplier_out71 = 18'sd890;
 3'b101: multiplier_out71 = 18'sd297;
 3'b111: multiplier_out71 = -18'sd297;
 3'b110: multiplier_out71 = -18'sd890;
 default: multiplier_out71 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x72)
 3'b000: multiplier_out72 = 18'sd0;
 3'b001: multiplier_out72 = 18'sd0;
 3'b011: multiplier_out72 = 18'sd0;
 3'b010: multiplier_out72 = 18'sd0;
 3'b100: multiplier_out72 = -18'sd458;
 3'b101: multiplier_out72 = -18'sd153;
 3'b111: multiplier_out72 = 18'sd153;
 3'b110: multiplier_out72 = 18'sd458;
 default: multiplier_out72 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x73)
 3'b000: multiplier_out73 = 18'sd0;
 3'b001: multiplier_out73 = 18'sd0;
 3'b011: multiplier_out73 = 18'sd0;
 3'b010: multiplier_out73 = 18'sd0;
 3'b100: multiplier_out73 = -18'sd1336;
 3'b101: multiplier_out73 = -18'sd445;
 3'b111: multiplier_out73 = 18'sd445;
 3'b110: multiplier_out73 = 18'sd1336;
 default: multiplier_out73 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x74)
 3'b000: multiplier_out74 = 18'sd0;
 3'b001: multiplier_out74 = 18'sd0;
 3'b011: multiplier_out74 = 18'sd0;
 3'b010: multiplier_out74 = 18'sd0;
 3'b100: multiplier_out74 = -18'sd1319;
 3'b101: multiplier_out74 = -18'sd440;
 3'b111: multiplier_out74 = 18'sd440;
 3'b110: multiplier_out74 = 18'sd1319;
 default: multiplier_out74 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x75)
 3'b000: multiplier_out75 = 18'sd0;
 3'b001: multiplier_out75 = 18'sd0;
 3'b011: multiplier_out75 = 18'sd0;
 3'b010: multiplier_out75 = 18'sd0;
 3'b100: multiplier_out75 = -18'sd553;
 3'b101: multiplier_out75 = -18'sd184;
 3'b111: multiplier_out75 = 18'sd184;
 3'b110: multiplier_out75 = 18'sd553;
 default: multiplier_out75 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x76)
 3'b000: multiplier_out76 = 18'sd0;
 3'b001: multiplier_out76 = 18'sd0;
 3'b011: multiplier_out76 = 18'sd0;
 3'b010: multiplier_out76 = 18'sd0;
 3'b100: multiplier_out76 = 18'sd418;
 3'b101: multiplier_out76 = 18'sd139;
 3'b111: multiplier_out76 = -18'sd139;
 3'b110: multiplier_out76 = -18'sd418;
 default: multiplier_out76 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x77)
 3'b000: multiplier_out77 = 18'sd0;
 3'b001: multiplier_out77 = 18'sd0;
 3'b011: multiplier_out77 = 18'sd0;
 3'b010: multiplier_out77 = 18'sd0;
 3'b100: multiplier_out77 = 18'sd1009;
 3'b101: multiplier_out77 = 18'sd336;
 3'b111: multiplier_out77 = -18'sd336;
 3'b110: multiplier_out77 = -18'sd1009;
 default: multiplier_out77 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x78)
 3'b000: multiplier_out78 = 18'sd0;
 3'b001: multiplier_out78 = 18'sd0;
 3'b011: multiplier_out78 = 18'sd0;
 3'b010: multiplier_out78 = 18'sd0;
 3'b100: multiplier_out78 = 18'sd935;
 3'b101: multiplier_out78 = 18'sd312;
 3'b111: multiplier_out78 = -18'sd312;
 3'b110: multiplier_out78 = -18'sd935;
 default: multiplier_out78 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x79)
 3'b000: multiplier_out79 = 18'sd0;
 3'b001: multiplier_out79 = 18'sd0;
 3'b011: multiplier_out79 = 18'sd0;
 3'b010: multiplier_out79 = 18'sd0;
 3'b100: multiplier_out79 = 18'sd335;
 3'b101: multiplier_out79 = 18'sd112;
 3'b111: multiplier_out79 = -18'sd112;
 3'b110: multiplier_out79 = -18'sd335;
 default: multiplier_out79 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x80)
 3'b000: multiplier_out80 = 18'sd0;
 3'b001: multiplier_out80 = 18'sd0;
 3'b011: multiplier_out80 = 18'sd0;
 3'b010: multiplier_out80 = 18'sd0;
 3'b100: multiplier_out80 = -18'sd373;
 3'b101: multiplier_out80 = -18'sd124;
 3'b111: multiplier_out80 = 18'sd124;
 3'b110: multiplier_out80 = 18'sd373;
 default: multiplier_out80 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x81)
 3'b000: multiplier_out81 = 18'sd0;
 3'b001: multiplier_out81 = 18'sd0;
 3'b011: multiplier_out81 = 18'sd0;
 3'b010: multiplier_out81 = 18'sd0;
 3'b100: multiplier_out81 = -18'sd767;
 3'b101: multiplier_out81 = -18'sd256;
 3'b111: multiplier_out81 = 18'sd256;
 3'b110: multiplier_out81 = 18'sd767;
 default: multiplier_out81 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x82)
 3'b000: multiplier_out82 = 18'sd0;
 3'b001: multiplier_out82 = 18'sd0;
 3'b011: multiplier_out82 = 18'sd0;
 3'b010: multiplier_out82 = 18'sd0;
 3'b100: multiplier_out82 = -18'sd662;
 3'b101: multiplier_out82 = -18'sd221;
 3'b111: multiplier_out82 = 18'sd221;
 3'b110: multiplier_out82 = 18'sd662;
 default: multiplier_out82 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x83)
 3'b000: multiplier_out83 = 18'sd0;
 3'b001: multiplier_out83 = 18'sd0;
 3'b011: multiplier_out83 = 18'sd0;
 3'b010: multiplier_out83 = 18'sd0;
 3'b100: multiplier_out83 = -18'sd190;
 3'b101: multiplier_out83 = -18'sd63;
 3'b111: multiplier_out83 = 18'sd63;
 3'b110: multiplier_out83 = 18'sd190;
 default: multiplier_out83 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x84)
 3'b000: multiplier_out84 = 18'sd0;
 3'b001: multiplier_out84 = 18'sd0;
 3'b011: multiplier_out84 = 18'sd0;
 3'b010: multiplier_out84 = 18'sd0;
 3'b100: multiplier_out84 = 18'sd325;
 3'b101: multiplier_out84 = 18'sd108;
 3'b111: multiplier_out84 = -18'sd108;
 3'b110: multiplier_out84 = -18'sd325;
 default: multiplier_out84 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x85)
 3'b000: multiplier_out85 = 18'sd0;
 3'b001: multiplier_out85 = 18'sd0;
 3'b011: multiplier_out85 = 18'sd0;
 3'b010: multiplier_out85 = 18'sd0;
 3'b100: multiplier_out85 = 18'sd578;
 3'b101: multiplier_out85 = 18'sd193;
 3'b111: multiplier_out85 = -18'sd193;
 3'b110: multiplier_out85 = -18'sd578;
 default: multiplier_out85 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x86)
 3'b000: multiplier_out86 = 18'sd0;
 3'b001: multiplier_out86 = 18'sd0;
 3'b011: multiplier_out86 = 18'sd0;
 3'b010: multiplier_out86 = 18'sd0;
 3'b100: multiplier_out86 = 18'sd461;
 3'b101: multiplier_out86 = 18'sd154;
 3'b111: multiplier_out86 = -18'sd154;
 3'b110: multiplier_out86 = -18'sd461;
 default: multiplier_out86 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x87)
 3'b000: multiplier_out87 = 18'sd0;
 3'b001: multiplier_out87 = 18'sd0;
 3'b011: multiplier_out87 = 18'sd0;
 3'b010: multiplier_out87 = 18'sd0;
 3'b100: multiplier_out87 = 18'sd92;
 3'b101: multiplier_out87 = 18'sd31;
 3'b111: multiplier_out87 = -18'sd31;
 3'b110: multiplier_out87 = -18'sd92;
 default: multiplier_out87 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x88)
 3'b000: multiplier_out88 = 18'sd0;
 3'b001: multiplier_out88 = 18'sd0;
 3'b011: multiplier_out88 = 18'sd0;
 3'b010: multiplier_out88 = 18'sd0;
 3'b100: multiplier_out88 = -18'sd276;
 3'b101: multiplier_out88 = -18'sd92;
 3'b111: multiplier_out88 = 18'sd92;
 3'b110: multiplier_out88 = 18'sd276;
 default: multiplier_out88 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x89)
 3'b000: multiplier_out89 = 18'sd0;
 3'b001: multiplier_out89 = 18'sd0;
 3'b011: multiplier_out89 = 18'sd0;
 3'b010: multiplier_out89 = 18'sd0;
 3'b100: multiplier_out89 = -18'sd430;
 3'b101: multiplier_out89 = -18'sd143;
 3'b111: multiplier_out89 = 18'sd143;
 3'b110: multiplier_out89 = 18'sd430;
 default: multiplier_out89 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x90)
 3'b000: multiplier_out90 = 18'sd0;
 3'b001: multiplier_out90 = 18'sd0;
 3'b011: multiplier_out90 = 18'sd0;
 3'b010: multiplier_out90 = 18'sd0;
 3'b100: multiplier_out90 = -18'sd310;
 3'b101: multiplier_out90 = -18'sd103;
 3'b111: multiplier_out90 = 18'sd103;
 3'b110: multiplier_out90 = 18'sd310;
 default: multiplier_out90 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x91)
 3'b000: multiplier_out91 = 18'sd0;
 3'b001: multiplier_out91 = 18'sd0;
 3'b011: multiplier_out91 = 18'sd0;
 3'b010: multiplier_out91 = 18'sd0;
 3'b100: multiplier_out91 = -18'sd28;
 3'b101: multiplier_out91 = -18'sd9;
 3'b111: multiplier_out91 = 18'sd9;
 3'b110: multiplier_out91 = 18'sd28;
 default: multiplier_out91 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x92)
 3'b000: multiplier_out92 = 18'sd0;
 3'b001: multiplier_out92 = 18'sd0;
 3'b011: multiplier_out92 = 18'sd0;
 3'b010: multiplier_out92 = 18'sd0;
 3'b100: multiplier_out92 = 18'sd228;
 3'b101: multiplier_out92 = 18'sd76;
 3'b111: multiplier_out92 = -18'sd76;
 3'b110: multiplier_out92 = -18'sd228;
 default: multiplier_out92 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x93)
 3'b000: multiplier_out93 = 18'sd0;
 3'b001: multiplier_out93 = 18'sd0;
 3'b011: multiplier_out93 = 18'sd0;
 3'b010: multiplier_out93 = 18'sd0;
 3'b100: multiplier_out93 = 18'sd311;
 3'b101: multiplier_out93 = 18'sd104;
 3'b111: multiplier_out93 = -18'sd104;
 3'b110: multiplier_out93 = -18'sd311;
 default: multiplier_out93 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x94)
 3'b000: multiplier_out94 = 18'sd0;
 3'b001: multiplier_out94 = 18'sd0;
 3'b011: multiplier_out94 = 18'sd0;
 3'b010: multiplier_out94 = 18'sd0;
 3'b100: multiplier_out94 = 18'sd199;
 3'b101: multiplier_out94 = 18'sd66;
 3'b111: multiplier_out94 = -18'sd66;
 3'b110: multiplier_out94 = -18'sd199;
 default: multiplier_out94 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x95)
 3'b000: multiplier_out95 = 18'sd0;
 3'b001: multiplier_out95 = 18'sd0;
 3'b011: multiplier_out95 = 18'sd0;
 3'b010: multiplier_out95 = 18'sd0;
 3'b100: multiplier_out95 = -18'sd13;
 3'b101: multiplier_out95 = -18'sd4;
 3'b111: multiplier_out95 = 18'sd4;
 3'b110: multiplier_out95 = 18'sd13;
 default: multiplier_out95 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x96)
 3'b000: multiplier_out96 = 18'sd0;
 3'b001: multiplier_out96 = 18'sd0;
 3'b011: multiplier_out96 = 18'sd0;
 3'b010: multiplier_out96 = 18'sd0;
 3'b100: multiplier_out96 = -18'sd183;
 3'b101: multiplier_out96 = -18'sd61;
 3'b111: multiplier_out96 = 18'sd61;
 3'b110: multiplier_out96 = 18'sd183;
 default: multiplier_out96 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x97)
 3'b000: multiplier_out97 = 18'sd0;
 3'b001: multiplier_out97 = 18'sd0;
 3'b011: multiplier_out97 = 18'sd0;
 3'b010: multiplier_out97 = 18'sd0;
 3'b100: multiplier_out97 = -18'sd218;
 3'b101: multiplier_out97 = -18'sd73;
 3'b111: multiplier_out97 = 18'sd73;
 3'b110: multiplier_out97 = 18'sd218;
 default: multiplier_out97 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x98)
 3'b000: multiplier_out98 = 18'sd0;
 3'b001: multiplier_out98 = 18'sd0;
 3'b011: multiplier_out98 = 18'sd0;
 3'b010: multiplier_out98 = 18'sd0;
 3'b100: multiplier_out98 = -18'sd118;
 3'b101: multiplier_out98 = -18'sd39;
 3'b111: multiplier_out98 = 18'sd39;
 3'b110: multiplier_out98 = 18'sd118;
 default: multiplier_out98 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x99)
 3'b000: multiplier_out99 = 18'sd0;
 3'b001: multiplier_out99 = 18'sd0;
 3'b011: multiplier_out99 = 18'sd0;
 3'b010: multiplier_out99 = 18'sd0;
 3'b100: multiplier_out99 = 18'sd35;
 3'b101: multiplier_out99 = 18'sd12;
 3'b111: multiplier_out99 = -18'sd12;
 3'b110: multiplier_out99 = -18'sd35;
 default: multiplier_out99 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x100)
 3'b000: multiplier_out100 = 18'sd0;
 3'b001: multiplier_out100 = 18'sd0;
 3'b011: multiplier_out100 = 18'sd0;
 3'b010: multiplier_out100 = 18'sd0;
 3'b100: multiplier_out100 = 18'sd141;
 3'b101: multiplier_out100 = 18'sd47;
 3'b111: multiplier_out100 = -18'sd47;
 3'b110: multiplier_out100 = -18'sd141;
 default: multiplier_out100 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x101)
 3'b000: multiplier_out101 = 18'sd0;
 3'b001: multiplier_out101 = 18'sd0;
 3'b011: multiplier_out101 = 18'sd0;
 3'b010: multiplier_out101 = 18'sd0;
 3'b100: multiplier_out101 = 18'sd145;
 3'b101: multiplier_out101 = 18'sd48;
 3'b111: multiplier_out101 = -18'sd48;
 3'b110: multiplier_out101 = -18'sd145;
 default: multiplier_out101 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x102)
 3'b000: multiplier_out102 = 18'sd0;
 3'b001: multiplier_out102 = 18'sd0;
 3'b011: multiplier_out102 = 18'sd0;
 3'b010: multiplier_out102 = 18'sd0;
 3'b100: multiplier_out102 = 18'sd60;
 3'b101: multiplier_out102 = 18'sd20;
 3'b111: multiplier_out102 = -18'sd20;
 3'b110: multiplier_out102 = -18'sd60;
 default: multiplier_out102 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x103)
 3'b000: multiplier_out103 = 18'sd0;
 3'b001: multiplier_out103 = 18'sd0;
 3'b011: multiplier_out103 = 18'sd0;
 3'b010: multiplier_out103 = 18'sd0;
 3'b100: multiplier_out103 = -18'sd45;
 3'b101: multiplier_out103 = -18'sd15;
 3'b111: multiplier_out103 = 18'sd15;
 3'b110: multiplier_out103 = 18'sd45;
 default: multiplier_out103 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x104)
 3'b000: multiplier_out104 = 18'sd0;
 3'b001: multiplier_out104 = 18'sd0;
 3'b011: multiplier_out104 = 18'sd0;
 3'b010: multiplier_out104 = 18'sd0;
 3'b100: multiplier_out104 = -18'sd104;
 3'b101: multiplier_out104 = -18'sd35;
 3'b111: multiplier_out104 = 18'sd35;
 3'b110: multiplier_out104 = 18'sd104;
 default: multiplier_out104 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x105)
 3'b000: multiplier_out105 = 18'sd0;
 3'b001: multiplier_out105 = 18'sd0;
 3'b011: multiplier_out105 = 18'sd0;
 3'b010: multiplier_out105 = 18'sd0;
 3'b100: multiplier_out105 = -18'sd89;
 3'b101: multiplier_out105 = -18'sd30;
 3'b111: multiplier_out105 = 18'sd30;
 3'b110: multiplier_out105 = 18'sd89;
 default: multiplier_out105 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x106)
 3'b000: multiplier_out106 = 18'sd0;
 3'b001: multiplier_out106 = 18'sd0;
 3'b011: multiplier_out106 = 18'sd0;
 3'b010: multiplier_out106 = 18'sd0;
 3'b100: multiplier_out106 = -18'sd22;
 3'b101: multiplier_out106 = -18'sd7;
 3'b111: multiplier_out106 = 18'sd7;
 3'b110: multiplier_out106 = 18'sd22;
 default: multiplier_out106 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x107)
 3'b000: multiplier_out107 = 18'sd0;
 3'b001: multiplier_out107 = 18'sd0;
 3'b011: multiplier_out107 = 18'sd0;
 3'b010: multiplier_out107 = 18'sd0;
 3'b100: multiplier_out107 = 18'sd47;
 3'b101: multiplier_out107 = 18'sd16;
 3'b111: multiplier_out107 = -18'sd16;
 3'b110: multiplier_out107 = -18'sd47;
 default: multiplier_out107 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x108)
 3'b000: multiplier_out108 = 18'sd0;
 3'b001: multiplier_out108 = 18'sd0;
 3'b011: multiplier_out108 = 18'sd0;
 3'b010: multiplier_out108 = 18'sd0;
 3'b100: multiplier_out108 = 18'sd74;
 3'b101: multiplier_out108 = 18'sd25;
 3'b111: multiplier_out108 = -18'sd25;
 3'b110: multiplier_out108 = -18'sd74;
 default: multiplier_out108 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x109)
 3'b000: multiplier_out109 = 18'sd0;
 3'b001: multiplier_out109 = 18'sd0;
 3'b011: multiplier_out109 = 18'sd0;
 3'b010: multiplier_out109 = 18'sd0;
 3'b100: multiplier_out109 = 18'sd50;
 3'b101: multiplier_out109 = 18'sd17;
 3'b111: multiplier_out109 = -18'sd17;
 3'b110: multiplier_out109 = -18'sd50;
 default: multiplier_out109 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x110)
 3'b000: multiplier_out110 = 18'sd0;
 3'b001: multiplier_out110 = 18'sd0;
 3'b011: multiplier_out110 = 18'sd0;
 3'b010: multiplier_out110 = 18'sd0;
 3'b100: multiplier_out110 = -18'sd2;
 3'b101: multiplier_out110 = -18'sd1;
 3'b111: multiplier_out110 = 18'sd1;
 3'b110: multiplier_out110 = 18'sd2;
 default: multiplier_out110 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x111)
 3'b000: multiplier_out111 = 18'sd0;
 3'b001: multiplier_out111 = 18'sd0;
 3'b011: multiplier_out111 = 18'sd0;
 3'b010: multiplier_out111 = 18'sd0;
 3'b100: multiplier_out111 = -18'sd42;
 3'b101: multiplier_out111 = -18'sd14;
 3'b111: multiplier_out111 = 18'sd14;
 3'b110: multiplier_out111 = 18'sd42;
 default: multiplier_out111 = -18'sd0;
 endcase

always @ (posedge sys_clk)
case(x112)
 3'b000: multiplier_out112 = 18'sd0;
 3'b001: multiplier_out112 = 18'sd0;
 3'b011: multiplier_out112 = 18'sd0;
 3'b010: multiplier_out112 = 18'sd0;
 3'b100: multiplier_out112 = -18'sd49;
 3'b101: multiplier_out112 = -18'sd16;
 3'b111: multiplier_out112 = 18'sd16;
 3'b110: multiplier_out112 = 18'sd49;
 default: multiplier_out112 = -18'sd0;
 endcase 
/*************************************************************************************************************************
First Summing Level
*************************************************************************************************************************/
always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_1 = multiplier_out0 + multiplier_out1 + multiplier_out2 + multiplier_out3;
  else
    sum_level1_1 = sum_level1_1;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_2 = multiplier_out4 + multiplier_out5 + multiplier_out6 + multiplier_out7;
  else
    sum_level1_2 = sum_level1_2;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_3 = multiplier_out8 + multiplier_out9 + multiplier_out10 + multiplier_out11;
  else
    sum_level1_3 = sum_level1_3;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_4 = multiplier_out12 + multiplier_out13 + multiplier_out14 + multiplier_out15;
  else
    sum_level1_4 = sum_level1_4;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_5 = multiplier_out16 + multiplier_out17 + multiplier_out18 + multiplier_out19;
  else
    sum_level1_5 = sum_level1_5;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_6 = multiplier_out20 + multiplier_out21 + multiplier_out22 + multiplier_out23;
  else
    sum_level1_6 = sum_level1_6;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_7 = multiplier_out24 + multiplier_out25 + multiplier_out26 + multiplier_out27;
  else
    sum_level1_7 = sum_level1_7;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_8 = multiplier_out28 + multiplier_out29 + multiplier_out30 + multiplier_out31;
  else
    sum_level1_8 = sum_level1_8;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_9 = multiplier_out32 + multiplier_out33 + multiplier_out34 + multiplier_out35;
  else
    sum_level1_9 = sum_level1_9;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_10 = multiplier_out36 + multiplier_out37 + multiplier_out38 + multiplier_out39;
  else
    sum_level1_10 = sum_level1_10;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_11 = multiplier_out40 + multiplier_out41 + multiplier_out42 + multiplier_out43;
  else
    sum_level1_11 = sum_level1_11;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_11a = multiplier_out44 + multiplier_out45 + multiplier_out46 + multiplier_out47;
  else
    sum_level1_11a = sum_level1_11a;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_12 = multiplier_out48 + multiplier_out49 + multiplier_out50 + multiplier_out51;
  else
    sum_level1_12 = sum_level1_12;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_13 = multiplier_out52 + multiplier_out53 + multiplier_out54 + multiplier_out55;
  else
    sum_level1_13 = sum_level1_13;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_14 = multiplier_out56 + multiplier_out57 + multiplier_out58 + multiplier_out59;
  else
    sum_level1_14 = sum_level1_14;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_15 = multiplier_out60 + multiplier_out61 + multiplier_out62 + multiplier_out63;
  else
    sum_level1_15 = sum_level1_15;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_16 = multiplier_out64 + multiplier_out65 + multiplier_out66 + multiplier_out67;
  else
    sum_level1_16 = sum_level1_16;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_17 = multiplier_out68 + multiplier_out69 + multiplier_out70 + multiplier_out71;
  else
    sum_level1_17 = sum_level1_17;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_18 = multiplier_out72 + multiplier_out73 + multiplier_out74 + multiplier_out75;
  else
    sum_level1_18 = sum_level1_18;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_19 = multiplier_out76 + multiplier_out77 + multiplier_out78 + multiplier_out79;
  else
    sum_level1_19 = sum_level1_19;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_20 = multiplier_out80 + multiplier_out81 + multiplier_out82 + multiplier_out83;
  else
    sum_level1_20 = sum_level1_20;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_21 = multiplier_out84 + multiplier_out85 + multiplier_out86 + multiplier_out87;
  else
    sum_level1_21 = sum_level1_21;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_22 = multiplier_out88 + multiplier_out89 + multiplier_out90 + multiplier_out91;
  else
    sum_level1_22 = sum_level1_22;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_23 = multiplier_out92 + multiplier_out93 + multiplier_out94 + multiplier_out95;
  else
    sum_level1_23 = sum_level1_23;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_24 = multiplier_out96 + multiplier_out97 + multiplier_out98 + multiplier_out99;
  else
    sum_level1_24 = sum_level1_24;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_25 = multiplier_out100 + multiplier_out101 + multiplier_out102 + multiplier_out103;
  else
    sum_level1_25 = sum_level1_25;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    sum_level1_26 = multiplier_out104 + multiplier_out105 + multiplier_out106 + multiplier_out107;
  else
    sum_level1_26 = sum_level1_26;

always @ (posedge sys_clk)
    if(sam_clk_enable == 1'b1)
      sum_level1_27 = multiplier_out108 + multiplier_out109 + multiplier_out110 + multiplier_out111 + multiplier_out112;
    else
      sum_level1_27 = sum_level1_27;

/*************************************************************************************************************************
Second Summing Level
*************************************************************************************************************************/
always @ (posedge sys_clk)
    if(sam_clk_enable == 1'b1)
      sum_level2_1 = sum_level1_1 + sum_level1_2 + sum_level1_3 +sum_level1_4;
    else
      sum_level2_1 = sum_level2_1;

  always @ (posedge sys_clk)
      if(sam_clk_enable == 1'b1)
        sum_level2_2 = sum_level1_5 + sum_level1_6 + sum_level1_7 +sum_level1_8;
      else
        sum_level2_2 = sum_level2_2;

  always @ (posedge sys_clk)
      if(sam_clk_enable == 1'b1)
        sum_level2_3 = sum_level1_9 + sum_level1_10 + sum_level1_11 +sum_level1_11a;
      else
        sum_level2_3 = sum_level2_3;

  always @ (posedge sys_clk)
      if(sam_clk_enable == 1'b1)
        sum_level2_4 = sum_level1_13 + sum_level1_14 + sum_level1_15 +sum_level1_16;
      else
        sum_level2_4= sum_level2_4;

  always @ (posedge sys_clk)
      if(sam_clk_enable == 1'b1)
        sum_level2_5 = sum_level1_17 + sum_level1_18 + sum_level1_19 +sum_level1_20;
      else
        sum_level2_5= sum_level2_5;

  always @ (posedge sys_clk)
    if(sam_clk_enable == 1'b1)
      sum_level2_6 = sum_level1_21 + sum_level1_22 + sum_level1_23 +sum_level1_24;
    else
      sum_level2_6= sum_level2_6;

  always @ (posedge sys_clk)
      if(sam_clk_enable == 1'b1)
        sum_level2_7 = sum_level1_25 + sum_level1_26 + sum_level1_27+ sum_level1_12;
      else
        sum_level2_7= sum_level2_7;
/*************************************************************************************************************************
Third Summing Level
*************************************************************************************************************************/
always @ (posedge sys_clk)
    if(sam_clk_enable == 1'b1)
      sum_level3_1 = sum_level2_1 + sum_level2_2 + sum_level2_3 +sum_level2_4;
    else
      sum_level3_1 = sum_level3_1;

always @ (posedge sys_clk)
    if(sam_clk_enable == 1'b1)
      sum_level3_2 = sum_level2_5 + sum_level2_6 + sum_level2_7;
    else
      sum_level3_2 = sum_level3_2;
/*************************************************************************************************************************
Fourth Summing Level
************************************************************************************************************************/
always @ (posedge sys_clk)
    if(sam_clk_enable == 1'b1)
      sum_level4_1 = sum_level3_2 + sum_level3_1;
    else
      sum_level4_1 = sum_level4_1;

/*************************************************************************************************************************
Output
*************************************************************************************************************************/
//reg signed [19:0] test;
//always @ *
//  test = sum_level4_1 << 1;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
      //y_out = {sum_level4_1[17], sum_level4_1[17:1]};
    y_out = sum_level4_1;
      //y_out = test[17:0];
  else
    y_out = y_out;

endmodule
`default_nettype wire
