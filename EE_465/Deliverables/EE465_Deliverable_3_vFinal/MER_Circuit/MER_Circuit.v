/************************************************************************************************************************
EE 465 MER Measurement Circuit Top Submodule
Sean Froome

Since the MER measurement circuit from Deliverable 2 is quite large on its own,
here it is set up as a separate module outside the top module of the system.
***********************************************************************************************************************/
`default_nettype none
module MER_Circuit(
input wire sys_clk,
input wire sam_clk_enable,
input wire sym_clk_enable,
input wire reset,
input wire [3:0] I_Select,
input wire [3:0] Q_Select,
input wire signed [17:0] output_gold_rx_I,
input wire signed [17:0] output_gold_rx_Q,
input wire signed [1:0]  q_out_I,
input wire signed [1:0]  q_out_Q,
output wire [31:0] MER_dB_Float_I,
output wire [31:0] MER_dB_Float_Q
);

/*************************************************************************************************************************
Conditions for Clear Accumulator
**************************************************************************************************************************/
reg clear_accumulator;
reg [24:0] counter;
always  @ (posedge sys_clk)
  if(reset == 1'b1)
    counter = 25'd0;
	else if ((sym_clk_enable == 1'b1)  && (counter == 25'd4194304))//&& counter == 22'd1024)
    counter = 25'd0;
  else if(sym_clk_enable == 1'b1)
  	counter = counter + 23'd1;
  else
    counter = counter;

always @ *
  if(reset)
    clear_accumulator = 1'b1;
  else if(counter == 25'd4194304)
  	clear_accumulator = 1'b1;
  else
  	clear_accumulator = 1'b0;
/************************************************************************************************************************
Delay Outputs from Filters

I SELECT USES SW[10:7]

Q SELECT USES SW[14:11]
***********************************************************************************************************************/
reg signed [17:0] Delayed_Output_Sam_Clk_I, Delayed_Output_Sam_Clk_Q;

reg signed  [17:0] delay_0_I,  delay_1_I,  delay_2_I,  delay_3_I;
reg signed  [17:0] delay_4_I,  delay_5_I,  delay_6_I,  delay_7_I;
reg signed  [17:0] delay_8_I,  delay_9_I,  delay_10_I, delay_11_I;
reg signed  [17:0] delay_12_I, delay_13_I, delay_14_I, delay_15_I;

reg signed  [17:0] delay_0_Q,  delay_1_Q,  delay_2_Q,  delay_3_Q;
reg signed  [17:0] delay_4_Q,  delay_5_Q,  delay_6_Q,  delay_7_Q;
reg signed  [17:0] delay_8_Q,  delay_9_Q,  delay_10_Q, delay_11_Q;
reg signed  [17:0] delay_12_Q, delay_13_Q, delay_14_Q, delay_15_Q;

always @ (posedge sys_clk)
  delay_0_I = output_gold_rx_I;
always @ (posedge sys_clk)
  delay_1_I = delay_0_I;
always @ (posedge sys_clk)
  delay_2_I = delay_1_I;
always @ (posedge sys_clk)
  delay_3_I = delay_2_I;
always @ (posedge sys_clk)
  delay_4_I = delay_3_I;
always @ (posedge sys_clk)
  delay_5_I = delay_4_I;
always @ (posedge sys_clk)
  delay_6_I = delay_5_I;
always @ (posedge sys_clk)
  delay_7_I = delay_6_I;
always @ (posedge sys_clk)
  delay_8_I = delay_7_I;
always @ (posedge sys_clk)
  delay_9_I = delay_8_I;
always @ (posedge sys_clk)
  delay_10_I = delay_9_I;
always @ (posedge sys_clk)
  delay_11_I = delay_10_I;
always @ (posedge sys_clk)
  delay_12_I = delay_11_I;
always @ (posedge sys_clk)
  delay_13_I = delay_12_I;
always @ (posedge sys_clk)
  delay_14_I = delay_13_I;
always @ (posedge sys_clk)
  delay_15_I = delay_14_I;

always @ (posedge sys_clk)
  delay_0_Q = output_gold_rx_Q;
always @ (posedge sys_clk)
  delay_1_Q = delay_0_Q;
always @ (posedge sys_clk)
  delay_2_Q = delay_1_Q;
always @ (posedge sys_clk)
  delay_3_Q = delay_2_Q;
always @ (posedge sys_clk)
  delay_4_Q = delay_3_Q;
always @ (posedge sys_clk)
  delay_5_Q = delay_4_Q;
always @ (posedge sys_clk)
  delay_6_Q = delay_5_Q;
always @ (posedge sys_clk)
  delay_7_Q = delay_6_Q;
always @ (posedge sys_clk)
  delay_8_Q = delay_7_Q;
always @ (posedge sys_clk)
  delay_9_Q = delay_8_Q;
always @ (posedge sys_clk)
  delay_10_Q = delay_9_Q;
always @ (posedge sys_clk)
  delay_11_Q = delay_10_Q;
always @ (posedge sys_clk)
  delay_12_Q = delay_11_Q;
always @ (posedge sys_clk)
  delay_13_Q = delay_12_Q;
always @ (posedge sys_clk)
  delay_14_Q = delay_13_Q;
always @ (posedge sys_clk)
  delay_15_Q = delay_14_Q;

always @ (posedge sys_clk)
  case(I_Select)
    4'b0000: Delayed_Output_Sam_Clk_I =  delay_0_I;
    4'b0001: Delayed_Output_Sam_Clk_I =  delay_1_I;
    4'b0010: Delayed_Output_Sam_Clk_I =  delay_2_I;
    4'b0011: Delayed_Output_Sam_Clk_I =  delay_3_I;
    4'b0100: Delayed_Output_Sam_Clk_I =  delay_4_I;
    4'b0101: Delayed_Output_Sam_Clk_I =  delay_5_I;
    4'b0110: Delayed_Output_Sam_Clk_I =  delay_6_I;
    4'b0111: Delayed_Output_Sam_Clk_I =  delay_7_I;
    4'b1000: Delayed_Output_Sam_Clk_I =  delay_8_I;
    4'b1001: Delayed_Output_Sam_Clk_I =  delay_9_I;
    4'b1010: Delayed_Output_Sam_Clk_I =  delay_10_I;
    4'b1011: Delayed_Output_Sam_Clk_I =  delay_11_I;
    4'b1100: Delayed_Output_Sam_Clk_I =  delay_12_I;
    4'b1101: Delayed_Output_Sam_Clk_I =  delay_13_I;
    4'b1110: Delayed_Output_Sam_Clk_I =  delay_14_I;
    4'b1111: Delayed_Output_Sam_Clk_I =  delay_15_I;
    default: Delayed_Output_Sam_Clk_I = 18'sd0;
  endcase


always @ (posedge sys_clk)
  case(Q_Select)
    4'b0000: Delayed_Output_Sam_Clk_Q =  delay_0_Q;
    4'b0001: Delayed_Output_Sam_Clk_Q =  delay_1_Q;
    4'b0010: Delayed_Output_Sam_Clk_Q =  delay_2_Q;
    4'b0011: Delayed_Output_Sam_Clk_Q =  delay_3_Q;
    4'b0100: Delayed_Output_Sam_Clk_Q =  delay_4_Q;
    4'b0101: Delayed_Output_Sam_Clk_Q =  delay_5_Q;
    4'b0110: Delayed_Output_Sam_Clk_Q =  delay_6_Q;
    4'b0111: Delayed_Output_Sam_Clk_Q =  delay_7_Q;
    4'b1000: Delayed_Output_Sam_Clk_Q =  delay_8_Q;
    4'b1001: Delayed_Output_Sam_Clk_Q =  delay_9_Q;
    4'b1010: Delayed_Output_Sam_Clk_Q =  delay_10_Q;
    4'b1011: Delayed_Output_Sam_Clk_Q =  delay_11_Q;
    4'b1100: Delayed_Output_Sam_Clk_Q =  delay_12_Q;
    4'b1101: Delayed_Output_Sam_Clk_Q =  delay_13_Q;
    4'b1110: Delayed_Output_Sam_Clk_Q =  delay_14_Q;
    4'b1111: Delayed_Output_Sam_Clk_Q =  delay_15_Q;
    default: Delayed_Output_Sam_Clk_Q = 18'sd0;
  endcase
/************************************************************************************************************************
I Phase
***********************************************************************************************************************/
wire signed [17:0] reference_level_I;
wire signed [34:0] mapper_out_power_I;
wire signed [1:0]  slicer_out_I;
wire signed [17:0] mapper_out_I;
wire signed [17:0] error_from_dv_I;
wire signed [34:0] accumulated_squared_error_I;
wire signed [17:0] accumulated_error_I;
wire sym_error_I;
wire sym_correct_I;
/************************************************************************************************************************
Q Phase
************************************************************************************************************************/
wire signed [17:0] reference_level_Q;
wire signed [34:0] mapper_out_power_Q;
wire signed [1:0]  slicer_out_Q;
wire signed [17:0] mapper_out_Q;
wire signed [17:0] error_from_dv_Q;
wire signed [34:0] accumulated_squared_error_Q;
wire signed [34:0] accumulated_error_Q;
wire sym_error_Q;
wire sym_correct_Q;
/*************************************************************************************************************************
REFERENCE LEVEL CHUNK
*************************************************************************************************************************/
reference_level_chunk reference_I(
	.reset(reset),
	.decision_variable(Delayed_Output_Sam_Clk_I),
	.clear_accumulator(clear_accumulator),
	.sym_clk_enable(sym_clk_enable),
	.sys_clk(sys_clk),
	.reference_level(reference_level_I),
	.mapper_out_power(mapper_out_power_I));

reference_level_chunk reference_Q(
	.reset(reset),
	.decision_variable(Delayed_Output_Sam_Clk_Q),
	.clear_accumulator(clear_accumulator),
	.sym_clk_enable(sym_clk_enable),
	.sys_clk(sys_clk),
	.reference_level(reference_level_Q),
	.mapper_out_power(mapper_out_power_Q));
/*************************************************************************************************************************
SLICER
*************************************************************************************************************************/
slicer slicer_I(
	.reference_level(reference_level_I),
	.decision_variable(Delayed_Output_Sam_Clk_I),
	.slicer_out(slicer_out_I));

slicer slicer_Q(
	.reference_level(reference_level_Q),
	.decision_variable(Delayed_Output_Sam_Clk_Q),
	.slicer_out(slicer_out_Q));
/*************************************************************************************************************************
Output MER Measurement MAPPER
**************************************************************************************************************************/
Output_Mapper Output_Map_I(
	.slicer_out(slicer_out_I),
	.reference_level(reference_level_I),
	.mapper_out(mapper_out_I));

Output_Mapper Output_Map_Q(
	.slicer_out(slicer_out_Q),
	.reference_level(reference_level_Q),
	.mapper_out(mapper_out_Q));
/*************************************************************************************************************************
Error Chunk and Comparison
*************************************************************************************************************************/
error_chunk error_circuit_I(
	.reset(reset),
	.sys_clk(sys_clk),
	.sym_clk_enable(sym_clk_enable),
	.decision_variable(Delayed_Output_Sam_Clk_I),
	.mapper_out(mapper_out_I),
	.slicer_out(slicer_out_I),
	.q_out(q_out_I),
	.error_from_dv(error_from_dv_I),
	.sym_correct(sym_correct_I),
	.sym_error(sym_error_I));

error_chunk error_circuit_Q(
	.reset(reset),
	.sys_clk(sys_clk),
	.sym_clk_enable(sym_clk_enable),
	.decision_variable(Delayed_Output_Sam_Clk_Q),
	.mapper_out(mapper_out_Q),
	.slicer_out(slicer_out_Q),
	.q_out(q_out_Q),
	.error_from_dv(error_from_dv_Q),
	.sym_correct(sym_correct_Q),
	.sym_error(sym_error_Q));
/**************************************************************************************************************************
SQUARED ACCUMULATED ERROR CHUNK
*************************************************************************************************************************/
accumulated_squared_error_chunk Acc_Squared_Error_I(
	.sys_clk(sys_clk),
	.reset(reset),
	.error(error_from_dv_I),
	.clear_accumulator(clear_accumulator),
	.sym_clk_enable(sym_clk_enable),
	.accumulated_squared_error(accumulated_squared_error_I));

accumulated_squared_error_chunk Acc_Squared_Error_Q(
	.sys_clk(sys_clk),
	.reset(reset),
	.error(error_from_dv_Q),
	.clear_accumulator(clear_accumulator),
	.sym_clk_enable(sym_clk_enable),
	.accumulated_squared_error(accumulated_squared_error_Q));
/*************************************************************************************************************************
ACCUMULATED ERROR CHUNK
*************************************************************************************************************************/
accumulated_error_chunk Error_I(
	.sys_clk(sys_clk),
	.reset(reset),
	.sym_clk_enable(sym_clk_enable),
	.clear_accumulator(clear_accumulator),
	.error(error_from_dv_I),
	.accumulated_error(accumulated_error_I));

accumulated_error_chunk Error_Q(
	.sys_clk(sys_clk),
	.reset(reset),
	.sym_clk_enable(sym_clk_enable),
	.clear_accumulator(clear_accumulator),
	.error(error_from_dv_Q),
	.accumulated_error(accumulated_error_Q));
/***************************************************************************************************************************
END ACCUMULATED ERROR CHUNK
***************************************************************************************************************************
Fixed To Floating Point
***************************************************************************************************************************/
wire [31:0] mapper_out_power_I_Float, mapper_out_power_Q_Float;
wire [31:0] accumulated_squared_error_I_Float, accumulated_squared_error_Q_Float;

FPCONVERT  Fixed_To_Float_Mapper_Out_Power_I(
	.clock ( sys_clk ),
	.dataa ( mapper_out_power_I ),
	.result ( mapper_out_power_I_Float));

FPCONVERT  Fixed_To_Float_Accumulated_Squared_Error_I(
	.clock ( sys_clk ),
	.dataa ( accumulated_squared_error_I),
	.result ( accumulated_squared_error_I_Float));

FPCONVERT  Fixed_To_Float_Mapper_Out_Power_Q(
	.clock ( sys_clk ),
	.dataa ( mapper_out_power_Q ),
	.result ( mapper_out_power_Q_Float));

FPCONVERT  Fixed_To_Float_Accumulated_Squared_Error_Q(
	.clock ( sys_clk ),
	.dataa ( accumulated_squared_error_Q),
	.result ( accumulated_squared_error_Q_Float));

/***************************************************************************************************************************
Floating Point Division
***************************************************************************************************************************/
wire [31:0] MER_Linear_Float_I, MER_Linear_Float_Q;

FP_DIV	mapper_out_power_Divided_by_Accumulated_Squared_Error_I (
	.clock ( sys_clk ),
	.dataa ( mapper_out_power_I_Float),
	.datab ( accumulated_squared_error_I_Float),
	.result ( MER_Linear_Float_I));

FP_DIV	mapper_out_power_Divided_by_Accumulated_Squared_Error_Q (
	.clock ( sys_clk ),
	.dataa ( mapper_out_power_Q_Float),
	.datab ( accumulated_squared_error_Q_Float),
	.result ( MER_Linear_Float_Q));
/***************************************************************************************************************************
Floating Point Log Function: Keep in mind that any value on SignalTap will need to
be multiplied by 10, as this doesn't do that.
***************************************************************************************************************************/
log10 MER_dB_Float_Out_I(
	.clk(sys_clk),
	.areset(reset),
	.a(MER_Linear_Float_I),
	.q(MER_dB_Float_I));

log10 MER_dB_Float_Out_Q(
	.clk(sys_clk),
	.areset(reset),
	.a(MER_Linear_Float_Q),
	.q(MER_dB_Float_Q));
/***************************************************************************************************************************
End Log MER Circuit Parts
***************************************************************************************************************************
MER Module from Rory
***************************************************************************************************************************/
wire [10:0] MER_I, MER_Q, MER_IQ;

EE465_MER_module MER_Sanity_Check(
    .sys_clk(sys_clk),
    .sym_clk_ena(sym_clk_enable),
    .decision_I(Delayed_Output_Sam_Clk_I),
    .decision_Q(Delayed_Output_Sam_Clk_Q),
    .MER_I(MER_I),
    .MER_Q(MER_Q),
    .MER_IQ(MER_IQ));
/***************************************************************************************************************************
MER Module
***************************************************************************************************************************/
endmodule
`default_nettype wire
