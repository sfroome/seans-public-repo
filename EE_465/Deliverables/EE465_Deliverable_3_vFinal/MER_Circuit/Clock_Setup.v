/*************************************************************
EE 465 Deliverable 3 Clock Setup Module
Written by: Sean Froome
*************************************************************/
`default_nettype none
module Clock_Setup(
input wire sys_clk,
input wire reset,
output reg half_clk_enable,
output reg sam_clk_enable,
output reg sym_clk_enable);

(* noprune *) reg [3:0] four_bit_counter;
(* noprune *) reg [3:0] clk_phase;
/************************************************************
Counter and Clock Phase Setup
************************************************************/
always @ (posedge sys_clk) // Might want to flip this around.
if((four_bit_counter < 4'b1111) || reset == 1'b0)
	four_bit_counter = four_bit_counter + 4'd1;
else
	four_bit_counter = 4'd0;
always @ *
	clk_phase = four_bit_counter;
/************************************************************
Clock Enables
************************************************************/
always @ (posedge sys_clk)
	if((clk_phase == 4'd1 || clk_phase == 4'd3 || clk_phase == 4'd5 || clk_phase == 4'd7 || clk_phase == 4'd9 || clk_phase == 4'd11 || clk_phase == 4'd13 || clk_phase == 4'd15) && reset == 1'b0)
		half_clk_enable = 1'b1;
	else
		half_clk_enable = 1'b0;

always @ (posedge sys_clk)
	if((clk_phase == 4'd3 || clk_phase == 4'd7 || clk_phase == 4'd11 || clk_phase == 4'd15) && reset == 1'b0)
		sam_clk_enable = 1'b1;
	else
		sam_clk_enable = 1'b0;

always @ (posedge sys_clk)
	if(clk_phase == 4'd15 && reset == 1'b0)
		sym_clk_enable = 1'b1;
	else
		sym_clk_enable = 1'b0;
/************************************************************
End Clock Enables
************************************************************/
endmodule
`default_nettype wire
