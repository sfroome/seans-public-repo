/********************************************************
EE 465 Deliverable 2 LFSR
Written by: Sean Froome
********************************************************/

`default_nettype none
module LFSR(
  input wire sys_clk,
  input wire sam_clk_enable,
  input wire reset,
  //input wire LFSR_SET,
  //output reg signed [21:0] q,
  output reg signed [1:0] q_out_I,
  output reg signed [1:0] q_out_Q);
reg signed [21:0] q;
(* noprune *) reg signed [21:0] q_int;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    q_int = 22'd2097152;
//  else if (LFSR_SET == 1'b1)
//    q_int = 22'd2097152;
  else
    q_int = {q_int[20:0],(q_int[21]^q_int[20])};

always @ (posedge sys_clk)
  if(reset == 1'b1)
    q = 21'b0;
  else if (sam_clk_enable == 1'b1)
    q = q_int;
  else
    q = q;

always @ *
  q_out_I = q[1:0];
always @ *
  q_out_Q = q[3:2];

endmodule
`default_nettype wire
