/********************************************************
EE 465  Mapper (MER circuit)
Written by: Sean Froome
********************************************************/
`default_nettype none
module Output_Mapper (
					input wire signed [1:0] slicer_out,
					input wire signed [17:0] reference_level,
					output reg signed [17:0] mapper_out);
(* noprune *) reg signed [17:0] reference_level_a;

always @ *
	reference_level_a = reference_level >>> 1;

always @ *
		case(slicer_out)
			2'b00: begin
				mapper_out = -reference_level+(-reference_level_a); // -0.75
			end
			2'b01:begin
				mapper_out = -reference_level_a; // -0.25
			end
			2'b10: begin
				mapper_out =  reference_level+reference_level_a; //  0.75
			end
			2'b11: begin
				mapper_out =  reference_level_a; //  0.25
			end
		endcase
endmodule
`default_nettype wire
