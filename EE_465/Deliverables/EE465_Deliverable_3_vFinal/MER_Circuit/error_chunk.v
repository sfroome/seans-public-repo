/********************************************************
EE 465 Deliverable 3 MER Measurement Circuit
Written by: Sean Froome
********************************************************/
`default_nettype none
module error_chunk(
input wire reset,
input wire sym_clk_enable,
input wire sys_clk,
input wire signed [17:0] decision_variable,
input wire signed [1:0] q_out,
input wire signed [17:0] mapper_out,
input wire signed [1:0] slicer_out,
output reg signed [17:0] error_from_dv, // Called error_from_dv in top module and testbench
output reg  sym_correct,
output reg  sym_error);

(* noprune *) reg signed [1:0] delayed_q1, delayed_q2, delayed_q3;
(* noprune *) reg signed [1:0] delayed_q4, delayed_q5, delayed_q6;
(* noprune *) reg signed [1:0] delayed_q7, delayed_q8, delayed_q9;
(* noprune *) reg signed [1:0] delayed_q10;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q1 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q1 <= q_out;
  else
    delayed_q1 <= delayed_q1;
always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q2 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q2 <= delayed_q1;
  else
    delayed_q2 <= delayed_q2;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q3 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q3 <= delayed_q2;
  else
    delayed_q3 <= delayed_q3;

always @ (posedge sys_clk)
  if(reset == 1'b1)
  delayed_q4 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
  delayed_q4 <= delayed_q3;
  else
  delayed_q4 <= delayed_q4;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q5 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q5 <= delayed_q4;
  else
    delayed_q5 <= delayed_q5;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q6 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q6 <= delayed_q5;
  else
    delayed_q6 <= delayed_q6;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q7 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q7 <= delayed_q6;
  else
    delayed_q7 <= delayed_q7;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q8 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q8 <= delayed_q7;
  else
    delayed_q8 <= delayed_q8;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q9 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q9 <= delayed_q8;
  else
    delayed_q9 <= delayed_q9;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q10 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q10 <= delayed_q9;
  else
    delayed_q10 <= delayed_q10;


always @ (posedge sys_clk)
  if(reset == 1'b1)
    error_from_dv = 18'd0; // Called error_from_dv in top module and testbench
  else if(sym_clk_enable == 1'b1)
    error_from_dv = decision_variable - mapper_out;
  else
    error_from_dv = error_from_dv;

always @ (posedge sys_clk)
	if( delayed_q10 == slicer_out)
		sym_correct = 1'b1;
	else
		sym_correct = 1'b0;

always @ *
	sym_error = ~sym_correct;

endmodule
`default_nettype wire
