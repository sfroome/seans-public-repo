/*************************************************************************************************************************
EE 465 Deliverable 3 Top Module.
Written by: Sean Froome

This is the top module of the Deliverable. The Deliverable contains an MER
measurement circuit, three filters, as well as a module to upsampled and pass
the transmit filter outputs to the DAC for viewing on the Spectrum Analyzer.

CURRENT SWITCH ASSIGNMENTS:
RESET =  SW[17]

To Control The Delays:
I Select is SW[9:6];
Q Select is SW[13:10];

SW[16:15]
00 ---> Gold TX I; 01 ---> Prac TX I
10 ---> Gold TX Q; 11 ---> Prac TX Q

Switch 14 determines whether the gold filters or the practical filters
get sent forward through the Communication system (ie: to Deliverable 4 crap)
If high (1'b1), the practical filters are sent through.
If low (1'b1), the gold filters are used.

Not really setup currently:
Gain Control for I is SW[1:0]
Gain Control for Q is SW[3:2]
*************************************************************************************************************************/
`default_nettype none
module EE465_Deliverable_3(
		input wire clock_50,
		input wire [17:0]SW,
		input wire [3:0] KEY,
		output reg [3:0]  LEDG,
		output reg [17:0] LEDR,
		input wire [13:0]ADC_DA,
		input wire[13:0]ADC_DB,
		output reg[13:0]DAC_DA,
		output reg [13:0]DAC_DB,
		output	wire ADC_CLK_A,
		output	wire ADC_CLK_B,
		output	wire ADC_OEB_A,
		output	wire ADC_OEB_B,
		output	wire DAC_CLK_A,
		output	wire DAC_CLK_B,
		output	wire DAC_MODE,
		output	wire DAC_WRT_A,
		output	wire DAC_WRT_B);
/************************************************************************************************************************
Tie LEDs to Switches and Keys, to make it clear when the switches and keys are active
************************************************************************************************************************/
always @ *
	LEDR = SW;
always @ *
	LEDG = KEY;

reg reset;
always @ *
	reset = SW[17];
/************************************************************************************************************************
Wires for LFSRs, clocks and DACs
************************************************************************************************************************/
wire signed [1:0]  q_out_I;
wire signed [1:0]  q_out_Q;
//assign q_out_Q = q_out_I; // Since I only really need one channel right now....

wire [3:0] I_Select, Q_Select;

assign I_Select = SW[9:6];
assign Q_Select = SW[13:10];

wire sys_clk, half_clk_enable, sam_clk_enable, sym_clk_enable;
wire signed [17:0]srrc_out, srrc_input;
reg signed [17:0] output_from_filter;

//wire unsigned [13:0] output_to_DAC;
/**************************************************************************************************************
				Setup DACs
***************************************************************************************************************/
//(* keep *) wire signed [13:0]	sin_out;
reg [4:0] NCO_freq;	// unsigned fraction with units cycles/sample
(* noprune *)reg [13:0] registered_ADC_A;
(* noprune *)reg [13:0] registered_ADC_B;
wire [13:0] DAC_out;

assign DAC_CLK_A = sys_clk;
assign DAC_CLK_B = sys_clk;
assign DAC_MODE = 1'b1; //treat DACs seperately
assign DAC_WRT_A = ~sys_clk;
assign DAC_WRT_B = ~sys_clk;

always@ (posedge sys_clk)// make DAC A echo ADC A
	DAC_DA = registered_ADC_A[13:0]; // so DAC_DA and DAC_DB are the signals to the DAC.
always@ (posedge sys_clk)
	DAC_DB = DAC_out;
/***********************************************************************************************************
End DAC setup
************************************************************************************************************
Setup ADCs
************************************************************************************************************/
assign ADC_CLK_A = sys_clk;
assign ADC_CLK_B = sys_clk;
assign ADC_OEB_A = 1'b1;
assign ADC_OEB_B = 1'b1;
always@ (posedge sys_clk)
	registered_ADC_A <= ADC_DA;
always@ (posedge sys_clk)
	registered_ADC_B <= ADC_DB;
/***********************************************************************************************************
End ADC setup
************************************************************************************************************
LFSR and Clocks Setup
************************************************************************************************************/
EE465_filter_test SRRC_test(
	.clock_50(clock_50),
 .reset(reset),
 .output_from_filter_1s17(output_from_filter),
// .lfsr_value(q_out_I),
 .symbol_clk_ena(sym_clk_enable),
 .sample_clk_ena(sam_clk_enable),
 .system_clk(sys_clk),
 .output_to_DAC(DAC_out));

LFSR LFSR1(
  .sys_clk(sys_clk),
  .sam_clk_enable(sam_clk_enable),
  .reset(reset),
  .q_out_I(q_out_I),
  .q_out_Q(q_out_Q));
/*************************************************************************************************************************
Filter Instantiation
**************************************************************************************************************************/
wire signed [17:0] output_gold_tx_I, output_prac_tx_I, output_gold_rx_I;
wire signed [17:0] output_gold_tx_Q, output_prac_tx_Q, output_gold_rx_Q;
reg signed [17:0] output_from_filter_I, output_from_filter_Q;

always @ *
	if(SW[16:15] == 2'd0)
		output_from_filter = output_gold_tx_I;
	else if (SW[16:15] == 2'd1)
		output_from_filter = output_prac_tx_I;
	else if(SW[16:15] == 2'd2)
		output_from_filter = output_gold_tx_Q;
	else // if (SW[16:15] == 2'd3)
		output_from_filter = output_prac_tx_Q;

always @ *
	if(SW[14] == 1'd0)
		output_from_filter_I = output_gold_tx_I;
	else //if (SW[14] == 1'd1)
		output_from_filter_I = output_prac_tx_I;
always @ *
	if(SW[14] == 1'd0)
		output_from_filter_Q = output_gold_tx_Q;
	else //if (SW[14] == 1'd1)
		output_from_filter_Q = output_prac_tx_Q;
/*************************************************************************************************************************
Filter Instantiation
**************************************************************************************************************************/
TX_PRAC TX_Filter_Practical_I(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.sym_clk_enable(sym_clk_enable),
	.x_in(q_out_I),
	.y_out(output_prac_tx_I));

TX_Gold TX_Filter_Gold_I(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.sym_clk_enable(sym_clk_enable),
	.x_in(q_out_I),
	.y_out(output_gold_tx_I));

RX_Gold RX_Filter_Gold_I(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.x_in(output_from_filter_I),
	.y_out(output_gold_rx_I));

TX_PRAC TX_Filter_Practical_Q(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.sym_clk_enable(sym_clk_enable),
	.x_in(q_out_Q),
	.y_out(output_prac_tx_Q));

TX_Gold TX_Filter_Gold_Q(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.sym_clk_enable(sym_clk_enable),
	.x_in(q_out_Q),
	.y_out(output_gold_tx_Q));

RX_Gold RX_Filter_Gold_Q(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.x_in(output_from_filter_Q),
	.y_out(output_gold_rx_Q));
/***************************************************************************************************************************
MER Module
***************************************************************************************************************************/
wire [31:0] MER_dB_Float_I;
wire [31:0] MER_dB_Float_Q;

MER_Circuit MER_Measurement(
.sys_clk(sys_clk),
.sam_clk_enable(sam_clk_enable),
.sym_clk_enable(sym_clk_enable),
.reset(reset),
.I_Select(I_Select),
.Q_Select(Q_Select),
.output_gold_rx_I(output_gold_rx_I),
.output_gold_rx_Q(output_gold_rx_Q),
.q_out_I(q_out_I),
.q_out_Q(q_out_Q),
.MER_dB_Float_I(MER_dB_Float_I),
.MER_dB_Float_Q(MER_dB_Float_Q)
);

endmodule
`default_nettype wire
