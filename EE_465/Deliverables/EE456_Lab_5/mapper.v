/********************************************************
EE 456 Lab 5 Mapper 
Sean Froome
********************************************************/
module mapper (input wire [1:0]symbol,
					output reg signed [17:0]value);
/*********************************************************					
MAPPER: 	Takes a 2-bit symbol value and converts the values
			into a 1s17 "voltage" for the I or Q constellation
			level.
**********************************************************
I represents the two MSB's, whereas Q represents the two LSB's.
See Figure 2 in the lab manual and this will make sense.
***********************************************************/
always @ * 
		case(symbol)		
			2'b00: begin 
				value = -18'd98304; // -0.75
			end
			2'b01:begin
				value = -18'd32768; // -0.25
			end
			2'b10: begin 
				value =  18'd98304; //  0.75
			end
			2'b11: begin
				value =  18'd32768; //  0.25
			end
		endcase
endmodule
