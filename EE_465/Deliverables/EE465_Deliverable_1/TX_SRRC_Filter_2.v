/*******************************************************************************************
EE 465 Deliverable 1 TX SRRC Filter 2 (the one without multipliers)
Written by: Sean Froome

Lookup Table used in place of multipliers. Lookup table now
apart of the module instead of being instantiated separately.

The Multiplierless filter was not updated with the improved filter.
Therefore, it has an MER of 18.6031, the same as TX1a, and not TX1
(the one I built a few hours before the lab exam because I was concerned about
the stopband attenuation being slightly less than 40dB).
*******************************************************************************************/
`default_nettype none
module TX_SRRC_Filter_2(
input wire sys_clk,
//input wire reset,
input wire signed [17:0] x_in,
output reg signed [17:0] y_out);
/******************************************************************************************
Declaring the Levels of the Filter
*******************************************************************************************/
reg signed [17:0] x0,x1,x2,x3,x4,x5,x6,x7,x8,x9;
reg signed [17:0] x10,x11,x12,x13,x14,x15,x16;
reg signed  [17:0] summation_out;

reg signed [17:0] multiplier_out0, multiplier_out1, multiplier_out2, multiplier_out3;
reg signed [17:0] multiplier_out4, multiplier_out5, multiplier_out6, multiplier_out7;
reg signed [17:0] multiplier_out8,multiplier_out9, multiplier_out10, multiplier_out11;
reg signed [17:0] multiplier_out12,multiplier_out13, multiplier_out14, multiplier_out15;
reg signed [17:0] multiplier_out16;
/*******************************************************************************************
End Top Multiplier Output Declarations
********************************************************************************************
Top Level Registers
********************************************************************************************/
always @ (posedge sys_clk)
				x0 <= x_in;
always @ (posedge sys_clk)
				x1 <= x0;
always @ (posedge sys_clk)
				x2 <= x1;
always @ (posedge sys_clk)
				x3 <= x2;
always @ (posedge sys_clk)
				x4 <= x3;
always @ (posedge sys_clk)
				x5 <= x4;
always @ (posedge sys_clk)
				x6 <= x5;
always @ (posedge sys_clk)
				x7 <= x6;
always @ (posedge sys_clk)
				x8 <= x7;
always @ (posedge sys_clk)
				x9 <= x8;
always @ (posedge sys_clk)
				x10 <= x9;
always @ (posedge sys_clk)
				x11 <= x10;
always @ (posedge sys_clk)
				x12 <= x11;
always @ (posedge sys_clk)
				x13 <= x12;
always @ (posedge sys_clk)
				x14 <= x13;
always @ (posedge sys_clk)
				x15 <= x14;
always @ (posedge sys_clk)
				x16 <= x15;
/*****************************************************************************************
End Top Level Registers
******************************************************************************************
Declaring Multiplier Outputs
*****************************************************************************************/
always @ *
  case(x0)
    -18'd98304: multiplier_out0 = -18'sd175;
    -18'd32768: multiplier_out0 = -18'sd58;
     18'd32768: multiplier_out0 =  18'sd58;
     18'd98304: multiplier_out0 =  18'sd175;
     default: multiplier_out0 = 18'sd0;
  endcase

always @ *
  case(x1)
    -18'd98304: multiplier_out1 =  18'sd491;
    -18'd32768: multiplier_out1 =  18'sd164;
     18'd32768: multiplier_out1 = -18'sd164;
     18'd98304: multiplier_out1 = -18'sd491;
    default: multiplier_out1 = 18'sd0;
  endcase

always @ *
  case(x2)
    -18'd98304: multiplier_out2 =  18'sd1695;
    -18'd32768: multiplier_out2 =  18'sd565;
     18'd32768: multiplier_out2 = -18'sd565;
     18'd98304: multiplier_out2 = -18'sd1695;
     default: multiplier_out2 = 18'sd0;
  endcase

always @ *
  case(x3)
    -18'd98304: multiplier_out3 =  18'sd2331;
    -18'd32768: multiplier_out3 =  18'sd777;
     18'd32768: multiplier_out3 = -18'sd777;
     18'd98304: multiplier_out3 = -18'sd2331;
     default: multiplier_out3 = 18'sd0;
  endcase

always @ *
  case(x4)
    -18'd98304: multiplier_out4 =  18'sd694;
    -18'd32768: multiplier_out4 =  18'sd231;
     18'd32768: multiplier_out4 = -18'sd231;
     18'd98304: multiplier_out4 = -18'sd694;
     default: multiplier_out4 = 18'sd0;
  endcase

always @ *
  case(x5)
    -18'd98304: multiplier_out5 = -18'sd4204;
    -18'd32768: multiplier_out5 = -18'sd1401;
     18'd32768: multiplier_out5 =  18'sd1401;
     18'd98304: multiplier_out5 =  18'sd4204;
     default: multiplier_out5 = 18'sd0;
  endcase

always @ *
  case(x6)
    -18'd98304: multiplier_out6 = -18'sd11380;
    -18'd32768: multiplier_out6 = -18'sd3793;
     18'd32768: multiplier_out6 =  18'sd3793;
     18'd98304: multiplier_out6 =  18'sd11380;
     default: multiplier_out6 = 18'sd0;
  endcase

always @ *
  case(x7)
    -18'd98304: multiplier_out7 = -18'sd17907;
    -18'd32768: multiplier_out7 = -18'sd5969;
     18'd32768: multiplier_out7 =  18'sd5969;
     18'd98304: multiplier_out7 =  18'sd17907;
     default: multiplier_out7 = 18'sd0;
  endcase

always @ *
  case(x8)
    -18'd98304: multiplier_out8 = -18'sd20551;
    -18'd32768: multiplier_out8 = -18'sd6850;
     18'd32768: multiplier_out8 =  18'sd6850;
     18'd98304: multiplier_out8 =  18'sd20551;
     default: multiplier_out8 = 18'sd0;
  endcase

always @ *
  case(x9)
    -18'd98304: multiplier_out9 = -18'sd17907;
    -18'd32768: multiplier_out9 = -18'sd5969;
     18'd32768: multiplier_out9 =  18'sd5969;
     18'd98304: multiplier_out9 =  18'sd17907;
     default: multiplier_out9 = 18'sd0;
  endcase

always @ *
  case(x10)
    -18'd98304: multiplier_out10 = -18'sd11380;
    -18'd32768: multiplier_out10 = -18'sd3793;
     18'd32768: multiplier_out10 =  18'sd3793;
     18'd98304: multiplier_out10 =  18'sd11380;
     default: multiplier_out10 = 18'sd0;
  endcase

always @ *
  case(x11)
    -18'd98304: multiplier_out11 = -18'sd4204;
    -18'd32768: multiplier_out11 = -18'sd1401;
     18'd32768: multiplier_out11 =  18'sd1401;
     18'd98304: multiplier_out11 =  18'sd4204;
     default: multiplier_out11 = 18'sd0;
  endcase

always @ *
  case(x12)
    -18'd98304: multiplier_out12 =  18'sd694;
    -18'd32768: multiplier_out12 =  18'sd231;
     18'd32768: multiplier_out12 = -18'sd231;
     18'd98304: multiplier_out12 = -18'sd694;
     default: multiplier_out12 = 18'sd0;
  endcase

always @ *
  case(x13)
    -18'd98304: multiplier_out13 =  18'sd2331;
    -18'd32768: multiplier_out13 =  18'sd777;
     18'd32768: multiplier_out13 = -18'sd777;
     18'd98304: multiplier_out13 = -18'sd2331;
     default: multiplier_out13 = 18'sd0;
  endcase

always @ *
  case(x14)
    -18'd98304: multiplier_out14 =  18'sd1695;
    -18'd32768: multiplier_out14 =  18'sd565;
     18'd32768: multiplier_out14 = -18'sd565;
     18'd98304: multiplier_out14 = -18'sd1695;
     default: multiplier_out14 = 18'sd0;
  endcase

always @ *
  case(x15)
    -18'd98304: multiplier_out15 =  18'sd491;
    -18'd32768: multiplier_out15 =  18'sd164;
     18'd32768: multiplier_out15 = -18'sd164;
     18'd98304: multiplier_out15 = -18'sd491;
     default: multiplier_out15 = 18'sd0;
  endcase

always @ *
  case(x16)
    -18'd98304: multiplier_out16 = -18'sd175;
    -18'd32768: multiplier_out16 = -18'sd58;
     18'd32768: multiplier_out16 =  18'sd58;
     18'd98304: multiplier_out16 =  18'sd175;
     default: multiplier_out16 = 18'sd0;
  endcase
/************************************************************
End Declaring Multiplier Outputs
************************************************************
Declaring Summations
************************************************************/
always @ *
summation_out = multiplier_out16+multiplier_out15 +multiplier_out14
							+multiplier_out13 +multiplier_out12 +multiplier_out11
							+multiplier_out10 +multiplier_out9  +multiplier_out8
							+multiplier_out7  +multiplier_out6  +multiplier_out5
							+multiplier_out4  +multiplier_out3  +multiplier_out2
							+multiplier_out1  +multiplier_out0;
/************************************************************
End Declaring Summations
************************************************************
Output Assignment
************************************************************/
always @ *
y_out = summation_out;
/************************************************************
End Output Assignment
************************************************************/
endmodule
`default_nettype wire

/*****************************************************************************************
OLD LUT BLOCK INSTANTIATION - CAN BE DELETED
******************************************************************************************
LUT multiplierless_multiplier_output(
		.x0(x0),.x1(x1),.x2(x2),.x3(x3),.x4(x4),.x5(x5),.x6(x6),
		.x7(x7),.x8(x8),.x9(x9),.x10(x10),.x11(x11),.x12(x12),
		.x13(x13),.x14(x14),.x15(x15),.x16(x16),
		.multiplier_out0(multiplier_out0),
		.multiplier_out1(multiplier_out1),
		.multiplier_out2(multiplier_out2),
		.multiplier_out3(multiplier_out3),
		.multiplier_out4(multiplier_out4),
		.multiplier_out5(multiplier_out5),
		.multiplier_out6(multiplier_out6),
		.multiplier_out7(multiplier_out7),
		.multiplier_out8(multiplier_out8),
		.multiplier_out9(multiplier_out9),
		.multiplier_out10(multiplier_out10),
		.multiplier_out11(multiplier_out11),
		.multiplier_out12(multiplier_out12),
		.multiplier_out13(multiplier_out13),
		.multiplier_out14(multiplier_out14),
		.multiplier_out15(multiplier_out15),
		.multiplier_out16(multiplier_out16));
		/*****************************************************************************************
		OLD LUT BLOCK INSTANTIATION - CAN BE DELETED
		*******************************************************************************************/
