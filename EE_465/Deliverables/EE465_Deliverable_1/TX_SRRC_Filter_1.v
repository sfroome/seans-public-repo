/*************************************************************
EE 465 Deliverable 1 TX_SRRC Filter 1

This filter has a slightly worse MER (16dB), but bottoms out closer to
0.2 cycles per sample. Don't bother using it unless absolutely necessary.
Written by: Sean Froome
*************************************************************/

`default_nettype none
module TX_SRRC_Filter_1(
input wire sys_clk,
//input wire reset,
input wire signed [17:0] x_in,
output reg signed [17:0] y_out);
integer i = 0;
/*************************************************************
DECLARING AND ASSIGNING FILTER COEFFICIENTS
**************************************************************
Coefficients were scaled to handle any 1s17 number
**************************************************************
MER of 16.64dB
**************************************************************
wire signed [17:0] b[8:0];
assign b[0] =  18'sd30;
assign b[1] = -18'sd943;
assign b[2] = -18'sd2388;
assign b[3] = -18'sd2783;
assign b[4] = -18'sd128;
assign b[5] =  18'sd6492;
assign b[6] =  18'sd15624;
assign b[7] =  18'sd23690;
assign b[8] =  18'sd26916;
************************************************************
MER of 18.1329
************************************************************/
wire signed [17:0] b[8:0];
assign b[0] =  18'sd198;
assign b[1] = -18'sd753;
assign b[2] = -18'sd2356;
assign b[3] = -18'sd3091;
assign b[4] = -18'sd759;
assign b[5] =  18'sd5813;
assign b[6] =  18'sd15246;
assign b[7] =  18'sd23739;
assign b[8] =  18'sd27163;
/************************************************************
REGISTER DECLARATIONS
************************************************************/
reg signed [17:0] x[16:0];
reg signed [17:0] sum_level_1[8:0];
reg signed [35:0] multiplier_out[8:0];
reg signed [17:0] multiplier_out_t[8:0];
//reg signed [35:0] multiplier_out[16:0];
//reg signed [17:0] multiplier_out_t[16:0];
reg signed [17:0] sum_level_2[4:0];
reg signed [17:0] sum_level_3[2:0];
reg signed [17:0] sum_level_4;
/*************************************************************
Top Level Registers
************************************************************/
always @ (posedge sys_clk)
				x[0] <= {x_in[17], x_in[17:1]};
always @ (posedge sys_clk)
		begin
			for(i=1; i<17;i=i+1)
				x[i] <= x[i-1];
		end

always @ *//(posedge sys_clk)
		begin
			for(i=0;i<=7;i=i+1)
				sum_level_1[i] = x[i]+x[16-i]; //2s17 - need room to add both coefficients togther
		end
always@ *//(posedge sys_clk)
	 sum_level_1[8] = x[8]; //2s17

always @ *
	 	begin
	 		for(i=0;i <= 8;i=i+1)
	 			multiplier_out[i] = b[i] * sum_level_1[i];
	 	end
always @ *
	begin
		for(i=0;i <= 8;i=i+1)
			multiplier_out_t[i] = multiplier_out[i][33:16];
			// Due to the additional round of additions, you will need to take off 3 bits instead
			// in order to make this one look like the 17-multiplier filter.
	 	end

always @ *//(posedge sys_clk)
		begin
			for(i=0;i<4;i=i+1)
				sum_level_2[i] = multiplier_out_t[i] + multiplier_out_t[8-i];
 		end
always @ *
		sum_level_2[4] = multiplier_out_t[4];

always @ *//(posedge sys_clk)
		begin
			for(i=0;i<2;i=i+1)
				sum_level_3[i] = sum_level_2[i] + sum_level_2[4-i];
		end
always @ *
	sum_level_3[2] = sum_level_2[2];

always @ * //(posedge sys_clk)
	begin
		sum_level_4 = sum_level_3[2]+ sum_level_3[1]+ sum_level_3[0];
	end
always @ (posedge sys_clk)
	y_out = sum_level_4;

endmodule
`default_nettype wire
