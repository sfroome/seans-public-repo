/**********************************************************************
EE465 Deliverable 1 Testbench.
Sean Froome
Originally used for Deliverable 0 and the demo testbench.
**********************************************************************/
module test_tb;
reg sys_clk;
reg reset;
reg [17:0] x_in;

reg [17:0] x_in_MER;

reg[17:0] x_in_multiless;

reg [8:0] counter;
//reg [7:0] counter_MER;
reg [8:0] counter_multiless;

wire [17:0] y_out_RCV;
wire [17:0] y_out_TX1;
wire [17:0] y_out_TX1a;
wire [17:0] y_out_TX2;
wire [17:0] y_out_MER;

localparam PERIOD = 10;
localparam RESET_DELAY = 2;
localparam RESET_LENGTH = 10;

/**************************************************
 GENERATE CLOCKS
***************************************************/
initial
  begin
    sys_clk = 0;
    forever
      begin
        #(PERIOD/2);
        sys_clk = ~sys_clk;
      end
  end
/****************************************************
COUNTER SETUP
*****************************************************/
always @(posedge sys_clk)
  if(reset)
    begin
      counter = 8'd0;
    end
  else if (counter == 7'd80)
// else if (counter == 7'd18)
    begin
        counter  = 8'd1;
    end
  else
    begin
        counter = counter + 8'd1;
    end

  always @(posedge sys_clk)
    if(reset)
      begin
        counter_multiless = 8'd0;
      end
    else if (counter_multiless == 8'd80)
      begin
        counter_multiless  = 8'd1;
      end
    else
      begin
        counter_multiless = counter_multiless + 8'd1;
      end
/************************************************
RESET GENERATION
*************************************************/
initial
  begin
    reset = 0;
    #(RESET_DELAY);
    reset = 1;
    #(RESET_LENGTH);
    reset = 0;
  end
/***************************************************
IMPULSE RESPONSE TEST
**************************************************/
always @ (posedge sys_clk)
  if(reset)
    begin
      x_in = 18'd0;
    end
  else if (counter == 7'd1 && reset == 1'b0)
    begin
      x_in = 18'h1FFFF;
    end
  else
    begin
      x_in = 18'd0;
    end
/***********************************************
            END IMPULSE RESPONSE TEST
***********************************************
WORST CASE INPUT TEST
************************************************
always @ (posedge sys_clk)
if(reset)
begin
x_in = 18'd0;
end
else if (counter == 8'd1) //x0
begin
x_in = 18'h1FFFF;
end
else if (counter == 8'd2) //x1
begin
x_in = 18'h20000;
end
else if (counter == 8'd3) //x2
begin
x_in = 18'h20000;
end
else if (counter == 8'd4) //x3
begin
x_in = 18'h20000;
end
else if (counter == 8'd5) //x4
begin
x_in = 18'h20000;
end
else if (counter == 8'd6) //x5
begin
x_in = 18'h1FFFF;
end
else if (counter == 8'd7) //x6
begin
x_in = 18'h1FFFF;
end
else if (counter == 8'd8) //x7
begin
x_in = 18'h1FFFF;
end
else if (counter == 8'd9) //x8 (AKA MAX/MIDDLE POINT)
begin
x_in = 18'h1FFFF;
end
else if (counter == 7'd10) //x9
begin
x_in = 18'h1FFFF;
end
else if (counter == 7'd11) //x10
begin
x_in = 18'h1FFFF;
end
else if (counter == 7'd12) //x11
begin
x_in = 18'h1FFFF;
end
else if (counter == 7'd13) //x12
begin
x_in = 18'h20000;
end
else if (counter == 7'd14) //x13
begin
x_in = 18'h20000;
end
else if (counter == 7'd15) //x14
begin
x_in = 18'h20000;
end
else if (counter == 7'd16) //x15
begin
x_in = 18'h20000;
end
else if (counter == 7'd17) //x16
begin
x_in = 18'h1FFFF;
end
else
begin
x_in = 18'd0;
end
/***********************************************
END WORST INPUT CASE TEST
************************************************
MER TESTING
************************************************/
always @ *
//x_in_MER = y_out_TX1a;
x_in_MER = y_out_TX1a;
/***********************************************
MULTIPLIERLESS TEST
**********************************************/
always @ (posedge sys_clk)
  if(reset)
      begin
        x_in_multiless = 18'd0;
      end
  else if (counter_multiless == 7'd1 && reset == 1'b0)
      begin
        x_in_multiless = -18'd98304;
        //x_in_multiless = x_in;
      end
  else if (counter_multiless == 7'd20 && reset == 1'b0)
      begin
        x_in_multiless = -18'd32768;
      end
  else if (counter_multiless == 7'd40 && reset == 1'b0)
      begin
        x_in_multiless =  18'd32768;
      end
  else if (counter_multiless == 7'd60 && reset == 1'b0)
      begin
        x_in_multiless =  18'd98304;
      end
  else
    begin
      x_in_multiless = 18'd0;
    end
/***********************************************
FILTER INSTANTIATIONS
*************************************************
RCV Filter
*************************************************/
RCV_SRRC_Filter filter1(
  .sys_clk(sys_clk),
  .x_in(x_in),
  .y_out(y_out_RCV));
/**********************************************************
TX FILTER 1 (MER OF 18.1329dB)
**********************************************************/
TX_SRRC_Filter_1 filter2(
   .sys_clk(sys_clk),
   .x_in(x_in),
   .y_out(y_out_TX1));
/**********************************************************
    TX FILTER 1a (MER OF 18.6031dB)
**********************************************************/
TX_SRRC_Filter_1a filter2a(
    .sys_clk(sys_clk),
    .x_in(x_in),
    .y_out(y_out_TX1a));
/**********************************************************
TX FILTER 2 (aka no multipliers filter)
**********************************************************/
TX_SRRC_Filter_2 filter3(
    .sys_clk(sys_clk),
    .x_in(x_in_multiless),
    .y_out(y_out_TX2));
/**********************************************************
RCV MER TEST TODO:( I will need to make a separate filter to test this properly,
even if it effectively has the same coefficients and overall structure.)
**********************************************************/
RCV_SRRC_Filter MER_TEST(
    .sys_clk(sys_clk),
    .x_in(x_in_MER),
    .y_out(y_out_MER));
/**********************************************************
END OF FILTER INSTANTIATIONS
**********************************************************/
endmodule



/*****************************************************
MER TEST. (Doesn't Work)
*****************************************************
always @ (posedge sys_clk)
  if(reset)
    begin
      x_in_MER = 18'd0;
    end
  else if (counter_MER > 7'd0 && counter_MER < 7'd64 && reset == 1'b0)
    begin
      x_in_MER = y_out_TX1a;
    end
  else
    begin
      x_in_MER = 18'd0;
    end


    /*
    always @(posedge sys_clk)
      if(reset)
        begin
          counter_MER = 7'd0;
        end
      else if (counter_MER == 7'd64)
        begin
          counter_MER  = 7'd1;
        end
      else
        begin
          counter_MER = counter_MER + 7'd1;
        end
    */
