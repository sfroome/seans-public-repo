onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix decimal /test_tb/sys_clk
add wave -noupdate -radix decimal /test_tb/reset
add wave -noupdate -format Analog-Step -height 84 -max 131071.00000000001 -radix decimal /test_tb/x_in
add wave -noupdate -format Analog-Step -height 84 -max 98304.0 -min -98304.0 -radix decimal /test_tb/x_in_multiless
add wave -noupdate -format Analog-Step -height 84 -max 27162.0 -min -3091.0 -radix decimal /test_tb/x_in_MER
add wave -noupdate -format Analog-Step -height 84 -max 24320.999999999996 -min -4524.0 -radix decimal /test_tb/y_out_RCV
add wave -noupdate -format Analog-Step -height 84 -max 27399.999999999996 -min -3108.0 -radix decimal /test_tb/y_out_TX1a
add wave -noupdate -format Analog-Step -height 84 -max 131071.00000000001 -min -131072.0 /test_tb/filter2/x_in
add wave -noupdate -format Analog-Step -height 84 -max 27162.0 -min -3091.0 /test_tb/filter2/y_out
add wave -noupdate -format Analog-Step -height 84 -max 20551.0 -min -20551.0 -radix decimal -childformat {{{/test_tb/y_out_TX2[17]} -radix decimal} {{/test_tb/y_out_TX2[16]} -radix decimal} {{/test_tb/y_out_TX2[15]} -radix decimal} {{/test_tb/y_out_TX2[14]} -radix decimal} {{/test_tb/y_out_TX2[13]} -radix decimal} {{/test_tb/y_out_TX2[12]} -radix decimal} {{/test_tb/y_out_TX2[11]} -radix decimal} {{/test_tb/y_out_TX2[10]} -radix decimal} {{/test_tb/y_out_TX2[9]} -radix decimal} {{/test_tb/y_out_TX2[8]} -radix decimal} {{/test_tb/y_out_TX2[7]} -radix decimal} {{/test_tb/y_out_TX2[6]} -radix decimal} {{/test_tb/y_out_TX2[5]} -radix decimal} {{/test_tb/y_out_TX2[4]} -radix decimal} {{/test_tb/y_out_TX2[3]} -radix decimal} {{/test_tb/y_out_TX2[2]} -radix decimal} {{/test_tb/y_out_TX2[1]} -radix decimal} {{/test_tb/y_out_TX2[0]} -radix decimal}} -subitemconfig {{/test_tb/y_out_TX2[17]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[16]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[15]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[14]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[13]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[12]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[11]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[10]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[9]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[8]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[7]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[6]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[5]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[4]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[3]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[2]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[1]} {-height 16 -radix decimal} {/test_tb/y_out_TX2[0]} {-height 16 -radix decimal}} /test_tb/y_out_TX2
add wave -noupdate -format Analog-Step -height 84 -max 80.0 -radix decimal /test_tb/counter_multiless
add wave -noupdate -format Analog-Step -height 84 -max 16970.0 -min -2772.0 -radix decimal /test_tb/y_out_MER
add wave -noupdate -format Analog-Step -height 84 -max 80.0 -radix decimal /test_tb/counter
add wave -noupdate -format Analog-Step -height 84 -max 27162.0 -min -3091.0 /test_tb/MER_TEST/x_in
add wave -noupdate -format Analog-Step -height 84 -max 16970.0 -min -2772.0 /test_tb/MER_TEST/y_out
add wave -noupdate -radix decimal /test_tb/MER_TEST/sys_clk
add wave -noupdate /test_tb/MER_TEST/i
add wave -noupdate /test_tb/MER_TEST/b
add wave -noupdate /test_tb/MER_TEST/summation_out
add wave -noupdate -radix decimal /test_tb/filter1/sys_clk
add wave -noupdate -format Analog-Step -height 84 -max 131071.00000000001 -min -131072.0 -radix decimal /test_tb/filter1/x_in
add wave -noupdate -format Analog-Step -height 84 -max 131047.0 -min -66952.0 -radix decimal /test_tb/filter1/y_out
add wave -noupdate -radix decimal /test_tb/filter1/b
add wave -noupdate -radix decimal /test_tb/filter1/summation_out
add wave -noupdate /test_tb/filter2/sys_clk
add wave -noupdate /test_tb/filter2/i
add wave -noupdate /test_tb/filter2/b
add wave -noupdate /test_tb/filter2/sum_level_4
add wave -noupdate -radix decimal /test_tb/filter2a/sys_clk
add wave -noupdate -format Analog-Step -height 84 -max 131071.00000000001 -min -131072.0 -radix decimal /test_tb/filter2a/x_in
add wave -noupdate -format Analog-Step -height 84 -max 131063.99999999999 -min -124069.0 -radix decimal /test_tb/filter2a/y_out
add wave -noupdate -radix decimal /test_tb/filter2a/i
add wave -noupdate -radix decimal /test_tb/filter2a/b
add wave -noupdate -radix decimal /test_tb/filter2a/sum_level_4
add wave -noupdate /test_tb/filter3/sys_clk
add wave -noupdate /test_tb/filter3/sys_clk
add wave -noupdate /test_tb/filter3/x_in
add wave -noupdate -format Analog-Step -height 74 -max 22796.0 -min -22796.0 /test_tb/filter3/y_out
add wave -noupdate /test_tb/filter3/x0
add wave -noupdate /test_tb/filter3/x1
add wave -noupdate /test_tb/filter3/x2
add wave -noupdate /test_tb/filter3/x3
add wave -noupdate /test_tb/filter3/x4
add wave -noupdate /test_tb/filter3/x5
add wave -noupdate /test_tb/filter3/x6
add wave -noupdate /test_tb/filter3/x7
add wave -noupdate /test_tb/filter3/x8
add wave -noupdate /test_tb/filter3/x9
add wave -noupdate /test_tb/filter3/x10
add wave -noupdate /test_tb/filter3/x11
add wave -noupdate /test_tb/filter3/x12
add wave -noupdate /test_tb/filter3/x13
add wave -noupdate /test_tb/filter3/x14
add wave -noupdate /test_tb/filter3/x15
add wave -noupdate /test_tb/filter3/x16
add wave -noupdate /test_tb/filter3/summation_out
add wave -noupdate /test_tb/filter3/multiplier_out0
add wave -noupdate /test_tb/filter3/multiplier_out1
add wave -noupdate /test_tb/filter3/multiplier_out2
add wave -noupdate /test_tb/filter3/multiplier_out3
add wave -noupdate /test_tb/filter3/multiplier_out4
add wave -noupdate /test_tb/filter3/multiplier_out5
add wave -noupdate /test_tb/filter3/multiplier_out6
add wave -noupdate /test_tb/filter3/multiplier_out7
add wave -noupdate /test_tb/filter3/multiplier_out8
add wave -noupdate /test_tb/filter3/multiplier_out9
add wave -noupdate /test_tb/filter3/multiplier_out10
add wave -noupdate /test_tb/filter3/multiplier_out11
add wave -noupdate /test_tb/filter3/multiplier_out12
add wave -noupdate /test_tb/filter3/multiplier_out13
add wave -noupdate /test_tb/filter3/multiplier_out14
add wave -noupdate /test_tb/filter3/multiplier_out15
add wave -noupdate /test_tb/filter3/multiplier_out16
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {925 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 225
configure wave -valuecolwidth 195
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {200 ns} {3148 ns}
