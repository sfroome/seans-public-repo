
/*************************************************************
EE 465 Deliverable 1 RCV Filter
Written by: Sean Froome
*************************************************************/
`default_nettype none
module RCV_SRRC_Filter(
input wire sys_clk,
//input wire reset,
input wire signed [17:0] x_in,
output reg signed [17:0] y_out);
integer i = 0;
/*************************************************************
DECLARING AND ASSIGNING FILTER COEFFICIENTS
*************************************************************
wire signed [17:0] b[16:0];
assign	b[0]  =   18'sd1631;
assign 	b[1]  =  -18'sd1691;
assign	b[2]  =  -18'sd5235;
assign 	b[3]  =  -18'sd6109;
assign	b[4]  =  -18'sd1975;
assign	b[5]  =   18'sd7312;
assign	b[6]  =   18'sd19115;
assign	b[7]  =   18'sd28994;
assign	b[8]  =   18'sd32842;
assign	b[9]  =   18'sd28994;
assign	b[10] =   18'sd19115;
assign	b[11] =   18'sd7312;
assign	b[12] =  -18'sd1975;
assign	b[13] =  -18'sd1975;
assign	b[14] =  -18'sd5235;
assign 	b[15] =  -18'sd1691;
assign	b[16] =   18'sd1631;
*************************************************************
COEFFICIENTS THAT SHOULD BE ABLE
TO HANDLE A MAX 1s17 INPUT.
(beta is .25 here, but magnitude response is not necessarily normalized)
********************************************************/
wire signed [17:0] b[16:0];
assign	b[0] =   18'sd1208;
assign 	b[1] =  -18'sd1252;
assign	b[2] =  -18'sd3877;
assign 	b[3] =  -18'sd4524;
assign	b[4] =  -18'sd1462;
assign	b[5] =   18'sd5415;
assign	b[6] =   18'sd14156;
assign	b[7] =   18'sd21473;
assign	b[8] =   18'sd24322;
assign	b[9]  =   18'sd21473;
assign	b[10] =   18'sd14156;
assign	b[11] =   18'sd5415;
assign	b[12] =  -18'sd1462;
assign	b[13] =  -18'sd4524;
assign	b[14] =  -18'sd3877;
assign 	b[15] =  -18'sd1252;
assign	b[16] =   18'sd1208;
/*************************************************************
END OF COEFFICIENT DECLARATIONS AND ASSIGNMENTS
*************************************************************
Declaring the top level of the filter
*************************************************************/
reg signed [17:0] x[16:0];
/*************************************************************
End Top Level Declaration
************************************************************
Declaring Multiplier Outputs
************************************************************/
reg signed [35:0] multiplier_out[16:0];
reg signed [17:0] summation_out;
/*************************************************************
End Top Multiplier Output Declarations
************************************************************
Top Level Registers
************************************************************/
always @ (posedge sys_clk)
				x[0] <= x_in;
always @ (posedge sys_clk)
		begin
			for(i=1; i<17;i=i+1)
				x[i] <= x[i-1];
		end
/************************************************************
End Top Level Registers
************************************************************
Declaring Multiplier Outputs
************************************************************/
always @ *
		begin
			for(i=0;i < 17;i=i+1)
				multiplier_out[i] = b[i] * x[i];
		end
/************************************************************
End Declaring Multiplier Outputs
************************************************************
Declaring Summations
************************************************************/
always @ *
summation_out = multiplier_out[16][34:17]+multiplier_out[15][34:17] +multiplier_out[14][34:17]
					+multiplier_out[13][34:17]+multiplier_out[12][34:17] +multiplier_out[11][34:17]
					+multiplier_out[10][34:17]+multiplier_out[9][34:17]  +multiplier_out[8][34:17]
					+multiplier_out[7][34:17] +multiplier_out[6][34:17]  +multiplier_out[5][34:17]
					+multiplier_out[4][34:17] +multiplier_out[3][34:17]  +multiplier_out[2][34:17]
					+multiplier_out[1][34:17] +multiplier_out[0][34:17];
/************************************************************
End Declaring Multiplier Outputs
************************************************************
Declaring Summations
************************************************************/
always @ (posedge sys_clk)
y_out = summation_out;

endmodule
`default_nettype wire
