// Copyright (C) 2017  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel MegaCore Function License Agreement, or other 
// applicable license agreement, including, without limitation, 
// that your use is for the sole purpose of programming logic 
// devices manufactured by Intel and sold by Intel or its 
// authorized distributors.  Please refer to the applicable 
// agreement for further details.

// VENDOR "Altera"
// PROGRAM "Quartus Prime"
// VERSION "Version 17.0.0 Build 595 04/25/2017 SJ Standard Edition"

// DATE "01/15/2018 10:42:15"

// 
// Device: Altera EP4CE115F29C7 Package FBGA780
// 

// 
// This greybox netlist file is for third party Synthesis Tools
// for timing and resource estimation only.
// 


module NCO_Party (
	clk,
	clken,
	phi_inc_i,
	fsin_o,
	out_valid,
	reset_n)/* synthesis synthesis_greybox=1 */;
input 	clk;
input 	clken;
input 	[31:0] phi_inc_i;
output 	[17:0] fsin_o;
output 	out_valid;
input 	reset_n;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \nco_ii_0|ux122|data_out[0]~q ;
wire \nco_ii_0|ux122|data_out[1]~q ;
wire \nco_ii_0|ux122|data_out[2]~q ;
wire \nco_ii_0|ux122|data_out[3]~q ;
wire \nco_ii_0|ux122|data_out[4]~q ;
wire \nco_ii_0|ux122|data_out[5]~q ;
wire \nco_ii_0|ux122|data_out[6]~q ;
wire \nco_ii_0|ux122|data_out[7]~q ;
wire \nco_ii_0|ux122|data_out[8]~q ;
wire \nco_ii_0|ux122|data_out[9]~q ;
wire \nco_ii_0|ux122|data_out[10]~q ;
wire \nco_ii_0|ux122|data_out[11]~q ;
wire \nco_ii_0|ux122|data_out[12]~q ;
wire \nco_ii_0|ux122|data_out[13]~q ;
wire \nco_ii_0|ux122|data_out[14]~q ;
wire \nco_ii_0|ux122|data_out[15]~q ;
wire \nco_ii_0|ux122|data_out[16]~q ;
wire \nco_ii_0|ux122|data_out[17]~q ;
wire \nco_ii_0|ux710isdr|data_ready~q ;
wire \~GND~combout ;
wire \clk~input_o ;
wire \reset_n~input_o ;
wire \clken~input_o ;
wire \phi_inc_i[16]~input_o ;
wire \phi_inc_i[15]~input_o ;
wire \phi_inc_i[14]~input_o ;
wire \phi_inc_i[13]~input_o ;
wire \phi_inc_i[12]~input_o ;
wire \phi_inc_i[11]~input_o ;
wire \phi_inc_i[10]~input_o ;
wire \phi_inc_i[9]~input_o ;
wire \phi_inc_i[8]~input_o ;
wire \phi_inc_i[7]~input_o ;
wire \phi_inc_i[6]~input_o ;
wire \phi_inc_i[5]~input_o ;
wire \phi_inc_i[4]~input_o ;
wire \phi_inc_i[3]~input_o ;
wire \phi_inc_i[2]~input_o ;
wire \phi_inc_i[1]~input_o ;
wire \phi_inc_i[0]~input_o ;
wire \phi_inc_i[17]~input_o ;
wire \phi_inc_i[18]~input_o ;
wire \phi_inc_i[19]~input_o ;
wire \phi_inc_i[20]~input_o ;
wire \phi_inc_i[21]~input_o ;
wire \phi_inc_i[22]~input_o ;
wire \phi_inc_i[23]~input_o ;
wire \phi_inc_i[24]~input_o ;
wire \phi_inc_i[25]~input_o ;
wire \phi_inc_i[26]~input_o ;
wire \phi_inc_i[27]~input_o ;
wire \phi_inc_i[28]~input_o ;
wire \phi_inc_i[30]~input_o ;
wire \phi_inc_i[29]~input_o ;
wire \phi_inc_i[31]~input_o ;


NCO_Party_NCO_Party_nco_ii_0 nco_ii_0(
	.data_out_0(\nco_ii_0|ux122|data_out[0]~q ),
	.data_out_1(\nco_ii_0|ux122|data_out[1]~q ),
	.data_out_2(\nco_ii_0|ux122|data_out[2]~q ),
	.data_out_3(\nco_ii_0|ux122|data_out[3]~q ),
	.data_out_4(\nco_ii_0|ux122|data_out[4]~q ),
	.data_out_5(\nco_ii_0|ux122|data_out[5]~q ),
	.data_out_6(\nco_ii_0|ux122|data_out[6]~q ),
	.data_out_7(\nco_ii_0|ux122|data_out[7]~q ),
	.data_out_8(\nco_ii_0|ux122|data_out[8]~q ),
	.data_out_9(\nco_ii_0|ux122|data_out[9]~q ),
	.data_out_10(\nco_ii_0|ux122|data_out[10]~q ),
	.data_out_11(\nco_ii_0|ux122|data_out[11]~q ),
	.data_out_12(\nco_ii_0|ux122|data_out[12]~q ),
	.data_out_13(\nco_ii_0|ux122|data_out[13]~q ),
	.data_out_14(\nco_ii_0|ux122|data_out[14]~q ),
	.data_out_15(\nco_ii_0|ux122|data_out[15]~q ),
	.data_out_16(\nco_ii_0|ux122|data_out[16]~q ),
	.data_out_17(\nco_ii_0|ux122|data_out[17]~q ),
	.data_ready(\nco_ii_0|ux710isdr|data_ready~q ),
	.GND_port(\~GND~combout ),
	.clk(\clk~input_o ),
	.reset_n(\reset_n~input_o ),
	.clken(\clken~input_o ),
	.phi_inc_i_16(\phi_inc_i[16]~input_o ),
	.phi_inc_i_15(\phi_inc_i[15]~input_o ),
	.phi_inc_i_14(\phi_inc_i[14]~input_o ),
	.phi_inc_i_13(\phi_inc_i[13]~input_o ),
	.phi_inc_i_12(\phi_inc_i[12]~input_o ),
	.phi_inc_i_11(\phi_inc_i[11]~input_o ),
	.phi_inc_i_10(\phi_inc_i[10]~input_o ),
	.phi_inc_i_9(\phi_inc_i[9]~input_o ),
	.phi_inc_i_8(\phi_inc_i[8]~input_o ),
	.phi_inc_i_7(\phi_inc_i[7]~input_o ),
	.phi_inc_i_6(\phi_inc_i[6]~input_o ),
	.phi_inc_i_5(\phi_inc_i[5]~input_o ),
	.phi_inc_i_4(\phi_inc_i[4]~input_o ),
	.phi_inc_i_3(\phi_inc_i[3]~input_o ),
	.phi_inc_i_2(\phi_inc_i[2]~input_o ),
	.phi_inc_i_1(\phi_inc_i[1]~input_o ),
	.phi_inc_i_0(\phi_inc_i[0]~input_o ),
	.phi_inc_i_17(\phi_inc_i[17]~input_o ),
	.phi_inc_i_18(\phi_inc_i[18]~input_o ),
	.phi_inc_i_19(\phi_inc_i[19]~input_o ),
	.phi_inc_i_20(\phi_inc_i[20]~input_o ),
	.phi_inc_i_21(\phi_inc_i[21]~input_o ),
	.phi_inc_i_22(\phi_inc_i[22]~input_o ),
	.phi_inc_i_23(\phi_inc_i[23]~input_o ),
	.phi_inc_i_24(\phi_inc_i[24]~input_o ),
	.phi_inc_i_25(\phi_inc_i[25]~input_o ),
	.phi_inc_i_26(\phi_inc_i[26]~input_o ),
	.phi_inc_i_27(\phi_inc_i[27]~input_o ),
	.phi_inc_i_28(\phi_inc_i[28]~input_o ),
	.phi_inc_i_30(\phi_inc_i[30]~input_o ),
	.phi_inc_i_29(\phi_inc_i[29]~input_o ),
	.phi_inc_i_31(\phi_inc_i[31]~input_o ));

cycloneive_lcell_comb \~GND (
	.dataa(gnd),
	.datab(gnd),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\~GND~combout ),
	.cout());
defparam \~GND .lut_mask = 16'h0000;
defparam \~GND .sum_lutc_input = "datac";

assign \clk~input_o  = clk;

assign \reset_n~input_o  = reset_n;

assign \clken~input_o  = clken;

assign \phi_inc_i[16]~input_o  = phi_inc_i[16];

assign \phi_inc_i[15]~input_o  = phi_inc_i[15];

assign \phi_inc_i[14]~input_o  = phi_inc_i[14];

assign \phi_inc_i[13]~input_o  = phi_inc_i[13];

assign \phi_inc_i[12]~input_o  = phi_inc_i[12];

assign \phi_inc_i[11]~input_o  = phi_inc_i[11];

assign \phi_inc_i[10]~input_o  = phi_inc_i[10];

assign \phi_inc_i[9]~input_o  = phi_inc_i[9];

assign \phi_inc_i[8]~input_o  = phi_inc_i[8];

assign \phi_inc_i[7]~input_o  = phi_inc_i[7];

assign \phi_inc_i[6]~input_o  = phi_inc_i[6];

assign \phi_inc_i[5]~input_o  = phi_inc_i[5];

assign \phi_inc_i[4]~input_o  = phi_inc_i[4];

assign \phi_inc_i[3]~input_o  = phi_inc_i[3];

assign \phi_inc_i[2]~input_o  = phi_inc_i[2];

assign \phi_inc_i[1]~input_o  = phi_inc_i[1];

assign \phi_inc_i[0]~input_o  = phi_inc_i[0];

assign \phi_inc_i[17]~input_o  = phi_inc_i[17];

assign \phi_inc_i[18]~input_o  = phi_inc_i[18];

assign \phi_inc_i[19]~input_o  = phi_inc_i[19];

assign \phi_inc_i[20]~input_o  = phi_inc_i[20];

assign \phi_inc_i[21]~input_o  = phi_inc_i[21];

assign \phi_inc_i[22]~input_o  = phi_inc_i[22];

assign \phi_inc_i[23]~input_o  = phi_inc_i[23];

assign \phi_inc_i[24]~input_o  = phi_inc_i[24];

assign \phi_inc_i[25]~input_o  = phi_inc_i[25];

assign \phi_inc_i[26]~input_o  = phi_inc_i[26];

assign \phi_inc_i[27]~input_o  = phi_inc_i[27];

assign \phi_inc_i[28]~input_o  = phi_inc_i[28];

assign \phi_inc_i[30]~input_o  = phi_inc_i[30];

assign \phi_inc_i[29]~input_o  = phi_inc_i[29];

assign \phi_inc_i[31]~input_o  = phi_inc_i[31];

assign fsin_o[0] = \nco_ii_0|ux122|data_out[0]~q ;

assign fsin_o[1] = \nco_ii_0|ux122|data_out[1]~q ;

assign fsin_o[2] = \nco_ii_0|ux122|data_out[2]~q ;

assign fsin_o[3] = \nco_ii_0|ux122|data_out[3]~q ;

assign fsin_o[4] = \nco_ii_0|ux122|data_out[4]~q ;

assign fsin_o[5] = \nco_ii_0|ux122|data_out[5]~q ;

assign fsin_o[6] = \nco_ii_0|ux122|data_out[6]~q ;

assign fsin_o[7] = \nco_ii_0|ux122|data_out[7]~q ;

assign fsin_o[8] = \nco_ii_0|ux122|data_out[8]~q ;

assign fsin_o[9] = \nco_ii_0|ux122|data_out[9]~q ;

assign fsin_o[10] = \nco_ii_0|ux122|data_out[10]~q ;

assign fsin_o[11] = \nco_ii_0|ux122|data_out[11]~q ;

assign fsin_o[12] = \nco_ii_0|ux122|data_out[12]~q ;

assign fsin_o[13] = \nco_ii_0|ux122|data_out[13]~q ;

assign fsin_o[14] = \nco_ii_0|ux122|data_out[14]~q ;

assign fsin_o[15] = \nco_ii_0|ux122|data_out[15]~q ;

assign fsin_o[16] = \nco_ii_0|ux122|data_out[16]~q ;

assign fsin_o[17] = \nco_ii_0|ux122|data_out[17]~q ;

assign out_valid = \nco_ii_0|ux710isdr|data_ready~q ;

endmodule

module NCO_Party_NCO_Party_nco_ii_0 (
	data_out_0,
	data_out_1,
	data_out_2,
	data_out_3,
	data_out_4,
	data_out_5,
	data_out_6,
	data_out_7,
	data_out_8,
	data_out_9,
	data_out_10,
	data_out_11,
	data_out_12,
	data_out_13,
	data_out_14,
	data_out_15,
	data_out_16,
	data_out_17,
	data_ready,
	GND_port,
	clk,
	reset_n,
	clken,
	phi_inc_i_16,
	phi_inc_i_15,
	phi_inc_i_14,
	phi_inc_i_13,
	phi_inc_i_12,
	phi_inc_i_11,
	phi_inc_i_10,
	phi_inc_i_9,
	phi_inc_i_8,
	phi_inc_i_7,
	phi_inc_i_6,
	phi_inc_i_5,
	phi_inc_i_4,
	phi_inc_i_3,
	phi_inc_i_2,
	phi_inc_i_1,
	phi_inc_i_0,
	phi_inc_i_17,
	phi_inc_i_18,
	phi_inc_i_19,
	phi_inc_i_20,
	phi_inc_i_21,
	phi_inc_i_22,
	phi_inc_i_23,
	phi_inc_i_24,
	phi_inc_i_25,
	phi_inc_i_26,
	phi_inc_i_27,
	phi_inc_i_28,
	phi_inc_i_30,
	phi_inc_i_29,
	phi_inc_i_31)/* synthesis synthesis_greybox=1 */;
output 	data_out_0;
output 	data_out_1;
output 	data_out_2;
output 	data_out_3;
output 	data_out_4;
output 	data_out_5;
output 	data_out_6;
output 	data_out_7;
output 	data_out_8;
output 	data_out_9;
output 	data_out_10;
output 	data_out_11;
output 	data_out_12;
output 	data_out_13;
output 	data_out_14;
output 	data_out_15;
output 	data_out_16;
output 	data_out_17;
output 	data_ready;
input 	GND_port;
input 	clk;
input 	reset_n;
input 	clken;
input 	phi_inc_i_16;
input 	phi_inc_i_15;
input 	phi_inc_i_14;
input 	phi_inc_i_13;
input 	phi_inc_i_12;
input 	phi_inc_i_11;
input 	phi_inc_i_10;
input 	phi_inc_i_9;
input 	phi_inc_i_8;
input 	phi_inc_i_7;
input 	phi_inc_i_6;
input 	phi_inc_i_5;
input 	phi_inc_i_4;
input 	phi_inc_i_3;
input 	phi_inc_i_2;
input 	phi_inc_i_1;
input 	phi_inc_i_0;
input 	phi_inc_i_17;
input 	phi_inc_i_18;
input 	phi_inc_i_19;
input 	phi_inc_i_20;
input 	phi_inc_i_21;
input 	phi_inc_i_22;
input 	phi_inc_i_23;
input 	phi_inc_i_24;
input 	phi_inc_i_25;
input 	phi_inc_i_26;
input 	phi_inc_i_27;
input 	phi_inc_i_28;
input 	phi_inc_i_30;
input 	phi_inc_i_29;
input 	phi_inc_i_31;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \ux001|dxxrv[3]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[16]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[15]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[14]~q ;
wire \ux001|dxxrv[2]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[13]~q ;
wire \ux001|dxxrv[1]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[12]~q ;
wire \ux001|dxxrv[0]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[11]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[17]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[18]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[19]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[20]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[21]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[22]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[23]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[24]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[25]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[26]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[27]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[28]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[30]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[29]~q ;
wire \ux000|acc|auto_generated|pipeline_dffe[31]~q ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~4_combout ;
wire \ux122|data_out[8]~0_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~9_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~14_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~19_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~24_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~29_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~34_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~39_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~44_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~49_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~54_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~59_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~64_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~69_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~74_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~79_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~84_combout ;
wire \ux0120|altsyncram_component0|auto_generated|mux2|_~89_combout ;
wire \ux009|rom_add[0]~q ;
wire \ux009|rom_add[1]~q ;
wire \ux009|rom_add[2]~q ;
wire \ux009|rom_add[3]~q ;
wire \ux009|rom_add[4]~q ;
wire \ux009|rom_add[5]~q ;
wire \ux009|rom_add[6]~q ;
wire \ux009|rom_add[7]~q ;
wire \ux009|rom_add[8]~q ;
wire \ux009|rom_add[9]~q ;
wire \ux009|rom_add[10]~q ;
wire \ux009|rom_add[11]~q ;
wire \ux009|rom_add[12]~q ;
wire \ux002|dxxpdo[5]~q ;
wire \ux002|dxxpdo[6]~q ;
wire \ux002|dxxpdo[7]~q ;
wire \ux002|dxxpdo[8]~q ;
wire \ux002|dxxpdo[9]~q ;
wire \ux002|dxxpdo[10]~q ;
wire \ux002|dxxpdo[11]~q ;
wire \ux002|dxxpdo[12]~q ;
wire \ux002|dxxpdo[13]~q ;
wire \ux002|dxxpdo[14]~q ;
wire \ux002|dxxpdo[15]~q ;
wire \ux002|dxxpdo[16]~q ;
wire \ux002|dxxpdo[17]~q ;
wire \ux009|rom_add[14]~q ;
wire \ux009|rom_add[13]~q ;
wire \ux009|rom_add[15]~q ;
wire \ux002|dxxpdo[19]~q ;
wire \ux002|dxxpdo[18]~q ;
wire \ux002|dxxpdo[20]~q ;


NCO_Party_asj_nco_isdr ux710isdr(
	.data_ready1(data_ready),
	.GND_port(GND_port),
	.clk(clk),
	.reset_n(reset_n),
	.clken(clken));

NCO_Party_asj_nco_mob_rw ux122(
	.data_out_0(data_out_0),
	.data_out_1(data_out_1),
	.data_out_2(data_out_2),
	.data_out_3(data_out_3),
	.data_out_4(data_out_4),
	.data_out_5(data_out_5),
	.data_out_6(data_out_6),
	.data_out_7(data_out_7),
	.data_out_8(data_out_8),
	.data_out_9(data_out_9),
	.data_out_10(data_out_10),
	.data_out_11(data_out_11),
	.data_out_12(data_out_12),
	.data_out_13(data_out_13),
	.data_out_14(data_out_14),
	.data_out_15(data_out_15),
	.data_out_16(data_out_16),
	.data_out_17(data_out_17),
	._(\ux0120|altsyncram_component0|auto_generated|mux2|_~4_combout ),
	.data_out_81(\ux122|data_out[8]~0_combout ),
	._1(\ux0120|altsyncram_component0|auto_generated|mux2|_~9_combout ),
	._2(\ux0120|altsyncram_component0|auto_generated|mux2|_~14_combout ),
	._3(\ux0120|altsyncram_component0|auto_generated|mux2|_~19_combout ),
	._4(\ux0120|altsyncram_component0|auto_generated|mux2|_~24_combout ),
	._5(\ux0120|altsyncram_component0|auto_generated|mux2|_~29_combout ),
	._6(\ux0120|altsyncram_component0|auto_generated|mux2|_~34_combout ),
	._7(\ux0120|altsyncram_component0|auto_generated|mux2|_~39_combout ),
	._8(\ux0120|altsyncram_component0|auto_generated|mux2|_~44_combout ),
	._9(\ux0120|altsyncram_component0|auto_generated|mux2|_~49_combout ),
	._10(\ux0120|altsyncram_component0|auto_generated|mux2|_~54_combout ),
	._11(\ux0120|altsyncram_component0|auto_generated|mux2|_~59_combout ),
	._12(\ux0120|altsyncram_component0|auto_generated|mux2|_~64_combout ),
	._13(\ux0120|altsyncram_component0|auto_generated|mux2|_~69_combout ),
	._14(\ux0120|altsyncram_component0|auto_generated|mux2|_~74_combout ),
	._15(\ux0120|altsyncram_component0|auto_generated|mux2|_~79_combout ),
	._16(\ux0120|altsyncram_component0|auto_generated|mux2|_~84_combout ),
	._17(\ux0120|altsyncram_component0|auto_generated|mux2|_~89_combout ),
	.clk(clk),
	.reset_n(reset_n),
	.clken(clken));

NCO_Party_asj_nco_as_m_cen ux0120(
	._(\ux0120|altsyncram_component0|auto_generated|mux2|_~4_combout ),
	._1(\ux0120|altsyncram_component0|auto_generated|mux2|_~9_combout ),
	._2(\ux0120|altsyncram_component0|auto_generated|mux2|_~14_combout ),
	._3(\ux0120|altsyncram_component0|auto_generated|mux2|_~19_combout ),
	._4(\ux0120|altsyncram_component0|auto_generated|mux2|_~24_combout ),
	._5(\ux0120|altsyncram_component0|auto_generated|mux2|_~29_combout ),
	._6(\ux0120|altsyncram_component0|auto_generated|mux2|_~34_combout ),
	._7(\ux0120|altsyncram_component0|auto_generated|mux2|_~39_combout ),
	._8(\ux0120|altsyncram_component0|auto_generated|mux2|_~44_combout ),
	._9(\ux0120|altsyncram_component0|auto_generated|mux2|_~49_combout ),
	._10(\ux0120|altsyncram_component0|auto_generated|mux2|_~54_combout ),
	._11(\ux0120|altsyncram_component0|auto_generated|mux2|_~59_combout ),
	._12(\ux0120|altsyncram_component0|auto_generated|mux2|_~64_combout ),
	._13(\ux0120|altsyncram_component0|auto_generated|mux2|_~69_combout ),
	._14(\ux0120|altsyncram_component0|auto_generated|mux2|_~74_combout ),
	._15(\ux0120|altsyncram_component0|auto_generated|mux2|_~79_combout ),
	._16(\ux0120|altsyncram_component0|auto_generated|mux2|_~84_combout ),
	._17(\ux0120|altsyncram_component0|auto_generated|mux2|_~89_combout ),
	.rom_add_0(\ux009|rom_add[0]~q ),
	.rom_add_1(\ux009|rom_add[1]~q ),
	.rom_add_2(\ux009|rom_add[2]~q ),
	.rom_add_3(\ux009|rom_add[3]~q ),
	.rom_add_4(\ux009|rom_add[4]~q ),
	.rom_add_5(\ux009|rom_add[5]~q ),
	.rom_add_6(\ux009|rom_add[6]~q ),
	.rom_add_7(\ux009|rom_add[7]~q ),
	.rom_add_8(\ux009|rom_add[8]~q ),
	.rom_add_9(\ux009|rom_add[9]~q ),
	.rom_add_10(\ux009|rom_add[10]~q ),
	.rom_add_11(\ux009|rom_add[11]~q ),
	.rom_add_12(\ux009|rom_add[12]~q ),
	.rom_add_14(\ux009|rom_add[14]~q ),
	.rom_add_13(\ux009|rom_add[13]~q ),
	.rom_add_15(\ux009|rom_add[15]~q ),
	.clk(clk),
	.clken(clken));

NCO_Party_asj_gal ux009(
	.data_out_8(\ux122|data_out[8]~0_combout ),
	.rom_add_0(\ux009|rom_add[0]~q ),
	.rom_add_1(\ux009|rom_add[1]~q ),
	.rom_add_2(\ux009|rom_add[2]~q ),
	.rom_add_3(\ux009|rom_add[3]~q ),
	.rom_add_4(\ux009|rom_add[4]~q ),
	.rom_add_5(\ux009|rom_add[5]~q ),
	.rom_add_6(\ux009|rom_add[6]~q ),
	.rom_add_7(\ux009|rom_add[7]~q ),
	.rom_add_8(\ux009|rom_add[8]~q ),
	.rom_add_9(\ux009|rom_add[9]~q ),
	.rom_add_10(\ux009|rom_add[10]~q ),
	.rom_add_11(\ux009|rom_add[11]~q ),
	.rom_add_12(\ux009|rom_add[12]~q ),
	.dxxpdo_5(\ux002|dxxpdo[5]~q ),
	.dxxpdo_6(\ux002|dxxpdo[6]~q ),
	.dxxpdo_7(\ux002|dxxpdo[7]~q ),
	.dxxpdo_8(\ux002|dxxpdo[8]~q ),
	.dxxpdo_9(\ux002|dxxpdo[9]~q ),
	.dxxpdo_10(\ux002|dxxpdo[10]~q ),
	.dxxpdo_11(\ux002|dxxpdo[11]~q ),
	.dxxpdo_12(\ux002|dxxpdo[12]~q ),
	.dxxpdo_13(\ux002|dxxpdo[13]~q ),
	.dxxpdo_14(\ux002|dxxpdo[14]~q ),
	.dxxpdo_15(\ux002|dxxpdo[15]~q ),
	.dxxpdo_16(\ux002|dxxpdo[16]~q ),
	.dxxpdo_17(\ux002|dxxpdo[17]~q ),
	.rom_add_14(\ux009|rom_add[14]~q ),
	.rom_add_13(\ux009|rom_add[13]~q ),
	.rom_add_15(\ux009|rom_add[15]~q ),
	.dxxpdo_19(\ux002|dxxpdo[19]~q ),
	.dxxpdo_18(\ux002|dxxpdo[18]~q ),
	.dxxpdo_20(\ux002|dxxpdo[20]~q ),
	.clk(clk),
	.reset_n(reset_n));

NCO_Party_asj_dxx ux002(
	.dxxrv_3(\ux001|dxxrv[3]~q ),
	.pipeline_dffe_16(\ux000|acc|auto_generated|pipeline_dffe[16]~q ),
	.pipeline_dffe_15(\ux000|acc|auto_generated|pipeline_dffe[15]~q ),
	.pipeline_dffe_14(\ux000|acc|auto_generated|pipeline_dffe[14]~q ),
	.dxxrv_2(\ux001|dxxrv[2]~q ),
	.pipeline_dffe_13(\ux000|acc|auto_generated|pipeline_dffe[13]~q ),
	.dxxrv_1(\ux001|dxxrv[1]~q ),
	.pipeline_dffe_12(\ux000|acc|auto_generated|pipeline_dffe[12]~q ),
	.dxxrv_0(\ux001|dxxrv[0]~q ),
	.pipeline_dffe_11(\ux000|acc|auto_generated|pipeline_dffe[11]~q ),
	.pipeline_dffe_17(\ux000|acc|auto_generated|pipeline_dffe[17]~q ),
	.pipeline_dffe_18(\ux000|acc|auto_generated|pipeline_dffe[18]~q ),
	.pipeline_dffe_19(\ux000|acc|auto_generated|pipeline_dffe[19]~q ),
	.pipeline_dffe_20(\ux000|acc|auto_generated|pipeline_dffe[20]~q ),
	.pipeline_dffe_21(\ux000|acc|auto_generated|pipeline_dffe[21]~q ),
	.pipeline_dffe_22(\ux000|acc|auto_generated|pipeline_dffe[22]~q ),
	.pipeline_dffe_23(\ux000|acc|auto_generated|pipeline_dffe[23]~q ),
	.pipeline_dffe_24(\ux000|acc|auto_generated|pipeline_dffe[24]~q ),
	.pipeline_dffe_25(\ux000|acc|auto_generated|pipeline_dffe[25]~q ),
	.pipeline_dffe_26(\ux000|acc|auto_generated|pipeline_dffe[26]~q ),
	.pipeline_dffe_27(\ux000|acc|auto_generated|pipeline_dffe[27]~q ),
	.pipeline_dffe_28(\ux000|acc|auto_generated|pipeline_dffe[28]~q ),
	.pipeline_dffe_30(\ux000|acc|auto_generated|pipeline_dffe[30]~q ),
	.pipeline_dffe_29(\ux000|acc|auto_generated|pipeline_dffe[29]~q ),
	.pipeline_dffe_31(\ux000|acc|auto_generated|pipeline_dffe[31]~q ),
	.data_out_8(\ux122|data_out[8]~0_combout ),
	.dxxpdo_5(\ux002|dxxpdo[5]~q ),
	.dxxpdo_6(\ux002|dxxpdo[6]~q ),
	.dxxpdo_7(\ux002|dxxpdo[7]~q ),
	.dxxpdo_8(\ux002|dxxpdo[8]~q ),
	.dxxpdo_9(\ux002|dxxpdo[9]~q ),
	.dxxpdo_10(\ux002|dxxpdo[10]~q ),
	.dxxpdo_11(\ux002|dxxpdo[11]~q ),
	.dxxpdo_12(\ux002|dxxpdo[12]~q ),
	.dxxpdo_13(\ux002|dxxpdo[13]~q ),
	.dxxpdo_14(\ux002|dxxpdo[14]~q ),
	.dxxpdo_15(\ux002|dxxpdo[15]~q ),
	.dxxpdo_16(\ux002|dxxpdo[16]~q ),
	.dxxpdo_17(\ux002|dxxpdo[17]~q ),
	.dxxpdo_19(\ux002|dxxpdo[19]~q ),
	.dxxpdo_18(\ux002|dxxpdo[18]~q ),
	.dxxpdo_20(\ux002|dxxpdo[20]~q ),
	.clk(clk),
	.reset_n(reset_n));

NCO_Party_asj_dxx_g ux001(
	.dxxrv_3(\ux001|dxxrv[3]~q ),
	.dxxrv_2(\ux001|dxxrv[2]~q ),
	.dxxrv_1(\ux001|dxxrv[1]~q ),
	.dxxrv_0(\ux001|dxxrv[0]~q ),
	.data_out_8(\ux122|data_out[8]~0_combout ),
	.clk(clk),
	.reset_n(reset_n));

NCO_Party_asj_altqmcpipe ux000(
	.pipeline_dffe_16(\ux000|acc|auto_generated|pipeline_dffe[16]~q ),
	.pipeline_dffe_15(\ux000|acc|auto_generated|pipeline_dffe[15]~q ),
	.pipeline_dffe_14(\ux000|acc|auto_generated|pipeline_dffe[14]~q ),
	.pipeline_dffe_13(\ux000|acc|auto_generated|pipeline_dffe[13]~q ),
	.pipeline_dffe_12(\ux000|acc|auto_generated|pipeline_dffe[12]~q ),
	.pipeline_dffe_11(\ux000|acc|auto_generated|pipeline_dffe[11]~q ),
	.pipeline_dffe_17(\ux000|acc|auto_generated|pipeline_dffe[17]~q ),
	.pipeline_dffe_18(\ux000|acc|auto_generated|pipeline_dffe[18]~q ),
	.pipeline_dffe_19(\ux000|acc|auto_generated|pipeline_dffe[19]~q ),
	.pipeline_dffe_20(\ux000|acc|auto_generated|pipeline_dffe[20]~q ),
	.pipeline_dffe_21(\ux000|acc|auto_generated|pipeline_dffe[21]~q ),
	.pipeline_dffe_22(\ux000|acc|auto_generated|pipeline_dffe[22]~q ),
	.pipeline_dffe_23(\ux000|acc|auto_generated|pipeline_dffe[23]~q ),
	.pipeline_dffe_24(\ux000|acc|auto_generated|pipeline_dffe[24]~q ),
	.pipeline_dffe_25(\ux000|acc|auto_generated|pipeline_dffe[25]~q ),
	.pipeline_dffe_26(\ux000|acc|auto_generated|pipeline_dffe[26]~q ),
	.pipeline_dffe_27(\ux000|acc|auto_generated|pipeline_dffe[27]~q ),
	.pipeline_dffe_28(\ux000|acc|auto_generated|pipeline_dffe[28]~q ),
	.pipeline_dffe_30(\ux000|acc|auto_generated|pipeline_dffe[30]~q ),
	.pipeline_dffe_29(\ux000|acc|auto_generated|pipeline_dffe[29]~q ),
	.pipeline_dffe_31(\ux000|acc|auto_generated|pipeline_dffe[31]~q ),
	.data_out_8(\ux122|data_out[8]~0_combout ),
	.clk(clk),
	.reset_n(reset_n),
	.clken(clken),
	.phi_inc_i_16(phi_inc_i_16),
	.phi_inc_i_15(phi_inc_i_15),
	.phi_inc_i_14(phi_inc_i_14),
	.phi_inc_i_13(phi_inc_i_13),
	.phi_inc_i_12(phi_inc_i_12),
	.phi_inc_i_11(phi_inc_i_11),
	.phi_inc_i_10(phi_inc_i_10),
	.phi_inc_i_9(phi_inc_i_9),
	.phi_inc_i_8(phi_inc_i_8),
	.phi_inc_i_7(phi_inc_i_7),
	.phi_inc_i_6(phi_inc_i_6),
	.phi_inc_i_5(phi_inc_i_5),
	.phi_inc_i_4(phi_inc_i_4),
	.phi_inc_i_3(phi_inc_i_3),
	.phi_inc_i_2(phi_inc_i_2),
	.phi_inc_i_1(phi_inc_i_1),
	.phi_inc_i_0(phi_inc_i_0),
	.phi_inc_i_17(phi_inc_i_17),
	.phi_inc_i_18(phi_inc_i_18),
	.phi_inc_i_19(phi_inc_i_19),
	.phi_inc_i_20(phi_inc_i_20),
	.phi_inc_i_21(phi_inc_i_21),
	.phi_inc_i_22(phi_inc_i_22),
	.phi_inc_i_23(phi_inc_i_23),
	.phi_inc_i_24(phi_inc_i_24),
	.phi_inc_i_25(phi_inc_i_25),
	.phi_inc_i_26(phi_inc_i_26),
	.phi_inc_i_27(phi_inc_i_27),
	.phi_inc_i_28(phi_inc_i_28),
	.phi_inc_i_30(phi_inc_i_30),
	.phi_inc_i_29(phi_inc_i_29),
	.phi_inc_i_31(phi_inc_i_31));

endmodule

module NCO_Party_asj_altqmcpipe (
	pipeline_dffe_16,
	pipeline_dffe_15,
	pipeline_dffe_14,
	pipeline_dffe_13,
	pipeline_dffe_12,
	pipeline_dffe_11,
	pipeline_dffe_17,
	pipeline_dffe_18,
	pipeline_dffe_19,
	pipeline_dffe_20,
	pipeline_dffe_21,
	pipeline_dffe_22,
	pipeline_dffe_23,
	pipeline_dffe_24,
	pipeline_dffe_25,
	pipeline_dffe_26,
	pipeline_dffe_27,
	pipeline_dffe_28,
	pipeline_dffe_30,
	pipeline_dffe_29,
	pipeline_dffe_31,
	data_out_8,
	clk,
	reset_n,
	clken,
	phi_inc_i_16,
	phi_inc_i_15,
	phi_inc_i_14,
	phi_inc_i_13,
	phi_inc_i_12,
	phi_inc_i_11,
	phi_inc_i_10,
	phi_inc_i_9,
	phi_inc_i_8,
	phi_inc_i_7,
	phi_inc_i_6,
	phi_inc_i_5,
	phi_inc_i_4,
	phi_inc_i_3,
	phi_inc_i_2,
	phi_inc_i_1,
	phi_inc_i_0,
	phi_inc_i_17,
	phi_inc_i_18,
	phi_inc_i_19,
	phi_inc_i_20,
	phi_inc_i_21,
	phi_inc_i_22,
	phi_inc_i_23,
	phi_inc_i_24,
	phi_inc_i_25,
	phi_inc_i_26,
	phi_inc_i_27,
	phi_inc_i_28,
	phi_inc_i_30,
	phi_inc_i_29,
	phi_inc_i_31)/* synthesis synthesis_greybox=1 */;
output 	pipeline_dffe_16;
output 	pipeline_dffe_15;
output 	pipeline_dffe_14;
output 	pipeline_dffe_13;
output 	pipeline_dffe_12;
output 	pipeline_dffe_11;
output 	pipeline_dffe_17;
output 	pipeline_dffe_18;
output 	pipeline_dffe_19;
output 	pipeline_dffe_20;
output 	pipeline_dffe_21;
output 	pipeline_dffe_22;
output 	pipeline_dffe_23;
output 	pipeline_dffe_24;
output 	pipeline_dffe_25;
output 	pipeline_dffe_26;
output 	pipeline_dffe_27;
output 	pipeline_dffe_28;
output 	pipeline_dffe_30;
output 	pipeline_dffe_29;
output 	pipeline_dffe_31;
input 	data_out_8;
input 	clk;
input 	reset_n;
input 	clken;
input 	phi_inc_i_16;
input 	phi_inc_i_15;
input 	phi_inc_i_14;
input 	phi_inc_i_13;
input 	phi_inc_i_12;
input 	phi_inc_i_11;
input 	phi_inc_i_10;
input 	phi_inc_i_9;
input 	phi_inc_i_8;
input 	phi_inc_i_7;
input 	phi_inc_i_6;
input 	phi_inc_i_5;
input 	phi_inc_i_4;
input 	phi_inc_i_3;
input 	phi_inc_i_2;
input 	phi_inc_i_1;
input 	phi_inc_i_0;
input 	phi_inc_i_17;
input 	phi_inc_i_18;
input 	phi_inc_i_19;
input 	phi_inc_i_20;
input 	phi_inc_i_21;
input 	phi_inc_i_22;
input 	phi_inc_i_23;
input 	phi_inc_i_24;
input 	phi_inc_i_25;
input 	phi_inc_i_26;
input 	phi_inc_i_27;
input 	phi_inc_i_28;
input 	phi_inc_i_30;
input 	phi_inc_i_29;
input 	phi_inc_i_31;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \phi_int_arr_reg[16]~q ;
wire \phi_int_arr_reg[15]~q ;
wire \phi_int_arr_reg[14]~q ;
wire \phi_int_arr_reg[13]~q ;
wire \phi_int_arr_reg[12]~q ;
wire \phi_int_arr_reg[11]~q ;
wire \phi_int_arr_reg[10]~q ;
wire \phi_int_arr_reg[9]~q ;
wire \phi_int_arr_reg[8]~q ;
wire \phi_int_arr_reg[7]~q ;
wire \phi_int_arr_reg[6]~q ;
wire \phi_int_arr_reg[5]~q ;
wire \phi_int_arr_reg[4]~q ;
wire \phi_int_arr_reg[3]~q ;
wire \phi_int_arr_reg[2]~q ;
wire \phi_int_arr_reg[1]~q ;
wire \phi_int_arr_reg[0]~q ;
wire \phi_int_arr_reg[17]~q ;
wire \phi_int_arr_reg[18]~q ;
wire \phi_int_arr_reg[19]~q ;
wire \phi_int_arr_reg[20]~q ;
wire \phi_int_arr_reg[21]~q ;
wire \phi_int_arr_reg[22]~q ;
wire \phi_int_arr_reg[23]~q ;
wire \phi_int_arr_reg[24]~q ;
wire \phi_int_arr_reg[25]~q ;
wire \phi_int_arr_reg[26]~q ;
wire \phi_int_arr_reg[27]~q ;
wire \phi_int_arr_reg[28]~q ;
wire \phi_int_arr_reg~0_combout ;
wire \phi_int_arr_reg~1_combout ;
wire \phi_int_arr_reg~2_combout ;
wire \phi_int_arr_reg~3_combout ;
wire \phi_int_arr_reg~4_combout ;
wire \phi_int_arr_reg~5_combout ;
wire \phi_int_arr_reg~6_combout ;
wire \phi_int_arr_reg~7_combout ;
wire \phi_int_arr_reg~8_combout ;
wire \phi_int_arr_reg~9_combout ;
wire \phi_int_arr_reg~10_combout ;
wire \phi_int_arr_reg~11_combout ;
wire \phi_int_arr_reg~12_combout ;
wire \phi_int_arr_reg~13_combout ;
wire \phi_int_arr_reg~14_combout ;
wire \phi_int_arr_reg~15_combout ;
wire \phi_int_arr_reg~16_combout ;
wire \phi_int_arr_reg~17_combout ;
wire \phi_int_arr_reg~18_combout ;
wire \phi_int_arr_reg~19_combout ;
wire \phi_int_arr_reg~20_combout ;
wire \phi_int_arr_reg~21_combout ;
wire \phi_int_arr_reg~22_combout ;
wire \phi_int_arr_reg~23_combout ;
wire \phi_int_arr_reg~24_combout ;
wire \phi_int_arr_reg~25_combout ;
wire \phi_int_arr_reg~26_combout ;
wire \phi_int_arr_reg~27_combout ;
wire \phi_int_arr_reg~28_combout ;
wire \phi_int_arr_reg[30]~q ;
wire \phi_int_arr_reg[29]~q ;
wire \phi_int_arr_reg[31]~q ;
wire \phi_int_arr_reg~29_combout ;
wire \phi_int_arr_reg~30_combout ;
wire \phi_int_arr_reg~31_combout ;


NCO_Party_lpm_add_sub_1 acc(
	.pipeline_dffe_16(pipeline_dffe_16),
	.pipeline_dffe_15(pipeline_dffe_15),
	.pipeline_dffe_14(pipeline_dffe_14),
	.pipeline_dffe_13(pipeline_dffe_13),
	.pipeline_dffe_12(pipeline_dffe_12),
	.pipeline_dffe_11(pipeline_dffe_11),
	.pipeline_dffe_17(pipeline_dffe_17),
	.pipeline_dffe_18(pipeline_dffe_18),
	.pipeline_dffe_19(pipeline_dffe_19),
	.pipeline_dffe_20(pipeline_dffe_20),
	.pipeline_dffe_21(pipeline_dffe_21),
	.pipeline_dffe_22(pipeline_dffe_22),
	.pipeline_dffe_23(pipeline_dffe_23),
	.pipeline_dffe_24(pipeline_dffe_24),
	.pipeline_dffe_25(pipeline_dffe_25),
	.pipeline_dffe_26(pipeline_dffe_26),
	.pipeline_dffe_27(pipeline_dffe_27),
	.pipeline_dffe_28(pipeline_dffe_28),
	.pipeline_dffe_30(pipeline_dffe_30),
	.pipeline_dffe_29(pipeline_dffe_29),
	.pipeline_dffe_31(pipeline_dffe_31),
	.phi_int_arr_reg_16(\phi_int_arr_reg[16]~q ),
	.phi_int_arr_reg_15(\phi_int_arr_reg[15]~q ),
	.phi_int_arr_reg_14(\phi_int_arr_reg[14]~q ),
	.phi_int_arr_reg_13(\phi_int_arr_reg[13]~q ),
	.phi_int_arr_reg_12(\phi_int_arr_reg[12]~q ),
	.phi_int_arr_reg_11(\phi_int_arr_reg[11]~q ),
	.phi_int_arr_reg_10(\phi_int_arr_reg[10]~q ),
	.phi_int_arr_reg_9(\phi_int_arr_reg[9]~q ),
	.phi_int_arr_reg_8(\phi_int_arr_reg[8]~q ),
	.phi_int_arr_reg_7(\phi_int_arr_reg[7]~q ),
	.phi_int_arr_reg_6(\phi_int_arr_reg[6]~q ),
	.phi_int_arr_reg_5(\phi_int_arr_reg[5]~q ),
	.phi_int_arr_reg_4(\phi_int_arr_reg[4]~q ),
	.phi_int_arr_reg_3(\phi_int_arr_reg[3]~q ),
	.phi_int_arr_reg_2(\phi_int_arr_reg[2]~q ),
	.phi_int_arr_reg_1(\phi_int_arr_reg[1]~q ),
	.phi_int_arr_reg_0(\phi_int_arr_reg[0]~q ),
	.phi_int_arr_reg_17(\phi_int_arr_reg[17]~q ),
	.phi_int_arr_reg_18(\phi_int_arr_reg[18]~q ),
	.phi_int_arr_reg_19(\phi_int_arr_reg[19]~q ),
	.phi_int_arr_reg_20(\phi_int_arr_reg[20]~q ),
	.phi_int_arr_reg_21(\phi_int_arr_reg[21]~q ),
	.phi_int_arr_reg_22(\phi_int_arr_reg[22]~q ),
	.phi_int_arr_reg_23(\phi_int_arr_reg[23]~q ),
	.phi_int_arr_reg_24(\phi_int_arr_reg[24]~q ),
	.phi_int_arr_reg_25(\phi_int_arr_reg[25]~q ),
	.phi_int_arr_reg_26(\phi_int_arr_reg[26]~q ),
	.phi_int_arr_reg_27(\phi_int_arr_reg[27]~q ),
	.phi_int_arr_reg_28(\phi_int_arr_reg[28]~q ),
	.phi_int_arr_reg_30(\phi_int_arr_reg[30]~q ),
	.phi_int_arr_reg_29(\phi_int_arr_reg[29]~q ),
	.phi_int_arr_reg_31(\phi_int_arr_reg[31]~q ),
	.clock(clk),
	.reset_n(reset_n),
	.clken(clken));

dffeas \phi_int_arr_reg[16] (
	.clk(clk),
	.d(\phi_int_arr_reg~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[16]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[16] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[16] .power_up = "low";

dffeas \phi_int_arr_reg[15] (
	.clk(clk),
	.d(\phi_int_arr_reg~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[15]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[15] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[15] .power_up = "low";

dffeas \phi_int_arr_reg[14] (
	.clk(clk),
	.d(\phi_int_arr_reg~2_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[14]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[14] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[14] .power_up = "low";

dffeas \phi_int_arr_reg[13] (
	.clk(clk),
	.d(\phi_int_arr_reg~3_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[13]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[13] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[13] .power_up = "low";

dffeas \phi_int_arr_reg[12] (
	.clk(clk),
	.d(\phi_int_arr_reg~4_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[12]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[12] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[12] .power_up = "low";

dffeas \phi_int_arr_reg[11] (
	.clk(clk),
	.d(\phi_int_arr_reg~5_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[11]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[11] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[11] .power_up = "low";

dffeas \phi_int_arr_reg[10] (
	.clk(clk),
	.d(\phi_int_arr_reg~6_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[10]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[10] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[10] .power_up = "low";

dffeas \phi_int_arr_reg[9] (
	.clk(clk),
	.d(\phi_int_arr_reg~7_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[9]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[9] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[9] .power_up = "low";

dffeas \phi_int_arr_reg[8] (
	.clk(clk),
	.d(\phi_int_arr_reg~8_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[8]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[8] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[8] .power_up = "low";

dffeas \phi_int_arr_reg[7] (
	.clk(clk),
	.d(\phi_int_arr_reg~9_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[7]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[7] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[7] .power_up = "low";

dffeas \phi_int_arr_reg[6] (
	.clk(clk),
	.d(\phi_int_arr_reg~10_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[6]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[6] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[6] .power_up = "low";

dffeas \phi_int_arr_reg[5] (
	.clk(clk),
	.d(\phi_int_arr_reg~11_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[5]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[5] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[5] .power_up = "low";

dffeas \phi_int_arr_reg[4] (
	.clk(clk),
	.d(\phi_int_arr_reg~12_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[4]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[4] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[4] .power_up = "low";

dffeas \phi_int_arr_reg[3] (
	.clk(clk),
	.d(\phi_int_arr_reg~13_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[3]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[3] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[3] .power_up = "low";

dffeas \phi_int_arr_reg[2] (
	.clk(clk),
	.d(\phi_int_arr_reg~14_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[2]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[2] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[2] .power_up = "low";

dffeas \phi_int_arr_reg[1] (
	.clk(clk),
	.d(\phi_int_arr_reg~15_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[1]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[1] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[1] .power_up = "low";

dffeas \phi_int_arr_reg[0] (
	.clk(clk),
	.d(\phi_int_arr_reg~16_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[0]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[0] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[0] .power_up = "low";

dffeas \phi_int_arr_reg[17] (
	.clk(clk),
	.d(\phi_int_arr_reg~17_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[17]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[17] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[17] .power_up = "low";

dffeas \phi_int_arr_reg[18] (
	.clk(clk),
	.d(\phi_int_arr_reg~18_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[18]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[18] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[18] .power_up = "low";

dffeas \phi_int_arr_reg[19] (
	.clk(clk),
	.d(\phi_int_arr_reg~19_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[19]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[19] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[19] .power_up = "low";

dffeas \phi_int_arr_reg[20] (
	.clk(clk),
	.d(\phi_int_arr_reg~20_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[20]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[20] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[20] .power_up = "low";

dffeas \phi_int_arr_reg[21] (
	.clk(clk),
	.d(\phi_int_arr_reg~21_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[21]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[21] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[21] .power_up = "low";

dffeas \phi_int_arr_reg[22] (
	.clk(clk),
	.d(\phi_int_arr_reg~22_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[22]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[22] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[22] .power_up = "low";

dffeas \phi_int_arr_reg[23] (
	.clk(clk),
	.d(\phi_int_arr_reg~23_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[23]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[23] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[23] .power_up = "low";

dffeas \phi_int_arr_reg[24] (
	.clk(clk),
	.d(\phi_int_arr_reg~24_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[24]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[24] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[24] .power_up = "low";

dffeas \phi_int_arr_reg[25] (
	.clk(clk),
	.d(\phi_int_arr_reg~25_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[25]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[25] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[25] .power_up = "low";

dffeas \phi_int_arr_reg[26] (
	.clk(clk),
	.d(\phi_int_arr_reg~26_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[26]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[26] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[26] .power_up = "low";

dffeas \phi_int_arr_reg[27] (
	.clk(clk),
	.d(\phi_int_arr_reg~27_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[27]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[27] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[27] .power_up = "low";

dffeas \phi_int_arr_reg[28] (
	.clk(clk),
	.d(\phi_int_arr_reg~28_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[28]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[28] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[28] .power_up = "low";

cycloneive_lcell_comb \phi_int_arr_reg~0 (
	.dataa(reset_n),
	.datab(phi_inc_i_16),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~0_combout ),
	.cout());
defparam \phi_int_arr_reg~0 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~0 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~1 (
	.dataa(reset_n),
	.datab(phi_inc_i_15),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~1_combout ),
	.cout());
defparam \phi_int_arr_reg~1 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~1 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~2 (
	.dataa(reset_n),
	.datab(phi_inc_i_14),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~2_combout ),
	.cout());
defparam \phi_int_arr_reg~2 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~2 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~3 (
	.dataa(reset_n),
	.datab(phi_inc_i_13),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~3_combout ),
	.cout());
defparam \phi_int_arr_reg~3 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~3 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~4 (
	.dataa(reset_n),
	.datab(phi_inc_i_12),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~4_combout ),
	.cout());
defparam \phi_int_arr_reg~4 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~4 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~5 (
	.dataa(reset_n),
	.datab(phi_inc_i_11),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~5_combout ),
	.cout());
defparam \phi_int_arr_reg~5 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~5 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~6 (
	.dataa(reset_n),
	.datab(phi_inc_i_10),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~6_combout ),
	.cout());
defparam \phi_int_arr_reg~6 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~6 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~7 (
	.dataa(reset_n),
	.datab(phi_inc_i_9),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~7_combout ),
	.cout());
defparam \phi_int_arr_reg~7 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~7 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~8 (
	.dataa(reset_n),
	.datab(phi_inc_i_8),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~8_combout ),
	.cout());
defparam \phi_int_arr_reg~8 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~8 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~9 (
	.dataa(reset_n),
	.datab(phi_inc_i_7),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~9_combout ),
	.cout());
defparam \phi_int_arr_reg~9 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~9 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~10 (
	.dataa(reset_n),
	.datab(phi_inc_i_6),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~10_combout ),
	.cout());
defparam \phi_int_arr_reg~10 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~10 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~11 (
	.dataa(reset_n),
	.datab(phi_inc_i_5),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~11_combout ),
	.cout());
defparam \phi_int_arr_reg~11 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~11 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~12 (
	.dataa(reset_n),
	.datab(phi_inc_i_4),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~12_combout ),
	.cout());
defparam \phi_int_arr_reg~12 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~12 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~13 (
	.dataa(reset_n),
	.datab(phi_inc_i_3),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~13_combout ),
	.cout());
defparam \phi_int_arr_reg~13 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~13 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~14 (
	.dataa(reset_n),
	.datab(phi_inc_i_2),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~14_combout ),
	.cout());
defparam \phi_int_arr_reg~14 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~14 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~15 (
	.dataa(reset_n),
	.datab(phi_inc_i_1),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~15_combout ),
	.cout());
defparam \phi_int_arr_reg~15 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~15 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~16 (
	.dataa(reset_n),
	.datab(phi_inc_i_0),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~16_combout ),
	.cout());
defparam \phi_int_arr_reg~16 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~16 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~17 (
	.dataa(reset_n),
	.datab(phi_inc_i_17),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~17_combout ),
	.cout());
defparam \phi_int_arr_reg~17 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~17 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~18 (
	.dataa(reset_n),
	.datab(phi_inc_i_18),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~18_combout ),
	.cout());
defparam \phi_int_arr_reg~18 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~18 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~19 (
	.dataa(reset_n),
	.datab(phi_inc_i_19),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~19_combout ),
	.cout());
defparam \phi_int_arr_reg~19 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~19 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~20 (
	.dataa(reset_n),
	.datab(phi_inc_i_20),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~20_combout ),
	.cout());
defparam \phi_int_arr_reg~20 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~20 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~21 (
	.dataa(reset_n),
	.datab(phi_inc_i_21),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~21_combout ),
	.cout());
defparam \phi_int_arr_reg~21 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~21 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~22 (
	.dataa(reset_n),
	.datab(phi_inc_i_22),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~22_combout ),
	.cout());
defparam \phi_int_arr_reg~22 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~22 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~23 (
	.dataa(reset_n),
	.datab(phi_inc_i_23),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~23_combout ),
	.cout());
defparam \phi_int_arr_reg~23 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~23 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~24 (
	.dataa(reset_n),
	.datab(phi_inc_i_24),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~24_combout ),
	.cout());
defparam \phi_int_arr_reg~24 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~24 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~25 (
	.dataa(reset_n),
	.datab(phi_inc_i_25),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~25_combout ),
	.cout());
defparam \phi_int_arr_reg~25 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~25 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~26 (
	.dataa(reset_n),
	.datab(phi_inc_i_26),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~26_combout ),
	.cout());
defparam \phi_int_arr_reg~26 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~26 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~27 (
	.dataa(reset_n),
	.datab(phi_inc_i_27),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~27_combout ),
	.cout());
defparam \phi_int_arr_reg~27 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~27 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~28 (
	.dataa(reset_n),
	.datab(phi_inc_i_28),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~28_combout ),
	.cout());
defparam \phi_int_arr_reg~28 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~28 .sum_lutc_input = "datac";

dffeas \phi_int_arr_reg[30] (
	.clk(clk),
	.d(\phi_int_arr_reg~29_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[30]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[30] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[30] .power_up = "low";

dffeas \phi_int_arr_reg[29] (
	.clk(clk),
	.d(\phi_int_arr_reg~30_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[29]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[29] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[29] .power_up = "low";

dffeas \phi_int_arr_reg[31] (
	.clk(clk),
	.d(\phi_int_arr_reg~31_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_int_arr_reg[31]~q ),
	.prn(vcc));
defparam \phi_int_arr_reg[31] .is_wysiwyg = "true";
defparam \phi_int_arr_reg[31] .power_up = "low";

cycloneive_lcell_comb \phi_int_arr_reg~29 (
	.dataa(reset_n),
	.datab(phi_inc_i_30),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~29_combout ),
	.cout());
defparam \phi_int_arr_reg~29 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~29 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~30 (
	.dataa(reset_n),
	.datab(phi_inc_i_29),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~30_combout ),
	.cout());
defparam \phi_int_arr_reg~30 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~30 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_int_arr_reg~31 (
	.dataa(reset_n),
	.datab(phi_inc_i_31),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\phi_int_arr_reg~31_combout ),
	.cout());
defparam \phi_int_arr_reg~31 .lut_mask = 16'hEEEE;
defparam \phi_int_arr_reg~31 .sum_lutc_input = "datac";

endmodule

module NCO_Party_lpm_add_sub_1 (
	pipeline_dffe_16,
	pipeline_dffe_15,
	pipeline_dffe_14,
	pipeline_dffe_13,
	pipeline_dffe_12,
	pipeline_dffe_11,
	pipeline_dffe_17,
	pipeline_dffe_18,
	pipeline_dffe_19,
	pipeline_dffe_20,
	pipeline_dffe_21,
	pipeline_dffe_22,
	pipeline_dffe_23,
	pipeline_dffe_24,
	pipeline_dffe_25,
	pipeline_dffe_26,
	pipeline_dffe_27,
	pipeline_dffe_28,
	pipeline_dffe_30,
	pipeline_dffe_29,
	pipeline_dffe_31,
	phi_int_arr_reg_16,
	phi_int_arr_reg_15,
	phi_int_arr_reg_14,
	phi_int_arr_reg_13,
	phi_int_arr_reg_12,
	phi_int_arr_reg_11,
	phi_int_arr_reg_10,
	phi_int_arr_reg_9,
	phi_int_arr_reg_8,
	phi_int_arr_reg_7,
	phi_int_arr_reg_6,
	phi_int_arr_reg_5,
	phi_int_arr_reg_4,
	phi_int_arr_reg_3,
	phi_int_arr_reg_2,
	phi_int_arr_reg_1,
	phi_int_arr_reg_0,
	phi_int_arr_reg_17,
	phi_int_arr_reg_18,
	phi_int_arr_reg_19,
	phi_int_arr_reg_20,
	phi_int_arr_reg_21,
	phi_int_arr_reg_22,
	phi_int_arr_reg_23,
	phi_int_arr_reg_24,
	phi_int_arr_reg_25,
	phi_int_arr_reg_26,
	phi_int_arr_reg_27,
	phi_int_arr_reg_28,
	phi_int_arr_reg_30,
	phi_int_arr_reg_29,
	phi_int_arr_reg_31,
	clock,
	reset_n,
	clken)/* synthesis synthesis_greybox=1 */;
output 	pipeline_dffe_16;
output 	pipeline_dffe_15;
output 	pipeline_dffe_14;
output 	pipeline_dffe_13;
output 	pipeline_dffe_12;
output 	pipeline_dffe_11;
output 	pipeline_dffe_17;
output 	pipeline_dffe_18;
output 	pipeline_dffe_19;
output 	pipeline_dffe_20;
output 	pipeline_dffe_21;
output 	pipeline_dffe_22;
output 	pipeline_dffe_23;
output 	pipeline_dffe_24;
output 	pipeline_dffe_25;
output 	pipeline_dffe_26;
output 	pipeline_dffe_27;
output 	pipeline_dffe_28;
output 	pipeline_dffe_30;
output 	pipeline_dffe_29;
output 	pipeline_dffe_31;
input 	phi_int_arr_reg_16;
input 	phi_int_arr_reg_15;
input 	phi_int_arr_reg_14;
input 	phi_int_arr_reg_13;
input 	phi_int_arr_reg_12;
input 	phi_int_arr_reg_11;
input 	phi_int_arr_reg_10;
input 	phi_int_arr_reg_9;
input 	phi_int_arr_reg_8;
input 	phi_int_arr_reg_7;
input 	phi_int_arr_reg_6;
input 	phi_int_arr_reg_5;
input 	phi_int_arr_reg_4;
input 	phi_int_arr_reg_3;
input 	phi_int_arr_reg_2;
input 	phi_int_arr_reg_1;
input 	phi_int_arr_reg_0;
input 	phi_int_arr_reg_17;
input 	phi_int_arr_reg_18;
input 	phi_int_arr_reg_19;
input 	phi_int_arr_reg_20;
input 	phi_int_arr_reg_21;
input 	phi_int_arr_reg_22;
input 	phi_int_arr_reg_23;
input 	phi_int_arr_reg_24;
input 	phi_int_arr_reg_25;
input 	phi_int_arr_reg_26;
input 	phi_int_arr_reg_27;
input 	phi_int_arr_reg_28;
input 	phi_int_arr_reg_30;
input 	phi_int_arr_reg_29;
input 	phi_int_arr_reg_31;
input 	clock;
input 	reset_n;
input 	clken;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;



NCO_Party_add_sub_v4i auto_generated(
	.pipeline_dffe_16(pipeline_dffe_16),
	.pipeline_dffe_15(pipeline_dffe_15),
	.pipeline_dffe_14(pipeline_dffe_14),
	.pipeline_dffe_13(pipeline_dffe_13),
	.pipeline_dffe_12(pipeline_dffe_12),
	.pipeline_dffe_11(pipeline_dffe_11),
	.pipeline_dffe_17(pipeline_dffe_17),
	.pipeline_dffe_18(pipeline_dffe_18),
	.pipeline_dffe_19(pipeline_dffe_19),
	.pipeline_dffe_20(pipeline_dffe_20),
	.pipeline_dffe_21(pipeline_dffe_21),
	.pipeline_dffe_22(pipeline_dffe_22),
	.pipeline_dffe_23(pipeline_dffe_23),
	.pipeline_dffe_24(pipeline_dffe_24),
	.pipeline_dffe_25(pipeline_dffe_25),
	.pipeline_dffe_26(pipeline_dffe_26),
	.pipeline_dffe_27(pipeline_dffe_27),
	.pipeline_dffe_28(pipeline_dffe_28),
	.pipeline_dffe_30(pipeline_dffe_30),
	.pipeline_dffe_29(pipeline_dffe_29),
	.pipeline_dffe_31(pipeline_dffe_31),
	.phi_int_arr_reg_16(phi_int_arr_reg_16),
	.phi_int_arr_reg_15(phi_int_arr_reg_15),
	.phi_int_arr_reg_14(phi_int_arr_reg_14),
	.phi_int_arr_reg_13(phi_int_arr_reg_13),
	.phi_int_arr_reg_12(phi_int_arr_reg_12),
	.phi_int_arr_reg_11(phi_int_arr_reg_11),
	.phi_int_arr_reg_10(phi_int_arr_reg_10),
	.phi_int_arr_reg_9(phi_int_arr_reg_9),
	.phi_int_arr_reg_8(phi_int_arr_reg_8),
	.phi_int_arr_reg_7(phi_int_arr_reg_7),
	.phi_int_arr_reg_6(phi_int_arr_reg_6),
	.phi_int_arr_reg_5(phi_int_arr_reg_5),
	.phi_int_arr_reg_4(phi_int_arr_reg_4),
	.phi_int_arr_reg_3(phi_int_arr_reg_3),
	.phi_int_arr_reg_2(phi_int_arr_reg_2),
	.phi_int_arr_reg_1(phi_int_arr_reg_1),
	.phi_int_arr_reg_0(phi_int_arr_reg_0),
	.phi_int_arr_reg_17(phi_int_arr_reg_17),
	.phi_int_arr_reg_18(phi_int_arr_reg_18),
	.phi_int_arr_reg_19(phi_int_arr_reg_19),
	.phi_int_arr_reg_20(phi_int_arr_reg_20),
	.phi_int_arr_reg_21(phi_int_arr_reg_21),
	.phi_int_arr_reg_22(phi_int_arr_reg_22),
	.phi_int_arr_reg_23(phi_int_arr_reg_23),
	.phi_int_arr_reg_24(phi_int_arr_reg_24),
	.phi_int_arr_reg_25(phi_int_arr_reg_25),
	.phi_int_arr_reg_26(phi_int_arr_reg_26),
	.phi_int_arr_reg_27(phi_int_arr_reg_27),
	.phi_int_arr_reg_28(phi_int_arr_reg_28),
	.phi_int_arr_reg_30(phi_int_arr_reg_30),
	.phi_int_arr_reg_29(phi_int_arr_reg_29),
	.phi_int_arr_reg_31(phi_int_arr_reg_31),
	.clock(clock),
	.reset_n(reset_n),
	.clken(clken));

endmodule

module NCO_Party_add_sub_v4i (
	pipeline_dffe_16,
	pipeline_dffe_15,
	pipeline_dffe_14,
	pipeline_dffe_13,
	pipeline_dffe_12,
	pipeline_dffe_11,
	pipeline_dffe_17,
	pipeline_dffe_18,
	pipeline_dffe_19,
	pipeline_dffe_20,
	pipeline_dffe_21,
	pipeline_dffe_22,
	pipeline_dffe_23,
	pipeline_dffe_24,
	pipeline_dffe_25,
	pipeline_dffe_26,
	pipeline_dffe_27,
	pipeline_dffe_28,
	pipeline_dffe_30,
	pipeline_dffe_29,
	pipeline_dffe_31,
	phi_int_arr_reg_16,
	phi_int_arr_reg_15,
	phi_int_arr_reg_14,
	phi_int_arr_reg_13,
	phi_int_arr_reg_12,
	phi_int_arr_reg_11,
	phi_int_arr_reg_10,
	phi_int_arr_reg_9,
	phi_int_arr_reg_8,
	phi_int_arr_reg_7,
	phi_int_arr_reg_6,
	phi_int_arr_reg_5,
	phi_int_arr_reg_4,
	phi_int_arr_reg_3,
	phi_int_arr_reg_2,
	phi_int_arr_reg_1,
	phi_int_arr_reg_0,
	phi_int_arr_reg_17,
	phi_int_arr_reg_18,
	phi_int_arr_reg_19,
	phi_int_arr_reg_20,
	phi_int_arr_reg_21,
	phi_int_arr_reg_22,
	phi_int_arr_reg_23,
	phi_int_arr_reg_24,
	phi_int_arr_reg_25,
	phi_int_arr_reg_26,
	phi_int_arr_reg_27,
	phi_int_arr_reg_28,
	phi_int_arr_reg_30,
	phi_int_arr_reg_29,
	phi_int_arr_reg_31,
	clock,
	reset_n,
	clken)/* synthesis synthesis_greybox=1 */;
output 	pipeline_dffe_16;
output 	pipeline_dffe_15;
output 	pipeline_dffe_14;
output 	pipeline_dffe_13;
output 	pipeline_dffe_12;
output 	pipeline_dffe_11;
output 	pipeline_dffe_17;
output 	pipeline_dffe_18;
output 	pipeline_dffe_19;
output 	pipeline_dffe_20;
output 	pipeline_dffe_21;
output 	pipeline_dffe_22;
output 	pipeline_dffe_23;
output 	pipeline_dffe_24;
output 	pipeline_dffe_25;
output 	pipeline_dffe_26;
output 	pipeline_dffe_27;
output 	pipeline_dffe_28;
output 	pipeline_dffe_30;
output 	pipeline_dffe_29;
output 	pipeline_dffe_31;
input 	phi_int_arr_reg_16;
input 	phi_int_arr_reg_15;
input 	phi_int_arr_reg_14;
input 	phi_int_arr_reg_13;
input 	phi_int_arr_reg_12;
input 	phi_int_arr_reg_11;
input 	phi_int_arr_reg_10;
input 	phi_int_arr_reg_9;
input 	phi_int_arr_reg_8;
input 	phi_int_arr_reg_7;
input 	phi_int_arr_reg_6;
input 	phi_int_arr_reg_5;
input 	phi_int_arr_reg_4;
input 	phi_int_arr_reg_3;
input 	phi_int_arr_reg_2;
input 	phi_int_arr_reg_1;
input 	phi_int_arr_reg_0;
input 	phi_int_arr_reg_17;
input 	phi_int_arr_reg_18;
input 	phi_int_arr_reg_19;
input 	phi_int_arr_reg_20;
input 	phi_int_arr_reg_21;
input 	phi_int_arr_reg_22;
input 	phi_int_arr_reg_23;
input 	phi_int_arr_reg_24;
input 	phi_int_arr_reg_25;
input 	phi_int_arr_reg_26;
input 	phi_int_arr_reg_27;
input 	phi_int_arr_reg_28;
input 	phi_int_arr_reg_30;
input 	phi_int_arr_reg_29;
input 	phi_int_arr_reg_31;
input 	clock;
input 	reset_n;
input 	clken;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \pipeline_dffe[0]~32_combout ;
wire \pipeline_dffe[0]~q ;
wire \pipeline_dffe[0]~33 ;
wire \pipeline_dffe[1]~34_combout ;
wire \pipeline_dffe[1]~q ;
wire \pipeline_dffe[1]~35 ;
wire \pipeline_dffe[2]~36_combout ;
wire \pipeline_dffe[2]~q ;
wire \pipeline_dffe[2]~37 ;
wire \pipeline_dffe[3]~38_combout ;
wire \pipeline_dffe[3]~q ;
wire \pipeline_dffe[3]~39 ;
wire \pipeline_dffe[4]~40_combout ;
wire \pipeline_dffe[4]~q ;
wire \pipeline_dffe[4]~41 ;
wire \pipeline_dffe[5]~42_combout ;
wire \pipeline_dffe[5]~q ;
wire \pipeline_dffe[5]~43 ;
wire \pipeline_dffe[6]~44_combout ;
wire \pipeline_dffe[6]~q ;
wire \pipeline_dffe[6]~45 ;
wire \pipeline_dffe[7]~46_combout ;
wire \pipeline_dffe[7]~q ;
wire \pipeline_dffe[7]~47 ;
wire \pipeline_dffe[8]~48_combout ;
wire \pipeline_dffe[8]~q ;
wire \pipeline_dffe[8]~49 ;
wire \pipeline_dffe[9]~50_combout ;
wire \pipeline_dffe[9]~q ;
wire \pipeline_dffe[9]~51 ;
wire \pipeline_dffe[10]~52_combout ;
wire \pipeline_dffe[10]~q ;
wire \pipeline_dffe[10]~53 ;
wire \pipeline_dffe[11]~55 ;
wire \pipeline_dffe[12]~57 ;
wire \pipeline_dffe[13]~59 ;
wire \pipeline_dffe[14]~61 ;
wire \pipeline_dffe[15]~63 ;
wire \pipeline_dffe[16]~64_combout ;
wire \pipeline_dffe[15]~62_combout ;
wire \pipeline_dffe[14]~60_combout ;
wire \pipeline_dffe[13]~58_combout ;
wire \pipeline_dffe[12]~56_combout ;
wire \pipeline_dffe[11]~54_combout ;
wire \pipeline_dffe[16]~65 ;
wire \pipeline_dffe[17]~66_combout ;
wire \pipeline_dffe[17]~67 ;
wire \pipeline_dffe[18]~68_combout ;
wire \pipeline_dffe[18]~69 ;
wire \pipeline_dffe[19]~70_combout ;
wire \pipeline_dffe[19]~71 ;
wire \pipeline_dffe[20]~72_combout ;
wire \pipeline_dffe[20]~73 ;
wire \pipeline_dffe[21]~74_combout ;
wire \pipeline_dffe[21]~75 ;
wire \pipeline_dffe[22]~76_combout ;
wire \pipeline_dffe[22]~77 ;
wire \pipeline_dffe[23]~78_combout ;
wire \pipeline_dffe[23]~79 ;
wire \pipeline_dffe[24]~80_combout ;
wire \pipeline_dffe[24]~81 ;
wire \pipeline_dffe[25]~82_combout ;
wire \pipeline_dffe[25]~83 ;
wire \pipeline_dffe[26]~84_combout ;
wire \pipeline_dffe[26]~85 ;
wire \pipeline_dffe[27]~86_combout ;
wire \pipeline_dffe[27]~87 ;
wire \pipeline_dffe[28]~88_combout ;
wire \pipeline_dffe[28]~89 ;
wire \pipeline_dffe[29]~91 ;
wire \pipeline_dffe[30]~92_combout ;
wire \pipeline_dffe[29]~90_combout ;
wire \pipeline_dffe[30]~93 ;
wire \pipeline_dffe[31]~94_combout ;


dffeas \pipeline_dffe[16] (
	.clk(clock),
	.d(\pipeline_dffe[16]~64_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_16),
	.prn(vcc));
defparam \pipeline_dffe[16] .is_wysiwyg = "true";
defparam \pipeline_dffe[16] .power_up = "low";

dffeas \pipeline_dffe[15] (
	.clk(clock),
	.d(\pipeline_dffe[15]~62_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_15),
	.prn(vcc));
defparam \pipeline_dffe[15] .is_wysiwyg = "true";
defparam \pipeline_dffe[15] .power_up = "low";

dffeas \pipeline_dffe[14] (
	.clk(clock),
	.d(\pipeline_dffe[14]~60_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_14),
	.prn(vcc));
defparam \pipeline_dffe[14] .is_wysiwyg = "true";
defparam \pipeline_dffe[14] .power_up = "low";

dffeas \pipeline_dffe[13] (
	.clk(clock),
	.d(\pipeline_dffe[13]~58_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_13),
	.prn(vcc));
defparam \pipeline_dffe[13] .is_wysiwyg = "true";
defparam \pipeline_dffe[13] .power_up = "low";

dffeas \pipeline_dffe[12] (
	.clk(clock),
	.d(\pipeline_dffe[12]~56_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_12),
	.prn(vcc));
defparam \pipeline_dffe[12] .is_wysiwyg = "true";
defparam \pipeline_dffe[12] .power_up = "low";

dffeas \pipeline_dffe[11] (
	.clk(clock),
	.d(\pipeline_dffe[11]~54_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_11),
	.prn(vcc));
defparam \pipeline_dffe[11] .is_wysiwyg = "true";
defparam \pipeline_dffe[11] .power_up = "low";

dffeas \pipeline_dffe[17] (
	.clk(clock),
	.d(\pipeline_dffe[17]~66_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_17),
	.prn(vcc));
defparam \pipeline_dffe[17] .is_wysiwyg = "true";
defparam \pipeline_dffe[17] .power_up = "low";

dffeas \pipeline_dffe[18] (
	.clk(clock),
	.d(\pipeline_dffe[18]~68_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_18),
	.prn(vcc));
defparam \pipeline_dffe[18] .is_wysiwyg = "true";
defparam \pipeline_dffe[18] .power_up = "low";

dffeas \pipeline_dffe[19] (
	.clk(clock),
	.d(\pipeline_dffe[19]~70_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_19),
	.prn(vcc));
defparam \pipeline_dffe[19] .is_wysiwyg = "true";
defparam \pipeline_dffe[19] .power_up = "low";

dffeas \pipeline_dffe[20] (
	.clk(clock),
	.d(\pipeline_dffe[20]~72_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_20),
	.prn(vcc));
defparam \pipeline_dffe[20] .is_wysiwyg = "true";
defparam \pipeline_dffe[20] .power_up = "low";

dffeas \pipeline_dffe[21] (
	.clk(clock),
	.d(\pipeline_dffe[21]~74_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_21),
	.prn(vcc));
defparam \pipeline_dffe[21] .is_wysiwyg = "true";
defparam \pipeline_dffe[21] .power_up = "low";

dffeas \pipeline_dffe[22] (
	.clk(clock),
	.d(\pipeline_dffe[22]~76_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_22),
	.prn(vcc));
defparam \pipeline_dffe[22] .is_wysiwyg = "true";
defparam \pipeline_dffe[22] .power_up = "low";

dffeas \pipeline_dffe[23] (
	.clk(clock),
	.d(\pipeline_dffe[23]~78_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_23),
	.prn(vcc));
defparam \pipeline_dffe[23] .is_wysiwyg = "true";
defparam \pipeline_dffe[23] .power_up = "low";

dffeas \pipeline_dffe[24] (
	.clk(clock),
	.d(\pipeline_dffe[24]~80_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_24),
	.prn(vcc));
defparam \pipeline_dffe[24] .is_wysiwyg = "true";
defparam \pipeline_dffe[24] .power_up = "low";

dffeas \pipeline_dffe[25] (
	.clk(clock),
	.d(\pipeline_dffe[25]~82_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_25),
	.prn(vcc));
defparam \pipeline_dffe[25] .is_wysiwyg = "true";
defparam \pipeline_dffe[25] .power_up = "low";

dffeas \pipeline_dffe[26] (
	.clk(clock),
	.d(\pipeline_dffe[26]~84_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_26),
	.prn(vcc));
defparam \pipeline_dffe[26] .is_wysiwyg = "true";
defparam \pipeline_dffe[26] .power_up = "low";

dffeas \pipeline_dffe[27] (
	.clk(clock),
	.d(\pipeline_dffe[27]~86_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_27),
	.prn(vcc));
defparam \pipeline_dffe[27] .is_wysiwyg = "true";
defparam \pipeline_dffe[27] .power_up = "low";

dffeas \pipeline_dffe[28] (
	.clk(clock),
	.d(\pipeline_dffe[28]~88_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_28),
	.prn(vcc));
defparam \pipeline_dffe[28] .is_wysiwyg = "true";
defparam \pipeline_dffe[28] .power_up = "low";

dffeas \pipeline_dffe[30] (
	.clk(clock),
	.d(\pipeline_dffe[30]~92_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_30),
	.prn(vcc));
defparam \pipeline_dffe[30] .is_wysiwyg = "true";
defparam \pipeline_dffe[30] .power_up = "low";

dffeas \pipeline_dffe[29] (
	.clk(clock),
	.d(\pipeline_dffe[29]~90_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_29),
	.prn(vcc));
defparam \pipeline_dffe[29] .is_wysiwyg = "true";
defparam \pipeline_dffe[29] .power_up = "low";

dffeas \pipeline_dffe[31] (
	.clk(clock),
	.d(\pipeline_dffe[31]~94_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(pipeline_dffe_31),
	.prn(vcc));
defparam \pipeline_dffe[31] .is_wysiwyg = "true";
defparam \pipeline_dffe[31] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[0]~32 (
	.dataa(\pipeline_dffe[0]~q ),
	.datab(phi_int_arr_reg_0),
	.datac(gnd),
	.datad(vcc),
	.cin(gnd),
	.combout(\pipeline_dffe[0]~32_combout ),
	.cout(\pipeline_dffe[0]~33 ));
defparam \pipeline_dffe[0]~32 .lut_mask = 16'h66EE;
defparam \pipeline_dffe[0]~32 .sum_lutc_input = "datac";

dffeas \pipeline_dffe[0] (
	.clk(clock),
	.d(\pipeline_dffe[0]~32_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[0]~q ),
	.prn(vcc));
defparam \pipeline_dffe[0] .is_wysiwyg = "true";
defparam \pipeline_dffe[0] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[1]~34 (
	.dataa(\pipeline_dffe[1]~q ),
	.datab(phi_int_arr_reg_1),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[0]~33 ),
	.combout(\pipeline_dffe[1]~34_combout ),
	.cout(\pipeline_dffe[1]~35 ));
defparam \pipeline_dffe[1]~34 .lut_mask = 16'h967F;
defparam \pipeline_dffe[1]~34 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[1] (
	.clk(clock),
	.d(\pipeline_dffe[1]~34_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[1]~q ),
	.prn(vcc));
defparam \pipeline_dffe[1] .is_wysiwyg = "true";
defparam \pipeline_dffe[1] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[2]~36 (
	.dataa(\pipeline_dffe[2]~q ),
	.datab(phi_int_arr_reg_2),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[1]~35 ),
	.combout(\pipeline_dffe[2]~36_combout ),
	.cout(\pipeline_dffe[2]~37 ));
defparam \pipeline_dffe[2]~36 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[2]~36 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[2] (
	.clk(clock),
	.d(\pipeline_dffe[2]~36_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[2]~q ),
	.prn(vcc));
defparam \pipeline_dffe[2] .is_wysiwyg = "true";
defparam \pipeline_dffe[2] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[3]~38 (
	.dataa(\pipeline_dffe[3]~q ),
	.datab(phi_int_arr_reg_3),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[2]~37 ),
	.combout(\pipeline_dffe[3]~38_combout ),
	.cout(\pipeline_dffe[3]~39 ));
defparam \pipeline_dffe[3]~38 .lut_mask = 16'h967F;
defparam \pipeline_dffe[3]~38 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[3] (
	.clk(clock),
	.d(\pipeline_dffe[3]~38_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[3]~q ),
	.prn(vcc));
defparam \pipeline_dffe[3] .is_wysiwyg = "true";
defparam \pipeline_dffe[3] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[4]~40 (
	.dataa(\pipeline_dffe[4]~q ),
	.datab(phi_int_arr_reg_4),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[3]~39 ),
	.combout(\pipeline_dffe[4]~40_combout ),
	.cout(\pipeline_dffe[4]~41 ));
defparam \pipeline_dffe[4]~40 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[4]~40 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[4] (
	.clk(clock),
	.d(\pipeline_dffe[4]~40_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[4]~q ),
	.prn(vcc));
defparam \pipeline_dffe[4] .is_wysiwyg = "true";
defparam \pipeline_dffe[4] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[5]~42 (
	.dataa(\pipeline_dffe[5]~q ),
	.datab(phi_int_arr_reg_5),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[4]~41 ),
	.combout(\pipeline_dffe[5]~42_combout ),
	.cout(\pipeline_dffe[5]~43 ));
defparam \pipeline_dffe[5]~42 .lut_mask = 16'h967F;
defparam \pipeline_dffe[5]~42 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[5] (
	.clk(clock),
	.d(\pipeline_dffe[5]~42_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[5]~q ),
	.prn(vcc));
defparam \pipeline_dffe[5] .is_wysiwyg = "true";
defparam \pipeline_dffe[5] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[6]~44 (
	.dataa(\pipeline_dffe[6]~q ),
	.datab(phi_int_arr_reg_6),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[5]~43 ),
	.combout(\pipeline_dffe[6]~44_combout ),
	.cout(\pipeline_dffe[6]~45 ));
defparam \pipeline_dffe[6]~44 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[6]~44 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[6] (
	.clk(clock),
	.d(\pipeline_dffe[6]~44_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[6]~q ),
	.prn(vcc));
defparam \pipeline_dffe[6] .is_wysiwyg = "true";
defparam \pipeline_dffe[6] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[7]~46 (
	.dataa(\pipeline_dffe[7]~q ),
	.datab(phi_int_arr_reg_7),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[6]~45 ),
	.combout(\pipeline_dffe[7]~46_combout ),
	.cout(\pipeline_dffe[7]~47 ));
defparam \pipeline_dffe[7]~46 .lut_mask = 16'h967F;
defparam \pipeline_dffe[7]~46 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[7] (
	.clk(clock),
	.d(\pipeline_dffe[7]~46_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[7]~q ),
	.prn(vcc));
defparam \pipeline_dffe[7] .is_wysiwyg = "true";
defparam \pipeline_dffe[7] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[8]~48 (
	.dataa(\pipeline_dffe[8]~q ),
	.datab(phi_int_arr_reg_8),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[7]~47 ),
	.combout(\pipeline_dffe[8]~48_combout ),
	.cout(\pipeline_dffe[8]~49 ));
defparam \pipeline_dffe[8]~48 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[8]~48 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[8] (
	.clk(clock),
	.d(\pipeline_dffe[8]~48_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[8]~q ),
	.prn(vcc));
defparam \pipeline_dffe[8] .is_wysiwyg = "true";
defparam \pipeline_dffe[8] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[9]~50 (
	.dataa(\pipeline_dffe[9]~q ),
	.datab(phi_int_arr_reg_9),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[8]~49 ),
	.combout(\pipeline_dffe[9]~50_combout ),
	.cout(\pipeline_dffe[9]~51 ));
defparam \pipeline_dffe[9]~50 .lut_mask = 16'h967F;
defparam \pipeline_dffe[9]~50 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[9] (
	.clk(clock),
	.d(\pipeline_dffe[9]~50_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[9]~q ),
	.prn(vcc));
defparam \pipeline_dffe[9] .is_wysiwyg = "true";
defparam \pipeline_dffe[9] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[10]~52 (
	.dataa(\pipeline_dffe[10]~q ),
	.datab(phi_int_arr_reg_10),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[9]~51 ),
	.combout(\pipeline_dffe[10]~52_combout ),
	.cout(\pipeline_dffe[10]~53 ));
defparam \pipeline_dffe[10]~52 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[10]~52 .sum_lutc_input = "cin";

dffeas \pipeline_dffe[10] (
	.clk(clock),
	.d(\pipeline_dffe[10]~52_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clken),
	.q(\pipeline_dffe[10]~q ),
	.prn(vcc));
defparam \pipeline_dffe[10] .is_wysiwyg = "true";
defparam \pipeline_dffe[10] .power_up = "low";

cycloneive_lcell_comb \pipeline_dffe[11]~54 (
	.dataa(pipeline_dffe_11),
	.datab(phi_int_arr_reg_11),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[10]~53 ),
	.combout(\pipeline_dffe[11]~54_combout ),
	.cout(\pipeline_dffe[11]~55 ));
defparam \pipeline_dffe[11]~54 .lut_mask = 16'h967F;
defparam \pipeline_dffe[11]~54 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[12]~56 (
	.dataa(pipeline_dffe_12),
	.datab(phi_int_arr_reg_12),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[11]~55 ),
	.combout(\pipeline_dffe[12]~56_combout ),
	.cout(\pipeline_dffe[12]~57 ));
defparam \pipeline_dffe[12]~56 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[12]~56 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[13]~58 (
	.dataa(pipeline_dffe_13),
	.datab(phi_int_arr_reg_13),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[12]~57 ),
	.combout(\pipeline_dffe[13]~58_combout ),
	.cout(\pipeline_dffe[13]~59 ));
defparam \pipeline_dffe[13]~58 .lut_mask = 16'h967F;
defparam \pipeline_dffe[13]~58 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[14]~60 (
	.dataa(pipeline_dffe_14),
	.datab(phi_int_arr_reg_14),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[13]~59 ),
	.combout(\pipeline_dffe[14]~60_combout ),
	.cout(\pipeline_dffe[14]~61 ));
defparam \pipeline_dffe[14]~60 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[14]~60 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[15]~62 (
	.dataa(pipeline_dffe_15),
	.datab(phi_int_arr_reg_15),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[14]~61 ),
	.combout(\pipeline_dffe[15]~62_combout ),
	.cout(\pipeline_dffe[15]~63 ));
defparam \pipeline_dffe[15]~62 .lut_mask = 16'h967F;
defparam \pipeline_dffe[15]~62 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[16]~64 (
	.dataa(pipeline_dffe_16),
	.datab(phi_int_arr_reg_16),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[15]~63 ),
	.combout(\pipeline_dffe[16]~64_combout ),
	.cout(\pipeline_dffe[16]~65 ));
defparam \pipeline_dffe[16]~64 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[16]~64 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[17]~66 (
	.dataa(pipeline_dffe_17),
	.datab(phi_int_arr_reg_17),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[16]~65 ),
	.combout(\pipeline_dffe[17]~66_combout ),
	.cout(\pipeline_dffe[17]~67 ));
defparam \pipeline_dffe[17]~66 .lut_mask = 16'h967F;
defparam \pipeline_dffe[17]~66 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[18]~68 (
	.dataa(pipeline_dffe_18),
	.datab(phi_int_arr_reg_18),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[17]~67 ),
	.combout(\pipeline_dffe[18]~68_combout ),
	.cout(\pipeline_dffe[18]~69 ));
defparam \pipeline_dffe[18]~68 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[18]~68 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[19]~70 (
	.dataa(pipeline_dffe_19),
	.datab(phi_int_arr_reg_19),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[18]~69 ),
	.combout(\pipeline_dffe[19]~70_combout ),
	.cout(\pipeline_dffe[19]~71 ));
defparam \pipeline_dffe[19]~70 .lut_mask = 16'h967F;
defparam \pipeline_dffe[19]~70 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[20]~72 (
	.dataa(pipeline_dffe_20),
	.datab(phi_int_arr_reg_20),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[19]~71 ),
	.combout(\pipeline_dffe[20]~72_combout ),
	.cout(\pipeline_dffe[20]~73 ));
defparam \pipeline_dffe[20]~72 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[20]~72 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[21]~74 (
	.dataa(pipeline_dffe_21),
	.datab(phi_int_arr_reg_21),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[20]~73 ),
	.combout(\pipeline_dffe[21]~74_combout ),
	.cout(\pipeline_dffe[21]~75 ));
defparam \pipeline_dffe[21]~74 .lut_mask = 16'h967F;
defparam \pipeline_dffe[21]~74 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[22]~76 (
	.dataa(pipeline_dffe_22),
	.datab(phi_int_arr_reg_22),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[21]~75 ),
	.combout(\pipeline_dffe[22]~76_combout ),
	.cout(\pipeline_dffe[22]~77 ));
defparam \pipeline_dffe[22]~76 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[22]~76 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[23]~78 (
	.dataa(pipeline_dffe_23),
	.datab(phi_int_arr_reg_23),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[22]~77 ),
	.combout(\pipeline_dffe[23]~78_combout ),
	.cout(\pipeline_dffe[23]~79 ));
defparam \pipeline_dffe[23]~78 .lut_mask = 16'h967F;
defparam \pipeline_dffe[23]~78 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[24]~80 (
	.dataa(pipeline_dffe_24),
	.datab(phi_int_arr_reg_24),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[23]~79 ),
	.combout(\pipeline_dffe[24]~80_combout ),
	.cout(\pipeline_dffe[24]~81 ));
defparam \pipeline_dffe[24]~80 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[24]~80 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[25]~82 (
	.dataa(pipeline_dffe_25),
	.datab(phi_int_arr_reg_25),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[24]~81 ),
	.combout(\pipeline_dffe[25]~82_combout ),
	.cout(\pipeline_dffe[25]~83 ));
defparam \pipeline_dffe[25]~82 .lut_mask = 16'h967F;
defparam \pipeline_dffe[25]~82 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[26]~84 (
	.dataa(pipeline_dffe_26),
	.datab(phi_int_arr_reg_26),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[25]~83 ),
	.combout(\pipeline_dffe[26]~84_combout ),
	.cout(\pipeline_dffe[26]~85 ));
defparam \pipeline_dffe[26]~84 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[26]~84 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[27]~86 (
	.dataa(pipeline_dffe_27),
	.datab(phi_int_arr_reg_27),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[26]~85 ),
	.combout(\pipeline_dffe[27]~86_combout ),
	.cout(\pipeline_dffe[27]~87 ));
defparam \pipeline_dffe[27]~86 .lut_mask = 16'h967F;
defparam \pipeline_dffe[27]~86 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[28]~88 (
	.dataa(pipeline_dffe_28),
	.datab(phi_int_arr_reg_28),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[27]~87 ),
	.combout(\pipeline_dffe[28]~88_combout ),
	.cout(\pipeline_dffe[28]~89 ));
defparam \pipeline_dffe[28]~88 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[28]~88 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[29]~90 (
	.dataa(pipeline_dffe_29),
	.datab(phi_int_arr_reg_29),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[28]~89 ),
	.combout(\pipeline_dffe[29]~90_combout ),
	.cout(\pipeline_dffe[29]~91 ));
defparam \pipeline_dffe[29]~90 .lut_mask = 16'h967F;
defparam \pipeline_dffe[29]~90 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[30]~92 (
	.dataa(pipeline_dffe_30),
	.datab(phi_int_arr_reg_30),
	.datac(gnd),
	.datad(vcc),
	.cin(\pipeline_dffe[29]~91 ),
	.combout(\pipeline_dffe[30]~92_combout ),
	.cout(\pipeline_dffe[30]~93 ));
defparam \pipeline_dffe[30]~92 .lut_mask = 16'h96EF;
defparam \pipeline_dffe[30]~92 .sum_lutc_input = "cin";

cycloneive_lcell_comb \pipeline_dffe[31]~94 (
	.dataa(pipeline_dffe_31),
	.datab(phi_int_arr_reg_31),
	.datac(gnd),
	.datad(gnd),
	.cin(\pipeline_dffe[30]~93 ),
	.combout(\pipeline_dffe[31]~94_combout ),
	.cout());
defparam \pipeline_dffe[31]~94 .lut_mask = 16'h9696;
defparam \pipeline_dffe[31]~94 .sum_lutc_input = "cin";

endmodule

module NCO_Party_asj_dxx (
	dxxrv_3,
	pipeline_dffe_16,
	pipeline_dffe_15,
	pipeline_dffe_14,
	dxxrv_2,
	pipeline_dffe_13,
	dxxrv_1,
	pipeline_dffe_12,
	dxxrv_0,
	pipeline_dffe_11,
	pipeline_dffe_17,
	pipeline_dffe_18,
	pipeline_dffe_19,
	pipeline_dffe_20,
	pipeline_dffe_21,
	pipeline_dffe_22,
	pipeline_dffe_23,
	pipeline_dffe_24,
	pipeline_dffe_25,
	pipeline_dffe_26,
	pipeline_dffe_27,
	pipeline_dffe_28,
	pipeline_dffe_30,
	pipeline_dffe_29,
	pipeline_dffe_31,
	data_out_8,
	dxxpdo_5,
	dxxpdo_6,
	dxxpdo_7,
	dxxpdo_8,
	dxxpdo_9,
	dxxpdo_10,
	dxxpdo_11,
	dxxpdo_12,
	dxxpdo_13,
	dxxpdo_14,
	dxxpdo_15,
	dxxpdo_16,
	dxxpdo_17,
	dxxpdo_19,
	dxxpdo_18,
	dxxpdo_20,
	clk,
	reset_n)/* synthesis synthesis_greybox=1 */;
input 	dxxrv_3;
input 	pipeline_dffe_16;
input 	pipeline_dffe_15;
input 	pipeline_dffe_14;
input 	dxxrv_2;
input 	pipeline_dffe_13;
input 	dxxrv_1;
input 	pipeline_dffe_12;
input 	dxxrv_0;
input 	pipeline_dffe_11;
input 	pipeline_dffe_17;
input 	pipeline_dffe_18;
input 	pipeline_dffe_19;
input 	pipeline_dffe_20;
input 	pipeline_dffe_21;
input 	pipeline_dffe_22;
input 	pipeline_dffe_23;
input 	pipeline_dffe_24;
input 	pipeline_dffe_25;
input 	pipeline_dffe_26;
input 	pipeline_dffe_27;
input 	pipeline_dffe_28;
input 	pipeline_dffe_30;
input 	pipeline_dffe_29;
input 	pipeline_dffe_31;
input 	data_out_8;
output 	dxxpdo_5;
output 	dxxpdo_6;
output 	dxxpdo_7;
output 	dxxpdo_8;
output 	dxxpdo_9;
output 	dxxpdo_10;
output 	dxxpdo_11;
output 	dxxpdo_12;
output 	dxxpdo_13;
output 	dxxpdo_14;
output 	dxxpdo_15;
output 	dxxpdo_16;
output 	dxxpdo_17;
output 	dxxpdo_19;
output 	dxxpdo_18;
output 	dxxpdo_20;
input 	clk;
input 	reset_n;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \phi_dither_out_w[5]~17_cout ;
wire \phi_dither_out_w[5]~19_cout ;
wire \phi_dither_out_w[5]~21_cout ;
wire \phi_dither_out_w[5]~23_cout ;
wire \phi_dither_out_w[5]~25_cout ;
wire \phi_dither_out_w[5]~26_combout ;
wire \phi_dither_out_w[5]~q ;
wire \dxxpdo~0_combout ;
wire \phi_dither_out_w[5]~27 ;
wire \phi_dither_out_w[6]~28_combout ;
wire \phi_dither_out_w[6]~q ;
wire \dxxpdo~1_combout ;
wire \phi_dither_out_w[6]~29 ;
wire \phi_dither_out_w[7]~30_combout ;
wire \phi_dither_out_w[7]~q ;
wire \dxxpdo~2_combout ;
wire \phi_dither_out_w[7]~31 ;
wire \phi_dither_out_w[8]~32_combout ;
wire \phi_dither_out_w[8]~q ;
wire \dxxpdo~3_combout ;
wire \phi_dither_out_w[8]~33 ;
wire \phi_dither_out_w[9]~34_combout ;
wire \phi_dither_out_w[9]~q ;
wire \dxxpdo~4_combout ;
wire \phi_dither_out_w[9]~35 ;
wire \phi_dither_out_w[10]~36_combout ;
wire \phi_dither_out_w[10]~q ;
wire \dxxpdo~5_combout ;
wire \phi_dither_out_w[10]~37 ;
wire \phi_dither_out_w[11]~38_combout ;
wire \phi_dither_out_w[11]~q ;
wire \dxxpdo~6_combout ;
wire \phi_dither_out_w[11]~39 ;
wire \phi_dither_out_w[12]~40_combout ;
wire \phi_dither_out_w[12]~q ;
wire \dxxpdo~7_combout ;
wire \phi_dither_out_w[12]~41 ;
wire \phi_dither_out_w[13]~42_combout ;
wire \phi_dither_out_w[13]~q ;
wire \dxxpdo~8_combout ;
wire \phi_dither_out_w[13]~43 ;
wire \phi_dither_out_w[14]~44_combout ;
wire \phi_dither_out_w[14]~q ;
wire \dxxpdo~9_combout ;
wire \phi_dither_out_w[14]~45 ;
wire \phi_dither_out_w[15]~46_combout ;
wire \phi_dither_out_w[15]~q ;
wire \dxxpdo~10_combout ;
wire \phi_dither_out_w[15]~47 ;
wire \phi_dither_out_w[16]~48_combout ;
wire \phi_dither_out_w[16]~q ;
wire \dxxpdo~11_combout ;
wire \phi_dither_out_w[16]~49 ;
wire \phi_dither_out_w[17]~50_combout ;
wire \phi_dither_out_w[17]~q ;
wire \dxxpdo~12_combout ;
wire \phi_dither_out_w[17]~51 ;
wire \phi_dither_out_w[18]~53 ;
wire \phi_dither_out_w[19]~54_combout ;
wire \phi_dither_out_w[19]~q ;
wire \dxxpdo~13_combout ;
wire \phi_dither_out_w[18]~52_combout ;
wire \phi_dither_out_w[18]~q ;
wire \dxxpdo~14_combout ;
wire \phi_dither_out_w[19]~55 ;
wire \phi_dither_out_w[20]~56_combout ;
wire \phi_dither_out_w[20]~q ;
wire \dxxpdo~15_combout ;


dffeas \dxxpdo[5] (
	.clk(clk),
	.d(\dxxpdo~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_5),
	.prn(vcc));
defparam \dxxpdo[5] .is_wysiwyg = "true";
defparam \dxxpdo[5] .power_up = "low";

dffeas \dxxpdo[6] (
	.clk(clk),
	.d(\dxxpdo~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_6),
	.prn(vcc));
defparam \dxxpdo[6] .is_wysiwyg = "true";
defparam \dxxpdo[6] .power_up = "low";

dffeas \dxxpdo[7] (
	.clk(clk),
	.d(\dxxpdo~2_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_7),
	.prn(vcc));
defparam \dxxpdo[7] .is_wysiwyg = "true";
defparam \dxxpdo[7] .power_up = "low";

dffeas \dxxpdo[8] (
	.clk(clk),
	.d(\dxxpdo~3_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_8),
	.prn(vcc));
defparam \dxxpdo[8] .is_wysiwyg = "true";
defparam \dxxpdo[8] .power_up = "low";

dffeas \dxxpdo[9] (
	.clk(clk),
	.d(\dxxpdo~4_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_9),
	.prn(vcc));
defparam \dxxpdo[9] .is_wysiwyg = "true";
defparam \dxxpdo[9] .power_up = "low";

dffeas \dxxpdo[10] (
	.clk(clk),
	.d(\dxxpdo~5_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_10),
	.prn(vcc));
defparam \dxxpdo[10] .is_wysiwyg = "true";
defparam \dxxpdo[10] .power_up = "low";

dffeas \dxxpdo[11] (
	.clk(clk),
	.d(\dxxpdo~6_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_11),
	.prn(vcc));
defparam \dxxpdo[11] .is_wysiwyg = "true";
defparam \dxxpdo[11] .power_up = "low";

dffeas \dxxpdo[12] (
	.clk(clk),
	.d(\dxxpdo~7_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_12),
	.prn(vcc));
defparam \dxxpdo[12] .is_wysiwyg = "true";
defparam \dxxpdo[12] .power_up = "low";

dffeas \dxxpdo[13] (
	.clk(clk),
	.d(\dxxpdo~8_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_13),
	.prn(vcc));
defparam \dxxpdo[13] .is_wysiwyg = "true";
defparam \dxxpdo[13] .power_up = "low";

dffeas \dxxpdo[14] (
	.clk(clk),
	.d(\dxxpdo~9_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_14),
	.prn(vcc));
defparam \dxxpdo[14] .is_wysiwyg = "true";
defparam \dxxpdo[14] .power_up = "low";

dffeas \dxxpdo[15] (
	.clk(clk),
	.d(\dxxpdo~10_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_15),
	.prn(vcc));
defparam \dxxpdo[15] .is_wysiwyg = "true";
defparam \dxxpdo[15] .power_up = "low";

dffeas \dxxpdo[16] (
	.clk(clk),
	.d(\dxxpdo~11_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_16),
	.prn(vcc));
defparam \dxxpdo[16] .is_wysiwyg = "true";
defparam \dxxpdo[16] .power_up = "low";

dffeas \dxxpdo[17] (
	.clk(clk),
	.d(\dxxpdo~12_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_17),
	.prn(vcc));
defparam \dxxpdo[17] .is_wysiwyg = "true";
defparam \dxxpdo[17] .power_up = "low";

dffeas \dxxpdo[19] (
	.clk(clk),
	.d(\dxxpdo~13_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_19),
	.prn(vcc));
defparam \dxxpdo[19] .is_wysiwyg = "true";
defparam \dxxpdo[19] .power_up = "low";

dffeas \dxxpdo[18] (
	.clk(clk),
	.d(\dxxpdo~14_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_18),
	.prn(vcc));
defparam \dxxpdo[18] .is_wysiwyg = "true";
defparam \dxxpdo[18] .power_up = "low";

dffeas \dxxpdo[20] (
	.clk(clk),
	.d(\dxxpdo~15_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxpdo_20),
	.prn(vcc));
defparam \dxxpdo[20] .is_wysiwyg = "true";
defparam \dxxpdo[20] .power_up = "low";

cycloneive_lcell_comb \phi_dither_out_w[5]~17 (
	.dataa(dxxrv_0),
	.datab(pipeline_dffe_11),
	.datac(gnd),
	.datad(vcc),
	.cin(gnd),
	.combout(),
	.cout(\phi_dither_out_w[5]~17_cout ));
defparam \phi_dither_out_w[5]~17 .lut_mask = 16'h00EE;
defparam \phi_dither_out_w[5]~17 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[5]~19 (
	.dataa(dxxrv_1),
	.datab(pipeline_dffe_12),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[5]~17_cout ),
	.combout(),
	.cout(\phi_dither_out_w[5]~19_cout ));
defparam \phi_dither_out_w[5]~19 .lut_mask = 16'h007F;
defparam \phi_dither_out_w[5]~19 .sum_lutc_input = "cin";

cycloneive_lcell_comb \phi_dither_out_w[5]~21 (
	.dataa(dxxrv_2),
	.datab(pipeline_dffe_13),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[5]~19_cout ),
	.combout(),
	.cout(\phi_dither_out_w[5]~21_cout ));
defparam \phi_dither_out_w[5]~21 .lut_mask = 16'h00EF;
defparam \phi_dither_out_w[5]~21 .sum_lutc_input = "cin";

cycloneive_lcell_comb \phi_dither_out_w[5]~23 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_14),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[5]~21_cout ),
	.combout(),
	.cout(\phi_dither_out_w[5]~23_cout ));
defparam \phi_dither_out_w[5]~23 .lut_mask = 16'h007F;
defparam \phi_dither_out_w[5]~23 .sum_lutc_input = "cin";

cycloneive_lcell_comb \phi_dither_out_w[5]~25 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_15),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[5]~23_cout ),
	.combout(),
	.cout(\phi_dither_out_w[5]~25_cout ));
defparam \phi_dither_out_w[5]~25 .lut_mask = 16'h00EF;
defparam \phi_dither_out_w[5]~25 .sum_lutc_input = "cin";

cycloneive_lcell_comb \phi_dither_out_w[5]~26 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_16),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[5]~25_cout ),
	.combout(\phi_dither_out_w[5]~26_combout ),
	.cout(\phi_dither_out_w[5]~27 ));
defparam \phi_dither_out_w[5]~26 .lut_mask = 16'h967F;
defparam \phi_dither_out_w[5]~26 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[5] (
	.clk(clk),
	.d(\phi_dither_out_w[5]~26_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[5]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[5] .is_wysiwyg = "true";
defparam \phi_dither_out_w[5] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~0 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[5]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~0_combout ),
	.cout());
defparam \dxxpdo~0 .lut_mask = 16'hEEEE;
defparam \dxxpdo~0 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[6]~28 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_17),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[5]~27 ),
	.combout(\phi_dither_out_w[6]~28_combout ),
	.cout(\phi_dither_out_w[6]~29 ));
defparam \phi_dither_out_w[6]~28 .lut_mask = 16'h96EF;
defparam \phi_dither_out_w[6]~28 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[6] (
	.clk(clk),
	.d(\phi_dither_out_w[6]~28_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[6]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[6] .is_wysiwyg = "true";
defparam \phi_dither_out_w[6] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~1 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[6]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~1_combout ),
	.cout());
defparam \dxxpdo~1 .lut_mask = 16'hEEEE;
defparam \dxxpdo~1 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[7]~30 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_18),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[6]~29 ),
	.combout(\phi_dither_out_w[7]~30_combout ),
	.cout(\phi_dither_out_w[7]~31 ));
defparam \phi_dither_out_w[7]~30 .lut_mask = 16'h967F;
defparam \phi_dither_out_w[7]~30 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[7] (
	.clk(clk),
	.d(\phi_dither_out_w[7]~30_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[7]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[7] .is_wysiwyg = "true";
defparam \phi_dither_out_w[7] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~2 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[7]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~2_combout ),
	.cout());
defparam \dxxpdo~2 .lut_mask = 16'hEEEE;
defparam \dxxpdo~2 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[8]~32 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_19),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[7]~31 ),
	.combout(\phi_dither_out_w[8]~32_combout ),
	.cout(\phi_dither_out_w[8]~33 ));
defparam \phi_dither_out_w[8]~32 .lut_mask = 16'h96EF;
defparam \phi_dither_out_w[8]~32 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[8] (
	.clk(clk),
	.d(\phi_dither_out_w[8]~32_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[8]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[8] .is_wysiwyg = "true";
defparam \phi_dither_out_w[8] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~3 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[8]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~3_combout ),
	.cout());
defparam \dxxpdo~3 .lut_mask = 16'hEEEE;
defparam \dxxpdo~3 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[9]~34 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_20),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[8]~33 ),
	.combout(\phi_dither_out_w[9]~34_combout ),
	.cout(\phi_dither_out_w[9]~35 ));
defparam \phi_dither_out_w[9]~34 .lut_mask = 16'h967F;
defparam \phi_dither_out_w[9]~34 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[9] (
	.clk(clk),
	.d(\phi_dither_out_w[9]~34_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[9]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[9] .is_wysiwyg = "true";
defparam \phi_dither_out_w[9] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~4 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[9]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~4_combout ),
	.cout());
defparam \dxxpdo~4 .lut_mask = 16'hEEEE;
defparam \dxxpdo~4 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[10]~36 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_21),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[9]~35 ),
	.combout(\phi_dither_out_w[10]~36_combout ),
	.cout(\phi_dither_out_w[10]~37 ));
defparam \phi_dither_out_w[10]~36 .lut_mask = 16'h96EF;
defparam \phi_dither_out_w[10]~36 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[10] (
	.clk(clk),
	.d(\phi_dither_out_w[10]~36_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[10]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[10] .is_wysiwyg = "true";
defparam \phi_dither_out_w[10] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~5 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[10]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~5_combout ),
	.cout());
defparam \dxxpdo~5 .lut_mask = 16'hEEEE;
defparam \dxxpdo~5 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[11]~38 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_22),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[10]~37 ),
	.combout(\phi_dither_out_w[11]~38_combout ),
	.cout(\phi_dither_out_w[11]~39 ));
defparam \phi_dither_out_w[11]~38 .lut_mask = 16'h967F;
defparam \phi_dither_out_w[11]~38 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[11] (
	.clk(clk),
	.d(\phi_dither_out_w[11]~38_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[11]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[11] .is_wysiwyg = "true";
defparam \phi_dither_out_w[11] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~6 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[11]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~6_combout ),
	.cout());
defparam \dxxpdo~6 .lut_mask = 16'hEEEE;
defparam \dxxpdo~6 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[12]~40 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_23),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[11]~39 ),
	.combout(\phi_dither_out_w[12]~40_combout ),
	.cout(\phi_dither_out_w[12]~41 ));
defparam \phi_dither_out_w[12]~40 .lut_mask = 16'h96EF;
defparam \phi_dither_out_w[12]~40 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[12] (
	.clk(clk),
	.d(\phi_dither_out_w[12]~40_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[12]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[12] .is_wysiwyg = "true";
defparam \phi_dither_out_w[12] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~7 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[12]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~7_combout ),
	.cout());
defparam \dxxpdo~7 .lut_mask = 16'hEEEE;
defparam \dxxpdo~7 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[13]~42 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_24),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[12]~41 ),
	.combout(\phi_dither_out_w[13]~42_combout ),
	.cout(\phi_dither_out_w[13]~43 ));
defparam \phi_dither_out_w[13]~42 .lut_mask = 16'h967F;
defparam \phi_dither_out_w[13]~42 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[13] (
	.clk(clk),
	.d(\phi_dither_out_w[13]~42_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[13]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[13] .is_wysiwyg = "true";
defparam \phi_dither_out_w[13] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~8 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[13]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~8_combout ),
	.cout());
defparam \dxxpdo~8 .lut_mask = 16'hEEEE;
defparam \dxxpdo~8 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[14]~44 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_25),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[13]~43 ),
	.combout(\phi_dither_out_w[14]~44_combout ),
	.cout(\phi_dither_out_w[14]~45 ));
defparam \phi_dither_out_w[14]~44 .lut_mask = 16'h96EF;
defparam \phi_dither_out_w[14]~44 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[14] (
	.clk(clk),
	.d(\phi_dither_out_w[14]~44_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[14]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[14] .is_wysiwyg = "true";
defparam \phi_dither_out_w[14] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~9 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[14]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~9_combout ),
	.cout());
defparam \dxxpdo~9 .lut_mask = 16'hEEEE;
defparam \dxxpdo~9 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[15]~46 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_26),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[14]~45 ),
	.combout(\phi_dither_out_w[15]~46_combout ),
	.cout(\phi_dither_out_w[15]~47 ));
defparam \phi_dither_out_w[15]~46 .lut_mask = 16'h967F;
defparam \phi_dither_out_w[15]~46 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[15] (
	.clk(clk),
	.d(\phi_dither_out_w[15]~46_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[15]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[15] .is_wysiwyg = "true";
defparam \phi_dither_out_w[15] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~10 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[15]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~10_combout ),
	.cout());
defparam \dxxpdo~10 .lut_mask = 16'hEEEE;
defparam \dxxpdo~10 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[16]~48 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_27),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[15]~47 ),
	.combout(\phi_dither_out_w[16]~48_combout ),
	.cout(\phi_dither_out_w[16]~49 ));
defparam \phi_dither_out_w[16]~48 .lut_mask = 16'h96EF;
defparam \phi_dither_out_w[16]~48 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[16] (
	.clk(clk),
	.d(\phi_dither_out_w[16]~48_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[16]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[16] .is_wysiwyg = "true";
defparam \phi_dither_out_w[16] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~11 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[16]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~11_combout ),
	.cout());
defparam \dxxpdo~11 .lut_mask = 16'hEEEE;
defparam \dxxpdo~11 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[17]~50 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_28),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[16]~49 ),
	.combout(\phi_dither_out_w[17]~50_combout ),
	.cout(\phi_dither_out_w[17]~51 ));
defparam \phi_dither_out_w[17]~50 .lut_mask = 16'h967F;
defparam \phi_dither_out_w[17]~50 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[17] (
	.clk(clk),
	.d(\phi_dither_out_w[17]~50_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[17]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[17] .is_wysiwyg = "true";
defparam \phi_dither_out_w[17] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~12 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[17]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~12_combout ),
	.cout());
defparam \dxxpdo~12 .lut_mask = 16'hEEEE;
defparam \dxxpdo~12 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[18]~52 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_29),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[17]~51 ),
	.combout(\phi_dither_out_w[18]~52_combout ),
	.cout(\phi_dither_out_w[18]~53 ));
defparam \phi_dither_out_w[18]~52 .lut_mask = 16'h96EF;
defparam \phi_dither_out_w[18]~52 .sum_lutc_input = "cin";

cycloneive_lcell_comb \phi_dither_out_w[19]~54 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_30),
	.datac(gnd),
	.datad(vcc),
	.cin(\phi_dither_out_w[18]~53 ),
	.combout(\phi_dither_out_w[19]~54_combout ),
	.cout(\phi_dither_out_w[19]~55 ));
defparam \phi_dither_out_w[19]~54 .lut_mask = 16'h967F;
defparam \phi_dither_out_w[19]~54 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[19] (
	.clk(clk),
	.d(\phi_dither_out_w[19]~54_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[19]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[19] .is_wysiwyg = "true";
defparam \phi_dither_out_w[19] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~13 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[19]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~13_combout ),
	.cout());
defparam \dxxpdo~13 .lut_mask = 16'hEEEE;
defparam \dxxpdo~13 .sum_lutc_input = "datac";

dffeas \phi_dither_out_w[18] (
	.clk(clk),
	.d(\phi_dither_out_w[18]~52_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[18]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[18] .is_wysiwyg = "true";
defparam \phi_dither_out_w[18] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~14 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[18]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~14_combout ),
	.cout());
defparam \dxxpdo~14 .lut_mask = 16'hEEEE;
defparam \dxxpdo~14 .sum_lutc_input = "datac";

cycloneive_lcell_comb \phi_dither_out_w[20]~56 (
	.dataa(dxxrv_3),
	.datab(pipeline_dffe_31),
	.datac(gnd),
	.datad(gnd),
	.cin(\phi_dither_out_w[19]~55 ),
	.combout(\phi_dither_out_w[20]~56_combout ),
	.cout());
defparam \phi_dither_out_w[20]~56 .lut_mask = 16'h9696;
defparam \phi_dither_out_w[20]~56 .sum_lutc_input = "cin";

dffeas \phi_dither_out_w[20] (
	.clk(clk),
	.d(\phi_dither_out_w[20]~56_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(\phi_dither_out_w[20]~q ),
	.prn(vcc));
defparam \phi_dither_out_w[20] .is_wysiwyg = "true";
defparam \phi_dither_out_w[20] .power_up = "low";

cycloneive_lcell_comb \dxxpdo~15 (
	.dataa(reset_n),
	.datab(\phi_dither_out_w[20]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\dxxpdo~15_combout ),
	.cout());
defparam \dxxpdo~15 .lut_mask = 16'hEEEE;
defparam \dxxpdo~15 .sum_lutc_input = "datac";

endmodule

module NCO_Party_asj_dxx_g (
	dxxrv_3,
	dxxrv_2,
	dxxrv_1,
	dxxrv_0,
	data_out_8,
	clk,
	reset_n)/* synthesis synthesis_greybox=1 */;
output 	dxxrv_3;
output 	dxxrv_2;
output 	dxxrv_1;
output 	dxxrv_0;
input 	data_out_8;
input 	clk;
input 	reset_n;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \lsfr_reg~16_combout ;
wire \lsfr_reg[0]~q ;
wire \lsfr_reg~15_combout ;
wire \lsfr_reg[1]~q ;
wire \lsfr_reg~14_combout ;
wire \lsfr_reg[2]~q ;
wire \lsfr_reg~13_combout ;
wire \lsfr_reg[3]~q ;
wire \lsfr_reg~12_combout ;
wire \lsfr_reg[4]~q ;
wire \lsfr_reg~11_combout ;
wire \lsfr_reg[5]~q ;
wire \lsfr_reg~10_combout ;
wire \lsfr_reg[6]~q ;
wire \lsfr_reg~9_combout ;
wire \lsfr_reg[7]~q ;
wire \lsfr_reg~8_combout ;
wire \lsfr_reg[8]~q ;
wire \lsfr_reg~7_combout ;
wire \lsfr_reg[9]~q ;
wire \lsfr_reg~6_combout ;
wire \lsfr_reg[10]~q ;
wire \lsfr_reg~5_combout ;
wire \lsfr_reg[11]~q ;
wire \lsfr_reg~4_combout ;
wire \lsfr_reg[12]~q ;
wire \lsfr_reg~3_combout ;
wire \lsfr_reg[13]~q ;
wire \lsfr_reg~2_combout ;
wire \lsfr_reg[14]~q ;
wire \lsfr_reg~1_combout ;
wire \lsfr_reg[15]~q ;
wire \Add0~0_combout ;
wire \Add0~1_combout ;
wire \Add0~2_combout ;
wire \lsfr_reg~0_combout ;


dffeas \dxxrv[3] (
	.clk(clk),
	.d(\Add0~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxrv_3),
	.prn(vcc));
defparam \dxxrv[3] .is_wysiwyg = "true";
defparam \dxxrv[3] .power_up = "low";

dffeas \dxxrv[2] (
	.clk(clk),
	.d(\Add0~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxrv_2),
	.prn(vcc));
defparam \dxxrv[2] .is_wysiwyg = "true";
defparam \dxxrv[2] .power_up = "low";

dffeas \dxxrv[1] (
	.clk(clk),
	.d(\Add0~2_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxrv_1),
	.prn(vcc));
defparam \dxxrv[1] .is_wysiwyg = "true";
defparam \dxxrv[1] .power_up = "low";

dffeas \dxxrv[0] (
	.clk(clk),
	.d(\lsfr_reg~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_8),
	.q(dxxrv_0),
	.prn(vcc));
defparam \dxxrv[0] .is_wysiwyg = "true";
defparam \dxxrv[0] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~16 (
	.dataa(\lsfr_reg[15]~q ),
	.datab(\lsfr_reg[14]~q ),
	.datac(\lsfr_reg[12]~q ),
	.datad(\lsfr_reg[3]~q ),
	.cin(gnd),
	.combout(\lsfr_reg~16_combout ),
	.cout());
defparam \lsfr_reg~16 .lut_mask = 16'h6996;
defparam \lsfr_reg~16 .sum_lutc_input = "datac";

dffeas \lsfr_reg[0] (
	.clk(clk),
	.d(\lsfr_reg~16_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(!reset_n),
	.ena(data_out_8),
	.q(\lsfr_reg[0]~q ),
	.prn(vcc));
defparam \lsfr_reg[0] .is_wysiwyg = "true";
defparam \lsfr_reg[0] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~15 (
	.dataa(reset_n),
	.datab(\lsfr_reg[0]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\lsfr_reg~15_combout ),
	.cout());
defparam \lsfr_reg~15 .lut_mask = 16'hEEEE;
defparam \lsfr_reg~15 .sum_lutc_input = "datac";

dffeas \lsfr_reg[1] (
	.clk(clk),
	.d(\lsfr_reg~15_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[1]~q ),
	.prn(vcc));
defparam \lsfr_reg[1] .is_wysiwyg = "true";
defparam \lsfr_reg[1] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~14 (
	.dataa(\lsfr_reg[1]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(\lsfr_reg~14_combout ),
	.cout());
defparam \lsfr_reg~14 .lut_mask = 16'hAAFF;
defparam \lsfr_reg~14 .sum_lutc_input = "datac";

dffeas \lsfr_reg[2] (
	.clk(clk),
	.d(\lsfr_reg~14_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[2]~q ),
	.prn(vcc));
defparam \lsfr_reg[2] .is_wysiwyg = "true";
defparam \lsfr_reg[2] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~13 (
	.dataa(\lsfr_reg[2]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(\lsfr_reg~13_combout ),
	.cout());
defparam \lsfr_reg~13 .lut_mask = 16'hAAFF;
defparam \lsfr_reg~13 .sum_lutc_input = "datac";

dffeas \lsfr_reg[3] (
	.clk(clk),
	.d(\lsfr_reg~13_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[3]~q ),
	.prn(vcc));
defparam \lsfr_reg[3] .is_wysiwyg = "true";
defparam \lsfr_reg[3] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~12 (
	.dataa(\lsfr_reg[3]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(\lsfr_reg~12_combout ),
	.cout());
defparam \lsfr_reg~12 .lut_mask = 16'hAAFF;
defparam \lsfr_reg~12 .sum_lutc_input = "datac";

dffeas \lsfr_reg[4] (
	.clk(clk),
	.d(\lsfr_reg~12_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[4]~q ),
	.prn(vcc));
defparam \lsfr_reg[4] .is_wysiwyg = "true";
defparam \lsfr_reg[4] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~11 (
	.dataa(reset_n),
	.datab(\lsfr_reg[4]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\lsfr_reg~11_combout ),
	.cout());
defparam \lsfr_reg~11 .lut_mask = 16'hEEEE;
defparam \lsfr_reg~11 .sum_lutc_input = "datac";

dffeas \lsfr_reg[5] (
	.clk(clk),
	.d(\lsfr_reg~11_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[5]~q ),
	.prn(vcc));
defparam \lsfr_reg[5] .is_wysiwyg = "true";
defparam \lsfr_reg[5] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~10 (
	.dataa(\lsfr_reg[5]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(\lsfr_reg~10_combout ),
	.cout());
defparam \lsfr_reg~10 .lut_mask = 16'hAAFF;
defparam \lsfr_reg~10 .sum_lutc_input = "datac";

dffeas \lsfr_reg[6] (
	.clk(clk),
	.d(\lsfr_reg~10_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[6]~q ),
	.prn(vcc));
defparam \lsfr_reg[6] .is_wysiwyg = "true";
defparam \lsfr_reg[6] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~9 (
	.dataa(\lsfr_reg[6]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(\lsfr_reg~9_combout ),
	.cout());
defparam \lsfr_reg~9 .lut_mask = 16'hAAFF;
defparam \lsfr_reg~9 .sum_lutc_input = "datac";

dffeas \lsfr_reg[7] (
	.clk(clk),
	.d(\lsfr_reg~9_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[7]~q ),
	.prn(vcc));
defparam \lsfr_reg[7] .is_wysiwyg = "true";
defparam \lsfr_reg[7] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~8 (
	.dataa(reset_n),
	.datab(\lsfr_reg[7]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\lsfr_reg~8_combout ),
	.cout());
defparam \lsfr_reg~8 .lut_mask = 16'hEEEE;
defparam \lsfr_reg~8 .sum_lutc_input = "datac";

dffeas \lsfr_reg[8] (
	.clk(clk),
	.d(\lsfr_reg~8_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[8]~q ),
	.prn(vcc));
defparam \lsfr_reg[8] .is_wysiwyg = "true";
defparam \lsfr_reg[8] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~7 (
	.dataa(\lsfr_reg[8]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(\lsfr_reg~7_combout ),
	.cout());
defparam \lsfr_reg~7 .lut_mask = 16'hAAFF;
defparam \lsfr_reg~7 .sum_lutc_input = "datac";

dffeas \lsfr_reg[9] (
	.clk(clk),
	.d(\lsfr_reg~7_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[9]~q ),
	.prn(vcc));
defparam \lsfr_reg[9] .is_wysiwyg = "true";
defparam \lsfr_reg[9] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~6 (
	.dataa(reset_n),
	.datab(\lsfr_reg[9]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\lsfr_reg~6_combout ),
	.cout());
defparam \lsfr_reg~6 .lut_mask = 16'hEEEE;
defparam \lsfr_reg~6 .sum_lutc_input = "datac";

dffeas \lsfr_reg[10] (
	.clk(clk),
	.d(\lsfr_reg~6_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[10]~q ),
	.prn(vcc));
defparam \lsfr_reg[10] .is_wysiwyg = "true";
defparam \lsfr_reg[10] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~5 (
	.dataa(\lsfr_reg[10]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(\lsfr_reg~5_combout ),
	.cout());
defparam \lsfr_reg~5 .lut_mask = 16'hAAFF;
defparam \lsfr_reg~5 .sum_lutc_input = "datac";

dffeas \lsfr_reg[11] (
	.clk(clk),
	.d(\lsfr_reg~5_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[11]~q ),
	.prn(vcc));
defparam \lsfr_reg[11] .is_wysiwyg = "true";
defparam \lsfr_reg[11] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~4 (
	.dataa(\lsfr_reg[11]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(\lsfr_reg~4_combout ),
	.cout());
defparam \lsfr_reg~4 .lut_mask = 16'hAAFF;
defparam \lsfr_reg~4 .sum_lutc_input = "datac";

dffeas \lsfr_reg[12] (
	.clk(clk),
	.d(\lsfr_reg~4_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[12]~q ),
	.prn(vcc));
defparam \lsfr_reg[12] .is_wysiwyg = "true";
defparam \lsfr_reg[12] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~3 (
	.dataa(reset_n),
	.datab(\lsfr_reg[12]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\lsfr_reg~3_combout ),
	.cout());
defparam \lsfr_reg~3 .lut_mask = 16'hEEEE;
defparam \lsfr_reg~3 .sum_lutc_input = "datac";

dffeas \lsfr_reg[13] (
	.clk(clk),
	.d(\lsfr_reg~3_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[13]~q ),
	.prn(vcc));
defparam \lsfr_reg[13] .is_wysiwyg = "true";
defparam \lsfr_reg[13] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~2 (
	.dataa(reset_n),
	.datab(\lsfr_reg[13]~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\lsfr_reg~2_combout ),
	.cout());
defparam \lsfr_reg~2 .lut_mask = 16'hEEEE;
defparam \lsfr_reg~2 .sum_lutc_input = "datac";

dffeas \lsfr_reg[14] (
	.clk(clk),
	.d(\lsfr_reg~2_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[14]~q ),
	.prn(vcc));
defparam \lsfr_reg[14] .is_wysiwyg = "true";
defparam \lsfr_reg[14] .power_up = "low";

cycloneive_lcell_comb \lsfr_reg~1 (
	.dataa(\lsfr_reg[14]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(\lsfr_reg~1_combout ),
	.cout());
defparam \lsfr_reg~1 .lut_mask = 16'hAAFF;
defparam \lsfr_reg~1 .sum_lutc_input = "datac";

dffeas \lsfr_reg[15] (
	.clk(clk),
	.d(\lsfr_reg~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(\lsfr_reg[15]~q ),
	.prn(vcc));
defparam \lsfr_reg[15] .is_wysiwyg = "true";
defparam \lsfr_reg[15] .power_up = "low";

cycloneive_lcell_comb \Add0~0 (
	.dataa(\lsfr_reg[15]~q ),
	.datab(\lsfr_reg[14]~q ),
	.datac(\lsfr_reg[13]~q ),
	.datad(\lsfr_reg[12]~q ),
	.cin(gnd),
	.combout(\Add0~0_combout ),
	.cout());
defparam \Add0~0 .lut_mask = 16'hBFFF;
defparam \Add0~0 .sum_lutc_input = "datac";

cycloneive_lcell_comb \Add0~1 (
	.dataa(\lsfr_reg[14]~q ),
	.datab(\lsfr_reg[15]~q ),
	.datac(\lsfr_reg[13]~q ),
	.datad(\lsfr_reg[12]~q ),
	.cin(gnd),
	.combout(\Add0~1_combout ),
	.cout());
defparam \Add0~1 .lut_mask = 16'h6996;
defparam \Add0~1 .sum_lutc_input = "datac";

cycloneive_lcell_comb \Add0~2 (
	.dataa(gnd),
	.datab(\lsfr_reg[13]~q ),
	.datac(\lsfr_reg[15]~q ),
	.datad(\lsfr_reg[12]~q ),
	.cin(gnd),
	.combout(\Add0~2_combout ),
	.cout());
defparam \Add0~2 .lut_mask = 16'hC33C;
defparam \Add0~2 .sum_lutc_input = "datac";

cycloneive_lcell_comb \lsfr_reg~0 (
	.dataa(gnd),
	.datab(gnd),
	.datac(\lsfr_reg[15]~q ),
	.datad(\lsfr_reg[12]~q ),
	.cin(gnd),
	.combout(\lsfr_reg~0_combout ),
	.cout());
defparam \lsfr_reg~0 .lut_mask = 16'h0FF0;
defparam \lsfr_reg~0 .sum_lutc_input = "datac";

endmodule

module NCO_Party_asj_gal (
	data_out_8,
	rom_add_0,
	rom_add_1,
	rom_add_2,
	rom_add_3,
	rom_add_4,
	rom_add_5,
	rom_add_6,
	rom_add_7,
	rom_add_8,
	rom_add_9,
	rom_add_10,
	rom_add_11,
	rom_add_12,
	dxxpdo_5,
	dxxpdo_6,
	dxxpdo_7,
	dxxpdo_8,
	dxxpdo_9,
	dxxpdo_10,
	dxxpdo_11,
	dxxpdo_12,
	dxxpdo_13,
	dxxpdo_14,
	dxxpdo_15,
	dxxpdo_16,
	dxxpdo_17,
	rom_add_14,
	rom_add_13,
	rom_add_15,
	dxxpdo_19,
	dxxpdo_18,
	dxxpdo_20,
	clk,
	reset_n)/* synthesis synthesis_greybox=1 */;
input 	data_out_8;
output 	rom_add_0;
output 	rom_add_1;
output 	rom_add_2;
output 	rom_add_3;
output 	rom_add_4;
output 	rom_add_5;
output 	rom_add_6;
output 	rom_add_7;
output 	rom_add_8;
output 	rom_add_9;
output 	rom_add_10;
output 	rom_add_11;
output 	rom_add_12;
input 	dxxpdo_5;
input 	dxxpdo_6;
input 	dxxpdo_7;
input 	dxxpdo_8;
input 	dxxpdo_9;
input 	dxxpdo_10;
input 	dxxpdo_11;
input 	dxxpdo_12;
input 	dxxpdo_13;
input 	dxxpdo_14;
input 	dxxpdo_15;
input 	dxxpdo_16;
input 	dxxpdo_17;
output 	rom_add_14;
output 	rom_add_13;
output 	rom_add_15;
input 	dxxpdo_19;
input 	dxxpdo_18;
input 	dxxpdo_20;
input 	clk;
input 	reset_n;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \rom_add~0_combout ;
wire \rom_add~1_combout ;
wire \rom_add~2_combout ;
wire \rom_add~3_combout ;
wire \rom_add~4_combout ;
wire \rom_add~5_combout ;
wire \rom_add~6_combout ;
wire \rom_add~7_combout ;
wire \rom_add~8_combout ;
wire \rom_add~9_combout ;
wire \rom_add~10_combout ;
wire \rom_add~11_combout ;
wire \rom_add~12_combout ;
wire \rom_add~13_combout ;
wire \rom_add~14_combout ;
wire \rom_add~15_combout ;


dffeas \rom_add[0] (
	.clk(clk),
	.d(\rom_add~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_0),
	.prn(vcc));
defparam \rom_add[0] .is_wysiwyg = "true";
defparam \rom_add[0] .power_up = "low";

dffeas \rom_add[1] (
	.clk(clk),
	.d(\rom_add~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_1),
	.prn(vcc));
defparam \rom_add[1] .is_wysiwyg = "true";
defparam \rom_add[1] .power_up = "low";

dffeas \rom_add[2] (
	.clk(clk),
	.d(\rom_add~2_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_2),
	.prn(vcc));
defparam \rom_add[2] .is_wysiwyg = "true";
defparam \rom_add[2] .power_up = "low";

dffeas \rom_add[3] (
	.clk(clk),
	.d(\rom_add~3_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_3),
	.prn(vcc));
defparam \rom_add[3] .is_wysiwyg = "true";
defparam \rom_add[3] .power_up = "low";

dffeas \rom_add[4] (
	.clk(clk),
	.d(\rom_add~4_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_4),
	.prn(vcc));
defparam \rom_add[4] .is_wysiwyg = "true";
defparam \rom_add[4] .power_up = "low";

dffeas \rom_add[5] (
	.clk(clk),
	.d(\rom_add~5_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_5),
	.prn(vcc));
defparam \rom_add[5] .is_wysiwyg = "true";
defparam \rom_add[5] .power_up = "low";

dffeas \rom_add[6] (
	.clk(clk),
	.d(\rom_add~6_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_6),
	.prn(vcc));
defparam \rom_add[6] .is_wysiwyg = "true";
defparam \rom_add[6] .power_up = "low";

dffeas \rom_add[7] (
	.clk(clk),
	.d(\rom_add~7_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_7),
	.prn(vcc));
defparam \rom_add[7] .is_wysiwyg = "true";
defparam \rom_add[7] .power_up = "low";

dffeas \rom_add[8] (
	.clk(clk),
	.d(\rom_add~8_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_8),
	.prn(vcc));
defparam \rom_add[8] .is_wysiwyg = "true";
defparam \rom_add[8] .power_up = "low";

dffeas \rom_add[9] (
	.clk(clk),
	.d(\rom_add~9_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_9),
	.prn(vcc));
defparam \rom_add[9] .is_wysiwyg = "true";
defparam \rom_add[9] .power_up = "low";

dffeas \rom_add[10] (
	.clk(clk),
	.d(\rom_add~10_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_10),
	.prn(vcc));
defparam \rom_add[10] .is_wysiwyg = "true";
defparam \rom_add[10] .power_up = "low";

dffeas \rom_add[11] (
	.clk(clk),
	.d(\rom_add~11_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_11),
	.prn(vcc));
defparam \rom_add[11] .is_wysiwyg = "true";
defparam \rom_add[11] .power_up = "low";

dffeas \rom_add[12] (
	.clk(clk),
	.d(\rom_add~12_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_12),
	.prn(vcc));
defparam \rom_add[12] .is_wysiwyg = "true";
defparam \rom_add[12] .power_up = "low";

dffeas \rom_add[14] (
	.clk(clk),
	.d(\rom_add~13_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_14),
	.prn(vcc));
defparam \rom_add[14] .is_wysiwyg = "true";
defparam \rom_add[14] .power_up = "low";

dffeas \rom_add[13] (
	.clk(clk),
	.d(\rom_add~14_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_13),
	.prn(vcc));
defparam \rom_add[13] .is_wysiwyg = "true";
defparam \rom_add[13] .power_up = "low";

dffeas \rom_add[15] (
	.clk(clk),
	.d(\rom_add~15_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(data_out_8),
	.q(rom_add_15),
	.prn(vcc));
defparam \rom_add[15] .is_wysiwyg = "true";
defparam \rom_add[15] .power_up = "low";

cycloneive_lcell_comb \rom_add~0 (
	.dataa(reset_n),
	.datab(dxxpdo_5),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~0_combout ),
	.cout());
defparam \rom_add~0 .lut_mask = 16'hEEEE;
defparam \rom_add~0 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~1 (
	.dataa(reset_n),
	.datab(dxxpdo_6),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~1_combout ),
	.cout());
defparam \rom_add~1 .lut_mask = 16'hEEEE;
defparam \rom_add~1 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~2 (
	.dataa(reset_n),
	.datab(dxxpdo_7),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~2_combout ),
	.cout());
defparam \rom_add~2 .lut_mask = 16'hEEEE;
defparam \rom_add~2 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~3 (
	.dataa(reset_n),
	.datab(dxxpdo_8),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~3_combout ),
	.cout());
defparam \rom_add~3 .lut_mask = 16'hEEEE;
defparam \rom_add~3 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~4 (
	.dataa(reset_n),
	.datab(dxxpdo_9),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~4_combout ),
	.cout());
defparam \rom_add~4 .lut_mask = 16'hEEEE;
defparam \rom_add~4 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~5 (
	.dataa(reset_n),
	.datab(dxxpdo_10),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~5_combout ),
	.cout());
defparam \rom_add~5 .lut_mask = 16'hEEEE;
defparam \rom_add~5 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~6 (
	.dataa(reset_n),
	.datab(dxxpdo_11),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~6_combout ),
	.cout());
defparam \rom_add~6 .lut_mask = 16'hEEEE;
defparam \rom_add~6 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~7 (
	.dataa(reset_n),
	.datab(dxxpdo_12),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~7_combout ),
	.cout());
defparam \rom_add~7 .lut_mask = 16'hEEEE;
defparam \rom_add~7 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~8 (
	.dataa(reset_n),
	.datab(dxxpdo_13),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~8_combout ),
	.cout());
defparam \rom_add~8 .lut_mask = 16'hEEEE;
defparam \rom_add~8 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~9 (
	.dataa(reset_n),
	.datab(dxxpdo_14),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~9_combout ),
	.cout());
defparam \rom_add~9 .lut_mask = 16'hEEEE;
defparam \rom_add~9 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~10 (
	.dataa(reset_n),
	.datab(dxxpdo_15),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~10_combout ),
	.cout());
defparam \rom_add~10 .lut_mask = 16'hEEEE;
defparam \rom_add~10 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~11 (
	.dataa(reset_n),
	.datab(dxxpdo_16),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~11_combout ),
	.cout());
defparam \rom_add~11 .lut_mask = 16'hEEEE;
defparam \rom_add~11 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~12 (
	.dataa(reset_n),
	.datab(dxxpdo_17),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~12_combout ),
	.cout());
defparam \rom_add~12 .lut_mask = 16'hEEEE;
defparam \rom_add~12 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~13 (
	.dataa(reset_n),
	.datab(dxxpdo_19),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~13_combout ),
	.cout());
defparam \rom_add~13 .lut_mask = 16'hEEEE;
defparam \rom_add~13 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~14 (
	.dataa(reset_n),
	.datab(dxxpdo_18),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~14_combout ),
	.cout());
defparam \rom_add~14 .lut_mask = 16'hEEEE;
defparam \rom_add~14 .sum_lutc_input = "datac";

cycloneive_lcell_comb \rom_add~15 (
	.dataa(reset_n),
	.datab(dxxpdo_20),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\rom_add~15_combout ),
	.cout());
defparam \rom_add~15 .lut_mask = 16'hEEEE;
defparam \rom_add~15 .sum_lutc_input = "datac";

endmodule

module NCO_Party_asj_nco_as_m_cen (
	_,
	_1,
	_2,
	_3,
	_4,
	_5,
	_6,
	_7,
	_8,
	_9,
	_10,
	_11,
	_12,
	_13,
	_14,
	_15,
	_16,
	_17,
	rom_add_0,
	rom_add_1,
	rom_add_2,
	rom_add_3,
	rom_add_4,
	rom_add_5,
	rom_add_6,
	rom_add_7,
	rom_add_8,
	rom_add_9,
	rom_add_10,
	rom_add_11,
	rom_add_12,
	rom_add_14,
	rom_add_13,
	rom_add_15,
	clk,
	clken)/* synthesis synthesis_greybox=1 */;
output 	_;
output 	_1;
output 	_2;
output 	_3;
output 	_4;
output 	_5;
output 	_6;
output 	_7;
output 	_8;
output 	_9;
output 	_10;
output 	_11;
output 	_12;
output 	_13;
output 	_14;
output 	_15;
output 	_16;
output 	_17;
input 	rom_add_0;
input 	rom_add_1;
input 	rom_add_2;
input 	rom_add_3;
input 	rom_add_4;
input 	rom_add_5;
input 	rom_add_6;
input 	rom_add_7;
input 	rom_add_8;
input 	rom_add_9;
input 	rom_add_10;
input 	rom_add_11;
input 	rom_add_12;
input 	rom_add_14;
input 	rom_add_13;
input 	rom_add_15;
input 	clk;
input 	clken;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;



NCO_Party_altsyncram_1 altsyncram_component0(
	._(_),
	._1(_1),
	._2(_2),
	._3(_3),
	._4(_4),
	._5(_5),
	._6(_6),
	._7(_7),
	._8(_8),
	._9(_9),
	._10(_10),
	._11(_11),
	._12(_12),
	._13(_13),
	._14(_14),
	._15(_15),
	._16(_16),
	._17(_17),
	.address_a({rom_add_15,rom_add_14,rom_add_13,rom_add_12,rom_add_11,rom_add_10,rom_add_9,rom_add_8,rom_add_7,rom_add_6,rom_add_5,rom_add_4,rom_add_3,rom_add_2,rom_add_1,rom_add_0}),
	.clock0(clk),
	.clocken0(clken));

endmodule

module NCO_Party_altsyncram_1 (
	_,
	_1,
	_2,
	_3,
	_4,
	_5,
	_6,
	_7,
	_8,
	_9,
	_10,
	_11,
	_12,
	_13,
	_14,
	_15,
	_16,
	_17,
	address_a,
	clock0,
	clocken0)/* synthesis synthesis_greybox=1 */;
output 	_;
output 	_1;
output 	_2;
output 	_3;
output 	_4;
output 	_5;
output 	_6;
output 	_7;
output 	_8;
output 	_9;
output 	_10;
output 	_11;
output 	_12;
output 	_13;
output 	_14;
output 	_15;
output 	_16;
output 	_17;
input 	[15:0] address_a;
input 	clock0;
input 	clocken0;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;



NCO_Party_altsyncram_vea1 auto_generated(
	._(_),
	._1(_1),
	._2(_2),
	._3(_3),
	._4(_4),
	._5(_5),
	._6(_6),
	._7(_7),
	._8(_8),
	._9(_9),
	._10(_10),
	._11(_11),
	._12(_12),
	._13(_13),
	._14(_14),
	._15(_15),
	._16(_16),
	._17(_17),
	.address_a({address_a[15],address_a[14],address_a[13],address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.clock0(clock0),
	.clocken0(clocken0));

endmodule

module NCO_Party_altsyncram_vea1 (
	_,
	_1,
	_2,
	_3,
	_4,
	_5,
	_6,
	_7,
	_8,
	_9,
	_10,
	_11,
	_12,
	_13,
	_14,
	_15,
	_16,
	_17,
	address_a,
	clock0,
	clocken0)/* synthesis synthesis_greybox=1 */;
output 	_;
output 	_1;
output 	_2;
output 	_3;
output 	_4;
output 	_5;
output 	_6;
output 	_7;
output 	_8;
output 	_9;
output 	_10;
output 	_11;
output 	_12;
output 	_13;
output 	_14;
output 	_15;
output 	_16;
output 	_17;
input 	[15:0] address_a;
input 	clock0;
input 	clocken0;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \ram_block1a108~portadataout ;
wire \ram_block1a90~portadataout ;
wire \ram_block1a72~portadataout ;
wire \ram_block1a126~portadataout ;
wire \ram_block1a18~portadataout ;
wire \ram_block1a36~portadataout ;
wire \ram_block1a0~portadataout ;
wire \ram_block1a54~portadataout ;
wire \ram_block1a109~portadataout ;
wire \ram_block1a91~portadataout ;
wire \ram_block1a73~portadataout ;
wire \ram_block1a127~portadataout ;
wire \ram_block1a19~portadataout ;
wire \ram_block1a37~portadataout ;
wire \ram_block1a1~portadataout ;
wire \ram_block1a55~portadataout ;
wire \ram_block1a110~portadataout ;
wire \ram_block1a92~portadataout ;
wire \ram_block1a74~portadataout ;
wire \ram_block1a128~portadataout ;
wire \ram_block1a20~portadataout ;
wire \ram_block1a38~portadataout ;
wire \ram_block1a2~portadataout ;
wire \ram_block1a56~portadataout ;
wire \ram_block1a111~portadataout ;
wire \ram_block1a93~portadataout ;
wire \ram_block1a75~portadataout ;
wire \ram_block1a129~portadataout ;
wire \ram_block1a21~portadataout ;
wire \ram_block1a39~portadataout ;
wire \ram_block1a3~portadataout ;
wire \ram_block1a57~portadataout ;
wire \ram_block1a112~portadataout ;
wire \ram_block1a94~portadataout ;
wire \ram_block1a76~portadataout ;
wire \ram_block1a130~portadataout ;
wire \ram_block1a22~portadataout ;
wire \ram_block1a40~portadataout ;
wire \ram_block1a4~portadataout ;
wire \ram_block1a58~portadataout ;
wire \ram_block1a113~portadataout ;
wire \ram_block1a95~portadataout ;
wire \ram_block1a77~portadataout ;
wire \ram_block1a131~portadataout ;
wire \ram_block1a23~portadataout ;
wire \ram_block1a41~portadataout ;
wire \ram_block1a5~portadataout ;
wire \ram_block1a59~portadataout ;
wire \ram_block1a114~portadataout ;
wire \ram_block1a96~portadataout ;
wire \ram_block1a78~portadataout ;
wire \ram_block1a132~portadataout ;
wire \ram_block1a24~portadataout ;
wire \ram_block1a42~portadataout ;
wire \ram_block1a6~portadataout ;
wire \ram_block1a60~portadataout ;
wire \ram_block1a115~portadataout ;
wire \ram_block1a97~portadataout ;
wire \ram_block1a79~portadataout ;
wire \ram_block1a133~portadataout ;
wire \ram_block1a25~portadataout ;
wire \ram_block1a43~portadataout ;
wire \ram_block1a7~portadataout ;
wire \ram_block1a61~portadataout ;
wire \ram_block1a116~portadataout ;
wire \ram_block1a98~portadataout ;
wire \ram_block1a80~portadataout ;
wire \ram_block1a134~portadataout ;
wire \ram_block1a26~portadataout ;
wire \ram_block1a44~portadataout ;
wire \ram_block1a8~portadataout ;
wire \ram_block1a62~portadataout ;
wire \ram_block1a117~portadataout ;
wire \ram_block1a99~portadataout ;
wire \ram_block1a81~portadataout ;
wire \ram_block1a135~portadataout ;
wire \ram_block1a27~portadataout ;
wire \ram_block1a45~portadataout ;
wire \ram_block1a9~portadataout ;
wire \ram_block1a63~portadataout ;
wire \ram_block1a118~portadataout ;
wire \ram_block1a100~portadataout ;
wire \ram_block1a82~portadataout ;
wire \ram_block1a136~portadataout ;
wire \ram_block1a28~portadataout ;
wire \ram_block1a46~portadataout ;
wire \ram_block1a10~portadataout ;
wire \ram_block1a64~portadataout ;
wire \ram_block1a119~portadataout ;
wire \ram_block1a101~portadataout ;
wire \ram_block1a83~portadataout ;
wire \ram_block1a137~portadataout ;
wire \ram_block1a29~portadataout ;
wire \ram_block1a47~portadataout ;
wire \ram_block1a11~portadataout ;
wire \ram_block1a65~portadataout ;
wire \ram_block1a120~portadataout ;
wire \ram_block1a102~portadataout ;
wire \ram_block1a84~portadataout ;
wire \ram_block1a138~portadataout ;
wire \ram_block1a30~portadataout ;
wire \ram_block1a48~portadataout ;
wire \ram_block1a12~portadataout ;
wire \ram_block1a66~portadataout ;
wire \ram_block1a121~portadataout ;
wire \ram_block1a103~portadataout ;
wire \ram_block1a85~portadataout ;
wire \ram_block1a139~portadataout ;
wire \ram_block1a31~portadataout ;
wire \ram_block1a49~portadataout ;
wire \ram_block1a13~portadataout ;
wire \ram_block1a67~portadataout ;
wire \ram_block1a122~portadataout ;
wire \ram_block1a104~portadataout ;
wire \ram_block1a86~portadataout ;
wire \ram_block1a140~portadataout ;
wire \ram_block1a32~portadataout ;
wire \ram_block1a50~portadataout ;
wire \ram_block1a14~portadataout ;
wire \ram_block1a68~portadataout ;
wire \ram_block1a123~portadataout ;
wire \ram_block1a105~portadataout ;
wire \ram_block1a87~portadataout ;
wire \ram_block1a141~portadataout ;
wire \ram_block1a33~portadataout ;
wire \ram_block1a51~portadataout ;
wire \ram_block1a15~portadataout ;
wire \ram_block1a69~portadataout ;
wire \ram_block1a124~portadataout ;
wire \ram_block1a106~portadataout ;
wire \ram_block1a88~portadataout ;
wire \ram_block1a142~portadataout ;
wire \ram_block1a34~portadataout ;
wire \ram_block1a52~portadataout ;
wire \ram_block1a16~portadataout ;
wire \ram_block1a70~portadataout ;
wire \ram_block1a125~portadataout ;
wire \ram_block1a107~portadataout ;
wire \ram_block1a89~portadataout ;
wire \ram_block1a143~portadataout ;
wire \ram_block1a35~portadataout ;
wire \ram_block1a53~portadataout ;
wire \ram_block1a17~portadataout ;
wire \ram_block1a71~portadataout ;
wire \out_address_reg_a[1]~q ;
wire \out_address_reg_a[0]~q ;
wire \out_address_reg_a[2]~q ;
wire \address_reg_a[1]~q ;
wire \address_reg_a[0]~q ;
wire \address_reg_a[2]~q ;

wire [143:0] ram_block1a108_PORTADATAOUT_bus;
wire [143:0] ram_block1a90_PORTADATAOUT_bus;
wire [143:0] ram_block1a72_PORTADATAOUT_bus;
wire [143:0] ram_block1a126_PORTADATAOUT_bus;
wire [143:0] ram_block1a18_PORTADATAOUT_bus;
wire [143:0] ram_block1a36_PORTADATAOUT_bus;
wire [143:0] ram_block1a0_PORTADATAOUT_bus;
wire [143:0] ram_block1a54_PORTADATAOUT_bus;
wire [143:0] ram_block1a109_PORTADATAOUT_bus;
wire [143:0] ram_block1a91_PORTADATAOUT_bus;
wire [143:0] ram_block1a73_PORTADATAOUT_bus;
wire [143:0] ram_block1a127_PORTADATAOUT_bus;
wire [143:0] ram_block1a19_PORTADATAOUT_bus;
wire [143:0] ram_block1a37_PORTADATAOUT_bus;
wire [143:0] ram_block1a1_PORTADATAOUT_bus;
wire [143:0] ram_block1a55_PORTADATAOUT_bus;
wire [143:0] ram_block1a110_PORTADATAOUT_bus;
wire [143:0] ram_block1a92_PORTADATAOUT_bus;
wire [143:0] ram_block1a74_PORTADATAOUT_bus;
wire [143:0] ram_block1a128_PORTADATAOUT_bus;
wire [143:0] ram_block1a20_PORTADATAOUT_bus;
wire [143:0] ram_block1a38_PORTADATAOUT_bus;
wire [143:0] ram_block1a2_PORTADATAOUT_bus;
wire [143:0] ram_block1a56_PORTADATAOUT_bus;
wire [143:0] ram_block1a111_PORTADATAOUT_bus;
wire [143:0] ram_block1a93_PORTADATAOUT_bus;
wire [143:0] ram_block1a75_PORTADATAOUT_bus;
wire [143:0] ram_block1a129_PORTADATAOUT_bus;
wire [143:0] ram_block1a21_PORTADATAOUT_bus;
wire [143:0] ram_block1a39_PORTADATAOUT_bus;
wire [143:0] ram_block1a3_PORTADATAOUT_bus;
wire [143:0] ram_block1a57_PORTADATAOUT_bus;
wire [143:0] ram_block1a112_PORTADATAOUT_bus;
wire [143:0] ram_block1a94_PORTADATAOUT_bus;
wire [143:0] ram_block1a76_PORTADATAOUT_bus;
wire [143:0] ram_block1a130_PORTADATAOUT_bus;
wire [143:0] ram_block1a22_PORTADATAOUT_bus;
wire [143:0] ram_block1a40_PORTADATAOUT_bus;
wire [143:0] ram_block1a4_PORTADATAOUT_bus;
wire [143:0] ram_block1a58_PORTADATAOUT_bus;
wire [143:0] ram_block1a113_PORTADATAOUT_bus;
wire [143:0] ram_block1a95_PORTADATAOUT_bus;
wire [143:0] ram_block1a77_PORTADATAOUT_bus;
wire [143:0] ram_block1a131_PORTADATAOUT_bus;
wire [143:0] ram_block1a23_PORTADATAOUT_bus;
wire [143:0] ram_block1a41_PORTADATAOUT_bus;
wire [143:0] ram_block1a5_PORTADATAOUT_bus;
wire [143:0] ram_block1a59_PORTADATAOUT_bus;
wire [143:0] ram_block1a114_PORTADATAOUT_bus;
wire [143:0] ram_block1a96_PORTADATAOUT_bus;
wire [143:0] ram_block1a78_PORTADATAOUT_bus;
wire [143:0] ram_block1a132_PORTADATAOUT_bus;
wire [143:0] ram_block1a24_PORTADATAOUT_bus;
wire [143:0] ram_block1a42_PORTADATAOUT_bus;
wire [143:0] ram_block1a6_PORTADATAOUT_bus;
wire [143:0] ram_block1a60_PORTADATAOUT_bus;
wire [143:0] ram_block1a115_PORTADATAOUT_bus;
wire [143:0] ram_block1a97_PORTADATAOUT_bus;
wire [143:0] ram_block1a79_PORTADATAOUT_bus;
wire [143:0] ram_block1a133_PORTADATAOUT_bus;
wire [143:0] ram_block1a25_PORTADATAOUT_bus;
wire [143:0] ram_block1a43_PORTADATAOUT_bus;
wire [143:0] ram_block1a7_PORTADATAOUT_bus;
wire [143:0] ram_block1a61_PORTADATAOUT_bus;
wire [143:0] ram_block1a116_PORTADATAOUT_bus;
wire [143:0] ram_block1a98_PORTADATAOUT_bus;
wire [143:0] ram_block1a80_PORTADATAOUT_bus;
wire [143:0] ram_block1a134_PORTADATAOUT_bus;
wire [143:0] ram_block1a26_PORTADATAOUT_bus;
wire [143:0] ram_block1a44_PORTADATAOUT_bus;
wire [143:0] ram_block1a8_PORTADATAOUT_bus;
wire [143:0] ram_block1a62_PORTADATAOUT_bus;
wire [143:0] ram_block1a117_PORTADATAOUT_bus;
wire [143:0] ram_block1a99_PORTADATAOUT_bus;
wire [143:0] ram_block1a81_PORTADATAOUT_bus;
wire [143:0] ram_block1a135_PORTADATAOUT_bus;
wire [143:0] ram_block1a27_PORTADATAOUT_bus;
wire [143:0] ram_block1a45_PORTADATAOUT_bus;
wire [143:0] ram_block1a9_PORTADATAOUT_bus;
wire [143:0] ram_block1a63_PORTADATAOUT_bus;
wire [143:0] ram_block1a118_PORTADATAOUT_bus;
wire [143:0] ram_block1a100_PORTADATAOUT_bus;
wire [143:0] ram_block1a82_PORTADATAOUT_bus;
wire [143:0] ram_block1a136_PORTADATAOUT_bus;
wire [143:0] ram_block1a28_PORTADATAOUT_bus;
wire [143:0] ram_block1a46_PORTADATAOUT_bus;
wire [143:0] ram_block1a10_PORTADATAOUT_bus;
wire [143:0] ram_block1a64_PORTADATAOUT_bus;
wire [143:0] ram_block1a119_PORTADATAOUT_bus;
wire [143:0] ram_block1a101_PORTADATAOUT_bus;
wire [143:0] ram_block1a83_PORTADATAOUT_bus;
wire [143:0] ram_block1a137_PORTADATAOUT_bus;
wire [143:0] ram_block1a29_PORTADATAOUT_bus;
wire [143:0] ram_block1a47_PORTADATAOUT_bus;
wire [143:0] ram_block1a11_PORTADATAOUT_bus;
wire [143:0] ram_block1a65_PORTADATAOUT_bus;
wire [143:0] ram_block1a120_PORTADATAOUT_bus;
wire [143:0] ram_block1a102_PORTADATAOUT_bus;
wire [143:0] ram_block1a84_PORTADATAOUT_bus;
wire [143:0] ram_block1a138_PORTADATAOUT_bus;
wire [143:0] ram_block1a30_PORTADATAOUT_bus;
wire [143:0] ram_block1a48_PORTADATAOUT_bus;
wire [143:0] ram_block1a12_PORTADATAOUT_bus;
wire [143:0] ram_block1a66_PORTADATAOUT_bus;
wire [143:0] ram_block1a121_PORTADATAOUT_bus;
wire [143:0] ram_block1a103_PORTADATAOUT_bus;
wire [143:0] ram_block1a85_PORTADATAOUT_bus;
wire [143:0] ram_block1a139_PORTADATAOUT_bus;
wire [143:0] ram_block1a31_PORTADATAOUT_bus;
wire [143:0] ram_block1a49_PORTADATAOUT_bus;
wire [143:0] ram_block1a13_PORTADATAOUT_bus;
wire [143:0] ram_block1a67_PORTADATAOUT_bus;
wire [143:0] ram_block1a122_PORTADATAOUT_bus;
wire [143:0] ram_block1a104_PORTADATAOUT_bus;
wire [143:0] ram_block1a86_PORTADATAOUT_bus;
wire [143:0] ram_block1a140_PORTADATAOUT_bus;
wire [143:0] ram_block1a32_PORTADATAOUT_bus;
wire [143:0] ram_block1a50_PORTADATAOUT_bus;
wire [143:0] ram_block1a14_PORTADATAOUT_bus;
wire [143:0] ram_block1a68_PORTADATAOUT_bus;
wire [143:0] ram_block1a123_PORTADATAOUT_bus;
wire [143:0] ram_block1a105_PORTADATAOUT_bus;
wire [143:0] ram_block1a87_PORTADATAOUT_bus;
wire [143:0] ram_block1a141_PORTADATAOUT_bus;
wire [143:0] ram_block1a33_PORTADATAOUT_bus;
wire [143:0] ram_block1a51_PORTADATAOUT_bus;
wire [143:0] ram_block1a15_PORTADATAOUT_bus;
wire [143:0] ram_block1a69_PORTADATAOUT_bus;
wire [143:0] ram_block1a124_PORTADATAOUT_bus;
wire [143:0] ram_block1a106_PORTADATAOUT_bus;
wire [143:0] ram_block1a88_PORTADATAOUT_bus;
wire [143:0] ram_block1a142_PORTADATAOUT_bus;
wire [143:0] ram_block1a34_PORTADATAOUT_bus;
wire [143:0] ram_block1a52_PORTADATAOUT_bus;
wire [143:0] ram_block1a16_PORTADATAOUT_bus;
wire [143:0] ram_block1a70_PORTADATAOUT_bus;
wire [143:0] ram_block1a125_PORTADATAOUT_bus;
wire [143:0] ram_block1a107_PORTADATAOUT_bus;
wire [143:0] ram_block1a89_PORTADATAOUT_bus;
wire [143:0] ram_block1a143_PORTADATAOUT_bus;
wire [143:0] ram_block1a35_PORTADATAOUT_bus;
wire [143:0] ram_block1a53_PORTADATAOUT_bus;
wire [143:0] ram_block1a17_PORTADATAOUT_bus;
wire [143:0] ram_block1a71_PORTADATAOUT_bus;

assign \ram_block1a108~portadataout  = ram_block1a108_PORTADATAOUT_bus[0];

assign \ram_block1a90~portadataout  = ram_block1a90_PORTADATAOUT_bus[0];

assign \ram_block1a72~portadataout  = ram_block1a72_PORTADATAOUT_bus[0];

assign \ram_block1a126~portadataout  = ram_block1a126_PORTADATAOUT_bus[0];

assign \ram_block1a18~portadataout  = ram_block1a18_PORTADATAOUT_bus[0];

assign \ram_block1a36~portadataout  = ram_block1a36_PORTADATAOUT_bus[0];

assign \ram_block1a0~portadataout  = ram_block1a0_PORTADATAOUT_bus[0];

assign \ram_block1a54~portadataout  = ram_block1a54_PORTADATAOUT_bus[0];

assign \ram_block1a109~portadataout  = ram_block1a109_PORTADATAOUT_bus[0];

assign \ram_block1a91~portadataout  = ram_block1a91_PORTADATAOUT_bus[0];

assign \ram_block1a73~portadataout  = ram_block1a73_PORTADATAOUT_bus[0];

assign \ram_block1a127~portadataout  = ram_block1a127_PORTADATAOUT_bus[0];

assign \ram_block1a19~portadataout  = ram_block1a19_PORTADATAOUT_bus[0];

assign \ram_block1a37~portadataout  = ram_block1a37_PORTADATAOUT_bus[0];

assign \ram_block1a1~portadataout  = ram_block1a1_PORTADATAOUT_bus[0];

assign \ram_block1a55~portadataout  = ram_block1a55_PORTADATAOUT_bus[0];

assign \ram_block1a110~portadataout  = ram_block1a110_PORTADATAOUT_bus[0];

assign \ram_block1a92~portadataout  = ram_block1a92_PORTADATAOUT_bus[0];

assign \ram_block1a74~portadataout  = ram_block1a74_PORTADATAOUT_bus[0];

assign \ram_block1a128~portadataout  = ram_block1a128_PORTADATAOUT_bus[0];

assign \ram_block1a20~portadataout  = ram_block1a20_PORTADATAOUT_bus[0];

assign \ram_block1a38~portadataout  = ram_block1a38_PORTADATAOUT_bus[0];

assign \ram_block1a2~portadataout  = ram_block1a2_PORTADATAOUT_bus[0];

assign \ram_block1a56~portadataout  = ram_block1a56_PORTADATAOUT_bus[0];

assign \ram_block1a111~portadataout  = ram_block1a111_PORTADATAOUT_bus[0];

assign \ram_block1a93~portadataout  = ram_block1a93_PORTADATAOUT_bus[0];

assign \ram_block1a75~portadataout  = ram_block1a75_PORTADATAOUT_bus[0];

assign \ram_block1a129~portadataout  = ram_block1a129_PORTADATAOUT_bus[0];

assign \ram_block1a21~portadataout  = ram_block1a21_PORTADATAOUT_bus[0];

assign \ram_block1a39~portadataout  = ram_block1a39_PORTADATAOUT_bus[0];

assign \ram_block1a3~portadataout  = ram_block1a3_PORTADATAOUT_bus[0];

assign \ram_block1a57~portadataout  = ram_block1a57_PORTADATAOUT_bus[0];

assign \ram_block1a112~portadataout  = ram_block1a112_PORTADATAOUT_bus[0];

assign \ram_block1a94~portadataout  = ram_block1a94_PORTADATAOUT_bus[0];

assign \ram_block1a76~portadataout  = ram_block1a76_PORTADATAOUT_bus[0];

assign \ram_block1a130~portadataout  = ram_block1a130_PORTADATAOUT_bus[0];

assign \ram_block1a22~portadataout  = ram_block1a22_PORTADATAOUT_bus[0];

assign \ram_block1a40~portadataout  = ram_block1a40_PORTADATAOUT_bus[0];

assign \ram_block1a4~portadataout  = ram_block1a4_PORTADATAOUT_bus[0];

assign \ram_block1a58~portadataout  = ram_block1a58_PORTADATAOUT_bus[0];

assign \ram_block1a113~portadataout  = ram_block1a113_PORTADATAOUT_bus[0];

assign \ram_block1a95~portadataout  = ram_block1a95_PORTADATAOUT_bus[0];

assign \ram_block1a77~portadataout  = ram_block1a77_PORTADATAOUT_bus[0];

assign \ram_block1a131~portadataout  = ram_block1a131_PORTADATAOUT_bus[0];

assign \ram_block1a23~portadataout  = ram_block1a23_PORTADATAOUT_bus[0];

assign \ram_block1a41~portadataout  = ram_block1a41_PORTADATAOUT_bus[0];

assign \ram_block1a5~portadataout  = ram_block1a5_PORTADATAOUT_bus[0];

assign \ram_block1a59~portadataout  = ram_block1a59_PORTADATAOUT_bus[0];

assign \ram_block1a114~portadataout  = ram_block1a114_PORTADATAOUT_bus[0];

assign \ram_block1a96~portadataout  = ram_block1a96_PORTADATAOUT_bus[0];

assign \ram_block1a78~portadataout  = ram_block1a78_PORTADATAOUT_bus[0];

assign \ram_block1a132~portadataout  = ram_block1a132_PORTADATAOUT_bus[0];

assign \ram_block1a24~portadataout  = ram_block1a24_PORTADATAOUT_bus[0];

assign \ram_block1a42~portadataout  = ram_block1a42_PORTADATAOUT_bus[0];

assign \ram_block1a6~portadataout  = ram_block1a6_PORTADATAOUT_bus[0];

assign \ram_block1a60~portadataout  = ram_block1a60_PORTADATAOUT_bus[0];

assign \ram_block1a115~portadataout  = ram_block1a115_PORTADATAOUT_bus[0];

assign \ram_block1a97~portadataout  = ram_block1a97_PORTADATAOUT_bus[0];

assign \ram_block1a79~portadataout  = ram_block1a79_PORTADATAOUT_bus[0];

assign \ram_block1a133~portadataout  = ram_block1a133_PORTADATAOUT_bus[0];

assign \ram_block1a25~portadataout  = ram_block1a25_PORTADATAOUT_bus[0];

assign \ram_block1a43~portadataout  = ram_block1a43_PORTADATAOUT_bus[0];

assign \ram_block1a7~portadataout  = ram_block1a7_PORTADATAOUT_bus[0];

assign \ram_block1a61~portadataout  = ram_block1a61_PORTADATAOUT_bus[0];

assign \ram_block1a116~portadataout  = ram_block1a116_PORTADATAOUT_bus[0];

assign \ram_block1a98~portadataout  = ram_block1a98_PORTADATAOUT_bus[0];

assign \ram_block1a80~portadataout  = ram_block1a80_PORTADATAOUT_bus[0];

assign \ram_block1a134~portadataout  = ram_block1a134_PORTADATAOUT_bus[0];

assign \ram_block1a26~portadataout  = ram_block1a26_PORTADATAOUT_bus[0];

assign \ram_block1a44~portadataout  = ram_block1a44_PORTADATAOUT_bus[0];

assign \ram_block1a8~portadataout  = ram_block1a8_PORTADATAOUT_bus[0];

assign \ram_block1a62~portadataout  = ram_block1a62_PORTADATAOUT_bus[0];

assign \ram_block1a117~portadataout  = ram_block1a117_PORTADATAOUT_bus[0];

assign \ram_block1a99~portadataout  = ram_block1a99_PORTADATAOUT_bus[0];

assign \ram_block1a81~portadataout  = ram_block1a81_PORTADATAOUT_bus[0];

assign \ram_block1a135~portadataout  = ram_block1a135_PORTADATAOUT_bus[0];

assign \ram_block1a27~portadataout  = ram_block1a27_PORTADATAOUT_bus[0];

assign \ram_block1a45~portadataout  = ram_block1a45_PORTADATAOUT_bus[0];

assign \ram_block1a9~portadataout  = ram_block1a9_PORTADATAOUT_bus[0];

assign \ram_block1a63~portadataout  = ram_block1a63_PORTADATAOUT_bus[0];

assign \ram_block1a118~portadataout  = ram_block1a118_PORTADATAOUT_bus[0];

assign \ram_block1a100~portadataout  = ram_block1a100_PORTADATAOUT_bus[0];

assign \ram_block1a82~portadataout  = ram_block1a82_PORTADATAOUT_bus[0];

assign \ram_block1a136~portadataout  = ram_block1a136_PORTADATAOUT_bus[0];

assign \ram_block1a28~portadataout  = ram_block1a28_PORTADATAOUT_bus[0];

assign \ram_block1a46~portadataout  = ram_block1a46_PORTADATAOUT_bus[0];

assign \ram_block1a10~portadataout  = ram_block1a10_PORTADATAOUT_bus[0];

assign \ram_block1a64~portadataout  = ram_block1a64_PORTADATAOUT_bus[0];

assign \ram_block1a119~portadataout  = ram_block1a119_PORTADATAOUT_bus[0];

assign \ram_block1a101~portadataout  = ram_block1a101_PORTADATAOUT_bus[0];

assign \ram_block1a83~portadataout  = ram_block1a83_PORTADATAOUT_bus[0];

assign \ram_block1a137~portadataout  = ram_block1a137_PORTADATAOUT_bus[0];

assign \ram_block1a29~portadataout  = ram_block1a29_PORTADATAOUT_bus[0];

assign \ram_block1a47~portadataout  = ram_block1a47_PORTADATAOUT_bus[0];

assign \ram_block1a11~portadataout  = ram_block1a11_PORTADATAOUT_bus[0];

assign \ram_block1a65~portadataout  = ram_block1a65_PORTADATAOUT_bus[0];

assign \ram_block1a120~portadataout  = ram_block1a120_PORTADATAOUT_bus[0];

assign \ram_block1a102~portadataout  = ram_block1a102_PORTADATAOUT_bus[0];

assign \ram_block1a84~portadataout  = ram_block1a84_PORTADATAOUT_bus[0];

assign \ram_block1a138~portadataout  = ram_block1a138_PORTADATAOUT_bus[0];

assign \ram_block1a30~portadataout  = ram_block1a30_PORTADATAOUT_bus[0];

assign \ram_block1a48~portadataout  = ram_block1a48_PORTADATAOUT_bus[0];

assign \ram_block1a12~portadataout  = ram_block1a12_PORTADATAOUT_bus[0];

assign \ram_block1a66~portadataout  = ram_block1a66_PORTADATAOUT_bus[0];

assign \ram_block1a121~portadataout  = ram_block1a121_PORTADATAOUT_bus[0];

assign \ram_block1a103~portadataout  = ram_block1a103_PORTADATAOUT_bus[0];

assign \ram_block1a85~portadataout  = ram_block1a85_PORTADATAOUT_bus[0];

assign \ram_block1a139~portadataout  = ram_block1a139_PORTADATAOUT_bus[0];

assign \ram_block1a31~portadataout  = ram_block1a31_PORTADATAOUT_bus[0];

assign \ram_block1a49~portadataout  = ram_block1a49_PORTADATAOUT_bus[0];

assign \ram_block1a13~portadataout  = ram_block1a13_PORTADATAOUT_bus[0];

assign \ram_block1a67~portadataout  = ram_block1a67_PORTADATAOUT_bus[0];

assign \ram_block1a122~portadataout  = ram_block1a122_PORTADATAOUT_bus[0];

assign \ram_block1a104~portadataout  = ram_block1a104_PORTADATAOUT_bus[0];

assign \ram_block1a86~portadataout  = ram_block1a86_PORTADATAOUT_bus[0];

assign \ram_block1a140~portadataout  = ram_block1a140_PORTADATAOUT_bus[0];

assign \ram_block1a32~portadataout  = ram_block1a32_PORTADATAOUT_bus[0];

assign \ram_block1a50~portadataout  = ram_block1a50_PORTADATAOUT_bus[0];

assign \ram_block1a14~portadataout  = ram_block1a14_PORTADATAOUT_bus[0];

assign \ram_block1a68~portadataout  = ram_block1a68_PORTADATAOUT_bus[0];

assign \ram_block1a123~portadataout  = ram_block1a123_PORTADATAOUT_bus[0];

assign \ram_block1a105~portadataout  = ram_block1a105_PORTADATAOUT_bus[0];

assign \ram_block1a87~portadataout  = ram_block1a87_PORTADATAOUT_bus[0];

assign \ram_block1a141~portadataout  = ram_block1a141_PORTADATAOUT_bus[0];

assign \ram_block1a33~portadataout  = ram_block1a33_PORTADATAOUT_bus[0];

assign \ram_block1a51~portadataout  = ram_block1a51_PORTADATAOUT_bus[0];

assign \ram_block1a15~portadataout  = ram_block1a15_PORTADATAOUT_bus[0];

assign \ram_block1a69~portadataout  = ram_block1a69_PORTADATAOUT_bus[0];

assign \ram_block1a124~portadataout  = ram_block1a124_PORTADATAOUT_bus[0];

assign \ram_block1a106~portadataout  = ram_block1a106_PORTADATAOUT_bus[0];

assign \ram_block1a88~portadataout  = ram_block1a88_PORTADATAOUT_bus[0];

assign \ram_block1a142~portadataout  = ram_block1a142_PORTADATAOUT_bus[0];

assign \ram_block1a34~portadataout  = ram_block1a34_PORTADATAOUT_bus[0];

assign \ram_block1a52~portadataout  = ram_block1a52_PORTADATAOUT_bus[0];

assign \ram_block1a16~portadataout  = ram_block1a16_PORTADATAOUT_bus[0];

assign \ram_block1a70~portadataout  = ram_block1a70_PORTADATAOUT_bus[0];

assign \ram_block1a125~portadataout  = ram_block1a125_PORTADATAOUT_bus[0];

assign \ram_block1a107~portadataout  = ram_block1a107_PORTADATAOUT_bus[0];

assign \ram_block1a89~portadataout  = ram_block1a89_PORTADATAOUT_bus[0];

assign \ram_block1a143~portadataout  = ram_block1a143_PORTADATAOUT_bus[0];

assign \ram_block1a35~portadataout  = ram_block1a35_PORTADATAOUT_bus[0];

assign \ram_block1a53~portadataout  = ram_block1a53_PORTADATAOUT_bus[0];

assign \ram_block1a17~portadataout  = ram_block1a17_PORTADATAOUT_bus[0];

assign \ram_block1a71~portadataout  = ram_block1a71_PORTADATAOUT_bus[0];

NCO_Party_mux_sob mux2(
	.ram_block1a108(\ram_block1a108~portadataout ),
	.ram_block1a90(\ram_block1a90~portadataout ),
	.ram_block1a72(\ram_block1a72~portadataout ),
	.ram_block1a126(\ram_block1a126~portadataout ),
	.ram_block1a18(\ram_block1a18~portadataout ),
	.ram_block1a36(\ram_block1a36~portadataout ),
	.ram_block1a0(\ram_block1a0~portadataout ),
	.ram_block1a54(\ram_block1a54~portadataout ),
	.ram_block1a109(\ram_block1a109~portadataout ),
	.ram_block1a91(\ram_block1a91~portadataout ),
	.ram_block1a73(\ram_block1a73~portadataout ),
	.ram_block1a127(\ram_block1a127~portadataout ),
	.ram_block1a19(\ram_block1a19~portadataout ),
	.ram_block1a37(\ram_block1a37~portadataout ),
	.ram_block1a1(\ram_block1a1~portadataout ),
	.ram_block1a55(\ram_block1a55~portadataout ),
	.ram_block1a110(\ram_block1a110~portadataout ),
	.ram_block1a92(\ram_block1a92~portadataout ),
	.ram_block1a74(\ram_block1a74~portadataout ),
	.ram_block1a128(\ram_block1a128~portadataout ),
	.ram_block1a20(\ram_block1a20~portadataout ),
	.ram_block1a38(\ram_block1a38~portadataout ),
	.ram_block1a2(\ram_block1a2~portadataout ),
	.ram_block1a56(\ram_block1a56~portadataout ),
	.ram_block1a111(\ram_block1a111~portadataout ),
	.ram_block1a93(\ram_block1a93~portadataout ),
	.ram_block1a75(\ram_block1a75~portadataout ),
	.ram_block1a129(\ram_block1a129~portadataout ),
	.ram_block1a21(\ram_block1a21~portadataout ),
	.ram_block1a39(\ram_block1a39~portadataout ),
	.ram_block1a3(\ram_block1a3~portadataout ),
	.ram_block1a57(\ram_block1a57~portadataout ),
	.ram_block1a112(\ram_block1a112~portadataout ),
	.ram_block1a94(\ram_block1a94~portadataout ),
	.ram_block1a76(\ram_block1a76~portadataout ),
	.ram_block1a130(\ram_block1a130~portadataout ),
	.ram_block1a22(\ram_block1a22~portadataout ),
	.ram_block1a40(\ram_block1a40~portadataout ),
	.ram_block1a4(\ram_block1a4~portadataout ),
	.ram_block1a58(\ram_block1a58~portadataout ),
	.ram_block1a113(\ram_block1a113~portadataout ),
	.ram_block1a95(\ram_block1a95~portadataout ),
	.ram_block1a77(\ram_block1a77~portadataout ),
	.ram_block1a131(\ram_block1a131~portadataout ),
	.ram_block1a23(\ram_block1a23~portadataout ),
	.ram_block1a41(\ram_block1a41~portadataout ),
	.ram_block1a5(\ram_block1a5~portadataout ),
	.ram_block1a59(\ram_block1a59~portadataout ),
	.ram_block1a114(\ram_block1a114~portadataout ),
	.ram_block1a96(\ram_block1a96~portadataout ),
	.ram_block1a78(\ram_block1a78~portadataout ),
	.ram_block1a132(\ram_block1a132~portadataout ),
	.ram_block1a24(\ram_block1a24~portadataout ),
	.ram_block1a42(\ram_block1a42~portadataout ),
	.ram_block1a6(\ram_block1a6~portadataout ),
	.ram_block1a60(\ram_block1a60~portadataout ),
	.ram_block1a115(\ram_block1a115~portadataout ),
	.ram_block1a97(\ram_block1a97~portadataout ),
	.ram_block1a79(\ram_block1a79~portadataout ),
	.ram_block1a133(\ram_block1a133~portadataout ),
	.ram_block1a25(\ram_block1a25~portadataout ),
	.ram_block1a43(\ram_block1a43~portadataout ),
	.ram_block1a7(\ram_block1a7~portadataout ),
	.ram_block1a61(\ram_block1a61~portadataout ),
	.ram_block1a116(\ram_block1a116~portadataout ),
	.ram_block1a98(\ram_block1a98~portadataout ),
	.ram_block1a80(\ram_block1a80~portadataout ),
	.ram_block1a134(\ram_block1a134~portadataout ),
	.ram_block1a26(\ram_block1a26~portadataout ),
	.ram_block1a44(\ram_block1a44~portadataout ),
	.ram_block1a8(\ram_block1a8~portadataout ),
	.ram_block1a62(\ram_block1a62~portadataout ),
	.ram_block1a117(\ram_block1a117~portadataout ),
	.ram_block1a99(\ram_block1a99~portadataout ),
	.ram_block1a81(\ram_block1a81~portadataout ),
	.ram_block1a135(\ram_block1a135~portadataout ),
	.ram_block1a27(\ram_block1a27~portadataout ),
	.ram_block1a45(\ram_block1a45~portadataout ),
	.ram_block1a9(\ram_block1a9~portadataout ),
	.ram_block1a63(\ram_block1a63~portadataout ),
	.ram_block1a118(\ram_block1a118~portadataout ),
	.ram_block1a100(\ram_block1a100~portadataout ),
	.ram_block1a82(\ram_block1a82~portadataout ),
	.ram_block1a136(\ram_block1a136~portadataout ),
	.ram_block1a28(\ram_block1a28~portadataout ),
	.ram_block1a46(\ram_block1a46~portadataout ),
	.ram_block1a10(\ram_block1a10~portadataout ),
	.ram_block1a64(\ram_block1a64~portadataout ),
	.ram_block1a119(\ram_block1a119~portadataout ),
	.ram_block1a101(\ram_block1a101~portadataout ),
	.ram_block1a83(\ram_block1a83~portadataout ),
	.ram_block1a137(\ram_block1a137~portadataout ),
	.ram_block1a29(\ram_block1a29~portadataout ),
	.ram_block1a47(\ram_block1a47~portadataout ),
	.ram_block1a11(\ram_block1a11~portadataout ),
	.ram_block1a65(\ram_block1a65~portadataout ),
	.ram_block1a120(\ram_block1a120~portadataout ),
	.ram_block1a102(\ram_block1a102~portadataout ),
	.ram_block1a84(\ram_block1a84~portadataout ),
	.ram_block1a138(\ram_block1a138~portadataout ),
	.ram_block1a30(\ram_block1a30~portadataout ),
	.ram_block1a48(\ram_block1a48~portadataout ),
	.ram_block1a12(\ram_block1a12~portadataout ),
	.ram_block1a66(\ram_block1a66~portadataout ),
	.ram_block1a121(\ram_block1a121~portadataout ),
	.ram_block1a103(\ram_block1a103~portadataout ),
	.ram_block1a85(\ram_block1a85~portadataout ),
	.ram_block1a139(\ram_block1a139~portadataout ),
	.ram_block1a31(\ram_block1a31~portadataout ),
	.ram_block1a49(\ram_block1a49~portadataout ),
	.ram_block1a13(\ram_block1a13~portadataout ),
	.ram_block1a67(\ram_block1a67~portadataout ),
	.ram_block1a122(\ram_block1a122~portadataout ),
	.ram_block1a104(\ram_block1a104~portadataout ),
	.ram_block1a86(\ram_block1a86~portadataout ),
	.ram_block1a140(\ram_block1a140~portadataout ),
	.ram_block1a32(\ram_block1a32~portadataout ),
	.ram_block1a50(\ram_block1a50~portadataout ),
	.ram_block1a14(\ram_block1a14~portadataout ),
	.ram_block1a68(\ram_block1a68~portadataout ),
	.ram_block1a123(\ram_block1a123~portadataout ),
	.ram_block1a105(\ram_block1a105~portadataout ),
	.ram_block1a87(\ram_block1a87~portadataout ),
	.ram_block1a141(\ram_block1a141~portadataout ),
	.ram_block1a33(\ram_block1a33~portadataout ),
	.ram_block1a51(\ram_block1a51~portadataout ),
	.ram_block1a15(\ram_block1a15~portadataout ),
	.ram_block1a69(\ram_block1a69~portadataout ),
	.ram_block1a124(\ram_block1a124~portadataout ),
	.ram_block1a106(\ram_block1a106~portadataout ),
	.ram_block1a88(\ram_block1a88~portadataout ),
	.ram_block1a142(\ram_block1a142~portadataout ),
	.ram_block1a34(\ram_block1a34~portadataout ),
	.ram_block1a52(\ram_block1a52~portadataout ),
	.ram_block1a16(\ram_block1a16~portadataout ),
	.ram_block1a70(\ram_block1a70~portadataout ),
	.ram_block1a125(\ram_block1a125~portadataout ),
	.ram_block1a107(\ram_block1a107~portadataout ),
	.ram_block1a89(\ram_block1a89~portadataout ),
	.ram_block1a143(\ram_block1a143~portadataout ),
	.ram_block1a35(\ram_block1a35~portadataout ),
	.ram_block1a53(\ram_block1a53~portadataout ),
	.ram_block1a17(\ram_block1a17~portadataout ),
	.ram_block1a71(\ram_block1a71~portadataout ),
	.out_address_reg_a_1(\out_address_reg_a[1]~q ),
	.out_address_reg_a_0(\out_address_reg_a[0]~q ),
	.out_address_reg_a_2(\out_address_reg_a[2]~q ),
	._(_),
	._1(_1),
	._2(_2),
	._3(_3),
	._4(_4),
	._5(_5),
	._6(_6),
	._7(_7),
	._8(_8),
	._9(_9),
	._10(_10),
	._11(_11),
	._12(_12),
	._13(_13),
	._14(_14),
	._15(_15),
	._16(_16),
	._17(_17));

cycloneive_ram_block ram_block1a108(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a108_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a108.clk0_core_clock_enable = "ena0";
defparam ram_block1a108.clk0_input_clock_enable = "ena0";
defparam ram_block1a108.clk0_output_clock_enable = "ena0";
defparam ram_block1a108.data_interleave_offset_in_bits = 1;
defparam ram_block1a108.data_interleave_width_in_bits = 1;
defparam ram_block1a108.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a108.init_file_layout = "port_a";
defparam ram_block1a108.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a108.operation_mode = "rom";
defparam ram_block1a108.port_a_address_clear = "none";
defparam ram_block1a108.port_a_address_width = 13;
defparam ram_block1a108.port_a_data_out_clear = "none";
defparam ram_block1a108.port_a_data_out_clock = "clock0";
defparam ram_block1a108.port_a_data_width = 1;
defparam ram_block1a108.port_a_first_address = 49152;
defparam ram_block1a108.port_a_first_bit_number = 0;
defparam ram_block1a108.port_a_last_address = 57343;
defparam ram_block1a108.port_a_logical_ram_depth = 65536;
defparam ram_block1a108.port_a_logical_ram_width = 18;
defparam ram_block1a108.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a108.ram_block_type = "auto";
defparam ram_block1a108.mem_init3 = 2048'h52A952AD5A952B52B5295AD6B5AD6B4A5AD2D2D2D2D2DA5B496D24B6D24924924924936DB24DB24D9364D93264C9932664CC9999B3333333333331999CCCE673398CE7318C6318C738C738E71C71C71C71C78E1C78F1E3C3C7878787C3C1E0F87C1F03E07C0FC07E03F807F007FC00FFC001FFF80001FFFFFFFC0000000007FFFFFFF00003FFF000FFE00FF803FC07F01F80FC0F81F07C1F0783C1E1F0F0F1E1E3C78F1E3871C70E38E71C718E31CE718C6319CE63398CC6663333999999999999B3332664CD9B366CD9364D926C936D9249249A492492DB496D25B4B4B694B4B4A5AD6B4AD6B5295A95AB56AD52AD52AB552AAD556AAAB5555555AAAAAAAAAA;
defparam ram_block1a108.mem_init2 = 2048'h95555556AAAA5552AAD54AA552AD52A54AD4AD4AD6B5294B5AD2D696969696D2DA4B6D2496DB6DB6DB64936D936C9B26C993264C99933266666CCCCCCC6666733399CCE6339CE631CE718E71CE38E38E38E3871E3C78F0E1E1E1E1E0F0783E0F83F03E07F03F80FF00FF801FFC003FFFC00001FFFFFFFFFFFFFFFF000003FFF8007FF003FE01FC03F81F81F81F07C1F07C3E1E1E1E1E1C3C78F1C38E1C71C71CE38C738C739CE7318CE6733998CCCE6666666666CCCD99B3264C99366C9B64DB249B6DB6DB6DB6D24B6D25A4B4B4B4B4A5AD294A5295AD4AD5A952AD52AD56AAD552AAB55556AAAAAAAAAAAAAAAAAA55555AAA9556AA552A956AD5AB52B5295A;
defparam ram_block1a108.mem_init1 = 2048'hD6B5AD296B4B4B4B49692DA496DB6924926DB6C936C9364D93264C99336664CCCCC9998CCCCCE6673399CC6339CE738C738C71C738E1C71C38F1E3C78787878787C1E0F83E07C0FC0FE03F807FC01FF8007FFF00000FFFFFFFFFFFFFFFF800007FFF000FFC01FF00FE03F81F81F03E0F83C1E0F0F0F0F1E1C3870E38F1C71C738E31CE318E7318CE73399CCC6666733333366666CCD993364C9B26C9B24DB24926DB6DA4925B692D25A4B4B5A5A5296B5AD4A56A56A54A956AB55AA9552AAB55554AAAAAAAAAAAAAAAAB55554AAAD55AAB55AA55AB56A56A56B5294A5AD69694B69696D25B492DB6DB6DB6DB24DB24D9364D9B366CC999B33333363333333999;
defparam ram_block1a108.mem_init0 = 2048'hCCE63398C6318C738C718E38E38E3871C3870F1E1E1E1E0F0F83E0F83F03F03F80FE01FF007FF0007FFFC0000000FFFFFF80000001FFFF000FFE007FC03F80FE07E07C0F83E1F0787C3C3C7878F1E3871C70E39C71CE39C6318C6319CC67331998CCCCCCCCCCCC999B3366CD9B26C9B26D924DB6DB6DB6DB492DB4B69696969694B5AD6B5AD4AD4AD5AB54AB55AA9552AAB55556AAAAAAAAAAAAAAAAA55556AAA554AAD56A956AD5A95A95AD6B5AD694B4B5A4B4B696DA496DB6DB6DB6D924DB26C9B264D9B32664CCD9999999999CCCC6673398C6739CE718E718E38E38E38E1C78F1E3C3C3C3C3E1F0F83E0FC1F81FC07F00FF007FE001FFFC00001FFFFFFF;

cycloneive_ram_block ram_block1a90(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a90_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a90.clk0_core_clock_enable = "ena0";
defparam ram_block1a90.clk0_input_clock_enable = "ena0";
defparam ram_block1a90.clk0_output_clock_enable = "ena0";
defparam ram_block1a90.data_interleave_offset_in_bits = 1;
defparam ram_block1a90.data_interleave_width_in_bits = 1;
defparam ram_block1a90.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a90.init_file_layout = "port_a";
defparam ram_block1a90.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a90.operation_mode = "rom";
defparam ram_block1a90.port_a_address_clear = "none";
defparam ram_block1a90.port_a_address_width = 13;
defparam ram_block1a90.port_a_data_out_clear = "none";
defparam ram_block1a90.port_a_data_out_clock = "clock0";
defparam ram_block1a90.port_a_data_width = 1;
defparam ram_block1a90.port_a_first_address = 40960;
defparam ram_block1a90.port_a_first_bit_number = 0;
defparam ram_block1a90.port_a_last_address = 49151;
defparam ram_block1a90.port_a_logical_ram_depth = 65536;
defparam ram_block1a90.port_a_logical_ram_width = 18;
defparam ram_block1a90.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a90.ram_block_type = "auto";
defparam ram_block1a90.mem_init3 = 2048'hFFFFFFF000007FFF000FFC01FE01FC07F03F07E0F83E1F0F878787878F1E3C70E38E38E38E31CE31CE739CC63399CCC666733333333336664CC99B364C9B26C9B64936DB6DB6DB6D24B6D2DA5A4B5A5A52D6B5AD6B52B52B56AD52AD56AA554AAAD5554AAAAAAAAAAAAAAAAAD5555AAA9552AB55AA55AB56A56A56B5AD6B5A52D2D2D2D2DA5B6925B6DB6DB6DB64936C9B26C9B366CD99B332666666666666333199CC67318C6318C738E71C738E1C71C38F1E3C3C78787C3C1F0F83E07C0FC0FE03F807FC00FFE001FFFF00000003FFFFFE00000007FFFC001FFC01FF00FE03F81F81F83E0F83E1E0F0F0F0F1E1C3871C38E38E38E31C639C6318C63398CE67;
defparam ram_block1a90.mem_init2 = 2048'h3339999998D999999B33266CD9B364D93649B649B6DB6DB6DB6925B496D2D2DA52D2D6B4A5295AD4AD4AD5AB54AB55AAB556AAA55555AAAAAAAAAAAAAAAAA55555AAA9552AB55AAD52A54AD4AD4A56B5AD294B4B5A5A4B49692DB4924B6DB6C9249B649B26C9B264D9933666CCCCD999999CCCCC6673399CE6319CE318E718E39C71C71E38E1C3870F1E1E1E1E0F0783E0F81F03F03F80FE01FF007FE001FFFC00003FFFFFFFFFFFFFFFE00001FFFC003FF007FC03F80FE07E07C0F83E0F07C3C3C3C3C3C78F1E3871C70E39C71C639C639CE7398C673399CCCE666663332666664CCD993264C99364D926D926DB6C92492DB6D24B692D25A5A5A5AD296B5AD6;
defparam ram_block1a90.mem_init1 = 2048'hB5295A95AB56AD52A954AAD552AAB55554AAAAAAAAAAAAAAAAAAD5555AAA9556AAD56A956A952B56A56B5294A5296B4A5A5A5A5A4B496DA496DB6DB6DB6DB249B64DB26CD93264C99B336666CCCCCCCCCCE66633399CCE6319CE739C639C638E71C71C70E3871E3C7870F0F0F0F0F87C1F07C1F03F03F03F807F00FF801FFC003FFF800001FFFFFFFFFFFFFFFF000007FFF8007FF003FE01FE03F81FC0F81F83E0F83C1E0F0F0F0F0E1E3C78F1C38E38E38E38E71CE31CE718CE7398CE6733999CCCCC6666666CCCCC99933264C99326C9B26D936D924DB6DB6DB6D2496DA4B696D2D2D2D2D696B5A5295AD6A56A56A54A956A954AA556AA9554AAAAD5555552;
defparam ram_block1a90.mem_init0 = 2048'hAAAAAAAAAB5555555AAAAD556AA955AA956A956AD5AB52B5295AD6A5AD6B4A5A5A52DA5A5B496D25B6924924B24924936D926C9364D9366CD9B3664CC9999B3333333333339998CCC663398CE7318C631CE718E31C71CE38E1C71C38F1E3C78F0F1E1E1F0F0783C1F07C1F03E07E03F01FC07F803FE00FFE001FFF80001FFFFFFFC0000000007FFFFFFF00003FFF0007FE007FC01FC03F80FC07E07C0F81F07C3E0F0787C3C3C3C7878F1E3C70E3C71C71C71C71CE39C639C6318C6319CE63399CCE6673331999999999999B3332664CC993264C99364D93649B649B6D92492492492496DA496D25B4B69696969696B4A5AD6B5AD6B5295A95A952B56A952A95;

cycloneive_ram_block ram_block1a72(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a72_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a72.clk0_core_clock_enable = "ena0";
defparam ram_block1a72.clk0_input_clock_enable = "ena0";
defparam ram_block1a72.clk0_output_clock_enable = "ena0";
defparam ram_block1a72.data_interleave_offset_in_bits = 1;
defparam ram_block1a72.data_interleave_width_in_bits = 1;
defparam ram_block1a72.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a72.init_file_layout = "port_a";
defparam ram_block1a72.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a72.operation_mode = "rom";
defparam ram_block1a72.port_a_address_clear = "none";
defparam ram_block1a72.port_a_address_width = 13;
defparam ram_block1a72.port_a_data_out_clear = "none";
defparam ram_block1a72.port_a_data_out_clock = "clock0";
defparam ram_block1a72.port_a_data_width = 1;
defparam ram_block1a72.port_a_first_address = 32768;
defparam ram_block1a72.port_a_first_bit_number = 0;
defparam ram_block1a72.port_a_last_address = 40959;
defparam ram_block1a72.port_a_logical_ram_depth = 65536;
defparam ram_block1a72.port_a_logical_ram_width = 18;
defparam ram_block1a72.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a72.ram_block_type = "auto";
defparam ram_block1a72.mem_init3 = 2048'h6AB552AA5552AAB55552AAAAAA5555555555555552AAAAAA55556AAAD552AA556AB55AA55AA54A95AB52B5A95AD6B5AD6B5A52D2969696969692D25B496DA496DB6D249249B6DB64936C93649B26C9B364C993264CD99B33266666CCCCCCCCCCE6666333399CCE67319CC6339CE739C631CE31C638E71C71C71C71E38F1C38F1E3C3878F0F0F0F0F0787C3E1F07C1F07C0F81F81F81FC0FE01FC03FC01FF003FF800FFFC0007FFFF0000000FFFFFFFFFFFFFFFFF80000007FFFF0000FFF8007FF003FE00FF00FF01FC07F03F01F03F07E0F83E0F83E1F0F8783C3C3C3C3C3878F0E1C3871E38F1C70E38E38E38C71C638C718E738C6318C6318CE7319CC66331;
defparam ram_block1a72.mem_init2 = 2048'h98CCE6663333331999999999B333336666CCD99B3264CD9B364C9B26C9B26C9B649B649B6D924936DB6DB6DB492496DA496DA4B692D25A5B4B4B4B4B4A5A52D294B5AD294A56B5AD4AD6A56A56AD4A952A55AB54AB55AAD56AA554AAB554AAAD5552AAAA5555552AAAAAAAAAAAAAAAAAAAAAAAAAAB5555552AAAA5555AAA9554AAB552AA552A954AA55AA54AB56AD4A95AB52B52B5A94AD6B5294A52D6B5A52D694B4B5A5A5A5A5A4B4B49692DA4B692DA496DB4924B6DB6DB6DB6DB6DB24926DB249B649B64DB26C9B26C9B264D9B264C993366CC99B336664CCD9999B333333336666673333333319999CCCC66733199CCE63319CC67318CE7398C6318C631;
defparam ram_block1a72.mem_init1 = 2048'hCE738C738C738E71CE38C71C718E38E38E3871C71C38E3C70E3C70E1C3870E1C3C7878F0F0E1E1E1E1E0F0F0F8783C1E0F07C3E0F87C1F03E0F83F07E0FC0FC0FC0FC0FC07E03F80FE03FC07F807F807FC01FF007FE007FF001FFE001FFF0003FFFC0003FFFF800001FFFFFFE00000000001FFFFFFFFFFFFFFFFFFFFC00000000003FFFFFFC000007FFFF80003FFFC0007FFC001FFE001FFC007FE007FE007FC01FE00FF00FF00FF01FE03F80FE03F81FC07E07E03F03F03F07E07C0F81F03E0FC1F07C1F07C1F07C3E0F07C3E1F0F87C3E1E0F0F07878787C3C3C3C3C78787878F0F1E1E3C3878F1E1C3870E1C3871E3C70E3C70E3C71E38F1C71E38E3871C7;
defparam ram_block1a72.mem_init0 = 2048'h1C71C71E38E71C71C71C71CE38E39C71CE38C71CE39C738C718E718E718E718E738C639CE318C639CE739CE739CE739CC6318CE7398C67398C67319CE63398CE63399CC673399CC6633198CC6633399CCC66733199CCCE6673331998CCCC66663333399998CCCCCC66666663333333333199999999999999999999999999999999999993333333333266666666CCCCCC999999B3333266664CCCC9999B33366664CCC999B3336664CCD9993336664CC999333666CCD99B32664CC99933666CC99B33664CD9933266CC99B3266CC9933664CD993266CC9933664C99B3264CD9B3264CD9B3264C99B3664C993366CD993264CD9B3664C993366CD9B3264C993366;

cycloneive_ram_block ram_block1a126(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a126_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a126.clk0_core_clock_enable = "ena0";
defparam ram_block1a126.clk0_input_clock_enable = "ena0";
defparam ram_block1a126.clk0_output_clock_enable = "ena0";
defparam ram_block1a126.data_interleave_offset_in_bits = 1;
defparam ram_block1a126.data_interleave_width_in_bits = 1;
defparam ram_block1a126.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a126.init_file_layout = "port_a";
defparam ram_block1a126.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a126.operation_mode = "rom";
defparam ram_block1a126.port_a_address_clear = "none";
defparam ram_block1a126.port_a_address_width = 13;
defparam ram_block1a126.port_a_data_out_clear = "none";
defparam ram_block1a126.port_a_data_out_clock = "clock0";
defparam ram_block1a126.port_a_data_width = 1;
defparam ram_block1a126.port_a_first_address = 57344;
defparam ram_block1a126.port_a_first_bit_number = 0;
defparam ram_block1a126.port_a_last_address = 65535;
defparam ram_block1a126.port_a_logical_ram_depth = 65536;
defparam ram_block1a126.port_a_logical_ram_width = 18;
defparam ram_block1a126.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a126.ram_block_type = "auto";
defparam ram_block1a126.mem_init3 = 2048'hCD993264C99B366CD993264CD9B3664C993366CD993264CD9B3264C99B3664C99B3664C99B3264CD993266CC9933664CD993266CC99B3266CC99933664CD99B3266CCD99332664CC99B33666CCD999332664CCD9993336664CCD999B3326664CCCD999B333266664CCCC99999B333332666666CCCCCCCC99999999999333333333333333333333333333333333333319999999998CCCCCCC6666663333399998CCCC66663331999CCCE66733199CCC66733998CC6633198CC673399CC673398CE63398CE7319CC6339CC6339CE6318C6739CE739CE739CE738C6318E738C639CE31CE31CE31CE31C639C738E71C638E71C738E38E71C71C71C71CE38F1C71C71;
defparam ram_block1a126.mem_init2 = 2048'hC71C38E38F1C71E38F1C78E1C78E1C78F1C3870E1C3870F1E3C3878F0F1E1E3C3C3C3C787878787C3C3C3C1E1E0F0F87C3E1F0F87C1E0F87C1F07C1F07C1F07E0F81F03E07C0FC1F81F81F80FC0FC07F03F80FE03F80FF01FE01FE01FE00FF007FC00FFC00FFC007FF000FFF0007FFC0007FFF80003FFFFC000007FFFFFF800000000007FFFFFFFFFFFFFFFFFFFF00000000000FFFFFFF000003FFFF80007FFF8001FFF000FFF001FFC00FFC01FF007FC03FC03FC07F80FE03F80FC07E07E07E07E07E0FC1F83E0F81F07C3E0F87C1E0F0783C3E1E1E0F0F0F0F0E1E1E3C3C7870E1C3870E1C78E1C78E3871C71C38E38E38E31C71C638E71CE39C639C639CE7;
defparam ram_block1a126.mem_init1 = 2048'h18C6318C6339CE6319CC673198CE6733199CCC66673333199999999CCCCCD99999999B33336664CCD99B3266CD993264C9B364C9B26C9B26C9B64DB24DB249B6C9249B6DB6DB6DB6DB6DA4925B6D24B692DA4B692D25A5A4B4B4B4B4B5A5A52D694B5AD694A5295AD6A52B5A95A95AB52A56AD5AA54AB54AA552A954AA955AAA5552AAB5554AAAA9555555AAAAAAAAAAAAAAAAAAAAAAAAAAA9555554AAAA95556AAA555AAA554AAD56AB55AA55AB54A952A56AD4AD4AD6A56B5AD4A5296B5A529694B4A5A5A5A5A5B4B49692DA4B6D24B6D24925B6DB6DB6D924936DB24DB24DB26C9B26C9B264D9B3664C99B33666CCCD99999B3333333331999998CCCE6633;
defparam ram_block1a126.mem_init0 = 2048'h198CC67319CE6318C6318C639CE31C638C71C638E38E38E1C71E38F1C3870E1E3C3878787878783C3E1F0F83E0F83E0FC1F81F01F81FC07F01FE01FE00FF801FFC003FFE0001FFFFC0000003FFFFFFFFFFFFFFFFE0000001FFFFC0007FFE003FF801FF007F807F00FE07F03F03F03E07C1F07C1F0F87C3C1E1E1E1E1E3C3878F1E3871E38F1C71C71C71CE38C718E718C739CE7398C67319CCE67339998CCCCE6666666666CCCCC999B33664C993264D9B26C9B24D926D924DB6DB2492496DB6D24B6D25B49692D2D2D2D2D29694B5AD6B5AD6B52B5A95AB52A54AB54AB55AAD54AA9556AAAD5554AAAAAA9555555555555554AAAAAA95555AAA9554AA955AAD;

cycloneive_ram_block ram_block1a18(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a18_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a18.clk0_core_clock_enable = "ena0";
defparam ram_block1a18.clk0_input_clock_enable = "ena0";
defparam ram_block1a18.clk0_output_clock_enable = "ena0";
defparam ram_block1a18.data_interleave_offset_in_bits = 1;
defparam ram_block1a18.data_interleave_width_in_bits = 1;
defparam ram_block1a18.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a18.init_file_layout = "port_a";
defparam ram_block1a18.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a18.operation_mode = "rom";
defparam ram_block1a18.port_a_address_clear = "none";
defparam ram_block1a18.port_a_address_width = 13;
defparam ram_block1a18.port_a_data_out_clear = "none";
defparam ram_block1a18.port_a_data_out_clock = "clock0";
defparam ram_block1a18.port_a_data_width = 1;
defparam ram_block1a18.port_a_first_address = 8192;
defparam ram_block1a18.port_a_first_bit_number = 0;
defparam ram_block1a18.port_a_last_address = 16383;
defparam ram_block1a18.port_a_logical_ram_depth = 65536;
defparam ram_block1a18.port_a_logical_ram_width = 18;
defparam ram_block1a18.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a18.ram_block_type = "auto";
defparam ram_block1a18.mem_init3 = 2048'hFFFFFFF000007FFF000FFC01FE01FC07F03F07E0F83E1F0F878787878F1E3C70E38E38E38E31CE31CE739CC63399CCC666733333333336664CC99B364C9B26C9B64936DB6DB6DB6D24B6D2DA5A4B5A5A52D6B5AD6B52B52B56AD52AD56AA554AAAD5554AAAAAAAAAAAAAAAAAD5555AAA9552AB55AA55AB56A56A56B5AD6B5A52D2D2D2D2DA5B6925B6DB6DB6DB64936C9B26C9B366CD99B332666666666666333199CC67318C6318C738E71C738E1C71C38F1E3C3C78787C3C1F0F83E07C0FC0FE03F807FC00FFE001FFFF00000003FFFFFE00000007FFFC001FFC01FF00FE03F81F81F83E0F83E1E0F0F0F0F1E1C3871C38E38E38E31C639C6318C63398CE67;
defparam ram_block1a18.mem_init2 = 2048'h3339999998D999999B33266CD9B364D93649B649B6DB6DB6DB6925B496D2D2DA52D2D6B4A5295AD4AD4AD5AB54AB55AAB556AAA55555AAAAAAAAAAAAAAAAA55555AAA9552AB55AAD52A54AD4AD4A56B5AD294B4B5A5A4B49692DB4924B6DB6C9249B649B26C9B264D9933666CCCCD999999CCCCC6673399CE6319CE318E718E39C71C71E38E1C3870F1E1E1E1E0F0783E0F81F03F03F80FE01FF007FE001FFFC00003FFFFFFFFFFFFFFFE00001FFFC003FF007FC03F80FE07E07C0F83E0F07C3C3C3C3C3C78F1E3871C70E39C71C639C639CE7398C673399CCCE666663332666664CCD993264C99364D926D926DB6C92492DB6D24B692D25A5A5A5AD296B5AD6;
defparam ram_block1a18.mem_init1 = 2048'hB5295A95AB56AD52A954AAD552AAB55554AAAAAAAAAAAAAAAAAAD5555AAA9556AAD56A956A952B56A56B5294A5296B4A5A5A5A5A4B496DA496DB6DB6DB6DB249B64DB26CD93264C99B336666CCCCCCCCCCE66633399CCE6319CE739C639C638E71C71C70E3871E3C7870F0F0F0F0F87C1F07C1F03F03F03F807F00FF801FFC003FFF800001FFFFFFFFFFFFFFFF000007FFF8007FF003FE01FE03F81FC0F81F83E0F83C1E0F0F0F0F0E1E3C78F1C38E38E38E38E71CE31CE718CE7398CE6733999CCCCC6666666CCCCC99933264C99326C9B26D936D924DB6DB6DB6D2496DA4B696D2D2D2D2D696B5A5295AD6A56A56A54A956A954AA556AA9554AAAAD5555552;
defparam ram_block1a18.mem_init0 = 2048'hAAAAAAAAAB5555555AAAAD556AA955AA956A956AD5AB52B5295AD6A5AD6B4A5A5A52DA5A5B496D25B6924924B24924936D926C9364D9366CD9B3664CC9999B3333333333339998CCC663398CE7318C631CE718E31C71CE38E1C71C38F1E3C78F0F1E1E1F0F0783C1F07C1F03E07E03F01FC07F803FE00FFE001FFF80001FFFFFFFC0000000007FFFFFFF00003FFF0007FE007FC01FC03F80FC07E07C0F81F07C3E0F0787C3C3C3C7878F1E3C70E3C71C71C71C71CE39C639C6318C6319CE63399CCE6673331999999999999B3332664CC993264C99364D93649B649B6D92492492492496DA496D25B4B69696969696B4A5AD6B5AD6B5295A95A952B56A952A95;

cycloneive_ram_block ram_block1a36(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a36_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a36.clk0_core_clock_enable = "ena0";
defparam ram_block1a36.clk0_input_clock_enable = "ena0";
defparam ram_block1a36.clk0_output_clock_enable = "ena0";
defparam ram_block1a36.data_interleave_offset_in_bits = 1;
defparam ram_block1a36.data_interleave_width_in_bits = 1;
defparam ram_block1a36.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a36.init_file_layout = "port_a";
defparam ram_block1a36.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a36.operation_mode = "rom";
defparam ram_block1a36.port_a_address_clear = "none";
defparam ram_block1a36.port_a_address_width = 13;
defparam ram_block1a36.port_a_data_out_clear = "none";
defparam ram_block1a36.port_a_data_out_clock = "clock0";
defparam ram_block1a36.port_a_data_width = 1;
defparam ram_block1a36.port_a_first_address = 16384;
defparam ram_block1a36.port_a_first_bit_number = 0;
defparam ram_block1a36.port_a_last_address = 24575;
defparam ram_block1a36.port_a_logical_ram_depth = 65536;
defparam ram_block1a36.port_a_logical_ram_width = 18;
defparam ram_block1a36.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a36.ram_block_type = "auto";
defparam ram_block1a36.mem_init3 = 2048'h52A952AD5A952B52B5295AD6B5AD6B4A5AD2D2D2D2D2DA5B496D24B6D24924924924936DB24DB24D9364D93264C9932664CC9999B3333333333331999CCCE673398CE7318C6318C738C738E71C71C71C71C78E1C78F1E3C3C7878787C3C1E0F87C1F03E07C0FC07E03F807F007FC00FFC001FFF80001FFFFFFFC0000000007FFFFFFF00003FFF000FFE00FF803FC07F01F80FC0F81F07C1F0783C1E1F0F0F1E1E3C78F1E3871C70E38E71C718E31CE718C6319CE63398CC6663333999999999999B3332664CD9B366CD9364D926C936D9249249A492492DB496D25B4B4B694B4B4A5AD6B4AD6B5295A95AB56AD52AD52AB552AAD556AAAB5555555AAAAAAAAAA;
defparam ram_block1a36.mem_init2 = 2048'h95555556AAAA5552AAD54AA552AD52A54AD4AD4AD6B5294B5AD2D696969696D2DA4B6D2496DB6DB6DB64936D936C9B26C993264C99933266666CCCCCCC6666733399CCE6339CE631CE718E71CE38E38E38E3871E3C78F0E1E1E1E1E0F0783E0F83F03E07F03F80FF00FF801FFC003FFFC00001FFFFFFFFFFFFFFFF000003FFF8007FF003FE01FC03F81F81F81F07C1F07C3E1E1E1E1E1C3C78F1C38E1C71C71CE38C738C739CE7318CE6733998CCCE6666666666CCCD99B3264C99366C9B64DB249B6DB6DB6DB6D24B6D25A4B4B4B4B4A5AD294A5295AD4AD5A952AD52AD56AAD552AAB55556AAAAAAAAAAAAAAAAAA55555AAA9556AA552A956AD5AB52B5295A;
defparam ram_block1a36.mem_init1 = 2048'hD6B5AD296B4B4B4B49692DA496DB6924926DB6C936C9364D93264C99336664CCCCC9998CCCCCE6673399CC6339CE738C738C71C738E1C71C38F1E3C78787878787C1E0F83E07C0FC0FE03F807FC01FF8007FFF00000FFFFFFFFFFFFFFFF800007FFF000FFC01FF00FE03F81F81F03E0F83C1E0F0F0F0F1E1C3870E38F1C71C738E31CE318E7318CE73399CCC6666733333366666CCD993364C9B26C9B24DB24926DB6DA4925B692D25A4B4B5A5A5296B5AD4A56A56A54A956AB55AA9552AAB55554AAAAAAAAAAAAAAAAB55554AAAD55AAB55AA55AB56A56A56B5294A5AD69694B69696D25B492DB6DB6DB6DB24DB24D9364D9B366CC999B33333363333333999;
defparam ram_block1a36.mem_init0 = 2048'hCCE63398C6318C738C718E38E38E3871C3870F1E1E1E1E0F0F83E0F83F03F03F80FE01FF007FF0007FFFC0000000FFFFFF80000001FFFF000FFE007FC03F80FE07E07C0F83E1F0787C3C3C7878F1E3871C70E39C71CE39C6318C6319CC67331998CCCCCCCCCCCC999B3366CD9B26C9B26D924DB6DB6DB6DB492DB4B69696969694B5AD6B5AD4AD4AD5AB54AB55AA9552AAB55556AAAAAAAAAAAAAAAAA55556AAA554AAD56A956AD5A95A95AD6B5AD694B4B5A4B4B696DA496DB6DB6DB6D924DB26C9B264D9B32664CCD9999999999CCCC6673398C6739CE718E718E38E38E38E1C78F1E3C3C3C3C3E1F0F83E0FC1F81FC07F00FF007FE001FFFC00001FFFFFFF;

cycloneive_ram_block ram_block1a0(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a0_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a0.clk0_core_clock_enable = "ena0";
defparam ram_block1a0.clk0_input_clock_enable = "ena0";
defparam ram_block1a0.clk0_output_clock_enable = "ena0";
defparam ram_block1a0.data_interleave_offset_in_bits = 1;
defparam ram_block1a0.data_interleave_width_in_bits = 1;
defparam ram_block1a0.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a0.init_file_layout = "port_a";
defparam ram_block1a0.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a0.operation_mode = "rom";
defparam ram_block1a0.port_a_address_clear = "none";
defparam ram_block1a0.port_a_address_width = 13;
defparam ram_block1a0.port_a_data_out_clear = "none";
defparam ram_block1a0.port_a_data_out_clock = "clock0";
defparam ram_block1a0.port_a_data_width = 1;
defparam ram_block1a0.port_a_first_address = 0;
defparam ram_block1a0.port_a_first_bit_number = 0;
defparam ram_block1a0.port_a_last_address = 8191;
defparam ram_block1a0.port_a_logical_ram_depth = 65536;
defparam ram_block1a0.port_a_logical_ram_width = 18;
defparam ram_block1a0.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a0.ram_block_type = "auto";
defparam ram_block1a0.mem_init3 = 2048'h6AB552AA5552AAB55552AAAAAA5555555555555552AAAAAA55556AAAD552AA556AB55AA55AA54A95AB52B5A95AD6B5AD6B5A52D2969696969692D25B496DA496DB6D249249B6DB64936C93649B26C9B364C993264CD99B33266666CCCCCCCCCCE6666333399CCE67319CC6339CE739C631CE31C638E71C71C71C71E38F1C38F1E3C3878F0F0F0F0F0787C3E1F07C1F07C0F81F81F81FC0FE01FC03FC01FF003FF800FFFC0007FFFF0000000FFFFFFFFFFFFFFFFF80000007FFFF0000FFF8007FF003FE00FF00FF01FC07F03F01F03F07E0F83E0F83E1F0F8783C3C3C3C3C3878F0E1C3871E38F1C70E38E38E38C71C638C718E738C6318C6318CE7319CC66331;
defparam ram_block1a0.mem_init2 = 2048'h98CCE6663333331999999999B333336666CCD99B3264CD9B364C9B26C9B26C9B649B649B6D924936DB6DB6DB492496DA496DA4B692D25A5B4B4B4B4B4A5A52D294B5AD294A56B5AD4AD6A56A56AD4A952A55AB54AB55AAD56AA554AAB554AAAD5552AAAA5555552AAAAAAAAAAAAAAAAAAAAAAAAAAB5555552AAAA5555AAA9554AAB552AA552A954AA55AA54AB56AD4A95AB52B52B5A94AD6B5294A52D6B5A52D694B4B5A5A5A5A5A4B4B49692DA4B692DA496DB4924B6DB6DB6DB6DB6DB24926DB249B649B64DB26C9B26C9B264D9B264C993366CC99B336664CCD9999B333333336666673333333319999CCCC66733199CCE63319CC67318CE7398C6318C631;
defparam ram_block1a0.mem_init1 = 2048'hCE738C738C738E71CE38C71C718E38E38E3871C71C38E3C70E3C70E1C3870E1C3C7878F0F0E1E1E1E1E0F0F0F8783C1E0F07C3E0F87C1F03E0F83F07E0FC0FC0FC0FC0FC07E03F80FE03FC07F807F807FC01FF007FE007FF001FFE001FFF0003FFFC0003FFFF800001FFFFFFE00000000001FFFFFFFFFFFFFFFFFFFFC00000000003FFFFFFC000007FFFF80003FFFC0007FFC001FFE001FFC007FE007FE007FC01FE00FF00FF00FF01FE03F80FE03F81FC07E07E03F03F03F07E07C0F81F03E0FC1F07C1F07C1F07C3E0F07C3E1F0F87C3E1E0F0F07878787C3C3C3C3C78787878F0F1E1E3C3878F1E1C3870E1C3871E3C70E3C70E3C71E38F1C71E38E3871C7;
defparam ram_block1a0.mem_init0 = 2048'h1C71C71E38E71C71C71C71CE38E39C71CE38C71CE39C738C718E718E718E718E738C639CE318C639CE739CE739CE739CC6318CE7398C67398C67319CE63398CE63399CC673399CC6633198CC6633399CCC66733199CCCE6673331998CCCC66663333399998CCCCCC66666663333333333199999999999999999999999999999999999993333333333266666666CCCCCC999999B3333266664CCCC9999B33366664CCC999B3336664CCD9993336664CC999333666CCD99B32664CC99933666CC99B33664CD9933266CC99B3266CC9933664CD993266CC9933664C99B3264CD9B3264CD9B3264C99B3664C993366CD993264CD9B3664C993366CD9B3264C993366;

cycloneive_ram_block ram_block1a54(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a54_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a54.clk0_core_clock_enable = "ena0";
defparam ram_block1a54.clk0_input_clock_enable = "ena0";
defparam ram_block1a54.clk0_output_clock_enable = "ena0";
defparam ram_block1a54.data_interleave_offset_in_bits = 1;
defparam ram_block1a54.data_interleave_width_in_bits = 1;
defparam ram_block1a54.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a54.init_file_layout = "port_a";
defparam ram_block1a54.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a54.operation_mode = "rom";
defparam ram_block1a54.port_a_address_clear = "none";
defparam ram_block1a54.port_a_address_width = 13;
defparam ram_block1a54.port_a_data_out_clear = "none";
defparam ram_block1a54.port_a_data_out_clock = "clock0";
defparam ram_block1a54.port_a_data_width = 1;
defparam ram_block1a54.port_a_first_address = 24576;
defparam ram_block1a54.port_a_first_bit_number = 0;
defparam ram_block1a54.port_a_last_address = 32767;
defparam ram_block1a54.port_a_logical_ram_depth = 65536;
defparam ram_block1a54.port_a_logical_ram_width = 18;
defparam ram_block1a54.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a54.ram_block_type = "auto";
defparam ram_block1a54.mem_init3 = 2048'hCD993264C99B366CD993264CD9B3664C993366CD993264CD9B3264C99B3664C99B3664C99B3264CD993266CC9933664CD993266CC99B3266CC99933664CD99B3266CCD99332664CC99B33666CCD999332664CCD9993336664CCD999B3326664CCCD999B333266664CCCC99999B333332666666CCCCCCCC99999999999333333333333333333333333333333333333319999999998CCCCCCC6666663333399998CCCC66663331999CCCE66733199CCC66733998CC6633198CC673399CC673398CE63398CE7319CC6339CC6339CE6318C6739CE739CE739CE738C6318E738C639CE31CE31CE31CE31C639C738E71C638E71C738E38E71C71C71C71CE38F1C71C71;
defparam ram_block1a54.mem_init2 = 2048'hC71C38E38F1C71E38F1C78E1C78E1C78F1C3870E1C3870F1E3C3878F0F1E1E3C3C3C3C787878787C3C3C3C1E1E0F0F87C3E1F0F87C1E0F87C1F07C1F07C1F07E0F81F03E07C0FC1F81F81F80FC0FC07F03F80FE03F80FF01FE01FE01FE00FF007FC00FFC00FFC007FF000FFF0007FFC0007FFF80003FFFFC000007FFFFFF800000000007FFFFFFFFFFFFFFFFFFFF00000000000FFFFFFF000003FFFF80007FFF8001FFF000FFF001FFC00FFC01FF007FC03FC03FC07F80FE03F80FC07E07E07E07E07E0FC1F83E0F81F07C3E0F87C1E0F0783C3E1E1E0F0F0F0F0E1E1E3C3C7870E1C3870E1C78E1C78E3871C71C38E38E38E31C71C638E71CE39C639C639CE7;
defparam ram_block1a54.mem_init1 = 2048'h18C6318C6339CE6319CC673198CE6733199CCC66673333199999999CCCCCD99999999B33336664CCD99B3266CD993264C9B364C9B26C9B26C9B64DB24DB249B6C9249B6DB6DB6DB6DB6DA4925B6D24B692DA4B692D25A5A4B4B4B4B4B5A5A52D694B5AD694A5295AD6A52B5A95A95AB52A56AD5AA54AB54AA552A954AA955AAA5552AAB5554AAAA9555555AAAAAAAAAAAAAAAAAAAAAAAAAAA9555554AAAA95556AAA555AAA554AAD56AB55AA55AB54A952A56AD4AD4AD6A56B5AD4A5296B5A529694B4A5A5A5A5A5B4B49692DA4B6D24B6D24925B6DB6DB6D924936DB24DB24DB26C9B26C9B264D9B3664C99B33666CCCD99999B3333333331999998CCCE6633;
defparam ram_block1a54.mem_init0 = 2048'h198CC67319CE6318C6318C639CE31C638C71C638E38E38E1C71E38F1C3870E1E3C3878787878783C3E1F0F83E0F83E0FC1F81F01F81FC07F01FE01FE00FF801FFC003FFE0001FFFFC0000003FFFFFFFFFFFFFFFFE0000001FFFFC0007FFE003FF801FF007F807F00FE07F03F03F03E07C1F07C1F0F87C3C1E1E1E1E1E3C3878F1E3871E38F1C71C71C71CE38C718E718C739CE7398C67319CCE67339998CCCCE6666666666CCCCC999B33664C993264D9B26C9B24D926D924DB6DB2492496DB6D24B6D25B49692D2D2D2D2D29694B5AD6B5AD6B52B5A95AB52A54AB54AB55AAD54AA9556AAAD5554AAAAAA9555555555555554AAAAAA95555AAA9554AA955AAD;

cycloneive_ram_block ram_block1a109(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a109_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a109.clk0_core_clock_enable = "ena0";
defparam ram_block1a109.clk0_input_clock_enable = "ena0";
defparam ram_block1a109.clk0_output_clock_enable = "ena0";
defparam ram_block1a109.data_interleave_offset_in_bits = 1;
defparam ram_block1a109.data_interleave_width_in_bits = 1;
defparam ram_block1a109.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a109.init_file_layout = "port_a";
defparam ram_block1a109.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a109.operation_mode = "rom";
defparam ram_block1a109.port_a_address_clear = "none";
defparam ram_block1a109.port_a_address_width = 13;
defparam ram_block1a109.port_a_data_out_clear = "none";
defparam ram_block1a109.port_a_data_out_clock = "clock0";
defparam ram_block1a109.port_a_data_width = 1;
defparam ram_block1a109.port_a_first_address = 49152;
defparam ram_block1a109.port_a_first_bit_number = 1;
defparam ram_block1a109.port_a_last_address = 57343;
defparam ram_block1a109.port_a_logical_ram_depth = 65536;
defparam ram_block1a109.port_a_logical_ram_width = 18;
defparam ram_block1a109.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a109.ram_block_type = "auto";
defparam ram_block1a109.mem_init3 = 2048'h9CCE63319CE6339CC6319CE739CE738C631CE31CE31CE39C718E38C71C71C71C71C71C71C38E3C71E3871E3C78F1E3C7870F1E1E3C3C3C3C3C3C3E1E1F0F0783C1F0F83E0F83E0F83F07C0F81F81F81F81F80FE07F01FC03F807F807FC01FF007FE003FF800FFF8003FFF80007FFFF000001FFFFFFFE0000000000000000000000000FFFFFFFF000001FFFF80003FFF0007FFC007FF003FF007FC01FF00FF01FE03F80FE07F03F01F81F03F07E0FC1F07C1F07C1E0F87C3E1E0F0F8787878787878F0F1E1C3C78F1E3C70E3C71E38F1C71C71C79C71C71C738E31C738C718C738C639CE739CE7318C67398CE63319CCE6733199CCCE666733333339999999999;
defparam ram_block1a109.mem_init2 = 2048'hB33333326666CCC999B3266CC99B366CD9B264D9B26C9B26C9B64DB24DB24DB64926DB6DB2492492492DB6DB4925B6925B496D25B4B696D2D2DA5A5A5AD2D2D696B4A5AD694A5294A52B5AD4A56A56A56A56AD4A952A55AB54AB54AA552A955AA9556AAD556AAA5555AAAAB555556AAAAAAAAB5555555555555555AAAAAAAAAD55555AAAAB5556AAAD552AAD55AA955AA954AB54AB54A956AD5A952B56A56A56B5295AD6A5294A5AD6B4A5AD2D696B4B4B4B4B4B69692D25B496D25B492DB692492DB6DB6DB6DB649249B6C926D926D936C9B26C9B26C99366CD9B3664C99B336664CCD9999B33333333333333333399999CCCE6673399CCE67319CC6339CE63;
defparam ram_block1a109.mem_init1 = 2048'h18C631CE738C738C718E31C718E38E38E38E38F1C70E3871E3C78F1E3C7878F0F0F1E1F0F0F0F8783C1E0F83C1F07C0F83F07E07C0FE07E03F01FC07F807F807F801FF003FF800FFF0003FFF80001FFFFF800000000FFFFFFFFFFFFFFFF800000000FFFFFC0000FFFE0007FF800FFE007FC01FF00FF00FE03F80FE07F03F03F07E0FC1F07E0F07C1F0F87C3C1E1E0F0F0F0E1E1E3C3870F1C3871E3871C38E38E1C71C638E38E71CE39C738C639CE718C6339CE6319CC673198CC667331998CCCCC66666666666666666CCCCD999B33666CC993366CD9326CD9364D9364DB24D924DB24936DB6492492492496DB6924B6D24B692DA5B4B6969696D6969696B4B;
defparam ram_block1a109.mem_init0 = 2048'h5A5296B5AD6B5AD6A52B5A95A95A952B56AD5AB54AB54AA55AA955AA9556AA9555AAAB55552AAAAAD55555555555AAAAAAD55555555555AAAAAB55556AAAD554AAB556AAD54AA552A956A952AD5AB52A56A54AD6A56B5294A5294A5296B5A5AD2D6969696969692D2DA5B496D24B6D24B6DB6924924924926DB6D924DB24DB24D926C9B26C99366C993266CD99332664CCD9999B3333333333333333399998CCC66733198CE67319CE6319CE739CE718C739C738C718E38E71C71C71C71E38E3C70E3C78E1C3C7870F1E1E1E1E1E1F0F0787C3E0F87C1F07E0F81F03F03F03F01F80FE03FC03FC03FE00FFC00FFE001FFF8000FFFF800001FFFFFFFFE0000000;

cycloneive_ram_block ram_block1a91(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a91_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a91.clk0_core_clock_enable = "ena0";
defparam ram_block1a91.clk0_input_clock_enable = "ena0";
defparam ram_block1a91.clk0_output_clock_enable = "ena0";
defparam ram_block1a91.data_interleave_offset_in_bits = 1;
defparam ram_block1a91.data_interleave_width_in_bits = 1;
defparam ram_block1a91.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a91.init_file_layout = "port_a";
defparam ram_block1a91.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a91.operation_mode = "rom";
defparam ram_block1a91.port_a_address_clear = "none";
defparam ram_block1a91.port_a_address_width = 13;
defparam ram_block1a91.port_a_data_out_clear = "none";
defparam ram_block1a91.port_a_data_out_clock = "clock0";
defparam ram_block1a91.port_a_data_width = 1;
defparam ram_block1a91.port_a_first_address = 40960;
defparam ram_block1a91.port_a_first_bit_number = 1;
defparam ram_block1a91.port_a_last_address = 49151;
defparam ram_block1a91.port_a_logical_ram_depth = 65536;
defparam ram_block1a91.port_a_logical_ram_width = 18;
defparam ram_block1a91.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a91.ram_block_type = "auto";
defparam ram_block1a91.mem_init3 = 2048'h0000000FFFFFFFFF000003FFFE0003FFF000FFE007FE00FF807F807F80FE03F01F81F81F81F03E0FC1F07C3E0F87C3C1E1F0F0F0F0F0F1E1C3C7870E3C78E1C78E38F1C71C71C71CE38E31C639C739C631CE739CE7318CE7319CCE633199CCC6663333399999999999999999B33336664CC9993366CC99326CD9326C9B26C93649B649B64936DB6C92492492492DB6DA496DA496D25B4B69692D2D2D2D2D2D696B4B5AD294A5294A5295AD4AD6A54AD4A95AB56A952AD52A954AA556AAD55AAA5556AAAD5555AAAAAB555555555556AAAAAB555555555556AAAAA95555AAAB5552AAD552AB552AB54AA55AA55AB56AD5A952B52B52B5A94AD6B5AD6B5AD294B5;
defparam ram_block1a91.mem_init2 = 2048'hA5AD2D2D2D6D2D2D2DA5B4B692DA496DA492DB6D24924924924DB6D9249B6493649B64D9364D9366C99366CD993266CCD99B33366666CCCCCCCCCCCCCCCCC66666333199CCC663319CC67318CE7398C631CE738C639C738E71CE38E38C71C70E38E3871C38F1C3871E1C3878F0F0E1E1E1E0F0F0787C3E1F07C1E0FC1F07E0FC1F81F81FC0FE03F80FE01FE01FF007FC00FFE003FFC000FFFE00007FFFFE000000003FFFFFFFFFFFFFFFE000000003FFFFF00003FFF8001FFE003FF801FF003FC03FC03FC07F01F80FC0FE07C0FC1F83E07C1F0783E0F0783C3E1E1E1F0F1E1E1E3C3C78F1E3C78F1C38E1C71E38E38E38E38E31C718E31C639C639CE718C631;
defparam ram_block1a91.mem_init1 = 2048'h8CE7398C67319CCE673399CCCE66733333999999999999999999B33336664CCD99B3264CD9B366CD9326C9B26C9B26D936C936C926DB24924DB6DB6DB6DB692492DB6925B496D25B49692D2DA5A5A5A5A5AD2D696B4A5AD6B4A5294AD6B5295AD4AD4AD5A952B56AD52A55AA55AA552AB552AB556AA9556AAAD555AAAAB555556AAAAAAAAB5555555555555555AAAAAAAAAD55555AAAAB5554AAAD556AAD552AB552A954AA55AA55AB54A952A56AD4AD4AD4AD4A56B5A94A5294A52D6B4A5AD2D69696B4B4B4B69696D2DA5B496D25B492DB4925B6DB69249249249B6DB6C924DB649B649B64DB26C9B26C9B364C9B366CD9B3266CC99B332666CCCC9999999B;
defparam ram_block1a91.mem_init0 = 2048'h33333333339999999CCCCE66733199CCE673198CE6339CC6319CE739CE738C639C631C639C718E39C71C71C73C71C71C71E38F1C78E1C78F1E3C7870F1E1E3C3C3C3C3C3C3E1E0F0F87C3E0F07C1F07C1F07E0FC1F81F03F01F81FC0FE03F80FF01FE01FF007FC01FF801FFC007FFC001FFF80003FFFF000001FFFFFFFE0000000000000000000000000FFFFFFFF000001FFFFC0003FFF8003FFE003FF800FFC01FF007FC03FC03F807F01FC0FE03F03F03F03F03E07C1F83E0F83E0F83E1F0783C1E1F0F0F8787878787878F0F1E1C3C78F1E3C78F1C38F1C78E3871C71C71C71C71C71C638E31C738E718E718E718C639CE739CE7318C67398CE73198CE673;

cycloneive_ram_block ram_block1a73(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a73_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a73.clk0_core_clock_enable = "ena0";
defparam ram_block1a73.clk0_input_clock_enable = "ena0";
defparam ram_block1a73.clk0_output_clock_enable = "ena0";
defparam ram_block1a73.data_interleave_offset_in_bits = 1;
defparam ram_block1a73.data_interleave_width_in_bits = 1;
defparam ram_block1a73.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a73.init_file_layout = "port_a";
defparam ram_block1a73.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a73.operation_mode = "rom";
defparam ram_block1a73.port_a_address_clear = "none";
defparam ram_block1a73.port_a_address_width = 13;
defparam ram_block1a73.port_a_data_out_clear = "none";
defparam ram_block1a73.port_a_data_out_clock = "clock0";
defparam ram_block1a73.port_a_data_width = 1;
defparam ram_block1a73.port_a_first_address = 32768;
defparam ram_block1a73.port_a_first_bit_number = 1;
defparam ram_block1a73.port_a_last_address = 40959;
defparam ram_block1a73.port_a_logical_ram_depth = 65536;
defparam ram_block1a73.port_a_logical_ram_width = 18;
defparam ram_block1a73.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a73.ram_block_type = "auto";
defparam ram_block1a73.mem_init3 = 2048'h198CCE663331998CCCCE6666663333333333333336666666CCCCD999B33666CCD993366CC993264C99366C9B364D9364D936C9B64DB24DB24DB64936DB24924DB6DB6DB6DB6DB6D24925B6D2496DA496D25B496D25B4B69692D2D25A5A5A5A5A52D2D69694B5A52D6B4A5296B5AD6B5294A56B5295AD4AD4AD4AD4A95AB56A54A956AD5AA55AA55AAD52A954AAD54AAD55AAB554AAB555AAAB5556AAAB55556AAAAA55555552AAAAAAAAAAA555555555555555552AAAAAAAAAAA55555552AAAAA55554AAAA5555AAA9555AAA555AAA554AAD54AAD54AA552AD56A956A956AD52A54A952A54AD5A95AB52B52B5295A94AD6A52B5AD6B5AD6B5AD6B5A5296B4A5A;
defparam ram_block1a73.mem_init2 = 2048'hD296B4B4A5A5A5AD2D2D2D2D25A5A5B4B49692D25B49692DA496D24B6D24B6D2492DB6D24924925B6DB6DB6D924924936DB6C924DB64936D926D926D936C9B64D926C9B26C9B26C99364C9B364C993264C993266CD9933664CC999332666CCC9999B33336666664CCCCCCCCCCCCCCCCCCCCCCCCCCC666666333339999CCCE66733399CCC6633198CC663398CC67318CE6339CC6339CE7318C6318C6318C639CE718C739C639C639C738C718E31C738E31C718E38E38C71C71C71C71C71C38E38E3C71C78E3871C38F1C38F1C3871E3C78F1E3C78F0E1C3C7878F0E1E1E3C3C3C3C3878787C3C3C3C3E1E1E0F0F8783C1E1F0F83C1E0F87C1F0F83E0F83E0F83E;
defparam ram_block1a73.mem_init1 = 2048'h0F83F07C0F83F07E0FC0F81F81F03F03F03F81F81FC0FC07F03F80FE03F80FE03F807F00FF01FE01FE00FF00FF803FE00FF803FF007FE003FF003FF800FFF000FFF000FFF8003FFF0003FFF80007FFF80001FFFF800007FFFFE000001FFFFFFC00000003FFFFFFFFFE000000000000000001FFFFFFFFFFFFFFFFFFFFC000000000000000003FFFFFFFFFF800000003FFFFFFC000001FFFFFC00001FFFFE00003FFFE0000FFFF0000FFFE0007FFE0007FFC001FFE000FFF000FFE003FF800FFE003FF003FF003FF003FE00FFC01FF007FC01FE00FF007F807FC03FC03FC07F807F80FF01FE03F807F01FC07F01FC07F01FC0FE03F01FC0FE07F03F01F81F80FC0;
defparam ram_block1a73.mem_init0 = 2048'hFC0FC0FE07E0FC0FC0FC0FC1F81F83F03E07C0FC1F83F07C0F81F07E0F81F07E0F83E07C1F07C1F83E0F83E0F83E0F83C1F07C1F0783E0F87C1F0F83E1F0783E1F0783C1F0F87C3E1F0F87C3E1F0F87C3C1E0F0F87C3C1E1F0F0F8783C3C1E1E0F0F078787C3C3C3E1E1E1E0F0F0F0F0F078787878787878787878787878787878787870F0F0F0F0F1E1E1E1E1C3C3C38787878F0F0E1E1E3C3C387878F0F1E1E3C3C7878F0F1E1C3C3878F0F1E1C3C7870F0E1E3C3878F1E1C3C7870F1E1C3878F0E1C3C78F0E1E3C7870E1E3C78F0E1C3C78F1E1C3870F1E3C7870E1C3C78F1E3C3870E1C3878F1E3C78F0E1C3870E1C3C78F1E3C78F0E1C3870E1C3870F1E;

cycloneive_ram_block ram_block1a127(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a127_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a127.clk0_core_clock_enable = "ena0";
defparam ram_block1a127.clk0_input_clock_enable = "ena0";
defparam ram_block1a127.clk0_output_clock_enable = "ena0";
defparam ram_block1a127.data_interleave_offset_in_bits = 1;
defparam ram_block1a127.data_interleave_width_in_bits = 1;
defparam ram_block1a127.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a127.init_file_layout = "port_a";
defparam ram_block1a127.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a127.operation_mode = "rom";
defparam ram_block1a127.port_a_address_clear = "none";
defparam ram_block1a127.port_a_address_width = 13;
defparam ram_block1a127.port_a_data_out_clear = "none";
defparam ram_block1a127.port_a_data_out_clock = "clock0";
defparam ram_block1a127.port_a_data_width = 1;
defparam ram_block1a127.port_a_first_address = 57344;
defparam ram_block1a127.port_a_first_bit_number = 1;
defparam ram_block1a127.port_a_last_address = 65535;
defparam ram_block1a127.port_a_logical_ram_depth = 65536;
defparam ram_block1a127.port_a_logical_ram_width = 18;
defparam ram_block1a127.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a127.ram_block_type = "auto";
defparam ram_block1a127.mem_init3 = 2048'hF1E1C3870E1C3870E1E3C78F1E3C7870E1C3870E1E3C78F1E3C3870E1C3878F1E3C7870E1C3C78F1E1C3870F1E3C7870E1E3C78F0E1C3C78F0E1E3C7870E1E3C3870F1E1C3C7870F1E3C3878F0E1E1C3C7870F1E1E3C387870F1E1E3C3C7878F0F1E1E3C3C387878F0F0E1E1E3C3C3C38787870F0F0F0F1E1E1E1E1E1C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C1E1E1E1E1E0F0F0F0F878787C3C3C1E1E0F0F078783C3E1E1F0F0787C3E1E0F0787C3E1F0F87C3E1F0F87C3E1F0783C1F0F83C1F0F83E1F07C3E0F83C1F07C1F0783E0F83E0F83E0F83F07C1F07C0F83E0FC1F03E0FC1F03E07C1F83F07E07C0F81F83F03F07E07E07E07E0FC0FE07E07E;
defparam ram_block1a127.mem_init2 = 2048'h07E03F03F01F81FC0FE07F01F80FE07F01FC07F01FC07F01FC03F80FF01FE03FC03FC07F807F807FC03FC01FE00FF007FC01FF007FE00FF801FF801FF801FF800FFE003FF800FFE001FFE000FFF0007FFC000FFFC000FFFE0001FFFE0000FFFF80000FFFFF000007FFFFF0000007FFFFFF800000003FFFFFFFFFF8000000000000000007FFFFFFFFFFFFFFFFFFFF000000000000000000FFFFFFFFFF800000007FFFFFF000000FFFFFC00003FFFF00003FFFC0003FFF8001FFF8003FFE001FFE001FFE003FF801FF800FFC01FF803FE00FF803FE01FE00FF00FF01FE01FC03F80FE03F80FE03F81FC07E07F03F03F81F81F81F03F03E07E0FC1F83E07C1F83E0;
defparam ram_block1a127.mem_init1 = 2048'hF83E0F83E0F83E1F07C3E0F0783E1F0F0783C3E1E0F0F0F87878787C3C3C3878787878F0F0E1E3C3C7870E1E3C78F1E3C78F1C3871E3871E3871C38E3C71C78E38E3871C71C71C71C71C638E38E31C718E39C718E31C639C738C738C739C631CE738C6318C6318C6319CE7398C67398CE6319CC663398CC6633198CC66733999CCCE667333399998CCCCCC6666666666666666666666666664CCCCCD9999B3332666CCC999332664CD993366CC993264C993264D9B264D9326C9B26C9B26C9364DB26D936C936C936D924DB64926DB6D924924936DB6DB6DB492492496DB692496DA496DA496D24B692D25B49692D25A5B4B4B49696969696B4B4B4A5A5AD296;
defparam ram_block1a127.mem_init0 = 2048'hB4A5AD294B5AD6B5AD6B5AD6B5A94AD6A52B5295A95A95AB52B56A54A952A54A956AD52AD52AD56A954AA556AA556AA554AAB554AAB5552AAB5554AAAA55554AAAAA95555554AAAAAAAAAAA955555555555555554AAAAAAAAAAA95555554AAAAAD5555AAAAD555AAAB555AAA555AAB556AA556AA552A956AB54AB54AB56AD52A54AD5AB52A56A56A56A56B5295AD4A5295AD6B5AD294A5AD694B5A52D2D69694B4B4B4B4B4969692D2DA5B496D25B496D24B6D2496DB492496DB6DB6DB6DB6DB649249B6D924DB649B649B64DB26D9364D9364D9B26CD93264C993266CD9933666CCD99B33366666CCCCCCD999999999999998CCCCCCE66663331998CCE66331;

cycloneive_ram_block ram_block1a19(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a19_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a19.clk0_core_clock_enable = "ena0";
defparam ram_block1a19.clk0_input_clock_enable = "ena0";
defparam ram_block1a19.clk0_output_clock_enable = "ena0";
defparam ram_block1a19.data_interleave_offset_in_bits = 1;
defparam ram_block1a19.data_interleave_width_in_bits = 1;
defparam ram_block1a19.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a19.init_file_layout = "port_a";
defparam ram_block1a19.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a19.operation_mode = "rom";
defparam ram_block1a19.port_a_address_clear = "none";
defparam ram_block1a19.port_a_address_width = 13;
defparam ram_block1a19.port_a_data_out_clear = "none";
defparam ram_block1a19.port_a_data_out_clock = "clock0";
defparam ram_block1a19.port_a_data_width = 1;
defparam ram_block1a19.port_a_first_address = 8192;
defparam ram_block1a19.port_a_first_bit_number = 1;
defparam ram_block1a19.port_a_last_address = 16383;
defparam ram_block1a19.port_a_logical_ram_depth = 65536;
defparam ram_block1a19.port_a_logical_ram_width = 18;
defparam ram_block1a19.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a19.ram_block_type = "auto";
defparam ram_block1a19.mem_init3 = 2048'hFFFFFFFFFFFF8000000FFFFE0001FFF8003FF800FFC01FF007F807F80FE03F80FC0FC0FC0FC1F03E0F83E0F83C1E0F078783C3C3C3C3C7878F0E1C3870E3C70E3871C71C71C71C71C738E31C638C639C6318C6318C6339CC67319CCE6733998CCCE66673333333333333333366666CCCD99B3266CC993264C9B364D9364D93649B649B64936DB24924924924924925B6D24B6D25B496D2DA5B4B4B4B4B4B4B5A5AD296B5A5294A5295AD4A56A52B56A56AD5AB56A952AD56A955AAD54AA9556AAB5552AAA955554AAAAAAA5555555555555555555552AAAAAAB55554AAAA5556AAB554AA955AA954AA55AA55AB54A952B56A56A56A56B5294AD6B5AD694A5AD2;
defparam ram_block1a19.mem_init2 = 2048'h9694B4B4B5B4B4B4B69692DA4B692DB492DB6D24924924924924936DB249B6493649B26D9364C9B264D9B366CD9933666CCD9993333366666666666666666333339998CCE673399CCE6339CC6339CE739CE738C739C638C718E38C71C71C71C71C78E3871E3871E3C78F0E1E3C3C3878787C3C3C1E0F0783E1F07C1F07E0F81F83F03F01F81FC07F00FE01FE01FF007FE007FF000FFF8001FFFF00001FFFFFFC00000000000000000000000001FFFFFFC00007FFFC000FFF8007FF003FF007FC03FC03FC07F01FC07E07F03E07E07C1F83E0F83E0F87C3E1F0F078787C3C38787870F1E1C3870E1C78E1C71E38E38F1C71CE38E38C71CE39C639C631CE739CE7;
defparam ram_block1a19.mem_init1 = 2048'h39CE6319CC67319CCE6733199CCCC6666733333333333333333366666CCCD99B33664CD9B3264D9B364D9B26C9B24D936C936C936D924936DB6DB6DB6DB6DB6D2496DB496DA4B692D25A4B4B69696969694B4B5A52D694B5AD6B5AD6B5294AD4A56A56A54AD5AB56AD5AA55AA55AAD56AA556AA555AAA5552AAA55552AAAA95555552AAAAAAAAAAAAAAAAAAAAAAAAAAD5555552AAAA95554AAA9554AAA554AA955AA954AA55AA55AA54A952A54A95A95A95A95AD4A56B5AD4A5AD6B5A52D694B4A5A5AD2D2D2DA5A5A4B49692DA4B6925B6924B6DB4924924924924924DB6D924DB649B649B24D936C9B364D9326CD93264CD9B3266CCD99B33266664CCCCCC9;
defparam ram_block1a19.mem_init0 = 2048'h9999999998CCCCCCC66663331998CC6673198CE63398CE7318C6319C6318C639C631C639C738E31C718E38E38E38E38F1C71E38F1C38F1E3C78F1E3C387878F0F0F0F0F0F078783C3E1F0783E0F07C1F03E0F81F03F03E07E03F03F80FE03F80FF01FE00FF007FC00FFC00FFE001FFF0003FFF80001FFFFE0000007FFFFFFFFFFFC0000000007FFFFFFFFFFFC0000007FFFF80001FFFC000FFF8007FF001FF803FF007F803FC03F807F01FC07F03F81F81F81F81F03E07C1F83E0F83E1F07C3E1F0F8783C3E1E1E1E1E1E1E3C3C3878F0E1C3870E1C78E1C78E3871C71E38E38E38E38E71C718E39C738E718E718E738C6318C6318C6319CE6319CC67319CCE6;

cycloneive_ram_block ram_block1a37(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a37_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a37.clk0_core_clock_enable = "ena0";
defparam ram_block1a37.clk0_input_clock_enable = "ena0";
defparam ram_block1a37.clk0_output_clock_enable = "ena0";
defparam ram_block1a37.data_interleave_offset_in_bits = 1;
defparam ram_block1a37.data_interleave_width_in_bits = 1;
defparam ram_block1a37.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a37.init_file_layout = "port_a";
defparam ram_block1a37.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a37.operation_mode = "rom";
defparam ram_block1a37.port_a_address_clear = "none";
defparam ram_block1a37.port_a_address_width = 13;
defparam ram_block1a37.port_a_data_out_clear = "none";
defparam ram_block1a37.port_a_data_out_clock = "clock0";
defparam ram_block1a37.port_a_data_width = 1;
defparam ram_block1a37.port_a_first_address = 16384;
defparam ram_block1a37.port_a_first_bit_number = 1;
defparam ram_block1a37.port_a_last_address = 24575;
defparam ram_block1a37.port_a_logical_ram_depth = 65536;
defparam ram_block1a37.port_a_logical_ram_width = 18;
defparam ram_block1a37.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a37.ram_block_type = "auto";
defparam ram_block1a37.mem_init3 = 2048'hCE67319CC67318CE7318C6318C6318C639CE31CE31CE39C738E31C71CE38E38E38E38F1C71C38E3C70E3C70E1C3870E1E3C387878F0F0F0F0F0F0F8783C3E1F0F87C1F0F83E0F83F07C0F81F03F03F03F03F81FC07F01FC03F807F803FC01FF803FF001FFC003FFE0007FFF00003FFFFC0000007FFFFFFFFFFFC0000000007FFFFFFFFFFFC000000FFFFF00003FFF8001FFF000FFE007FE007FC01FE00FF01FE03F80FE03F81F80FC0F81F81F03E0F81F07C1E0F83C1F0F8783C3C1E1E1E1E1E1E3C3C3878F1E3C78F1E3871E38F1C71E38E38E38E38E31C718E39C738C718C738C6318C7318C6319CE63398CE63319CCC663331998CCCC66666663333333333;
defparam ram_block1a37.mem_init2 = 2048'h26666664CCCC999B33666CC99B3664C99366C99364D9B26D93649B24DB24DB64936DB64924924924924925B6DA492DB492DA4B692D25A4B4B4B6969696B4B4A5A52D694B5AD6B4A56B5AD4A56B52B52B52B52A54A952A54AB54AB54AA552AB552AA554AAA5552AAA55552AAAA95555556AAAAAAAAAAAAAAAAAAAAAAAAAA95555552AAAA95554AAA9554AAB554AAD54AAD56AB54AB54AB56AD5AB56A54AD4AD4A56A5295AD6B5AD6B5A52D694B5A5A52D2D2D2D2DA5A4B49692DA4B6D25B6D2496DB6DB6DB6DB6DB6D924936D926D926D93649B26C9B364D9B364C99B3664CD99B336666CCCCD999999999999999999CCCCC666733199CCE67319CC67318CE739;
defparam ram_block1a37.mem_init1 = 2048'hCE739CE718C738C738E71C638E38E71C71E38E38F1C70E3C70E1C3870F1E1C3C3C38787C3C3C1E1F0F87C3E0F83E0F83F07C0FC0F81FC0FC07F01FC07F807F807FC01FF801FFC003FFE0007FFFC00007FFFFFF000000000000000000000000007FFFFFF00001FFFF0003FFE001FFC00FFC01FF00FF00FE01FC07F03F01F81F83F03E0FC1F07C1F0F83C1E0F078787C3C3C387878F0E1E3C78F1C38F1C38E3C71C71C71C71C638E31C638C739C639CE739CE7398C67398CE673399CCE66333399998CCCCCCCCCCCCCCCCD99999333666CCD993366CD9B364C9B264D936C9B24D924DB249B6D92492492492492496DB6925B692DA4B692D2DA5A5A5B5A5A5A52D2;
defparam ram_block1a37.mem_init0 = 2048'h96B4A52D6B5AD6A5295AD4AD4AD4AD5A952A55AB54AB54AA552AB552AA555AAAD554AAAA55555AAAAAAA9555555555555555555554AAAAAAA555552AAA9555AAAD552AA556AB552AD56A952AD5AB56AD4AD5A94AD4A56B5294A5294B5AD296B4B5A5A5A5A5A5A5B4B696D25B496DA496DB49249249249249249B6D924DB24DB24D9364D9364D9B264C993266CC99B336666CCCCD99999999999999999CCCCE66633399CCE67319CC67398C6318C6318C738C638C718E39C71C71C71C71C71C38E1C78E1C3870E1E3C3C78787878783C3C1E0F0783E0F83E0F81F07E07E07E07E03F80FE03FC03FC01FF007FE003FF8003FFF0000FFFFE0000003FFFFFFFFFFFF;

cycloneive_ram_block ram_block1a1(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a1_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a1.clk0_core_clock_enable = "ena0";
defparam ram_block1a1.clk0_input_clock_enable = "ena0";
defparam ram_block1a1.clk0_output_clock_enable = "ena0";
defparam ram_block1a1.data_interleave_offset_in_bits = 1;
defparam ram_block1a1.data_interleave_width_in_bits = 1;
defparam ram_block1a1.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a1.init_file_layout = "port_a";
defparam ram_block1a1.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a1.operation_mode = "rom";
defparam ram_block1a1.port_a_address_clear = "none";
defparam ram_block1a1.port_a_address_width = 13;
defparam ram_block1a1.port_a_data_out_clear = "none";
defparam ram_block1a1.port_a_data_out_clock = "clock0";
defparam ram_block1a1.port_a_data_width = 1;
defparam ram_block1a1.port_a_first_address = 0;
defparam ram_block1a1.port_a_first_bit_number = 1;
defparam ram_block1a1.port_a_last_address = 8191;
defparam ram_block1a1.port_a_logical_ram_depth = 65536;
defparam ram_block1a1.port_a_logical_ram_width = 18;
defparam ram_block1a1.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a1.ram_block_type = "auto";
defparam ram_block1a1.mem_init3 = 2048'h73399CCC66633339999CCCCCCC6666666666666664CCCCCC9999B3336664CC99B3266CC993366CD93264D9326C9B26C9B26C9B64DB24DB24DB249B6D924936DB6DB6492492DB6DB6DA4925B6D24B6D25B692DA4B696D2DA5B4B4B49696969696B4B4B5A5AD296B4A5AD694A5294A5294A56B5A94AD4A56A56A56A54AD5A952A54A952AD5AA55AA55AAD56AB55AA955AA9552AAD552AA9554AAA95556AAAA555552AAAAA955555555AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD55555555AAAAAAD55556AAAA5555AAAB5552AA9554AA9552AA556AA556AB55AAD56A956A956A952A55AB56AD4A95AB52A56A56A56A52B5295AD4A5295AD6B5AD6B5A5294B5AD296B;
defparam ram_block1a1.mem_init2 = 2048'h4A5A52D2969696B4B4B4B4B4969696D2D25A4B49692DA4B692DA496DA496DA492DB6D24924B6DB6DB6DB6DB6DB6DB24924DB6D9249B6C936D926D926D936C9B64D93649B26CD9364D9B26CD93264D9B366CC993266CC99B3266CCD9993326664CCC99999333333666666666666666666666666666733333319999CCCC6667333998CCE6633198CC663399CC67319CC67398CE7318C6739CE7318C631CE739CE318C738C639C639C638C738E71C638E71C638E38C71C71C71C71C71C71C71C71E38E3871C78E3C71E3871E3871E3C78E1C3870F1E3C7870F1E1C3C387878F0F0F0F0E1E1E0F0F0F0F0F8787C3C3E1F0F0783C1E0F07C3E0F07C1F0783E0F83E0F;
defparam ram_block1a1.mem_init1 = 2048'hC1F07C0F83F07E0FC1F83F03F07E07E07E07F03F03F81FC0FE03F01FC07F01FC03F807F00FE01FE01FE00FF007F803FE00FFC01FF803FF001FF800FFE003FFC003FFC003FFE0007FFE0003FFF80007FFFC0000FFFFE00000FFFFFE000000FFFFFFFC000000007FFFFFFFFFFFE0000000000000000000000000000000000000000003FFFFFFFFFFFF8000000003FFFFFFF8000001FFFFFE000007FFFF800007FFFE0000FFFF0000FFFE0003FFF0003FFE0007FF8003FFC003FF8007FF001FFC00FFE007FE007FE007FC00FF803FE00FF803FE00FF007F807F803FC03FC07F807F80FF01FE03FC07F01FE03F80FE03F81FC07F03F80FC07E03F01F81FC0FC07E07;
defparam ram_block1a1.mem_init0 = 2048'hE07E07E03F07E07E07E07E0FC0FC1F81F03F07E0FC1F83F07E0F81F07E0F81F07C0F83E0FC1F07C1F07C1F07C1F07C1F07C1F0F83E0F87C1F0783E1F07C3E0F07C3E1F0783C1E0F87C3E1F0F87C3C1E0F0787C3E1E0F0F8783C3E1E0F0F078783C3C3E1E1F0F0F0F87878783C3C3C3C3C1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E3C3C3C3C3C3878787870F0F0F1E1E1E3C3C3C787870F0F1E1E3C3C787870F0E1E3C3C7878F0E1E1C3C7878F0E1E3C3878F0E1E3C3878F0E1E3C7870F1E3C3878F1E1C3C78F0E1C3C78F0E1C3878F1E1C3870F1E3C7870E1C3C78F1E3C3870E1C3C78F1E3C7870E1C3870E1E3C78F1E3C7870E1C3870E1C3C78F1E3C78;

cycloneive_ram_block ram_block1a55(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a55_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a55.clk0_core_clock_enable = "ena0";
defparam ram_block1a55.clk0_input_clock_enable = "ena0";
defparam ram_block1a55.clk0_output_clock_enable = "ena0";
defparam ram_block1a55.data_interleave_offset_in_bits = 1;
defparam ram_block1a55.data_interleave_width_in_bits = 1;
defparam ram_block1a55.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a55.init_file_layout = "port_a";
defparam ram_block1a55.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a55.operation_mode = "rom";
defparam ram_block1a55.port_a_address_clear = "none";
defparam ram_block1a55.port_a_address_width = 13;
defparam ram_block1a55.port_a_data_out_clear = "none";
defparam ram_block1a55.port_a_data_out_clock = "clock0";
defparam ram_block1a55.port_a_data_width = 1;
defparam ram_block1a55.port_a_first_address = 24576;
defparam ram_block1a55.port_a_first_bit_number = 1;
defparam ram_block1a55.port_a_last_address = 32767;
defparam ram_block1a55.port_a_logical_ram_depth = 65536;
defparam ram_block1a55.port_a_logical_ram_width = 18;
defparam ram_block1a55.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a55.ram_block_type = "auto";
defparam ram_block1a55.mem_init3 = 2048'h3C78F1E3C7870E1C3870E1C3C78F1E3C78F0E1C3870E1C3C78F1E3C7870E1C3878F1E3C7870E1C3C78F1E1C3870F1E3C3870E1E3C7870E1E3C7870F1E3C3878F1E1C3C78F0E1E3C3878F0E1E3C3878F0E1E3C3C7870F0E1E3C3C7878F0E1E1C3C3C7878F0F1E1E1C3C3C787878F0F0F1E1E1E1C3C3C3C387878787878F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F078787878783C3C3C3E1E1E1F0F0F878783C3C1E1E0F0F8783C3E1E0F0F87C3C1E0F0787C3E1F0F87C3E0F0783C1F0F87C1E0F87C1F0F83C1F07C3E0F83E1F07C1F07C1F07C1F07C1F07C1F07E0F83E07C1F03E0FC1F03E0FC1F83F07E0FC1F81F03F07E07E0FC0FC0FC0FC1F80FC0FC0F;
defparam ram_block1a55.mem_init2 = 2048'hC0FC07E07F03F01F80FC07E03F81FC07F03F80FE03F80FF01FC07F80FF01FE03FC03FC07F807F803FC03FC01FE00FF803FE00FF803FE007FC00FFC00FFC00FFE007FF001FFC003FF8007FF8003FFC000FFF8001FFF8000FFFE0001FFFE0000FFFFC00003FFFFC00000FFFFFF0000003FFFFFFF8000000003FFFFFFFFFFFF8000000000000000000000000000000000000000000FFFFFFFFFFFFC000000007FFFFFFE000000FFFFFE00000FFFFE00007FFFC0003FFF8000FFFC000FFF8007FF8007FF800FFE003FF001FF803FF007FE00FF803FC01FE00FF00FF00FE01FC03F807F01FC07F01F80FE07F03F81F81FC0FC0FC0FC1F81F83F07E0FC1F83E07C1F07;
defparam ram_block1a55.mem_init1 = 2048'hE0F83E0F83C1F07C1E0F87C1E0F0783C1E1F0F8787C3C3E1E1E1E1E0F0F0E1E1E1E1E3C3C387870F1E1C3C78F1E1C3870E3C78F1C38F1C38F1C78E3C71C38E38F1C71C71C71C71C71C71C71C638E38C71CE38C71CE39C638C738C738C639C6318E739CE718C6319CE739CC6319CE6339CC67319CC673398CC6633198CCE66333999CCCC666733331999999CCCCCCCCCCCCCCCCCCCCCCCCCCCD999999333326664CCC999333666CC99B3266CC993266CD9B364C99366C9B364D9366C9B24D9364DB26D936C936C936D926DB24936DB649249B6DB6DB6DB6DB6DB6DA492496DB6924B6D24B6D24B692DA4B692D25A4B49696D2D2D25A5A5A5A5AD2D2D29694B4A5;
defparam ram_block1a55.mem_init0 = 2048'hAD296B5A5294B5AD6B5AD6B5294A56B5295A94AD4AD4AD4A95AB52A56AD5AB54A952AD52AD52AD56AB55AAD54AAD54AA9552AA5552AA9555AAAB5554AAAAD55556AAAAAB555555556AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB555555552AAAAA955554AAAAD5552AAA5552AA9556AA9552AB552AB55AAD56AB54AB54AB56A952A54A952B56A54AD4AD4AD4A56A52B5AD4A5294A5294A52D6B4A5AD296B4B5A5A5AD2D2D2D2D25A5A5B4B696D2DA4B692DB496DA496DB4924B6DB6DB6924924DB6DB6D924936DB249B649B649B64DB26C9B26C9B26C99364C99366CD993266CC99B32664CCD999B33326666664CCCCCCCCCCCCCCC666666733339998CCC6673399C;

cycloneive_ram_block ram_block1a110(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a110_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a110.clk0_core_clock_enable = "ena0";
defparam ram_block1a110.clk0_input_clock_enable = "ena0";
defparam ram_block1a110.clk0_output_clock_enable = "ena0";
defparam ram_block1a110.data_interleave_offset_in_bits = 1;
defparam ram_block1a110.data_interleave_width_in_bits = 1;
defparam ram_block1a110.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a110.init_file_layout = "port_a";
defparam ram_block1a110.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a110.operation_mode = "rom";
defparam ram_block1a110.port_a_address_clear = "none";
defparam ram_block1a110.port_a_address_width = 13;
defparam ram_block1a110.port_a_data_out_clear = "none";
defparam ram_block1a110.port_a_data_out_clock = "clock0";
defparam ram_block1a110.port_a_data_width = 1;
defparam ram_block1a110.port_a_first_address = 49152;
defparam ram_block1a110.port_a_first_bit_number = 2;
defparam ram_block1a110.port_a_last_address = 57343;
defparam ram_block1a110.port_a_logical_ram_depth = 65536;
defparam ram_block1a110.port_a_logical_ram_width = 18;
defparam ram_block1a110.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a110.ram_block_type = "auto";
defparam ram_block1a110.mem_init3 = 2048'hE0F07C3E1F07C3E0F83E1F07C1F07C0F83E0FC1F03E0FC1F81F03F07E07E07E07E07E07E03F03F81FC07E03F80FE03F807F01FE03FC03FC03FC03FE01FF007FC01FF003FF003FF003FF800FFE001FFE001FFF0007FFE0003FFF80007FFFE00007FFFFC00000FFFFFFC00000007FFFFFFFFFE00000000000000000000000000000000000000000FFFFFFFFFF80000000FFFFFFC00000FFFFF00003FFFF0000FFFE0007FFE000FFF0007FF000FFE003FF003FF003FE007FC01FE00FF807F807F807F80FF01FC03F80FE03F01FC0FE07F03F03F03F83F03F03F07E0FC0F83F07C0F83E07C1F07C1F0F83E0F87C1E0F07C3E1F0F0783C3E1E1F0F0F0F07878787878;
defparam ram_block1a110.mem_init2 = 2048'h70F0F0F1E1E1C3C7878F1E1C3878F1E3C78E1C3871E3871E3871C38E3C71C38E38E1C71C71C71C71C71C71C738E38E71C738E31C738E71CE31C639C639CE31CE718C639CE739CE739CE739CC6319CE6319CE63398CE63398CC673399CCE6733998CCE663331999CCCC66667333331999999998CCCCCCCCCCCCCCCC999999999B3333366666CCCD999B336664CC99B33664CD993266CD9B3264C9B366CD9326CD9364C9B26C9B26C9B26D93649B24D926D926D926DB249B6C924DB6C9249B6DB6DB6492492492492DB6DB6DA4924B6DB4925B6925B6925B492DA4B692D25B49692D2DA5B4B4B6969696969696969696B4B4B5A5AD2D694B5A52D6B4A5296B5AD6;
defparam ram_block1a110.mem_init1 = 2048'hB5AD6B5AD6A5295AD4A56B52B5A95A95A95A95AB52A56AD4A952A54A952AD5AA55AB54AA55AA552A954AA556AB552AA556AAD552AA5552AA9554AAAD5552AAAD5554AAAA955555AAAAAA955555554AAAAAAAAAAAAAA55555555555555552AAAAAAAAAAAAA955555554AAAAAAD55554AAAA95555AAAA5554AAAD554AAA555AAA554AA955AAB55AA955AAD56A954AB55AA55AB54AB56AD5AA56AD5AB52A56AD4AD4A95A94AD4AD4A56B5295AD6B5294A5294A5294B5AD694A5AD296B4A5A52D2969694B4B4B4B4B4B4B4B4969692D2DA5B4B692DA5B496DA4B6925B6925B692496DB6924925B6DB6DB6DB6DB6DB6DB249249B6DB24936D924DB24DB64DB24DB26D;
defparam ram_block1a110.mem_init0 = 2048'h9364DB26C9B26C9B364D9326CD93264D9B366CD993266CC9933266CCD99B332666CCCD9999B33333666666666666CCCCCCE6666666666633333399998CCCE667333998CCE673399CCE67319CCE6339CC67398CE7398C6318C6318C6318C639CE318E718E718E71CE31C638E71C738E38C71C71C71C71C71C71C71E38E3C71C38E1C70E3C70E1C78F1E3C78F1E1C3C7870F1E1E1C3C3C3C3C3C3C3C3C3E1E1F0F0787C3E1F0F87C1E0F83E1F07C1F07E0F83E07C0F81F03F07E07E07E07E03F03F80FC07F01FC07F80FE01FE01FE01FF007F803FF007FE007FF001FFC003FFC001FFF0003FFFC0003FFFF00000FFFFFE0000000FFFFFFFFFE0000000000000000;

cycloneive_ram_block ram_block1a92(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a92_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a92.clk0_core_clock_enable = "ena0";
defparam ram_block1a92.clk0_input_clock_enable = "ena0";
defparam ram_block1a92.clk0_output_clock_enable = "ena0";
defparam ram_block1a92.data_interleave_offset_in_bits = 1;
defparam ram_block1a92.data_interleave_width_in_bits = 1;
defparam ram_block1a92.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a92.init_file_layout = "port_a";
defparam ram_block1a92.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a92.operation_mode = "rom";
defparam ram_block1a92.port_a_address_clear = "none";
defparam ram_block1a92.port_a_address_width = 13;
defparam ram_block1a92.port_a_data_out_clear = "none";
defparam ram_block1a92.port_a_data_out_clock = "clock0";
defparam ram_block1a92.port_a_data_width = 1;
defparam ram_block1a92.port_a_first_address = 40960;
defparam ram_block1a92.port_a_first_bit_number = 2;
defparam ram_block1a92.port_a_last_address = 49151;
defparam ram_block1a92.port_a_logical_ram_depth = 65536;
defparam ram_block1a92.port_a_logical_ram_width = 18;
defparam ram_block1a92.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a92.ram_block_type = "auto";
defparam ram_block1a92.mem_init3 = 2048'h0000000000000000FFFFFFFFFE0000000FFFFFE00001FFFF80007FFF8001FFF0007FF8007FF001FFC00FFC01FF803FC01FF00FF00FF00FE03FC07F01FC07E03F81F80FC0FC0FC0FC1F81F03E07C0F83E0FC1F07C1F0F83E0F07C3E1F0F87C3C1E1F0F0F8787878787878787870F0F1E1C3C7870F1E3C78F1E3C70E1C78E1C70E3871C78E38F1C71C71C71C71C71C71C638E39C71CE38C718E71CE31CE31CE318E738C6318C6318C6318C6339CE6339CC67398CE67319CCE673399CCE66333999CCCE66633333999998CCCCCCCCCCCE666666CCCCCCCCCCCD99999B33336666CCC999B33666CC9993266CC993366CD9B364C99366C99364D9B26C9B26C9B64D93;
defparam ram_block1a92.mem_init2 = 2048'h6C9B649B64DB649B64936D9249B6DB249249B6DB6DB6DB6DB6DB6DB492492DB6D2492DB492DB492DA4B6D25B4B692DA5B4B69692D2D25A5A5A5A5A5A5A5A52D2D29694B4A5AD296B4A52D6B5A5294A5294A5295AD6B5295AD4A56A56A52B52A56A56AD4A95AB56AD4AB56AD5AA55AB54AB55AA552AD56AB552AB55AAB552AA554AAB554AAA5556AAA5554AAAB55552AAAA555556AAAAAA555555552AAAAAAAAAAAAA95555555555555554AAAAAAAAAAAAAA555555552AAAAAB555552AAAA55556AAA95556AAA5552AA9554AA9556AAD54AA955AAD54AA552A954AB54AA55AB54AB56A952A54A952A56AD4A95AB52B52B52B52B5A95AD4A56B5294AD6B5AD6B5A;
defparam ram_block1a92.mem_init1 = 2048'hD6B5AD294A5AD694B5A52D696B4B5A5A5AD2D2D2D2D2D2D2D2D2DA5A5B4B69692D25B49692DA4B6925B492DB492DB4925B6DA4924B6DB6DB6924924924924DB6DB6DB24926DB64926DB249B6C936C936C93649B24D936C9B26C9B26C9B264D9366C99366CD9B264C99B366CC9933664CD99B32664CCD99B3336666CCCCD99999B3333333326666666666666666333333333199999CCCCC66673331998CCE6633399CCE673399CC663398CE63398CE7318CE7318C6739CE739CE739CE738C631CE718E738C738C718E71CE39C718E39C71CE38E39C71C71C71C71C71C71C70E38E3871C78E3871C38F1C38F1C3870E3C78F1E3C3870F1E3C3C7870F0F1E1E1E1C;
defparam ram_block1a92.mem_init0 = 2048'h3C3C3C3C3C1E1E1E1F0F0F8783C1E1F0F87C1E0F07C3E0F83E1F07C1F07C0F83E07C1F83E07E0FC1F81F81F83F81F81F81FC0FE07F01F80FE03F807F01FE03FC03FC03FC03FE00FF007FC00FF801FF801FF800FFE001FFC001FFE000FFFC000FFFE0001FFFF80001FFFFE000007FFFFFE00000003FFFFFFFFFE00000000000000000000000000000000000000000FFFFFFFFFFC00000007FFFFFE000007FFFFC0000FFFFC0003FFF8000FFFC001FFF000FFF000FFE003FF801FF801FF801FF007FC01FF00FF807F807F807F80FF01FC03F80FE03F80FC07F03F81F80FC0FC0FC0FC0FC0FC1F81F03F07E0F81F07E0F83E07C1F07C1F0F83E0F87C1F0F87C1E0F;

cycloneive_ram_block ram_block1a74(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a74_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a74.clk0_core_clock_enable = "ena0";
defparam ram_block1a74.clk0_input_clock_enable = "ena0";
defparam ram_block1a74.clk0_output_clock_enable = "ena0";
defparam ram_block1a74.data_interleave_offset_in_bits = 1;
defparam ram_block1a74.data_interleave_width_in_bits = 1;
defparam ram_block1a74.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a74.init_file_layout = "port_a";
defparam ram_block1a74.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a74.operation_mode = "rom";
defparam ram_block1a74.port_a_address_clear = "none";
defparam ram_block1a74.port_a_address_width = 13;
defparam ram_block1a74.port_a_data_out_clear = "none";
defparam ram_block1a74.port_a_data_out_clock = "clock0";
defparam ram_block1a74.port_a_data_width = 1;
defparam ram_block1a74.port_a_first_address = 32768;
defparam ram_block1a74.port_a_first_bit_number = 2;
defparam ram_block1a74.port_a_last_address = 40959;
defparam ram_block1a74.port_a_logical_ram_depth = 65536;
defparam ram_block1a74.port_a_logical_ram_width = 18;
defparam ram_block1a74.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a74.ram_block_type = "auto";
defparam ram_block1a74.mem_init3 = 2048'h0783C1E1F0F0787C3C3E1E1E1E0F0F0F0F0F0F0F0E1E1E1E3C3C387870F1E1C3C78F0E1C3870E1C3870E1C78F1C38F1C38F1C78E3C71C38E3C71C70E38E38E3C71C71C71C71C71CE38E38E31C71C638E31C738E31C738E718E31CE39C639C639CE31CE718C739CE318C6318E739CE7318C6318CE739CC6339CC63398C67319CC67319CC663399CC6633198CC6633399CCC667333998CCC666733319998CCCCE666663333333199999999999CCCCCCCCCCCCCCCCC9999999999993333333666666CCCCD999933336664CCC999333666CCD99B32664CD9933664CD9B3264CD9B366CD9B366CD9B364C99366C99364C9B264D9366C9B26C9B26C9B26C9364D926C9;
defparam ram_block1a74.mem_init2 = 2048'hB64D926D936C93649B649B64936C936D924DB64936DB249B6DB24926DB6D9249249B6DB6DB6DB6C924924924B6DB6DB6DB6DA492492DB6DB4924B6DB4925B6D24B6DA496DA496DA4B6D25B692DA4B692DA4B692DA4B496D2DA5B4B696D2DA5A4B4B69696D2D2D2DA5A5A5A5A5A5A5A5A5A5A5A5A5AD2D2D2969694B4B5A5AD2D696B4A5AD296B4A5AD296B5A52D6B5A5296B5AD694A5294A5294A5294A5294A52B5AD6B5294AD6B5295AD4A56B5295A94AD4A56A56A52B52B52B52B52B56A56A56AD4AD5A952B56A54A95AB56AD4A952A54A952A55AB56AD52A55AB54A956A956A952AD52A956A956AB54AA55AAD56AB54AA556AB55AAD54AA556AA556AA556A;
defparam ram_block1a74.mem_init1 = 2048'hA556AAD55AA9552AA555AAB554AA9556AA9554AAB555AAAD556AAA5556AAA5556AAAD555AAAB5554AAAA5555AAAA95555AAAA955552AAAA955556AAAAA555555AAAAAA5555556AAAAAA955555552AAAAAAAB5555555552AAAAAAAAAAB555555555555556AAAAAAAAAAAAAAAAAAAAAAAAAAAB555555555555555555556AAAAAAAAAAAAAAAAAAAAAAAAAAAAD555555555555556AAAAAAAAAAA95555555554AAAAAAAAB55555555AAAAAAAB5555554AAAAAA9555554AAAAAA555554AAAAAD55554AAAAA55555AAAAA55554AAAA95555AAAA95554AAAA55552AAA95556AAA95552AAAD555AAAB5552AAA5556AAA5556AAA5556AAB555AAA9554AAA555AAAD552AA95;
defparam ram_block1a74.mem_init0 = 2048'h56AA9554AAB556AA9556AA9552AAD55AAB556AA9552AA556AAD55AAB552AA554AAD54AA955AA9552AB552AB552AB552A955AA955AAD54AAD56AA552AB55AAD54AA552A955AAD56AB55AAD56AB55AAD56A954AA552A956AB55AA552AD56A954AB55AA552AD56A956AB54AB54AA55AA55AA552AD52AD52AD52AD52AD52AD52AD52AD52AD5AA55AA55AA54AB54AB56A956AD52AD52A55AB54AB56A952AD52A55AB54A956AD52A55AB56A952AD5AA54A956AD5AA54AB56AD52A54A956AD5AA54A952AD5AB56A952A54AB56AD5AB54A952A54A956AD5AB56AD5AA54A952A54A956AD5AB56AD5AB56AD52A54A952A54A952A54A956AD5AB56AD5AB56AD5AB56AD5AA54;

cycloneive_ram_block ram_block1a128(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a128_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a128.clk0_core_clock_enable = "ena0";
defparam ram_block1a128.clk0_input_clock_enable = "ena0";
defparam ram_block1a128.clk0_output_clock_enable = "ena0";
defparam ram_block1a128.data_interleave_offset_in_bits = 1;
defparam ram_block1a128.data_interleave_width_in_bits = 1;
defparam ram_block1a128.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a128.init_file_layout = "port_a";
defparam ram_block1a128.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a128.operation_mode = "rom";
defparam ram_block1a128.port_a_address_clear = "none";
defparam ram_block1a128.port_a_address_width = 13;
defparam ram_block1a128.port_a_data_out_clear = "none";
defparam ram_block1a128.port_a_data_out_clock = "clock0";
defparam ram_block1a128.port_a_data_width = 1;
defparam ram_block1a128.port_a_first_address = 57344;
defparam ram_block1a128.port_a_first_bit_number = 2;
defparam ram_block1a128.port_a_last_address = 65535;
defparam ram_block1a128.port_a_logical_ram_depth = 65536;
defparam ram_block1a128.port_a_logical_ram_width = 18;
defparam ram_block1a128.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a128.ram_block_type = "auto";
defparam ram_block1a128.mem_init3 = 2048'h54AB56AD5AB56AD5AB56AD5AB56AD52A54A952A54A952A54A956AD5AB56AD5AB56AD52A54A952A54AB56AD5AB56AD52A54A952A55AB56AD5AA54A952AD5AB56A952A54AB56AD52A54A956AD5AA54AB56AD52A54AB56A952AD5AB54A956AD52A55AB54A956A952AD5AA55AB54A956A956AD52AD5AA55AA54AB54AB54AB56A956A956A956A956A956A956A956A956A954AB54AB54AA55AA55AAD52AD56A954AB55AA552AD56A954AB55AAD52A954AA552AD56AB55AAD56AB55AAD56AB552A954AA556AB55AA954AAD56AA556AB552AB552A955AA955AA955AA9552AB552AA556AA554AA955AAB556AAD54AA9552AAD55AAB556AA9552AAD552AAD55AAA5552AAD5;
defparam ram_block1a128.mem_init2 = 2048'h52AA9556AAB554AAA5552AAB555AAAD554AAAD554AAAD554AAA9555AAAB5556AAA95552AAAD5552AAA95554AAAA55552AAAB55552AAAA55554AAAAB55554AAAAA555556AAAAA555554AAAAAA5555552AAAAAA5555555AAAAAAAB55555555AAAAAAAAA55555555552AAAAAAAAAAAD555555555555556AAAAAAAAAAAAAAAAAAAAAAAAAAAAD55555555555555555555AAAAAAAAAAAAAAAAAAAAAAAAAAAAD55555555555555AAAAAAAAAAA9555555555AAAAAAAA955555552AAAAAAD555554AAAAAB555554AAAAAD55552AAAA955552AAAB55552AAAB5554AAAA5555AAAB5556AAAD554AAAD554AAAD556AAB555AAA5552AAD552AA555AAB554AA9552AB556AAD54A;
defparam ram_block1a128.mem_init1 = 2048'hAD54AAD54AAD54AA556AB55AAD54AA55AAD56AB54AA55AAD52AD52A956A952AD52AD52A55AB54A956AD5AB54A952A54A952A56AD5AB52A54AD5A952B56A56AD4AD4AD5A95A95A95A95A94AD4AD4A56A52B5295AD4A56B5295AD6A5295AD6B5A94A5294A5294A5294A5294A52D6B5AD294B5AD694B5AD296B4A5AD296B4A5AD2D696B4B5A5A52D2D2969696B4B4B4B4B4B4B4B4B4B4B4B4B4B6969696D2D2DA5A4B4B696D2DA5B4B696D25A4B692DA4B692DA4B692DB496DA4B6D24B6D24B6DA496DB4925B6DA4925B6DB6924924B6DB6DB6DB6DA4924924926DB6DB6DB6DB24924936DB6C9249B6DB249B6D924DB64936D926D924DB24DB24D926D936C9364DB;
defparam ram_block1a128.mem_init0 = 2048'h26C9364D926C9B26C9B26C9B26CD9364C9B264D9326CD93264D9B366CD9B366CD9B3664C99B3664CD9933664CC99B33666CCD9993326664CCD9999333366666CCCCCD9999999333333333332666666666666666673333333333319999998CCCCCE666633331999CCCC666333999CCC66733998CC6633198CC673398CC67319CC67319CC63398C67398C6739CE6318C6319CE739CE318C6318E739C631CE718E738C738C738E718E31CE39C718E39C718E38C71C718E38E38E71C71C71C71C71C78E38E38E1C71C78E3871C78E3C71E3871E3871E3C70E1C3870E1C3870E1E3C7870F1E1C3C387878F0F0F0E1E1E1E1E1E1E1E0F0F0F0F8787C3C1E1F0F0783C1;

cycloneive_ram_block ram_block1a20(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a20_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a20.clk0_core_clock_enable = "ena0";
defparam ram_block1a20.clk0_input_clock_enable = "ena0";
defparam ram_block1a20.clk0_output_clock_enable = "ena0";
defparam ram_block1a20.data_interleave_offset_in_bits = 1;
defparam ram_block1a20.data_interleave_width_in_bits = 1;
defparam ram_block1a20.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a20.init_file_layout = "port_a";
defparam ram_block1a20.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a20.operation_mode = "rom";
defparam ram_block1a20.port_a_address_clear = "none";
defparam ram_block1a20.port_a_address_width = 13;
defparam ram_block1a20.port_a_data_out_clear = "none";
defparam ram_block1a20.port_a_data_out_clock = "clock0";
defparam ram_block1a20.port_a_data_width = 1;
defparam ram_block1a20.port_a_first_address = 8192;
defparam ram_block1a20.port_a_first_bit_number = 2;
defparam ram_block1a20.port_a_last_address = 16383;
defparam ram_block1a20.port_a_logical_ram_depth = 65536;
defparam ram_block1a20.port_a_logical_ram_width = 18;
defparam ram_block1a20.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a20.ram_block_type = "auto";
defparam ram_block1a20.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFF000000001FFFFFFC00000FFFFE00007FFF8000FFFC000FFF000FFF001FFC00FFC00FFC01FF007F803FC03FC03F807F00FE03F80FC07F03F81F81F81F81F81F83F03E07C0F83E07C1F07C1F07C3E0F87C1E0F0783C1E0F0F078783C3C3C3C3C3C3C3C387878F0F1E1C3C78F0E1C3870E3C78E1C78E1C78E3871C78E38E3C71C71C71C71C71C638E38C71C638E71CE39C738C738C738C639CE318C639CE739CE6318C6739CC67398CE63398CE633198CE6633198CCE667333999CCCCE666673333333999999999999999999999B333333266666CCCC999B332666CCD9933266CC993366CD993264D9B364C9B364D9B26C9B26C9B26C9364;
defparam ram_block1a20.mem_init2 = 2048'hDB26D926D926D926DB24DB6C924DB6D9249249B6DB6DB6DB6DB6DA492492DB6DA492DB4925B692DB496D25B496D25A4B49692D25A5A5B4B4B4B4B4B4B4B4B5A5A52D2D694B5A52D694B5AD694A5294A5294A5295AD6B5295AD4AD6A56A56A56A56AD4AD5AB52A54A952A54AB56A952AD52A956A954AA552AB55AA955AAB552AAD55AAA5552AA9555AAAB5554AAAA55554AAAAA5555552AAAAAAA555555555556AAAAAAAAAAAAAAAAAAAAAAAAAB555555555552AAAAAAA5555552AAAA955552AAA95556AAAD554AAAD552AA9552AAD54AA955AA955AAD56AB55AAD52AD56A952AD52A54AB56AD5AB52A54AD4A95A95AB52B5A95A95AD4A56B5294AD6B5AD6B5AD;
defparam ram_block1a20.mem_init1 = 2048'h6B5AD6B4A52D6B4A5AD296B4B5A5AD2D2D6969696969696969692D2D25A5B4B696D2DA4B696D24B692DB496DA496DB4925B6DA4924B6DB6DB6DB6DB6DB6DB6DB6DB24924DB6D9249B6C926D924DB24DB24D926C9364DB26C9B26C9B26C9B264D9326CD93264C993264C993366CC99B3266CCD99333666CCC99993333666664CCCCCC999999999999999999999999999CCCCCCCE6666733339998CCC666333998CC6673399CC663399CC67319CC67398C67398C6339CE739CC639CE739CE318C739C639CE31CE39C639C738E71C638E71C718E38E38C71C71C71C71C71C38E38E3C71C78E3871C38F1C78F1C38F1E3C70E1C3C78F1E1C3C7870F1E1E1C3C3C3C7;
defparam ram_block1a20.mem_init0 = 2048'h8787878787C3C3C3C1E1E0F0F8783C1E0F0783E1F0783E0F07C1F07C1F07C1F83E0FC1F83F07E0FC0F81F81F81F81F80FC0FE07F03F80FE03F80FE03F807F80FF00FF00FF007F803FE00FF801FF003FF001FF800FFF001FFE000FFF8001FFF8000FFFE0000FFFFC00003FFFFE000000FFFFFFF8000000001FFFFFFFFFFFFFFFFFFC0000000007FFFFFFFFFFFFFFFFFF8000000001FFFFFFF0000007FFFFE00003FFFF80003FFFC0007FFE0007FFC001FFE001FFE003FF801FFC00FFC01FF803FE00FF803FC01FE01FE01FE03FC03F80FF01FC07F01F80FE07F03F81F81FC0FC0FC0FC0F81F81F03E07C0F81F07E0F83F07C1F07C1F07C1E0F83E1F0783E1F0F8;

cycloneive_ram_block ram_block1a38(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a38_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a38.clk0_core_clock_enable = "ena0";
defparam ram_block1a38.clk0_input_clock_enable = "ena0";
defparam ram_block1a38.clk0_output_clock_enable = "ena0";
defparam ram_block1a38.data_interleave_offset_in_bits = 1;
defparam ram_block1a38.data_interleave_width_in_bits = 1;
defparam ram_block1a38.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a38.init_file_layout = "port_a";
defparam ram_block1a38.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a38.operation_mode = "rom";
defparam ram_block1a38.port_a_address_clear = "none";
defparam ram_block1a38.port_a_address_width = 13;
defparam ram_block1a38.port_a_data_out_clear = "none";
defparam ram_block1a38.port_a_data_out_clock = "clock0";
defparam ram_block1a38.port_a_data_width = 1;
defparam ram_block1a38.port_a_first_address = 16384;
defparam ram_block1a38.port_a_first_bit_number = 2;
defparam ram_block1a38.port_a_last_address = 24575;
defparam ram_block1a38.port_a_logical_ram_depth = 65536;
defparam ram_block1a38.port_a_logical_ram_width = 18;
defparam ram_block1a38.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a38.ram_block_type = "auto";
defparam ram_block1a38.mem_init3 = 2048'h3E1F0F83C1F0F83E0F07C1F07C1F07C1F83E0FC1F03E07C0F81F03F03E07E07E07E07F03F03F81FC0FE03F01FC07F01FE03F807F80FF00FF00FF007F803FE00FF803FF007FE007FF003FF800FFF000FFF0007FFC000FFFC0007FFF80003FFFF80000FFFFFC000001FFFFFFF0000000003FFFFFFFFFFFFFFFFFFC0000000007FFFFFFFFFFFFFFFFFF0000000003FFFFFFE000000FFFFF800007FFFE0000FFFE0003FFF0003FFE000FFF001FFE003FF001FF801FF003FE00FF803FC01FE01FE01FE03FC03F80FE03F80FE03F81FC0FE07E03F03F03F03F03E07E0FC1F83F07E0F83F07C1F07C1F07C1E0F83C1F0F83C1E0F0783C3E1E0F0F07878787C3C3C3C3C3;
defparam ram_block1a38.mem_init2 = 2048'hC78787870F0F1E1C3C7870F1E3C7870E1C78F1E3871E3C71E3871C38E3C71C78E38E3871C71C71C71C71C638E38E31C71CE38C71CE39C738C738E718E738C739C6318E739CE738C6739CE7398C6339CC6339CC67319CC673398CC673399CCC66333998CCC66633339999CCCCCE66666673333333333333333333333333326666664CCCCD99993332666CCD99933666CC99B3266CD993264C993264C99366C99364C9B26C9B26C9B26C9B64D926C93649B649B64936C926DB24936DB649249B6DB6DB6DB6DB6DB6DB6DB6DA4924B6DB4925B6D24B6D25B692DA496D2DA4B696D2DA5B4B4969692D2D2D2D2D2D2D2D2D69696B4B5A5AD296B4A5AD694A5AD6B5AD;
defparam ram_block1a38.mem_init1 = 2048'h6B5AD6B5AD6A5295AD4A56B52B52B5A95AB52B52A56A54A95AB56AD5AA54A956A952AD56A956AB55AAD56AB552AB552AA556AA9552AA9556AAA5556AAAD5552AAA955552AAAA9555554AAAAAAA955555555555AAAAAAAAAAAAAAAAAAAAAAAAAAD55555555554AAAAAAA9555554AAAAA55554AAAA5555AAAB5552AA9554AAB556AA955AAB552AB55AA954AA552AD52A956A952AD5AA54A952A54A95AB56A56AD4AD4AD4AD4AD6A56B5295AD6B5294A5294A5294A52D6B5A52D694B5A52D69694B4B5A5A5A5A5A5A5A5A5B4B4B49692D25A4B496D25B496D25B692DB4925B6924B6DB6924924B6DB6DB6DB6DB6DB24924936DB64926DB649B6C936C936C936C9B6;
defparam ram_block1a38.mem_init0 = 2048'h4D926C9B26C9B26C9B364D9B264D9B364C993366CD993266CC99933666CCC999B3326666CCCCC9999999B3333333333333333333339999999CCCCCE6667333999CCCE6633198CCE633198CE63398CE6339CC6739CC6318CE739CE738C6318E738C639C639C639C738E71CE38C71C638E38C71C71C71C71C71C78E38E3C71C38E3C70E3C70E3C78E1C3870E1E3C7870F1E1E3C3C3878787878787878783C3C1E1E0F0783C1E0F07C3E0F87C1F07C1F07C0F83E07C0F81F83F03F03F03F03F03F81FC07E03F80FE01FC03F807F807F803FC01FF007FE007FE007FF001FFE001FFE0007FFE0003FFFC0000FFFFE000007FFFFFF000000001FFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a2(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a2_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a2.clk0_core_clock_enable = "ena0";
defparam ram_block1a2.clk0_input_clock_enable = "ena0";
defparam ram_block1a2.clk0_output_clock_enable = "ena0";
defparam ram_block1a2.data_interleave_offset_in_bits = 1;
defparam ram_block1a2.data_interleave_width_in_bits = 1;
defparam ram_block1a2.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a2.init_file_layout = "port_a";
defparam ram_block1a2.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a2.operation_mode = "rom";
defparam ram_block1a2.port_a_address_clear = "none";
defparam ram_block1a2.port_a_address_width = 13;
defparam ram_block1a2.port_a_data_out_clear = "none";
defparam ram_block1a2.port_a_data_out_clock = "clock0";
defparam ram_block1a2.port_a_data_width = 1;
defparam ram_block1a2.port_a_first_address = 0;
defparam ram_block1a2.port_a_first_bit_number = 2;
defparam ram_block1a2.port_a_last_address = 8191;
defparam ram_block1a2.port_a_logical_ram_depth = 65536;
defparam ram_block1a2.port_a_logical_ram_width = 18;
defparam ram_block1a2.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a2.ram_block_type = "auto";
defparam ram_block1a2.mem_init3 = 2048'h7C3E1F0F8783C3C1E1E0F0F0F07878787878787878F0F0F0E1E1C3C387870F1E3C3870F1E3C78F1E3C78E1C38F1C38F1C38F1C78E3C71C38E3C71C71E38E38E38E3871C71CE38E38E38E39C71C738E39C71CE38C718E31C638C738E718E718E738C739C631CE738C6318E739CE739CE7398C6318CE7398C67398C67319CE63398CE63319CC663399CCE673399CCE6633199CCCE663331998CCCE6667333399999CCCCCCE666666663333333333333333333333333333333666666666CCCCCC99999B33336666CCCD999B332666CCD99B33664CC99B3266CC99B3264CD9B3264C993264C99326CD9B364C9B364C9B264D9366C9B26C9B26C9B26C9B26D9364DB2;
defparam ram_block1a2.mem_init2 = 2048'h6C93649B24DB24D926D926D924DB249B64936D924DB6C924DB6C924936DB6C9249249B6DB6DB6DB6DB6DB6DB6DB6DB6DB692492492DB6DA4924B6DB4925B6D2496DA492DB496DA496D24B6925B496D25B496D25B4B692D25B4B696D2DA5B4B49696D2D2DA5A5A5B4B4B4B4B4B4B4B4B4B4B4B4B4B5A5A5A5AD2D29696B4B5A5AD2D694B4A5AD296B4A52D694A5AD694A52D6B5A5294A5294A5AD6B5A94A5294A5295AD6B5294AD6B5295AD4A56B52B5A94AD4AD6A56A56A56A56A56A56A56A54AD4AD5A952B56A54AD5AB52A54A952B56AD5AA54A952A55AB56A952AD52A55AA55AB54AB55AA55AA552AD56A954AA55AAD56AB55AA954AA556AA552AB552AB55;
defparam ram_block1a2.mem_init1 = 2048'h6AA556AAD55AAB556AAD55AAA554AAB554AAA555AAAD556AAB555AAA9555AAA95552AAA5554AAAB5554AAAA55552AAAB55556AAAAD5555AAAAAD55554AAAAA9555556AAAAAB5555554AAAAAAAD55555556AAAAAAAAB55555555554AAAAAAAAAAAAA9555555555555555555554AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA95555555555555555555556AAAAAAAAAAAAAB555555555552AAAAAAAAAD55555555AAAAAAAA55555556AAAAAA95555552AAAAA9555556AAAAAD55554AAAAA555552AAAAD55552AAAA55556AAAA55556AAAA55552AAAD5556AAA95552AAAD555AAAB5556AAAD554AAA9555AAA9554AAAD556AAA5552AA9554AAB555AAAD552;
defparam ram_block1a2.mem_init0 = 2048'hAAD552AA9552AAD552AAD55AAA554AAB556AAD55AAB556AAD55AAB552AA554AAD55AA955AAB552AB552AB552AB552AB552AB55AA955AAD54AAD56AB552A955AAD56AB552A954AA552A954AA552A954AA552AD56AB55AA552A956AB55AA552AD56A956AB54AA55AA552AD52A956A956A954AB54AB54AB54AB54AB54AB54AB54AB54AB54A956A956A956AD52AD52A55AA54AB54A956A952AD52A55AB54A956AD52AD5AA54A956AD52A55AB54A952AD5AA54A956AD5AA54A956AD5AA54A952AD5AB56A952A54AB56AD5AA54A952A55AB56AD5AB54A952A54A952AD5AB56AD5AB56A952A54A952A54A952AD5AB56AD5AB56AD5AB56AD52A54A952A54A952A54A952A;

cycloneive_ram_block ram_block1a56(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a56_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a56.clk0_core_clock_enable = "ena0";
defparam ram_block1a56.clk0_input_clock_enable = "ena0";
defparam ram_block1a56.clk0_output_clock_enable = "ena0";
defparam ram_block1a56.data_interleave_offset_in_bits = 1;
defparam ram_block1a56.data_interleave_width_in_bits = 1;
defparam ram_block1a56.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a56.init_file_layout = "port_a";
defparam ram_block1a56.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a56.operation_mode = "rom";
defparam ram_block1a56.port_a_address_clear = "none";
defparam ram_block1a56.port_a_address_width = 13;
defparam ram_block1a56.port_a_data_out_clear = "none";
defparam ram_block1a56.port_a_data_out_clock = "clock0";
defparam ram_block1a56.port_a_data_width = 1;
defparam ram_block1a56.port_a_first_address = 24576;
defparam ram_block1a56.port_a_first_bit_number = 2;
defparam ram_block1a56.port_a_last_address = 32767;
defparam ram_block1a56.port_a_logical_ram_depth = 65536;
defparam ram_block1a56.port_a_logical_ram_width = 18;
defparam ram_block1a56.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a56.ram_block_type = "auto";
defparam ram_block1a56.mem_init3 = 2048'hA952A54A952A54A952A54A956AD5AB56AD5AB56AD5AB56A952A54A952A54A952AD5AB56AD5AB56A952A54A952A55AB56AD5AB54A952A54AB56AD5AA54A952AD5AB56A952A54AB56AD52A54AB56AD52A54AB56A952A55AB54A956AD52A54AB56A956AD52A55AB54A956A952AD52A55AA54AB54A956A956AD52AD52AD52A55AA55AA55AA55AA55AA55AA55AA55AA55AA552AD52AD52A956A954AB54AA55AAD52AD56A954AB55AAD52A954AB55AAD56A954AA552A954AA552A954AA552A955AAD56AB552A955AAD56AA556AB552AB55AA955AA955AA955AA955AA955AAB552AB556AA554AA955AAB556AAD55AAB556AAD55AAA554AAB556AA9556AA9552AA9556AA;
defparam ram_block1a56.mem_init2 = 2048'h9556AAB555AAA5552AA9554AAAD556AAA5552AAB5552AAA5556AAAD555AAAB5556AAA95552AAAD5556AAA95554AAAAD5554AAAAD5554AAAA955556AAAA955554AAAAA555556AAAAAD555552AAAAA95555552AAAAAAD5555554AAAAAAAB555555556AAAAAAAAA955555555555AAAAAAAAAAAAAAD5555555555555555555552AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5555555555555555555552AAAAAAAAAAAAA55555555555AAAAAAAAAD55555556AAAAAAA5555555AAAAAAD555552AAAAA555556AAAAB55556AAAAD5555AAAA95554AAAA5555AAAA5554AAA95552AAB5552AAB555AAAD556AAB554AAA555AAA554AAB556AAD55AAB556AAD54AAD;
defparam ram_block1a56.mem_init1 = 2048'h55AA955AA954AAD54AA552AB55AAD56AB54AA552AD56A954AB54AB55AA55AB54AB54A956A952AD5AB54A952A54AB56AD5A952A54A95AB56A54AD5A952B56A56A54AD4AD4AD4AD4AD4AD4AD4AD6A56A52B5A95AD4A56B5295AD6A5295AD6B5294A5294A52B5AD6B4A5294A5294B5AD694A52D6B4A52D694A5AD296B4A5A52D696B4B5A5AD2D29696B4B4B4B5A5A5A5A5A5A5A5A5A5A5A5A5A5B4B4B4B69696D2D25A5B4B696D2DA5B49692DA5B496D25B496D25B492DA496D24B6D25B6924B6D2496DB4925B6DA4924B6DB692492492DB6DB6DB6DB6DB6DB6DB6DB6DB6DB24924926DB6D924926DB64926DB64936D924DB249B64936C936C93649B649B24D926C;
defparam ram_block1a56.mem_init0 = 2048'h9B64D936C9B26C9B26C9B26C9B26CD9364C9B264D9B264D9B366C993264C993264C99B3664C99B3266CC99B32664CD99B33666CCC999B3336666CCCD9999B33332666666CCCCCCCCD9999999999999999999999999999998CCCCCCCCE666667333339999CCCCE6663331998CCE66733198CCE673399CCE673398CC673198CE63398CE7319CC6339CC6339CE6318C6339CE739CE739CE318C639CE718C739C639CE31CE31CE39C638C718E31C638E71C738E39C71C738E38E38E38E71C71C38E38E38E38F1C71C78E3871C78E3C71E3871E3871E3870E3C78F1E3C78F1E1C3878F1E1C3C387870F0E1E1E1E3C3C3C3C3C3C3C3C1E1E1E0F0F078783C3E1F0F87C;

cycloneive_ram_block ram_block1a111(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a111_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a111.clk0_core_clock_enable = "ena0";
defparam ram_block1a111.clk0_input_clock_enable = "ena0";
defparam ram_block1a111.clk0_output_clock_enable = "ena0";
defparam ram_block1a111.data_interleave_offset_in_bits = 1;
defparam ram_block1a111.data_interleave_width_in_bits = 1;
defparam ram_block1a111.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a111.init_file_layout = "port_a";
defparam ram_block1a111.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a111.operation_mode = "rom";
defparam ram_block1a111.port_a_address_clear = "none";
defparam ram_block1a111.port_a_address_width = 13;
defparam ram_block1a111.port_a_data_out_clear = "none";
defparam ram_block1a111.port_a_data_out_clock = "clock0";
defparam ram_block1a111.port_a_data_width = 1;
defparam ram_block1a111.port_a_first_address = 49152;
defparam ram_block1a111.port_a_first_bit_number = 3;
defparam ram_block1a111.port_a_last_address = 57343;
defparam ram_block1a111.port_a_logical_ram_depth = 65536;
defparam ram_block1a111.port_a_logical_ram_width = 18;
defparam ram_block1a111.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a111.ram_block_type = "auto";
defparam ram_block1a111.mem_init3 = 2048'hAA552A954AAD56AA556AB552AB552AA556AA554AA955AAB554AA9552AAD552AAD552AAD556AA9554AAAD556AAA5556AAAD554AAA95556AAA95556AAAB55552AAAB55556AAAA955556AAAAA555554AAAAAB5555552AAAAAA955555552AAAAAAAAD5555555555AAAAAAAAAAAAAAD55555555555555555555555555555555555555555555555555555555555552AAAAAAAAAAAAA95555555555AAAAAAAAA55555554AAAAAAB555555AAAAAA555554AAAAA55555AAAAB55556AAAB55552AAAD5552AAAD555AAA95552AAB555AAA9554AAA555AAA5552AA555AAA554AA9552AA556AAD54AA955AA955AAD54AAD56AB55AA954AA55AAD56AB54AA55AA55AAD52AD52AD;
defparam ram_block1a111.mem_init2 = 2048'h5AA55AA54AB56A952AD5AB56AD52A54A952B56AD5AB52A54AD5A952B56A56AD4AD4A95A95A95A95A95A95A95AD4AD4A56A52B5A95AD4A56B5A94AD6B5294A56B5AD6B5294A5294A5294A5296B5AD6B4A5294B5AD294B5AD296B5A52D694B5A52D296B4B5A5AD2D69694B4B5A5A5A52D2D2D2D29696969696969696D2D2D2D2D25A5A5B4B4B69692D2DA5B4B696D2DA5B49692DA4B496D25B496D25B496DA4B6925B692DB492DB492DB4925B6D2496DB4924B6DB492492DB6DB692492492DB6DB6DB6DB6DB6DB6DB6DB6DB6C924924926DB6DB24924DB6D924936DB249B6D924DB64936D926DB24DB24DB24DB24DB24D926D936C9B64D926C9B64D9364DB26C9B;
defparam ram_block1a111.mem_init1 = 2048'h26C9B26C9B364D9366C9B264D9326CD9326CD93264C9B366CD9B366CD9B366CC993266CC9933664CD9933664CD99B33664CC999B336664CCD99933366664CCC999993333266666CCCCCCD9999999933333333333333666666666666666633333333333333199999998CCCCCCE66667333319999CCCC66673331998CCC666333998CCE6633399CCE6633198CE673399CC663398CC67319CC67319CC63398CE7318CE6318CE7318C6739CE6318C6318C6318C6318C6318E739CE318C739C631CE718E738C738C738C738C718E71CE31C638C71CE39C718E38C71C638E39C71C718E38E38E39C71C71C71C71C71C71C38E38E38E3C71C71E38E3C71C78E3C71C38E;
defparam ram_block1a111.mem_init0 = 2048'h1C78E3C70E3C70E3C78E1C38F1E3C78E1C3870E1E3C78F0E1C3C78F0E1E3C3C7870F0E1E1E3C3C3C787878787878F0F0F0F878787878783C3C3C1E1E0F0F0787C3C1E0F0F87C3E1F0F87C1E0F07C3E0F87C1F0F83E0F83E0F83E0F83E0F83E0FC1F07E0F81F07E0FC1F83F07E07C0FC0F81F81F81F81F81F81F81FC0FC07E03F01F80FC07F01F80FE03F80FE01FC07F80FE01FE03FC03FC03FC03FC03FE01FF007F803FE00FF801FF003FE007FE007FF003FF800FFE003FF8007FF8007FFC003FFF0007FFE0007FFF0001FFFE0001FFFF80003FFFF800007FFFFE000003FFFFFE0000003FFFFFFFC000000000FFFFFFFFFFFFF00000000000000000000000000;

cycloneive_ram_block ram_block1a93(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a93_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a93.clk0_core_clock_enable = "ena0";
defparam ram_block1a93.clk0_input_clock_enable = "ena0";
defparam ram_block1a93.clk0_output_clock_enable = "ena0";
defparam ram_block1a93.data_interleave_offset_in_bits = 1;
defparam ram_block1a93.data_interleave_width_in_bits = 1;
defparam ram_block1a93.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a93.init_file_layout = "port_a";
defparam ram_block1a93.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a93.operation_mode = "rom";
defparam ram_block1a93.port_a_address_clear = "none";
defparam ram_block1a93.port_a_address_width = 13;
defparam ram_block1a93.port_a_data_out_clear = "none";
defparam ram_block1a93.port_a_data_out_clock = "clock0";
defparam ram_block1a93.port_a_data_width = 1;
defparam ram_block1a93.port_a_first_address = 40960;
defparam ram_block1a93.port_a_first_bit_number = 3;
defparam ram_block1a93.port_a_last_address = 49151;
defparam ram_block1a93.port_a_logical_ram_depth = 65536;
defparam ram_block1a93.port_a_logical_ram_width = 18;
defparam ram_block1a93.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a93.ram_block_type = "auto";
defparam ram_block1a93.mem_init3 = 2048'h00000000000000000000000001FFFFFFFFFFFFE0000000007FFFFFFF8000000FFFFFF800000FFFFFC00003FFFF80003FFFF0000FFFF0001FFFC000FFFC001FFF8007FFC003FFC003FF800FFE003FF801FFC00FFC00FF801FF003FE00FF803FC01FF00FF807F807F807F807F80FF00FE03FC07F00FE03F80FE03F01FC07E03F01F80FC07E07F03F03F03F03F03F03F03E07E07C0FC1F83F07E0FC1F03E0FC1F07E0F83E0F83E0F83E0F83E0F83E1F07C3E0F87C1E0F07C3E1F0F87C3E1E0F0787C3C1E1E0F0F07878783C3C3C3C3C3E1E1E1E3C3C3C3C3C3C787878F0F0E1E1C3C7878F0E1E3C7870E1E3C78F0E1C3870E3C78F1E3870E3C78E1C78E1C78E3C70;
defparam ram_block1a93.mem_init2 = 2048'hE3871C78E3C71C78E38F1C71C78E38E38E3871C71C71C71C71C71C738E38E38E31C71C738E38C71C638E31C738E71C638C718E71CE31C639C639C639C639CE31CE718C739C6318E739CE318C6318C6318C6318C6318CE739CC6319CE6318CE6319CE63398C67319CC67319CC663398CC673399CCE633198CCE6733998CCE66333998CCC6663331999CCCC6667333319999CCCCCE66666633333333199999999999998CCCCCCCCCCCCCCCD99999999999999333333336666666CCCCC99999333326664CCCD9993336664CCD99B332664CD99B33664CD9933664CD993266CC993266CD9B366CD9B366CD9B264C99366C99366C99364C9B26CD9364D9B26C9B26C9;
defparam ram_block1a93.mem_init1 = 2048'hB26C9B64D9364DB26C9364DB26D936C93649B649B649B649B649B6C936D924DB64936DB249B6D924936DB649249B6DB6C924924926DB6DB6DB6DB6DB6DB6DB6DB6DB692492492DB6DB6924925B6DA4925B6D2496DB4925B6925B6925B692DB492DA4B6D25B496D25B496D25A4B692D25B4B696D2DA5B4B69692D2DA5A5B4B4B49696969696D2D2D2D2D2D2D2D29696969694B4B4B5A5A52D2D696B4B5A5AD29694B5A52D694B5AD296B5A5296B5A5294A5AD6B5AD294A5294A5294A5295AD6B5AD4A5295AD6A52B5AD4A56B52B5A94AD4A56A56B52B52B52B52B52B52B52A56A56AD4AD5A952B56A54A95AB56AD5A952A54A956AD5AB56A952AD5AA54AB54AB5;
defparam ram_block1a93.mem_init0 = 2048'h6A956A956AB54AB54AA55AAD56AB54AA552AB55AAD56AA556AB552AB552AA556AAD54AA9552AA554AAB554AA9554AAB554AAA5552AAB555AAA95552AAB5556AAA95556AAA95555AAAAD5555AAAAB55554AAAAA555554AAAAAB555555AAAAAAA55555554AAAAAAAAB55555555552AAAAAAAAAAAAA955555555555555555555555555555555555555555555555555555555555556AAAAAAAAAAAAAB55555555556AAAAAAAA955555552AAAAAA9555555AAAAAA555554AAAAAD55552AAAAD5555AAAA95555AAAAD5552AAAD5552AAA5556AAAD554AAAD556AAA5552AAD556AA9556AA9556AA9552AA555AAB552AA554AAD54AA955AA955AAD54AAD56AA552A954AA;

cycloneive_ram_block ram_block1a75(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a75_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a75.clk0_core_clock_enable = "ena0";
defparam ram_block1a75.clk0_input_clock_enable = "ena0";
defparam ram_block1a75.clk0_output_clock_enable = "ena0";
defparam ram_block1a75.data_interleave_offset_in_bits = 1;
defparam ram_block1a75.data_interleave_width_in_bits = 1;
defparam ram_block1a75.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a75.init_file_layout = "port_a";
defparam ram_block1a75.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a75.operation_mode = "rom";
defparam ram_block1a75.port_a_address_clear = "none";
defparam ram_block1a75.port_a_address_width = 13;
defparam ram_block1a75.port_a_data_out_clear = "none";
defparam ram_block1a75.port_a_data_out_clock = "clock0";
defparam ram_block1a75.port_a_data_width = 1;
defparam ram_block1a75.port_a_first_address = 32768;
defparam ram_block1a75.port_a_first_bit_number = 3;
defparam ram_block1a75.port_a_last_address = 40959;
defparam ram_block1a75.port_a_logical_ram_depth = 65536;
defparam ram_block1a75.port_a_logical_ram_width = 18;
defparam ram_block1a75.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a75.ram_block_type = "auto";
defparam ram_block1a75.mem_init3 = 2048'h552A954AA55AAD56A954AB54AB55AA55AA55AA55AB54AB54A956AD52A55AB56A952A54A952A54A952A54A952A56AD5A952A56AD4A95A952B56A56A54AD4AD4A95A95A95A95A95A94AD4AD4A56A56B52B5A95AD4A56A52B5AD4A56B5294AD6B5294A56B5AD6A5294A5294A52B5AD6B5A5294A5294A5296B5AD694A52D6B5A5296B5A5296B4A52D694B5A52D694B5A52D696B4A5A52D29694B4A5A5AD2D29696B4B4B4A5A5A5A52D2D2D2D2D2969696969696969692D2D2D2D2D2DA5A5A5A4B4B4B69696D2D25A5A4B49696D2DA5A4B49692D25B4B696D25A4B696D25B49692DA4B692DA4B692DA496D25B492DA496D24B6925B492DB492DB492DB4925B6924B6D;
defparam ram_block1a75.mem_init2 = 2048'h2496DB4925B6DA492DB6D24925B6DA492496DB6DA492492DB6DB6DB492492492492DB6DB6DB6DB6DB6DB6DB6DB6DB6DB6DB6C924924924926DB6DB6D9249249B6DB6C924936DB6C9249B6DB24936DB24936DB24936D9249B6C926DB249B6C936D924DB249B649B6C936C936C936C936C936C936C93649B64DB24D926D936C9B64DB26C9364DB26C9364DB26C9B64D9364DB26C9B26C9B26C9B26C9B26C9B26C9B26C9B264D9364D9B26C99364D9B26CD9366C9B364C9B264D9B264D9B264C9B364C99366CD9B264C99326CD9B366CD9B366CD9B366CD9B3664C993266CD9B3264CD9B3664CD9B3264CD993366CC99B3266CC99B3266CC99933664CC99B33664C;
defparam ram_block1a75.mem_init1 = 2048'hC99B33666CCD99B33666CCD999332664CCD999332666CCC999B3336664CCC999B3336666CCCD999933336666CCCCD99993333266664CCCCD9999B33333666666CCCCCC999999B333333266666664CCCCCCCD999999999B33333333332666666666666664CCCCCCCCCCCCCCCCCCCCCCCCCCCD999999999999999999998CCCCCCCCCCCCCCCCCCCCCCCCCCCCE6666666666666673333333333319999999998CCCCCCCCC66666666333333339999998CCCCCCE666667333333999998CCCCCE666673333399999CCCCC66667333319999CCCCE666733339999CCCCE66673331999CCCCE6663333999CCCC66673339998CCC6667333999CCCE667333999CCCE6633319;
defparam ram_block1a75.mem_init0 = 2048'h98CCE667333998CCE66733199CCCE66333998CCE66333998CCE6633399CCC66733198CCE6633199CCC6633399CCC6633199CCE6633198CCE673399CCC6633198CC6633199CCE673399CCE673399CCE673198CC6633198CC663399CCE673198CC663399CCE673198CC673398CC663399CC663319CCE63319CCE63319CCE63319CCE63319CC663399CC673398CC673198CE63319CC663398CC67319CCE63399CC673198CE63399CC67319CCE63398CE67319CC673398CE63398CE67319CC67319CCE63398CE63398CC67319CC67319CC673198CE63398CE63398CE63398CE67319CC67319CC67319CC67319CC67319CC673198CE63398CE63398CE63398CE63398;

cycloneive_ram_block ram_block1a129(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a129_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a129.clk0_core_clock_enable = "ena0";
defparam ram_block1a129.clk0_input_clock_enable = "ena0";
defparam ram_block1a129.clk0_output_clock_enable = "ena0";
defparam ram_block1a129.data_interleave_offset_in_bits = 1;
defparam ram_block1a129.data_interleave_width_in_bits = 1;
defparam ram_block1a129.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a129.init_file_layout = "port_a";
defparam ram_block1a129.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a129.operation_mode = "rom";
defparam ram_block1a129.port_a_address_clear = "none";
defparam ram_block1a129.port_a_address_width = 13;
defparam ram_block1a129.port_a_data_out_clear = "none";
defparam ram_block1a129.port_a_data_out_clock = "clock0";
defparam ram_block1a129.port_a_data_width = 1;
defparam ram_block1a129.port_a_first_address = 57344;
defparam ram_block1a129.port_a_first_bit_number = 3;
defparam ram_block1a129.port_a_last_address = 65535;
defparam ram_block1a129.port_a_logical_ram_depth = 65536;
defparam ram_block1a129.port_a_logical_ram_width = 18;
defparam ram_block1a129.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a129.ram_block_type = "auto";
defparam ram_block1a129.mem_init3 = 2048'h3398CE63398CE63398CE63398CE63319CC67319CC67319CC67319CC67319CC67319CCE63398CE63398CE63398CE63319CC67319CC67319CC663398CE63398CE67319CC67319CCE63398CE63399CC67319CCE63398CE67319CC673398CE63319CC673398CE67319CC663398CC673198CE63319CC663399CC673398CC673198CE673198CE673198CE673198CE673198CC673398CC663399CC663319CCE673398CC663319CCE673398CC6633198CC663319CCE673399CCE673399CCE6733198CC6633198CC6673399CCE6633198CCE6733198CC66733998CC66733198CCE6633199CCC66733998CCE66333998CCE66333998CCE66733199CCCE66333999CCCE6633;
defparam ram_block1a129.mem_init2 = 2048'h31998CCE667333999CCCE667333999CCCC6663333999CCCC66673339998CCCE66673331999CCCCE666733339999CCCCE6667333319999CCCCC666673333399999CCCCCE66666333333999999CCCCCCE666666333333399999998CCCCCCCC6666666663333333333199999999999CCCCCCCCCCCCCCCE666666666666666666666666666633333333333333333333366666666666666666666666666664CCCCCCCCCCCCCC99999999999B333333333666666664CCCCCCC9999999B333332666666CCCCCD99999B3333666664CCCC99999333366666CCCD999933336666CCCD999B3326664CCD999B332666CCC9993336664CC999333666CCD99B33666CCD99B326;
defparam ram_block1a129.mem_init1 = 2048'h64CD99B32664CD9933266CC99B3266CC99B3266CD9933664C99B3664CD9B3664C99B366CC993264CD9B366CD9B366CD9B366CD9B366C993264C9B366CD93264D9B264C9B364C9B364C9B264D9B26CD9366C9B364D9326C9B364D9364C9B26C9B26C9B26C9B26C9B26C9B26C9B26C9B64D9364DB26C9B64D926C9B64D926C9B64DB26D936C93649B64DB24D926D926D926D926D926D926D926DB24DB249B64936D926DB249B6C926DB24936D9249B6D9249B6D9249B6DB24926DB6D924926DB6DB24924936DB6DB6C924924924926DB6DB6DB6DB6DB6DB6DB6DB6DB6DB6DB6924924924925B6DB6DB6924924B6DB6D24924B6DB492496DB6924B6DB4925B6D249;
defparam ram_block1a129.mem_init0 = 2048'h6DA492DB4925B6925B6925B6925B492DA496D24B6925B496D24B692DA4B692DA4B692D25B496D2DA4B496D2DA5B49692D25A4B4B696D2D25A4B4B49696D2D2DA5A5A4B4B4B4B6969696969692D2D2D2D2D2D2D2D2969696969694B4B4B4A5A5A5AD2D29696B4B4A5A52D29694B4A5AD2D694B5A52D694B5A52D694A5AD294B5AD294B5AD694A52D6B5AD294A5294A5294B5AD6B5A94A5294A5294AD6B5AD4A5295AD6A5295AD4A56B5A94AD4A56B52B5A95AD4AD4A56A56A52B52B52B52B52B52A56A56A54AD4AD5A952B52A56AD4A952B56AD4A952A54A952A54A952A54A952AD5AB54A956AD52A55AA55AB54AB54AB54AB55AA55AA552AD56AB54AA552A954;

cycloneive_ram_block ram_block1a21(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a21_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a21.clk0_core_clock_enable = "ena0";
defparam ram_block1a21.clk0_input_clock_enable = "ena0";
defparam ram_block1a21.clk0_output_clock_enable = "ena0";
defparam ram_block1a21.data_interleave_offset_in_bits = 1;
defparam ram_block1a21.data_interleave_width_in_bits = 1;
defparam ram_block1a21.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a21.init_file_layout = "port_a";
defparam ram_block1a21.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a21.operation_mode = "rom";
defparam ram_block1a21.port_a_address_clear = "none";
defparam ram_block1a21.port_a_address_width = 13;
defparam ram_block1a21.port_a_data_out_clear = "none";
defparam ram_block1a21.port_a_data_out_clock = "clock0";
defparam ram_block1a21.port_a_data_width = 1;
defparam ram_block1a21.port_a_first_address = 8192;
defparam ram_block1a21.port_a_first_bit_number = 3;
defparam ram_block1a21.port_a_last_address = 16383;
defparam ram_block1a21.port_a_logical_ram_depth = 65536;
defparam ram_block1a21.port_a_logical_ram_width = 18;
defparam ram_block1a21.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a21.ram_block_type = "auto";
defparam ram_block1a21.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000FFFFFFFFF80000000FFFFFFF000000FFFFFE00000FFFFF00001FFFF80003FFFC0003FFF8000FFFC000FFF8003FFE001FFE001FFE003FFC007FF003FF801FF801FF803FF007FE00FF803FE00FF007F803FC03FC03FC03FC03F807F00FE01FC07F00FE03F80FC07F01F80FE07F03F81F80FC0FC07E07E07E07E07E07C0FC0F81F83F07E0FC1F83F07C0F83F07C1F03E0F83E0F83E0F83E0F87C1F0783E0F07C3E0F07C3E1F0F87C3E1F0F0787C3C1E1F0F0F878783C3C3C3E1E1E1E1E1E1E1E1E1E1E3C3C3C3C787870F0F1E1C3C3878F0E1E3C3870F1E3C78F1E1C3871E3C78F1C3871E3C70E3C70E3C70E387;
defparam ram_block1a21.mem_init2 = 2048'h1C38E1C71E38E1C71C38E38F1C71C71E38E38E38E38E38E38E38E38E38E31C71C71CE38E39C71CE38E71C638E71C638C718E31C639C638C738C738C738C739C639CE318E739C6318E739CE718C6318C6318C6319CE739CE6318CE7398C67398C67318CE6339CC67319CC673398CE63319CCE673198CC6633399CCE6633399CCCE66333999CCCE66633339998CCCC6666733333999999CCCCCCCC66666666666733333333333333333333333332666666666664CCCCCCC999999B3333266664CCCD999B3336666CCC999B332664CC99933266CCD9933664CD9933664C99B3264C99B366CD9B366CD9B366C99326CD93264D9326CD9366C9B264D9364D9364D936;
defparam ram_block1a21.mem_init1 = 2048'h4D9364D93649B26C9364DB26D936C9B649B24DB24DB24DB24DB249B64936D924DB64936DB249B6DB24926DB6C924926DB6DB6C92492492492492492492492492492492496DB6DB6D24924B6DB692496DB6924B6DA496DB492DB492DB492DB496DA4B6925B496D25B496D25A4B692D25B4B696D25A5B4B696D2D25A5A4B4B496969692D2D2D2D2D2D2D2D2D2D2D2D2D296969694B4B4A5A5AD2D29694B4A5AD2D694B5A52D694B5AD296B5A5296B5AD294A52D6B5AD6B5AD694AD6B5AD6B5AD6A5294AD6B5A94AD6B5295AD4A56B52B5A95AD4AD4AD6A56A56A56A56A56AD4AD4A95A952B52A56AD5A952A56AD5AB56A54A956AD5AB56A952A55AB54A956A956A;
defparam ram_block1a21.mem_init0 = 2048'hD52AD52AD56A956A954AB55AAD52A954AA552AB55AAD54AA556AA556AA556AAD54AA9552AA554AA9552AAD552AAD552AA9554AAA5552AAB5552AAB5552AAAD555AAAA5555AAAAD5554AAAAD5555AAAAA555552AAAAA555554AAAAAAD5555552AAAAAAB555555556AAAAAAAAAB55555555555552AAAAAAAAAAAAAAAAAAAAAAAAAAA95555555552AAAAAAAAAAAAAAAAAAAAAAAAAAAB55555555555552AAAAAAAAA9555555556AAAAAAAD5555552AAAAAB555554AAAAA955554AAAAA55554AAAA95555AAAA95554AAAB5554AAA95556AAA5554AAAD554AAA5552AA9554AAB555AAA555AAA554AAB556AAD55AAB552AA556AAD54AAD54AAD54AA556AB552A954AA55;

cycloneive_ram_block ram_block1a39(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a39_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a39.clk0_core_clock_enable = "ena0";
defparam ram_block1a39.clk0_input_clock_enable = "ena0";
defparam ram_block1a39.clk0_output_clock_enable = "ena0";
defparam ram_block1a39.data_interleave_offset_in_bits = 1;
defparam ram_block1a39.data_interleave_width_in_bits = 1;
defparam ram_block1a39.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a39.init_file_layout = "port_a";
defparam ram_block1a39.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a39.operation_mode = "rom";
defparam ram_block1a39.port_a_address_clear = "none";
defparam ram_block1a39.port_a_address_width = 13;
defparam ram_block1a39.port_a_data_out_clear = "none";
defparam ram_block1a39.port_a_data_out_clock = "clock0";
defparam ram_block1a39.port_a_data_width = 1;
defparam ram_block1a39.port_a_first_address = 16384;
defparam ram_block1a39.port_a_first_bit_number = 3;
defparam ram_block1a39.port_a_last_address = 24575;
defparam ram_block1a39.port_a_logical_ram_depth = 65536;
defparam ram_block1a39.port_a_logical_ram_width = 18;
defparam ram_block1a39.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a39.ram_block_type = "auto";
defparam ram_block1a39.mem_init3 = 2048'h54AA552A955AAD54AA556AA556AA556AAD54AA955AAB556AAD55AAA554AAB554AAB555AAA5552AA9554AAA5556AAA5554AAAD5552AAA5555AAAA55552AAAB55552AAAA55554AAAAA555552AAAAA555555AAAAAA95555556AAAAAAAD555555552AAAAAAAAA95555555555555AAAAAAAAAAAAAAAAAAAAAAAAAAAA95555555552AAAAAAAAAAAAAAAAAAAAAAAAAAA95555555555555AAAAAAAAAAD55555555AAAAAAA95555556AAAAAA555554AAAAA955554AAAAB55556AAAA55556AAAB5554AAAB5556AAA9555AAA9555AAA9554AAA5552AA9556AA9556AA9552AA554AA9552AA556AAD54AAD54AAD54AA556AB55AA954AA552A956AB55AA552AD52AD56A956A956;
defparam ram_block1a39.mem_init2 = 2048'hAD52AD52A55AB54A952AD5AB56AD52A54AD5AB56AD4A952B56AD4A95A952B52A56A56AD4AD4AD4AD4AD4AD6A56A56B52B5A95AD4A56B5295AD6A52B5AD6A5294AD6B5AD6B5AD6A52D6B5AD6B5AD694A5296B5AD294B5AD296B5A52D694B5A52D696B4A5A52D29696B4B4A5A5A52D2D2D29696969696969696969696969692D2D2D25A5A4B4B49696D2DA5B4B496D2DA5B49692DA4B496D25B496D25B492DA4B6D25B6925B6925B6925B6D24B6DA492DB6D2492DB6DA492496DB6DB6D24924924924924924924924924924924926DB6DB6C924926DB6C9249B6DB249B6D924DB64936D924DB249B649B649B649B649B24DB26D936C9B64D926C9B24D9364D9364;
defparam ram_block1a39.mem_init1 = 2048'hD9364D9364D9364C9B26CD9366C99364C99366C99326CD9B366CD9B366CD9B3264C99B3264CD9933664CD9933666CC999332664CC999B332666CCCD999B33366664CCCC99999B33333266666664CCCCCCCCCCC99999999999999999999999999CCCCCCCCCCCC6666666733333399999CCCCC666633339998CCCE667333998CCE66733998CCE6733998CC663319CCE673198CE63399CC67319CC67398CE6319CC6339CC6339CE6318CE739CE7318C6318C6318C631CE739CE318C739CE318E738C739C639C639C639C638C738C718E31C638C71CE38C71CE38E71C738E38E71C71C718E38E38E38E38E38E38E38E38E38F1C71C71E38E3871C70E38F1C70E3871;
defparam ram_block1a39.mem_init0 = 2048'hC38E1C78E1C78E1C78F1C3871E3C78F1C3870F1E3C78F1E1C3878F0E1E3C387870F1E1E1C3C3C78787878F0F0F0F0F0F0F0F0F0F0F87878783C3C3E1E1F0F0787C3C1E1F0F87C3E1F0F87C1E0F87C1E0F83C1F07C3E0F83E0F83E0F83E0F81F07C1F83E07C1F83F07E0FC1F83F03E07E07C0FC0FC0FC0FC0FC07E07E03F03F81FC0FE03F01FC07E03F80FE01FC07F00FE01FC03F807F807F807F807F803FC01FE00FF803FE00FFC01FF803FF003FF003FF801FFC007FF800FFF000FFF000FFF8003FFE0007FFE0003FFF80007FFF80003FFFF00001FFFFE00000FFFFFE000001FFFFFFE00000003FFFFFFFFE000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a3(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a3_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a3.clk0_core_clock_enable = "ena0";
defparam ram_block1a3.clk0_input_clock_enable = "ena0";
defparam ram_block1a3.clk0_output_clock_enable = "ena0";
defparam ram_block1a3.data_interleave_offset_in_bits = 1;
defparam ram_block1a3.data_interleave_width_in_bits = 1;
defparam ram_block1a3.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a3.init_file_layout = "port_a";
defparam ram_block1a3.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a3.operation_mode = "rom";
defparam ram_block1a3.port_a_address_clear = "none";
defparam ram_block1a3.port_a_address_width = 13;
defparam ram_block1a3.port_a_data_out_clear = "none";
defparam ram_block1a3.port_a_data_out_clock = "clock0";
defparam ram_block1a3.port_a_data_width = 1;
defparam ram_block1a3.port_a_first_address = 0;
defparam ram_block1a3.port_a_first_bit_number = 3;
defparam ram_block1a3.port_a_last_address = 8191;
defparam ram_block1a3.port_a_logical_ram_depth = 65536;
defparam ram_block1a3.port_a_logical_ram_width = 18;
defparam ram_block1a3.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a3.ram_block_type = "auto";
defparam ram_block1a3.mem_init3 = 2048'h2A954AA552A956AB54AA55AA552AD52AD52AD52AD5AA55AA54AB56A952AD5AB56A952A54A952A54A952A54A95AB56A54A95AB52A56AD4A95A952B52B56A56A56A56AD4AD4A56A56A56A56B52B5295A94AD4A56A52B5A94AD6A5295AD4A52B5AD6A5294AD6B5AD6A5294A5294A5294A5294A5294A5AD6B5AD294A52D6B4A5296B5A5296B4A52D694B5A52D694B5A52D694B4A5A52D696B4B5A5A52D2D69694B4B4A5A5A5AD2D2D2D296969696969696969696969696969692D2D2D2D25A5A5A4B4B4969692D2DA5A4B4B69692D25A4B49692D25A4B696D25A4B696D25B49692DA4B692DA4B6925B496D25B692DA496D24B6D25B6925B6925B6925B6924B6D2496;
defparam ram_block1a3.mem_init2 = 2048'hDA492DB692496DB4924B6DB492496DB6D24924B6DB6DA4924925B6DB6DB6DA49249249249249249249249249249249249249249249B6DB6DB6D924924936DB6DB249249B6DB24924DB6D924936DB24936DB24936D9249B6C926DB249B6C926DB24DB649B6C936C926D926D926D926D926D926D926C936C93649B64DB26D936C9B64DB26D93649B26D9364DB26C9B24D9364D936C9B26C9B26C9B26C9B26C9B26C9B364D9364D9B26C9B364D9326C99364D9B264D9326CD9326CD9326CD9326CD9B264C9B366CD93264C99366CD9B366CD9B366CD9B366CC993264C99B366CC993366CD993366CC9933664CD9B3266CC99B3266CC99B3266CCD9933666CC99933;
defparam ram_block1a3.mem_init1 = 2048'h266CCD99B33666CCD99B33666CCD999332666CCC999B332666CCC999B3336664CCC999933326666CCCD9999333366666CCCCD9999B3333666664CCCCD99999B333332666666CCCCCCD9999999B33333332666666666CCCCCCCCCCD9999999999999B3333333333333333333326666666666666666666666666666666666666666667333333333333333333333199999999999998CCCCCCCCCCCE66666666633333333399999999CCCCCCCE66666673333331999998CCCCCE6666633333399999CCCCCE66663333319999CCCCE6666333319999CCCCE666333319998CCCE66633339998CCCE66633339998CCC6667333999CCCE6663331998CCC667333999CCCE;
defparam ram_block1a3.mem_init0 = 2048'h663331998CCE66333199CCC666333998CCE66333998CCE66333998CCE6633399CCC66733998CCE6733198CCE6733198CCE6733998CC6633399CCE6733198CC6633198CCE673399CCE673399CCE673399CCE633198CC6633198CE673399CCE633198CE673399CC663319CCE673198CE673398CC673398CC673398CC673398CC673398CC673198CE67319CCE63319CC663398CC673198CE63319CC673398CE63319CC663398CE63319CC673398CE63399CC67319CC663398CE63399CC67319CC673198CE63398CE63399CC67319CC67319CC673398CE63398CE63398CE63398CE67319CC67319CC67319CC67319CC67319CC67319CCE63398CE63398CE63398CE6;

cycloneive_ram_block ram_block1a57(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a57_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a57.clk0_core_clock_enable = "ena0";
defparam ram_block1a57.clk0_input_clock_enable = "ena0";
defparam ram_block1a57.clk0_output_clock_enable = "ena0";
defparam ram_block1a57.data_interleave_offset_in_bits = 1;
defparam ram_block1a57.data_interleave_width_in_bits = 1;
defparam ram_block1a57.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a57.init_file_layout = "port_a";
defparam ram_block1a57.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a57.operation_mode = "rom";
defparam ram_block1a57.port_a_address_clear = "none";
defparam ram_block1a57.port_a_address_width = 13;
defparam ram_block1a57.port_a_data_out_clear = "none";
defparam ram_block1a57.port_a_data_out_clock = "clock0";
defparam ram_block1a57.port_a_data_width = 1;
defparam ram_block1a57.port_a_first_address = 24576;
defparam ram_block1a57.port_a_first_bit_number = 3;
defparam ram_block1a57.port_a_last_address = 32767;
defparam ram_block1a57.port_a_logical_ram_depth = 65536;
defparam ram_block1a57.port_a_logical_ram_width = 18;
defparam ram_block1a57.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a57.ram_block_type = "auto";
defparam ram_block1a57.mem_init3 = 2048'hCE63398CE63398CE63398CE67319CC67319CC67319CC67319CC67319CC67319CCE63398CE63398CE63398CE63399CC67319CC67319CC673398CE63398CE63319CC67319CC673398CE63398CC67319CC673398CE63399CC673198CE63398CC673198CE63399CC673198CE63319CC663398CC673198CE67319CCE63319CC663399CC663399CC663399CC663399CC663399CCE63319CCE673198CC673399CCE633198CE673399CCE633198CC6633198CE673399CCE673399CCE673399CCE6633198CC6633199CCE6733998CC6633399CCE6633199CCE6633199CCE6633399CCC66733998CCE66333998CCE66333998CCE66333998CCC667331998CCE663331998CC;
defparam ram_block1a57.mem_init2 = 2048'hE667333999CCC6663331998CCCE667333999CCCC66633339998CCCE66633339998CCCE666333319998CCCE6667333319998CCCCE66673333199998CCCCE6666733333999998CCCCCE666663333331999999CCCCCCCE666666733333333999999998CCCCCCCCCE66666666666333333333333331999999999999999999999CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC999999999999999999999B333333333333366666666666CCCCCCCCC99999999B33333336666666CCCCCC999999B33333666664CCCCD9999B333366666CCCCD999933336666CCCC999933326664CCD999B332666CCC999B332666CCC999333666CCD99B33666CCD99B33666CC9;
defparam ram_block1a57.mem_init1 = 2048'h9933266CCD9933666CC99B3266CC99B3266CC99B3664CD993266CD993366CD993266CD9B3264C993266CD9B366CD9B366CD9B366CD93264C99366CD9B264C9B366C99366C99366C99366C99364C9B364D9326C99364D9B26C9B364D9364D9B26C9B26C9B26C9B26C9B26C9B26D9364D93649B26C9B64D936C9B24D936C9B64DB26D936C9B64DB24D926D926C936C936C936C936C936C936C926D926DB24DB649B6C926DB249B6C926DB24936D9249B6D9249B6D924936DB649249B6DB249249B6DB6D924924936DB6DB6DB24924924924924924924924924924924924924924924B6DB6DB6DB4924924B6DB6DA492496DB6D24925B6DA4925B6D2492DB6924B6;
defparam ram_block1a57.mem_init0 = 2048'hD2496DA492DB492DB492DB492DB496DA496D24B692DB496D25B492DA4B692DA4B692D25B496D2DA4B496D2DA4B49692D25A4B49692D2DA5A4B4B69692D2D25A5A4B4B4B49696969692D2D2D2D2D2D2D2D2D2D2D2D2D2D2D296969696B4B4B4A5A5A52D2D69694B4B5A5AD2D694B4A5A52D694B5A52D694B5A52D694A5AD294B5AD294A5AD694A5296B5AD6B4A5294A5294A5294A5294A5294AD6B5AD6A5294AD6B5A94A56B5294AD6A52B5A94AD4A56A52B5295A95AD4AD4AD4AD4A56A56AD4AD4AD4AD5A95A952B52A56AD4A95AB52A54AD5AB52A54A952A54A952A54A952AD5AB56A952AD5AA54AB54AB56A956A956A956A954AB54AA55AAD52A954AA552A9;

cycloneive_ram_block ram_block1a112(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a112_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a112.clk0_core_clock_enable = "ena0";
defparam ram_block1a112.clk0_input_clock_enable = "ena0";
defparam ram_block1a112.clk0_output_clock_enable = "ena0";
defparam ram_block1a112.data_interleave_offset_in_bits = 1;
defparam ram_block1a112.data_interleave_width_in_bits = 1;
defparam ram_block1a112.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a112.init_file_layout = "port_a";
defparam ram_block1a112.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a112.operation_mode = "rom";
defparam ram_block1a112.port_a_address_clear = "none";
defparam ram_block1a112.port_a_address_width = 13;
defparam ram_block1a112.port_a_data_out_clear = "none";
defparam ram_block1a112.port_a_data_out_clock = "clock0";
defparam ram_block1a112.port_a_data_width = 1;
defparam ram_block1a112.port_a_first_address = 49152;
defparam ram_block1a112.port_a_first_bit_number = 4;
defparam ram_block1a112.port_a_last_address = 57343;
defparam ram_block1a112.port_a_logical_ram_depth = 65536;
defparam ram_block1a112.port_a_logical_ram_width = 18;
defparam ram_block1a112.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a112.ram_block_type = "auto";
defparam ram_block1a112.mem_init3 = 2048'h33664CD9933664CC99B32664CD99B33664CC99933266CCD999332664CC999B336664CC999B332666CCC999B3336664CCC999933326664CCCD999B333266664CCCD9999B3333266664CCCCC9999993333326666664CCCCCCD9999999B3333333366666666666CCCCCCCCCCCCCC99999999999999999999999999999999999999999999999999999999999999CCCCCCCCCCCCCCE666666666633333333399999998CCCCCCC666666333333999998CCCCC666663333399998CCCC666633331999CCCCE6663331999CCCC6663331998CCC666333999CCC666333998CCE66333998CCE6733199CCE6633198CCE673399CCE673399CCE673398CC663399CCE63319CCE;
defparam ram_block1a112.mem_init2 = 2048'h63399CC673398CE63319CC67319CC67319CC67319CC63398CE6319CC67398CE7318CE6319CE6319CE6319CE6318CE7398C6339CE6318C6739CE7318C6318C6739CE739CE739CE739CE739CE739CE738C6318C631CE739CE318C639CE718C639CE318C739C631CE718E738C639C639CE31CE31CE718E718E718E718E31CE31CE39C639C738C718E31CE39C738E71CE39C718E31C738E71C638E71C638E71C738E39C71CE38E31C71CE38E39C71C718E38E38C71C71C71CE38E38E38E38E31C71C71C71C71C71C71C71C71C70E38E38E38E38E3C71C71C71E38E38E3C71C71E38E3871C71E38E3C71C38E3C71C38E3C71E38E1C70E3871E38F1C78E1C78E3C70E3;
defparam ram_block1a112.mem_init1 = 2048'hC70E3C70E3C78E1C78F1C3871E3C70E1C38F1E3C78F1C3870E1C3870E1C3870F1E3C78F0E1C3878F1E1C3878F1E1C3C7870F1E1C3C7878F0E1E1C3C787870F0E1E1E3C3C387878F0F0F0E1E1E1E1E3C3C3C3C3C3C3C78787878787878783C3C3C3C3C3C3C1E1E1E1E0F0F0F0F878783C3C1E1E1F0F078783C3E1E0F0F8783C3E1F0F0783C3E1F0F87C3E1F0F87C3E1F0783C1F0F87C1E0F87C1E0F83C1F0F83E0F07C1F0F83E0F87C1F07C1F07C1F07C1F07C1F07C1F07C1F03E0F83E07C1F07E0F83F07C0F83F07C0F81F07E0FC1F83F07E0FC1F81F03F07E07C0FC1F81F81F03F03F03E07E07E07E07E07E07E03F03F03F03F81F81FC0FC07E07F03F81FC0F;
defparam ram_block1a112.mem_init0 = 2048'hE07F03F80FC07F03F80FE03F01FC07F01FC07F01FC07F00FE03F80FF01FC03F807F00FE01FC03FC07F807F807F80FF00FF007F807F807FC03FC01FE00FF007F803FE00FF007FC01FF007FE00FF803FF007FE00FFC00FFC00FFC00FFC00FFC00FFE007FF001FF800FFE003FF8007FF000FFE001FFE001FFE001FFE000FFF8003FFE000FFF8001FFF0003FFF0001FFF8000FFFE0003FFFC0003FFFC0003FFFE00007FFFC0000FFFFE00003FFFF800007FFFFC00000FFFFFC000007FFFFF8000003FFFFFF80000007FFFFFFE00000001FFFFFFFFC0000000007FFFFFFFFFFC0000000000003FFFFFFFFFFFFFFFFF000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a94(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a94_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a94.clk0_core_clock_enable = "ena0";
defparam ram_block1a94.clk0_input_clock_enable = "ena0";
defparam ram_block1a94.clk0_output_clock_enable = "ena0";
defparam ram_block1a94.data_interleave_offset_in_bits = 1;
defparam ram_block1a94.data_interleave_width_in_bits = 1;
defparam ram_block1a94.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a94.init_file_layout = "port_a";
defparam ram_block1a94.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a94.operation_mode = "rom";
defparam ram_block1a94.port_a_address_clear = "none";
defparam ram_block1a94.port_a_address_width = 13;
defparam ram_block1a94.port_a_data_out_clear = "none";
defparam ram_block1a94.port_a_data_out_clock = "clock0";
defparam ram_block1a94.port_a_data_width = 1;
defparam ram_block1a94.port_a_first_address = 40960;
defparam ram_block1a94.port_a_first_bit_number = 4;
defparam ram_block1a94.port_a_last_address = 49151;
defparam ram_block1a94.port_a_logical_ram_depth = 65536;
defparam ram_block1a94.port_a_logical_ram_width = 18;
defparam ram_block1a94.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a94.ram_block_type = "auto";
defparam ram_block1a94.mem_init3 = 2048'h000000000000000000000000000000000000001FFFFFFFFFFFFFFFFF80000000000007FFFFFFFFFFC0000000007FFFFFFFF00000000FFFFFFFC0000003FFFFFF8000003FFFFFC000007FFFFE000007FFFFC00003FFFF80000FFFFE00007FFFC0000FFFF80007FFF80007FFF8000FFFE0003FFF0001FFF8001FFF0003FFE000FFF8003FFE000FFF000FFF000FFF000FFE001FFC003FF800FFE003FF001FFC00FFE007FE007FE007FE007FE007FE00FFC01FF803FE00FFC01FF007FC01FE00FF803FC01FE00FF007F807FC03FC03FC01FE01FE03FC03FC03FC07F807F00FE01FC03F807F01FE03F80FE01FC07F01FC07F01FC07F01F80FE03F81FC07E03F81FC0F;
defparam ram_block1a94.mem_init2 = 2048'hE07F03F81FC0FC07E07F03F03F81F81F81F80FC0FC0FC0FC0FC0FC0F81F81F81F03F03F07E07C0FC1F81F03F07E0FC1F83F07E0FC1F03E07C1F83E07C1F83E0FC1F07C0F83E0F81F07C1F07C1F07C1F07C1F07C1F07C1F07C3E0F83E1F07C1E0F83E1F0783E0F07C3E0F07C3E1F0783C1F0F87C3E1F0F87C3E1F0F8783C1E1F0F8783C3E1E0F0F8783C3C1E1F0F0F078783C3C3E1E1E1E0F0F0F0F0787878787878783C3C3C3C3C3C3C3C78787878787878F0F0F0F0E1E1E1E3C3C387878F0F0E1E1C3C3C7870F0E1E3C3C7870F1E1C3C7870F1E3C3870F1E3C3870E1E3C78F1E1C3870E1C3870E1C3871E3C78F1E3870E1C78F1C3871E3C70E3C78E1C78E1C7;
defparam ram_block1a94.mem_init1 = 2048'h8E1C78E3C70E3C71E38F1C38E1C70E38F1C78E3871C78E3871C78E38F1C71C38E38F1C71C78E38E38F1C71C71C78E38E38E38E38E1C71C71C71C71C71C71C71C71C718E38E38E38E38E71C71C71C638E38E31C71C738E38E71C718E38E71C738E39C71CE38C71CE38C71CE39C718E31C738E71CE39C738E718E31C639C738C738E718E718E31CE31CE31CE31CE718E718E738C738C639CE31CE718C739C6318E738C631CE738C6318E739CE718C6318C639CE739CE739CE739CE739CE739CE739CC6318C6319CE739CC6318CE7398C6339CE6318CE7318CE7318CE7318CE6319CE6339CC67318CE63398C67319CC67319CC67319CC673198CE63399CC673398C;
defparam ram_block1a94.mem_init0 = 2048'hE673198CE673398CC663399CCE673399CCE673399CCE6633198CCE6733199CCE66333998CCE66333998CCC667333998CCC6663331998CCC66673331998CCCE666733319998CCCC66663333399998CCCCC66666333333999998CCCCCC666666633333333999999998CCCCCCCCCCE666666666666673333333333333333333333333333333333333333333333333333333333333266666666666666CCCCCCCCCCD99999999B333333366666664CCCCCC999999333332666664CCCC99999B333366664CCCC9999B33366664CCC999933326664CCD999B332666CCC999B332664CCD99B332664CC999333666CC999332664CD99B33664CC99B32664CD9933664CD99;

cycloneive_ram_block ram_block1a76(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a76_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a76.clk0_core_clock_enable = "ena0";
defparam ram_block1a76.clk0_input_clock_enable = "ena0";
defparam ram_block1a76.clk0_output_clock_enable = "ena0";
defparam ram_block1a76.data_interleave_offset_in_bits = 1;
defparam ram_block1a76.data_interleave_width_in_bits = 1;
defparam ram_block1a76.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a76.init_file_layout = "port_a";
defparam ram_block1a76.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a76.operation_mode = "rom";
defparam ram_block1a76.port_a_address_clear = "none";
defparam ram_block1a76.port_a_address_width = 13;
defparam ram_block1a76.port_a_data_out_clear = "none";
defparam ram_block1a76.port_a_data_out_clock = "clock0";
defparam ram_block1a76.port_a_data_width = 1;
defparam ram_block1a76.port_a_first_address = 32768;
defparam ram_block1a76.port_a_first_bit_number = 4;
defparam ram_block1a76.port_a_last_address = 40959;
defparam ram_block1a76.port_a_logical_ram_depth = 65536;
defparam ram_block1a76.port_a_logical_ram_width = 18;
defparam ram_block1a76.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a76.ram_block_type = "auto";
defparam ram_block1a76.mem_init3 = 2048'h33664CD9933664CD9B3266CD993366CC993366CC993266CD9B3264C993366CD9B366CD9B366CD9B366CD9B366CD9B364C993264D9B364C99326CD93264D9B264C9B364C9B364C9B264D9B26CD9326C99364C9B26CD9366C9B26CD9364D9B26C9B26CD9364D9364D9364D9366C9B26C9364D9364D9364D9364DB26C9B26C9364D936C9B26D9364DB26C9364DB26C9364DB26D936C9B64DB26D936C9B649B24D926D926C936C93649B649B649B24DB24DB24DB24DB649B649B649B6C936C926D926DB24DB64936C926DB24DB64936D924DB64936D924DB6C926DB24936DB249B6D9249B6D9249B6DB24936DB64924DB6D924936DB649249B6DB64924936DB6D924;
defparam ram_block1a76.mem_init2 = 2048'h924DB6DB6C9249249B6DB6DB6C924924924DB6DB6DB6DB649249249249249249249B6DB6DB6DB6DB6DB6DB6DB6DB6DB6DB6DA4924924924924924924B6DB6DB6DB6DA4924924925B6DB6DB692492496DB6DB6924924B6DB6DA492496DB6DA4924B6DB692492DB6DA4925B6DA4925B6DA4925B6DA492DB6D2496DB4924B6DA492DB6925B6D2496DA492DB6925B6D24B6D2496DA496DA496DA496DA496DA496DA496DA496D24B6D24B6925B492DB496DA4B6D25B692DA496D24B692DB496D25B692DA4B6D25B496D25B496DA4B692DA4B692DA4B692DA4B692D25B496D25B49692DA4B692D25B49692DA4B496D25A4B696D25A4B696D25A4B496D2DA5B49692D25;
defparam ram_block1a76.mem_init1 = 2048'hA4B696D2DA5B4B696D2DA5B4B49692D25A4B4B696D2DA5A4B49696D2D25A5B4B69692D2DA5A4B4B49696D2D25A5A4B4B4969692D2D25A5A4B4B4969696D2D2D25A5A5A4B4B4B696969692D2D2D2DA5A5A5A4B4B4B4B4B6969696969692D2D2D2D2D2D2D25A5A5A5A5A5A5A5A5A5A5A5A5A5B4B4B4B4B4B4B4B4B4B4B5A5A5A5A5A5A5A5A5A5A5A5A5A5A5AD2D2D2D2D2D2D2D69696969696B4B4B4B4B4A5A5A5A5A52D2D2D2D696969694B4B4B5A5A5A5AD2D2D2969696B4B4B5A5A5A52D2D2969694B4B4A5A5AD2D2D69694B4B4A5A5AD2D29696B4B4A5A5AD2D29694B4B5A5A52D29696B4B5A5AD2D29694B4A5A52D2D696B4B5A5AD2D696B4B5A5AD29694B;
defparam ram_block1a76.mem_init0 = 2048'h4A5A52D29694B5A5AD2D694B4A5A52D696B4A5A52D696B4A5A52D696B4A5AD2D694B5A5AD296B4B5A52D696B4A5AD296B4B5A52D694B5A5AD296B4A5AD296B4A5AD296B4B5A52D694B5A52D694B5A52D6B4A5AD296B4A5AD296B4A5AD294B5A52D694B5A52D6B4A5AD296B5A52D694B5AD296B4A5AD694B5A5296B4A5AD694B5A5296B4A52D694B5AD296B5A52D6B4A5AD694B5AD296B5A52D6B4A5AD694B5AD294B5A5296B4A52D6B4A5AD694A5AD294B5AD296B5A5296B5A52D6B4A52D6B4A5AD694A5AD694A5AD294B5AD294B5AD294B5A5296B5A5296B5A5296B5A52D6B4A52D6B4A52D6B4A52D6B4A52D6B4A52D6B4A5AD694A5AD694A5AD694A5AD694A;

cycloneive_ram_block ram_block1a130(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a130_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a130.clk0_core_clock_enable = "ena0";
defparam ram_block1a130.clk0_input_clock_enable = "ena0";
defparam ram_block1a130.clk0_output_clock_enable = "ena0";
defparam ram_block1a130.data_interleave_offset_in_bits = 1;
defparam ram_block1a130.data_interleave_width_in_bits = 1;
defparam ram_block1a130.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a130.init_file_layout = "port_a";
defparam ram_block1a130.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a130.operation_mode = "rom";
defparam ram_block1a130.port_a_address_clear = "none";
defparam ram_block1a130.port_a_address_width = 13;
defparam ram_block1a130.port_a_data_out_clear = "none";
defparam ram_block1a130.port_a_data_out_clock = "clock0";
defparam ram_block1a130.port_a_data_width = 1;
defparam ram_block1a130.port_a_first_address = 57344;
defparam ram_block1a130.port_a_first_bit_number = 4;
defparam ram_block1a130.port_a_last_address = 65535;
defparam ram_block1a130.port_a_logical_ram_depth = 65536;
defparam ram_block1a130.port_a_logical_ram_width = 18;
defparam ram_block1a130.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a130.ram_block_type = "auto";
defparam ram_block1a130.mem_init3 = 2048'hA52D6B4A52D6B4A52D6B4A52D6B4A5AD694A5AD694A5AD694A5AD694A5AD694A5AD694B5AD294B5AD294B5AD294B5A5296B5A5296B5A5296B4A52D6B4A52D6B4A5AD694A5AD694B5AD294B5AD296B5A5296B4A52D6B4A5AD694A5AD294B5A5296B5A52D6B4A5AD694B5AD296B5A52D6B4A5AD694B5AD296B5A52D694A5AD294B5A52D6B4A5AD294B5A52D6B4A5AD296B5A52D694B5AD296B4A5AD694B5A52D694B5A5296B4A5AD296B4A5AD296B4A5AD694B5A52D694B5A52D694B5A5AD296B4A5AD296B4A5AD296B4B5A52D694B5A5AD296B4A5AD2D694B5A5AD296B4B5A52D696B4A5AD2D694B4A5AD2D694B4A5AD2D694B4A5A52D696B4B5A52D29694B4A5;
defparam ram_block1a130.mem_init2 = 2048'hA52D296B4B5A5AD2D696B4B5A5AD2D69694B4A5A52D29696B4B5A5AD2D29694B4B5A5A52D29696B4B4A5A5AD2D29696B4B4A5A5A52D2D69696B4B4A5A5A52D2D2969694B4B4B5A5A5AD2D2D2969696B4B4B4B5A5A5A52D2D2D2D696969694B4B4B4B4A5A5A5A5A5AD2D2D2D2D2D696969696969696B4B4B4B4B4B4B4B4B4B4B4B4B4B4B5A5A5A5A5A5A5A5A5A5A5B4B4B4B4B4B4B4B4B4B4B4B4B4B49696969696969692D2D2D2D2D2DA5A5A5A5A4B4B4B4B696969692D2D2D2DA5A5A4B4B4B4969696D2D2D25A5A4B4B4969692D2D25A5A4B4B49696D2D25A5A4B4B69692D2DA5B4B49696D2D25A4B4B696D2DA5A4B49692D25A5B4B696D2DA5B4B696D2DA4B;
defparam ram_block1a130.mem_init1 = 2048'h49692D25B4B696D25A4B496D2DA4B496D2DA4B496D25A4B692D25B49692DA4B692D25B496D25B49692DA4B692DA4B692DA4B692DA4B6D25B496D25B496DA4B692DB496D25B692DA496D24B692DB496DA4B6D25B6925B492DA496DA496D24B6D24B6D24B6D24B6D24B6D24B6D24B6D2496DA496DB492DB6924B6D2496DB492DB6924B6DA4925B6D2496DB6924B6DB4924B6DB4924B6DB4924B6DB692492DB6DA4924B6DB6D24924B6DB6DA492492DB6DB6D2492492DB6DB6DB4924924924B6DB6DB6DB6DA4924924924924924924B6DB6DB6DB6DB6DB6DB6DB6DB6DB6DB6DB24924924924924924924DB6DB6DB6DB64924924926DB6DB6DB24924926DB6DB6492;
defparam ram_block1a130.mem_init0 = 2048'h4936DB6D924924DB6DB24924DB6D924936DB64924DB6D9249B6DB24936DB24936DB249B6D9249B6C926DB64936D924DB64936D924DB649B6C926D924DB649B6C936C926D926DB24DB24DB24DB649B649B649B649B24DB24DB24D926D926C936C93649B24DB26D936C9B64DB26D936C9B64D926C9B64D926C9B64D936C9B26D9364D926C9B26C9B64D9364D9364D9364D926C9B26CD9364D9364D9364D9366C9B26C9B364D9366C9B26CD9366C9B264D9326C99366C9B364C9B264D9B264D9B264C9B364C99366C993264D9B364C993264D9B366CD9B366CD9B366CD9B366CD9B366CD993264C99B366CC993266CD993266CD993366CC99B3664CD9933664CD99;

cycloneive_ram_block ram_block1a22(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a22_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a22.clk0_core_clock_enable = "ena0";
defparam ram_block1a22.clk0_input_clock_enable = "ena0";
defparam ram_block1a22.clk0_output_clock_enable = "ena0";
defparam ram_block1a22.data_interleave_offset_in_bits = 1;
defparam ram_block1a22.data_interleave_width_in_bits = 1;
defparam ram_block1a22.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a22.init_file_layout = "port_a";
defparam ram_block1a22.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a22.operation_mode = "rom";
defparam ram_block1a22.port_a_address_clear = "none";
defparam ram_block1a22.port_a_address_width = 13;
defparam ram_block1a22.port_a_data_out_clear = "none";
defparam ram_block1a22.port_a_data_out_clock = "clock0";
defparam ram_block1a22.port_a_data_width = 1;
defparam ram_block1a22.port_a_first_address = 8192;
defparam ram_block1a22.port_a_first_bit_number = 4;
defparam ram_block1a22.port_a_last_address = 16383;
defparam ram_block1a22.port_a_logical_ram_depth = 65536;
defparam ram_block1a22.port_a_logical_ram_width = 18;
defparam ram_block1a22.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a22.ram_block_type = "auto";
defparam ram_block1a22.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000FFFFFFFFFFFFF00000000000FFFFFFFFFE000000003FFFFFFFC0000000FFFFFFF0000003FFFFFE000001FFFFFC000007FFFFC00001FFFFE00003FFFF80000FFFFC0000FFFF80003FFFC0003FFFC0003FFF8000FFFE0007FFF0003FFF0007FFE000FFF8003FFE000FFF0007FF8007FF8007FF800FFF001FFC007FF001FFC007FF003FF801FFC00FFC00FFC00FFC00FF801FF803FF007FC00FF803FE00FF803FE00FF807FC01FE00FF007F803FC03FC01FE01FE01FE01FE01FE03FC03FC07F807F00FE01FC03F80FF01FC03F80FE03F80FE01FC07E03F80FE03F81FC07F03F80FC07F03F8;
defparam ram_block1a22.mem_init2 = 2048'h1FC0FE07E03F01F81FC0FC0FE07E07E03F03F03F03F03F03F03F03F03F03E07E07E0FC0FC1F81F03F07E07C0F81F83F07E0FC1F83E07C0F83F07C0F83F07C1F83E0FC1F07C1F83E0F83E0F81F07C1F07C1F07C1E0F83E0F83E0F07C1F0783E0F87C1F0F83C1F0783E1F0783C1F0F83C1E0F0783E1F0F87C3C1E0F0783C3E1F0F0783C3E1E0F0F8783C3C1E1F0F0F878783C3C3E1E1E1F0F0F0F07878787878783C3C3C3C3C3C3C3C3C3C3C3C3C787878787878F0F0F0F1E1E1E3C3C3C787870F0E1E1C3C387870F0E1E3C3C7870F1E1C3C78F0E1E3C7870E1E3C7870E1C3C78F1E3C78F1E3C78F1E3C78F1E3C70E1C3871E3C70E1C78F1C3871E3871E3871E38;
defparam ram_block1a22.mem_init1 = 2048'h71E3871E3871C38F1C78E3C71E38F1C78E3C71C38E3C71C38E3C71C78E38E1C71C78E38E3C71C71C38E38E38F1C71C71C71C70E38E38E38E38E38E38E38E38E38E38E38E71C71C71C71C738E38E38E71C71C738E38E71C71CE38E31C71CE38E71C738E39C718E39C718E39C738E31C638C718E39C638C718E31C639C738C718E718E31CE31CE31CE31CE31CE31CE31CE718E718C738C639CE31CE718C739CE318E739C6318E739CE318C639CE739CE318C6318C6318C6318E7318C6318C6318C6318CE739CE7318C6319CE7398C6339CE6318CE7318C67398C67398C67318CE7319CE6339CC67319CE63398CE63398C673198CE63398CE63399CC673198CE673;
defparam ram_block1a22.mem_init0 = 2048'h19CCE633198CE673198CC663319CCE673399CCC6633198CC66733998CC66733198CCE66333998CCE66333199CCCE663331998CCC6663333999CCCC66633331999CCCC6666333319998CCCCE66663333399999CCCCCC6666673333331999999CCCCCCCC66666666733333333339999999999999CCCCCCCCCCCCCCCCCCCCCCCCCCCCE6666666664CCCCCCCCCCCCCCCCCCCCCCCCCCCD9999999999999B3333333332666666664CCCCCCC9999999B333332666666CCCCCD999993333366666CCCCD9999333326666CCCD999933326664CCC99993336666CCC999B332666CCD999333666CCC999332664CC999332664CC99B33666CC99933666CC99B32664CD993366;

cycloneive_ram_block ram_block1a40(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a40_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a40.clk0_core_clock_enable = "ena0";
defparam ram_block1a40.clk0_input_clock_enable = "ena0";
defparam ram_block1a40.clk0_output_clock_enable = "ena0";
defparam ram_block1a40.data_interleave_offset_in_bits = 1;
defparam ram_block1a40.data_interleave_width_in_bits = 1;
defparam ram_block1a40.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a40.init_file_layout = "port_a";
defparam ram_block1a40.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a40.operation_mode = "rom";
defparam ram_block1a40.port_a_address_clear = "none";
defparam ram_block1a40.port_a_address_width = 13;
defparam ram_block1a40.port_a_data_out_clear = "none";
defparam ram_block1a40.port_a_data_out_clock = "clock0";
defparam ram_block1a40.port_a_data_width = 1;
defparam ram_block1a40.port_a_first_address = 16384;
defparam ram_block1a40.port_a_first_bit_number = 4;
defparam ram_block1a40.port_a_last_address = 24575;
defparam ram_block1a40.port_a_logical_ram_depth = 65536;
defparam ram_block1a40.port_a_logical_ram_width = 18;
defparam ram_block1a40.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a40.ram_block_type = "auto";
defparam ram_block1a40.mem_init3 = 2048'hCD9933664CC99B3266CCD9933266CCD99B32664CC999332664CC999332666CCD999333666CCC999B332666CCCD99933326664CCC999933336666CCCC99999333366666CCCCD9999933333666666CCCCCC999999B333333266666664CCCCCCCC9999999999B333333333333366666666666666666666666666664CCCCCCCCCE666666666666666666666666666733333333333339999999999CCCCCCCCC666666673333331999999CCCCCC6666673333399998CCCCE6666333319998CCCC666733319998CCC66673339998CCC6663331998CCE667331998CCE66333998CCE6633199CCC6633399CCC6633198CC6673399CCE673198CC663319CCE633198CE6731;
defparam ram_block1a40.mem_init2 = 2048'h9CCE63319CC673398CE63398CE63319CC63398CE63398CE7319CC67398CE7319CE6319CC6339CC6339CC6319CE6318CE7398C6339CE7318C6319CE739CE6318C6318C6318C6319CE318C6318C6318C6318E739CE738C6318E739CE318C739CE318E739C631CE718E738C639C631CE31CE718E718E718E718E718E718E718E31CE31C639C738C718E31C638C738E31C638C718E39C738E31C738E31C738E39C71CE38E71C718E38E71C71CE38E39C71C71CE38E38E39C71C71C71C71CE38E38E38E38E38E38E38E38E38E38E38E1C71C71C71C71E38E38E3871C71C78E38E3C71C70E38E3C71C78E3871C78E3871C78E3C71E38F1C78E3C71E3871C38F1C38F1C;
defparam ram_block1a40.mem_init1 = 2048'h38F1C38F1C38F1C3871E3C70E1C78F1C3870E1C78F1E3C78F1E3C78F1E3C78F1E3C7870E1C3C78F0E1C3C78F0E1E3C7870F1E1C3C7878F0E1E1C3C387870F0E1E1C3C3C787878F0F0F1E1E1E1E3C3C3C3C3C3C787878787878787878787878783C3C3C3C3C3C1E1E1E1F0F0F0F878783C3C3E1E1F0F078783C3E1E0F0F8783C1E1F0F8783C1E0F0787C3E1F0F83C1E0F0783E1F0783C1F0F83C1F0783E1F07C3E0F83C1F07C1E0F83E0F83E0F07C1F07C1F07C1F03E0F83E0F83F07C1F07E0F83F07C1F83E07C1F83E07C0F83F07E0FC1F83F03E07C0FC1F81F03F07E07E0FC0FC0F81F81F81F81F81F81F81F81F81F80FC0FC0FE07E07F03F01F80FC0FE07F0;
defparam ram_block1a40.mem_init0 = 2048'h3F81FC07E03F81FC07F03F80FE03F80FC07F00FE03F80FE03F807F01FE03F807F00FE01FC03FC07F807F80FF00FF00FF00FF00FF007F807F803FC01FE00FF007FC03FE00FF803FE00FF803FE007FC01FF803FF003FE007FE007FE007FE007FF003FF801FFC007FF001FFC007FF001FFE003FFC003FFC003FFC001FFE000FFF8003FFE000FFFC001FFF8001FFFC000FFFE0003FFF80007FFF80007FFF80003FFFE00007FFFE00003FFFF80000FFFFF000007FFFFC000007FFFFF000000FFFFFF8000001FFFFFFE00000007FFFFFFF800000000FFFFFFFFFE00000000001FFFFFFFFFFFFE00000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a4(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a4_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a4.clk0_core_clock_enable = "ena0";
defparam ram_block1a4.clk0_input_clock_enable = "ena0";
defparam ram_block1a4.clk0_output_clock_enable = "ena0";
defparam ram_block1a4.data_interleave_offset_in_bits = 1;
defparam ram_block1a4.data_interleave_width_in_bits = 1;
defparam ram_block1a4.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a4.init_file_layout = "port_a";
defparam ram_block1a4.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a4.operation_mode = "rom";
defparam ram_block1a4.port_a_address_clear = "none";
defparam ram_block1a4.port_a_address_width = 13;
defparam ram_block1a4.port_a_data_out_clear = "none";
defparam ram_block1a4.port_a_data_out_clock = "clock0";
defparam ram_block1a4.port_a_data_width = 1;
defparam ram_block1a4.port_a_first_address = 0;
defparam ram_block1a4.port_a_first_bit_number = 4;
defparam ram_block1a4.port_a_last_address = 8191;
defparam ram_block1a4.port_a_logical_ram_depth = 65536;
defparam ram_block1a4.port_a_logical_ram_width = 18;
defparam ram_block1a4.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a4.ram_block_type = "auto";
defparam ram_block1a4.mem_init3 = 2048'h4CD9933664CD9B3266CC9933664C99B3664C99B366CC993366CD9B3264C993264CD9B366CD9B366CD9B366CD93264C99326CD9B364C99326CD9B264D9B364C9B364C99366C9B364C9B364D9B264D9326C99364C9B26CD9364C9B26C99364D9364C9B26C9B26C9B364D9364D9364D9364D9364D936C9B26C9B26C9B64D9364DB26C9B24D93649B26D93649B26D93649B26D936C9B64DB26D936C9B649B24D926D936C936C9B649B64DB24DB24DB24DB24DB24DB24DB24DB249B649B64936C936D926DB24DB64936C926DB24DB64936D924DB64936DB249B6C924DB64926DB24936DB24936DB24926DB64924DB6C9249B6DB64924DB6DB24924DB6DB249249B6DB;
defparam ram_block1a4.mem_init2 = 2048'h6C924924DB6DB6D924924926DB6DB6DB64924924924936DB6DB6DB6DB6DB6C92492492492492492492492492492492492492492492DB6DB6DB6DB6DB6DA492492492492DB6DB6DB6924924925B6DB6DA4924925B6DB6D24924B6DB6D24924B6DB692492DB6DA4924B6DB4924B6DB4924B6DB4924B6DA4925B6D2496DB4925B6D2496DB4925B6D24B6DA496DB492DB6925B6925B6D24B6D24B6D24B6D24B6D24B6D25B6925B692DB492DA496DA4B6D25B692DB496DA4B6925B496DA4B6925B496D24B692DA4B6925B496D25B496D25B496D25B496D25B496D25B496D2DA4B692DA5B496D25A4B692DA5B49692DA4B496D2DA4B496D2DA4B49692DA5B4B692D25A;
defparam ram_block1a4.mem_init1 = 2048'h4B49692D25A4B49692D25A4B49692D25A4B4B696D2D25A4B4B696D2D25A5B4B69692D2DA5A4B4B49696D2D25A5A4B4B4969692D2D25A5A4B4B4969696D2D2D25A5A5B4B4B4B6969696D2D2D2D25A5A5A5B4B4B4B4B4969696969692D2D2D2D2D2D2DA5A5A5A5A5A5A5A5A5A5B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B4B5A5A5A5A5A5A5A5A5A5A5A52D2D2D2D2D2D2D69696969696B4B4B4B4B4A5A5A5A5AD2D2D2D296969694B4B4B4A5A5A5A52D2D2D69696B4B4B4A5A5A52D2D2969694B4B4A5A5A52D2D69694B4B4A5A5AD2D29696B4B4A5A5AD2D29694B4B5A5AD2D29694B4B5A5AD2D29694B4A5A52D29694B4B5A5AD2D696B4A5A52D29694;
defparam ram_block1a4.mem_init0 = 2048'hB4A5A52D296B4B5A5AD29694B4A5AD2D694B4A5AD2D694B4A5AD2D694B4A5AD29694B5A52D296B4A5A52D694B5A5AD296B4A5AD2D694B5A52D694B5A5AD296B4A5AD296B4A5AD296B4A5AD296B4A5AD296B4A5AD296B4A5AD294B5A52D694B5A52D6B4A5AD296B4A5AD694B5A52D6B4A5AD296B5A52D694A5AD296B5A52D694A5AD296B5A52D6B4A5AD694B5A5296B4A52D694A5AD294B5A5296B5A52D6B4A5AD694B5AD294B5A5296B5A52D6B4A52D694A5AD694B5AD294B5AD296B5A5296B5A52D6B4A52D6B4A52D694A5AD694A5AD694A5AD294B5AD294B5AD294B5AD294B5A5296B5A5296B5A5296B5A5296B5A5296B5A5296B4A52D6B4A52D6B4A52D6B4;

cycloneive_ram_block ram_block1a58(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a58_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a58.clk0_core_clock_enable = "ena0";
defparam ram_block1a58.clk0_input_clock_enable = "ena0";
defparam ram_block1a58.clk0_output_clock_enable = "ena0";
defparam ram_block1a58.data_interleave_offset_in_bits = 1;
defparam ram_block1a58.data_interleave_width_in_bits = 1;
defparam ram_block1a58.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a58.init_file_layout = "port_a";
defparam ram_block1a58.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a58.operation_mode = "rom";
defparam ram_block1a58.port_a_address_clear = "none";
defparam ram_block1a58.port_a_address_width = 13;
defparam ram_block1a58.port_a_data_out_clear = "none";
defparam ram_block1a58.port_a_data_out_clock = "clock0";
defparam ram_block1a58.port_a_data_width = 1;
defparam ram_block1a58.port_a_first_address = 24576;
defparam ram_block1a58.port_a_first_bit_number = 4;
defparam ram_block1a58.port_a_last_address = 32767;
defparam ram_block1a58.port_a_logical_ram_depth = 65536;
defparam ram_block1a58.port_a_logical_ram_width = 18;
defparam ram_block1a58.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a58.ram_block_type = "auto";
defparam ram_block1a58.mem_init3 = 2048'h5AD694A5AD694A5AD694A5AD294B5AD294B5AD294B5AD294B5AD294B5AD294B5A5296B5A5296B5A5296B5A5296B4A52D6B4A52D6B4A52D694A5AD694A5AD694B5AD294B5AD296B5A5296B5A52D6B4A52D694A5AD694B5AD294B5A5296B5A52D6B4A5AD694B5AD294B5A5296B4A52D694A5AD294B5A52D6B4A5AD694B5AD296B4A52D694B5AD296B4A52D694B5AD296B4A5AD694B5A52D6B4A5AD296B4A5AD694B5A52D694B5A5296B4A5AD296B4A5AD296B4A5AD296B4A5AD296B4A5AD296B4A5AD296B4B5A52D694B5A52D696B4A5AD296B4B5A52D694B4A5AD29694B5A52D296B4A5A52D696B4A5A52D696B4A5A52D696B4A5A52D296B4B5A5AD29694B4A5A;
defparam ram_block1a58.mem_init2 = 2048'h52D29694B4A5AD2D696B4B5A5A52D29694B4A5A52D29696B4B5A5A52D29696B4B5A5A52D29696B4B4A5A5AD2D29696B4B4A5A5A52D2D69694B4B4A5A5A52D2D2969694B4B4A5A5A5AD2D2D6969694B4B4B4A5A5A5A52D2D2D296969696B4B4B4B4A5A5A5A5A5AD2D2D2D2D2D696969696969694B4B4B4B4B4B4B4B4B4B4B5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5B4B4B4B4B4B4B4B4B4B4B696969696969692D2D2D2D2D25A5A5A5A5B4B4B4B496969696D2D2D2DA5A5A5B4B4B4969696D2D2D25A5A4B4B4969692D2D25A5A4B4B49696D2D25A5A4B4B69692D2DA5B4B49696D2DA5A4B49696D2DA5A4B49692D25A4B49692D25A4B49692D25A4;
defparam ram_block1a58.mem_init1 = 2048'hB49692DA5B4B692D25A4B696D25A4B696D25A4B692D25B4B692DA4B496D25B4B692DA4B696D25B496D25B496D25B496D25B496D25B496D25B492DA4B692DA496D25B492DA4B6D25B492DA4B6D25B692DB496DA4B6D24B6925B692DB492DB496DA496DA496DA496DA496DA496DB492DB492DB6925B6D24B6DA496DB4925B6D2496DB4925B6D2496DB4924B6DA4925B6DA4925B6DA4925B6DA4924B6DB692492DB6DA492496DB6DA492496DB6DB4924924B6DB6DB492492492DB6DB6DB6924924924924B6DB6DB6DB6DB6DB6924924924924924924924924924924924924924924926DB6DB6DB6DB6DB6D924924924924DB6DB6DB6C924924936DB6DB64924926D;
defparam ram_block1a58.mem_init0 = 2048'hB6DB249249B6DB649249B6DB64924DB6DB24926DB64924DB6C9249B6D9249B6D9249B6C924DB64926DB249B6D924DB64936D924DB649B6C926D924DB649B6C936D926D924DB24DB249B649B649B649B649B649B649B649B64DB24DB26D926D936C93649B24DB26D936C9B64DB26D936C9B24D936C9B24D936C9B24D93649B26C9B64D9364DB26C9B26C9B26D9364D9364D9364D9364D9364D9B26C9B26C9B264D9364D9326C9B264D9366C9B264D9326C99364C9B364D9B264D9B26CD93264D9B264D9B364C9B366C993264D9B366C993264C99366CD9B366CD9B366CD9B3664C993264C99B366CD993266CD9B3264CD9B3264CD993266CC99B3664CD9933664;

cycloneive_ram_block ram_block1a113(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a113_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a113.clk0_core_clock_enable = "ena0";
defparam ram_block1a113.clk0_input_clock_enable = "ena0";
defparam ram_block1a113.clk0_output_clock_enable = "ena0";
defparam ram_block1a113.data_interleave_offset_in_bits = 1;
defparam ram_block1a113.data_interleave_width_in_bits = 1;
defparam ram_block1a113.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a113.init_file_layout = "port_a";
defparam ram_block1a113.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a113.operation_mode = "rom";
defparam ram_block1a113.port_a_address_clear = "none";
defparam ram_block1a113.port_a_address_width = 13;
defparam ram_block1a113.port_a_data_out_clear = "none";
defparam ram_block1a113.port_a_data_out_clock = "clock0";
defparam ram_block1a113.port_a_data_width = 1;
defparam ram_block1a113.port_a_first_address = 49152;
defparam ram_block1a113.port_a_first_bit_number = 5;
defparam ram_block1a113.port_a_last_address = 57343;
defparam ram_block1a113.port_a_logical_ram_depth = 65536;
defparam ram_block1a113.port_a_logical_ram_width = 18;
defparam ram_block1a113.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a113.ram_block_type = "auto";
defparam ram_block1a113.mem_init3 = 2048'hC3878F1E1C3878F0E1C3C7870E1E3C3878F0E1E3C3870F1E1E3C3878F0E1E3C387870F1E1C3C3878F0F1E1C3C387870F0E1E1C3C387870F0E1E1C3C3C787870F0E1E1E3C3C3C787870F0F0E1E1E1C3C3C38787878F0F0F0E1E1E1E1C3C3C3C3C787878787870F0F0F0F0F0F0F1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E0F0F0F0F0F0F0F078787878783C3C3C3C3E1E1E1E0F0F0F0F878787C3C3C3E1E1E0F0F0F878783C3C3E1E1F0F0F8787C3C3E1E1F0F0F8783C3E1E1F0F0787C3C1E1F0F0787C3C1E1F0F8783C3E1F0F0783C3E1F0F0783C1E1F0F87C3E1F0F0783C1E0F0783C1E0F0783C1F0F87C3E1F0F83C1E0F0;
defparam ram_block1a113.mem_init2 = 2048'h7C3E1F0783C1F0F83C1E0F87C1E0F87C1E0F87C1E0F83C1F0F83E1F0783E0F07C1F0F83E1F07C1E0F83E1F07C1F0F83E0F83C1F07C1F0783E0F83E0F83E0F87C1F07C1F07C1F07C1F07C1F07C1F07C0F83E0F83E0F83E0FC1F07C1F07E0F83E0FC1F07C1F83E0F81F07C0F83E07C1F03E0FC1F07E0F81F07E0F81F03E0FC1F03E07C1F83F07E0FC1F03E07C0F81F03E07E0FC1F83F07E07C0F81F83F07E07C0FC1F81F03F03E07E0FC0FC1F81F81F03F03F07E07E07E0FC0FC0FC0FC0FC1F81F81F81F81F81F81F81F81F80FC0FC0FC0FC0FC07E07E07E03F03F03F81F81FC0FC07E07E03F03F81FC0FC07E03F03F81FC0FE07F03F81FC0FE07F01F80FC07F03;
defparam ram_block1a113.mem_init1 = 2048'hF80FC07F03F80FE07F01FC07E03F80FE03F01FC07F01FC07F01FC07F01FC07F01FC07F00FE03F80FE01FC07F01FE03F807F01FE03F807F00FE01FC07F807F00FE01FC03FC07F80FF00FF01FE01FE03FC03FC03FC03F807F807F807F807FC03FC03FC03FC01FE01FE00FF00FF007F803FC01FE01FF007F803FC01FF00FF803FC01FF007FC03FE00FF803FE00FF803FE007FC01FF007FE00FF801FF003FE00FFC00FF801FF003FF007FE007FE007FE007FE007FE007FE007FE003FF003FF801FF800FFC007FF003FF800FFE007FF001FFC007FF001FFE003FF8007FF001FFE001FFC003FFC007FF8007FF8007FF8003FFC003FFC001FFE000FFF8007FFC001FFF0;
defparam ram_block1a113.mem_init0 = 2048'h007FFC000FFF8003FFF0003FFE0007FFE0007FFE0007FFF0003FFF0001FFFC0007FFF0001FFFC0007FFF80007FFF0000FFFF80007FFF80003FFFE0000FFFF80003FFFF00007FFFE00007FFFF00003FFFF80000FFFFF00000FFFFF00000FFFFF000007FFFFE00000FFFFFC000007FFFFF000001FFFFFE000001FFFFFF0000003FFFFFF0000001FFFFFFC0000001FFFFFFF00000003FFFFFFFC00000003FFFFFFFF800000000FFFFFFFFFC0000000007FFFFFFFFFF000000000007FFFFFFFFFFFC00000000000007FFFFFFFFFFFFFFE0000000000000000007FFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a95(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a95_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a95.clk0_core_clock_enable = "ena0";
defparam ram_block1a95.clk0_input_clock_enable = "ena0";
defparam ram_block1a95.clk0_output_clock_enable = "ena0";
defparam ram_block1a95.data_interleave_offset_in_bits = 1;
defparam ram_block1a95.data_interleave_width_in_bits = 1;
defparam ram_block1a95.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a95.init_file_layout = "port_a";
defparam ram_block1a95.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a95.operation_mode = "rom";
defparam ram_block1a95.port_a_address_clear = "none";
defparam ram_block1a95.port_a_address_width = 13;
defparam ram_block1a95.port_a_data_out_clear = "none";
defparam ram_block1a95.port_a_data_out_clock = "clock0";
defparam ram_block1a95.port_a_data_width = 1;
defparam ram_block1a95.port_a_first_address = 40960;
defparam ram_block1a95.port_a_first_bit_number = 5;
defparam ram_block1a95.port_a_last_address = 49151;
defparam ram_block1a95.port_a_logical_ram_depth = 65536;
defparam ram_block1a95.port_a_logical_ram_width = 18;
defparam ram_block1a95.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a95.ram_block_type = "auto";
defparam ram_block1a95.mem_init3 = 2048'h000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFC000000000000000000FFFFFFFFFFFFFFFC00000000000007FFFFFFFFFFFC00000000001FFFFFFFFFFC0000000007FFFFFFFFE000000003FFFFFFFF800000007FFFFFFF80000001FFFFFFF00000007FFFFFF0000001FFFFFF8000001FFFFFF000000FFFFFF000001FFFFFC000007FFFFE00000FFFFFC00001FFFFE00001FFFFE00001FFFFE00003FFFF80001FFFFC0000FFFFC0001FFFF80003FFFE0000FFFF80003FFFC0003FFFE0001FFFC0003FFFC0007FFF0001FFFC0007FFF0001FFF8001FFFC000FFFC000FFFC000FFF8001FFF8003FFE0007FFC00;
defparam ram_block1a95.mem_init2 = 2048'h1FFF0007FFC003FFE000FFF0007FF8007FF8003FFC003FFC003FFC007FF8007FF000FFF001FFC003FF800FFF001FFC007FF001FFC00FFE003FF801FFC007FE003FF003FF801FF800FFC00FFC00FFC00FFC00FFC00FFC00FFC01FF801FF003FE007FE00FF801FF003FE00FFC01FF007FC00FF803FE00FF803FE00FF807FC01FF007F803FE01FF007F803FC01FF00FF007F803FC01FE01FE00FF00FF007F807F807F807FC03FC03FC03FC03F807F807F807F80FF00FF01FE01FE03FC07F807F00FE01FC03FC07F00FE01FC03F80FF01FC03F80FF01FC07F00FE03F80FE01FC07F01FC07F01FC07F01FC07F01FC07F01F80FE03F80FC07F01FC0FE03F81FC07E03F;
defparam ram_block1a95.mem_init1 = 2048'h81FC07E03F01FC0FE07F03F81FC0FE07F03F81F80FC07E07F03F81F80FC0FC07E07F03F03F81F81F80FC0FC0FC07E07E07E07E07E03F03F03F03F03F03F03F03F03F07E07E07E07E07E0FC0FC0FC1F81F81F03F03F07E07E0FC0F81F81F03F07E07C0FC1F83F03E07C0FC1F83F07E0FC0F81F03E07C0F81F07E0FC1F83F07C0F81F07E0F81F03E0FC1F03E0FC1F07E0F81F07C0F83E07C1F03E0F83F07C1F07E0F83E0FC1F07C1F07E0F83E0F83E0F83E07C1F07C1F07C1F07C1F07C1F07C1F07C3E0F83E0F83E0F83C1F07C1F0783E0F83E1F07C1F0F83E0F07C1F0F83E1F07C1E0F83C1F0F83E1F0783E0F07C3E0F07C3E0F07C3E0F0783E1F0783C1F0F87C;
defparam ram_block1a95.mem_init0 = 2048'h1E0F0783E1F0F87C3E1F0783C1E0F0783C1E0F0783C1E1F0F87C3E1F0F0783C1E1F0F8783C1E1F0F8783C3E1F0F0787C3C1E1F0F0787C3C1E1F0F0F8783C3E1E1F0F0F8787C3C3E1E1F0F0F878783C3C3E1E1E0F0F0F878787C3C3C3E1E1E1E0F0F0F0F8787878783C3C3C3C3C1E1E1E1E1E1E1E0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F1E1E1E1E1E1E1E1C3C3C3C3C3C7878787870F0F0F0E1E1E1E3C3C3C38787870F0F0E1E1E1C3C3C787878F0F0E1E1C3C3C787870F0E1E1C3C387870F0E1E1C3C387870F1E1E3C387870F1E1C3C3878F0E1E3C3878F0F1E1C3878F0E1E3C3878F0E1C3C7870E1E3C3870F1E3C387;

cycloneive_ram_block ram_block1a77(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a77_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a77.clk0_core_clock_enable = "ena0";
defparam ram_block1a77.clk0_input_clock_enable = "ena0";
defparam ram_block1a77.clk0_output_clock_enable = "ena0";
defparam ram_block1a77.data_interleave_offset_in_bits = 1;
defparam ram_block1a77.data_interleave_width_in_bits = 1;
defparam ram_block1a77.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a77.init_file_layout = "port_a";
defparam ram_block1a77.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a77.operation_mode = "rom";
defparam ram_block1a77.port_a_address_clear = "none";
defparam ram_block1a77.port_a_address_width = 13;
defparam ram_block1a77.port_a_data_out_clear = "none";
defparam ram_block1a77.port_a_data_out_clock = "clock0";
defparam ram_block1a77.port_a_data_width = 1;
defparam ram_block1a77.port_a_first_address = 32768;
defparam ram_block1a77.port_a_first_bit_number = 5;
defparam ram_block1a77.port_a_last_address = 40959;
defparam ram_block1a77.port_a_logical_ram_depth = 65536;
defparam ram_block1a77.port_a_logical_ram_width = 18;
defparam ram_block1a77.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a77.ram_block_type = "auto";
defparam ram_block1a77.mem_init3 = 2048'h0F1E3C3870F1E3C3870E1E3C78F0E1C3870F1E3C78F1E1C3870E1C3870F1E3C78F1E3C78F1E3C78F1E3C78F1E3C78F1C3870E1C3870E3C78F1E3C70E1C3871E3C78F1C3870E3C78E1C3871E3C70E1C78F1C3871E3C70E1C78E1C38F1C3871E3871E3C70E3C70E3C70E3C70E1C78E1C70E3C70E3C70E3C70E3C71E3871E38F1C38F1C78E1C70E3C71E38F1C38E1C70E3C71E38F1C78E3C71E38F1C78E3871C38E1C71E38F1C70E3871C78E3871C38E3C71C38E3C71C78E3871C78E38F1C71E38E1C71C38E38F1C71E38E3C71C70E38E3C71C70E38E3C71C71E38E38F1C71C78E38E3871C71C78E38E38F1C71C71C38E38E38F1C71C71C78E38E38E38F1C71C71C;
defparam ram_block1a77.mem_init2 = 2048'h71C38E38E38E38E3871C71C71C71C71C71C38E38E38E38E38E38E38E38E38E38E3871C71C71C71C71C71C71C71C71C71C71C638E38E38E38E38E38E38E38E38E38E39C71C71C71C71C71C718E38E38E38E38E71C71C71C71C638E38E38E39C71C71C718E38E38E39C71C71C638E38E39C71C71C638E38E31C71C738E38E39C71C718E38E31C71C638E38E71C71CE38E31C71C638E39C71C638E39C71C638E39C71C638E31C71CE38E71C738E38C71C638E31C718E39C71CE38E71C738E31C718E39C71CE38C71CE38C71C638E71C638E71C638E71C638E71CE38C71CE38C718E39C718E31C738E71C638C71CE39C718E31C638E71CE39C738E31C638C718E31C;
defparam ram_block1a77.mem_init1 = 2048'h638E71CE39C738E71CE39C738C718E31C638C718E31C639C738E71CE31C638C718E71CE39C638C738E71CE31C639C738C718E71CE31C639C738C718E71CE31CE39C639C738C718E718E71CE31CE39C639C638C738C738E718E718E718E31CE31CE31CE31C639C639C639C639C639C639C638C738C738C738C738C738C639C639C639C639C639C639C639C631CE31CE31CE31CE718E718E718C738C738C639C639C631CE31CE318E718E738C738C639C639CE31CE718E718C738C639C631CE318E718C738C639C631CE318E738C739C639CE318E718C739C639CE318E738C739C631CE718E738C639CE318E738C639CE31CE718C739C631CE718C739C6318E738;
defparam ram_block1a77.mem_init0 = 2048'hC639CE318E738C639CE318C739C631CE718C639CE318E739C631CE718C639CE318C739C6318E738C631CE718C639CE718C739CE318C739C6318E739C6318E739C6318E738C631CE738C631CE738C631CE739C6318E739C6318E739C6318C739CE318C739CE318C639CE718C631CE738C6318E739C6318C739CE718C639CE738C6318E739CE318C739CE718C631CE739C6318C739CE718C631CE739C6318C739CE738C6318E739CE318C639CE739C6318C739CE718C6318E739CE318C631CE739C6318C639CE739C6318C739CE738C6318C739CE718C6318E739CE718C631CE739CE318C631CE739CE318C631CE739CE318C639CE739C6318C639CE739C6318C6;

cycloneive_ram_block ram_block1a131(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a131_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a131.clk0_core_clock_enable = "ena0";
defparam ram_block1a131.clk0_input_clock_enable = "ena0";
defparam ram_block1a131.clk0_output_clock_enable = "ena0";
defparam ram_block1a131.data_interleave_offset_in_bits = 1;
defparam ram_block1a131.data_interleave_width_in_bits = 1;
defparam ram_block1a131.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a131.init_file_layout = "port_a";
defparam ram_block1a131.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a131.operation_mode = "rom";
defparam ram_block1a131.port_a_address_clear = "none";
defparam ram_block1a131.port_a_address_width = 13;
defparam ram_block1a131.port_a_data_out_clear = "none";
defparam ram_block1a131.port_a_data_out_clock = "clock0";
defparam ram_block1a131.port_a_data_width = 1;
defparam ram_block1a131.port_a_first_address = 57344;
defparam ram_block1a131.port_a_first_bit_number = 5;
defparam ram_block1a131.port_a_last_address = 65535;
defparam ram_block1a131.port_a_logical_ram_depth = 65536;
defparam ram_block1a131.port_a_logical_ram_width = 18;
defparam ram_block1a131.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a131.ram_block_type = "auto";
defparam ram_block1a131.mem_init3 = 2048'hC6318C739CE738C6318C739CE738C6318E739CE718C6318E739CE718C6318E739CE718C631CE739CE318C631CE739C6318C639CE739C6318C739CE738C6318C739CE718C6318E739CE318C631CE739C6318C739CE738C6318E739CE318C639CE739C6318C739CE718C631CE739C6318C739CE718C631CE739C6318E739CE318C639CE738C631CE739C6318C739CE318C639CE718C631CE738C6318E739C6318E739C6318C739CE318C739CE318C739CE718C639CE718C639CE718C639CE318C739CE318C739CE318C739C6318E739C631CE738C631CE718C639CE318C739C6318E738C631CE718C739CE318E738C631CE718C739C6318E738C639CE318E738C6;
defparam ram_block1a131.mem_init2 = 2048'h39CE318C739C631CE718C739C631CE718E738C639CE318E738C639CE31CE718C739C639CE318E738C739C631CE318E738C739C639CE318E718C738C639C631CE318E718C738C639C631CE31CE718E738C738C639C639CE31CE318E718E718C738C738C639C639C631CE31CE31CE718E718E718E718C738C738C738C738C738C738C738C639C639C639C639C639C638C738C738C738C738C738C738C718E718E718E718E31CE31CE31CE39C639C638C738C738E718E71CE31CE31C639C738C738E718E71CE31C639C738C718E71CE31C639C738C718E71CE39C638C738E71CE31C638C718E71CE39C738C718E31C638C718E31C639C738E71CE39C738E71CE38C;
defparam ram_block1a131.mem_init1 = 2048'h718E31C638C718E39C738E71CE38C718E31C738E71C638C71CE39C718E31C738E31C638E71C638E71CE38C71CE38C71CE38C71CE38C71C638E71C638E71C738E31C718E39C71CE38E71C738E31C718E38C71C638E39C71CE38E71C718E38C71C738E38C71C738E38C71C738E38C71C718E38E71C71CE38E38C71C718E38E31C71C738E38E39C71C718E38E38C71C71C738E38E38C71C71C738E38E38E31C71C71C738E38E38E38C71C71C71C71CE38E38E38E38E31C71C71C71C71C71C738E38E38E38E38E38E38E38E38E38E38C71C71C71C71C71C71C71C71C71C71C71C38E38E38E38E38E38E38E38E38E38E3871C71C71C71C71C71C38E38E38E38E3871C;
defparam ram_block1a131.mem_init0 = 2048'h71C71C71E38E38E38E3C71C71C71E38E38E3871C71C71E38E38E3C71C71C38E38E3C71C71E38E38F1C71C78E38E1C71C78E38E1C71C78E38F1C71E38E3871C70E38F1C71E38E3C71C38E3C71C78E3871C78E3871C38E3C71C38E1C71E38F1C70E3871C38E3C71E38F1C78E3C71E38F1C78E1C70E3871E38F1C78E1C70E3C71E3871E38F1C38F1C78E1C78E1C78E1C78E1C70E3C70E1C78E1C78E1C78E1C78F1C38F1C3871E3870E3C70E1C78F1C3871E3C70E1C78F1C3870E3C78E1C3871E3C78F1C3870E1C78F1E3C78E1C3870E1C3871E3C78F1E3C78F1E3C78F1E3C78F1E3C78F1E1C3870E1C3870F1E3C78F1E1C3870E1E3C78F0E1C3878F1E1C3878F1E1;

cycloneive_ram_block ram_block1a23(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a23_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a23.clk0_core_clock_enable = "ena0";
defparam ram_block1a23.clk0_input_clock_enable = "ena0";
defparam ram_block1a23.clk0_output_clock_enable = "ena0";
defparam ram_block1a23.data_interleave_offset_in_bits = 1;
defparam ram_block1a23.data_interleave_width_in_bits = 1;
defparam ram_block1a23.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a23.init_file_layout = "port_a";
defparam ram_block1a23.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a23.operation_mode = "rom";
defparam ram_block1a23.port_a_address_clear = "none";
defparam ram_block1a23.port_a_address_width = 13;
defparam ram_block1a23.port_a_data_out_clear = "none";
defparam ram_block1a23.port_a_data_out_clock = "clock0";
defparam ram_block1a23.port_a_data_width = 1;
defparam ram_block1a23.port_a_first_address = 8192;
defparam ram_block1a23.port_a_first_bit_number = 5;
defparam ram_block1a23.port_a_last_address = 16383;
defparam ram_block1a23.port_a_logical_ram_depth = 65536;
defparam ram_block1a23.port_a_logical_ram_width = 18;
defparam ram_block1a23.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a23.ram_block_type = "auto";
defparam ram_block1a23.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFC000000000000000FFFFFFFFFFFFFC000000000001FFFFFFFFFFF80000000001FFFFFFFFFC000000000FFFFFFFFF000000003FFFFFFFC00000003FFFFFFF00000007FFFFFFC0000007FFFFFF0000003FFFFFF0000007FFFFF8000007FFFFF000001FFFFF800001FFFFF800003FFFFE00000FFFFF00000FFFFF00001FFFFC00007FFFF00003FFFF00003FFFF00007FFFE0000FFFF80003FFFC0001FFFE0001FFFE0001FFFC0003FFF80007FFF0001FFFC000FFFE0003FFF0003FFF0001FFF8003FFF0003FFE0007FFC000FFF8003FF;
defparam ram_block1a23.mem_init2 = 2048'hE000FFF8003FFE001FFF000FFF8007FFC003FFC003FFC003FFC003FFC003FF8007FF000FFE001FFC007FF800FFE003FF800FFE003FF800FFC007FF003FF801FFC00FFE007FE003FF003FF001FF801FF801FF801FF003FF003FF007FE007FC00FF801FF003FE007FC01FF803FE00FFC01FF007FC01FF007FC01FF007FC03FE00FF803FC01FF00FF803FC01FE00FF007F803FC03FE01FE00FF00FF807F807F807FC03FC03FC03FC03FC03FC03FC07F807F807F80FF00FF01FE01FC03FC07F807F00FE01FC03F807F00FE03FC07F80FE01FC07F00FE03F807F01FC07F80FE03F80FE03F80FE03F80FE03F80FE03F80FE03F81FC07F01F80FE03F81FC07E03F81FC0;
defparam ram_block1a23.mem_init1 = 2048'h7E03F81FC07E03F01F80FC07E03F01F80FC07E03F03F81FC0FC07E07F03F01F81F80FC0FC07E07E03F03F03F01F81F81F81F80FC0FC0FC0FC0FC0FC0FC0FC0FC0FC0FC0F81F81F81F81F83F03F03F07E07E07C0FC0F81F81F03F03E07E0FC0F81F83F03E07E0FC1F81F03E07C0FC1F83F07E0FC1F83F07E0FC1F83E07C0F81F07E0FC1F03E0FC1F03E0FC1F03E0FC1F07E0F81F07C0F83E0FC1F07E0F83E0FC1F07C1F83E0F83E0FC1F07C1F07C1F03E0F83E0F83E0F83E0F83E0F83E0F83E0F83E0F07C1F07C1F07C1E0F83E0F83C1F07C1F0F83E0F87C1F0783E0F87C1F0F83E1F07C3E0F87C1E0F83C1F0F83C1F0783E1F0F83C1F0F83C1E0F87C1E0F0783;
defparam ram_block1a23.mem_init0 = 2048'hE1F0F83C1E0F0783E1F0F87C3E1F0F87C3E1F0F87C3E1F0F8783C1E0F0787C3E1F0F0783C3E1F0F0783C3E1E0F0F87C3C1E1F0F0787C3C3E1E0F0F8783C3C1E1E0F0F8787C3C3E1E1F0F0F078783C3C3E1E1E0F0F0F878787C3C3C3E1E1E1E0F0F0F0F8787878783C3C3C3C3C1E1E1E1E1E1E1F0F0F0F0F0F0F0F0F0F0F0F0F0F0F87878787870F0F0F0F0F0F0F0F0F0F0F0F0F0E1E1E1E1E1E1E1C3C3C3C3C3C7878787870F0F0F0E1E1E1E3C3C3C38787870F0F0E1E1E1C3C3C787870F0F1E1E1C3C3C7878F0F1E1E1C3C387870F0E1E1C3C7878F0F1E1C3C3878F0E1E1C3C7870F0E1E3C3878F0E1E3C3878F0E1C3C7870F1E1C3878F0E1C3C7870E1E3C78;

cycloneive_ram_block ram_block1a41(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a41_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a41.clk0_core_clock_enable = "ena0";
defparam ram_block1a41.clk0_input_clock_enable = "ena0";
defparam ram_block1a41.clk0_output_clock_enable = "ena0";
defparam ram_block1a41.data_interleave_offset_in_bits = 1;
defparam ram_block1a41.data_interleave_width_in_bits = 1;
defparam ram_block1a41.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a41.init_file_layout = "port_a";
defparam ram_block1a41.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a41.operation_mode = "rom";
defparam ram_block1a41.port_a_address_clear = "none";
defparam ram_block1a41.port_a_address_width = 13;
defparam ram_block1a41.port_a_data_out_clear = "none";
defparam ram_block1a41.port_a_data_out_clock = "clock0";
defparam ram_block1a41.port_a_data_width = 1;
defparam ram_block1a41.port_a_first_address = 16384;
defparam ram_block1a41.port_a_first_bit_number = 5;
defparam ram_block1a41.port_a_last_address = 24575;
defparam ram_block1a41.port_a_logical_ram_depth = 65536;
defparam ram_block1a41.port_a_logical_ram_width = 18;
defparam ram_block1a41.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a41.ram_block_type = "auto";
defparam ram_block1a41.mem_init3 = 2048'h3C78F0E1C3C7870E1E3C3870F1E1C3C7870E1E3C3878F0E1E3C3878F0E1E1C3C7870F0E1E3C387870F1E1E3C3C7870F0E1E1C3C387870F0F1E1E3C3C787870F0F1E1E1C3C3C787870F0F0E1E1E1C3C3C38787878F0F0F0E1E1E1E1C3C3C3C3C787878787870F0F0F0F0F0F0E1E1E1E1E1E1E1E1E1E1E1E1E1E1C3C3C3C3C3E1E1E1E1E1E1E1E1E1E1E1E1E1E1F0F0F0F0F0F0F078787878783C3C3C3C3E1E1E1E0F0F0F0F878787C3C3C3E1E1E0F0F0F878783C3C1E1E1F0F0F8787C3C3E1E0F0F078783C3E1E0F0F8787C3C1E1F0F0787C3E1E0F0F8783C1E1F0F8783C1E1F0F87C3C1E0F0783C3E1F0F87C3E1F0F87C3E1F0F87C3E1F0F83C1E0F0783E1F0F;
defparam ram_block1a41.mem_init2 = 2048'h83C1E0F07C3E0F0783E1F0783E1F0F83C1F0783E1F0783E0F07C3E0F87C1F0F83E1F07C3E0F83C1F07C3E0F83E1F07C1F0783E0F83E0F07C1F07C1F07C1E0F83E0F83E0F83E0F83E0F83E0F83E0F83E0F81F07C1F07C1F07E0F83E0F83F07C1F07E0F83E0FC1F07E0F83E07C1F03E0FC1F07E0F81F07E0F81F07E0F81F07E0FC1F03E07C0F83F07E0FC1F83F07E0FC1F83F07E07C0F81F03F07E0FC0F81F83F03E07E0FC0F81F81F03F03E07E07C0FC0FC1F81F81F83F03F03F03F03E07E07E07E07E07E07E07E07E07E07E07E03F03F03F03F01F81F81F80FC0FC07E07E03F03F01F81FC0FC07E07F03F81F80FC07E03F01F80FC07E03F01F80FC07F03F80FC;
defparam ram_block1a41.mem_init1 = 2048'h07F03F80FC07F03F80FE03F01FC07F03F80FE03F80FE03F80FE03F80FE03F80FE03F80FE03FC07F01FC03F80FE01FC07F00FE03FC07F80FE01FC03F807F00FE01FC03FC07F807F00FF01FE01FE03FC03FC03FC07F807F807F807F807F807F807FC03FC03FC03FE01FE00FF00FF807F803FC01FE00FF007F803FE01FF007F803FE00FF807FC01FF007FC01FF007FC01FF007FE00FF803FF007FC00FF801FF003FE007FC00FFC01FF801FF801FF003FF003FF003FF001FF801FF800FFC00FFE007FF003FF801FFC007FE003FF800FFE003FF800FFE003FFC007FF000FFE001FFC003FF8007FF8007FF8007FF8007FF8007FFC003FFE001FFF000FFF8003FFE000F;
defparam ram_block1a41.mem_init0 = 2048'hFF8003FFE0007FFC000FFF8001FFF8003FFF0001FFF8001FFF8000FFFE0007FFF0001FFFC0003FFF80007FFF0000FFFF0000FFFF00007FFF80003FFFE0000FFFFC0001FFFF80001FFFF80001FFFFC00007FFFF00001FFFFE00001FFFFE00000FFFFF800003FFFFF000003FFFFF000001FFFFFC000003FFFFFC000001FFFFFF8000001FFFFFFC0000007FFFFFFC0000001FFFFFFF800000007FFFFFFF800000001FFFFFFFFE0000000007FFFFFFFFF00000000003FFFFFFFFFFF0000000000007FFFFFFFFFFFFE0000000000000007FFFFFFFFFFFFFFFFFE000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a5(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a5_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a5.clk0_core_clock_enable = "ena0";
defparam ram_block1a5.clk0_input_clock_enable = "ena0";
defparam ram_block1a5.clk0_output_clock_enable = "ena0";
defparam ram_block1a5.data_interleave_offset_in_bits = 1;
defparam ram_block1a5.data_interleave_width_in_bits = 1;
defparam ram_block1a5.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a5.init_file_layout = "port_a";
defparam ram_block1a5.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a5.operation_mode = "rom";
defparam ram_block1a5.port_a_address_clear = "none";
defparam ram_block1a5.port_a_address_width = 13;
defparam ram_block1a5.port_a_data_out_clear = "none";
defparam ram_block1a5.port_a_data_out_clock = "clock0";
defparam ram_block1a5.port_a_data_width = 1;
defparam ram_block1a5.port_a_first_address = 0;
defparam ram_block1a5.port_a_first_bit_number = 5;
defparam ram_block1a5.port_a_last_address = 8191;
defparam ram_block1a5.port_a_logical_ram_depth = 65536;
defparam ram_block1a5.port_a_logical_ram_width = 18;
defparam ram_block1a5.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a5.ram_block_type = "auto";
defparam ram_block1a5.mem_init3 = 2048'h70E1E3C7870E1C3C78F0E1C3878F1E3C7870E1C3870F1E3C78F1E3C3870E1C3870E1C3870E1C3870E1C3870E1C3870E1C38F1E3C78F1E3C70E1C3871E3C78F1C3870E1C78F1C3870E3C78E1C3871E3C70E1C78F1C38F1E3870E3C70E1C78E1C78F1C38F1C38F1C3871E3871E3871E3871E3871E38F1C38F1C38F1C78E1C78E3C70E3C71E3871C38E1C78E3C71E3871C38E1C70E3871C38E1C70E3871C38E1C71E38F1C70E3871C78E3C71C38E3C71C38E3C71C38E3C71C38E3871C78E38F1C71E38E3C71C78E38F1C71C38E3871C71E38E3871C71C38E38F1C71C78E38E3C71C71C38E38E3C71C71C78E38E38F1C71C71C78E38E38E3C71C71C71C38E38E38E3;
defparam ram_block1a5.mem_init2 = 2048'h8F1C71C71C71C71E38E38E38E38E38E3871C71C71C71C71C71C71C71C71C70E38E38E38E38E38E38E38E38E38E38E38E38E38E38E31C71C71C71C71C71C71C71C71C71CE38E38E38E38E38E39C71C71C71C71C638E38E38E38C71C71C71C738E38E38E31C71C71C738E38E38C71C71C738E38E38C71C71C638E38E71C71C638E38E71C71C638E38C71C718E38E31C71C638E39C71C738E38C71C738E38C71C738E39C71C638E31C71CE38E71C738E39C71CE38E71C738E39C718E38C71C638E71C738E31C738E39C718E39C718E39C718E39C718E39C718E39C718E31C738E31C638E71C638C71CE39C718E31C738E71CE38C718E31C738E71CE39C738E31C63;
defparam ram_block1a5.mem_init1 = 2048'h8C718E31C638C718E31C638C718E31C638C738E71CE39C738C718E31C639C738E71CE31C638C738E718E31C639C738C718E71CE31C639C738C718E718E31CE39C639C738C738E718E71CE31CE39C639C638C738C738E718E718E71CE31CE31CE31CE39C639C639C639C639C638C738C738C738C738C738C738C738C738C738C738C639C639C639C639C639C639CE31CE31CE31CE718E718E718C738C738C739C639C631CE31CE318E718E738C738C639C639CE31CE718E738C738C639C631CE318E718C738C639C631CE718E738C739C631CE318E738C739C631CE318E738C639CE31CE718C739C631CE318E738C639CE318E738C639CE318E738C639CE318E7;
defparam ram_block1a5.mem_init0 = 2048'h38C639CE318C739C631CE718C739CE318E738C631CE718C739CE318E738C631CE718C639CE318C739C6318E739C631CE738C631CE718C639CE718C639CE318C739CE318C739CE318C739CE318C739CE318C739CE318C739CE318C639CE718C639CE738C631CE738C6318E739C6318C739CE318C639CE718C631CE739C6318E739CE318C639CE738C6318E739C6318C739CE718C631CE739C6318C639CE738C6318E739CE318C639CE739C6318C739CE718C6318E739CE318C631CE739C6318C639CE738C6318C739CE718C6318E739CE718C631CE739CE318C631CE739CE318C639CE739C6318C639CE739C6318C639CE739C6318C739CE738C6318C739CE738;

cycloneive_ram_block ram_block1a59(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a59_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a59.clk0_core_clock_enable = "ena0";
defparam ram_block1a59.clk0_input_clock_enable = "ena0";
defparam ram_block1a59.clk0_output_clock_enable = "ena0";
defparam ram_block1a59.data_interleave_offset_in_bits = 1;
defparam ram_block1a59.data_interleave_width_in_bits = 1;
defparam ram_block1a59.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a59.init_file_layout = "port_a";
defparam ram_block1a59.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a59.operation_mode = "rom";
defparam ram_block1a59.port_a_address_clear = "none";
defparam ram_block1a59.port_a_address_width = 13;
defparam ram_block1a59.port_a_data_out_clear = "none";
defparam ram_block1a59.port_a_data_out_clock = "clock0";
defparam ram_block1a59.port_a_data_width = 1;
defparam ram_block1a59.port_a_first_address = 24576;
defparam ram_block1a59.port_a_first_bit_number = 5;
defparam ram_block1a59.port_a_last_address = 32767;
defparam ram_block1a59.port_a_logical_ram_depth = 65536;
defparam ram_block1a59.port_a_logical_ram_width = 18;
defparam ram_block1a59.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a59.ram_block_type = "auto";
defparam ram_block1a59.mem_init3 = 2048'h39CE739C6318C639CE739C6318C739CE738C6318C739CE738C6318C739CE738C6318E739CE718C6318E739CE718C631CE739CE318C631CE739C6318C639CE738C6318C739CE718C6318E739CE318C631CE739C6318C739CE738C6318E739CE318C639CE738C6318C739CE718C631CE739C6318C739CE318C639CE738C6318E739CE318C739CE718C631CE738C6318E739C6318C739CE318C639CE718C639CE738C631CE738C6318E739C6318E739C6318E739C6318E739C6318E739C6318E739C6318E738C631CE738C631CE718C639CE718C739CE318C739C6318E738C631CE718C639CE318E739C631CE718C639CE318E739C631CE718C739C6318E738C639;
defparam ram_block1a59.mem_init2 = 2048'hCE318E738C639CE318E738C639CE318E738C639CE318E718C739C631CE718E738C639CE318E718C739C639CE318E718C739C639CE31CE718C738C639C631CE318E718C738C639C639CE31CE718E738C738C639C639CE31CE318E718E718C738C739C639C639C631CE31CE31CE718E718E718E738C738C738C738C738C738C639C639C639C639C639C639C639C639C639C639C638C738C738C738C738C738E718E718E718E71CE31CE31CE39C639C638C738C738E718E71CE31CE39C639C738C738E718E31CE31C639C738C718E71CE31C639C738C718E31CE39C638C718E71CE39C738C718E31C639C738E71CE39C638C718E31C638C718E31C638C718E31C63;
defparam ram_block1a59.mem_init1 = 2048'h8C718E39C738E71CE39C718E31C638E71CE39C718E31C738E71C638C71CE38C718E39C718E31C738E31C738E31C738E31C738E31C738E31C738E39C718E39C71CE38C71C638E31C738E39C71CE38E71C738E39C71CE38E71C718E38C71C738E39C71C638E39C71C638E39C71C738E38C71C718E38E31C71C638E38C71C71CE38E38C71C71CE38E38C71C71C638E38E39C71C71C638E38E39C71C71C718E38E38E39C71C71C71C638E38E38E38C71C71C71C71C738E38E38E38E38E38E71C71C71C71C71C71C71C71C71C718E38E38E38E38E38E38E38E38E38E38E38E38E38E38E1C71C71C71C71C71C71C71C71C71C38E38E38E38E38E38F1C71C71C71C71E3;
defparam ram_block1a59.mem_init0 = 2048'h8E38E38E3871C71C71C78E38E38E3C71C71C71E38E38E3C71C71C78E38E3871C71C78E38E3C71C71E38E3871C71C38E38F1C71C38E3871C71E38E3C71C78E38F1C71E38E3C71C38E3871C78E3871C78E3871C78E3871C78E3C71C38E1C71E38F1C70E3871C38E1C70E3871C38E1C70E3871C38F1C78E3C70E3871C38F1C78E1C78E3C70E3C71E3871E3871E38F1C38F1C38F1C38F1C38F1C3871E3871E3871E3C70E3C70E1C78E1C38F1E3871E3C70E1C78F1C3870E3C78E1C3871E3C70E1C3871E3C78F1C3870E1C78F1E3C78F1E3870E1C3870E1C3870E1C3870E1C3870E1C3870E1C3878F1E3C78F1E1C3870E1C3C78F1E3C3870E1E3C7870E1C3C78F0E1C;

cycloneive_ram_block ram_block1a114(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a114_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a114.clk0_core_clock_enable = "ena0";
defparam ram_block1a114.clk0_input_clock_enable = "ena0";
defparam ram_block1a114.clk0_output_clock_enable = "ena0";
defparam ram_block1a114.data_interleave_offset_in_bits = 1;
defparam ram_block1a114.data_interleave_width_in_bits = 1;
defparam ram_block1a114.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a114.init_file_layout = "port_a";
defparam ram_block1a114.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a114.operation_mode = "rom";
defparam ram_block1a114.port_a_address_clear = "none";
defparam ram_block1a114.port_a_address_width = 13;
defparam ram_block1a114.port_a_data_out_clear = "none";
defparam ram_block1a114.port_a_data_out_clock = "clock0";
defparam ram_block1a114.port_a_data_width = 1;
defparam ram_block1a114.port_a_first_address = 49152;
defparam ram_block1a114.port_a_first_bit_number = 6;
defparam ram_block1a114.port_a_last_address = 57343;
defparam ram_block1a114.port_a_logical_ram_depth = 65536;
defparam ram_block1a114.port_a_logical_ram_width = 18;
defparam ram_block1a114.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a114.ram_block_type = "auto";
defparam ram_block1a114.mem_init3 = 2048'hFC07F01FE03F80FF01FC07F80FE03FC07F00FE03FC07F01FE03FC07F00FE03FC07F80FE01FC03F80FF01FE03FC07F80FF01FE03FC07F80FF01FE03FC07F807F00FE01FC03FC07F807F00FF01FE01FC03FC07F807F00FF00FE01FE01FC03FC03F807F807F807F00FF00FF00FF01FE01FE01FE01FE01FE01FE01FE01FE01FE01FE01FE01FE01FE01FE01FE01FF00FF00FF00FF007F807F807FC03FC03FC01FE01FF00FF00FF807F803FC03FE01FF00FF007F803FC03FE01FF00FF807FC03FE01FF00FF803FC01FE00FF807FC01FE00FF807FC01FE00FF803FC01FF007FC03FE00FF803FE01FF007FC01FF007FC01FF007FC01FF007FC01FF007FC01FF003FE00FF;
defparam ram_block1a114.mem_init2 = 2048'h803FE007FC01FF003FE00FF801FF007FE00FF801FF003FE00FFC01FF803FF007FE00FFC01FF801FF003FE007FE00FFC00FFC01FF801FF803FF003FF003FF007FE007FE007FE007FE007FE007FE007FF003FF003FF003FF001FF801FF800FFC00FFE007FE003FF001FF800FFC007FE003FF001FF800FFE007FF001FFC00FFE003FF801FFC007FF001FFC007FF001FFC007FF001FFC007FF800FFE003FF8007FF001FFE003FFC007FF000FFE001FFE003FFC007FF8007FF000FFF000FFF001FFE001FFE001FFE001FFE001FFF000FFF000FFF0007FF8007FFC003FFC001FFE000FFF8007FFC003FFE000FFF8003FFC001FFF0007FFC001FFF0007FFE000FFF8003;
defparam ram_block1a114.mem_init1 = 2048'hFFF0007FFC000FFF8001FFF8003FFF0003FFE0007FFE0007FFE0007FFE0007FFE0007FFF0003FFF0001FFF8001FFFC0007FFE0003FFF8000FFFE0007FFF8000FFFE0003FFF8000FFFF0001FFFE0003FFFC0003FFFC0007FFF80007FFF80003FFFC0003FFFE0001FFFF0000FFFF80003FFFE0001FFFF80003FFFE0000FFFFC0001FFFF80003FFFF00003FFFF00003FFFF80001FFFF80000FFFFE00003FFFF00000FFFFE00003FFFF800007FFFF800007FFFF800007FFFF800003FFFFC00001FFFFF000007FFFFC00000FFFFF800001FFFFF800001FFFFFC000007FFFFE000001FFFFFC000007FFFFF8000007FFFFFC000003FFFFFE000000FFFFFF8000001FFFF;
defparam ram_block1a114.mem_init0 = 2048'hFF8000000FFFFFFC0000003FFFFFF80000007FFFFFF80000003FFFFFFE00000007FFFFFFE00000007FFFFFFF80000000FFFFFFFF800000003FFFFFFFF000000003FFFFFFFF8000000007FFFFFFFFC000000000FFFFFFFFFF0000000000FFFFFFFFFF80000000000FFFFFFFFFFF800000000001FFFFFFFFFFFE0000000000003FFFFFFFFFFFFE00000000000001FFFFFFFFFFFFFFC0000000000000003FFFFFFFFFFFFFFFFF00000000000000000007FFFFFFFFFFFFFFFFFFFFF800000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a96(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a96_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a96.clk0_core_clock_enable = "ena0";
defparam ram_block1a96.clk0_input_clock_enable = "ena0";
defparam ram_block1a96.clk0_output_clock_enable = "ena0";
defparam ram_block1a96.data_interleave_offset_in_bits = 1;
defparam ram_block1a96.data_interleave_width_in_bits = 1;
defparam ram_block1a96.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a96.init_file_layout = "port_a";
defparam ram_block1a96.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a96.operation_mode = "rom";
defparam ram_block1a96.port_a_address_clear = "none";
defparam ram_block1a96.port_a_address_width = 13;
defparam ram_block1a96.port_a_data_out_clear = "none";
defparam ram_block1a96.port_a_data_out_clock = "clock0";
defparam ram_block1a96.port_a_data_width = 1;
defparam ram_block1a96.port_a_first_address = 40960;
defparam ram_block1a96.port_a_first_bit_number = 6;
defparam ram_block1a96.port_a_last_address = 49151;
defparam ram_block1a96.port_a_logical_ram_depth = 65536;
defparam ram_block1a96.port_a_logical_ram_width = 18;
defparam ram_block1a96.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a96.ram_block_type = "auto";
defparam ram_block1a96.mem_init3 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000003FFFFFFFFFFFFFFFFFFFFFC0000000000000000001FFFFFFFFFFFFFFFFF80000000000000007FFFFFFFFFFFFFF00000000000000FFFFFFFFFFFFF8000000000000FFFFFFFFFFFF000000000003FFFFFFFFFFE00000000003FFFFFFFFFE0000000001FFFFFFFFFE0000000007FFFFFFFFC000000003FFFFFFFF800000001FFFFFFFF800000003FFFFFFFE00000003FFFFFFFC0000000FFFFFFFC0000000FFFFFFF80000003FFFFFFC0000003FFFFFF80000007FFFFFE0000003FF;
defparam ram_block1a96.mem_init2 = 2048'hFFFF0000003FFFFFE000000FFFFFF8000007FFFFFC000003FFFFFC000007FFFFF000000FFFFFC000007FFFFF000003FFFFF000003FFFFE000007FFFFC00001FFFFF000007FFFF800003FFFFC00003FFFFC00003FFFFC00003FFFF80000FFFFE00001FFFF80000FFFFE00003FFFF00003FFFF80001FFFF80001FFFF80003FFFF00007FFFE0000FFFF80003FFFF0000FFFF80003FFFE0001FFFF0000FFFF80007FFF80003FFFC0003FFFC0007FFF80007FFF8000FFFF0001FFFE0003FFF8000FFFE0003FFFC000FFFE0003FFF8000FFFC0007FFF0003FFF0001FFF8001FFFC000FFFC000FFFC000FFFC000FFFC000FFF8001FFF8003FFF0003FFE0007FFC001FFF;
defparam ram_block1a96.mem_init1 = 2048'h8003FFE000FFFC001FFF0007FFC001FFF0007FF8003FFE000FFF8007FFC003FFE000FFF0007FF8007FFC003FFC001FFE001FFE001FFF000FFF000FFF000FFF000FFF001FFE001FFE001FFC003FFC007FF800FFF000FFE001FFC007FF800FFF001FFC003FF800FFE003FFC007FF001FFC007FF001FFC007FF001FFC007FF003FF800FFE007FF001FFC00FFE003FF001FF800FFC007FE003FF001FF800FFC00FFE007FE003FF003FF001FF801FF801FF801FFC00FFC00FFC00FFC00FFC00FFC00FFC01FF801FF801FF803FF003FF007FE007FE00FFC00FF801FF003FF007FE00FFC01FF803FF007FE00FF801FF003FE00FFC01FF003FE00FF801FF007FC00FF803;
defparam ram_block1a96.mem_init0 = 2048'hFE00FF801FF007FC01FF007FC01FF007FC01FF007FC01FF007FC01FF00FF803FE00FF807FC01FF007F803FE00FF007FC03FE00FF007FC03FE00FF007F803FE01FF00FF807FC03FE01FF00FF807F803FC01FE01FF00FF807F803FC03FE01FE01FF00FF007F807F807FC03FC03FC01FE01FE01FE01FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF01FE01FE01FE01FC03FC03FC03F807F807F00FF00FE01FE01FC03FC07F807F00FF01FE01FC03FC07F807F00FE01FC03FC07F80FF01FE03FC07F80FF01FE03FC07F80FF01FE03F807F00FE03FC07F80FE01FC07F80FF01FC07F80FE01FC07F80FE03FC07F01FE03F80FF01FC07F;

cycloneive_ram_block ram_block1a78(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a78_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a78.clk0_core_clock_enable = "ena0";
defparam ram_block1a78.clk0_input_clock_enable = "ena0";
defparam ram_block1a78.clk0_output_clock_enable = "ena0";
defparam ram_block1a78.data_interleave_offset_in_bits = 1;
defparam ram_block1a78.data_interleave_width_in_bits = 1;
defparam ram_block1a78.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a78.init_file_layout = "port_a";
defparam ram_block1a78.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a78.operation_mode = "rom";
defparam ram_block1a78.port_a_address_clear = "none";
defparam ram_block1a78.port_a_address_width = 13;
defparam ram_block1a78.port_a_data_out_clear = "none";
defparam ram_block1a78.port_a_data_out_clock = "clock0";
defparam ram_block1a78.port_a_data_width = 1;
defparam ram_block1a78.port_a_first_address = 32768;
defparam ram_block1a78.port_a_first_bit_number = 6;
defparam ram_block1a78.port_a_last_address = 40959;
defparam ram_block1a78.port_a_logical_ram_depth = 65536;
defparam ram_block1a78.port_a_logical_ram_width = 18;
defparam ram_block1a78.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a78.ram_block_type = "auto";
defparam ram_block1a78.mem_init3 = 2048'h00FE03F80FF01FC07F01FE03F80FE03F80FF01FC07F01FC07F01FC07F00FE03F80FE03F80FE03F80FE03F80FE03F80FC07F01FC07F01FC07F01FC0FE03F80FE03F80FC07F01FC07E03F80FE03F01FC07F03F80FE03F01FC07E03F80FC07F01F80FE03F01FC0FE03F01FC0FE03F81FC0FE03F01FC0FE03F01FC0FE07F01F80FC07F03F81FC0FE03F01F80FC07E03F01FC0FE07F03F81FC0FE07F03F81F80FC07E03F01F80FC0FE07F03F81F80FC07E03F03F81FC0FC07E07F03F81F80FC0FE07E03F03F81F80FC0FE07E03F03F01F81FC0FC0FE07E03F03F01F81F80FC0FC07E07E07F03F03F81F81F80FC0FC0FC07E07E07F03F03F03F81F81F81F80FC0FC0FC;
defparam ram_block1a78.mem_init2 = 2048'h0FC07E07E07E07E07F03F03F03F03F03F03F81F81F81F81F81F81F81F81F81F81F80FC0FC0FC0FC0FC0FC0FC0FC0FC0FC0FC1F81F81F81F81F81F81F81F81F81F81F83F03F03F03F03F03F07E07E07E07E07E0FC0FC0FC0FC1F81F81F81F83F03F03F07E07E07E07C0FC0FC1F81F81F83F03F03E07E07E0FC0FC0F81F81F83F03F07E07E0FC0FC1F81F81F03F03E07E0FC0FC1F81F83F03E07E07C0FC1F81F83F03E07E0FC0FC1F81F03F07E07C0FC1F81F03F07E07C0FC1F81F03F07E0FC0F81F83F03E07C0FC1F83F03E07E0FC1F81F03E07E0FC1F81F03E07C0FC1F83F07E07C0F81F03F07E0FC1F83F03E07C0F81F03E07E0FC1F83F07E0FC1F83F07E0FC;
defparam ram_block1a78.mem_init1 = 2048'h1F81F03E07C0F81F03E07C0F83F07E0FC1F83F07E0FC1F83F07E0FC1F03E07C0F81F03E07C1F83F07E0FC1F03E07C0F83F07E0FC1F03E07C0F83F07E0FC1F03E07C1F83F07C0F81F07E0FC1F03E07C1F83E07C0F83F07E0F81F07E0F81F03E0FC1F03E0FC1F83E07C1F83E07C1F83E07C1F83F07C0F83F07C0F83F07C1F83E07C1F83E07C1F83E07C1F83E0FC1F03E0FC1F03E0F81F07E0F83F07C0F83E07C1F83E0FC1F03E0F81F07E0F83F07C1F83E07C1F03E0F81F07C0F83E07C1F03E0F81F07C0F83E07C1F03E0F81F07C0F83E07C1F07E0F83F07C1F83E0F81F07C0F83E0FC1F07E0F83E07C1F07E0F83E07C1F03E0F83F07C1F03E0F83F07C1F07E0F8;
defparam ram_block1a78.mem_init0 = 2048'h3E07C1F07E0F83E07C1F07C0F83E0FC1F07C1F83E0F81F07C1F03E0F83E07C1F07C0F83E0F81F07C1F03E0F83E07C1F07C0F83E0F83F07C1F07E0F83E0F81F07C1F07E0F83E0FC1F07C1F03E0F83E0FC1F07C1F07E0F83E0F81F07C1F07C0F83E0F83F07C1F07C1F83E0F83E0FC1F07C1F07E0F83E0F83F07C1F07C1F83E0F83E0F81F07C1F07C0F83E0F83E0FC1F07C1F07C0F83E0F83E0FC1F07C1F07C0F83E0F83E0F81F07C1F07C1F83E0F83E0F83F07C1F07C1F07E0F83E0F83E0FC1F07C1F07C1F83E0F83E0F83F07C1F07C1F07C0F83E0F83E0F81F07C1F07C1F03E0F83E0F83E0FC1F07C1F07C1F03E0F83E0F83E07C1F07C1F07C1F83E0F83E0F83E;

cycloneive_ram_block ram_block1a132(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a132_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a132.clk0_core_clock_enable = "ena0";
defparam ram_block1a132.clk0_input_clock_enable = "ena0";
defparam ram_block1a132.clk0_output_clock_enable = "ena0";
defparam ram_block1a132.data_interleave_offset_in_bits = 1;
defparam ram_block1a132.data_interleave_width_in_bits = 1;
defparam ram_block1a132.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a132.init_file_layout = "port_a";
defparam ram_block1a132.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a132.operation_mode = "rom";
defparam ram_block1a132.port_a_address_clear = "none";
defparam ram_block1a132.port_a_address_width = 13;
defparam ram_block1a132.port_a_data_out_clear = "none";
defparam ram_block1a132.port_a_data_out_clock = "clock0";
defparam ram_block1a132.port_a_data_width = 1;
defparam ram_block1a132.port_a_first_address = 57344;
defparam ram_block1a132.port_a_first_bit_number = 6;
defparam ram_block1a132.port_a_last_address = 65535;
defparam ram_block1a132.port_a_logical_ram_depth = 65536;
defparam ram_block1a132.port_a_logical_ram_width = 18;
defparam ram_block1a132.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a132.ram_block_type = "auto";
defparam ram_block1a132.mem_init3 = 2048'hF83E0F83E0F83F07C1F07C1F07C0F83E0F83E0F81F07C1F07C1F07E0F83E0F83E0F81F07C1F07C1F03E0F83E0F83E07C1F07C1F07C1F83E0F83E0F83F07C1F07C1F07E0F83E0F83E0FC1F07C1F07C1F83E0F83E0F83F07C1F07C1F03E0F83E0F83E07C1F07C1F07E0F83E0F83E07C1F07C1F07E0F83E0F83E07C1F07C1F03E0F83E0F83F07C1F07C1F83E0F83E0FC1F07C1F07E0F83E0F83F07C1F07C1F83E0F83E07C1F07C1F03E0F83E0FC1F07C1F07E0F83E0F81F07C1F07E0F83E0FC1F07C1F03E0F83E0FC1F07C1F83E0F83E07C1F07C0F83E0F81F07C1F03E0F83E07C1F07C0F83E0F81F07C1F03E0F83F07C1F07E0F83E07C1F07C0F83E0FC1F07C0F8;
defparam ram_block1a132.mem_init2 = 2048'h3E0FC1F07C1F83E0F81F07C1F83E0F81F07C0F83E0FC1F07C0F83E0FC1F07E0F83E07C1F03E0F83F07C1F83E0FC1F07C0F83E07C1F03E0F81F07C0F83E07C1F03E0F81F07C0F83E07C1F03E0F81F07C0F83F07C1F83E0FC1F03E0F81F07E0F83F07C0F83E07C1F83E0FC1F03E0F81F07E0F81F07E0F83F07C0F83F07C0F83F07C0F83F07C1F83E07C1F83E07C1F83F07C0F83F07C0F83F07C0F83F07E0F81F07E0F81F03E0FC1F03E0FC1F83E07C0F83F07C0F81F07E0FC1F03E07C1F83F07C0F81F07E0FC1F83E07C0F81F07E0FC1F83E07C0F81F07E0FC1F83F07C0F81F03E07C0F81F07E0FC1F83F07E0FC1F83F07E0FC1F83E07C0F81F03E07C0F81F03F0;
defparam ram_block1a132.mem_init1 = 2048'h7E0FC1F83F07E0FC1F83F07E0FC0F81F03E07C0F81F83F07E0FC1F81F03E07C0FC1F83F07E07C0F81F03F07E0FC0F81F03F07E0FC0F81F83F07E07C0F81F83F03E07E0FC1F81F03F07E07C0FC1F81F03F07E07C0FC1F81F03F07E07E0FC0F81F83F03F07E07C0FC0F81F83F03F07E07E0FC0F81F81F03F03F07E07E0FC0FC1F81F83F03F03E07E07E0FC0FC0F81F81F83F03F03F07E07E07C0FC0FC0FC1F81F81F83F03F03F03F07E07E07E07E0FC0FC0FC0FC0FC1F81F81F81F81F81F83F03F03F03F03F03F03F03F03F03F03F07E07E07E07E07E07E07E07E07E07E07E03F03F03F03F03F03F03F03F03F03F03F81F81F81F81F81F81FC0FC0FC0FC0FC07E0;
defparam ram_block1a132.mem_init0 = 2048'h7E07E07E03F03F03F03F81F81F81FC0FC0FC07E07E07E03F03F03F81F81FC0FC0FC07E07E03F03F01F81F80FC0FE07E07F03F01F81F80FC0FE07E03F03F81F80FC0FE07E03F03F81FC0FC07E07F03F81F80FC07E03F03F81FC0FE07E03F01F80FC07E03F03F81FC0FE07F03F81FC0FE07F01F80FC07E03F01F80FE07F03F81FC07E03F01FC0FE07F01F80FE07F01F80FE07F03F80FE07F01F80FE07F01F80FE03F01FC07E03F80FC07F01F80FE03F81FC07F01F80FE03F80FC07F01FC07E03F80FE03F80FE07F01FC07F01FC07F01FC07E03F80FE03F80FE03F80FE03F80FE03F80FE01FC07F01FC07F01FC07F01FE03F80FE03F80FF01FC07F01FE03F80FE01;

cycloneive_ram_block ram_block1a24(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a24_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a24.clk0_core_clock_enable = "ena0";
defparam ram_block1a24.clk0_input_clock_enable = "ena0";
defparam ram_block1a24.clk0_output_clock_enable = "ena0";
defparam ram_block1a24.data_interleave_offset_in_bits = 1;
defparam ram_block1a24.data_interleave_width_in_bits = 1;
defparam ram_block1a24.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a24.init_file_layout = "port_a";
defparam ram_block1a24.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a24.operation_mode = "rom";
defparam ram_block1a24.port_a_address_clear = "none";
defparam ram_block1a24.port_a_address_width = 13;
defparam ram_block1a24.port_a_data_out_clear = "none";
defparam ram_block1a24.port_a_data_out_clock = "clock0";
defparam ram_block1a24.port_a_data_width = 1;
defparam ram_block1a24.port_a_first_address = 8192;
defparam ram_block1a24.port_a_first_bit_number = 6;
defparam ram_block1a24.port_a_last_address = 16383;
defparam ram_block1a24.port_a_logical_ram_depth = 65536;
defparam ram_block1a24.port_a_logical_ram_width = 18;
defparam ram_block1a24.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a24.ram_block_type = "auto";
defparam ram_block1a24.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000001FFFFFFFFFFFFFFFFFFF000000000000000003FFFFFFFFFFFFFFFC000000000000007FFFFFFFFFFFFF80000000000003FFFFFFFFFFFF8000000000007FFFFFFFFFFE00000000001FFFFFFFFFFC0000000000FFFFFFFFFF0000000001FFFFFFFFF8000000003FFFFFFFFC000000007FFFFFFFF000000003FFFFFFFE00000001FFFFFFFE00000003FFFFFFF80000001FFFFFFF00000003FFFFFFC0000001FFFFFFC0000003FFFFFF8000000FFFFFFC00;
defparam ram_block1a24.mem_init2 = 2048'h0000FFFFFFC000001FFFFFF0000007FFFFFC000003FFFFFC000003FFFFFC000007FFFFF000001FFFFF800000FFFFFC00000FFFFFC00000FFFFF800003FFFFE00000FFFFF800003FFFFC00001FFFFE00001FFFFE00003FFFFC00007FFFF80000FFFFE00003FFFF80001FFFFC0000FFFFE00007FFFE00007FFFE00007FFFC0000FFFFC0001FFFF00003FFFE0000FFFF80003FFFC0001FFFF0000FFFF80007FFF80003FFFC0003FFFC0003FFFC0007FFF80007FFF0000FFFE0001FFFC0007FFF8000FFFE0003FFF8000FFFC0007FFF0001FFF8000FFFC0007FFE0007FFF0003FFF0003FFF0003FFF0003FFF0003FFF0003FFE0007FFE000FFFC001FFF8003FFE000;
defparam ram_block1a24.mem_init1 = 2048'h7FFC001FFF8003FFE000FFF8003FFE000FFF8003FFC001FFF0007FF8003FFE001FFF000FFF8007FFC003FFC001FFE001FFE000FFF000FFF000FFF000FFF000FFF000FFF001FFE001FFE003FFC003FF8007FF800FFF001FFE003FFC007FF000FFE003FFC007FF001FFE003FF800FFE003FF800FFE003FF800FFE003FF800FFE007FF001FFC00FFE003FF001FFC00FFE007FF001FF800FFC00FFE007FF003FF001FF801FFC00FFC00FFE007FE007FE003FF003FF003FF003FF003FF003FF003FF003FF007FE007FE007FE00FFC00FFC01FF801FF003FF007FE007FC00FF801FF003FE007FC00FF801FF003FE00FFC01FF803FE00FFC01FF003FE00FF801FF007FC;
defparam ram_block1a24.mem_init0 = 2048'h01FF003FE00FF803FE00FF803FE00FF803FE00FF803FE00FF803FE00FF807FC01FF007FC03FE00FF803FC01FF00FF803FE01FF007F803FC01FF00FF803FC01FE00FF007F803FC01FE00FF007F803FC03FE01FF00FF007F807FC03FC01FE01FF00FF00FF807F807FC03FC03FC01FE01FE01FE01FF00FF00FF00FF00FF00FF00FF00FF807F807F80FF00FF00FF00FF00FF00FF00FF01FE01FE01FE01FC03FC03FC07F807F807F00FF00FE01FE03FC03FC07F807F00FF01FE01FC03F807F80FF01FE01FC03F807F00FE01FE03FC07F80FF01FE03F807F00FE01FC03F80FF01FE03F807F00FE03FC07F00FE03FC07F00FE03F807F01FE03F80FF01FC07F80FE03F80;

cycloneive_ram_block ram_block1a42(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a42_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a42.clk0_core_clock_enable = "ena0";
defparam ram_block1a42.clk0_input_clock_enable = "ena0";
defparam ram_block1a42.clk0_output_clock_enable = "ena0";
defparam ram_block1a42.data_interleave_offset_in_bits = 1;
defparam ram_block1a42.data_interleave_width_in_bits = 1;
defparam ram_block1a42.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a42.init_file_layout = "port_a";
defparam ram_block1a42.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a42.operation_mode = "rom";
defparam ram_block1a42.port_a_address_clear = "none";
defparam ram_block1a42.port_a_address_width = 13;
defparam ram_block1a42.port_a_data_out_clear = "none";
defparam ram_block1a42.port_a_data_out_clock = "clock0";
defparam ram_block1a42.port_a_data_width = 1;
defparam ram_block1a42.port_a_first_address = 16384;
defparam ram_block1a42.port_a_first_bit_number = 6;
defparam ram_block1a42.port_a_last_address = 24575;
defparam ram_block1a42.port_a_logical_ram_depth = 65536;
defparam ram_block1a42.port_a_logical_ram_width = 18;
defparam ram_block1a42.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a42.ram_block_type = "auto";
defparam ram_block1a42.mem_init3 = 2048'h03F80FE03FC07F01FE03F80FF01FC03F80FE01FC07F80FE01FC07F80FE01FC03F80FF01FE03F807F00FE01FC03F80FF01FE03FC07F80FF00FE01FC03F807F00FF01FE03FC03F807F00FF01FE01FC03FC07F807F80FF00FE01FE01FC03FC03FC07F807F807F00FF00FF00FF01FE01FE01FE01FE01FE01FE01FE03FC03FC03FE01FE01FE01FE01FE01FE01FE01FF00FF00FF00FF007F807F807FC03FC03FE01FE01FF00FF007F807FC03FC01FE01FF00FF807F803FC01FE00FF007F803FC01FE00FF007F803FE01FF007F803FC01FF00FF803FE01FF007F803FE00FF807FC01FF007FC03FE00FF803FE00FF803FE00FF803FE00FF803FE00FF803FE00FF801FF00;
defparam ram_block1a42.mem_init2 = 2048'h7FC01FF003FE00FF801FF007FE00FF803FF007FE00FF801FF003FE007FC00FF801FF003FE007FC00FFC01FF801FF003FF007FE007FE00FFC00FFC00FFC01FF801FF801FF801FF801FF801FF801FF801FF800FFC00FFC00FFE007FE007FF003FF001FF801FFC00FFE007FE003FF001FFC00FFE007FF001FF800FFE007FF001FFC00FFE003FF800FFE003FF800FFE003FF800FFE003FF800FFF001FFC007FF800FFE001FFC007FF800FFF001FFE003FFC003FF8007FF800FFF000FFF001FFE001FFE001FFE001FFE001FFE001FFE000FFF000FFF0007FF8007FFC003FFE001FFF000FFF8003FFC001FFF0007FF8003FFE000FFF8003FFE000FFF8003FFF0007FFC;
defparam ram_block1a42.mem_init1 = 2048'h000FFF8003FFF0007FFE000FFFC000FFF8001FFF8001FFF8001FFF8001FFF8001FFF8001FFFC000FFFC0007FFE0003FFF0001FFFC0007FFE0003FFF8000FFFE0003FFFC0007FFF0000FFFE0001FFFC0003FFFC0007FFF80007FFF80007FFF80003FFFC0003FFFE0001FFFF00007FFF80003FFFE0000FFFF80001FFFF00007FFFE00007FFFC0000FFFFC0000FFFFC0000FFFFE00007FFFF00003FFFF80000FFFFE00003FFFFC00007FFFF80000FFFFF00000FFFFF000007FFFF800003FFFFE00000FFFFF800003FFFFE000007FFFFE000007FFFFE000003FFFFF000001FFFFFC000007FFFFF8000007FFFFF8000007FFFFFC000001FFFFFF0000007FFFFFE0000;
defparam ram_block1a42.mem_init0 = 2048'h007FFFFFE0000003FFFFFF80000007FFFFFF00000007FFFFFF80000001FFFFFFF00000003FFFFFFF80000000FFFFFFFF00000000FFFFFFFF800000001FFFFFFFFC000000007FFFFFFFF8000000003FFFFFFFFF0000000001FFFFFFFFFE00000000007FFFFFFFFFF00000000000FFFFFFFFFFFC000000000003FFFFFFFFFFFF80000000000003FFFFFFFFFFFFFC000000000000007FFFFFFFFFFFFFFF800000000000000001FFFFFFFFFFFFFFFFFFF0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a6(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a6_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a6.clk0_core_clock_enable = "ena0";
defparam ram_block1a6.clk0_input_clock_enable = "ena0";
defparam ram_block1a6.clk0_output_clock_enable = "ena0";
defparam ram_block1a6.data_interleave_offset_in_bits = 1;
defparam ram_block1a6.data_interleave_width_in_bits = 1;
defparam ram_block1a6.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a6.init_file_layout = "port_a";
defparam ram_block1a6.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a6.operation_mode = "rom";
defparam ram_block1a6.port_a_address_clear = "none";
defparam ram_block1a6.port_a_address_width = 13;
defparam ram_block1a6.port_a_data_out_clear = "none";
defparam ram_block1a6.port_a_data_out_clock = "clock0";
defparam ram_block1a6.port_a_data_width = 1;
defparam ram_block1a6.port_a_first_address = 0;
defparam ram_block1a6.port_a_first_bit_number = 6;
defparam ram_block1a6.port_a_last_address = 8191;
defparam ram_block1a6.port_a_logical_ram_depth = 65536;
defparam ram_block1a6.port_a_logical_ram_width = 18;
defparam ram_block1a6.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a6.ram_block_type = "auto";
defparam ram_block1a6.mem_init3 = 2048'h7F01FC07F80FE03F80FF01FC07F01FC07F80FE03F80FE03F80FE03FC07F01FC07F01FC07F01FC07F01FC07F01FC07F01FC0FE03F80FE03F80FE03F81FC07F01FC07F01F80FE03F80FC07F01FC07E03F80FE07F01FC0FE03F80FC07F01F80FE07F01FC0FE03F01FC07E03F81FC07E03F81FC07E03F01FC0FE03F01F80FE07F03F80FC07E03F81FC0FE07F03F81FC07E03F01F80FC07E03F01F80FC07E03F01F81FC0FE07F03F81F80FC07E03F03F81FC0FC07E03F03F81FC0FC07E07F03F01F81FC0FC07E07F03F01F81FC0FC07E07E03F03F81F81FC0FC0FE07E07F03F03F81F81FC0FC0FC07E07E07F03F03F01F81F81F80FC0FC0FC07E07E07E03F03F03F03;
defparam ram_block1a6.mem_init2 = 2048'hF01F81F81F81F81FC0FC0FC0FC0FC0FC07E07E07E07E07E07E07E07E07E07F03F03F03F03F03F03F03F03F03F03F03F03F03F03F03E07E07E07E07E07E07E07E07E07E0FC0FC0FC0FC0FC0FC1F81F81F81F81F83F03F03F03F07E07E07E07C0FC0FC0FC1F81F81F83F03F03F07E07E07C0FC0FC0F81F81F83F03F07E07E07C0FC0F81F81F83F03F07E07E0FC0FC1F81F83F03E07E07C0FC0F81F83F03F07E07C0FC1F81F83F03E07E0FC0F81F83F03E07E0FC0F81F83F03E07E0FC0F81F83F07E07C0FC1F83F03E07E0FC1F81F03E07E0FC1F81F03E07E0FC1F81F03E07C0FC1F83F07E07C0F81F03E07E0FC1F83F07E0FC0F81F03E07C0F81F03E07C0FC1F83;
defparam ram_block1a6.mem_init1 = 2048'hF07E0FC1F83F07E0FC1F83F07E0FC1F83F07C0F81F03E07C0F81F03E07C1F83F07E0FC1F83F07C0F81F03E07C1F83F07E0F81F03E07C1F83F07E0F81F03E0FC1F83E07C0F83F07E0F81F03E0FC1F83E07C0F83F07C0F81F07E0F81F03E0FC1F03E0FC1F83E07C1F83E07C1F83F07C0F83F07C0F83F07C0F83F07C0F83F07C0F83F07C1F83E07C1F83E07C1F83E0FC1F03E0FC1F07E0F81F07E0F83F07C0F83E07C1F83E0FC1F03E0F81F07C0F83F07C1F83E0FC1F07E0F83F07C0F83E07C1F03E0F81F07C0F83E07C1F07E0F83F07C1F83E0FC1F07C0F83E07C1F03E0F83F07C1F03E0F81F07C1F83E0FC1F07C0F83E0FC1F07C0F83E0FC1F07C0F83E0FC1F07;
defparam ram_block1a6.mem_init0 = 2048'hC0F83E0FC1F07C1F83E0F81F07C1F03E0F83F07C1F07E0F83E0FC1F07C0F83E0F81F07C1F03E0F83E07C1F07C1F83E0F83F07C1F07E0F83E0F81F07C1F03E0F83E0FC1F07C1F03E0F83E0FC1F07C1F03E0F83E0FC1F07C1F03E0F83E0F81F07C1F07C0F83E0F83F07C1F07C1F83E0F83E0FC1F07C1F07E0F83E0F83E07C1F07C1F03E0F83E0F83F07C1F07C1F83E0F83E0F81F07C1F07C1F83E0F83E0F83F07C1F07C1F03E0F83E0F83E07C1F07C1F07E0F83E0F83E0FC1F07C1F07C1F83E0F83E0F83F07C1F07C1F07E0F83E0F83E0F81F07C1F07C1F03E0F83E0F83E0FC1F07C1F07C1F83E0F83E0F83E07C1F07C1F07C1F83E0F83E0F83F07C1F07C1F07C0;

cycloneive_ram_block ram_block1a60(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a60_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a60.clk0_core_clock_enable = "ena0";
defparam ram_block1a60.clk0_input_clock_enable = "ena0";
defparam ram_block1a60.clk0_output_clock_enable = "ena0";
defparam ram_block1a60.data_interleave_offset_in_bits = 1;
defparam ram_block1a60.data_interleave_width_in_bits = 1;
defparam ram_block1a60.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a60.init_file_layout = "port_a";
defparam ram_block1a60.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a60.operation_mode = "rom";
defparam ram_block1a60.port_a_address_clear = "none";
defparam ram_block1a60.port_a_address_width = 13;
defparam ram_block1a60.port_a_data_out_clear = "none";
defparam ram_block1a60.port_a_data_out_clock = "clock0";
defparam ram_block1a60.port_a_data_width = 1;
defparam ram_block1a60.port_a_first_address = 24576;
defparam ram_block1a60.port_a_first_bit_number = 6;
defparam ram_block1a60.port_a_last_address = 32767;
defparam ram_block1a60.port_a_logical_ram_depth = 65536;
defparam ram_block1a60.port_a_logical_ram_width = 18;
defparam ram_block1a60.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a60.ram_block_type = "auto";
defparam ram_block1a60.mem_init3 = 2048'h07C1F07C1F07C1F83E0F83E0F83F07C1F07C1F07C0F83E0F83E0F83F07C1F07C1F07E0F83E0F83E0F81F07C1F07C1F03E0F83E0F83E0FC1F07C1F07C1F83E0F83E0F83F07C1F07C1F07E0F83E0F83E0FC1F07C1F07C0F83E0F83E0F81F07C1F07C1F83E0F83E0F83F07C1F07C1F03E0F83E0F83F07C1F07C1F83E0F83E0F81F07C1F07C0F83E0F83E0FC1F07C1F07E0F83E0F83F07C1F07C1F83E0F83E07C1F07C1F03E0F83E0F81F07C1F07E0F83E0F81F07C1F07E0F83E0F81F07C1F07E0F83E0F81F07C1F03E0F83E0FC1F07C1F83E0F83F07C1F07C0F83E0F81F07C1F03E0F83E07C1F07E0F83E0FC1F07C1F83E0F81F07C1F03E0F83F07C1F07E0F83E07;
defparam ram_block1a60.mem_init2 = 2048'hC1F07E0F83E07C1F07E0F83E07C1F07E0F83E07C1F07E0F83F07C1F03E0F81F07C1F83E0F81F07C0F83E07C1F07E0F83F07C1F83E0FC1F07C0F83E07C1F03E0F81F07C0F83E07C1F83E0FC1F07E0F83F07C1F83E07C1F03E0F81F07E0F83F07C0F83E07C1F83E0FC1F03E0FC1F07E0F81F07E0F83F07C0F83F07C0F83F07C1F83E07C1F83E07C1F83E07C1F83E07C1F83E07C1F83F07C0F83F07C0F83F07E0F81F07E0F81F03E0FC1F03E07C1F83E07C0F83F07E0F81F03E0FC1F83E07C0F83F07E0F81F03E0FC1F83F07C0F81F03E0FC1F83F07C0F81F03E07C1F83F07E0FC1F83F07C0F81F03E07C0F81F03E07C1F83F07E0FC1F83F07E0FC1F83F07E0FC1F;
defparam ram_block1a60.mem_init1 = 2048'h83F07E07C0F81F03E07C0F81F03E07E0FC1F83F07E0FC0F81F03E07C0FC1F83F07E07C0F81F03F07E0FC0F81F03F07E0FC0F81F03F07E0FC0F81F83F07E07C0FC1F83F03E07E0FC0F81F83F03E07E0FC0F81F83F03E07E0FC0F81F83F03F07E07C0FC1F81F83F03E07E07C0FC0F81F83F03F07E07E0FC0FC1F81F83F03F03E07E07C0FC0FC1F81F83F03F03E07E07E07C0FC0FC1F81F81F83F03F03F07E07E07E07C0FC0FC0FC1F81F81F81F83F03F03F03F03F07E07E07E07E07E07E0FC0FC0FC0FC0FC0FC0FC0FC0FC0F81F81F81F81F81F81F81F81F81F81F81F81F81F81F81FC0FC0FC0FC0FC0FC0FC0FC0FC0FC07E07E07E07E07E07F03F03F03F03F01F;
defparam ram_block1a60.mem_init0 = 2048'h81F81F81F80FC0FC0FC07E07E07E03F03F03F01F81F81FC0FC0FC07E07E07F03F03F81F81FC0FC0FE07E07F03F03F81F80FC0FC07E07F03F01F81FC0FC07E07F03F01F81FC0FC07E07F03F81F80FC07E07F03F81F80FC07E03F03F81FC0FE07F03F01F80FC07E03F01F80FC07E03F01F80FC07F03F81FC0FE07F03F80FC07E03F81FC0FE03F01F80FE07F01F80FC07F03F80FC07F03F80FC07F01F80FE07F01FC0FE03F01FC07E03F80FE07F01FC0FE03F80FC07F01FC07E03F80FE03F01FC07F01FC07F03F80FE03F80FE03F80FE07F01FC07F01FC07F01FC07F01FC07F01FC07F01FC07F80FE03F80FE03F80FE03FC07F01FC07F01FE03F80FE03FC07F01FC;

cycloneive_ram_block ram_block1a115(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a115_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a115.clk0_core_clock_enable = "ena0";
defparam ram_block1a115.clk0_input_clock_enable = "ena0";
defparam ram_block1a115.clk0_output_clock_enable = "ena0";
defparam ram_block1a115.data_interleave_offset_in_bits = 1;
defparam ram_block1a115.data_interleave_width_in_bits = 1;
defparam ram_block1a115.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a115.init_file_layout = "port_a";
defparam ram_block1a115.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a115.operation_mode = "rom";
defparam ram_block1a115.port_a_address_clear = "none";
defparam ram_block1a115.port_a_address_width = 13;
defparam ram_block1a115.port_a_data_out_clear = "none";
defparam ram_block1a115.port_a_data_out_clock = "clock0";
defparam ram_block1a115.port_a_data_width = 1;
defparam ram_block1a115.port_a_first_address = 49152;
defparam ram_block1a115.port_a_first_bit_number = 7;
defparam ram_block1a115.port_a_last_address = 57343;
defparam ram_block1a115.port_a_logical_ram_depth = 65536;
defparam ram_block1a115.port_a_logical_ram_width = 18;
defparam ram_block1a115.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a115.ram_block_type = "auto";
defparam ram_block1a115.mem_init3 = 2048'hFFF8001FFFC000FFFE0007FFF0003FFF8000FFFC0007FFE0003FFF8000FFFC0007FFF0001FFFC000FFFE0003FFF8000FFFE0003FFF8000FFFE0003FFF80007FFF0001FFFC0007FFF8000FFFE0001FFFC0007FFF8000FFFF0001FFFE0003FFFC0007FFF80007FFF0000FFFF0001FFFE0001FFFE0001FFFE0001FFFE0001FFFE0001FFFE0001FFFE0001FFFE0000FFFF0000FFFF80007FFF80003FFFC0001FFFE0000FFFF00007FFFC0003FFFE0000FFFF80003FFFC0001FFFF00007FFFC0001FFFF00003FFFE0000FFFF80001FFFF00007FFFE0000FFFFC0001FFFF80003FFFF00003FFFE00007FFFE00007FFFE00007FFFE00007FFFE00007FFFE00003FFFF00;
defparam ram_block1a115.mem_init2 = 2048'h003FFFF80001FFFFC0000FFFFE00007FFFF00001FFFFC0000FFFFE00003FFFF80000FFFFE00001FFFFC00007FFFF00000FFFFE00001FFFFC00003FFFFC00007FFFF800007FFFF800007FFFF800007FFFFC00003FFFFC00001FFFFE00000FFFFF000007FFFFC00001FFFFF000007FFFFC00001FFFFF000007FFFFE00000FFFFFC00001FFFFF800001FFFFF800001FFFFF800001FFFFF800000FFFFFC000007FFFFE000003FFFFF800000FFFFFE000003FFFFF8000007FFFFF000000FFFFFE000001FFFFFE000001FFFFFE000000FFFFFF0000007FFFFF8000003FFFFFE000000FFFFFF8000003FFFFFF0000003FFFFFE0000007FFFFFE0000007FFFFFF0000003;
defparam ram_block1a115.mem_init1 = 2048'hFFFFFF8000000FFFFFFE0000003FFFFFFC0000007FFFFFF80000007FFFFFF80000007FFFFFFC0000001FFFFFFE00000007FFFFFFC0000000FFFFFFF80000000FFFFFFFC0000000FFFFFFFE00000003FFFFFFFC00000007FFFFFFF800000003FFFFFFFC00000001FFFFFFFF000000003FFFFFFFE000000003FFFFFFFF000000001FFFFFFFFC000000003FFFFFFFFC000000001FFFFFFFFF0000000003FFFFFFFFF0000000003FFFFFFFFF80000000007FFFFFFFFF80000000003FFFFFFFFFE00000000007FFFFFFFFFF00000000001FFFFFFFFFFE000000000007FFFFFFFFFFE000000000007FFFFFFFFFFF8000000000003FFFFFFFFFFFF0000000000001FFFF;
defparam ram_block1a115.mem_init0 = 2048'hFFFFFFFFF00000000000003FFFFFFFFFFFFF800000000000003FFFFFFFFFFFFFF8000000000000007FFFFFFFFFFFFFFF00000000000000003FFFFFFFFFFFFFFFFC000000000000000007FFFFFFFFFFFFFFFFFF00000000000000000000FFFFFFFFFFFFFFFFFFFFF00000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a97(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a97_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a97.clk0_core_clock_enable = "ena0";
defparam ram_block1a97.clk0_input_clock_enable = "ena0";
defparam ram_block1a97.clk0_output_clock_enable = "ena0";
defparam ram_block1a97.data_interleave_offset_in_bits = 1;
defparam ram_block1a97.data_interleave_width_in_bits = 1;
defparam ram_block1a97.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a97.init_file_layout = "port_a";
defparam ram_block1a97.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a97.operation_mode = "rom";
defparam ram_block1a97.port_a_address_clear = "none";
defparam ram_block1a97.port_a_address_width = 13;
defparam ram_block1a97.port_a_data_out_clear = "none";
defparam ram_block1a97.port_a_data_out_clock = "clock0";
defparam ram_block1a97.port_a_data_width = 1;
defparam ram_block1a97.port_a_first_address = 40960;
defparam ram_block1a97.port_a_first_bit_number = 7;
defparam ram_block1a97.port_a_last_address = 49151;
defparam ram_block1a97.port_a_logical_ram_depth = 65536;
defparam ram_block1a97.port_a_logical_ram_width = 18;
defparam ram_block1a97.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a97.ram_block_type = "auto";
defparam ram_block1a97.mem_init3 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000001FFFFFFFFFFFFFFFFFFFFE00000000000000000001FFFFFFFFFFFFFFFFFFC000000000000000007FFFFFFFFFFFFFFFF80000000000000001FFFFFFFFFFFFFFFC000000000000003FFFFFFFFFFFFFF800000000000003FFFFFFFFFFFFF80000000000001FFFFFFFFF;
defparam ram_block1a97.mem_init2 = 2048'hFFFF0000000000001FFFFFFFFFFFF8000000000003FFFFFFFFFFFC00000000000FFFFFFFFFFFC00000000000FFFFFFFFFFF00000000001FFFFFFFFFFC0000000000FFFFFFFFFF80000000003FFFFFFFFFC0000000003FFFFFFFFF8000000001FFFFFFFFF8000000001FFFFFFFFF0000000007FFFFFFFF8000000007FFFFFFFF000000001FFFFFFFF800000000FFFFFFFF800000001FFFFFFFF000000007FFFFFFF800000003FFFFFFFC00000007FFFFFFF80000000FFFFFFFE00000007FFFFFFE00000003FFFFFFE00000007FFFFFFC0000000FFFFFFF00000007FFFFFFC0000003FFFFFFC0000003FFFFFFC0000007FFFFFF8000000FFFFFFE0000003FFFFFF;
defparam ram_block1a97.mem_init1 = 2048'h8000001FFFFFFC000000FFFFFFC000000FFFFFF8000001FFFFFF8000003FFFFFE000000FFFFFF8000003FFFFFC000001FFFFFE000000FFFFFF000000FFFFFF000000FFFFFE000001FFFFFC000003FFFFF800000FFFFFE000003FFFFF800000FFFFFC000007FFFFE000003FFFFF000003FFFFF000003FFFFF000003FFFFF000007FFFFE00000FFFFFC00001FFFFF000007FFFFC00001FFFFF000007FFFFC00001FFFFE00000FFFFF000007FFFF800007FFFFC00003FFFFC00003FFFFC00003FFFFC00007FFFF800007FFFF00000FFFFE00001FFFFC00007FFFF00000FFFFE00003FFFF80000FFFFE00007FFFF00001FFFFC0000FFFFE00007FFFF00003FFFF800;
defparam ram_block1a97.mem_init0 = 2048'h01FFFF80000FFFFC0000FFFFC0000FFFFC0000FFFFC0000FFFFC0000FFFF80001FFFF80003FFFF00007FFFE0000FFFFC0001FFFF00003FFFE0000FFFF80001FFFF00007FFFC0001FFFF00007FFF80003FFFE0000FFFF80007FFFC0001FFFE0000FFFF00007FFF80003FFFC0003FFFE0001FFFE0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0001FFFE0001FFFC0003FFFC0007FFF8000FFFF0001FFFE0003FFFC0007FFF0000FFFE0003FFFC0007FFF0001FFFC0003FFF8000FFFE0003FFF8000FFFE0003FFF8000FFFE0007FFF0001FFFC0007FFE0003FFF8000FFFC0007FFE0003FFF8001FFFC000FFFE0007FFF0003FFF;

cycloneive_ram_block ram_block1a79(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a79_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a79.clk0_core_clock_enable = "ena0";
defparam ram_block1a79.clk0_input_clock_enable = "ena0";
defparam ram_block1a79.clk0_output_clock_enable = "ena0";
defparam ram_block1a79.data_interleave_offset_in_bits = 1;
defparam ram_block1a79.data_interleave_width_in_bits = 1;
defparam ram_block1a79.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a79.init_file_layout = "port_a";
defparam ram_block1a79.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a79.operation_mode = "rom";
defparam ram_block1a79.port_a_address_clear = "none";
defparam ram_block1a79.port_a_address_width = 13;
defparam ram_block1a79.port_a_data_out_clear = "none";
defparam ram_block1a79.port_a_data_out_clock = "clock0";
defparam ram_block1a79.port_a_data_width = 1;
defparam ram_block1a79.port_a_first_address = 32768;
defparam ram_block1a79.port_a_first_bit_number = 7;
defparam ram_block1a79.port_a_last_address = 40959;
defparam ram_block1a79.port_a_logical_ram_depth = 65536;
defparam ram_block1a79.port_a_logical_ram_width = 18;
defparam ram_block1a79.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a79.ram_block_type = "auto";
defparam ram_block1a79.mem_init3 = 2048'h0001FFF8000FFFC000FFFE0007FFE0007FFF0003FFF0003FFF0003FFF0001FFF8001FFF8001FFF8001FFF8001FFF8003FFF0003FFF0003FFF0003FFE0007FFE0007FFC000FFFC001FFF8001FFF0003FFF0007FFE000FFFC001FFF8003FFF0007FFE000FFFC001FFF0003FFE0007FFC001FFF0003FFE000FFFC001FFF0007FFC000FFF8003FFE000FFF8003FFE000FFFC001FFF0007FFC001FFF0007FF8003FFE000FFF8003FFE000FFF8007FFC001FFF0007FFC003FFE000FFF8007FFC001FFE000FFF8007FFC001FFE000FFF0007FFC003FFE001FFF000FFF8007FFC003FFE001FFF000FFF8007FF8003FFC003FFE001FFF000FFF0007FF8007FF8003FFC003;
defparam ram_block1a79.mem_init2 = 2048'hFFC001FFE001FFE000FFF000FFF000FFF0007FF8007FF8007FF8007FF8007FF8007FFC003FFC003FFC003FFC003FFC003FFC007FF8007FF8007FF8007FF8007FF8007FF000FFF000FFF000FFE001FFE001FFE003FFC003FFC007FF8007FF800FFF000FFE001FFE003FFC003FF8007FF800FFF001FFE001FFC003FF8007FF800FFF001FFE003FFC007FF800FFF001FFE003FFC007FF800FFE001FFC003FF8007FF001FFE003FFC007FF000FFE003FFC007FF000FFE003FFC007FF000FFE003FF8007FF001FFC003FF800FFE001FFC007FF001FFE003FF800FFE003FFC007FF001FFC007FF000FFE003FF800FFE003FF800FFE001FFC007FF001FFC007FF001FFC;
defparam ram_block1a79.mem_init1 = 2048'h007FF001FFC007FF001FFC007FF001FFC007FF001FFC007FF001FFC00FFE003FF800FFE003FF800FFE003FF001FFC007FF001FFC00FFE003FF800FFE003FF001FFC007FF003FF800FFE003FF001FFC007FE003FF800FFE007FF001FF800FFE003FF001FFC007FE003FF801FFC007FE003FF800FFC007FF003FF800FFC007FE003FF801FFC007FE003FF801FFC00FFE003FF001FF800FFE007FF003FF801FFC007FE003FF001FF800FFE007FF003FF801FFC00FFE007FF003FF801FFC00FFE007FF003FF801FFC00FFE007FF003FF801FFC00FFE007FF003FF801FF800FFC007FE003FF001FF801FFC00FFE007FE003FF001FF800FFC00FFE007FF003FF001FF8;
defparam ram_block1a79.mem_init0 = 2048'h01FFC00FFE007FE003FF003FF801FFC00FFC007FE007FF003FF001FF801FFC00FFC007FE007FF003FF001FF801FFC00FFC007FE007FF003FF001FF801FF800FFC00FFE007FE003FF003FF001FF801FFC00FFC00FFE007FE007FF003FF003FF801FF800FFC00FFC007FE007FE003FF003FF001FF801FF800FFC00FFC007FE007FE007FF003FF003FF801FF801FFC00FFC00FFC007FE007FE003FF003FF003FF801FF801FF800FFC00FFC007FE007FE007FF003FF003FF001FF801FF801FFC00FFC00FFC007FE007FE007FF003FF003FF003FF801FF801FF800FFC00FFC00FFE007FE007FE003FF003FF003FF001FF801FF801FFC00FFC00FFC007FE007FE007FE;

cycloneive_ram_block ram_block1a133(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a133_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a133.clk0_core_clock_enable = "ena0";
defparam ram_block1a133.clk0_input_clock_enable = "ena0";
defparam ram_block1a133.clk0_output_clock_enable = "ena0";
defparam ram_block1a133.data_interleave_offset_in_bits = 1;
defparam ram_block1a133.data_interleave_width_in_bits = 1;
defparam ram_block1a133.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a133.init_file_layout = "port_a";
defparam ram_block1a133.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a133.operation_mode = "rom";
defparam ram_block1a133.port_a_address_clear = "none";
defparam ram_block1a133.port_a_address_width = 13;
defparam ram_block1a133.port_a_data_out_clear = "none";
defparam ram_block1a133.port_a_data_out_clock = "clock0";
defparam ram_block1a133.port_a_data_width = 1;
defparam ram_block1a133.port_a_first_address = 57344;
defparam ram_block1a133.port_a_first_bit_number = 7;
defparam ram_block1a133.port_a_last_address = 65535;
defparam ram_block1a133.port_a_logical_ram_depth = 65536;
defparam ram_block1a133.port_a_logical_ram_width = 18;
defparam ram_block1a133.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a133.ram_block_type = "auto";
defparam ram_block1a133.mem_init3 = 2048'hFFC00FFC00FFC007FE007FE007FF003FF003FF001FF801FF801FF800FFC00FFC00FFE007FE007FE003FF003FF003FF801FF801FF801FFC00FFC00FFC007FE007FE007FF003FF003FF001FF801FF801FFC00FFC00FFC007FE007FE003FF003FF003FF801FF801FF800FFC00FFC007FE007FE007FF003FF003FF801FF801FFC00FFC00FFC007FE007FE003FF003FF001FF801FF800FFC00FFC007FE007FE003FF003FF801FF801FFC00FFC00FFE007FE007FF003FF001FF801FF800FFC00FFE007FE003FF003FF001FF801FFC00FFC007FE007FF003FF001FF801FFC00FFC007FE007FF003FF001FF801FFC00FFC007FE007FF003FF801FF800FFC00FFE007FF00;
defparam ram_block1a133.mem_init2 = 2048'h3FF001FF801FFC00FFE007FE003FF001FF800FFC00FFE007FF003FF001FF800FFC007FE003FF003FF801FFC00FFE007FF003FF801FFC00FFE007FF003FF801FFC00FFE007FF003FF801FFC00FFE007FF003FF801FFC00FFE003FF001FF800FFC007FF003FF801FFC00FFE003FF001FF800FFE007FF003FF800FFC007FF003FF800FFC007FE003FF801FFC007FE003FF800FFC007FF003FF800FFC007FF001FF800FFE003FF001FFC00FFE003FF800FFC007FF001FF800FFE003FF801FFC007FF001FF800FFE003FF800FFE007FF001FFC007FF001FF800FFE003FF800FFE003FF800FFE007FF001FFC007FF001FFC007FF001FFC007FF001FFC007FF001FFC00;
defparam ram_block1a133.mem_init1 = 2048'h7FF001FFC007FF001FFC007FF000FFE003FF800FFE003FF800FFE001FFC007FF001FFC007FF800FFE003FF800FFF001FFC007FF000FFE003FF8007FF001FFC003FF800FFE001FFC007FF800FFE001FFC007FF800FFE001FFC007FF800FFF001FFC003FF8007FF000FFE003FFC007FF800FFF001FFE003FFC007FF800FFF001FFE003FFC003FF8007FF000FFF001FFE003FFC003FF8007FF800FFF000FFE001FFE003FFC003FFC007FF8007FF800FFF000FFF000FFE001FFE001FFE001FFC003FFC003FFC003FFC003FFC003FFC007FF8007FF8007FF8007FF8007FF8007FFC003FFC003FFC003FFC003FFC003FFC001FFE001FFE001FFE000FFF000FFF0007FF;
defparam ram_block1a133.mem_init0 = 2048'h8007FF8003FFC003FFC001FFE001FFF000FFF8007FF8003FFC003FFE001FFF000FFF8007FFC003FFE001FFF000FFF8007FFC001FFE000FFF0007FFC003FFE000FFF0007FFC003FFE000FFF8007FFC001FFF0007FFC003FFE000FFF8003FFE000FFF8003FFC001FFF0007FFC001FFF0007FFE000FFF8003FFE000FFF8003FFE0007FFC001FFF0007FFE000FFF8001FFF0007FFC000FFF8001FFF0007FFE000FFFC001FFF8003FFF0007FFE000FFFC001FFF8001FFF0003FFF0007FFE0007FFC000FFFC000FFF8001FFF8001FFF8001FFF8003FFF0003FFF0003FFF0003FFF0003FFF0001FFF8001FFF8001FFF8001FFFC000FFFC000FFFE0007FFE0003FFF0001;

cycloneive_ram_block ram_block1a25(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a25_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a25.clk0_core_clock_enable = "ena0";
defparam ram_block1a25.clk0_input_clock_enable = "ena0";
defparam ram_block1a25.clk0_output_clock_enable = "ena0";
defparam ram_block1a25.data_interleave_offset_in_bits = 1;
defparam ram_block1a25.data_interleave_width_in_bits = 1;
defparam ram_block1a25.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a25.init_file_layout = "port_a";
defparam ram_block1a25.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a25.operation_mode = "rom";
defparam ram_block1a25.port_a_address_clear = "none";
defparam ram_block1a25.port_a_address_width = 13;
defparam ram_block1a25.port_a_data_out_clear = "none";
defparam ram_block1a25.port_a_data_out_clock = "clock0";
defparam ram_block1a25.port_a_data_width = 1;
defparam ram_block1a25.port_a_first_address = 8192;
defparam ram_block1a25.port_a_first_bit_number = 7;
defparam ram_block1a25.port_a_last_address = 16383;
defparam ram_block1a25.port_a_logical_ram_depth = 65536;
defparam ram_block1a25.port_a_logical_ram_width = 18;
defparam ram_block1a25.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a25.ram_block_type = "auto";
defparam ram_block1a25.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000007FFFFFFFFFFFFFFFFFFFFFE000000000000000000000FFFFFFFFFFFFFFFFFFFE0000000000000000003FFFFFFFFFFFFFFFFF800000000000000003FFFFFFFFFFFFFFFE0000000000000003FFFFFFFFFFFFFFE000000000000003FFFFFFFFFFFFFE00000000000003FFFFFFFFFFFFF000000000;
defparam ram_block1a25.mem_init2 = 2048'h0000FFFFFFFFFFFFE0000000000007FFFFFFFFFFFC000000000003FFFFFFFFFFF800000000001FFFFFFFFFFF00000000000FFFFFFFFFFF00000000003FFFFFFFFFF00000000003FFFFFFFFFE0000000001FFFFFFFFFC0000000007FFFFFFFFF0000000003FFFFFFFFE000000000FFFFFFFFF8000000007FFFFFFFF800000000FFFFFFFFE000000003FFFFFFFF000000003FFFFFFFE00000000FFFFFFFF800000003FFFFFFFC00000003FFFFFFF800000007FFFFFFF00000001FFFFFFF80000000FFFFFFFC0000000FFFFFFF80000001FFFFFFF00000007FFFFFF80000003FFFFFFC0000003FFFFFFC0000003FFFFFFC0000007FFFFFF0000001FFFFFFC000000;
defparam ram_block1a25.mem_init1 = 2048'h7FFFFFE0000003FFFFFF0000003FFFFFF0000003FFFFFE0000007FFFFFC000001FFFFFF0000007FFFFFC000001FFFFFE000000FFFFFF000000FFFFFF000000FFFFFF000001FFFFFE000003FFFFFC000007FFFFF000001FFFFFC000007FFFFF000003FFFFF800001FFFFFC00000FFFFFC00000FFFFFC00000FFFFFC00000FFFFF800001FFFFF000003FFFFE00000FFFFF800001FFFFF00000FFFFF800003FFFFE00001FFFFF00000FFFFF800007FFFFC00003FFFFC00003FFFFC00003FFFFC00003FFFF800007FFFF80000FFFFF00001FFFFE00003FFFF800007FFFF00001FFFFC00007FFFF00001FFFFC0000FFFFE00003FFFF00001FFFFC0000FFFFE00007FF;
defparam ram_block1a25.mem_init0 = 2048'hFE00003FFFF00003FFFF00003FFFF00003FFFF00003FFFF00003FFFF00007FFFE00007FFFC0000FFFFC0001FFFF00003FFFE00007FFFC0001FFFF00003FFFE0000FFFF80003FFFE0000FFFF80003FFFC0001FFFF00007FFF80003FFFE0001FFFF0000FFFF80007FFFC0003FFFE0001FFFE0001FFFF0000FFFF0000FFFF0000FFFF00007FFF8000FFFF0000FFFF0000FFFF0000FFFE0001FFFE0001FFFC0003FFF80007FFF8000FFFF0001FFFC0003FFF80007FFF0001FFFE0003FFF8000FFFE0001FFFC0007FFF0001FFFC0007FFF0001FFFC0007FFF0001FFFC000FFFE0003FFF8000FFFC0007FFF0003FFF8000FFFC0007FFE0003FFF0001FFF8000FFFC000;

cycloneive_ram_block ram_block1a43(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a43_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a43.clk0_core_clock_enable = "ena0";
defparam ram_block1a43.clk0_input_clock_enable = "ena0";
defparam ram_block1a43.clk0_output_clock_enable = "ena0";
defparam ram_block1a43.data_interleave_offset_in_bits = 1;
defparam ram_block1a43.data_interleave_width_in_bits = 1;
defparam ram_block1a43.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a43.init_file_layout = "port_a";
defparam ram_block1a43.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a43.operation_mode = "rom";
defparam ram_block1a43.port_a_address_clear = "none";
defparam ram_block1a43.port_a_address_width = 13;
defparam ram_block1a43.port_a_data_out_clear = "none";
defparam ram_block1a43.port_a_data_out_clock = "clock0";
defparam ram_block1a43.port_a_data_width = 1;
defparam ram_block1a43.port_a_first_address = 16384;
defparam ram_block1a43.port_a_first_bit_number = 7;
defparam ram_block1a43.port_a_last_address = 24575;
defparam ram_block1a43.port_a_logical_ram_depth = 65536;
defparam ram_block1a43.port_a_logical_ram_width = 18;
defparam ram_block1a43.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a43.ram_block_type = "auto";
defparam ram_block1a43.mem_init3 = 2048'h0007FFE0003FFF0001FFF8000FFFC0007FFE0003FFF8001FFFC0007FFE0003FFF8000FFFE0007FFF0001FFFC0007FFF0001FFFC0007FFF0001FFFC0007FFF0000FFFE0003FFF8000FFFF0001FFFC0003FFF80007FFF0001FFFE0003FFFC0003FFF80007FFF0000FFFF0000FFFE0001FFFE0001FFFE0001FFFE0003FFFC0001FFFE0001FFFE0001FFFE0001FFFF0000FFFF0000FFFF80007FFFC0003FFFE0001FFFF0000FFFF80003FFFC0001FFFF00007FFF80003FFFE0000FFFF80003FFFE0000FFFF80001FFFF00007FFFC0000FFFF80001FFFF00007FFFE00007FFFC0000FFFFC0001FFFF80001FFFF80001FFFF80001FFFF80001FFFF80001FFFF80000FF;
defparam ram_block1a43.mem_init2 = 2048'hFFC0000FFFFE00007FFFF00001FFFF80000FFFFE00007FFFF00001FFFFC00007FFFF00001FFFFC00003FFFF80000FFFFF00001FFFFE00003FFFFC00003FFFF800007FFFF800007FFFF800007FFFF800007FFFFC00003FFFFE00001FFFFF00000FFFFF800003FFFFE00001FFFFF000003FFFFE00000FFFFF800001FFFFF000003FFFFE000007FFFFE000007FFFFE000007FFFFE000007FFFFF000003FFFFF800001FFFFFC000007FFFFF000001FFFFFC000007FFFFF800000FFFFFF000001FFFFFE000001FFFFFE000001FFFFFE000000FFFFFF0000007FFFFFC000001FFFFFF0000007FFFFFC000000FFFFFF8000001FFFFFF8000001FFFFFF8000000FFFFFFC;
defparam ram_block1a43.mem_init1 = 2048'h0000007FFFFFF0000001FFFFFFC0000007FFFFFF80000007FFFFFF80000007FFFFFF80000003FFFFFFC0000001FFFFFFF00000003FFFFFFE00000007FFFFFFE00000003FFFFFFF00000001FFFFFFFC00000003FFFFFFF800000007FFFFFFF800000003FFFFFFFE00000000FFFFFFFF800000001FFFFFFFF800000000FFFFFFFFE000000003FFFFFFFFC000000003FFFFFFFFE000000000FFFFFFFFF8000000001FFFFFFFFFC0000000007FFFFFFFFF0000000000FFFFFFFFFF80000000001FFFFFFFFFF80000000001FFFFFFFFFFE00000000001FFFFFFFFFFF000000000003FFFFFFFFFFF8000000000007FFFFFFFFFFFC000000000000FFFFFFFFFFFFE0000;
defparam ram_block1a43.mem_init0 = 2048'h000000001FFFFFFFFFFFFF80000000000000FFFFFFFFFFFFFF800000000000000FFFFFFFFFFFFFFF8000000000000000FFFFFFFFFFFFFFFF800000000000000003FFFFFFFFFFFFFFFFF8000000000000000000FFFFFFFFFFFFFFFFFFFE000000000000000000000FFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a7(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a7_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a7.clk0_core_clock_enable = "ena0";
defparam ram_block1a7.clk0_input_clock_enable = "ena0";
defparam ram_block1a7.clk0_output_clock_enable = "ena0";
defparam ram_block1a7.data_interleave_offset_in_bits = 1;
defparam ram_block1a7.data_interleave_width_in_bits = 1;
defparam ram_block1a7.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a7.init_file_layout = "port_a";
defparam ram_block1a7.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a7.operation_mode = "rom";
defparam ram_block1a7.port_a_address_clear = "none";
defparam ram_block1a7.port_a_address_width = 13;
defparam ram_block1a7.port_a_data_out_clear = "none";
defparam ram_block1a7.port_a_data_out_clock = "clock0";
defparam ram_block1a7.port_a_data_width = 1;
defparam ram_block1a7.port_a_first_address = 0;
defparam ram_block1a7.port_a_first_bit_number = 7;
defparam ram_block1a7.port_a_last_address = 8191;
defparam ram_block1a7.port_a_logical_ram_depth = 65536;
defparam ram_block1a7.port_a_logical_ram_width = 18;
defparam ram_block1a7.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a7.ram_block_type = "auto";
defparam ram_block1a7.mem_init3 = 2048'h7FFE0007FFF0003FFF0001FFF8001FFF8000FFFC000FFFC000FFFC0007FFE0007FFE0007FFE0007FFE0007FFE0007FFE000FFFC000FFFC000FFFC001FFF8001FFF8001FFF0003FFF0007FFE0007FFC000FFF8001FFF0003FFF0007FFE000FFF8001FFF0003FFE0007FFC001FFF8003FFE0007FFC001FFF0003FFE000FFF8003FFF0007FFC001FFF0007FFC001FFF8003FFE000FFF8003FFE000FFF8003FFE001FFF0007FFC001FFF0007FFC003FFE000FFF8003FFC001FFF0007FF8003FFE001FFF0007FF8003FFE001FFF0007FF8003FFC001FFE000FFF0007FF8003FFC001FFE000FFF0007FF8007FFC003FFE001FFE000FFF000FFF8007FF8003FFC003FFC;
defparam ram_block1a7.mem_init2 = 2048'h001FFE001FFE001FFF000FFF000FFF0007FF8007FF8007FF8007FF8007FF8003FFC003FFC003FFC003FFC003FFC003FFC003FFC003FF8007FF8007FF8007FF8007FF800FFF000FFF000FFF001FFE001FFE001FFC003FFC003FF8007FF8007FF000FFF001FFE001FFC003FFC007FF8007FF000FFF001FFE003FFC007FF8007FF000FFE001FFC003FF8007FF000FFE001FFC003FF8007FF000FFE003FFC007FF800FFE001FFC003FF800FFF001FFC003FF800FFF001FFC003FF800FFF001FFC007FF800FFE003FFC007FF001FFE003FF800FFE001FFC007FF001FFE003FF800FFE003FF8007FF001FFC007FF001FFC007FF000FFE003FF800FFE003FF800FFE003;
defparam ram_block1a7.mem_init1 = 2048'hFF800FFE003FF800FFE003FF800FFE003FF800FFE003FF800FFE003FF801FFC007FF001FFC007FF001FFC007FE003FF800FFE003FF801FFC007FF001FFC00FFE003FF800FFC007FF001FFC00FFE003FF800FFC007FF001FF800FFE003FF001FFC00FFE003FF801FFC007FE003FF800FFC007FF003FF800FFC007FF003FF800FFC007FE003FF801FFC007FE003FF001FFC00FFE007FF001FF800FFC007FF003FF801FFC00FFE003FF001FF800FFC007FE003FF001FF800FFC007FF003FF801FFC00FFE007FF003FF801FF800FFC007FE003FF001FF800FFC007FE003FF003FF801FFC00FFE007FE003FF001FF800FFC00FFE007FF003FF001FF800FFC00FFE007;
defparam ram_block1a7.mem_init0 = 2048'hFF003FF001FF801FFC00FFE007FE003FF003FF801FF800FFC00FFE007FF003FF001FF801FFC00FFC007FE007FE003FF003FF801FF800FFC00FFE007FE003FF003FF001FF801FFC00FFC00FFE007FE003FF003FF001FF801FFC00FFC00FFE007FE007FF003FF003FF801FF801FFC00FFC00FFE007FE007FF003FF003FF801FF801FFC00FFC00FFC007FE007FE003FF003FF001FF801FF801FFC00FFC00FFC007FE007FE003FF003FF003FF801FF801FF800FFC00FFC00FFE007FE007FE003FF003FF003FF801FF801FF800FFC00FFC00FFE007FE007FE003FF003FF003FF001FF801FF801FFC00FFC00FFC007FE007FE007FE003FF003FF003FF801FF801FF800;

cycloneive_ram_block ram_block1a61(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a61_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a61.clk0_core_clock_enable = "ena0";
defparam ram_block1a61.clk0_input_clock_enable = "ena0";
defparam ram_block1a61.clk0_output_clock_enable = "ena0";
defparam ram_block1a61.data_interleave_offset_in_bits = 1;
defparam ram_block1a61.data_interleave_width_in_bits = 1;
defparam ram_block1a61.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a61.init_file_layout = "port_a";
defparam ram_block1a61.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a61.operation_mode = "rom";
defparam ram_block1a61.port_a_address_clear = "none";
defparam ram_block1a61.port_a_address_width = 13;
defparam ram_block1a61.port_a_data_out_clear = "none";
defparam ram_block1a61.port_a_data_out_clock = "clock0";
defparam ram_block1a61.port_a_data_width = 1;
defparam ram_block1a61.port_a_first_address = 24576;
defparam ram_block1a61.port_a_first_bit_number = 7;
defparam ram_block1a61.port_a_last_address = 32767;
defparam ram_block1a61.port_a_logical_ram_depth = 65536;
defparam ram_block1a61.port_a_logical_ram_width = 18;
defparam ram_block1a61.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a61.ram_block_type = "auto";
defparam ram_block1a61.mem_init3 = 2048'h003FF003FF003FF801FF801FF800FFC00FFC00FFC007FE007FE007FF003FF003FF001FF801FF801FF800FFC00FFC00FFE007FE007FE003FF003FF003FF801FF801FF800FFC00FFC00FFE007FE007FE003FF003FF003FF801FF801FF800FFC00FFC007FE007FE007FF003FF003FF001FF801FF800FFC00FFC007FE007FE007FF003FF003FF801FF801FFC00FFC00FFE007FE007FF003FF003FF801FF801FFC00FFC00FFE007FE007FF003FF001FF801FF800FFC00FFE007FE007FF003FF001FF801FF800FFC00FFE007FE003FF003FF801FF800FFC00FFC007FE007FF003FF001FF801FFC00FFE007FE003FF003FF801FF800FFC00FFE007FF003FF001FF801FF;
defparam ram_block1a61.mem_init2 = 2048'hC00FFE007FE003FF001FF801FFC00FFE007FE003FF001FF800FFC00FFE007FF003FF801FF800FFC007FE003FF001FF800FFC007FE003FF003FF801FFC00FFE007FF003FF801FFC007FE003FF001FF800FFC007FE003FF001FF800FFE007FF003FF801FFC007FE003FF001FFC00FFE007FF001FF800FFC007FF003FF800FFC007FE003FF801FFC007FE003FF801FFC007FE003FF800FFC007FF003FF800FFE007FF001FF800FFE003FF001FFC007FE003FF800FFE007FF001FFC007FE003FF800FFE007FF001FFC007FF003FF800FFE003FF800FFC007FF001FFC007FF001FFC007FF003FF800FFE003FF800FFE003FF800FFE003FF800FFE003FF800FFE003FF;
defparam ram_block1a61.mem_init1 = 2048'h800FFE003FF800FFE003FF800FFE001FFC007FF001FFC007FF001FFC003FF800FFE003FF800FFF001FFC007FF000FFE003FF800FFF001FFC007FF800FFE003FFC007FF001FFE003FF8007FF001FFE003FF8007FF001FFE003FF8007FF000FFE003FFC007FF800FFE001FFC003FF8007FF000FFE001FFC003FF8007FF000FFE001FFC003FFC007FF800FFF001FFE001FFC003FFC007FF8007FF000FFF001FFE001FFC003FFC003FF8007FF8007FF000FFF000FFF001FFE001FFE001FFE003FFC003FFC003FFC003FFC003FF8007FF8007FF8007FF8007FF8007FF8007FF8007FF8003FFC003FFC003FFC003FFC003FFC001FFE001FFE001FFF000FFF000FFF000;
defparam ram_block1a61.mem_init0 = 2048'h7FF8007FF8003FFC003FFE001FFE000FFF000FFF8007FFC003FFC001FFE000FFF0007FF8003FFC001FFE000FFF0007FF8003FFC001FFF000FFF8003FFC001FFF000FFF8003FFC001FFF0007FF8003FFE000FFF8007FFC001FFF0007FFC001FFF000FFF8003FFE000FFF8003FFE000FFF8003FFF0007FFC001FFF0007FFC001FFF8003FFE000FFF8001FFF0007FFC000FFF8003FFF0007FFC000FFF8001FFF0003FFE000FFFC001FFF8001FFF0003FFE0007FFC000FFFC001FFF8001FFF0003FFF0003FFF0007FFE0007FFE0007FFE000FFFC000FFFC000FFFC000FFFC000FFFC000FFFC0007FFE0007FFE0007FFE0003FFF0003FFF0001FFF8001FFFC000FFFC;

cycloneive_ram_block ram_block1a116(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a116_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a116.clk0_core_clock_enable = "ena0";
defparam ram_block1a116.clk0_input_clock_enable = "ena0";
defparam ram_block1a116.clk0_output_clock_enable = "ena0";
defparam ram_block1a116.data_interleave_offset_in_bits = 1;
defparam ram_block1a116.data_interleave_width_in_bits = 1;
defparam ram_block1a116.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a116.init_file_layout = "port_a";
defparam ram_block1a116.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a116.operation_mode = "rom";
defparam ram_block1a116.port_a_address_clear = "none";
defparam ram_block1a116.port_a_address_width = 13;
defparam ram_block1a116.port_a_data_out_clear = "none";
defparam ram_block1a116.port_a_data_out_clock = "clock0";
defparam ram_block1a116.port_a_data_width = 1;
defparam ram_block1a116.port_a_first_address = 49152;
defparam ram_block1a116.port_a_first_bit_number = 8;
defparam ram_block1a116.port_a_last_address = 57343;
defparam ram_block1a116.port_a_logical_ram_depth = 65536;
defparam ram_block1a116.port_a_logical_ram_width = 18;
defparam ram_block1a116.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a116.ram_block_type = "auto";
defparam ram_block1a116.mem_init3 = 2048'hFFFFFFE0000000FFFFFFF80000003FFFFFFF00000007FFFFFFC0000000FFFFFFF80000001FFFFFFF00000003FFFFFFF00000003FFFFFFF00000003FFFFFFF80000001FFFFFFF80000000FFFFFFFE00000007FFFFFFF00000001FFFFFFFC00000007FFFFFFF80000000FFFFFFFE00000001FFFFFFFE00000001FFFFFFFE00000001FFFFFFFE00000001FFFFFFFF00000000FFFFFFFF800000003FFFFFFFE00000000FFFFFFFF800000003FFFFFFFF000000003FFFFFFFE000000007FFFFFFFE000000003FFFFFFFF000000001FFFFFFFF800000000FFFFFFFFE000000003FFFFFFFFC000000007FFFFFFFF8000000007FFFFFFFF8000000007FFFFFFFFC000000;
defparam ram_block1a116.mem_init2 = 2048'h003FFFFFFFFE000000000FFFFFFFFF8000000001FFFFFFFFF0000000003FFFFFFFFF0000000001FFFFFFFFF8000000000FFFFFFFFFE0000000003FFFFFFFFF80000000007FFFFFFFFF80000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFE00000000007FFFFFFFFFE00000000007FFFFFFFFFF00000000001FFFFFFFFFFE00000000001FFFFFFFFFFE00000000000FFFFFFFFFFF800000000003FFFFFFFFFFF000000000003FFFFFFFFFFF800000000000FFFFFFFFFFFE000000000001FFFFFFFFFFFF0000000000007FFFFFFFFFFFC000000000000FFFFFFFFFFFFC0000000000003FFFFFFFFFFFF80000000000007FFFFFFFFFFFFC;
defparam ram_block1a116.mem_init1 = 2048'h0000000000000FFFFFFFFFFFFFC00000000000007FFFFFFFFFFFFF800000000000007FFFFFFFFFFFFFE000000000000007FFFFFFFFFFFFFF000000000000000FFFFFFFFFFFFFFF0000000000000003FFFFFFFFFFFFFFF80000000000000003FFFFFFFFFFFFFFFE00000000000000003FFFFFFFFFFFFFFFFC00000000000000001FFFFFFFFFFFFFFFFFC000000000000000001FFFFFFFFFFFFFFFFFFC0000000000000000003FFFFFFFFFFFFFFFFFFF800000000000000000003FFFFFFFFFFFFFFFFFFFF8000000000000000000001FFFFFFFFFFFFFFFFFFFFFF800000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000001FFFF;
defparam ram_block1a116.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a98(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a98_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a98.clk0_core_clock_enable = "ena0";
defparam ram_block1a98.clk0_input_clock_enable = "ena0";
defparam ram_block1a98.clk0_output_clock_enable = "ena0";
defparam ram_block1a98.data_interleave_offset_in_bits = 1;
defparam ram_block1a98.data_interleave_width_in_bits = 1;
defparam ram_block1a98.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a98.init_file_layout = "port_a";
defparam ram_block1a98.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a98.operation_mode = "rom";
defparam ram_block1a98.port_a_address_clear = "none";
defparam ram_block1a98.port_a_address_width = 13;
defparam ram_block1a98.port_a_data_out_clear = "none";
defparam ram_block1a98.port_a_data_out_clock = "clock0";
defparam ram_block1a98.port_a_data_width = 1;
defparam ram_block1a98.port_a_first_address = 40960;
defparam ram_block1a98.port_a_first_bit_number = 8;
defparam ram_block1a98.port_a_last_address = 49151;
defparam ram_block1a98.port_a_logical_ram_depth = 65536;
defparam ram_block1a98.port_a_logical_ram_width = 18;
defparam ram_block1a98.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a98.ram_block_type = "auto";
defparam ram_block1a98.mem_init3 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a98.mem_init2 = 2048'hFFFF00000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000003FFFFFFFFFFFFFFFFFFFFFF0000000000000000000003FFFFFFFFFFFFFFFFFFFF800000000000000000003FFFFFFFFFFFFFFFFFFF80000000000000000007FFFFFFFFFFFFFFFFFF0000000000000000007FFFFFFFFFFFFFFFFF000000000000000007FFFFFFFFFFFFFFFF80000000000000000FFFFFFFFFFFFFFFF80000000000000003FFFFFFFFFFFFFFF8000000000000001FFFFFFFFFFFFFFE000000000000001FFFFFFFFFFFFFFC00000000000000FFFFFFFFFFFFFFC00000000000003FFFFFFFFFFFFFC00000000000007FFFFFFFFFFFFE0000000000000;
defparam ram_block1a98.mem_init1 = 2048'h7FFFFFFFFFFFFC0000000000003FFFFFFFFFFFF80000000000007FFFFFFFFFFFE0000000000007FFFFFFFFFFFC000000000001FFFFFFFFFFFF000000000000FFFFFFFFFFFE000000000003FFFFFFFFFFF800000000001FFFFFFFFFFF800000000003FFFFFFFFFFE00000000000FFFFFFFFFFF00000000000FFFFFFFFFFF00000000001FFFFFFFFFFC0000000000FFFFFFFFFFC0000000000FFFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000003FFFFFFFFFC0000000003FFFFFFFFF8000000000FFFFFFFFFE0000000003FFFFFFFFF0000000001FFFFFFFFF8000000001FFFFFFFFF0000000003FFFFFFFFE000000000FFFFFFFFF800;
defparam ram_block1a98.mem_init0 = 2048'h0000007FFFFFFFFC000000003FFFFFFFFC000000003FFFFFFFFC000000007FFFFFFFF800000000FFFFFFFFE000000003FFFFFFFF000000001FFFFFFFF800000000FFFFFFFFC00000000FFFFFFFF800000001FFFFFFFF800000003FFFFFFFE00000000FFFFFFFF800000003FFFFFFFE00000001FFFFFFFF00000000FFFFFFFF00000000FFFFFFFF00000000FFFFFFFF00000000FFFFFFFE00000003FFFFFFFC00000007FFFFFFF00000001FFFFFFFC0000000FFFFFFFE00000003FFFFFFF00000003FFFFFFF80000001FFFFFFF80000001FFFFFFF80000001FFFFFFF00000003FFFFFFE00000007FFFFFFC0000001FFFFFFF80000003FFFFFFE0000000FFFFFFF;

cycloneive_ram_block ram_block1a80(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a80_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a80.clk0_core_clock_enable = "ena0";
defparam ram_block1a80.clk0_input_clock_enable = "ena0";
defparam ram_block1a80.clk0_output_clock_enable = "ena0";
defparam ram_block1a80.data_interleave_offset_in_bits = 1;
defparam ram_block1a80.data_interleave_width_in_bits = 1;
defparam ram_block1a80.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a80.init_file_layout = "port_a";
defparam ram_block1a80.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a80.operation_mode = "rom";
defparam ram_block1a80.port_a_address_clear = "none";
defparam ram_block1a80.port_a_address_width = 13;
defparam ram_block1a80.port_a_data_out_clear = "none";
defparam ram_block1a80.port_a_data_out_clock = "clock0";
defparam ram_block1a80.port_a_data_width = 1;
defparam ram_block1a80.port_a_first_address = 32768;
defparam ram_block1a80.port_a_first_bit_number = 8;
defparam ram_block1a80.port_a_last_address = 40959;
defparam ram_block1a80.port_a_logical_ram_depth = 65536;
defparam ram_block1a80.port_a_logical_ram_width = 18;
defparam ram_block1a80.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a80.ram_block_type = "auto";
defparam ram_block1a80.mem_init3 = 2048'h00000007FFFFFFC0000001FFFFFFE0000000FFFFFFF0000000FFFFFFF00000007FFFFFF80000007FFFFFF80000007FFFFFF0000000FFFFFFF0000001FFFFFFE0000003FFFFFFC0000007FFFFFF0000000FFFFFFE0000003FFFFFF8000000FFFFFFE0000003FFFFFF0000001FFFFFFC000000FFFFFFE0000003FFFFFF0000003FFFFFF8000001FFFFFF8000001FFFFFFC000000FFFFFFC000000FFFFFF8000001FFFFFF8000001FFFFFF8000003FFFFFF0000003FFFFFE0000007FFFFFC000001FFFFFF8000003FFFFFE000000FFFFFFC000001FFFFFF0000007FFFFFC000001FFFFFF0000007FFFFF8000003FFFFFE000000FFFFFF0000007FFFFF8000003FFF;
defparam ram_block1a80.mem_init2 = 2048'hFFC000001FFFFFE000000FFFFFF000000FFFFFF8000007FFFFF8000007FFFFF8000003FFFFFC000003FFFFFC000003FFFFFC000007FFFFF8000007FFFFF8000007FFFFF000000FFFFFF000001FFFFFE000001FFFFFC000003FFFFF8000007FFFFF000001FFFFFE000003FFFFF8000007FFFFF000001FFFFFC000007FFFFF800000FFFFFE000003FFFFF800000FFFFFE000003FFFFF800001FFFFFC000007FFFFF000001FFFFFC00000FFFFFE000003FFFFF000001FFFFFC00000FFFFFE000007FFFFF000003FFFFF800001FFFFFC00000FFFFFE000007FFFFE000003FFFFF000003FFFFF000001FFFFF800001FFFFF800001FFFFFC00000FFFFFC00000FFFFFC;
defparam ram_block1a80.mem_init1 = 2048'h00000FFFFFC00000FFFFFC00000FFFFFC00000FFFFFC00000FFFFFC00001FFFFF800001FFFFF800001FFFFF000003FFFFF000003FFFFE000007FFFFE00000FFFFFC00000FFFFF800001FFFFF000003FFFFE000007FFFFE00000FFFFF800001FFFFF000003FFFFE000007FFFFC00001FFFFF800003FFFFF000007FFFFC00001FFFFF800003FFFFE000007FFFFC00001FFFFF000007FFFFE00000FFFFF800003FFFFE00000FFFFF800001FFFFF000007FFFFC00001FFFFF000007FFFFC00001FFFFF000007FFFFC00001FFFFF000007FFFFC00001FFFFF000007FFFF800003FFFFE00000FFFFF800003FFFFE00001FFFFF000007FFFFC00001FFFFF00000FFFFF8;
defparam ram_block1a80.mem_init0 = 2048'h00003FFFFE00001FFFFF000007FFFFC00003FFFFE00000FFFFF000007FFFFC00003FFFFE00000FFFFF000007FFFFC00003FFFFE00000FFFFF000007FFFF800003FFFFE00001FFFFF00000FFFFF800003FFFFC00001FFFFE00000FFFFF000007FFFF800003FFFFC00001FFFFE00000FFFFF000007FFFF800003FFFFC00001FFFFE00000FFFFF000007FFFF800003FFFFC00003FFFFE00001FFFFF00000FFFFF800007FFFF800003FFFFC00001FFFFE00000FFFFF00000FFFFF800007FFFFC00003FFFFC00001FFFFE00000FFFFF00000FFFFF800007FFFF800003FFFFC00001FFFFE00001FFFFF00000FFFFF000007FFFF800003FFFFC00003FFFFE00001FFFFE;

cycloneive_ram_block ram_block1a134(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a134_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a134.clk0_core_clock_enable = "ena0";
defparam ram_block1a134.clk0_input_clock_enable = "ena0";
defparam ram_block1a134.clk0_output_clock_enable = "ena0";
defparam ram_block1a134.data_interleave_offset_in_bits = 1;
defparam ram_block1a134.data_interleave_width_in_bits = 1;
defparam ram_block1a134.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a134.init_file_layout = "port_a";
defparam ram_block1a134.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a134.operation_mode = "rom";
defparam ram_block1a134.port_a_address_clear = "none";
defparam ram_block1a134.port_a_address_width = 13;
defparam ram_block1a134.port_a_data_out_clear = "none";
defparam ram_block1a134.port_a_data_out_clock = "clock0";
defparam ram_block1a134.port_a_data_width = 1;
defparam ram_block1a134.port_a_first_address = 57344;
defparam ram_block1a134.port_a_first_bit_number = 8;
defparam ram_block1a134.port_a_last_address = 65535;
defparam ram_block1a134.port_a_logical_ram_depth = 65536;
defparam ram_block1a134.port_a_logical_ram_width = 18;
defparam ram_block1a134.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a134.ram_block_type = "auto";
defparam ram_block1a134.mem_init3 = 2048'hFFFFF00000FFFFF800007FFFF800003FFFFC00001FFFFE00001FFFFF00000FFFFF000007FFFF800003FFFFC00003FFFFE00001FFFFE00000FFFFF000007FFFF800007FFFFC00003FFFFE00001FFFFE00000FFFFF000007FFFF800003FFFFC00003FFFFE00001FFFFF00000FFFFF800007FFFF800003FFFFC00001FFFFE00000FFFFF000007FFFF800003FFFFC00001FFFFE00000FFFFF000007FFFF800003FFFFC00001FFFFE00000FFFFF000007FFFF800003FFFFE00001FFFFF00000FFFFF800003FFFFC00001FFFFE00000FFFFF800007FFFFC00001FFFFE00000FFFFF800007FFFFC00001FFFFE00000FFFFF800007FFFFC00001FFFFF00000FFFFF80000;
defparam ram_block1a134.mem_init2 = 2048'h3FFFFE00001FFFFF000007FFFFC00001FFFFF00000FFFFF800003FFFFE00000FFFFF800003FFFFC00001FFFFF000007FFFFC00001FFFFF000007FFFFC00001FFFFF000007FFFFC00001FFFFF000007FFFFC00001FFFFF000003FFFFE00000FFFFF800003FFFFE00000FFFFFC00001FFFFF000007FFFFC00000FFFFF800003FFFFF000007FFFFC00001FFFFF800003FFFFF000007FFFFC00000FFFFF800001FFFFF000003FFFFE00000FFFFFC00000FFFFF800001FFFFF000003FFFFE000007FFFFE00000FFFFFC00000FFFFF800001FFFFF800001FFFFF000003FFFFF000003FFFFF000007FFFFE000007FFFFE000007FFFFE000007FFFFE000007FFFFE00000;
defparam ram_block1a134.mem_init1 = 2048'h7FFFFE000007FFFFE000007FFFFF000003FFFFF000003FFFFF000001FFFFF800001FFFFF800000FFFFFC00000FFFFFE000007FFFFF000003FFFFF800001FFFFFC00000FFFFFE000007FFFFF000001FFFFF800000FFFFFE000007FFFFF000001FFFFFC000007FFFFF000003FFFFF800000FFFFFE000003FFFFF800000FFFFFE000003FFFFFC000007FFFFF000001FFFFFC000003FFFFF800000FFFFFF000001FFFFFC000003FFFFF8000007FFFFF000000FFFFFF000001FFFFFE000001FFFFFC000003FFFFFC000003FFFFFC000007FFFFF8000007FFFFF8000007FFFFF8000003FFFFFC000003FFFFFC000003FFFFFE000001FFFFFE000000FFFFFF0000007FF;
defparam ram_block1a134.mem_init0 = 2048'hFFF8000003FFFFFC000001FFFFFE000000FFFFFF8000003FFFFFC000001FFFFFF0000007FFFFFC000001FFFFFF0000007FFFFFE000000FFFFFF8000003FFFFFF0000007FFFFFC000000FFFFFF8000001FFFFFF8000003FFFFFF0000003FFFFFF0000003FFFFFE0000007FFFFFE0000007FFFFFF0000003FFFFFF0000003FFFFFF8000001FFFFFF8000000FFFFFFE0000007FFFFFF0000001FFFFFF8000000FFFFFFE0000003FFFFFF8000000FFFFFFE0000001FFFFFFC0000007FFFFFF8000000FFFFFFF0000001FFFFFFE0000001FFFFFFC0000003FFFFFFC0000003FFFFFFC0000001FFFFFFE0000001FFFFFFE0000000FFFFFFF00000007FFFFFFC0000001;

cycloneive_ram_block ram_block1a26(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a26_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a26.clk0_core_clock_enable = "ena0";
defparam ram_block1a26.clk0_input_clock_enable = "ena0";
defparam ram_block1a26.clk0_output_clock_enable = "ena0";
defparam ram_block1a26.data_interleave_offset_in_bits = 1;
defparam ram_block1a26.data_interleave_width_in_bits = 1;
defparam ram_block1a26.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a26.init_file_layout = "port_a";
defparam ram_block1a26.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a26.operation_mode = "rom";
defparam ram_block1a26.port_a_address_clear = "none";
defparam ram_block1a26.port_a_address_width = 13;
defparam ram_block1a26.port_a_data_out_clear = "none";
defparam ram_block1a26.port_a_data_out_clock = "clock0";
defparam ram_block1a26.port_a_data_width = 1;
defparam ram_block1a26.port_a_first_address = 8192;
defparam ram_block1a26.port_a_first_bit_number = 8;
defparam ram_block1a26.port_a_last_address = 16383;
defparam ram_block1a26.port_a_logical_ram_depth = 65536;
defparam ram_block1a26.port_a_logical_ram_width = 18;
defparam ram_block1a26.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a26.ram_block_type = "auto";
defparam ram_block1a26.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000;
defparam ram_block1a26.mem_init2 = 2048'h0000FFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000FFFFFFFFFFFFFFFFFFFFFC000000000000000000003FFFFFFFFFFFFFFFFFFFE00000000000000000007FFFFFFFFFFFFFFFFFFC000000000000000000FFFFFFFFFFFFFFFFFF800000000000000000FFFFFFFFFFFFFFFFFC00000000000000003FFFFFFFFFFFFFFFF00000000000000003FFFFFFFFFFFFFFFC0000000000000007FFFFFFFFFFFFFFE000000000000000FFFFFFFFFFFFFFF000000000000001FFFFFFFFFFFFFF800000000000003FFFFFFFFFFFFFC00000000000003FFFFFFFFFFFFF80000000000001FFFFFFFFFFFFF;
defparam ram_block1a26.mem_init1 = 2048'h80000000000003FFFFFFFFFFFFC0000000000003FFFFFFFFFFFF8000000000001FFFFFFFFFFFF8000000000001FFFFFFFFFFFF000000000000FFFFFFFFFFFF000000000001FFFFFFFFFFFC000000000007FFFFFFFFFFE000000000007FFFFFFFFFFC00000000001FFFFFFFFFFF00000000000FFFFFFFFFFF00000000000FFFFFFFFFFE00000000003FFFFFFFFFF00000000001FFFFFFFFFF00000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFC0000000003FFFFFFFFFC0000000007FFFFFFFFF0000000001FFFFFFFFFC0000000007FFFFFFFFE0000000007FFFFFFFFE000000000FFFFFFFFFC000000001FFFFFFFFF0000000007FF;
defparam ram_block1a26.mem_init0 = 2048'hFFFFFFC000000003FFFFFFFFC000000003FFFFFFFFC000000003FFFFFFFF8000000007FFFFFFFF000000001FFFFFFFFC000000007FFFFFFFE000000003FFFFFFFF000000003FFFFFFFF000000003FFFFFFFE000000007FFFFFFFC00000001FFFFFFFF000000007FFFFFFFC00000001FFFFFFFE00000000FFFFFFFF00000000FFFFFFFF80000000FFFFFFFF00000000FFFFFFFF00000001FFFFFFFE00000003FFFFFFF80000000FFFFFFFE00000003FFFFFFF80000001FFFFFFFC0000000FFFFFFFE00000007FFFFFFE00000007FFFFFFE00000007FFFFFFE0000000FFFFFFFC0000000FFFFFFF80000003FFFFFFF00000007FFFFFFC0000001FFFFFFF0000000;

cycloneive_ram_block ram_block1a44(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a44_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a44.clk0_core_clock_enable = "ena0";
defparam ram_block1a44.clk0_input_clock_enable = "ena0";
defparam ram_block1a44.clk0_output_clock_enable = "ena0";
defparam ram_block1a44.data_interleave_offset_in_bits = 1;
defparam ram_block1a44.data_interleave_width_in_bits = 1;
defparam ram_block1a44.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a44.init_file_layout = "port_a";
defparam ram_block1a44.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a44.operation_mode = "rom";
defparam ram_block1a44.port_a_address_clear = "none";
defparam ram_block1a44.port_a_address_width = 13;
defparam ram_block1a44.port_a_data_out_clear = "none";
defparam ram_block1a44.port_a_data_out_clock = "clock0";
defparam ram_block1a44.port_a_data_width = 1;
defparam ram_block1a44.port_a_first_address = 16384;
defparam ram_block1a44.port_a_first_bit_number = 8;
defparam ram_block1a44.port_a_last_address = 24575;
defparam ram_block1a44.port_a_logical_ram_depth = 65536;
defparam ram_block1a44.port_a_logical_ram_width = 18;
defparam ram_block1a44.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a44.ram_block_type = "auto";
defparam ram_block1a44.mem_init3 = 2048'h0000001FFFFFFF00000007FFFFFFC0000001FFFFFFF80000003FFFFFFE00000007FFFFFFE0000000FFFFFFFC0000000FFFFFFFC0000000FFFFFFFC0000000FFFFFFFE00000007FFFFFFF00000003FFFFFFF80000000FFFFFFFE00000003FFFFFFF80000000FFFFFFFF00000001FFFFFFFE00000001FFFFFFFE00000003FFFFFFFE00000001FFFFFFFE00000000FFFFFFFF000000007FFFFFFFC00000001FFFFFFFF000000007FFFFFFFC00000000FFFFFFFF800000001FFFFFFFF800000001FFFFFFFF800000000FFFFFFFFC000000007FFFFFFFF000000001FFFFFFFFC000000003FFFFFFFF8000000007FFFFFFFF8000000007FFFFFFFF8000000007FFFFFF;
defparam ram_block1a44.mem_init2 = 2048'hFFC000000001FFFFFFFFF0000000007FFFFFFFFE000000000FFFFFFFFFC000000000FFFFFFFFFC0000000007FFFFFFFFF0000000001FFFFFFFFFC0000000007FFFFFFFFF80000000007FFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000001FFFFFFFFFF00000000001FFFFFFFFFF80000000000FFFFFFFFFFE00000000001FFFFFFFFFFE00000000001FFFFFFFFFFF000000000007FFFFFFFFFFC00000000000FFFFFFFFFFFC000000000007FFFFFFFFFFF000000000001FFFFFFFFFFFE000000000001FFFFFFFFFFFF0000000000003FFFFFFFFFFFF0000000000003FFFFFFFFFFFF80000000000007FFFFFFFFFFFF80000000000003;
defparam ram_block1a44.mem_init1 = 2048'hFFFFFFFFFFFFF00000000000003FFFFFFFFFFFFF800000000000007FFFFFFFFFFFFF800000000000003FFFFFFFFFFFFFF000000000000001FFFFFFFFFFFFFFE000000000000000FFFFFFFFFFFFFFFC0000000000000007FFFFFFFFFFFFFFF80000000000000001FFFFFFFFFFFFFFFF800000000000000007FFFFFFFFFFFFFFFFE000000000000000003FFFFFFFFFFFFFFFFFE0000000000000000007FFFFFFFFFFFFFFFFFFC0000000000000000000FFFFFFFFFFFFFFFFFFFF8000000000000000000007FFFFFFFFFFFFFFFFFFFFE0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFE0000;
defparam ram_block1a44.mem_init0 = 2048'h00000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a8(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a8_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a8.clk0_core_clock_enable = "ena0";
defparam ram_block1a8.clk0_input_clock_enable = "ena0";
defparam ram_block1a8.clk0_output_clock_enable = "ena0";
defparam ram_block1a8.data_interleave_offset_in_bits = 1;
defparam ram_block1a8.data_interleave_width_in_bits = 1;
defparam ram_block1a8.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a8.init_file_layout = "port_a";
defparam ram_block1a8.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a8.operation_mode = "rom";
defparam ram_block1a8.port_a_address_clear = "none";
defparam ram_block1a8.port_a_address_width = 13;
defparam ram_block1a8.port_a_data_out_clear = "none";
defparam ram_block1a8.port_a_data_out_clock = "clock0";
defparam ram_block1a8.port_a_data_width = 1;
defparam ram_block1a8.port_a_first_address = 0;
defparam ram_block1a8.port_a_first_bit_number = 8;
defparam ram_block1a8.port_a_last_address = 8191;
defparam ram_block1a8.port_a_logical_ram_depth = 65536;
defparam ram_block1a8.port_a_logical_ram_width = 18;
defparam ram_block1a8.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a8.ram_block_type = "auto";
defparam ram_block1a8.mem_init3 = 2048'h7FFFFFF80000003FFFFFFE0000001FFFFFFF0000000FFFFFFF00000007FFFFFF80000007FFFFFF80000007FFFFFF8000000FFFFFFF0000000FFFFFFE0000001FFFFFFE0000003FFFFFF80000007FFFFFF0000001FFFFFFC0000007FFFFFF0000001FFFFFFC0000007FFFFFE0000003FFFFFF8000001FFFFFFC000000FFFFFFC0000007FFFFFE0000007FFFFFE0000003FFFFFF0000003FFFFFF0000003FFFFFE0000007FFFFFE0000007FFFFFC000000FFFFFFC000001FFFFFF8000003FFFFFE0000007FFFFFC000001FFFFFF8000003FFFFFE000000FFFFFF8000003FFFFFE000000FFFFFF8000007FFFFFC000001FFFFFF000000FFFFFF8000003FFFFFC000;
defparam ram_block1a8.mem_init2 = 2048'h001FFFFFE000001FFFFFF000000FFFFFF8000007FFFFF8000007FFFFF8000003FFFFFC000003FFFFFC000003FFFFFC000003FFFFFC000007FFFFF8000007FFFFF800000FFFFFF000000FFFFFE000001FFFFFE000003FFFFFC000007FFFFF800000FFFFFE000001FFFFFC000007FFFFF800000FFFFFE000003FFFFF8000007FFFFF000001FFFFFC000007FFFFF000001FFFFFC000007FFFFF000003FFFFF800000FFFFFE000003FFFFF000001FFFFFC00000FFFFFE000003FFFFF000001FFFFF800000FFFFFC000007FFFFE000003FFFFF000001FFFFF800001FFFFFC00000FFFFFC000007FFFFE000007FFFFE000007FFFFF000003FFFFF000003FFFFF000003;
defparam ram_block1a8.mem_init1 = 2048'hFFFFF000003FFFFF000003FFFFF000003FFFFF000003FFFFF000003FFFFE000007FFFFE000007FFFFE000007FFFFC00000FFFFFC00001FFFFF800001FFFFF000003FFFFF000007FFFFE00000FFFFFC00000FFFFF800001FFFFF000003FFFFE00000FFFFFC00001FFFFF800003FFFFF000007FFFFC00000FFFFF800003FFFFF000007FFFFC00001FFFFF800003FFFFE00000FFFFF800001FFFFF000007FFFFC00001FFFFF000003FFFFE00000FFFFF800003FFFFE00000FFFFF800003FFFFE00000FFFFF800003FFFFE00000FFFFF800003FFFFE00000FFFFF800003FFFFC00001FFFFF000007FFFFC00001FFFFF00000FFFFF800003FFFFE00000FFFFF000007;
defparam ram_block1a8.mem_init0 = 2048'hFFFFC00001FFFFE00000FFFFF800003FFFFC00001FFFFF00000FFFFF800003FFFFE00001FFFFF000007FFFF800003FFFFC00001FFFFF00000FFFFF800003FFFFC00001FFFFE00000FFFFF000007FFFFC00003FFFFE00001FFFFF00000FFFFF800007FFFFC00003FFFFE00001FFFFF00000FFFFF800007FFFFC00003FFFFE00001FFFFF00000FFFFF800007FFFFC00003FFFFE00001FFFFE00000FFFFF000007FFFF800003FFFFC00003FFFFE00001FFFFF00000FFFFF000007FFFF800003FFFFC00003FFFFE00001FFFFF00000FFFFF000007FFFF800003FFFFC00003FFFFE00001FFFFE00000FFFFF000007FFFF800007FFFFC00003FFFFC00001FFFFE00000;

cycloneive_ram_block ram_block1a62(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a62_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a62.clk0_core_clock_enable = "ena0";
defparam ram_block1a62.clk0_input_clock_enable = "ena0";
defparam ram_block1a62.clk0_output_clock_enable = "ena0";
defparam ram_block1a62.data_interleave_offset_in_bits = 1;
defparam ram_block1a62.data_interleave_width_in_bits = 1;
defparam ram_block1a62.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a62.init_file_layout = "port_a";
defparam ram_block1a62.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a62.operation_mode = "rom";
defparam ram_block1a62.port_a_address_clear = "none";
defparam ram_block1a62.port_a_address_width = 13;
defparam ram_block1a62.port_a_data_out_clear = "none";
defparam ram_block1a62.port_a_data_out_clock = "clock0";
defparam ram_block1a62.port_a_data_width = 1;
defparam ram_block1a62.port_a_first_address = 24576;
defparam ram_block1a62.port_a_first_bit_number = 8;
defparam ram_block1a62.port_a_last_address = 32767;
defparam ram_block1a62.port_a_logical_ram_depth = 65536;
defparam ram_block1a62.port_a_logical_ram_width = 18;
defparam ram_block1a62.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a62.ram_block_type = "auto";
defparam ram_block1a62.mem_init3 = 2048'h00000FFFFF000007FFFF800007FFFFC00003FFFFC00001FFFFE00000FFFFF00000FFFFF800007FFFF800003FFFFC00001FFFFE00001FFFFF00000FFFFF800007FFFF800003FFFFC00001FFFFE00001FFFFF00000FFFFF800007FFFF800003FFFFC00001FFFFE00000FFFFF00000FFFFF800007FFFFC00003FFFFE00001FFFFF00000FFFFF800007FFFFC00003FFFFE00001FFFFF00000FFFFF800007FFFFC00003FFFFE00001FFFFF00000FFFFF800007FFFFC00001FFFFE00000FFFFF000007FFFF800003FFFFE00001FFFFF000007FFFF800003FFFFC00001FFFFF00000FFFFF800003FFFFE00001FFFFF000007FFFF800003FFFFE00000FFFFF000007FFFF;
defparam ram_block1a62.mem_init2 = 2048'hC00001FFFFE00000FFFFF800003FFFFE00001FFFFF000007FFFFC00001FFFFF000007FFFF800003FFFFE00000FFFFF800003FFFFE00000FFFFF800003FFFFE00000FFFFF800003FFFFE00000FFFFF800003FFFFE00000FFFFF800001FFFFF000007FFFFC00001FFFFF000003FFFFE00000FFFFF800003FFFFF000007FFFFC00001FFFFF800003FFFFE000007FFFFC00001FFFFF800003FFFFF000007FFFFE00000FFFFF800001FFFFF000003FFFFE000007FFFFE00000FFFFFC00001FFFFF800001FFFFF000003FFFFF000007FFFFE000007FFFFC00000FFFFFC00000FFFFFC00000FFFFF800001FFFFF800001FFFFF800001FFFFF800001FFFFF800001FFFFF;
defparam ram_block1a62.mem_init1 = 2048'h800001FFFFF800001FFFFF800001FFFFFC00000FFFFFC00000FFFFFC000007FFFFE000007FFFFF000003FFFFF000001FFFFF800000FFFFFC000007FFFFE000003FFFFF000001FFFFF800000FFFFFE000007FFFFF000001FFFFF800000FFFFFE000003FFFFF800001FFFFFC000007FFFFF000001FFFFFC000007FFFFF000001FFFFFC000003FFFFF800000FFFFFE000003FFFFFC000007FFFFF000000FFFFFE000003FFFFFC000007FFFFF800000FFFFFF000000FFFFFE000001FFFFFE000003FFFFFC000003FFFFFC000007FFFFF8000007FFFFF8000007FFFFF8000007FFFFF8000003FFFFFC000003FFFFFC000003FFFFFE000001FFFFFF000000FFFFFF000;
defparam ram_block1a62.mem_init0 = 2048'h0007FFFFF8000003FFFFFE000001FFFFFF0000007FFFFFC000003FFFFFE000000FFFFFF8000003FFFFFE000000FFFFFF8000003FFFFFF0000007FFFFFC000000FFFFFF8000003FFFFFF0000007FFFFFE0000007FFFFFC000000FFFFFFC000000FFFFFF8000001FFFFFF8000001FFFFFF8000000FFFFFFC000000FFFFFFC0000007FFFFFE0000007FFFFFF0000003FFFFFF8000000FFFFFFC0000007FFFFFF0000001FFFFFFC0000007FFFFFF0000001FFFFFFC0000003FFFFFF8000000FFFFFFF0000000FFFFFFE0000001FFFFFFE0000003FFFFFFC0000003FFFFFFC0000003FFFFFFC0000001FFFFFFE0000001FFFFFFF0000000FFFFFFF80000003FFFFFFC;

cycloneive_ram_block ram_block1a117(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a117_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a117.clk0_core_clock_enable = "ena0";
defparam ram_block1a117.clk0_input_clock_enable = "ena0";
defparam ram_block1a117.clk0_output_clock_enable = "ena0";
defparam ram_block1a117.data_interleave_offset_in_bits = 1;
defparam ram_block1a117.data_interleave_width_in_bits = 1;
defparam ram_block1a117.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a117.init_file_layout = "port_a";
defparam ram_block1a117.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a117.operation_mode = "rom";
defparam ram_block1a117.port_a_address_clear = "none";
defparam ram_block1a117.port_a_address_width = 13;
defparam ram_block1a117.port_a_data_out_clear = "none";
defparam ram_block1a117.port_a_data_out_clock = "clock0";
defparam ram_block1a117.port_a_data_width = 1;
defparam ram_block1a117.port_a_first_address = 49152;
defparam ram_block1a117.port_a_first_bit_number = 9;
defparam ram_block1a117.port_a_last_address = 57343;
defparam ram_block1a117.port_a_logical_ram_depth = 65536;
defparam ram_block1a117.port_a_logical_ram_width = 18;
defparam ram_block1a117.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a117.ram_block_type = "auto";
defparam ram_block1a117.mem_init3 = 2048'h00000000000000FFFFFFFFFFFFFFC000000000000007FFFFFFFFFFFFFF000000000000001FFFFFFFFFFFFFFC000000000000003FFFFFFFFFFFFFFC000000000000001FFFFFFFFFFFFFFF0000000000000007FFFFFFFFFFFFFFE0000000000000007FFFFFFFFFFFFFFF0000000000000001FFFFFFFFFFFFFFFE0000000000000001FFFFFFFFFFFFFFFE0000000000000000FFFFFFFFFFFFFFFFC0000000000000000FFFFFFFFFFFFFFFFC00000000000000003FFFFFFFFFFFFFFFF800000000000000003FFFFFFFFFFFFFFFFE00000000000000000FFFFFFFFFFFFFFFFFC000000000000000007FFFFFFFFFFFFFFFFF8000000000000000007FFFFFFFFFFFFFFF;
defparam ram_block1a117.mem_init2 = 2048'hFFC000000000000000000FFFFFFFFFFFFFFFFFFE0000000000000000003FFFFFFFFFFFFFFFFFFE0000000000000000000FFFFFFFFFFFFFFFFFFFC00000000000000000007FFFFFFFFFFFFFFFFFFF800000000000000000001FFFFFFFFFFFFFFFFFFFF8000000000000000000007FFFFFFFFFFFFFFFFFFFF8000000000000000000001FFFFFFFFFFFFFFFFFFFFFE0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFC00000000000000000000003FFFFFFFFFFFFFFFFFFFFFFF000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000007FFFFFFFFFFFFF;
defparam ram_block1a117.mem_init1 = 2048'hFFFFFFFFFFFFF0000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000;
defparam ram_block1a117.mem_init0 = 2048'h000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a99(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a99_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a99.clk0_core_clock_enable = "ena0";
defparam ram_block1a99.clk0_input_clock_enable = "ena0";
defparam ram_block1a99.clk0_output_clock_enable = "ena0";
defparam ram_block1a99.data_interleave_offset_in_bits = 1;
defparam ram_block1a99.data_interleave_width_in_bits = 1;
defparam ram_block1a99.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a99.init_file_layout = "port_a";
defparam ram_block1a99.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a99.operation_mode = "rom";
defparam ram_block1a99.port_a_address_clear = "none";
defparam ram_block1a99.port_a_address_width = 13;
defparam ram_block1a99.port_a_data_out_clear = "none";
defparam ram_block1a99.port_a_data_out_clock = "clock0";
defparam ram_block1a99.port_a_data_width = 1;
defparam ram_block1a99.port_a_first_address = 40960;
defparam ram_block1a99.port_a_first_bit_number = 9;
defparam ram_block1a99.port_a_last_address = 49151;
defparam ram_block1a99.port_a_logical_ram_depth = 65536;
defparam ram_block1a99.port_a_logical_ram_width = 18;
defparam ram_block1a99.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a99.ram_block_type = "auto";
defparam ram_block1a99.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000;
defparam ram_block1a99.mem_init2 = 2048'h0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000001FFFFFFFFFFFFF;
defparam ram_block1a99.mem_init1 = 2048'hFFFFFFFFFFFFFC00000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFF800000000000000000000007FFFFFFFFFFFFFFFFFFFFFE0000000000000000000000FFFFFFFFFFFFFFFFFFFFFF0000000000000000000003FFFFFFFFFFFFFFFFFFFFC000000000000000000003FFFFFFFFFFFFFFFFFFFF000000000000000000003FFFFFFFFFFFFFFFFFFFC00000000000000000007FFFFFFFFFFFFFFFFFFE0000000000000000000FFFFFFFFFFFFFFFFFFF8000000000000000000FFFFFFFFFFFFFFFFFFE0000000000000000007FF;
defparam ram_block1a99.mem_init0 = 2048'hFFFFFFFFFFFFFFFC000000000000000003FFFFFFFFFFFFFFFFFC000000000000000007FFFFFFFFFFFFFFFFE00000000000000000FFFFFFFFFFFFFFFFF800000000000000003FFFFFFFFFFFFFFFF800000000000000007FFFFFFFFFFFFFFFE00000000000000007FFFFFFFFFFFFFFFE0000000000000000FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000001FFFFFFFFFFFFFFFC000000000000000FFFFFFFFFFFFFFFC000000000000001FFFFFFFFFFFFFFF0000000000000007FFFFFFFFFFFFFF8000000000000007FFFFFFFFFFFFFF000000000000001FFFFFFFFFFFFFFC000000000000007FFFFFFFFFFFFFE00000000000000;

cycloneive_ram_block ram_block1a81(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a81_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a81.clk0_core_clock_enable = "ena0";
defparam ram_block1a81.clk0_input_clock_enable = "ena0";
defparam ram_block1a81.clk0_output_clock_enable = "ena0";
defparam ram_block1a81.data_interleave_offset_in_bits = 1;
defparam ram_block1a81.data_interleave_width_in_bits = 1;
defparam ram_block1a81.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a81.init_file_layout = "port_a";
defparam ram_block1a81.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a81.operation_mode = "rom";
defparam ram_block1a81.port_a_address_clear = "none";
defparam ram_block1a81.port_a_address_width = 13;
defparam ram_block1a81.port_a_data_out_clear = "none";
defparam ram_block1a81.port_a_data_out_clock = "clock0";
defparam ram_block1a81.port_a_data_width = 1;
defparam ram_block1a81.port_a_first_address = 32768;
defparam ram_block1a81.port_a_first_bit_number = 9;
defparam ram_block1a81.port_a_last_address = 40959;
defparam ram_block1a81.port_a_logical_ram_depth = 65536;
defparam ram_block1a81.port_a_logical_ram_width = 18;
defparam ram_block1a81.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a81.ram_block_type = "auto";
defparam ram_block1a81.mem_init3 = 2048'hFFFFFFFFFFFFFFC00000000000001FFFFFFFFFFFFFF00000000000000FFFFFFFFFFFFFF800000000000007FFFFFFFFFFFFF00000000000000FFFFFFFFFFFFFE00000000000003FFFFFFFFFFFFF00000000000001FFFFFFFFFFFFF80000000000001FFFFFFFFFFFFF00000000000003FFFFFFFFFFFFE0000000000000FFFFFFFFFFFFF80000000000007FFFFFFFFFFFFC0000000000003FFFFFFFFFFFF80000000000007FFFFFFFFFFFF8000000000000FFFFFFFFFFFFE0000000000003FFFFFFFFFFFF8000000000001FFFFFFFFFFFFC000000000000FFFFFFFFFFFFC000000000000FFFFFFFFFFFF8000000000001FFFFFFFFFFFF0000000000007FFFFFFFFF;
defparam ram_block1a81.mem_init2 = 2048'hFFC000000000001FFFFFFFFFFFF0000000000007FFFFFFFFFFF8000000000007FFFFFFFFFFFC000000000003FFFFFFFFFFFC000000000007FFFFFFFFFFF800000000000FFFFFFFFFFFF000000000001FFFFFFFFFFFC000000000007FFFFFFFFFFF000000000001FFFFFFFFFFF800000000000FFFFFFFFFFFC000000000007FFFFFFFFFFE000000000007FFFFFFFFFFE000000000007FFFFFFFFFFC00000000000FFFFFFFFFFFC00000000001FFFFFFFFFFF000000000003FFFFFFFFFFE00000000000FFFFFFFFFFF800000000003FFFFFFFFFFE00000000001FFFFFFFFFFF00000000000FFFFFFFFFFF800000000007FFFFFFFFFFC00000000003FFFFFFFFFFC;
defparam ram_block1a81.mem_init1 = 2048'h00000000003FFFFFFFFFFC00000000003FFFFFFFFFFC00000000003FFFFFFFFFF800000000007FFFFFFFFFF00000000000FFFFFFFFFFE00000000001FFFFFFFFFFC00000000007FFFFFFFFFF00000000001FFFFFFFFFFE00000000007FFFFFFFFFF00000000001FFFFFFFFFFC00000000007FFFFFFFFFF00000000003FFFFFFFFFF80000000001FFFFFFFFFFC0000000000FFFFFFFFFFE00000000007FFFFFFFFFE00000000007FFFFFFFFFF00000000003FFFFFFFFFF00000000003FFFFFFFFFF00000000003FFFFFFFFFF00000000003FFFFFFFFFF00000000007FFFFFFFFFE00000000007FFFFFFFFFE0000000000FFFFFFFFFFC0000000000FFFFFFFFFF8;
defparam ram_block1a81.mem_init0 = 2048'h0000000001FFFFFFFFFF00000000003FFFFFFFFFE0000000000FFFFFFFFFFC0000000001FFFFFFFFFF00000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFE;

cycloneive_ram_block ram_block1a135(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a135_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a135.clk0_core_clock_enable = "ena0";
defparam ram_block1a135.clk0_input_clock_enable = "ena0";
defparam ram_block1a135.clk0_output_clock_enable = "ena0";
defparam ram_block1a135.data_interleave_offset_in_bits = 1;
defparam ram_block1a135.data_interleave_width_in_bits = 1;
defparam ram_block1a135.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a135.init_file_layout = "port_a";
defparam ram_block1a135.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a135.operation_mode = "rom";
defparam ram_block1a135.port_a_address_clear = "none";
defparam ram_block1a135.port_a_address_width = 13;
defparam ram_block1a135.port_a_data_out_clear = "none";
defparam ram_block1a135.port_a_data_out_clock = "clock0";
defparam ram_block1a135.port_a_data_width = 1;
defparam ram_block1a135.port_a_first_address = 57344;
defparam ram_block1a135.port_a_first_bit_number = 9;
defparam ram_block1a135.port_a_last_address = 65535;
defparam ram_block1a135.port_a_logical_ram_depth = 65536;
defparam ram_block1a135.port_a_logical_ram_width = 18;
defparam ram_block1a135.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a135.ram_block_type = "auto";
defparam ram_block1a135.mem_init3 = 2048'hFFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000001FFFFFFFFFF00000000007FFFFFFFFFE0000000000FFFFFFFFFF80000000001FFFFFFFFFF0000000000;
defparam ram_block1a135.mem_init2 = 2048'h3FFFFFFFFFE00000000007FFFFFFFFFE0000000000FFFFFFFFFFC0000000000FFFFFFFFFFC0000000001FFFFFFFFFF80000000001FFFFFFFFFF80000000001FFFFFFFFFF80000000001FFFFFFFFFF80000000001FFFFFFFFFFC0000000000FFFFFFFFFFC0000000000FFFFFFFFFFE00000000007FFFFFFFFFF00000000003FFFFFFFFFF80000000001FFFFFFFFFFC00000000007FFFFFFFFFF00000000001FFFFFFFFFFC0000000000FFFFFFFFFFF00000000001FFFFFFFFFFC00000000007FFFFFFFFFF00000000000FFFFFFFFFFE00000000001FFFFFFFFFFC00000000003FFFFFFFFFF800000000007FFFFFFFFFF800000000007FFFFFFFFFF80000000000;
defparam ram_block1a135.mem_init1 = 2048'h7FFFFFFFFFF800000000007FFFFFFFFFFC00000000003FFFFFFFFFFE00000000001FFFFFFFFFFF00000000000FFFFFFFFFFF800000000003FFFFFFFFFFE00000000000FFFFFFFFFFF800000000001FFFFFFFFFFF000000000007FFFFFFFFFFE000000000007FFFFFFFFFFC00000000000FFFFFFFFFFFC00000000000FFFFFFFFFFFC000000000007FFFFFFFFFFE000000000003FFFFFFFFFFF000000000001FFFFFFFFFFFC000000000007FFFFFFFFFFF000000000001FFFFFFFFFFFE000000000003FFFFFFFFFFFC000000000007FFFFFFFFFFF8000000000007FFFFFFFFFFFC000000000003FFFFFFFFFFFC000000000001FFFFFFFFFFFF0000000000007FF;
defparam ram_block1a135.mem_init0 = 2048'hFFFFFFFFFC000000000001FFFFFFFFFFFF0000000000003FFFFFFFFFFFE0000000000007FFFFFFFFFFFE0000000000007FFFFFFFFFFFF0000000000003FFFFFFFFFFFF8000000000000FFFFFFFFFFFFE0000000000003FFFFFFFFFFFFC0000000000003FFFFFFFFFFFF80000000000007FFFFFFFFFFFFC0000000000003FFFFFFFFFFFFE0000000000000FFFFFFFFFFFFF80000000000001FFFFFFFFFFFFF00000000000003FFFFFFFFFFFFF00000000000001FFFFFFFFFFFFF80000000000000FFFFFFFFFFFFFE00000000000001FFFFFFFFFFFFFC00000000000003FFFFFFFFFFFFFE00000000000001FFFFFFFFFFFFFF000000000000007FFFFFFFFFFFFFE;

cycloneive_ram_block ram_block1a27(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a27_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a27.clk0_core_clock_enable = "ena0";
defparam ram_block1a27.clk0_input_clock_enable = "ena0";
defparam ram_block1a27.clk0_output_clock_enable = "ena0";
defparam ram_block1a27.data_interleave_offset_in_bits = 1;
defparam ram_block1a27.data_interleave_width_in_bits = 1;
defparam ram_block1a27.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a27.init_file_layout = "port_a";
defparam ram_block1a27.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a27.operation_mode = "rom";
defparam ram_block1a27.port_a_address_clear = "none";
defparam ram_block1a27.port_a_address_width = 13;
defparam ram_block1a27.port_a_data_out_clear = "none";
defparam ram_block1a27.port_a_data_out_clock = "clock0";
defparam ram_block1a27.port_a_data_width = 1;
defparam ram_block1a27.port_a_first_address = 8192;
defparam ram_block1a27.port_a_first_bit_number = 9;
defparam ram_block1a27.port_a_last_address = 16383;
defparam ram_block1a27.port_a_logical_ram_depth = 65536;
defparam ram_block1a27.port_a_logical_ram_width = 18;
defparam ram_block1a27.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a27.ram_block_type = "auto";
defparam ram_block1a27.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a27.mem_init2 = 2048'hFFFF00000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000;
defparam ram_block1a27.mem_init1 = 2048'h00000000000003FFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000007FFFFFFFFFFFFFFFFFFFFFF80000000000000000000001FFFFFFFFFFFFFFFFFFFFFF0000000000000000000000FFFFFFFFFFFFFFFFFFFFFC000000000000000000001FFFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFFC00000000000000000003FFFFFFFFFFFFFFFFFFF80000000000000000001FFFFFFFFFFFFFFFFFFF80000000000000000007FFFFFFFFFFFFFFFFFF0000000000000000001FFFFFFFFFFFFFFFFFF800;
defparam ram_block1a27.mem_init0 = 2048'h0000000000000003FFFFFFFFFFFFFFFFFC000000000000000003FFFFFFFFFFFFFFFFF800000000000000001FFFFFFFFFFFFFFFFF800000000000000003FFFFFFFFFFFFFFFFC00000000000000003FFFFFFFFFFFFFFFF80000000000000001FFFFFFFFFFFFFFFF80000000000000001FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFE0000000000000003FFFFFFFFFFFFFFF0000000000000003FFFFFFFFFFFFFFE000000000000000FFFFFFFFFFFFFFF8000000000000007FFFFFFFFFFFFFF800000000000000FFFFFFFFFFFFFFF000000000000003FFFFFFFFFFFFFF800000000000001FFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a45(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a45_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a45.clk0_core_clock_enable = "ena0";
defparam ram_block1a45.clk0_input_clock_enable = "ena0";
defparam ram_block1a45.clk0_output_clock_enable = "ena0";
defparam ram_block1a45.data_interleave_offset_in_bits = 1;
defparam ram_block1a45.data_interleave_width_in_bits = 1;
defparam ram_block1a45.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a45.init_file_layout = "port_a";
defparam ram_block1a45.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a45.operation_mode = "rom";
defparam ram_block1a45.port_a_address_clear = "none";
defparam ram_block1a45.port_a_address_width = 13;
defparam ram_block1a45.port_a_data_out_clear = "none";
defparam ram_block1a45.port_a_data_out_clock = "clock0";
defparam ram_block1a45.port_a_data_width = 1;
defparam ram_block1a45.port_a_first_address = 16384;
defparam ram_block1a45.port_a_first_bit_number = 9;
defparam ram_block1a45.port_a_last_address = 24575;
defparam ram_block1a45.port_a_logical_ram_depth = 65536;
defparam ram_block1a45.port_a_logical_ram_width = 18;
defparam ram_block1a45.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a45.ram_block_type = "auto";
defparam ram_block1a45.mem_init3 = 2048'hFFFFFFFFFFFFFF000000000000003FFFFFFFFFFFFFF800000000000001FFFFFFFFFFFFFFE000000000000003FFFFFFFFFFFFFFC000000000000003FFFFFFFFFFFFFFE000000000000000FFFFFFFFFFFFFFF8000000000000001FFFFFFFFFFFFFFF8000000000000000FFFFFFFFFFFFFFFE0000000000000001FFFFFFFFFFFFFFFE0000000000000001FFFFFFFFFFFFFFFF00000000000000003FFFFFFFFFFFFFFFF00000000000000003FFFFFFFFFFFFFFFF800000000000000007FFFFFFFFFFFFFFFF800000000000000003FFFFFFFFFFFFFFFFF000000000000000003FFFFFFFFFFFFFFFFF8000000000000000007FFFFFFFFFFFFFFFFF8000000000000000;
defparam ram_block1a45.mem_init2 = 2048'h003FFFFFFFFFFFFFFFFFF0000000000000000001FFFFFFFFFFFFFFFFFFC0000000000000000003FFFFFFFFFFFFFFFFFFF00000000000000000003FFFFFFFFFFFFFFFFFFF800000000000000000007FFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFF0000000000000000000007FFFFFFFFFFFFFFFFFFFFE0000000000000000000001FFFFFFFFFFFFFFFFFFFFFF00000000000000000000003FFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFF80000000000000;
defparam ram_block1a45.mem_init1 = 2048'h0000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000001FFFF;
defparam ram_block1a45.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a9(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a9_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a9.clk0_core_clock_enable = "ena0";
defparam ram_block1a9.clk0_input_clock_enable = "ena0";
defparam ram_block1a9.clk0_output_clock_enable = "ena0";
defparam ram_block1a9.data_interleave_offset_in_bits = 1;
defparam ram_block1a9.data_interleave_width_in_bits = 1;
defparam ram_block1a9.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a9.init_file_layout = "port_a";
defparam ram_block1a9.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a9.operation_mode = "rom";
defparam ram_block1a9.port_a_address_clear = "none";
defparam ram_block1a9.port_a_address_width = 13;
defparam ram_block1a9.port_a_data_out_clear = "none";
defparam ram_block1a9.port_a_data_out_clock = "clock0";
defparam ram_block1a9.port_a_data_width = 1;
defparam ram_block1a9.port_a_first_address = 0;
defparam ram_block1a9.port_a_first_bit_number = 9;
defparam ram_block1a9.port_a_last_address = 8191;
defparam ram_block1a9.port_a_logical_ram_depth = 65536;
defparam ram_block1a9.port_a_logical_ram_width = 18;
defparam ram_block1a9.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a9.ram_block_type = "auto";
defparam ram_block1a9.mem_init3 = 2048'h800000000000003FFFFFFFFFFFFFE00000000000000FFFFFFFFFFFFFF800000000000007FFFFFFFFFFFFF80000000000000FFFFFFFFFFFFFF00000000000001FFFFFFFFFFFFFC00000000000007FFFFFFFFFFFFE00000000000007FFFFFFFFFFFFE00000000000007FFFFFFFFFFFFC0000000000001FFFFFFFFFFFFF00000000000007FFFFFFFFFFFF80000000000003FFFFFFFFFFFFC0000000000003FFFFFFFFFFFF80000000000007FFFFFFFFFFFF0000000000001FFFFFFFFFFFFC0000000000007FFFFFFFFFFFE0000000000003FFFFFFFFFFFF0000000000003FFFFFFFFFFFF0000000000007FFFFFFFFFFFE000000000000FFFFFFFFFFFFC000000000;
defparam ram_block1a9.mem_init2 = 2048'h001FFFFFFFFFFFE000000000000FFFFFFFFFFFF8000000000007FFFFFFFFFFFC000000000003FFFFFFFFFFFC000000000003FFFFFFFFFFF8000000000007FFFFFFFFFFF000000000000FFFFFFFFFFFE000000000003FFFFFFFFFFF800000000000FFFFFFFFFFFE000000000007FFFFFFFFFFF000000000003FFFFFFFFFFF800000000001FFFFFFFFFFF800000000001FFFFFFFFFFF800000000003FFFFFFFFFFF000000000003FFFFFFFFFFE00000000000FFFFFFFFFFFC00000000001FFFFFFFFFFF000000000007FFFFFFFFFFC00000000001FFFFFFFFFFE00000000000FFFFFFFFFFF800000000007FFFFFFFFFF800000000003FFFFFFFFFFC00000000003;
defparam ram_block1a9.mem_init1 = 2048'hFFFFFFFFFFC00000000003FFFFFFFFFFC00000000003FFFFFFFFFFC00000000007FFFFFFFFFF800000000007FFFFFFFFFF00000000001FFFFFFFFFFE00000000003FFFFFFFFFF80000000000FFFFFFFFFFF00000000001FFFFFFFFFFC0000000000FFFFFFFFFFE00000000003FFFFFFFFFF80000000000FFFFFFFFFFC00000000007FFFFFFFFFE00000000003FFFFFFFFFF00000000001FFFFFFFFFF80000000001FFFFFFFFFFC0000000000FFFFFFFFFFC0000000000FFFFFFFFFFC0000000000FFFFFFFFFFC0000000000FFFFFFFFFFC0000000000FFFFFFFFFFC0000000001FFFFFFFFFF80000000001FFFFFFFFFF00000000003FFFFFFFFFF00000000007;
defparam ram_block1a9.mem_init0 = 2048'hFFFFFFFFFE0000000000FFFFFFFFFFC0000000001FFFFFFFFFF00000000003FFFFFFFFFE00000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFF80000000003FFFFFFFFFE0000000000;

cycloneive_ram_block ram_block1a63(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a63_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a63.clk0_core_clock_enable = "ena0";
defparam ram_block1a63.clk0_input_clock_enable = "ena0";
defparam ram_block1a63.clk0_output_clock_enable = "ena0";
defparam ram_block1a63.data_interleave_offset_in_bits = 1;
defparam ram_block1a63.data_interleave_width_in_bits = 1;
defparam ram_block1a63.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a63.init_file_layout = "port_a";
defparam ram_block1a63.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a63.operation_mode = "rom";
defparam ram_block1a63.port_a_address_clear = "none";
defparam ram_block1a63.port_a_address_width = 13;
defparam ram_block1a63.port_a_data_out_clear = "none";
defparam ram_block1a63.port_a_data_out_clock = "clock0";
defparam ram_block1a63.port_a_data_width = 1;
defparam ram_block1a63.port_a_first_address = 24576;
defparam ram_block1a63.port_a_first_bit_number = 9;
defparam ram_block1a63.port_a_last_address = 32767;
defparam ram_block1a63.port_a_logical_ram_depth = 65536;
defparam ram_block1a63.port_a_logical_ram_width = 18;
defparam ram_block1a63.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a63.ram_block_type = "auto";
defparam ram_block1a63.mem_init3 = 2048'h0000000000FFFFFFFFFF80000000003FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000007FFFFFFFFFC0000000001FFFFFFFFFF00000000007FFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000003FFFFFFFFFE0000000000FFFFFFFFFF80000000001FFFFFFFFFF00000000007FFFFFFFFFC0000000000FFFFFFFFFF80000000001FFFFFFFFFF00000000007FFFFFFFFFE0000000000FFFFFFFFFF;
defparam ram_block1a63.mem_init2 = 2048'hC0000000001FFFFFFFFFF80000000001FFFFFFFFFF00000000003FFFFFFFFFF00000000007FFFFFFFFFE00000000007FFFFFFFFFE00000000007FFFFFFFFFE00000000007FFFFFFFFFE00000000007FFFFFFFFFE00000000007FFFFFFFFFF00000000003FFFFFFFFFF00000000001FFFFFFFFFF80000000000FFFFFFFFFFC00000000007FFFFFFFFFE00000000003FFFFFFFFFF80000000000FFFFFFFFFFE00000000007FFFFFFFFFF00000000001FFFFFFFFFFE00000000003FFFFFFFFFF80000000000FFFFFFFFFFF00000000001FFFFFFFFFFC00000000003FFFFFFFFFFC00000000007FFFFFFFFFF800000000007FFFFFFFFFF800000000007FFFFFFFFFF;
defparam ram_block1a63.mem_init1 = 2048'h800000000007FFFFFFFFFF800000000003FFFFFFFFFFC00000000003FFFFFFFFFFE00000000000FFFFFFFFFFF000000000007FFFFFFFFFFC00000000001FFFFFFFFFFF000000000007FFFFFFFFFFE00000000000FFFFFFFFFFF800000000001FFFFFFFFFFF800000000003FFFFFFFFFFF000000000003FFFFFFFFFFF000000000003FFFFFFFFFFF800000000001FFFFFFFFFFFC00000000000FFFFFFFFFFFE000000000003FFFFFFFFFFF800000000000FFFFFFFFFFFE000000000001FFFFFFFFFFFC000000000003FFFFFFFFFFF8000000000007FFFFFFFFFFF8000000000007FFFFFFFFFFFC000000000003FFFFFFFFFFFE000000000000FFFFFFFFFFFF000;
defparam ram_block1a63.mem_init0 = 2048'h0000000007FFFFFFFFFFFE000000000000FFFFFFFFFFFFC000000000001FFFFFFFFFFFF8000000000001FFFFFFFFFFFF8000000000000FFFFFFFFFFFFC0000000000007FFFFFFFFFFFF0000000000001FFFFFFFFFFFFC0000000000003FFFFFFFFFFFF80000000000007FFFFFFFFFFFF80000000000003FFFFFFFFFFFFC0000000000001FFFFFFFFFFFFF00000000000007FFFFFFFFFFFFC0000000000000FFFFFFFFFFFFFC0000000000000FFFFFFFFFFFFFC00000000000007FFFFFFFFFFFFF00000000000001FFFFFFFFFFFFFE00000000000003FFFFFFFFFFFFFC00000000000003FFFFFFFFFFFFFE00000000000000FFFFFFFFFFFFFF800000000000003;

cycloneive_ram_block ram_block1a118(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a118_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a118.clk0_core_clock_enable = "ena0";
defparam ram_block1a118.clk0_input_clock_enable = "ena0";
defparam ram_block1a118.clk0_output_clock_enable = "ena0";
defparam ram_block1a118.data_interleave_offset_in_bits = 1;
defparam ram_block1a118.data_interleave_width_in_bits = 1;
defparam ram_block1a118.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a118.init_file_layout = "port_a";
defparam ram_block1a118.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a118.operation_mode = "rom";
defparam ram_block1a118.port_a_address_clear = "none";
defparam ram_block1a118.port_a_address_width = 13;
defparam ram_block1a118.port_a_data_out_clear = "none";
defparam ram_block1a118.port_a_data_out_clock = "clock0";
defparam ram_block1a118.port_a_data_width = 1;
defparam ram_block1a118.port_a_first_address = 49152;
defparam ram_block1a118.port_a_first_bit_number = 10;
defparam ram_block1a118.port_a_last_address = 57343;
defparam ram_block1a118.port_a_logical_ram_depth = 65536;
defparam ram_block1a118.port_a_logical_ram_width = 18;
defparam ram_block1a118.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a118.ram_block_type = "auto";
defparam ram_block1a118.mem_init3 = 2048'hFFFFFFFFFFFFFF000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000007FFFFFFFFFFFFFFF;
defparam ram_block1a118.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000007FFFFFFFFFFFFF;
defparam ram_block1a118.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000;
defparam ram_block1a118.mem_init0 = 2048'h000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a100(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a100_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a100.clk0_core_clock_enable = "ena0";
defparam ram_block1a100.clk0_input_clock_enable = "ena0";
defparam ram_block1a100.clk0_output_clock_enable = "ena0";
defparam ram_block1a100.data_interleave_offset_in_bits = 1;
defparam ram_block1a100.data_interleave_width_in_bits = 1;
defparam ram_block1a100.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a100.init_file_layout = "port_a";
defparam ram_block1a100.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a100.operation_mode = "rom";
defparam ram_block1a100.port_a_address_clear = "none";
defparam ram_block1a100.port_a_address_width = 13;
defparam ram_block1a100.port_a_data_out_clear = "none";
defparam ram_block1a100.port_a_data_out_clock = "clock0";
defparam ram_block1a100.port_a_data_width = 1;
defparam ram_block1a100.port_a_first_address = 40960;
defparam ram_block1a100.port_a_first_bit_number = 10;
defparam ram_block1a100.port_a_last_address = 49151;
defparam ram_block1a100.port_a_logical_ram_depth = 65536;
defparam ram_block1a100.port_a_logical_ram_width = 18;
defparam ram_block1a100.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a100.ram_block_type = "auto";
defparam ram_block1a100.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000;
defparam ram_block1a100.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a100.mem_init1 = 2048'hFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a100.mem_init0 = 2048'hFFFFFFFFFFFFFFFC000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000001FFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a82(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a82_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a82.clk0_core_clock_enable = "ena0";
defparam ram_block1a82.clk0_input_clock_enable = "ena0";
defparam ram_block1a82.clk0_output_clock_enable = "ena0";
defparam ram_block1a82.data_interleave_offset_in_bits = 1;
defparam ram_block1a82.data_interleave_width_in_bits = 1;
defparam ram_block1a82.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a82.init_file_layout = "port_a";
defparam ram_block1a82.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a82.operation_mode = "rom";
defparam ram_block1a82.port_a_address_clear = "none";
defparam ram_block1a82.port_a_address_width = 13;
defparam ram_block1a82.port_a_data_out_clear = "none";
defparam ram_block1a82.port_a_data_out_clock = "clock0";
defparam ram_block1a82.port_a_data_width = 1;
defparam ram_block1a82.port_a_first_address = 32768;
defparam ram_block1a82.port_a_first_bit_number = 10;
defparam ram_block1a82.port_a_last_address = 40959;
defparam ram_block1a82.port_a_logical_ram_depth = 65536;
defparam ram_block1a82.port_a_logical_ram_width = 18;
defparam ram_block1a82.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a82.ram_block_type = "auto";
defparam ram_block1a82.mem_init3 = 2048'hFFFFFFFFFFFFFFC0000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000;
defparam ram_block1a82.mem_init2 = 2048'h003FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000FFFFFFFFFFFFFFFFFFFFFFF800000000000000000000003FFFFFFFFFFFFFFFFFFFFFFE00000000000000000000001FFFFFFFFFFFFFFFFFFFFFFC00000000000000000000003FFFFFFFFFFFFFFFFFFFFFF00000000000000000000001FFFFFFFFFFFFFFFFFFFFFF80000000000000000000001FFFFFFFFFFFFFFFFFFFFFF00000000000000000000007FFFFFFFFFFFFFFFFFFFFFC0000000000000000000003;
defparam ram_block1a82.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFC0000000000000000000003FFFFFFFFFFFFFFFFFFFFF8000000000000000000000FFFFFFFFFFFFFFFFFFFFFE0000000000000000000003FFFFFFFFFFFFFFFFFFFFF0000000000000000000001FFFFFFFFFFFFFFFFFFFFF0000000000000000000003FFFFFFFFFFFFFFFFFFFFF0000000000000000000007FFFFFFFFFFFFFFFFFFFFC000000000000000000001FFFFFFFFFFFFFFFFFFFFE000000000000000000000FFFFFFFFFFFFFFFFFFFFF000000000000000000000FFFFFFFFFFFFFFFFFFFFF000000000000000000000FFFFFFFFFFFFFFFFFFFFE000000000000000000001FFFFFFFFFFFFFFFFFFFFC000000000000000000007;
defparam ram_block1a82.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFF000000000000000000001FFFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFF000000000000000000003FFFFFFFFFFFFFFFFFFFF000000000000000000003FFFFFFFFFFFFFFFFFFFF000000000000000000003FFFFFFFFFFFFFFFFFFFF000000000000000000003FFFFFFFFFFFFFFFFFFFF000000000000000000007FFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFF800000000000000000001FFFFFFFFFFFFFFFFFFFF000000000000000000003FFFFFFFFFFFFFFFFFFFE;

cycloneive_ram_block ram_block1a136(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a136_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a136.clk0_core_clock_enable = "ena0";
defparam ram_block1a136.clk0_input_clock_enable = "ena0";
defparam ram_block1a136.clk0_output_clock_enable = "ena0";
defparam ram_block1a136.data_interleave_offset_in_bits = 1;
defparam ram_block1a136.data_interleave_width_in_bits = 1;
defparam ram_block1a136.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a136.init_file_layout = "port_a";
defparam ram_block1a136.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a136.operation_mode = "rom";
defparam ram_block1a136.port_a_address_clear = "none";
defparam ram_block1a136.port_a_address_width = 13;
defparam ram_block1a136.port_a_data_out_clear = "none";
defparam ram_block1a136.port_a_data_out_clock = "clock0";
defparam ram_block1a136.port_a_data_width = 1;
defparam ram_block1a136.port_a_first_address = 57344;
defparam ram_block1a136.port_a_first_bit_number = 10;
defparam ram_block1a136.port_a_last_address = 65535;
defparam ram_block1a136.port_a_logical_ram_depth = 65536;
defparam ram_block1a136.port_a_logical_ram_width = 18;
defparam ram_block1a136.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a136.ram_block_type = "auto";
defparam ram_block1a136.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFF800000000000000000001FFFFFFFFFFFFFFFFFFFF000000000000000000003FFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFFC00000000000000000001FFFFFFFFFFFFFFFFFFFF800000000000000000001FFFFFFFFFFFFFFFFFFFF800000000000000000001FFFFFFFFFFFFFFFFFFFF800000000000000000001FFFFFFFFFFFFFFFFFFFF800000000000000000001FFFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFF000000000000000000001FFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a136.mem_init2 = 2048'hC000000000000000000007FFFFFFFFFFFFFFFFFFFF000000000000000000000FFFFFFFFFFFFFFFFFFFFE000000000000000000001FFFFFFFFFFFFFFFFFFFFE000000000000000000001FFFFFFFFFFFFFFFFFFFFE000000000000000000000FFFFFFFFFFFFFFFFFFFFF0000000000000000000007FFFFFFFFFFFFFFFFFFFFC000000000000000000001FFFFFFFFFFFFFFFFFFFFF8000000000000000000001FFFFFFFFFFFFFFFFFFFFF0000000000000000000001FFFFFFFFFFFFFFFFFFFFF8000000000000000000000FFFFFFFFFFFFFFFFFFFFFE0000000000000000000003FFFFFFFFFFFFFFFFFFFFF80000000000000000000007FFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a136.mem_init1 = 2048'h80000000000000000000007FFFFFFFFFFFFFFFFFFFFFC0000000000000000000001FFFFFFFFFFFFFFFFFFFFFF00000000000000000000003FFFFFFFFFFFFFFFFFFFFFF00000000000000000000001FFFFFFFFFFFFFFFFFFFFFF800000000000000000000007FFFFFFFFFFFFFFFFFFFFFF00000000000000000000000FFFFFFFFFFFFFFFFFFFFFFF800000000000000000000003FFFFFFFFFFFFFFFFFFFFFFE000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFE000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFF800;
defparam ram_block1a136.mem_init0 = 2048'h0000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000007FFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a28(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a28_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a28.clk0_core_clock_enable = "ena0";
defparam ram_block1a28.clk0_input_clock_enable = "ena0";
defparam ram_block1a28.clk0_output_clock_enable = "ena0";
defparam ram_block1a28.data_interleave_offset_in_bits = 1;
defparam ram_block1a28.data_interleave_width_in_bits = 1;
defparam ram_block1a28.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a28.init_file_layout = "port_a";
defparam ram_block1a28.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a28.operation_mode = "rom";
defparam ram_block1a28.port_a_address_clear = "none";
defparam ram_block1a28.port_a_address_width = 13;
defparam ram_block1a28.port_a_data_out_clear = "none";
defparam ram_block1a28.port_a_data_out_clock = "clock0";
defparam ram_block1a28.port_a_data_width = 1;
defparam ram_block1a28.port_a_first_address = 8192;
defparam ram_block1a28.port_a_first_bit_number = 10;
defparam ram_block1a28.port_a_last_address = 16383;
defparam ram_block1a28.port_a_logical_ram_depth = 65536;
defparam ram_block1a28.port_a_logical_ram_width = 18;
defparam ram_block1a28.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a28.ram_block_type = "auto";
defparam ram_block1a28.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a28.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000;
defparam ram_block1a28.mem_init1 = 2048'h00000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000;
defparam ram_block1a28.mem_init0 = 2048'h0000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000;

cycloneive_ram_block ram_block1a46(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a46_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a46.clk0_core_clock_enable = "ena0";
defparam ram_block1a46.clk0_input_clock_enable = "ena0";
defparam ram_block1a46.clk0_output_clock_enable = "ena0";
defparam ram_block1a46.data_interleave_offset_in_bits = 1;
defparam ram_block1a46.data_interleave_width_in_bits = 1;
defparam ram_block1a46.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a46.init_file_layout = "port_a";
defparam ram_block1a46.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a46.operation_mode = "rom";
defparam ram_block1a46.port_a_address_clear = "none";
defparam ram_block1a46.port_a_address_width = 13;
defparam ram_block1a46.port_a_data_out_clear = "none";
defparam ram_block1a46.port_a_data_out_clock = "clock0";
defparam ram_block1a46.port_a_data_width = 1;
defparam ram_block1a46.port_a_first_address = 16384;
defparam ram_block1a46.port_a_first_bit_number = 10;
defparam ram_block1a46.port_a_last_address = 24575;
defparam ram_block1a46.port_a_logical_ram_depth = 65536;
defparam ram_block1a46.port_a_logical_ram_width = 18;
defparam ram_block1a46.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a46.ram_block_type = "auto";
defparam ram_block1a46.mem_init3 = 2048'h00000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000;
defparam ram_block1a46.mem_init2 = 2048'h000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000;
defparam ram_block1a46.mem_init1 = 2048'h00000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a46.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a10(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a10_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a10.clk0_core_clock_enable = "ena0";
defparam ram_block1a10.clk0_input_clock_enable = "ena0";
defparam ram_block1a10.clk0_output_clock_enable = "ena0";
defparam ram_block1a10.data_interleave_offset_in_bits = 1;
defparam ram_block1a10.data_interleave_width_in_bits = 1;
defparam ram_block1a10.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a10.init_file_layout = "port_a";
defparam ram_block1a10.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a10.operation_mode = "rom";
defparam ram_block1a10.port_a_address_clear = "none";
defparam ram_block1a10.port_a_address_width = 13;
defparam ram_block1a10.port_a_data_out_clear = "none";
defparam ram_block1a10.port_a_data_out_clock = "clock0";
defparam ram_block1a10.port_a_data_width = 1;
defparam ram_block1a10.port_a_first_address = 0;
defparam ram_block1a10.port_a_first_bit_number = 10;
defparam ram_block1a10.port_a_last_address = 8191;
defparam ram_block1a10.port_a_logical_ram_depth = 65536;
defparam ram_block1a10.port_a_logical_ram_width = 18;
defparam ram_block1a10.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a10.ram_block_type = "auto";
defparam ram_block1a10.mem_init3 = 2048'h000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000FFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a10.mem_init2 = 2048'hFFE000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFF000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFF000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFC00000000000000000000001FFFFFFFFFFFFFFFFFFFFFFE00000000000000000000003FFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFE00000000000000000000007FFFFFFFFFFFFFFFFFFFFFE0000000000000000000000FFFFFFFFFFFFFFFFFFFFFF80000000000000000000003FFFFFFFFFFFFFFFFFFFFFC;
defparam ram_block1a10.mem_init1 = 2048'h0000000000000000000003FFFFFFFFFFFFFFFFFFFFFC0000000000000000000007FFFFFFFFFFFFFFFFFFFFF8000000000000000000001FFFFFFFFFFFFFFFFFFFFFC000000000000000000000FFFFFFFFFFFFFFFFFFFFFE000000000000000000000FFFFFFFFFFFFFFFFFFFFFC000000000000000000000FFFFFFFFFFFFFFFFFFFFF8000000000000000000003FFFFFFFFFFFFFFFFFFFFE000000000000000000001FFFFFFFFFFFFFFFFFFFFF000000000000000000000FFFFFFFFFFFFFFFFFFFFF000000000000000000000FFFFFFFFFFFFFFFFFFFFF000000000000000000001FFFFFFFFFFFFFFFFFFFFE000000000000000000003FFFFFFFFFFFFFFFFFFFF8;
defparam ram_block1a10.mem_init0 = 2048'h00000000000000000000FFFFFFFFFFFFFFFFFFFFE000000000000000000003FFFFFFFFFFFFFFFFFFFF800000000000000000001FFFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFFC00000000000000000001FFFFFFFFFFFFFFFFFFFF800000000000000000003FFFFFFFFFFFFFFFFFFFF000000000000000000003FFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFC00000000000000000000;

cycloneive_ram_block ram_block1a64(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a64_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a64.clk0_core_clock_enable = "ena0";
defparam ram_block1a64.clk0_input_clock_enable = "ena0";
defparam ram_block1a64.clk0_output_clock_enable = "ena0";
defparam ram_block1a64.data_interleave_offset_in_bits = 1;
defparam ram_block1a64.data_interleave_width_in_bits = 1;
defparam ram_block1a64.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a64.init_file_layout = "port_a";
defparam ram_block1a64.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a64.operation_mode = "rom";
defparam ram_block1a64.port_a_address_clear = "none";
defparam ram_block1a64.port_a_address_width = 13;
defparam ram_block1a64.port_a_data_out_clear = "none";
defparam ram_block1a64.port_a_data_out_clock = "clock0";
defparam ram_block1a64.port_a_data_width = 1;
defparam ram_block1a64.port_a_first_address = 24576;
defparam ram_block1a64.port_a_first_bit_number = 10;
defparam ram_block1a64.port_a_last_address = 32767;
defparam ram_block1a64.port_a_logical_ram_depth = 65536;
defparam ram_block1a64.port_a_logical_ram_width = 18;
defparam ram_block1a64.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a64.ram_block_type = "auto";
defparam ram_block1a64.mem_init3 = 2048'h000000000000000000007FFFFFFFFFFFFFFFFFFFC00000000000000000000FFFFFFFFFFFFFFFFFFFF800000000000000000001FFFFFFFFFFFFFFFFFFFF800000000000000000003FFFFFFFFFFFFFFFFFFFF000000000000000000007FFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFE000000000000000000007FFFFFFFFFFFFFFFFFFFF000000000000000000003FFFFFFFFFFFFFFFFFFFF800000000000000000000FFFFFFFFFFFFFFFFFFFFE00000000000000000000;
defparam ram_block1a64.mem_init2 = 2048'h3FFFFFFFFFFFFFFFFFFFF800000000000000000000FFFFFFFFFFFFFFFFFFFFF000000000000000000001FFFFFFFFFFFFFFFFFFFFE000000000000000000001FFFFFFFFFFFFFFFFFFFFE000000000000000000001FFFFFFFFFFFFFFFFFFFFF000000000000000000000FFFFFFFFFFFFFFFFFFFFF8000000000000000000003FFFFFFFFFFFFFFFFFFFFE0000000000000000000007FFFFFFFFFFFFFFFFFFFFE000000000000000000000FFFFFFFFFFFFFFFFFFFFFE0000000000000000000007FFFFFFFFFFFFFFFFFFFFF0000000000000000000003FFFFFFFFFFFFFFFFFFFFFC0000000000000000000007FFFFFFFFFFFFFFFFFFFFF8000000000000000000000;
defparam ram_block1a64.mem_init1 = 2048'h7FFFFFFFFFFFFFFFFFFFFF80000000000000000000003FFFFFFFFFFFFFFFFFFFFFE0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFE00000000000000000000007FFFFFFFFFFFFFFFFFFFFFF80000000000000000000000FFFFFFFFFFFFFFFFFFFFFFF000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFC00000000000000000000001FFFFFFFFFFFFFFFFFFFFFFF800000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000FFF;
defparam ram_block1a64.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000;

cycloneive_ram_block ram_block1a119(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a119_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a119.clk0_core_clock_enable = "ena0";
defparam ram_block1a119.clk0_input_clock_enable = "ena0";
defparam ram_block1a119.clk0_output_clock_enable = "ena0";
defparam ram_block1a119.data_interleave_offset_in_bits = 1;
defparam ram_block1a119.data_interleave_width_in_bits = 1;
defparam ram_block1a119.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a119.init_file_layout = "port_a";
defparam ram_block1a119.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a119.operation_mode = "rom";
defparam ram_block1a119.port_a_address_clear = "none";
defparam ram_block1a119.port_a_address_width = 13;
defparam ram_block1a119.port_a_data_out_clear = "none";
defparam ram_block1a119.port_a_data_out_clock = "clock0";
defparam ram_block1a119.port_a_data_width = 1;
defparam ram_block1a119.port_a_first_address = 49152;
defparam ram_block1a119.port_a_first_bit_number = 11;
defparam ram_block1a119.port_a_last_address = 57343;
defparam ram_block1a119.port_a_logical_ram_depth = 65536;
defparam ram_block1a119.port_a_logical_ram_width = 18;
defparam ram_block1a119.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a119.ram_block_type = "auto";
defparam ram_block1a119.mem_init3 = 2048'h00000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000;
defparam ram_block1a119.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000;
defparam ram_block1a119.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a119.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a101(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a101_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a101.clk0_core_clock_enable = "ena0";
defparam ram_block1a101.clk0_input_clock_enable = "ena0";
defparam ram_block1a101.clk0_output_clock_enable = "ena0";
defparam ram_block1a101.data_interleave_offset_in_bits = 1;
defparam ram_block1a101.data_interleave_width_in_bits = 1;
defparam ram_block1a101.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a101.init_file_layout = "port_a";
defparam ram_block1a101.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a101.operation_mode = "rom";
defparam ram_block1a101.port_a_address_clear = "none";
defparam ram_block1a101.port_a_address_width = 13;
defparam ram_block1a101.port_a_data_out_clear = "none";
defparam ram_block1a101.port_a_data_out_clock = "clock0";
defparam ram_block1a101.port_a_data_width = 1;
defparam ram_block1a101.port_a_first_address = 40960;
defparam ram_block1a101.port_a_first_bit_number = 11;
defparam ram_block1a101.port_a_last_address = 49151;
defparam ram_block1a101.port_a_logical_ram_depth = 65536;
defparam ram_block1a101.port_a_logical_ram_width = 18;
defparam ram_block1a101.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a101.ram_block_type = "auto";
defparam ram_block1a101.mem_init3 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a101.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a101.mem_init1 = 2048'h00000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a101.mem_init0 = 2048'h0000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a83(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a83_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a83.clk0_core_clock_enable = "ena0";
defparam ram_block1a83.clk0_input_clock_enable = "ena0";
defparam ram_block1a83.clk0_output_clock_enable = "ena0";
defparam ram_block1a83.data_interleave_offset_in_bits = 1;
defparam ram_block1a83.data_interleave_width_in_bits = 1;
defparam ram_block1a83.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a83.init_file_layout = "port_a";
defparam ram_block1a83.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a83.operation_mode = "rom";
defparam ram_block1a83.port_a_address_clear = "none";
defparam ram_block1a83.port_a_address_width = 13;
defparam ram_block1a83.port_a_data_out_clear = "none";
defparam ram_block1a83.port_a_data_out_clock = "clock0";
defparam ram_block1a83.port_a_data_width = 1;
defparam ram_block1a83.port_a_first_address = 32768;
defparam ram_block1a83.port_a_first_bit_number = 11;
defparam ram_block1a83.port_a_last_address = 40959;
defparam ram_block1a83.port_a_logical_ram_depth = 65536;
defparam ram_block1a83.port_a_logical_ram_width = 18;
defparam ram_block1a83.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a83.ram_block_type = "auto";
defparam ram_block1a83.mem_init3 = 2048'h000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000;
defparam ram_block1a83.mem_init2 = 2048'h000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a83.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a83.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE;

cycloneive_ram_block ram_block1a137(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a137_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a137.clk0_core_clock_enable = "ena0";
defparam ram_block1a137.clk0_input_clock_enable = "ena0";
defparam ram_block1a137.clk0_output_clock_enable = "ena0";
defparam ram_block1a137.data_interleave_offset_in_bits = 1;
defparam ram_block1a137.data_interleave_width_in_bits = 1;
defparam ram_block1a137.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a137.init_file_layout = "port_a";
defparam ram_block1a137.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a137.operation_mode = "rom";
defparam ram_block1a137.port_a_address_clear = "none";
defparam ram_block1a137.port_a_address_width = 13;
defparam ram_block1a137.port_a_data_out_clear = "none";
defparam ram_block1a137.port_a_data_out_clock = "clock0";
defparam ram_block1a137.port_a_data_width = 1;
defparam ram_block1a137.port_a_first_address = 57344;
defparam ram_block1a137.port_a_first_bit_number = 11;
defparam ram_block1a137.port_a_last_address = 65535;
defparam ram_block1a137.port_a_logical_ram_depth = 65536;
defparam ram_block1a137.port_a_logical_ram_width = 18;
defparam ram_block1a137.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a137.ram_block_type = "auto";
defparam ram_block1a137.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a137.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a137.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000;
defparam ram_block1a137.mem_init0 = 2048'h0000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000;

cycloneive_ram_block ram_block1a29(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a29_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a29.clk0_core_clock_enable = "ena0";
defparam ram_block1a29.clk0_input_clock_enable = "ena0";
defparam ram_block1a29.clk0_output_clock_enable = "ena0";
defparam ram_block1a29.data_interleave_offset_in_bits = 1;
defparam ram_block1a29.data_interleave_width_in_bits = 1;
defparam ram_block1a29.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a29.init_file_layout = "port_a";
defparam ram_block1a29.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a29.operation_mode = "rom";
defparam ram_block1a29.port_a_address_clear = "none";
defparam ram_block1a29.port_a_address_width = 13;
defparam ram_block1a29.port_a_data_out_clear = "none";
defparam ram_block1a29.port_a_data_out_clock = "clock0";
defparam ram_block1a29.port_a_data_width = 1;
defparam ram_block1a29.port_a_first_address = 8192;
defparam ram_block1a29.port_a_first_bit_number = 11;
defparam ram_block1a29.port_a_last_address = 16383;
defparam ram_block1a29.port_a_logical_ram_depth = 65536;
defparam ram_block1a29.port_a_logical_ram_width = 18;
defparam ram_block1a29.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a29.ram_block_type = "auto";
defparam ram_block1a29.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000;
defparam ram_block1a29.mem_init2 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a29.mem_init1 = 2048'hFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a29.mem_init0 = 2048'hFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a47(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a47_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a47.clk0_core_clock_enable = "ena0";
defparam ram_block1a47.clk0_input_clock_enable = "ena0";
defparam ram_block1a47.clk0_output_clock_enable = "ena0";
defparam ram_block1a47.data_interleave_offset_in_bits = 1;
defparam ram_block1a47.data_interleave_width_in_bits = 1;
defparam ram_block1a47.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a47.init_file_layout = "port_a";
defparam ram_block1a47.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a47.operation_mode = "rom";
defparam ram_block1a47.port_a_address_clear = "none";
defparam ram_block1a47.port_a_address_width = 13;
defparam ram_block1a47.port_a_data_out_clear = "none";
defparam ram_block1a47.port_a_data_out_clock = "clock0";
defparam ram_block1a47.port_a_data_width = 1;
defparam ram_block1a47.port_a_first_address = 16384;
defparam ram_block1a47.port_a_first_bit_number = 11;
defparam ram_block1a47.port_a_last_address = 24575;
defparam ram_block1a47.port_a_logical_ram_depth = 65536;
defparam ram_block1a47.port_a_logical_ram_width = 18;
defparam ram_block1a47.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a47.ram_block_type = "auto";
defparam ram_block1a47.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFF;
defparam ram_block1a47.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFF;
defparam ram_block1a47.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a47.mem_init0 = 2048'h000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a11(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a11_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a11.clk0_core_clock_enable = "ena0";
defparam ram_block1a11.clk0_input_clock_enable = "ena0";
defparam ram_block1a11.clk0_output_clock_enable = "ena0";
defparam ram_block1a11.data_interleave_offset_in_bits = 1;
defparam ram_block1a11.data_interleave_width_in_bits = 1;
defparam ram_block1a11.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a11.init_file_layout = "port_a";
defparam ram_block1a11.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a11.operation_mode = "rom";
defparam ram_block1a11.port_a_address_clear = "none";
defparam ram_block1a11.port_a_address_width = 13;
defparam ram_block1a11.port_a_data_out_clear = "none";
defparam ram_block1a11.port_a_data_out_clock = "clock0";
defparam ram_block1a11.port_a_data_width = 1;
defparam ram_block1a11.port_a_first_address = 0;
defparam ram_block1a11.port_a_first_bit_number = 11;
defparam ram_block1a11.port_a_last_address = 8191;
defparam ram_block1a11.port_a_logical_ram_depth = 65536;
defparam ram_block1a11.port_a_logical_ram_width = 18;
defparam ram_block1a11.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a11.ram_block_type = "auto";
defparam ram_block1a11.mem_init3 = 2048'hFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a11.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000;
defparam ram_block1a11.mem_init1 = 2048'h0000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000;
defparam ram_block1a11.mem_init0 = 2048'h00000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a65(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a65_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a65.clk0_core_clock_enable = "ena0";
defparam ram_block1a65.clk0_input_clock_enable = "ena0";
defparam ram_block1a65.clk0_output_clock_enable = "ena0";
defparam ram_block1a65.data_interleave_offset_in_bits = 1;
defparam ram_block1a65.data_interleave_width_in_bits = 1;
defparam ram_block1a65.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a65.init_file_layout = "port_a";
defparam ram_block1a65.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a65.operation_mode = "rom";
defparam ram_block1a65.port_a_address_clear = "none";
defparam ram_block1a65.port_a_address_width = 13;
defparam ram_block1a65.port_a_data_out_clear = "none";
defparam ram_block1a65.port_a_data_out_clock = "clock0";
defparam ram_block1a65.port_a_data_width = 1;
defparam ram_block1a65.port_a_first_address = 24576;
defparam ram_block1a65.port_a_first_bit_number = 11;
defparam ram_block1a65.port_a_last_address = 32767;
defparam ram_block1a65.port_a_logical_ram_depth = 65536;
defparam ram_block1a65.port_a_logical_ram_width = 18;
defparam ram_block1a65.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a65.ram_block_type = "auto";
defparam ram_block1a65.mem_init3 = 2048'h00000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000;
defparam ram_block1a65.mem_init2 = 2048'h0000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000;
defparam ram_block1a65.mem_init1 = 2048'h00000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a65.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a120(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a120_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a120.clk0_core_clock_enable = "ena0";
defparam ram_block1a120.clk0_input_clock_enable = "ena0";
defparam ram_block1a120.clk0_output_clock_enable = "ena0";
defparam ram_block1a120.data_interleave_offset_in_bits = 1;
defparam ram_block1a120.data_interleave_width_in_bits = 1;
defparam ram_block1a120.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a120.init_file_layout = "port_a";
defparam ram_block1a120.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a120.operation_mode = "rom";
defparam ram_block1a120.port_a_address_clear = "none";
defparam ram_block1a120.port_a_address_width = 13;
defparam ram_block1a120.port_a_data_out_clear = "none";
defparam ram_block1a120.port_a_data_out_clock = "clock0";
defparam ram_block1a120.port_a_data_width = 1;
defparam ram_block1a120.port_a_first_address = 49152;
defparam ram_block1a120.port_a_first_bit_number = 12;
defparam ram_block1a120.port_a_last_address = 57343;
defparam ram_block1a120.port_a_logical_ram_depth = 65536;
defparam ram_block1a120.port_a_logical_ram_width = 18;
defparam ram_block1a120.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a120.ram_block_type = "auto";
defparam ram_block1a120.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a120.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a120.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a120.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a102(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a102_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a102.clk0_core_clock_enable = "ena0";
defparam ram_block1a102.clk0_input_clock_enable = "ena0";
defparam ram_block1a102.clk0_output_clock_enable = "ena0";
defparam ram_block1a102.data_interleave_offset_in_bits = 1;
defparam ram_block1a102.data_interleave_width_in_bits = 1;
defparam ram_block1a102.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a102.init_file_layout = "port_a";
defparam ram_block1a102.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a102.operation_mode = "rom";
defparam ram_block1a102.port_a_address_clear = "none";
defparam ram_block1a102.port_a_address_width = 13;
defparam ram_block1a102.port_a_data_out_clear = "none";
defparam ram_block1a102.port_a_data_out_clock = "clock0";
defparam ram_block1a102.port_a_data_width = 1;
defparam ram_block1a102.port_a_first_address = 40960;
defparam ram_block1a102.port_a_first_bit_number = 12;
defparam ram_block1a102.port_a_last_address = 49151;
defparam ram_block1a102.port_a_logical_ram_depth = 65536;
defparam ram_block1a102.port_a_logical_ram_width = 18;
defparam ram_block1a102.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a102.ram_block_type = "auto";
defparam ram_block1a102.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a102.mem_init2 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a102.mem_init1 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a102.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a84(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a84_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a84.clk0_core_clock_enable = "ena0";
defparam ram_block1a84.clk0_input_clock_enable = "ena0";
defparam ram_block1a84.clk0_output_clock_enable = "ena0";
defparam ram_block1a84.data_interleave_offset_in_bits = 1;
defparam ram_block1a84.data_interleave_width_in_bits = 1;
defparam ram_block1a84.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a84.init_file_layout = "port_a";
defparam ram_block1a84.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a84.operation_mode = "rom";
defparam ram_block1a84.port_a_address_clear = "none";
defparam ram_block1a84.port_a_address_width = 13;
defparam ram_block1a84.port_a_data_out_clear = "none";
defparam ram_block1a84.port_a_data_out_clock = "clock0";
defparam ram_block1a84.port_a_data_width = 1;
defparam ram_block1a84.port_a_first_address = 32768;
defparam ram_block1a84.port_a_first_bit_number = 12;
defparam ram_block1a84.port_a_last_address = 40959;
defparam ram_block1a84.port_a_logical_ram_depth = 65536;
defparam ram_block1a84.port_a_logical_ram_width = 18;
defparam ram_block1a84.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a84.ram_block_type = "auto";
defparam ram_block1a84.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000;
defparam ram_block1a84.mem_init2 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a84.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a84.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE;

cycloneive_ram_block ram_block1a138(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a138_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a138.clk0_core_clock_enable = "ena0";
defparam ram_block1a138.clk0_input_clock_enable = "ena0";
defparam ram_block1a138.clk0_output_clock_enable = "ena0";
defparam ram_block1a138.data_interleave_offset_in_bits = 1;
defparam ram_block1a138.data_interleave_width_in_bits = 1;
defparam ram_block1a138.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a138.init_file_layout = "port_a";
defparam ram_block1a138.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a138.operation_mode = "rom";
defparam ram_block1a138.port_a_address_clear = "none";
defparam ram_block1a138.port_a_address_width = 13;
defparam ram_block1a138.port_a_data_out_clear = "none";
defparam ram_block1a138.port_a_data_out_clock = "clock0";
defparam ram_block1a138.port_a_data_width = 1;
defparam ram_block1a138.port_a_first_address = 57344;
defparam ram_block1a138.port_a_first_bit_number = 12;
defparam ram_block1a138.port_a_last_address = 65535;
defparam ram_block1a138.port_a_logical_ram_depth = 65536;
defparam ram_block1a138.port_a_logical_ram_width = 18;
defparam ram_block1a138.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a138.ram_block_type = "auto";
defparam ram_block1a138.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a138.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a138.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a138.mem_init0 = 2048'h0000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a30(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a30_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a30.clk0_core_clock_enable = "ena0";
defparam ram_block1a30.clk0_input_clock_enable = "ena0";
defparam ram_block1a30.clk0_output_clock_enable = "ena0";
defparam ram_block1a30.data_interleave_offset_in_bits = 1;
defparam ram_block1a30.data_interleave_width_in_bits = 1;
defparam ram_block1a30.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a30.init_file_layout = "port_a";
defparam ram_block1a30.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a30.operation_mode = "rom";
defparam ram_block1a30.port_a_address_clear = "none";
defparam ram_block1a30.port_a_address_width = 13;
defparam ram_block1a30.port_a_data_out_clear = "none";
defparam ram_block1a30.port_a_data_out_clock = "clock0";
defparam ram_block1a30.port_a_data_width = 1;
defparam ram_block1a30.port_a_first_address = 8192;
defparam ram_block1a30.port_a_first_bit_number = 12;
defparam ram_block1a30.port_a_last_address = 16383;
defparam ram_block1a30.port_a_logical_ram_depth = 65536;
defparam ram_block1a30.port_a_logical_ram_width = 18;
defparam ram_block1a30.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a30.ram_block_type = "auto";
defparam ram_block1a30.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a30.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a30.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a30.mem_init0 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a48(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a48_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a48.clk0_core_clock_enable = "ena0";
defparam ram_block1a48.clk0_input_clock_enable = "ena0";
defparam ram_block1a48.clk0_output_clock_enable = "ena0";
defparam ram_block1a48.data_interleave_offset_in_bits = 1;
defparam ram_block1a48.data_interleave_width_in_bits = 1;
defparam ram_block1a48.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a48.init_file_layout = "port_a";
defparam ram_block1a48.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a48.operation_mode = "rom";
defparam ram_block1a48.port_a_address_clear = "none";
defparam ram_block1a48.port_a_address_width = 13;
defparam ram_block1a48.port_a_data_out_clear = "none";
defparam ram_block1a48.port_a_data_out_clock = "clock0";
defparam ram_block1a48.port_a_data_width = 1;
defparam ram_block1a48.port_a_first_address = 16384;
defparam ram_block1a48.port_a_first_bit_number = 12;
defparam ram_block1a48.port_a_last_address = 24575;
defparam ram_block1a48.port_a_logical_ram_depth = 65536;
defparam ram_block1a48.port_a_logical_ram_width = 18;
defparam ram_block1a48.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a48.ram_block_type = "auto";
defparam ram_block1a48.mem_init3 = 2048'h00000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a48.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a48.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a48.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a12(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a12_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a12.clk0_core_clock_enable = "ena0";
defparam ram_block1a12.clk0_input_clock_enable = "ena0";
defparam ram_block1a12.clk0_output_clock_enable = "ena0";
defparam ram_block1a12.data_interleave_offset_in_bits = 1;
defparam ram_block1a12.data_interleave_width_in_bits = 1;
defparam ram_block1a12.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a12.init_file_layout = "port_a";
defparam ram_block1a12.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a12.operation_mode = "rom";
defparam ram_block1a12.port_a_address_clear = "none";
defparam ram_block1a12.port_a_address_width = 13;
defparam ram_block1a12.port_a_data_out_clear = "none";
defparam ram_block1a12.port_a_data_out_clock = "clock0";
defparam ram_block1a12.port_a_data_width = 1;
defparam ram_block1a12.port_a_first_address = 0;
defparam ram_block1a12.port_a_first_bit_number = 12;
defparam ram_block1a12.port_a_last_address = 8191;
defparam ram_block1a12.port_a_logical_ram_depth = 65536;
defparam ram_block1a12.port_a_logical_ram_width = 18;
defparam ram_block1a12.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a12.ram_block_type = "auto";
defparam ram_block1a12.mem_init3 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a12.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a12.mem_init1 = 2048'h0000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a12.mem_init0 = 2048'h00000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a66(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a66_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a66.clk0_core_clock_enable = "ena0";
defparam ram_block1a66.clk0_input_clock_enable = "ena0";
defparam ram_block1a66.clk0_output_clock_enable = "ena0";
defparam ram_block1a66.data_interleave_offset_in_bits = 1;
defparam ram_block1a66.data_interleave_width_in_bits = 1;
defparam ram_block1a66.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a66.init_file_layout = "port_a";
defparam ram_block1a66.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a66.operation_mode = "rom";
defparam ram_block1a66.port_a_address_clear = "none";
defparam ram_block1a66.port_a_address_width = 13;
defparam ram_block1a66.port_a_data_out_clear = "none";
defparam ram_block1a66.port_a_data_out_clock = "clock0";
defparam ram_block1a66.port_a_data_width = 1;
defparam ram_block1a66.port_a_first_address = 24576;
defparam ram_block1a66.port_a_first_bit_number = 12;
defparam ram_block1a66.port_a_last_address = 32767;
defparam ram_block1a66.port_a_logical_ram_depth = 65536;
defparam ram_block1a66.port_a_logical_ram_width = 18;
defparam ram_block1a66.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a66.ram_block_type = "auto";
defparam ram_block1a66.mem_init3 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000;
defparam ram_block1a66.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000;
defparam ram_block1a66.mem_init1 = 2048'h0000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a66.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a121(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a121_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a121.clk0_core_clock_enable = "ena0";
defparam ram_block1a121.clk0_input_clock_enable = "ena0";
defparam ram_block1a121.clk0_output_clock_enable = "ena0";
defparam ram_block1a121.data_interleave_offset_in_bits = 1;
defparam ram_block1a121.data_interleave_width_in_bits = 1;
defparam ram_block1a121.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a121.init_file_layout = "port_a";
defparam ram_block1a121.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a121.operation_mode = "rom";
defparam ram_block1a121.port_a_address_clear = "none";
defparam ram_block1a121.port_a_address_width = 13;
defparam ram_block1a121.port_a_data_out_clear = "none";
defparam ram_block1a121.port_a_data_out_clock = "clock0";
defparam ram_block1a121.port_a_data_width = 1;
defparam ram_block1a121.port_a_first_address = 49152;
defparam ram_block1a121.port_a_first_bit_number = 13;
defparam ram_block1a121.port_a_last_address = 57343;
defparam ram_block1a121.port_a_logical_ram_depth = 65536;
defparam ram_block1a121.port_a_logical_ram_width = 18;
defparam ram_block1a121.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a121.ram_block_type = "auto";
defparam ram_block1a121.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a121.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a121.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a121.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a103(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a103_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a103.clk0_core_clock_enable = "ena0";
defparam ram_block1a103.clk0_input_clock_enable = "ena0";
defparam ram_block1a103.clk0_output_clock_enable = "ena0";
defparam ram_block1a103.data_interleave_offset_in_bits = 1;
defparam ram_block1a103.data_interleave_width_in_bits = 1;
defparam ram_block1a103.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a103.init_file_layout = "port_a";
defparam ram_block1a103.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a103.operation_mode = "rom";
defparam ram_block1a103.port_a_address_clear = "none";
defparam ram_block1a103.port_a_address_width = 13;
defparam ram_block1a103.port_a_data_out_clear = "none";
defparam ram_block1a103.port_a_data_out_clock = "clock0";
defparam ram_block1a103.port_a_data_width = 1;
defparam ram_block1a103.port_a_first_address = 40960;
defparam ram_block1a103.port_a_first_bit_number = 13;
defparam ram_block1a103.port_a_last_address = 49151;
defparam ram_block1a103.port_a_logical_ram_depth = 65536;
defparam ram_block1a103.port_a_logical_ram_width = 18;
defparam ram_block1a103.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a103.ram_block_type = "auto";
defparam ram_block1a103.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a103.mem_init2 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a103.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a103.mem_init0 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a85(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a85_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a85.clk0_core_clock_enable = "ena0";
defparam ram_block1a85.clk0_input_clock_enable = "ena0";
defparam ram_block1a85.clk0_output_clock_enable = "ena0";
defparam ram_block1a85.data_interleave_offset_in_bits = 1;
defparam ram_block1a85.data_interleave_width_in_bits = 1;
defparam ram_block1a85.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a85.init_file_layout = "port_a";
defparam ram_block1a85.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a85.operation_mode = "rom";
defparam ram_block1a85.port_a_address_clear = "none";
defparam ram_block1a85.port_a_address_width = 13;
defparam ram_block1a85.port_a_data_out_clear = "none";
defparam ram_block1a85.port_a_data_out_clock = "clock0";
defparam ram_block1a85.port_a_data_width = 1;
defparam ram_block1a85.port_a_first_address = 32768;
defparam ram_block1a85.port_a_first_bit_number = 13;
defparam ram_block1a85.port_a_last_address = 40959;
defparam ram_block1a85.port_a_logical_ram_depth = 65536;
defparam ram_block1a85.port_a_logical_ram_width = 18;
defparam ram_block1a85.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a85.ram_block_type = "auto";
defparam ram_block1a85.mem_init3 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a85.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a85.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a85.mem_init0 = 2048'h00000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE;

cycloneive_ram_block ram_block1a139(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a139_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a139.clk0_core_clock_enable = "ena0";
defparam ram_block1a139.clk0_input_clock_enable = "ena0";
defparam ram_block1a139.clk0_output_clock_enable = "ena0";
defparam ram_block1a139.data_interleave_offset_in_bits = 1;
defparam ram_block1a139.data_interleave_width_in_bits = 1;
defparam ram_block1a139.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a139.init_file_layout = "port_a";
defparam ram_block1a139.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a139.operation_mode = "rom";
defparam ram_block1a139.port_a_address_clear = "none";
defparam ram_block1a139.port_a_address_width = 13;
defparam ram_block1a139.port_a_data_out_clear = "none";
defparam ram_block1a139.port_a_data_out_clock = "clock0";
defparam ram_block1a139.port_a_data_width = 1;
defparam ram_block1a139.port_a_first_address = 57344;
defparam ram_block1a139.port_a_first_bit_number = 13;
defparam ram_block1a139.port_a_last_address = 65535;
defparam ram_block1a139.port_a_logical_ram_depth = 65536;
defparam ram_block1a139.port_a_logical_ram_width = 18;
defparam ram_block1a139.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a139.ram_block_type = "auto";
defparam ram_block1a139.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000;
defparam ram_block1a139.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a139.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a139.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a31(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a31_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a31.clk0_core_clock_enable = "ena0";
defparam ram_block1a31.clk0_input_clock_enable = "ena0";
defparam ram_block1a31.clk0_output_clock_enable = "ena0";
defparam ram_block1a31.data_interleave_offset_in_bits = 1;
defparam ram_block1a31.data_interleave_width_in_bits = 1;
defparam ram_block1a31.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a31.init_file_layout = "port_a";
defparam ram_block1a31.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a31.operation_mode = "rom";
defparam ram_block1a31.port_a_address_clear = "none";
defparam ram_block1a31.port_a_address_width = 13;
defparam ram_block1a31.port_a_data_out_clear = "none";
defparam ram_block1a31.port_a_data_out_clock = "clock0";
defparam ram_block1a31.port_a_data_width = 1;
defparam ram_block1a31.port_a_first_address = 8192;
defparam ram_block1a31.port_a_first_bit_number = 13;
defparam ram_block1a31.port_a_last_address = 16383;
defparam ram_block1a31.port_a_logical_ram_depth = 65536;
defparam ram_block1a31.port_a_logical_ram_width = 18;
defparam ram_block1a31.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a31.ram_block_type = "auto";
defparam ram_block1a31.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a31.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a31.mem_init1 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a31.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a49(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a49_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a49.clk0_core_clock_enable = "ena0";
defparam ram_block1a49.clk0_input_clock_enable = "ena0";
defparam ram_block1a49.clk0_output_clock_enable = "ena0";
defparam ram_block1a49.data_interleave_offset_in_bits = 1;
defparam ram_block1a49.data_interleave_width_in_bits = 1;
defparam ram_block1a49.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a49.init_file_layout = "port_a";
defparam ram_block1a49.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a49.operation_mode = "rom";
defparam ram_block1a49.port_a_address_clear = "none";
defparam ram_block1a49.port_a_address_width = 13;
defparam ram_block1a49.port_a_data_out_clear = "none";
defparam ram_block1a49.port_a_data_out_clock = "clock0";
defparam ram_block1a49.port_a_data_width = 1;
defparam ram_block1a49.port_a_first_address = 16384;
defparam ram_block1a49.port_a_first_bit_number = 13;
defparam ram_block1a49.port_a_last_address = 24575;
defparam ram_block1a49.port_a_logical_ram_depth = 65536;
defparam ram_block1a49.port_a_logical_ram_width = 18;
defparam ram_block1a49.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a49.ram_block_type = "auto";
defparam ram_block1a49.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a49.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a49.mem_init1 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a49.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a13(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a13_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a13.clk0_core_clock_enable = "ena0";
defparam ram_block1a13.clk0_input_clock_enable = "ena0";
defparam ram_block1a13.clk0_output_clock_enable = "ena0";
defparam ram_block1a13.data_interleave_offset_in_bits = 1;
defparam ram_block1a13.data_interleave_width_in_bits = 1;
defparam ram_block1a13.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a13.init_file_layout = "port_a";
defparam ram_block1a13.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a13.operation_mode = "rom";
defparam ram_block1a13.port_a_address_clear = "none";
defparam ram_block1a13.port_a_address_width = 13;
defparam ram_block1a13.port_a_data_out_clear = "none";
defparam ram_block1a13.port_a_data_out_clock = "clock0";
defparam ram_block1a13.port_a_data_width = 1;
defparam ram_block1a13.port_a_first_address = 0;
defparam ram_block1a13.port_a_first_bit_number = 13;
defparam ram_block1a13.port_a_last_address = 8191;
defparam ram_block1a13.port_a_logical_ram_depth = 65536;
defparam ram_block1a13.port_a_logical_ram_width = 18;
defparam ram_block1a13.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a13.ram_block_type = "auto";
defparam ram_block1a13.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000;
defparam ram_block1a13.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a13.mem_init1 = 2048'h0000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a13.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a67(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a67_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a67.clk0_core_clock_enable = "ena0";
defparam ram_block1a67.clk0_input_clock_enable = "ena0";
defparam ram_block1a67.clk0_output_clock_enable = "ena0";
defparam ram_block1a67.data_interleave_offset_in_bits = 1;
defparam ram_block1a67.data_interleave_width_in_bits = 1;
defparam ram_block1a67.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a67.init_file_layout = "port_a";
defparam ram_block1a67.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a67.operation_mode = "rom";
defparam ram_block1a67.port_a_address_clear = "none";
defparam ram_block1a67.port_a_address_width = 13;
defparam ram_block1a67.port_a_data_out_clear = "none";
defparam ram_block1a67.port_a_data_out_clock = "clock0";
defparam ram_block1a67.port_a_data_width = 1;
defparam ram_block1a67.port_a_first_address = 24576;
defparam ram_block1a67.port_a_first_bit_number = 13;
defparam ram_block1a67.port_a_last_address = 32767;
defparam ram_block1a67.port_a_logical_ram_depth = 65536;
defparam ram_block1a67.port_a_logical_ram_width = 18;
defparam ram_block1a67.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a67.ram_block_type = "auto";
defparam ram_block1a67.mem_init3 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a67.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000;
defparam ram_block1a67.mem_init1 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a67.mem_init0 = 2048'h0000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a122(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a122_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a122.clk0_core_clock_enable = "ena0";
defparam ram_block1a122.clk0_input_clock_enable = "ena0";
defparam ram_block1a122.clk0_output_clock_enable = "ena0";
defparam ram_block1a122.data_interleave_offset_in_bits = 1;
defparam ram_block1a122.data_interleave_width_in_bits = 1;
defparam ram_block1a122.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a122.init_file_layout = "port_a";
defparam ram_block1a122.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a122.operation_mode = "rom";
defparam ram_block1a122.port_a_address_clear = "none";
defparam ram_block1a122.port_a_address_width = 13;
defparam ram_block1a122.port_a_data_out_clear = "none";
defparam ram_block1a122.port_a_data_out_clock = "clock0";
defparam ram_block1a122.port_a_data_width = 1;
defparam ram_block1a122.port_a_first_address = 49152;
defparam ram_block1a122.port_a_first_bit_number = 14;
defparam ram_block1a122.port_a_last_address = 57343;
defparam ram_block1a122.port_a_logical_ram_depth = 65536;
defparam ram_block1a122.port_a_logical_ram_width = 18;
defparam ram_block1a122.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a122.ram_block_type = "auto";
defparam ram_block1a122.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a122.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a122.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a122.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a104(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a104_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a104.clk0_core_clock_enable = "ena0";
defparam ram_block1a104.clk0_input_clock_enable = "ena0";
defparam ram_block1a104.clk0_output_clock_enable = "ena0";
defparam ram_block1a104.data_interleave_offset_in_bits = 1;
defparam ram_block1a104.data_interleave_width_in_bits = 1;
defparam ram_block1a104.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a104.init_file_layout = "port_a";
defparam ram_block1a104.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a104.operation_mode = "rom";
defparam ram_block1a104.port_a_address_clear = "none";
defparam ram_block1a104.port_a_address_width = 13;
defparam ram_block1a104.port_a_data_out_clear = "none";
defparam ram_block1a104.port_a_data_out_clock = "clock0";
defparam ram_block1a104.port_a_data_width = 1;
defparam ram_block1a104.port_a_first_address = 40960;
defparam ram_block1a104.port_a_first_bit_number = 14;
defparam ram_block1a104.port_a_last_address = 49151;
defparam ram_block1a104.port_a_logical_ram_depth = 65536;
defparam ram_block1a104.port_a_logical_ram_width = 18;
defparam ram_block1a104.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a104.ram_block_type = "auto";
defparam ram_block1a104.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a104.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a104.mem_init1 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a104.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a86(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a86_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a86.clk0_core_clock_enable = "ena0";
defparam ram_block1a86.clk0_input_clock_enable = "ena0";
defparam ram_block1a86.clk0_output_clock_enable = "ena0";
defparam ram_block1a86.data_interleave_offset_in_bits = 1;
defparam ram_block1a86.data_interleave_width_in_bits = 1;
defparam ram_block1a86.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a86.init_file_layout = "port_a";
defparam ram_block1a86.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a86.operation_mode = "rom";
defparam ram_block1a86.port_a_address_clear = "none";
defparam ram_block1a86.port_a_address_width = 13;
defparam ram_block1a86.port_a_data_out_clear = "none";
defparam ram_block1a86.port_a_data_out_clock = "clock0";
defparam ram_block1a86.port_a_data_width = 1;
defparam ram_block1a86.port_a_first_address = 32768;
defparam ram_block1a86.port_a_first_bit_number = 14;
defparam ram_block1a86.port_a_last_address = 40959;
defparam ram_block1a86.port_a_logical_ram_depth = 65536;
defparam ram_block1a86.port_a_logical_ram_width = 18;
defparam ram_block1a86.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a86.ram_block_type = "auto";
defparam ram_block1a86.mem_init3 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a86.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a86.mem_init1 = 2048'h0000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a86.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE;

cycloneive_ram_block ram_block1a140(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a140_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a140.clk0_core_clock_enable = "ena0";
defparam ram_block1a140.clk0_input_clock_enable = "ena0";
defparam ram_block1a140.clk0_output_clock_enable = "ena0";
defparam ram_block1a140.data_interleave_offset_in_bits = 1;
defparam ram_block1a140.data_interleave_width_in_bits = 1;
defparam ram_block1a140.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a140.init_file_layout = "port_a";
defparam ram_block1a140.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a140.operation_mode = "rom";
defparam ram_block1a140.port_a_address_clear = "none";
defparam ram_block1a140.port_a_address_width = 13;
defparam ram_block1a140.port_a_data_out_clear = "none";
defparam ram_block1a140.port_a_data_out_clock = "clock0";
defparam ram_block1a140.port_a_data_width = 1;
defparam ram_block1a140.port_a_first_address = 57344;
defparam ram_block1a140.port_a_first_bit_number = 14;
defparam ram_block1a140.port_a_last_address = 65535;
defparam ram_block1a140.port_a_logical_ram_depth = 65536;
defparam ram_block1a140.port_a_logical_ram_width = 18;
defparam ram_block1a140.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a140.ram_block_type = "auto";
defparam ram_block1a140.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a140.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000;
defparam ram_block1a140.mem_init1 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a140.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a32(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a32_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a32.clk0_core_clock_enable = "ena0";
defparam ram_block1a32.clk0_input_clock_enable = "ena0";
defparam ram_block1a32.clk0_output_clock_enable = "ena0";
defparam ram_block1a32.data_interleave_offset_in_bits = 1;
defparam ram_block1a32.data_interleave_width_in_bits = 1;
defparam ram_block1a32.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a32.init_file_layout = "port_a";
defparam ram_block1a32.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a32.operation_mode = "rom";
defparam ram_block1a32.port_a_address_clear = "none";
defparam ram_block1a32.port_a_address_width = 13;
defparam ram_block1a32.port_a_data_out_clear = "none";
defparam ram_block1a32.port_a_data_out_clock = "clock0";
defparam ram_block1a32.port_a_data_width = 1;
defparam ram_block1a32.port_a_first_address = 8192;
defparam ram_block1a32.port_a_first_bit_number = 14;
defparam ram_block1a32.port_a_last_address = 16383;
defparam ram_block1a32.port_a_logical_ram_depth = 65536;
defparam ram_block1a32.port_a_logical_ram_width = 18;
defparam ram_block1a32.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a32.ram_block_type = "auto";
defparam ram_block1a32.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a32.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a32.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a32.mem_init0 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a50(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a50_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a50.clk0_core_clock_enable = "ena0";
defparam ram_block1a50.clk0_input_clock_enable = "ena0";
defparam ram_block1a50.clk0_output_clock_enable = "ena0";
defparam ram_block1a50.data_interleave_offset_in_bits = 1;
defparam ram_block1a50.data_interleave_width_in_bits = 1;
defparam ram_block1a50.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a50.init_file_layout = "port_a";
defparam ram_block1a50.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a50.operation_mode = "rom";
defparam ram_block1a50.port_a_address_clear = "none";
defparam ram_block1a50.port_a_address_width = 13;
defparam ram_block1a50.port_a_data_out_clear = "none";
defparam ram_block1a50.port_a_data_out_clock = "clock0";
defparam ram_block1a50.port_a_data_width = 1;
defparam ram_block1a50.port_a_first_address = 16384;
defparam ram_block1a50.port_a_first_bit_number = 14;
defparam ram_block1a50.port_a_last_address = 24575;
defparam ram_block1a50.port_a_logical_ram_depth = 65536;
defparam ram_block1a50.port_a_logical_ram_width = 18;
defparam ram_block1a50.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a50.ram_block_type = "auto";
defparam ram_block1a50.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a50.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a50.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a50.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a14(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a14_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a14.clk0_core_clock_enable = "ena0";
defparam ram_block1a14.clk0_input_clock_enable = "ena0";
defparam ram_block1a14.clk0_output_clock_enable = "ena0";
defparam ram_block1a14.data_interleave_offset_in_bits = 1;
defparam ram_block1a14.data_interleave_width_in_bits = 1;
defparam ram_block1a14.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a14.init_file_layout = "port_a";
defparam ram_block1a14.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a14.operation_mode = "rom";
defparam ram_block1a14.port_a_address_clear = "none";
defparam ram_block1a14.port_a_address_width = 13;
defparam ram_block1a14.port_a_data_out_clear = "none";
defparam ram_block1a14.port_a_data_out_clock = "clock0";
defparam ram_block1a14.port_a_data_width = 1;
defparam ram_block1a14.port_a_first_address = 0;
defparam ram_block1a14.port_a_first_bit_number = 14;
defparam ram_block1a14.port_a_last_address = 8191;
defparam ram_block1a14.port_a_logical_ram_depth = 65536;
defparam ram_block1a14.port_a_logical_ram_width = 18;
defparam ram_block1a14.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a14.ram_block_type = "auto";
defparam ram_block1a14.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a14.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a14.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a14.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a68(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a68_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a68.clk0_core_clock_enable = "ena0";
defparam ram_block1a68.clk0_input_clock_enable = "ena0";
defparam ram_block1a68.clk0_output_clock_enable = "ena0";
defparam ram_block1a68.data_interleave_offset_in_bits = 1;
defparam ram_block1a68.data_interleave_width_in_bits = 1;
defparam ram_block1a68.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a68.init_file_layout = "port_a";
defparam ram_block1a68.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a68.operation_mode = "rom";
defparam ram_block1a68.port_a_address_clear = "none";
defparam ram_block1a68.port_a_address_width = 13;
defparam ram_block1a68.port_a_data_out_clear = "none";
defparam ram_block1a68.port_a_data_out_clock = "clock0";
defparam ram_block1a68.port_a_data_width = 1;
defparam ram_block1a68.port_a_first_address = 24576;
defparam ram_block1a68.port_a_first_bit_number = 14;
defparam ram_block1a68.port_a_last_address = 32767;
defparam ram_block1a68.port_a_logical_ram_depth = 65536;
defparam ram_block1a68.port_a_logical_ram_width = 18;
defparam ram_block1a68.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a68.ram_block_type = "auto";
defparam ram_block1a68.mem_init3 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a68.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a68.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a68.mem_init0 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a123(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a123_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a123.clk0_core_clock_enable = "ena0";
defparam ram_block1a123.clk0_input_clock_enable = "ena0";
defparam ram_block1a123.clk0_output_clock_enable = "ena0";
defparam ram_block1a123.data_interleave_offset_in_bits = 1;
defparam ram_block1a123.data_interleave_width_in_bits = 1;
defparam ram_block1a123.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a123.init_file_layout = "port_a";
defparam ram_block1a123.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a123.operation_mode = "rom";
defparam ram_block1a123.port_a_address_clear = "none";
defparam ram_block1a123.port_a_address_width = 13;
defparam ram_block1a123.port_a_data_out_clear = "none";
defparam ram_block1a123.port_a_data_out_clock = "clock0";
defparam ram_block1a123.port_a_data_width = 1;
defparam ram_block1a123.port_a_first_address = 49152;
defparam ram_block1a123.port_a_first_bit_number = 15;
defparam ram_block1a123.port_a_last_address = 57343;
defparam ram_block1a123.port_a_logical_ram_depth = 65536;
defparam ram_block1a123.port_a_logical_ram_width = 18;
defparam ram_block1a123.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a123.ram_block_type = "auto";
defparam ram_block1a123.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a123.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a123.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a123.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a105(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a105_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a105.clk0_core_clock_enable = "ena0";
defparam ram_block1a105.clk0_input_clock_enable = "ena0";
defparam ram_block1a105.clk0_output_clock_enable = "ena0";
defparam ram_block1a105.data_interleave_offset_in_bits = 1;
defparam ram_block1a105.data_interleave_width_in_bits = 1;
defparam ram_block1a105.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a105.init_file_layout = "port_a";
defparam ram_block1a105.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a105.operation_mode = "rom";
defparam ram_block1a105.port_a_address_clear = "none";
defparam ram_block1a105.port_a_address_width = 13;
defparam ram_block1a105.port_a_data_out_clear = "none";
defparam ram_block1a105.port_a_data_out_clock = "clock0";
defparam ram_block1a105.port_a_data_width = 1;
defparam ram_block1a105.port_a_first_address = 40960;
defparam ram_block1a105.port_a_first_bit_number = 15;
defparam ram_block1a105.port_a_last_address = 49151;
defparam ram_block1a105.port_a_logical_ram_depth = 65536;
defparam ram_block1a105.port_a_logical_ram_width = 18;
defparam ram_block1a105.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a105.ram_block_type = "auto";
defparam ram_block1a105.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a105.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a105.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a105.mem_init0 = 2048'h0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a87(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a87_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a87.clk0_core_clock_enable = "ena0";
defparam ram_block1a87.clk0_input_clock_enable = "ena0";
defparam ram_block1a87.clk0_output_clock_enable = "ena0";
defparam ram_block1a87.data_interleave_offset_in_bits = 1;
defparam ram_block1a87.data_interleave_width_in_bits = 1;
defparam ram_block1a87.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a87.init_file_layout = "port_a";
defparam ram_block1a87.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a87.operation_mode = "rom";
defparam ram_block1a87.port_a_address_clear = "none";
defparam ram_block1a87.port_a_address_width = 13;
defparam ram_block1a87.port_a_data_out_clear = "none";
defparam ram_block1a87.port_a_data_out_clock = "clock0";
defparam ram_block1a87.port_a_data_width = 1;
defparam ram_block1a87.port_a_first_address = 32768;
defparam ram_block1a87.port_a_first_bit_number = 15;
defparam ram_block1a87.port_a_last_address = 40959;
defparam ram_block1a87.port_a_logical_ram_depth = 65536;
defparam ram_block1a87.port_a_logical_ram_width = 18;
defparam ram_block1a87.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a87.ram_block_type = "auto";
defparam ram_block1a87.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a87.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a87.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a87.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE;

cycloneive_ram_block ram_block1a141(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a141_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a141.clk0_core_clock_enable = "ena0";
defparam ram_block1a141.clk0_input_clock_enable = "ena0";
defparam ram_block1a141.clk0_output_clock_enable = "ena0";
defparam ram_block1a141.data_interleave_offset_in_bits = 1;
defparam ram_block1a141.data_interleave_width_in_bits = 1;
defparam ram_block1a141.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a141.init_file_layout = "port_a";
defparam ram_block1a141.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a141.operation_mode = "rom";
defparam ram_block1a141.port_a_address_clear = "none";
defparam ram_block1a141.port_a_address_width = 13;
defparam ram_block1a141.port_a_data_out_clear = "none";
defparam ram_block1a141.port_a_data_out_clock = "clock0";
defparam ram_block1a141.port_a_data_width = 1;
defparam ram_block1a141.port_a_first_address = 57344;
defparam ram_block1a141.port_a_first_bit_number = 15;
defparam ram_block1a141.port_a_last_address = 65535;
defparam ram_block1a141.port_a_logical_ram_depth = 65536;
defparam ram_block1a141.port_a_logical_ram_width = 18;
defparam ram_block1a141.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a141.ram_block_type = "auto";
defparam ram_block1a141.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a141.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a141.mem_init1 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a141.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a33(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a33_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a33.clk0_core_clock_enable = "ena0";
defparam ram_block1a33.clk0_input_clock_enable = "ena0";
defparam ram_block1a33.clk0_output_clock_enable = "ena0";
defparam ram_block1a33.data_interleave_offset_in_bits = 1;
defparam ram_block1a33.data_interleave_width_in_bits = 1;
defparam ram_block1a33.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a33.init_file_layout = "port_a";
defparam ram_block1a33.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a33.operation_mode = "rom";
defparam ram_block1a33.port_a_address_clear = "none";
defparam ram_block1a33.port_a_address_width = 13;
defparam ram_block1a33.port_a_data_out_clear = "none";
defparam ram_block1a33.port_a_data_out_clock = "clock0";
defparam ram_block1a33.port_a_data_width = 1;
defparam ram_block1a33.port_a_first_address = 8192;
defparam ram_block1a33.port_a_first_bit_number = 15;
defparam ram_block1a33.port_a_last_address = 16383;
defparam ram_block1a33.port_a_logical_ram_depth = 65536;
defparam ram_block1a33.port_a_logical_ram_width = 18;
defparam ram_block1a33.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a33.ram_block_type = "auto";
defparam ram_block1a33.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a33.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a33.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a33.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a51(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a51_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a51.clk0_core_clock_enable = "ena0";
defparam ram_block1a51.clk0_input_clock_enable = "ena0";
defparam ram_block1a51.clk0_output_clock_enable = "ena0";
defparam ram_block1a51.data_interleave_offset_in_bits = 1;
defparam ram_block1a51.data_interleave_width_in_bits = 1;
defparam ram_block1a51.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a51.init_file_layout = "port_a";
defparam ram_block1a51.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a51.operation_mode = "rom";
defparam ram_block1a51.port_a_address_clear = "none";
defparam ram_block1a51.port_a_address_width = 13;
defparam ram_block1a51.port_a_data_out_clear = "none";
defparam ram_block1a51.port_a_data_out_clock = "clock0";
defparam ram_block1a51.port_a_data_width = 1;
defparam ram_block1a51.port_a_first_address = 16384;
defparam ram_block1a51.port_a_first_bit_number = 15;
defparam ram_block1a51.port_a_last_address = 24575;
defparam ram_block1a51.port_a_logical_ram_depth = 65536;
defparam ram_block1a51.port_a_logical_ram_width = 18;
defparam ram_block1a51.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a51.ram_block_type = "auto";
defparam ram_block1a51.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a51.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a51.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a51.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a15(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a15_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a15.clk0_core_clock_enable = "ena0";
defparam ram_block1a15.clk0_input_clock_enable = "ena0";
defparam ram_block1a15.clk0_output_clock_enable = "ena0";
defparam ram_block1a15.data_interleave_offset_in_bits = 1;
defparam ram_block1a15.data_interleave_width_in_bits = 1;
defparam ram_block1a15.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a15.init_file_layout = "port_a";
defparam ram_block1a15.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a15.operation_mode = "rom";
defparam ram_block1a15.port_a_address_clear = "none";
defparam ram_block1a15.port_a_address_width = 13;
defparam ram_block1a15.port_a_data_out_clear = "none";
defparam ram_block1a15.port_a_data_out_clock = "clock0";
defparam ram_block1a15.port_a_data_width = 1;
defparam ram_block1a15.port_a_first_address = 0;
defparam ram_block1a15.port_a_first_bit_number = 15;
defparam ram_block1a15.port_a_last_address = 8191;
defparam ram_block1a15.port_a_logical_ram_depth = 65536;
defparam ram_block1a15.port_a_logical_ram_width = 18;
defparam ram_block1a15.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a15.ram_block_type = "auto";
defparam ram_block1a15.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a15.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a15.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a15.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a69(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a69_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a69.clk0_core_clock_enable = "ena0";
defparam ram_block1a69.clk0_input_clock_enable = "ena0";
defparam ram_block1a69.clk0_output_clock_enable = "ena0";
defparam ram_block1a69.data_interleave_offset_in_bits = 1;
defparam ram_block1a69.data_interleave_width_in_bits = 1;
defparam ram_block1a69.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a69.init_file_layout = "port_a";
defparam ram_block1a69.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a69.operation_mode = "rom";
defparam ram_block1a69.port_a_address_clear = "none";
defparam ram_block1a69.port_a_address_width = 13;
defparam ram_block1a69.port_a_data_out_clear = "none";
defparam ram_block1a69.port_a_data_out_clock = "clock0";
defparam ram_block1a69.port_a_data_width = 1;
defparam ram_block1a69.port_a_first_address = 24576;
defparam ram_block1a69.port_a_first_bit_number = 15;
defparam ram_block1a69.port_a_last_address = 32767;
defparam ram_block1a69.port_a_logical_ram_depth = 65536;
defparam ram_block1a69.port_a_logical_ram_width = 18;
defparam ram_block1a69.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a69.ram_block_type = "auto";
defparam ram_block1a69.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a69.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a69.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a69.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a124(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a124_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a124.clk0_core_clock_enable = "ena0";
defparam ram_block1a124.clk0_input_clock_enable = "ena0";
defparam ram_block1a124.clk0_output_clock_enable = "ena0";
defparam ram_block1a124.data_interleave_offset_in_bits = 1;
defparam ram_block1a124.data_interleave_width_in_bits = 1;
defparam ram_block1a124.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a124.init_file_layout = "port_a";
defparam ram_block1a124.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a124.operation_mode = "rom";
defparam ram_block1a124.port_a_address_clear = "none";
defparam ram_block1a124.port_a_address_width = 13;
defparam ram_block1a124.port_a_data_out_clear = "none";
defparam ram_block1a124.port_a_data_out_clock = "clock0";
defparam ram_block1a124.port_a_data_width = 1;
defparam ram_block1a124.port_a_first_address = 49152;
defparam ram_block1a124.port_a_first_bit_number = 16;
defparam ram_block1a124.port_a_last_address = 57343;
defparam ram_block1a124.port_a_logical_ram_depth = 65536;
defparam ram_block1a124.port_a_logical_ram_width = 18;
defparam ram_block1a124.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a124.ram_block_type = "auto";
defparam ram_block1a124.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a124.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a124.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a124.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a106(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a106_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a106.clk0_core_clock_enable = "ena0";
defparam ram_block1a106.clk0_input_clock_enable = "ena0";
defparam ram_block1a106.clk0_output_clock_enable = "ena0";
defparam ram_block1a106.data_interleave_offset_in_bits = 1;
defparam ram_block1a106.data_interleave_width_in_bits = 1;
defparam ram_block1a106.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a106.init_file_layout = "port_a";
defparam ram_block1a106.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a106.operation_mode = "rom";
defparam ram_block1a106.port_a_address_clear = "none";
defparam ram_block1a106.port_a_address_width = 13;
defparam ram_block1a106.port_a_data_out_clear = "none";
defparam ram_block1a106.port_a_data_out_clock = "clock0";
defparam ram_block1a106.port_a_data_width = 1;
defparam ram_block1a106.port_a_first_address = 40960;
defparam ram_block1a106.port_a_first_bit_number = 16;
defparam ram_block1a106.port_a_last_address = 49151;
defparam ram_block1a106.port_a_logical_ram_depth = 65536;
defparam ram_block1a106.port_a_logical_ram_width = 18;
defparam ram_block1a106.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a106.ram_block_type = "auto";
defparam ram_block1a106.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a106.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a106.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a106.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a88(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a88_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a88.clk0_core_clock_enable = "ena0";
defparam ram_block1a88.clk0_input_clock_enable = "ena0";
defparam ram_block1a88.clk0_output_clock_enable = "ena0";
defparam ram_block1a88.data_interleave_offset_in_bits = 1;
defparam ram_block1a88.data_interleave_width_in_bits = 1;
defparam ram_block1a88.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a88.init_file_layout = "port_a";
defparam ram_block1a88.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a88.operation_mode = "rom";
defparam ram_block1a88.port_a_address_clear = "none";
defparam ram_block1a88.port_a_address_width = 13;
defparam ram_block1a88.port_a_data_out_clear = "none";
defparam ram_block1a88.port_a_data_out_clock = "clock0";
defparam ram_block1a88.port_a_data_width = 1;
defparam ram_block1a88.port_a_first_address = 32768;
defparam ram_block1a88.port_a_first_bit_number = 16;
defparam ram_block1a88.port_a_last_address = 40959;
defparam ram_block1a88.port_a_logical_ram_depth = 65536;
defparam ram_block1a88.port_a_logical_ram_width = 18;
defparam ram_block1a88.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a88.ram_block_type = "auto";
defparam ram_block1a88.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a88.mem_init2 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a88.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a88.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE;

cycloneive_ram_block ram_block1a142(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a142_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a142.clk0_core_clock_enable = "ena0";
defparam ram_block1a142.clk0_input_clock_enable = "ena0";
defparam ram_block1a142.clk0_output_clock_enable = "ena0";
defparam ram_block1a142.data_interleave_offset_in_bits = 1;
defparam ram_block1a142.data_interleave_width_in_bits = 1;
defparam ram_block1a142.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a142.init_file_layout = "port_a";
defparam ram_block1a142.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a142.operation_mode = "rom";
defparam ram_block1a142.port_a_address_clear = "none";
defparam ram_block1a142.port_a_address_width = 13;
defparam ram_block1a142.port_a_data_out_clear = "none";
defparam ram_block1a142.port_a_data_out_clock = "clock0";
defparam ram_block1a142.port_a_data_width = 1;
defparam ram_block1a142.port_a_first_address = 57344;
defparam ram_block1a142.port_a_first_bit_number = 16;
defparam ram_block1a142.port_a_last_address = 65535;
defparam ram_block1a142.port_a_logical_ram_depth = 65536;
defparam ram_block1a142.port_a_logical_ram_width = 18;
defparam ram_block1a142.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a142.ram_block_type = "auto";
defparam ram_block1a142.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a142.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a142.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a142.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a34(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a34_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a34.clk0_core_clock_enable = "ena0";
defparam ram_block1a34.clk0_input_clock_enable = "ena0";
defparam ram_block1a34.clk0_output_clock_enable = "ena0";
defparam ram_block1a34.data_interleave_offset_in_bits = 1;
defparam ram_block1a34.data_interleave_width_in_bits = 1;
defparam ram_block1a34.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a34.init_file_layout = "port_a";
defparam ram_block1a34.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a34.operation_mode = "rom";
defparam ram_block1a34.port_a_address_clear = "none";
defparam ram_block1a34.port_a_address_width = 13;
defparam ram_block1a34.port_a_data_out_clear = "none";
defparam ram_block1a34.port_a_data_out_clock = "clock0";
defparam ram_block1a34.port_a_data_width = 1;
defparam ram_block1a34.port_a_first_address = 8192;
defparam ram_block1a34.port_a_first_bit_number = 16;
defparam ram_block1a34.port_a_last_address = 16383;
defparam ram_block1a34.port_a_logical_ram_depth = 65536;
defparam ram_block1a34.port_a_logical_ram_width = 18;
defparam ram_block1a34.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a34.ram_block_type = "auto";
defparam ram_block1a34.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a34.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a34.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a34.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a52(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a52_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a52.clk0_core_clock_enable = "ena0";
defparam ram_block1a52.clk0_input_clock_enable = "ena0";
defparam ram_block1a52.clk0_output_clock_enable = "ena0";
defparam ram_block1a52.data_interleave_offset_in_bits = 1;
defparam ram_block1a52.data_interleave_width_in_bits = 1;
defparam ram_block1a52.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a52.init_file_layout = "port_a";
defparam ram_block1a52.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a52.operation_mode = "rom";
defparam ram_block1a52.port_a_address_clear = "none";
defparam ram_block1a52.port_a_address_width = 13;
defparam ram_block1a52.port_a_data_out_clear = "none";
defparam ram_block1a52.port_a_data_out_clock = "clock0";
defparam ram_block1a52.port_a_data_width = 1;
defparam ram_block1a52.port_a_first_address = 16384;
defparam ram_block1a52.port_a_first_bit_number = 16;
defparam ram_block1a52.port_a_last_address = 24575;
defparam ram_block1a52.port_a_logical_ram_depth = 65536;
defparam ram_block1a52.port_a_logical_ram_width = 18;
defparam ram_block1a52.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a52.ram_block_type = "auto";
defparam ram_block1a52.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a52.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a52.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a52.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a16(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a16_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a16.clk0_core_clock_enable = "ena0";
defparam ram_block1a16.clk0_input_clock_enable = "ena0";
defparam ram_block1a16.clk0_output_clock_enable = "ena0";
defparam ram_block1a16.data_interleave_offset_in_bits = 1;
defparam ram_block1a16.data_interleave_width_in_bits = 1;
defparam ram_block1a16.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a16.init_file_layout = "port_a";
defparam ram_block1a16.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a16.operation_mode = "rom";
defparam ram_block1a16.port_a_address_clear = "none";
defparam ram_block1a16.port_a_address_width = 13;
defparam ram_block1a16.port_a_data_out_clear = "none";
defparam ram_block1a16.port_a_data_out_clock = "clock0";
defparam ram_block1a16.port_a_data_width = 1;
defparam ram_block1a16.port_a_first_address = 0;
defparam ram_block1a16.port_a_first_bit_number = 16;
defparam ram_block1a16.port_a_last_address = 8191;
defparam ram_block1a16.port_a_logical_ram_depth = 65536;
defparam ram_block1a16.port_a_logical_ram_width = 18;
defparam ram_block1a16.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a16.ram_block_type = "auto";
defparam ram_block1a16.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a16.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a16.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a16.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a70(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a70_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a70.clk0_core_clock_enable = "ena0";
defparam ram_block1a70.clk0_input_clock_enable = "ena0";
defparam ram_block1a70.clk0_output_clock_enable = "ena0";
defparam ram_block1a70.data_interleave_offset_in_bits = 1;
defparam ram_block1a70.data_interleave_width_in_bits = 1;
defparam ram_block1a70.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a70.init_file_layout = "port_a";
defparam ram_block1a70.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a70.operation_mode = "rom";
defparam ram_block1a70.port_a_address_clear = "none";
defparam ram_block1a70.port_a_address_width = 13;
defparam ram_block1a70.port_a_data_out_clear = "none";
defparam ram_block1a70.port_a_data_out_clock = "clock0";
defparam ram_block1a70.port_a_data_width = 1;
defparam ram_block1a70.port_a_first_address = 24576;
defparam ram_block1a70.port_a_first_bit_number = 16;
defparam ram_block1a70.port_a_last_address = 32767;
defparam ram_block1a70.port_a_logical_ram_depth = 65536;
defparam ram_block1a70.port_a_logical_ram_width = 18;
defparam ram_block1a70.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a70.ram_block_type = "auto";
defparam ram_block1a70.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a70.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a70.mem_init1 = 2048'h000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a70.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a125(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a125_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a125.clk0_core_clock_enable = "ena0";
defparam ram_block1a125.clk0_input_clock_enable = "ena0";
defparam ram_block1a125.clk0_output_clock_enable = "ena0";
defparam ram_block1a125.data_interleave_offset_in_bits = 1;
defparam ram_block1a125.data_interleave_width_in_bits = 1;
defparam ram_block1a125.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a125.init_file_layout = "port_a";
defparam ram_block1a125.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a125.operation_mode = "rom";
defparam ram_block1a125.port_a_address_clear = "none";
defparam ram_block1a125.port_a_address_width = 13;
defparam ram_block1a125.port_a_data_out_clear = "none";
defparam ram_block1a125.port_a_data_out_clock = "clock0";
defparam ram_block1a125.port_a_data_width = 1;
defparam ram_block1a125.port_a_first_address = 49152;
defparam ram_block1a125.port_a_first_bit_number = 17;
defparam ram_block1a125.port_a_last_address = 57343;
defparam ram_block1a125.port_a_logical_ram_depth = 65536;
defparam ram_block1a125.port_a_logical_ram_width = 18;
defparam ram_block1a125.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a125.ram_block_type = "auto";
defparam ram_block1a125.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a125.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a125.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a125.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a107(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a107_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a107.clk0_core_clock_enable = "ena0";
defparam ram_block1a107.clk0_input_clock_enable = "ena0";
defparam ram_block1a107.clk0_output_clock_enable = "ena0";
defparam ram_block1a107.data_interleave_offset_in_bits = 1;
defparam ram_block1a107.data_interleave_width_in_bits = 1;
defparam ram_block1a107.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a107.init_file_layout = "port_a";
defparam ram_block1a107.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a107.operation_mode = "rom";
defparam ram_block1a107.port_a_address_clear = "none";
defparam ram_block1a107.port_a_address_width = 13;
defparam ram_block1a107.port_a_data_out_clear = "none";
defparam ram_block1a107.port_a_data_out_clock = "clock0";
defparam ram_block1a107.port_a_data_width = 1;
defparam ram_block1a107.port_a_first_address = 40960;
defparam ram_block1a107.port_a_first_bit_number = 17;
defparam ram_block1a107.port_a_last_address = 49151;
defparam ram_block1a107.port_a_logical_ram_depth = 65536;
defparam ram_block1a107.port_a_logical_ram_width = 18;
defparam ram_block1a107.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a107.ram_block_type = "auto";
defparam ram_block1a107.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a107.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a107.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a107.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a89(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a89_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a89.clk0_core_clock_enable = "ena0";
defparam ram_block1a89.clk0_input_clock_enable = "ena0";
defparam ram_block1a89.clk0_output_clock_enable = "ena0";
defparam ram_block1a89.data_interleave_offset_in_bits = 1;
defparam ram_block1a89.data_interleave_width_in_bits = 1;
defparam ram_block1a89.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a89.init_file_layout = "port_a";
defparam ram_block1a89.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a89.operation_mode = "rom";
defparam ram_block1a89.port_a_address_clear = "none";
defparam ram_block1a89.port_a_address_width = 13;
defparam ram_block1a89.port_a_data_out_clear = "none";
defparam ram_block1a89.port_a_data_out_clock = "clock0";
defparam ram_block1a89.port_a_data_width = 1;
defparam ram_block1a89.port_a_first_address = 32768;
defparam ram_block1a89.port_a_first_bit_number = 17;
defparam ram_block1a89.port_a_last_address = 40959;
defparam ram_block1a89.port_a_logical_ram_depth = 65536;
defparam ram_block1a89.port_a_logical_ram_width = 18;
defparam ram_block1a89.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a89.ram_block_type = "auto";
defparam ram_block1a89.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a89.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a89.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a89.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE;

cycloneive_ram_block ram_block1a143(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a143_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a143.clk0_core_clock_enable = "ena0";
defparam ram_block1a143.clk0_input_clock_enable = "ena0";
defparam ram_block1a143.clk0_output_clock_enable = "ena0";
defparam ram_block1a143.data_interleave_offset_in_bits = 1;
defparam ram_block1a143.data_interleave_width_in_bits = 1;
defparam ram_block1a143.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a143.init_file_layout = "port_a";
defparam ram_block1a143.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a143.operation_mode = "rom";
defparam ram_block1a143.port_a_address_clear = "none";
defparam ram_block1a143.port_a_address_width = 13;
defparam ram_block1a143.port_a_data_out_clear = "none";
defparam ram_block1a143.port_a_data_out_clock = "clock0";
defparam ram_block1a143.port_a_data_width = 1;
defparam ram_block1a143.port_a_first_address = 57344;
defparam ram_block1a143.port_a_first_bit_number = 17;
defparam ram_block1a143.port_a_last_address = 65535;
defparam ram_block1a143.port_a_logical_ram_depth = 65536;
defparam ram_block1a143.port_a_logical_ram_width = 18;
defparam ram_block1a143.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a143.ram_block_type = "auto";
defparam ram_block1a143.mem_init3 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a143.mem_init2 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a143.mem_init1 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
defparam ram_block1a143.mem_init0 = 2048'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;

cycloneive_ram_block ram_block1a35(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a35_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a35.clk0_core_clock_enable = "ena0";
defparam ram_block1a35.clk0_input_clock_enable = "ena0";
defparam ram_block1a35.clk0_output_clock_enable = "ena0";
defparam ram_block1a35.data_interleave_offset_in_bits = 1;
defparam ram_block1a35.data_interleave_width_in_bits = 1;
defparam ram_block1a35.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a35.init_file_layout = "port_a";
defparam ram_block1a35.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a35.operation_mode = "rom";
defparam ram_block1a35.port_a_address_clear = "none";
defparam ram_block1a35.port_a_address_width = 13;
defparam ram_block1a35.port_a_data_out_clear = "none";
defparam ram_block1a35.port_a_data_out_clock = "clock0";
defparam ram_block1a35.port_a_data_width = 1;
defparam ram_block1a35.port_a_first_address = 8192;
defparam ram_block1a35.port_a_first_bit_number = 17;
defparam ram_block1a35.port_a_last_address = 16383;
defparam ram_block1a35.port_a_logical_ram_depth = 65536;
defparam ram_block1a35.port_a_logical_ram_width = 18;
defparam ram_block1a35.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a35.ram_block_type = "auto";
defparam ram_block1a35.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a35.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a35.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a35.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a53(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a53_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a53.clk0_core_clock_enable = "ena0";
defparam ram_block1a53.clk0_input_clock_enable = "ena0";
defparam ram_block1a53.clk0_output_clock_enable = "ena0";
defparam ram_block1a53.data_interleave_offset_in_bits = 1;
defparam ram_block1a53.data_interleave_width_in_bits = 1;
defparam ram_block1a53.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a53.init_file_layout = "port_a";
defparam ram_block1a53.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a53.operation_mode = "rom";
defparam ram_block1a53.port_a_address_clear = "none";
defparam ram_block1a53.port_a_address_width = 13;
defparam ram_block1a53.port_a_data_out_clear = "none";
defparam ram_block1a53.port_a_data_out_clock = "clock0";
defparam ram_block1a53.port_a_data_width = 1;
defparam ram_block1a53.port_a_first_address = 16384;
defparam ram_block1a53.port_a_first_bit_number = 17;
defparam ram_block1a53.port_a_last_address = 24575;
defparam ram_block1a53.port_a_logical_ram_depth = 65536;
defparam ram_block1a53.port_a_logical_ram_width = 18;
defparam ram_block1a53.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a53.ram_block_type = "auto";
defparam ram_block1a53.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a53.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a53.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a53.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a17(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a17_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a17.clk0_core_clock_enable = "ena0";
defparam ram_block1a17.clk0_input_clock_enable = "ena0";
defparam ram_block1a17.clk0_output_clock_enable = "ena0";
defparam ram_block1a17.data_interleave_offset_in_bits = 1;
defparam ram_block1a17.data_interleave_width_in_bits = 1;
defparam ram_block1a17.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a17.init_file_layout = "port_a";
defparam ram_block1a17.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a17.operation_mode = "rom";
defparam ram_block1a17.port_a_address_clear = "none";
defparam ram_block1a17.port_a_address_width = 13;
defparam ram_block1a17.port_a_data_out_clear = "none";
defparam ram_block1a17.port_a_data_out_clock = "clock0";
defparam ram_block1a17.port_a_data_width = 1;
defparam ram_block1a17.port_a_first_address = 0;
defparam ram_block1a17.port_a_first_bit_number = 17;
defparam ram_block1a17.port_a_last_address = 8191;
defparam ram_block1a17.port_a_logical_ram_depth = 65536;
defparam ram_block1a17.port_a_logical_ram_width = 18;
defparam ram_block1a17.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a17.ram_block_type = "auto";
defparam ram_block1a17.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a17.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a17.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a17.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

cycloneive_ram_block ram_block1a71(
	.portawe(vcc),
	.portare(vcc),
	.portaaddrstall(gnd),
	.portbwe(gnd),
	.portbre(vcc),
	.portbaddrstall(gnd),
	.clk0(clock0),
	.clk1(gnd),
	.ena0(clocken0),
	.ena1(vcc),
	.ena2(vcc),
	.ena3(vcc),
	.clr0(gnd),
	.clr1(gnd),
	.portadatain(1'b0),
	.portaaddr({gnd,gnd,gnd,address_a[12],address_a[11],address_a[10],address_a[9],address_a[8],address_a[7],address_a[6],address_a[5],address_a[4],address_a[3],address_a[2],address_a[1],address_a[0]}),
	.portabyteenamasks(1'b1),
	.portbdatain(1'b0),
	.portbaddr(1'b0),
	.portbbyteenamasks(1'b1),
	.portadataout(ram_block1a71_PORTADATAOUT_bus),
	.portbdataout());
defparam ram_block1a71.clk0_core_clock_enable = "ena0";
defparam ram_block1a71.clk0_input_clock_enable = "ena0";
defparam ram_block1a71.clk0_output_clock_enable = "ena0";
defparam ram_block1a71.data_interleave_offset_in_bits = 1;
defparam ram_block1a71.data_interleave_width_in_bits = 1;
defparam ram_block1a71.init_file = "NCO_Party_nco_ii_0_sin.hex";
defparam ram_block1a71.init_file_layout = "port_a";
defparam ram_block1a71.logical_ram_name = "NCO_Party_nco_ii_0:nco_ii_0|asj_nco_as_m_cen:ux0120|altsyncram:altsyncram_component0|altsyncram_vea1:auto_generated|ALTSYNCRAM";
defparam ram_block1a71.operation_mode = "rom";
defparam ram_block1a71.port_a_address_clear = "none";
defparam ram_block1a71.port_a_address_width = 13;
defparam ram_block1a71.port_a_data_out_clear = "none";
defparam ram_block1a71.port_a_data_out_clock = "clock0";
defparam ram_block1a71.port_a_data_width = 1;
defparam ram_block1a71.port_a_first_address = 24576;
defparam ram_block1a71.port_a_first_bit_number = 17;
defparam ram_block1a71.port_a_last_address = 32767;
defparam ram_block1a71.port_a_logical_ram_depth = 65536;
defparam ram_block1a71.port_a_logical_ram_width = 18;
defparam ram_block1a71.port_a_read_during_write_mode = "new_data_with_nbe_read";
defparam ram_block1a71.ram_block_type = "auto";
defparam ram_block1a71.mem_init3 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a71.mem_init2 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a71.mem_init1 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
defparam ram_block1a71.mem_init0 = 2048'h00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;

dffeas \out_address_reg_a[1] (
	.clk(clock0),
	.d(\address_reg_a[1]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clocken0),
	.q(\out_address_reg_a[1]~q ),
	.prn(vcc));
defparam \out_address_reg_a[1] .is_wysiwyg = "true";
defparam \out_address_reg_a[1] .power_up = "low";

dffeas \out_address_reg_a[0] (
	.clk(clock0),
	.d(\address_reg_a[0]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clocken0),
	.q(\out_address_reg_a[0]~q ),
	.prn(vcc));
defparam \out_address_reg_a[0] .is_wysiwyg = "true";
defparam \out_address_reg_a[0] .power_up = "low";

dffeas \out_address_reg_a[2] (
	.clk(clock0),
	.d(\address_reg_a[2]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clocken0),
	.q(\out_address_reg_a[2]~q ),
	.prn(vcc));
defparam \out_address_reg_a[2] .is_wysiwyg = "true";
defparam \out_address_reg_a[2] .power_up = "low";

dffeas \address_reg_a[1] (
	.clk(clock0),
	.d(address_a[14]),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clocken0),
	.q(\address_reg_a[1]~q ),
	.prn(vcc));
defparam \address_reg_a[1] .is_wysiwyg = "true";
defparam \address_reg_a[1] .power_up = "low";

dffeas \address_reg_a[0] (
	.clk(clock0),
	.d(address_a[13]),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clocken0),
	.q(\address_reg_a[0]~q ),
	.prn(vcc));
defparam \address_reg_a[0] .is_wysiwyg = "true";
defparam \address_reg_a[0] .power_up = "low";

dffeas \address_reg_a[2] (
	.clk(clock0),
	.d(address_a[15]),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(clocken0),
	.q(\address_reg_a[2]~q ),
	.prn(vcc));
defparam \address_reg_a[2] .is_wysiwyg = "true";
defparam \address_reg_a[2] .power_up = "low";

endmodule

module NCO_Party_mux_sob (
	ram_block1a108,
	ram_block1a90,
	ram_block1a72,
	ram_block1a126,
	ram_block1a18,
	ram_block1a36,
	ram_block1a0,
	ram_block1a54,
	ram_block1a109,
	ram_block1a91,
	ram_block1a73,
	ram_block1a127,
	ram_block1a19,
	ram_block1a37,
	ram_block1a1,
	ram_block1a55,
	ram_block1a110,
	ram_block1a92,
	ram_block1a74,
	ram_block1a128,
	ram_block1a20,
	ram_block1a38,
	ram_block1a2,
	ram_block1a56,
	ram_block1a111,
	ram_block1a93,
	ram_block1a75,
	ram_block1a129,
	ram_block1a21,
	ram_block1a39,
	ram_block1a3,
	ram_block1a57,
	ram_block1a112,
	ram_block1a94,
	ram_block1a76,
	ram_block1a130,
	ram_block1a22,
	ram_block1a40,
	ram_block1a4,
	ram_block1a58,
	ram_block1a113,
	ram_block1a95,
	ram_block1a77,
	ram_block1a131,
	ram_block1a23,
	ram_block1a41,
	ram_block1a5,
	ram_block1a59,
	ram_block1a114,
	ram_block1a96,
	ram_block1a78,
	ram_block1a132,
	ram_block1a24,
	ram_block1a42,
	ram_block1a6,
	ram_block1a60,
	ram_block1a115,
	ram_block1a97,
	ram_block1a79,
	ram_block1a133,
	ram_block1a25,
	ram_block1a43,
	ram_block1a7,
	ram_block1a61,
	ram_block1a116,
	ram_block1a98,
	ram_block1a80,
	ram_block1a134,
	ram_block1a26,
	ram_block1a44,
	ram_block1a8,
	ram_block1a62,
	ram_block1a117,
	ram_block1a99,
	ram_block1a81,
	ram_block1a135,
	ram_block1a27,
	ram_block1a45,
	ram_block1a9,
	ram_block1a63,
	ram_block1a118,
	ram_block1a100,
	ram_block1a82,
	ram_block1a136,
	ram_block1a28,
	ram_block1a46,
	ram_block1a10,
	ram_block1a64,
	ram_block1a119,
	ram_block1a101,
	ram_block1a83,
	ram_block1a137,
	ram_block1a29,
	ram_block1a47,
	ram_block1a11,
	ram_block1a65,
	ram_block1a120,
	ram_block1a102,
	ram_block1a84,
	ram_block1a138,
	ram_block1a30,
	ram_block1a48,
	ram_block1a12,
	ram_block1a66,
	ram_block1a121,
	ram_block1a103,
	ram_block1a85,
	ram_block1a139,
	ram_block1a31,
	ram_block1a49,
	ram_block1a13,
	ram_block1a67,
	ram_block1a122,
	ram_block1a104,
	ram_block1a86,
	ram_block1a140,
	ram_block1a32,
	ram_block1a50,
	ram_block1a14,
	ram_block1a68,
	ram_block1a123,
	ram_block1a105,
	ram_block1a87,
	ram_block1a141,
	ram_block1a33,
	ram_block1a51,
	ram_block1a15,
	ram_block1a69,
	ram_block1a124,
	ram_block1a106,
	ram_block1a88,
	ram_block1a142,
	ram_block1a34,
	ram_block1a52,
	ram_block1a16,
	ram_block1a70,
	ram_block1a125,
	ram_block1a107,
	ram_block1a89,
	ram_block1a143,
	ram_block1a35,
	ram_block1a53,
	ram_block1a17,
	ram_block1a71,
	out_address_reg_a_1,
	out_address_reg_a_0,
	out_address_reg_a_2,
	_,
	_1,
	_2,
	_3,
	_4,
	_5,
	_6,
	_7,
	_8,
	_9,
	_10,
	_11,
	_12,
	_13,
	_14,
	_15,
	_16,
	_17)/* synthesis synthesis_greybox=1 */;
input 	ram_block1a108;
input 	ram_block1a90;
input 	ram_block1a72;
input 	ram_block1a126;
input 	ram_block1a18;
input 	ram_block1a36;
input 	ram_block1a0;
input 	ram_block1a54;
input 	ram_block1a109;
input 	ram_block1a91;
input 	ram_block1a73;
input 	ram_block1a127;
input 	ram_block1a19;
input 	ram_block1a37;
input 	ram_block1a1;
input 	ram_block1a55;
input 	ram_block1a110;
input 	ram_block1a92;
input 	ram_block1a74;
input 	ram_block1a128;
input 	ram_block1a20;
input 	ram_block1a38;
input 	ram_block1a2;
input 	ram_block1a56;
input 	ram_block1a111;
input 	ram_block1a93;
input 	ram_block1a75;
input 	ram_block1a129;
input 	ram_block1a21;
input 	ram_block1a39;
input 	ram_block1a3;
input 	ram_block1a57;
input 	ram_block1a112;
input 	ram_block1a94;
input 	ram_block1a76;
input 	ram_block1a130;
input 	ram_block1a22;
input 	ram_block1a40;
input 	ram_block1a4;
input 	ram_block1a58;
input 	ram_block1a113;
input 	ram_block1a95;
input 	ram_block1a77;
input 	ram_block1a131;
input 	ram_block1a23;
input 	ram_block1a41;
input 	ram_block1a5;
input 	ram_block1a59;
input 	ram_block1a114;
input 	ram_block1a96;
input 	ram_block1a78;
input 	ram_block1a132;
input 	ram_block1a24;
input 	ram_block1a42;
input 	ram_block1a6;
input 	ram_block1a60;
input 	ram_block1a115;
input 	ram_block1a97;
input 	ram_block1a79;
input 	ram_block1a133;
input 	ram_block1a25;
input 	ram_block1a43;
input 	ram_block1a7;
input 	ram_block1a61;
input 	ram_block1a116;
input 	ram_block1a98;
input 	ram_block1a80;
input 	ram_block1a134;
input 	ram_block1a26;
input 	ram_block1a44;
input 	ram_block1a8;
input 	ram_block1a62;
input 	ram_block1a117;
input 	ram_block1a99;
input 	ram_block1a81;
input 	ram_block1a135;
input 	ram_block1a27;
input 	ram_block1a45;
input 	ram_block1a9;
input 	ram_block1a63;
input 	ram_block1a118;
input 	ram_block1a100;
input 	ram_block1a82;
input 	ram_block1a136;
input 	ram_block1a28;
input 	ram_block1a46;
input 	ram_block1a10;
input 	ram_block1a64;
input 	ram_block1a119;
input 	ram_block1a101;
input 	ram_block1a83;
input 	ram_block1a137;
input 	ram_block1a29;
input 	ram_block1a47;
input 	ram_block1a11;
input 	ram_block1a65;
input 	ram_block1a120;
input 	ram_block1a102;
input 	ram_block1a84;
input 	ram_block1a138;
input 	ram_block1a30;
input 	ram_block1a48;
input 	ram_block1a12;
input 	ram_block1a66;
input 	ram_block1a121;
input 	ram_block1a103;
input 	ram_block1a85;
input 	ram_block1a139;
input 	ram_block1a31;
input 	ram_block1a49;
input 	ram_block1a13;
input 	ram_block1a67;
input 	ram_block1a122;
input 	ram_block1a104;
input 	ram_block1a86;
input 	ram_block1a140;
input 	ram_block1a32;
input 	ram_block1a50;
input 	ram_block1a14;
input 	ram_block1a68;
input 	ram_block1a123;
input 	ram_block1a105;
input 	ram_block1a87;
input 	ram_block1a141;
input 	ram_block1a33;
input 	ram_block1a51;
input 	ram_block1a15;
input 	ram_block1a69;
input 	ram_block1a124;
input 	ram_block1a106;
input 	ram_block1a88;
input 	ram_block1a142;
input 	ram_block1a34;
input 	ram_block1a52;
input 	ram_block1a16;
input 	ram_block1a70;
input 	ram_block1a125;
input 	ram_block1a107;
input 	ram_block1a89;
input 	ram_block1a143;
input 	ram_block1a35;
input 	ram_block1a53;
input 	ram_block1a17;
input 	ram_block1a71;
input 	out_address_reg_a_1;
input 	out_address_reg_a_0;
input 	out_address_reg_a_2;
output 	_;
output 	_1;
output 	_2;
output 	_3;
output 	_4;
output 	_5;
output 	_6;
output 	_7;
output 	_8;
output 	_9;
output 	_10;
output 	_11;
output 	_12;
output 	_13;
output 	_14;
output 	_15;
output 	_16;
output 	_17;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \_~0_combout ;
wire \_~1_combout ;
wire \_~2_combout ;
wire \_~3_combout ;
wire \_~5_combout ;
wire \_~6_combout ;
wire \_~7_combout ;
wire \_~8_combout ;
wire \_~10_combout ;
wire \_~11_combout ;
wire \_~12_combout ;
wire \_~13_combout ;
wire \_~15_combout ;
wire \_~16_combout ;
wire \_~17_combout ;
wire \_~18_combout ;
wire \_~20_combout ;
wire \_~21_combout ;
wire \_~22_combout ;
wire \_~23_combout ;
wire \_~25_combout ;
wire \_~26_combout ;
wire \_~27_combout ;
wire \_~28_combout ;
wire \_~30_combout ;
wire \_~31_combout ;
wire \_~32_combout ;
wire \_~33_combout ;
wire \_~35_combout ;
wire \_~36_combout ;
wire \_~37_combout ;
wire \_~38_combout ;
wire \_~40_combout ;
wire \_~41_combout ;
wire \_~42_combout ;
wire \_~43_combout ;
wire \_~45_combout ;
wire \_~46_combout ;
wire \_~47_combout ;
wire \_~48_combout ;
wire \_~50_combout ;
wire \_~51_combout ;
wire \_~52_combout ;
wire \_~53_combout ;
wire \_~55_combout ;
wire \_~56_combout ;
wire \_~57_combout ;
wire \_~58_combout ;
wire \_~60_combout ;
wire \_~61_combout ;
wire \_~62_combout ;
wire \_~63_combout ;
wire \_~65_combout ;
wire \_~66_combout ;
wire \_~67_combout ;
wire \_~68_combout ;
wire \_~70_combout ;
wire \_~71_combout ;
wire \_~72_combout ;
wire \_~73_combout ;
wire \_~75_combout ;
wire \_~76_combout ;
wire \_~77_combout ;
wire \_~78_combout ;
wire \_~80_combout ;
wire \_~81_combout ;
wire \_~82_combout ;
wire \_~83_combout ;
wire \_~85_combout ;
wire \_~86_combout ;
wire \_~87_combout ;
wire \_~88_combout ;


cycloneive_lcell_comb \_~4 (
	.dataa(\_~1_combout ),
	.datab(\_~3_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_),
	.cout());
defparam \_~4 .lut_mask = 16'hAACC;
defparam \_~4 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~9 (
	.dataa(\_~6_combout ),
	.datab(\_~8_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_1),
	.cout());
defparam \_~9 .lut_mask = 16'hAACC;
defparam \_~9 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~14 (
	.dataa(\_~11_combout ),
	.datab(\_~13_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_2),
	.cout());
defparam \_~14 .lut_mask = 16'hAACC;
defparam \_~14 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~19 (
	.dataa(\_~16_combout ),
	.datab(\_~18_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_3),
	.cout());
defparam \_~19 .lut_mask = 16'hAACC;
defparam \_~19 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~24 (
	.dataa(\_~21_combout ),
	.datab(\_~23_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_4),
	.cout());
defparam \_~24 .lut_mask = 16'hAACC;
defparam \_~24 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~29 (
	.dataa(\_~26_combout ),
	.datab(\_~28_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_5),
	.cout());
defparam \_~29 .lut_mask = 16'hAACC;
defparam \_~29 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~34 (
	.dataa(\_~31_combout ),
	.datab(\_~33_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_6),
	.cout());
defparam \_~34 .lut_mask = 16'hAACC;
defparam \_~34 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~39 (
	.dataa(\_~36_combout ),
	.datab(\_~38_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_7),
	.cout());
defparam \_~39 .lut_mask = 16'hAACC;
defparam \_~39 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~44 (
	.dataa(\_~41_combout ),
	.datab(\_~43_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_8),
	.cout());
defparam \_~44 .lut_mask = 16'hAACC;
defparam \_~44 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~49 (
	.dataa(\_~46_combout ),
	.datab(\_~48_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_9),
	.cout());
defparam \_~49 .lut_mask = 16'hAACC;
defparam \_~49 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~54 (
	.dataa(\_~51_combout ),
	.datab(\_~53_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_10),
	.cout());
defparam \_~54 .lut_mask = 16'hAACC;
defparam \_~54 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~59 (
	.dataa(\_~56_combout ),
	.datab(\_~58_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_11),
	.cout());
defparam \_~59 .lut_mask = 16'hAACC;
defparam \_~59 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~64 (
	.dataa(\_~61_combout ),
	.datab(\_~63_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_12),
	.cout());
defparam \_~64 .lut_mask = 16'hAACC;
defparam \_~64 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~69 (
	.dataa(\_~66_combout ),
	.datab(\_~68_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_13),
	.cout());
defparam \_~69 .lut_mask = 16'hAACC;
defparam \_~69 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~74 (
	.dataa(\_~71_combout ),
	.datab(\_~73_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_14),
	.cout());
defparam \_~74 .lut_mask = 16'hAACC;
defparam \_~74 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~79 (
	.dataa(\_~76_combout ),
	.datab(\_~78_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_15),
	.cout());
defparam \_~79 .lut_mask = 16'hAACC;
defparam \_~79 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~84 (
	.dataa(\_~81_combout ),
	.datab(\_~83_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_16),
	.cout());
defparam \_~84 .lut_mask = 16'hAACC;
defparam \_~84 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~89 (
	.dataa(\_~86_combout ),
	.datab(\_~88_combout ),
	.datac(gnd),
	.datad(out_address_reg_a_2),
	.cin(gnd),
	.combout(_17),
	.cout());
defparam \_~89 .lut_mask = 16'hAACC;
defparam \_~89 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~0 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a90),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a72),
	.cin(gnd),
	.combout(\_~0_combout ),
	.cout());
defparam \_~0 .lut_mask = 16'hFFDE;
defparam \_~0 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~1 (
	.dataa(ram_block1a108),
	.datab(out_address_reg_a_1),
	.datac(\_~0_combout ),
	.datad(ram_block1a126),
	.cin(gnd),
	.combout(\_~1_combout ),
	.cout());
defparam \_~1 .lut_mask = 16'hFFBE;
defparam \_~1 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~2 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a36),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a0),
	.cin(gnd),
	.combout(\_~2_combout ),
	.cout());
defparam \_~2 .lut_mask = 16'hFFDE;
defparam \_~2 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~3 (
	.dataa(ram_block1a18),
	.datab(out_address_reg_a_0),
	.datac(\_~2_combout ),
	.datad(ram_block1a54),
	.cin(gnd),
	.combout(\_~3_combout ),
	.cout());
defparam \_~3 .lut_mask = 16'hFFBE;
defparam \_~3 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~5 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a91),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a73),
	.cin(gnd),
	.combout(\_~5_combout ),
	.cout());
defparam \_~5 .lut_mask = 16'hFFDE;
defparam \_~5 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~6 (
	.dataa(ram_block1a109),
	.datab(out_address_reg_a_1),
	.datac(\_~5_combout ),
	.datad(ram_block1a127),
	.cin(gnd),
	.combout(\_~6_combout ),
	.cout());
defparam \_~6 .lut_mask = 16'hFFBE;
defparam \_~6 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~7 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a37),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a1),
	.cin(gnd),
	.combout(\_~7_combout ),
	.cout());
defparam \_~7 .lut_mask = 16'hFFDE;
defparam \_~7 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~8 (
	.dataa(ram_block1a19),
	.datab(out_address_reg_a_0),
	.datac(\_~7_combout ),
	.datad(ram_block1a55),
	.cin(gnd),
	.combout(\_~8_combout ),
	.cout());
defparam \_~8 .lut_mask = 16'hFFBE;
defparam \_~8 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~10 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a92),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a74),
	.cin(gnd),
	.combout(\_~10_combout ),
	.cout());
defparam \_~10 .lut_mask = 16'hFFDE;
defparam \_~10 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~11 (
	.dataa(ram_block1a110),
	.datab(out_address_reg_a_1),
	.datac(\_~10_combout ),
	.datad(ram_block1a128),
	.cin(gnd),
	.combout(\_~11_combout ),
	.cout());
defparam \_~11 .lut_mask = 16'hFFBE;
defparam \_~11 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~12 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a38),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a2),
	.cin(gnd),
	.combout(\_~12_combout ),
	.cout());
defparam \_~12 .lut_mask = 16'hFFDE;
defparam \_~12 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~13 (
	.dataa(ram_block1a20),
	.datab(out_address_reg_a_0),
	.datac(\_~12_combout ),
	.datad(ram_block1a56),
	.cin(gnd),
	.combout(\_~13_combout ),
	.cout());
defparam \_~13 .lut_mask = 16'hFFBE;
defparam \_~13 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~15 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a93),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a75),
	.cin(gnd),
	.combout(\_~15_combout ),
	.cout());
defparam \_~15 .lut_mask = 16'hFFDE;
defparam \_~15 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~16 (
	.dataa(ram_block1a111),
	.datab(out_address_reg_a_1),
	.datac(\_~15_combout ),
	.datad(ram_block1a129),
	.cin(gnd),
	.combout(\_~16_combout ),
	.cout());
defparam \_~16 .lut_mask = 16'hFFBE;
defparam \_~16 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~17 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a39),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a3),
	.cin(gnd),
	.combout(\_~17_combout ),
	.cout());
defparam \_~17 .lut_mask = 16'hFFDE;
defparam \_~17 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~18 (
	.dataa(ram_block1a21),
	.datab(out_address_reg_a_0),
	.datac(\_~17_combout ),
	.datad(ram_block1a57),
	.cin(gnd),
	.combout(\_~18_combout ),
	.cout());
defparam \_~18 .lut_mask = 16'hFFBE;
defparam \_~18 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~20 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a94),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a76),
	.cin(gnd),
	.combout(\_~20_combout ),
	.cout());
defparam \_~20 .lut_mask = 16'hFFDE;
defparam \_~20 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~21 (
	.dataa(ram_block1a112),
	.datab(out_address_reg_a_1),
	.datac(\_~20_combout ),
	.datad(ram_block1a130),
	.cin(gnd),
	.combout(\_~21_combout ),
	.cout());
defparam \_~21 .lut_mask = 16'hFFBE;
defparam \_~21 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~22 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a40),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a4),
	.cin(gnd),
	.combout(\_~22_combout ),
	.cout());
defparam \_~22 .lut_mask = 16'hFFDE;
defparam \_~22 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~23 (
	.dataa(ram_block1a22),
	.datab(out_address_reg_a_0),
	.datac(\_~22_combout ),
	.datad(ram_block1a58),
	.cin(gnd),
	.combout(\_~23_combout ),
	.cout());
defparam \_~23 .lut_mask = 16'hFFBE;
defparam \_~23 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~25 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a95),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a77),
	.cin(gnd),
	.combout(\_~25_combout ),
	.cout());
defparam \_~25 .lut_mask = 16'hFFDE;
defparam \_~25 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~26 (
	.dataa(ram_block1a113),
	.datab(out_address_reg_a_1),
	.datac(\_~25_combout ),
	.datad(ram_block1a131),
	.cin(gnd),
	.combout(\_~26_combout ),
	.cout());
defparam \_~26 .lut_mask = 16'hFFBE;
defparam \_~26 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~27 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a41),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a5),
	.cin(gnd),
	.combout(\_~27_combout ),
	.cout());
defparam \_~27 .lut_mask = 16'hFFDE;
defparam \_~27 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~28 (
	.dataa(ram_block1a23),
	.datab(out_address_reg_a_0),
	.datac(\_~27_combout ),
	.datad(ram_block1a59),
	.cin(gnd),
	.combout(\_~28_combout ),
	.cout());
defparam \_~28 .lut_mask = 16'hFFBE;
defparam \_~28 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~30 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a96),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a78),
	.cin(gnd),
	.combout(\_~30_combout ),
	.cout());
defparam \_~30 .lut_mask = 16'hFFDE;
defparam \_~30 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~31 (
	.dataa(ram_block1a114),
	.datab(out_address_reg_a_1),
	.datac(\_~30_combout ),
	.datad(ram_block1a132),
	.cin(gnd),
	.combout(\_~31_combout ),
	.cout());
defparam \_~31 .lut_mask = 16'hFFBE;
defparam \_~31 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~32 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a42),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a6),
	.cin(gnd),
	.combout(\_~32_combout ),
	.cout());
defparam \_~32 .lut_mask = 16'hFFDE;
defparam \_~32 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~33 (
	.dataa(ram_block1a24),
	.datab(out_address_reg_a_0),
	.datac(\_~32_combout ),
	.datad(ram_block1a60),
	.cin(gnd),
	.combout(\_~33_combout ),
	.cout());
defparam \_~33 .lut_mask = 16'hFFBE;
defparam \_~33 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~35 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a97),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a79),
	.cin(gnd),
	.combout(\_~35_combout ),
	.cout());
defparam \_~35 .lut_mask = 16'hFFDE;
defparam \_~35 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~36 (
	.dataa(ram_block1a115),
	.datab(out_address_reg_a_1),
	.datac(\_~35_combout ),
	.datad(ram_block1a133),
	.cin(gnd),
	.combout(\_~36_combout ),
	.cout());
defparam \_~36 .lut_mask = 16'hFFBE;
defparam \_~36 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~37 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a43),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a7),
	.cin(gnd),
	.combout(\_~37_combout ),
	.cout());
defparam \_~37 .lut_mask = 16'hFFDE;
defparam \_~37 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~38 (
	.dataa(ram_block1a25),
	.datab(out_address_reg_a_0),
	.datac(\_~37_combout ),
	.datad(ram_block1a61),
	.cin(gnd),
	.combout(\_~38_combout ),
	.cout());
defparam \_~38 .lut_mask = 16'hFFBE;
defparam \_~38 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~40 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a98),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a80),
	.cin(gnd),
	.combout(\_~40_combout ),
	.cout());
defparam \_~40 .lut_mask = 16'hFFDE;
defparam \_~40 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~41 (
	.dataa(ram_block1a116),
	.datab(out_address_reg_a_1),
	.datac(\_~40_combout ),
	.datad(ram_block1a134),
	.cin(gnd),
	.combout(\_~41_combout ),
	.cout());
defparam \_~41 .lut_mask = 16'hFFBE;
defparam \_~41 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~42 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a44),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a8),
	.cin(gnd),
	.combout(\_~42_combout ),
	.cout());
defparam \_~42 .lut_mask = 16'hFFDE;
defparam \_~42 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~43 (
	.dataa(ram_block1a26),
	.datab(out_address_reg_a_0),
	.datac(\_~42_combout ),
	.datad(ram_block1a62),
	.cin(gnd),
	.combout(\_~43_combout ),
	.cout());
defparam \_~43 .lut_mask = 16'hFFBE;
defparam \_~43 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~45 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a99),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a81),
	.cin(gnd),
	.combout(\_~45_combout ),
	.cout());
defparam \_~45 .lut_mask = 16'hFFDE;
defparam \_~45 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~46 (
	.dataa(ram_block1a117),
	.datab(out_address_reg_a_1),
	.datac(\_~45_combout ),
	.datad(ram_block1a135),
	.cin(gnd),
	.combout(\_~46_combout ),
	.cout());
defparam \_~46 .lut_mask = 16'hFFBE;
defparam \_~46 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~47 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a45),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a9),
	.cin(gnd),
	.combout(\_~47_combout ),
	.cout());
defparam \_~47 .lut_mask = 16'hFFDE;
defparam \_~47 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~48 (
	.dataa(ram_block1a27),
	.datab(out_address_reg_a_0),
	.datac(\_~47_combout ),
	.datad(ram_block1a63),
	.cin(gnd),
	.combout(\_~48_combout ),
	.cout());
defparam \_~48 .lut_mask = 16'hFFBE;
defparam \_~48 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~50 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a100),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a82),
	.cin(gnd),
	.combout(\_~50_combout ),
	.cout());
defparam \_~50 .lut_mask = 16'hFFDE;
defparam \_~50 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~51 (
	.dataa(ram_block1a118),
	.datab(out_address_reg_a_1),
	.datac(\_~50_combout ),
	.datad(ram_block1a136),
	.cin(gnd),
	.combout(\_~51_combout ),
	.cout());
defparam \_~51 .lut_mask = 16'hFFBE;
defparam \_~51 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~52 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a46),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a10),
	.cin(gnd),
	.combout(\_~52_combout ),
	.cout());
defparam \_~52 .lut_mask = 16'hFFDE;
defparam \_~52 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~53 (
	.dataa(ram_block1a28),
	.datab(out_address_reg_a_0),
	.datac(\_~52_combout ),
	.datad(ram_block1a64),
	.cin(gnd),
	.combout(\_~53_combout ),
	.cout());
defparam \_~53 .lut_mask = 16'hFFBE;
defparam \_~53 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~55 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a101),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a83),
	.cin(gnd),
	.combout(\_~55_combout ),
	.cout());
defparam \_~55 .lut_mask = 16'hFFDE;
defparam \_~55 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~56 (
	.dataa(ram_block1a119),
	.datab(out_address_reg_a_1),
	.datac(\_~55_combout ),
	.datad(ram_block1a137),
	.cin(gnd),
	.combout(\_~56_combout ),
	.cout());
defparam \_~56 .lut_mask = 16'hFFBE;
defparam \_~56 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~57 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a47),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a11),
	.cin(gnd),
	.combout(\_~57_combout ),
	.cout());
defparam \_~57 .lut_mask = 16'hFFDE;
defparam \_~57 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~58 (
	.dataa(ram_block1a29),
	.datab(out_address_reg_a_0),
	.datac(\_~57_combout ),
	.datad(ram_block1a65),
	.cin(gnd),
	.combout(\_~58_combout ),
	.cout());
defparam \_~58 .lut_mask = 16'hFFBE;
defparam \_~58 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~60 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a102),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a84),
	.cin(gnd),
	.combout(\_~60_combout ),
	.cout());
defparam \_~60 .lut_mask = 16'hFFDE;
defparam \_~60 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~61 (
	.dataa(ram_block1a120),
	.datab(out_address_reg_a_1),
	.datac(\_~60_combout ),
	.datad(ram_block1a138),
	.cin(gnd),
	.combout(\_~61_combout ),
	.cout());
defparam \_~61 .lut_mask = 16'hFFBE;
defparam \_~61 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~62 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a48),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a12),
	.cin(gnd),
	.combout(\_~62_combout ),
	.cout());
defparam \_~62 .lut_mask = 16'hFFDE;
defparam \_~62 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~63 (
	.dataa(ram_block1a30),
	.datab(out_address_reg_a_0),
	.datac(\_~62_combout ),
	.datad(ram_block1a66),
	.cin(gnd),
	.combout(\_~63_combout ),
	.cout());
defparam \_~63 .lut_mask = 16'hFFBE;
defparam \_~63 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~65 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a103),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a85),
	.cin(gnd),
	.combout(\_~65_combout ),
	.cout());
defparam \_~65 .lut_mask = 16'hFFDE;
defparam \_~65 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~66 (
	.dataa(ram_block1a121),
	.datab(out_address_reg_a_1),
	.datac(\_~65_combout ),
	.datad(ram_block1a139),
	.cin(gnd),
	.combout(\_~66_combout ),
	.cout());
defparam \_~66 .lut_mask = 16'hFFBE;
defparam \_~66 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~67 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a49),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a13),
	.cin(gnd),
	.combout(\_~67_combout ),
	.cout());
defparam \_~67 .lut_mask = 16'hFFDE;
defparam \_~67 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~68 (
	.dataa(ram_block1a31),
	.datab(out_address_reg_a_0),
	.datac(\_~67_combout ),
	.datad(ram_block1a67),
	.cin(gnd),
	.combout(\_~68_combout ),
	.cout());
defparam \_~68 .lut_mask = 16'hFFBE;
defparam \_~68 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~70 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a104),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a86),
	.cin(gnd),
	.combout(\_~70_combout ),
	.cout());
defparam \_~70 .lut_mask = 16'hFFDE;
defparam \_~70 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~71 (
	.dataa(ram_block1a122),
	.datab(out_address_reg_a_1),
	.datac(\_~70_combout ),
	.datad(ram_block1a140),
	.cin(gnd),
	.combout(\_~71_combout ),
	.cout());
defparam \_~71 .lut_mask = 16'hFFBE;
defparam \_~71 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~72 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a50),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a14),
	.cin(gnd),
	.combout(\_~72_combout ),
	.cout());
defparam \_~72 .lut_mask = 16'hFFDE;
defparam \_~72 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~73 (
	.dataa(ram_block1a32),
	.datab(out_address_reg_a_0),
	.datac(\_~72_combout ),
	.datad(ram_block1a68),
	.cin(gnd),
	.combout(\_~73_combout ),
	.cout());
defparam \_~73 .lut_mask = 16'hFFBE;
defparam \_~73 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~75 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a105),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a87),
	.cin(gnd),
	.combout(\_~75_combout ),
	.cout());
defparam \_~75 .lut_mask = 16'hFFDE;
defparam \_~75 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~76 (
	.dataa(ram_block1a123),
	.datab(out_address_reg_a_1),
	.datac(\_~75_combout ),
	.datad(ram_block1a141),
	.cin(gnd),
	.combout(\_~76_combout ),
	.cout());
defparam \_~76 .lut_mask = 16'hFFBE;
defparam \_~76 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~77 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a51),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a15),
	.cin(gnd),
	.combout(\_~77_combout ),
	.cout());
defparam \_~77 .lut_mask = 16'hFFDE;
defparam \_~77 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~78 (
	.dataa(ram_block1a33),
	.datab(out_address_reg_a_0),
	.datac(\_~77_combout ),
	.datad(ram_block1a69),
	.cin(gnd),
	.combout(\_~78_combout ),
	.cout());
defparam \_~78 .lut_mask = 16'hFFBE;
defparam \_~78 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~80 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a106),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a88),
	.cin(gnd),
	.combout(\_~80_combout ),
	.cout());
defparam \_~80 .lut_mask = 16'hFFDE;
defparam \_~80 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~81 (
	.dataa(ram_block1a124),
	.datab(out_address_reg_a_1),
	.datac(\_~80_combout ),
	.datad(ram_block1a142),
	.cin(gnd),
	.combout(\_~81_combout ),
	.cout());
defparam \_~81 .lut_mask = 16'hFFBE;
defparam \_~81 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~82 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a52),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a16),
	.cin(gnd),
	.combout(\_~82_combout ),
	.cout());
defparam \_~82 .lut_mask = 16'hFFDE;
defparam \_~82 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~83 (
	.dataa(ram_block1a34),
	.datab(out_address_reg_a_0),
	.datac(\_~82_combout ),
	.datad(ram_block1a70),
	.cin(gnd),
	.combout(\_~83_combout ),
	.cout());
defparam \_~83 .lut_mask = 16'hFFBE;
defparam \_~83 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~85 (
	.dataa(out_address_reg_a_1),
	.datab(ram_block1a107),
	.datac(out_address_reg_a_0),
	.datad(ram_block1a89),
	.cin(gnd),
	.combout(\_~85_combout ),
	.cout());
defparam \_~85 .lut_mask = 16'hFFDE;
defparam \_~85 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~86 (
	.dataa(ram_block1a125),
	.datab(out_address_reg_a_1),
	.datac(\_~85_combout ),
	.datad(ram_block1a143),
	.cin(gnd),
	.combout(\_~86_combout ),
	.cout());
defparam \_~86 .lut_mask = 16'hFFBE;
defparam \_~86 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~87 (
	.dataa(out_address_reg_a_0),
	.datab(ram_block1a53),
	.datac(out_address_reg_a_1),
	.datad(ram_block1a17),
	.cin(gnd),
	.combout(\_~87_combout ),
	.cout());
defparam \_~87 .lut_mask = 16'hFFDE;
defparam \_~87 .sum_lutc_input = "datac";

cycloneive_lcell_comb \_~88 (
	.dataa(ram_block1a35),
	.datab(out_address_reg_a_0),
	.datac(\_~87_combout ),
	.datad(ram_block1a71),
	.cin(gnd),
	.combout(\_~88_combout ),
	.cout());
defparam \_~88 .lut_mask = 16'hFFBE;
defparam \_~88 .sum_lutc_input = "datac";

endmodule

module NCO_Party_asj_nco_isdr (
	data_ready1,
	GND_port,
	clk,
	reset_n,
	clken)/* synthesis synthesis_greybox=1 */;
output 	data_ready1;
input 	GND_port;
input 	clk;
input 	reset_n;
input 	clken;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \lpm_counter_component|auto_generated|counter_reg_bit[2]~q ;
wire \lpm_counter_component|auto_generated|counter_reg_bit[1]~q ;
wire \lpm_counter_component|auto_generated|counter_reg_bit[0]~q ;
wire \always0~0_combout ;
wire \data_ready~0_combout ;


NCO_Party_lpm_counter_1 lpm_counter_component(
	.counter_reg_bit_2(\lpm_counter_component|auto_generated|counter_reg_bit[2]~q ),
	.counter_reg_bit_1(\lpm_counter_component|auto_generated|counter_reg_bit[1]~q ),
	.counter_reg_bit_0(\lpm_counter_component|auto_generated|counter_reg_bit[0]~q ),
	.GND_port(GND_port),
	.clock(clk),
	.reset_n(reset_n),
	.clken(clken));

dffeas data_ready(
	.clk(clk),
	.d(\data_ready~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(vcc),
	.q(data_ready1),
	.prn(vcc));
defparam data_ready.is_wysiwyg = "true";
defparam data_ready.power_up = "low";

cycloneive_lcell_comb \always0~0 (
	.dataa(clken),
	.datab(\lpm_counter_component|auto_generated|counter_reg_bit[2]~q ),
	.datac(\lpm_counter_component|auto_generated|counter_reg_bit[1]~q ),
	.datad(\lpm_counter_component|auto_generated|counter_reg_bit[0]~q ),
	.cin(gnd),
	.combout(\always0~0_combout ),
	.cout());
defparam \always0~0 .lut_mask = 16'hFEFF;
defparam \always0~0 .sum_lutc_input = "datac";

cycloneive_lcell_comb \data_ready~0 (
	.dataa(data_ready1),
	.datab(\always0~0_combout ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\data_ready~0_combout ),
	.cout());
defparam \data_ready~0 .lut_mask = 16'hEEEE;
defparam \data_ready~0 .sum_lutc_input = "datac";

endmodule

module NCO_Party_lpm_counter_1 (
	counter_reg_bit_2,
	counter_reg_bit_1,
	counter_reg_bit_0,
	GND_port,
	clock,
	reset_n,
	clken)/* synthesis synthesis_greybox=1 */;
output 	counter_reg_bit_2;
output 	counter_reg_bit_1;
output 	counter_reg_bit_0;
input 	GND_port;
input 	clock;
input 	reset_n;
input 	clken;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;



NCO_Party_cntr_9si auto_generated(
	.counter_reg_bit_2(counter_reg_bit_2),
	.counter_reg_bit_1(counter_reg_bit_1),
	.counter_reg_bit_0(counter_reg_bit_0),
	.GND_port(GND_port),
	.clock(clock),
	.reset_n(reset_n),
	.clken(clken));

endmodule

module NCO_Party_cntr_9si (
	counter_reg_bit_2,
	counter_reg_bit_1,
	counter_reg_bit_0,
	GND_port,
	clock,
	reset_n,
	clken)/* synthesis synthesis_greybox=1 */;
output 	counter_reg_bit_2;
output 	counter_reg_bit_1;
output 	counter_reg_bit_0;
input 	GND_port;
input 	clock;
input 	reset_n;
input 	clken;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \counter_comb_bita0~COUT ;
wire \counter_comb_bita1~COUT ;
wire \counter_comb_bita2~combout ;
wire \counter_comb_bita1~combout ;
wire \counter_comb_bita0~combout ;


dffeas \counter_reg_bit[2] (
	.clk(clock),
	.d(\counter_comb_bita2~combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(!reset_n),
	.ena(clken),
	.q(counter_reg_bit_2),
	.prn(vcc));
defparam \counter_reg_bit[2] .is_wysiwyg = "true";
defparam \counter_reg_bit[2] .power_up = "low";

dffeas \counter_reg_bit[1] (
	.clk(clock),
	.d(\counter_comb_bita1~combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(!reset_n),
	.ena(clken),
	.q(counter_reg_bit_1),
	.prn(vcc));
defparam \counter_reg_bit[1] .is_wysiwyg = "true";
defparam \counter_reg_bit[1] .power_up = "low";

dffeas \counter_reg_bit[0] (
	.clk(clock),
	.d(\counter_comb_bita0~combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(!reset_n),
	.ena(clken),
	.q(counter_reg_bit_0),
	.prn(vcc));
defparam \counter_reg_bit[0] .is_wysiwyg = "true";
defparam \counter_reg_bit[0] .power_up = "low";

cycloneive_lcell_comb counter_comb_bita0(
	.dataa(counter_reg_bit_0),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(gnd),
	.combout(\counter_comb_bita0~combout ),
	.cout(\counter_comb_bita0~COUT ));
defparam counter_comb_bita0.lut_mask = 16'h55AA;
defparam counter_comb_bita0.sum_lutc_input = "cin";

cycloneive_lcell_comb counter_comb_bita1(
	.dataa(counter_reg_bit_1),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\counter_comb_bita0~COUT ),
	.combout(\counter_comb_bita1~combout ),
	.cout(\counter_comb_bita1~COUT ));
defparam counter_comb_bita1.lut_mask = 16'h5A5F;
defparam counter_comb_bita1.sum_lutc_input = "cin";

cycloneive_lcell_comb counter_comb_bita2(
	.dataa(counter_reg_bit_2),
	.datab(gnd),
	.datac(gnd),
	.datad(gnd),
	.cin(\counter_comb_bita1~COUT ),
	.combout(\counter_comb_bita2~combout ),
	.cout());
defparam counter_comb_bita2.lut_mask = 16'h5A5A;
defparam counter_comb_bita2.sum_lutc_input = "cin";

endmodule

module NCO_Party_asj_nco_mob_rw (
	data_out_0,
	data_out_1,
	data_out_2,
	data_out_3,
	data_out_4,
	data_out_5,
	data_out_6,
	data_out_7,
	data_out_8,
	data_out_9,
	data_out_10,
	data_out_11,
	data_out_12,
	data_out_13,
	data_out_14,
	data_out_15,
	data_out_16,
	data_out_17,
	_,
	data_out_81,
	_1,
	_2,
	_3,
	_4,
	_5,
	_6,
	_7,
	_8,
	_9,
	_10,
	_11,
	_12,
	_13,
	_14,
	_15,
	_16,
	_17,
	clk,
	reset_n,
	clken)/* synthesis synthesis_greybox=1 */;
output 	data_out_0;
output 	data_out_1;
output 	data_out_2;
output 	data_out_3;
output 	data_out_4;
output 	data_out_5;
output 	data_out_6;
output 	data_out_7;
output 	data_out_8;
output 	data_out_9;
output 	data_out_10;
output 	data_out_11;
output 	data_out_12;
output 	data_out_13;
output 	data_out_14;
output 	data_out_15;
output 	data_out_16;
output 	data_out_17;
input 	_;
output 	data_out_81;
input 	_1;
input 	_2;
input 	_3;
input 	_4;
input 	_5;
input 	_6;
input 	_7;
input 	_8;
input 	_9;
input 	_10;
input 	_11;
input 	_12;
input 	_13;
input 	_14;
input 	_15;
input 	_16;
input 	_17;
input 	clk;
input 	reset_n;
input 	clken;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;



dffeas \data_out[0] (
	.clk(clk),
	.d(_),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_0),
	.prn(vcc));
defparam \data_out[0] .is_wysiwyg = "true";
defparam \data_out[0] .power_up = "low";

dffeas \data_out[1] (
	.clk(clk),
	.d(_1),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_1),
	.prn(vcc));
defparam \data_out[1] .is_wysiwyg = "true";
defparam \data_out[1] .power_up = "low";

dffeas \data_out[2] (
	.clk(clk),
	.d(_2),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_2),
	.prn(vcc));
defparam \data_out[2] .is_wysiwyg = "true";
defparam \data_out[2] .power_up = "low";

dffeas \data_out[3] (
	.clk(clk),
	.d(_3),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_3),
	.prn(vcc));
defparam \data_out[3] .is_wysiwyg = "true";
defparam \data_out[3] .power_up = "low";

dffeas \data_out[4] (
	.clk(clk),
	.d(_4),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_4),
	.prn(vcc));
defparam \data_out[4] .is_wysiwyg = "true";
defparam \data_out[4] .power_up = "low";

dffeas \data_out[5] (
	.clk(clk),
	.d(_5),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_5),
	.prn(vcc));
defparam \data_out[5] .is_wysiwyg = "true";
defparam \data_out[5] .power_up = "low";

dffeas \data_out[6] (
	.clk(clk),
	.d(_6),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_6),
	.prn(vcc));
defparam \data_out[6] .is_wysiwyg = "true";
defparam \data_out[6] .power_up = "low";

dffeas \data_out[7] (
	.clk(clk),
	.d(_7),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_7),
	.prn(vcc));
defparam \data_out[7] .is_wysiwyg = "true";
defparam \data_out[7] .power_up = "low";

dffeas \data_out[8] (
	.clk(clk),
	.d(_8),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_8),
	.prn(vcc));
defparam \data_out[8] .is_wysiwyg = "true";
defparam \data_out[8] .power_up = "low";

dffeas \data_out[9] (
	.clk(clk),
	.d(_9),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_9),
	.prn(vcc));
defparam \data_out[9] .is_wysiwyg = "true";
defparam \data_out[9] .power_up = "low";

dffeas \data_out[10] (
	.clk(clk),
	.d(_10),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_10),
	.prn(vcc));
defparam \data_out[10] .is_wysiwyg = "true";
defparam \data_out[10] .power_up = "low";

dffeas \data_out[11] (
	.clk(clk),
	.d(_11),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_11),
	.prn(vcc));
defparam \data_out[11] .is_wysiwyg = "true";
defparam \data_out[11] .power_up = "low";

dffeas \data_out[12] (
	.clk(clk),
	.d(_12),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_12),
	.prn(vcc));
defparam \data_out[12] .is_wysiwyg = "true";
defparam \data_out[12] .power_up = "low";

dffeas \data_out[13] (
	.clk(clk),
	.d(_13),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_13),
	.prn(vcc));
defparam \data_out[13] .is_wysiwyg = "true";
defparam \data_out[13] .power_up = "low";

dffeas \data_out[14] (
	.clk(clk),
	.d(_14),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_14),
	.prn(vcc));
defparam \data_out[14] .is_wysiwyg = "true";
defparam \data_out[14] .power_up = "low";

dffeas \data_out[15] (
	.clk(clk),
	.d(_15),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_15),
	.prn(vcc));
defparam \data_out[15] .is_wysiwyg = "true";
defparam \data_out[15] .power_up = "low";

dffeas \data_out[16] (
	.clk(clk),
	.d(_16),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_16),
	.prn(vcc));
defparam \data_out[16] .is_wysiwyg = "true";
defparam \data_out[16] .power_up = "low";

dffeas \data_out[17] (
	.clk(clk),
	.d(_17),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(!reset_n),
	.sload(gnd),
	.ena(data_out_81),
	.q(data_out_17),
	.prn(vcc));
defparam \data_out[17] .is_wysiwyg = "true";
defparam \data_out[17] .power_up = "low";

cycloneive_lcell_comb \data_out[8]~0 (
	.dataa(clken),
	.datab(gnd),
	.datac(gnd),
	.datad(reset_n),
	.cin(gnd),
	.combout(data_out_81),
	.cout());
defparam \data_out[8]~0 .lut_mask = 16'hAAFF;
defparam \data_out[8]~0 .sum_lutc_input = "datac";

endmodule
