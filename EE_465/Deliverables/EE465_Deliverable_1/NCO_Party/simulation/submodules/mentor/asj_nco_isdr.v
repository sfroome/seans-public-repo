// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 07:24:49 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
cL6/dkrFx4lAEyY0jiYvQBwJqfdIa2HD1ZoR0C6ou7fa3NquQzb4sV0wwW5B3HmE
1bU702E8e2HFBxRk+q3h5gp0j6NM1eGt7Dip8mRQexL1Cq7GwhpYT6jNkBKVb/6x
kOrFXZT2lcl43V7CdeSi/+kyZAc2lwXQwzCdpSse2PI=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3008)
DmkTgKru3zIBbu/wb/wI+Fp7IyGNgSuV+Umh0OYVwyF8iwWJPrAzCPYiz64n8qNY
ymYn5I+skgV1C0775Mxwg3NxOMMm9MbOLoys6BDSC/K/9ribNAs4hShF2oxWRCG+
aKE7QFpm/8oLFN42PTh4kctoI4tL5q4EJfeGIK5eU1vrDFInBOiCYFjloKI7uR0a
m8fqHdhSG2hN64AwLALPOlwSldqcgOYm11ql4kUDrR7J99lbsiHZXTb4+q+31rQd
nKEDgDnPiWlr9On8OqbS1nTIsuIoN/3ygdi0qgHqO+Nlb3kA2pNlww/Y0/lyNL6I
OJ9QySoe0taXBD9hsxN1ISyNm+2ZtK3cMVdjiY3d/tm0CjwtUH+KMJiK3NY1PXc6
mFo0XcrQm+pVM8yMWxwm6PsvNXc6iUAXYzYBlUbEXgZOfJZI+RwcifkrTeobwbcS
ZJYUYj2bM+AlnW3MDUqeHj9Pi+nTYwWt7VhHAVSF7/w0IHvYa+8op7owi7p2X+Dc
8lPWaMPfV0FvEauE++cy+xJFYuMjvQrCy5PXcntUc/R53NOm/Db71/44VIeTcf3e
b2GP5r+mTfIFdA0Si98kCzTqJsD2VpXuewt/a8Df/ZFL5aN6+HpmGcrM2KW/9TTq
qpQ2s7r7WWnOk7+rYiBeGXZhpcq93uLJmep8IH/g/M64itxHdmjzvJzKyC/8qtZL
dKnSq1wPXatuMjVXNrorKLRACfbYIpmqU4COv1s7GNb0ih+6hbH8XOH116T148HV
FZqHHzmAfMUFyCVtHmYL7XXDJIhsGtem+fOhu2HGgoTu0X2TxMpuA+klvlrjipQn
fZ79W3t6AGzYBUbjAm1tv/0GOe2JLIuICxUkih8AM6gZc4BA6OQbFrXqJrlLDt7g
XrXEDKPE6bQQr62jTVXFNYwsmpY4DcucWGwKo5So25KMELz+/sntY1Yu+p214WfD
yaX02HdW8IUy88izd1wZPAmOrtmYJCMtJ/PtBJezho9t5FgYkLiP6A861b9nUh6V
7N4oh0FHCFD/4XAuQLBh986/hqd096A9PjQKTi2+tnAh7HHNn3wL/H7/wtSXF9aN
BFQAtQfuPopI/KR1GyogIIHHJtZNxpviFMXVRahfN8VJbS4304IFXfIDi+vluAvX
PgfLP7oWatHKy7lfaQz79g+nQQgblw5FrKEFJsrV1GI5ZoSUWZJWq7l5yvRcAL+G
JRm2f53xQ3/iQVM0NPIURWsihcuwnRmgq1CbSIUqhHBohLbNxQlMLuCXbmWRphpW
Y1sdm9YlmgTg4GlsXnMTJWya5zaSN++LIy9C/tFO2DLty4On5hcSF+NVktGmS67j
UksrfudORUDE2qlfI29zq+AV4GCf4+Vwn2CD8KY26ejGMerHrr4IoknIfrxvMrUr
hFnwohjMbI7qgj7AHxCndTll3opkExZN6CJ+iqGEgGiOzZP3U9FV+YAfiUav4bRK
Ph4mLkLGrNgfqJyKX7b90WGdyTjynt43y1qH/vkhNb7S9tWKA0cJDB41r/RtU9mX
Y0vQPPiCX3RGCNgs6/vdHZ9CfVGLlwtUzcrdDlwUwIL1rtxXWnh7JRhMWwdheudq
NNEx9JKT6ZFlVHJB92JVEd32ooNDHcSyoLHGmX4+19a6I9yIm8ro5R5FsGZryilm
oBfaXuFI2Ykk5rUsPbrcIr3uVCSetd/iGYJAZyn0duO07jU9iUN0gNcXOZmWvwvE
57NnrBq0kMBEcbcwhQyKckE/9HchjpBDyBYjr8bN3CUZxd+AetrQPx/qbiRXl4eH
vataxKJcHq0B0i3QnMny1h6CsAac5ru8XfsSzfLIxQhmGH5tUVVb6Uw0zGjhUWdT
q9tsYv/jN9p/JNsBx+zV5C2fctRq0mhGBXPZFcfUN9X6AwxXVPnMqVCLKCJTdX3j
w/Iq2NxY4TupGMsFh0P0BWD6h3SFs1Nm7sc0+quPp27Gq8LNoMFvt4w+Gc/VsSSj
RhYaEy9Tmz2QmiLE242Qihu5+g7ivWnT2Ti4NnEpMrbuF0zqqmc6i6rYi46Ba6ss
bXc5y+8hOeRaF0vBm9/9VVCe3SxtXFqzEf0VYOzpWehaytmfl+QK2kvwojl8Ddjj
buEIJGJ8y0+TFWr52dFA8iCNVtw47W4YbtYPr79CB8VyJEQxqScrGeEjzfTvwK+I
NhQZKAzcEL/SFVOoQWgjhJ7Ig3/iMVYXumGi/KxcvTympg8O6kAUSuSWIofQz6u8
xf1qZEfQ/6TxG5zrK+pw7BCzYJja5w8IvshrMMxNQs1TlSRNE02nzOxE2AvBSDZ0
eoaEhPWDB2dP528cVN3xC+N7r3NnICN4HICwp6QP3Fn4B9le72i8ut6RNRt1+Szc
n+uK/+SJiZ2RYfK06kuYni/W33trFwYg8aPKHze/vvR5Z3PRzEg6JOtWWUmnZJt5
d+aJ76a/0KyKqTA+x8aSRz6xw52K6qEZmnEg++QQbhfiaIb5x6PQPTsQe1lpoQ/P
URiMC8XcJuNA4Nv0frWv/JeWQ1634g8uSj2Md1275994cGZFVDsEsUvRAx+TBMxJ
trgQ46V1atTfKkBFGc3ny/cnv7IjH/94pCQbgegaOp1kJre56AqwXRE/Sp/SLVwR
KZPB7hHV8TXR4PS2o0ku4YlmfGO849onBRM7PyC48JjBlx459Img4cnyr2E6IwGS
jQVW2hggTHmJvQqTAYhYjuRAqq3eCktIG2QQgMvZO/XzNZLZhkH/Jo1vSXjPiuVR
Af+3cCqjIza2JtI2/RigoJVNNVzKlmVxZdGHbjcINFL4yzpxOAKN9BN8+OxSkIjn
VYjBayEHeIPWzZ0pyDLs9S/b7aTLBoDI4JVLD532JlYVqjk0sFE7GxG9oXGLM5bB
5gjaIg829PJsUWFkEt2WnAzSxhdbciko2vR4HMi4Q5ka2cZ+qZZmA6FIEbahHrjf
0vjaSLekpMC97WnYIGHs0Hm/xtIgUcpNsiNWV5evweZhQt7kdjbHm9wGRQYApFW3
rFjI6jwUs/Pqoef+Py9aky2xUNKbB5v5+obb6c187PtFnJ8F/+wESGLznSwf8u2W
Rpqk6g5NRQyBTvdrYN9HJJp9vyUIEG/r36hmOSHLI6K9VTrqii45j4VLI5t/AQj6
5VnPxzP/9aHkIwUr7ONIvjpZVN+VWUW1gR7e52Dbo19XrjB7O3Uy1A9WCTAqd4g5
iUXzosm497feEPkButuw9XdC63UIqLeTPzcNJtuTvurDkSlQS0zm80hur4YCH9ym
qMSgvB8oN7X8WZv9mcf3Qv5VTyAuPWI1YYcQ81ZrzebSu2JfrMZoFUlhogLS4UV2
k5Tkbr4LEirNXRlSOgWrLmPsk9DG+DRMA3mBShUUsWsNJqgs2r2nz4/KKNma86r4
sAgeAaAGwJQ+MceXIvdFhbkmZbbisDydElmhU3L8A4sFjcjynA5JcpREYXf/iAzm
WqWwqEoKe0FsVKTl7qLcZKRK5uhKp4oIYwX5j/e5a+MZn03okxxP0wLKCf1mScv0
HeB7d9J4vISBIezaM2iG5QRx20v3U1fXnqgH4LveN0E+GOTItr1WvYgITt2h/423
PJX+qQyNuyC1h3R0/v1kXDsOKxAgM23jEPlFXb6lgehzAS6TXzDqeNqpX6269GR7
LiU+CjO4KfzDduH374kNkllIS449tSVpeFlsjeeZQBJbS9Wd9dE6vtuh/fAOs9o0
o4KI/kvd0Tew0FE7op9iiTUT2wreyWYC1ajIo34akTRGXyKhoSOLHA/iltUTq2Cc
DwG4D7nclcnrmkcYIRVXpIdUweKCRxwBq763KzLroxMhOWgiP148lv6ulk5AT+Qq
OgcL++NcmW5mfiOHzfbSmlcew4jzZJbd9A7Qja8ACOHSP3glSw3lv7jRlFnOmbtN
eDQe0ns/Hth/94mH2gf2nW3x5w+T2axM0CWExr3JMUkIyljac7a2dr9ZMv3A8OvT
gXN8tX7xn0EPZu8XWOzu6Lhvt/1TXBFCKHbvaDSgcps=
`pragma protect end_protected
