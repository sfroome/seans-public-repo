// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H-8Q(PT674V3W77&QB*UHK!L>58C-A,YW,#*8/[KC<):DMR W[2FR8@  
H20(T/8K*PKJ'[ 0XNWZ=]M8C82BL$+ZPW.&T5Y2695INO/B[A/IMWP  
HN\_O_ESAV#F0L.X\##S^JK*F#YTJZ:^@]Z%NJ[C)KV /\/>DSSID7P  
HU:D3(E+O6_+=69I$>S#/3X"?P<0IS9D!%]HZFDPU3D&/FB;C:D';TP  
H$/PS+_##7Q_?T='1I50M27'[^2;H:)@#,_(QAU3L"*O>:L"V$"/B[@  
`pragma protect encoding=(enctype="uuencode",bytes=448         )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@XP]IHA9#.% PK$; (4?'("ZW?276$7?/(ZMWQF .%C\ 
@:''(N2KKO,19:*QH%[U#D5OAWO5]8-5@R_:_#$;'&N( 
@2E^S9W"1[E$7 G5MQ)B4P57!B-F;3(BO4YSAU3P0_2< 
@^ #</X/5DYQN"0]R"[#<BROVZ'R."6PH;R2D;]=D'$D 
@)B+$B/E))I(IL&/A>V"'+']%0P.W%RUUE](]\)KX^2\ 
@ZA/.T"0#Z$P#A8-7#4G$PB$)'9+FO[L]?9D.,0SNOP( 
@5ZHEIR'(+Q/U.FB68\XLI(ZE5@[(>+M-1):S4%C,Q*, 
@MJ>01F!,KGX/7"O0#-(=)[A\XF0B3J)GF.8)93G3'&X 
@#CP/9X>_;,Z8S!G)!G]R+I5!7N])[ZV(I+OT])[^\2\ 
@ L'5M;YX;J\%EE.FM=[E4W#7V,;8J 4NKA\6.P$OOF  
@=]+2JZU%L^@_JNH+,HV_%>9>F!N[P/4F,4@U]):P+%T 
@];_ ?MQ#Z?)VYPJ;Z7R"_Z4R?5/7&^DI=8=I("C^<S8 
@K/5Y/YD?Z-F=07+[IOBX;=3C.7G)2[*+KG>)(UA;\L@ 
05S#UE8H#"Z;<LJ(!Z9.XC0  
0U7CP[?6KM@HN#K>T!5VXT@  
`pragma protect end_protected
