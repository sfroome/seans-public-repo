// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HNF TG%-/K$JH&_]2XTGVDSK6>]ZHM5<H*AK6\M)WHZ/Q,!0-/N?M@P  
H?D*':&9$':E\N=PZC2+]6E<-J]/AZ5;S!#<QUR\DRP6#E1V76I+. P  
H?21Q4LXH*HC\-,V(WSLU'/@JGM\/#X@Y7\2?,V\.EX?7Y1+M(B_!G@  
H"Z"]$7WXNHHF."[GO=B3: 5P=;FX^:U_@&!RT(NZ-B7PF5V I[A)H0  
H5_$< P!^EM[Y()^;G'2=IV]?^D1^CYT71 FWH;X^:UW?D'2QE]B9D0  
`pragma protect encoding=(enctype="uuencode",bytes=800         )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@8*)44[)BM0! \1#,\9+;$UJLN=;.]A[GH<EA>JK4<;< 
@2OI,0I$XB4+*]Q=>*N;V>-B/:)9'(]J'-WQ CP3$4+T 
@L"!4NP%V.BNWD8ACPDRC"DZ8/$*(MK71[13^*G,3'/T 
@ZJW;F3HVY'+!  NO=U5"?IP*EY3ET.6>4\ 5W@%A-^@ 
@+R=M6OCOH-[#!?- !)2VWN"F"8 Q[S3&X3GN2^P-_^$ 
@FO4QOVY&SCQ?KO+>=FXBC]JVZ0 E/3VZ_Q/10,\*87L 
@V<BP(G[!IWFPZS'WU8:\FEQ",9R\F5(:O59\##T6S$P 
@'XZ:A[ 9AAG=RKN*.N-:^9.\9QFOC7,O'X.N";ZHCRT 
@LE[^/$6X<R=T8M2#.2['3JR!6,,7'9C8I<6-2]1XHD  
@A!9BP+:$,2'&*;_H!XJWJ8T[,!T7:;R\^^=V[0> &<\ 
@PMHKHBTT)]8BMG,-JMBUXUKT.XY,]51#*;4;Y\ZZSUH 
@ V3XZ\JCI,4&C5XHD#\3V)NO>O6<A(PY8L@#%.AV*UX 
@XUD^2.7GB%,CQ0;*L:99\8N@GQ>1B7TS"]4@J8@6"', 
@_5-)OH"/Q-6F=$^='T=VC(0+SL1G6RK:[?TR,*)><>< 
@,B)U_18=QV=,P,;W^3)@F5C4,#.5^=SX.LJ+F]19>@$ 
@F'>HQ/"85QT3=,07[6[$AI(=N=+.&+K%P;I#(7G4YV, 
@0:GIVT=L5#M8OF]J77D^Q?;/,3!TC+L'U5'%09B'9;  
@A3[.XF/%;G*7W D7C74EI!:6. L^7L=\'A$3H':'-/L 
@$D)J+H U7NB1T]KXH,D@OK'O?$;#_6QLM/_T^\MAJAX 
@ESSD4G26VZ%VSE4Q!>LQ94]:3$;\K]^U:/[RZ.S<1/L 
@^1=D[F-'>S"<68&I9V^0. 99\*;B!QK@5 G.FOG(DNL 
@\IRC9?H!H)[["+U_N8[5GF.+ Z!SDL#XVM@KK"S5,0( 
@46=XX(BZ#X_-UX^*<2^3PTO)M1(;Q9+U^V=;8OU74VP 
@1#,RREZ[]RIW_ZK2FL;D'3EN[YK]Z9NN&IB*7#QB41@ 
00LCI"V+9%=9JFZO]K!-Q^P  
0_9(<T>\-6ME^W;,_^"&;:P  
`pragma protect end_protected
