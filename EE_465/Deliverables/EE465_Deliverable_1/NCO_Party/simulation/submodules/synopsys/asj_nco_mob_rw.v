// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HK:U0E"Y,._COHHN-$Y/[M^)UF=)XR-U#Z7JM(NL%UJ"I2_5<,TLH P  
H/^7972ZJ(1I&4-<V,L0:[6G>]LR"9$0P.2O?>Q4-']#Z!O@]P0(7VP  
H.0:OYAG\NAI4&WQWAWX;;#4U$7V*WE!%]AKJ+Y2=6FJBBI'<19VX&   
HD"%S!&^,PD^%I2??4"X<E.5O<Z>)4ZY?(& \=B>::85O&(2XF;*74   
HM !,!9^J*J2B=DX;X$M,_56ZSHG2!INDH!6.[SH'B_6Z!V2$3BEN;P  
`pragma protect encoding=(enctype="uuencode",bytes=688         )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@.\Z@3%]B>CC%2\=0.D?(B!K[JD0OQY7QC9B,04VK8VL 
@")4X"Z]$;H//!F1'EHST .4H=<!^Q7@P PUS3Q:DYWH 
@U(!/('/3CW:"DX/(>AMH:E=E&4\,:PDUHX3 7J92C^$ 
@D_8VY%JVI778Y(9(5R'<%Z_#Z9BJ0?:;LCJQG@_2&]X 
@:] -\C&6]D[Q! E42II5325G'.3GJC<P?JOWPV;Q5'$ 
@:J$2IXXH+)Y);!3'^SO.Y>  /[!Q9""]:C;INT0CP_  
@)6[+A;P"-&NTC=UT:_F @37_R>L/0 3?_U$!K9%S'%@ 
@-/13]SWN91'X'5?Z5B^72D4%YA@H!^0B!?&^".%66G\ 
@8>\E ;A]V#H7L]O=X'+!Q/.$_@P;" E%*MRO]6B)8<X 
@,=@]HTJXXO3Z_T#75$V/?1V]#KU.=56->-;Y\G;,CDH 
@@67E- ;60Y G$#T$!X0@@#DEIF^5>Q5R.Z=SL2J]#], 
@#5>76&RNPN/=)V$)$S+^>9II)XF%]"<<PJP +\FWKIL 
@TGA?=64Z.  CV3L/^<(/(RLN/SK<[H6AS.4EA4CE@G$ 
@GT65VU3_L7_QO8B+<%/_RSPXJ$E1;FX39"RQ";CWQ6\ 
@&[4EDB=&$_H7PU]C)^T\[T+(,7T NQ>6%;/03J/AR]H 
@;W2"PN0_P73**K]";'SU?847UK[P8@,7("TU[:T;3GL 
@PQJ-+5M,?^!5F&AQ[\P]'\@L9!EUP\'$JH1@EQA-GEP 
@-M*RB!JG$$E]JC0RK(M7#LI$TZX=>#^?0-OP5&&%4<X 
@^+MY$QB"IYZRFAG_#;"7L5D]D60X0YLH5IK[*:M*=LX 
@\2F->Z:/IJ2RE-NW>PCI#C)^PL'&%9YT/<:AUHIV:QT 
@&)Q=^?O\9Y>)K2&!\<FQ@ 5B+.W\NVCPJJTQPQ\<11$ 
0S=2_A^LR#V#%T1Z8Q G'@P  
`pragma protect end_protected
