// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H?^294S+WX$5*-8MYK("$@)=B--#?_.EUAU46G@610X8Y*@KJ+==\-0  
H\NP@DD_J0""!8OU.+IR[M[4;9"/A\TQ@*50W[[]='13/M-WA@)'KZ   
H0:FV194CR%?GF>P09UY*ZYSUI1"(F0\79AVS3W"@8 S<AQ7#=^.Y7@  
H <>(]DHMO><=H#T;KE0II =I!D.8PXH/AK&"0?*$95^)8KQ'^ZG=L@  
H/P[KA7@.S'']$0U(A8@.GF^VNXPN]5487D^. Q3V?U4?0S/FV8C55P  
`pragma protect encoding=(enctype="uuencode",bytes=1008        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@6(*)3K?UGQ/6<JK[X5:@%/FM4!RG@[E0+QG>G29T\@< 
@ASRR%F5W'S"/,6/QZ[:3#MML66WC2?]:AT:<P_1ZCJ< 
@&$2<C4]O>'RK3\S!.KS &^E._0UU1J>JC;ASH[UN/*0 
@]3X "S1<)*6J[E\0ME1/.36_*\&>\JXK M@/2:XW<@0 
@^[BQMG8<W5VYI@_J8P8;W-D-7=Z'2HWX^U98:&CXH-( 
@C<<13/?O]0.=J^R0M9RT]-FST0RN3<UW\E8S!U(T1G, 
@(HY?'\Y(??@AFH#9=?$@))_2#B]6Y(N3%7))TUKP'AX 
@EHK(>@;LKA!'XP"GN^6FJXD KP^F;'"@:.H&[Y7+)>4 
@P,8VQ@>-=V2<@K)P1K&/IX0'Z65E*9E\Y]9#2I834]T 
@!"19>X(Y&R2]!1]NE6PT'W':=/C(#GVE[]^%X(%(XIX 
@-4GDY72\;B/KLAYHA"]G]/VO#8H$G1*NTO-2SS=<MBD 
@/8S1&JG9?Z"4O##8F\L<P2I:BRQ"'40+97L_+6[TVLX 
@BLL^\6K(RP5LY".ECHJR5'ASKYOKCU67*OXUM<S)-A( 
@5Y-S/_;KH\:-,$L!>\WA9.4SFXZ(G2[]GMA?81=.G6< 
@+&:30<DW,L\GYA@Y&W]!YW;X98?$T]N* !NRV4W1;3X 
@0A;7<I5*MLQ)Q#&.[.IQ43%V]JN"UXD('.!XXE5>89H 
@W+9J0T981_-^PHV[/ YT_$W5F=13VW&KZ:D($RT'KP0 
@,E3;'X9<$VS9,SS2:126HS'Z'+_I&_PP1.CLQ@F_V.P 
@(.W^C)P3Z?Y;2)-N&@NUVU;3W5LH&[.G3TGZQOZAC1$ 
@1[B$:POC,(XNJJ+N0#JZJ!*$ &/!F?4+W%L^K&"<2^H 
@%UA_QF?4G^[:?1?XL)GYD JC B*ZS8S!%IU%DG O!6, 
@.R_%@/>BZE6_3 P+<:2D-PR*CX56\?\2DE!9W4^EYY< 
@C,J1RSBEW@KF:4XFIGSR.VT@.O%K]8%)[Z="\K'<$Y< 
@MU7HB4RT!D[7_R&4GEK,%@$O%QA>%MNMU(@'26CJMC8 
@TVLP6MF-F,=Q55RF(#*^%!<6(K8=Y\V@VNU@L2,-+_H 
@&3RY,U$66W&?0/RKAS-]*990>/"A$D'CE$A0!TK9E<( 
@1#M8-=6:?5#E]L.VAO%* 1!5_^+;90=HR\AV2)S40L4 
@8M7U\?^M0-N[?W5!E"LRTU?_=0-R9M)L;U(BWK(.E6H 
@.U<OW;/PHU8CC!5Y0H=LSKOD)5L'\J),;PN%%J>S=RT 
@--M-7DMAW/C\"BN4BR?]-GX4H%;^E_)[?Q/CBO%CA38 
@6-5E"L_/YJ;VK@G\QMPES$0")6Y3?!@+*/XYCVD+:X\ 
0ZWPJ:MXF!R6E@ B9+J*H[0  
`pragma protect end_protected
