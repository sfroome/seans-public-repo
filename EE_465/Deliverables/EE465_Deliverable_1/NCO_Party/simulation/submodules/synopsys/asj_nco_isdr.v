// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HMJ#T_HV8=U7*7)V!^<RN5Z5,L"X<P"D+H.]IKN4-N\^'I2H$HN1)Y0  
H]"0H=/5\6H)KY^*?S@N]D"WOMYMYIHW,8+]2:=0T@8JRRQ*@(\N7R0  
H5NZ#%V\.-NL&<%T6[H.%>:OOS_O@D66^[4P8'@O+X:GW5EE@WZ9K9   
H&*^^42R\K4.@5K*RV3.EC6+,<((T!8[KH5&;@2QKJ"R\Y F$P;*78   
H&YB(F$!2USU)3R4<?86$\]_OM+77:OF+$ D<"R/U)MVW[(1!B(]H4   
`pragma protect encoding=(enctype="uuencode",bytes=624         )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@ 9AA,,K^I_OW_LR8.E<86S@^)P%?>JAH-BGEZT0NX,8 
@\X/K]&$GCYJM[?;0^:X5U.,*1S68N0'2%5>]AXTE4"@ 
@IP[;(MY%E:#IZW#,WD*VXHJPI9'FZF'VR#L) 6!\D2P 
@U!Z87PW>)X&9HCW_?B9<KOV6_#^D$0VJC&2,U!WX?MH 
@B;9%P"^#C_%#?6+>$M]+?LY:%=K "(G7":?J-[@PQU@ 
@9JD[PW%CL'V:;;'"!Q UH?4K-:"^@(?!J9B?@L? K9L 
@8%XW&W%%%(Q11$3J]!*K%>)'4Q5L.(<6W70^;CQI830 
@EB7$UX>@ J'[1.Y(Y1(."Z\V[8@0ZH(=BRZBD!]HJ;T 
@E<J>.M>-=9""^%%Z?_O&$$_6J7!YK_FI)\G___+%0?( 
@6$*BS[6,\6^J1EC%,T7PFG-M.+S 5+0.FW%';!"[60$ 
@C4 M>_W6&ALN:+LO\>VI(^0IZ=59$^:H ?$\N*"<7M0 
@9B60W$T\>C#<^?0-K..SZV@(1;BB''@ GOA.*(WJW7( 
@_YM@;'734J".'A* ZUV=N*Y=(A*EE].W-^K$6"0_#Z$ 
@A/&UX&"W"8-$!@;ZS\K5H;[C&!-,M)%<NRJ"*7.K]/D 
@HJ268[C!I_9UUN4$,G?XN7OJ 8BZ^KUX#./I^0W*X"@ 
@+)$5RSQC@1'NO%N.5N: ?]^@I]"/&K@^!VG(F-7QM!$ 
@*9AX)P%;YJ@MVE?(ZJ#-Y9TOT(CI$[QNK>6<6K>G"ZT 
@UPB&#LWV+T$5)[(E]\]_.W<9:#.679> M9GHI-9HN6@ 
@/[WX__XT=(,N,I'Q;Z#-&]FA(BA%T<(M0*)C")[W9.D 
0/=]>)\]2%Z!:\S>'9E];:0  
`pragma protect end_protected
