// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent= "Aldec protectip", encrypt_agent_info= "Riviera-PRO 2015.06.92"
`pragma protect data_method= "aes128-cbc"
`pragma protect key_keyowner= "Aldec", key_keyname= "ALDEC15_001", key_method= "rsa"
`pragma protect key_block encoding= (enctype="base64", line_length= 76, bytes= 256)
gmzp12+VRvEhPpnBJ64udasqnNEZEQkwQfDdhv6lxfnbsuyDVVTHwglhtCBek7lveCTwivT+6ImM
Bufn4xaxBxH9zdX7ta+YvJzDV8WmRGLfly1qrhIfRyKlOHwF5YBmVNTiE+DJ+9fcfqSCt9cRBaDE
NLVAB2DTW73uRTS/sxqXYk2tDpWTb7OTnE8APMMBoh6DdZQbA5I6BJ60q7o+liWsdExoCuqgw96T
T2XeSJh2/9KEcM9X2vUWX5Sp1MYkddCxHZoyxxnYcb0DFd99kc5h3yi2hgdlbV/DQ8iosMxytXc/
IiIezS4lNLD/yXvblQFY0Vflciloeual9neqiQ==
`pragma protect data_keyowner= "altera", data_keyname= "altera"
`pragma protect data_block encoding= (enctype="base64", line_length= 76, bytes= 2288)
oBaz7ZIiTVLhyOPA+RCSsHm8FS41tM8Dbywt+TfsZ214V4qbHCQg1buVRALvcAa0JsOlycEP8HIr
CzZULY1JmGJEQnWrFg1EYYq16rVLlLDfo65Ee33q6Cn4ndPzE1dS74zqOLlbKg/pqk+g/kTd5fUE
8RaIEeJButK7I3wFfM+EcS7YSTLLOrUsjvCUwYSPeoDaHotpJq9sHbmfANu+mQ5qBxPv40i8qvSh
3ycEeT77v6HmAiszM6MkbBD/Aq9u6S7GOsMAkSBM0J69cpcu2MLIWe17iaikmSbkyyJpvjH2pLtj
6q0/yhiIMUpttHz/hB5Bw8JpsUuYhNF2e782jmnoRPl763cx/EgBHfPci+kWvtmEQvGX8bIf+Bvo
qllvHyV5qLQJvuzHApm4OES5tINpCawYqJSpX//z+vPbdgE1C3KZgQNBFmUQ4d5tdqL2J0/KD4u+
wt0OT6wh9UpPn5TTcCtiJwqVTwF+uhFPomzBv4+eVjrEjpC9mo8EvNIyP1Kb2/Upw5VJ5qs0ZVmu
9bNxaeKXUgHpmP3SSnfmtkDhbis2xkrHLhUAmTq0PGEiS5SDJeCqj1/GI5YFcmiDa5y/2v+9Xdba
ZNJL/w/rUqetMaADo/COOactlr9+Nio01phzOZDeSDvS7+asMedmVxrdmD60DT8sbonBS9auBMBg
WfxO25vIJJmv0v+wzynvlbzeytmTq6DPQKKlMyhdTr/3L+KLyysGf38ERlMkHjA5J6dx1zGWeIif
/dcTgcEXs1qZTCgIlHpvIfH3Mz3seV9yaeeTBT+oF+Z99m1EG2quncNM3vX66k+0218HoaZ/L2Up
lKXNHLCNH6QXFfkXFjLcXmexWtCz48SGmwl7wixg7AJlaTv8sZiAFoRf5yyYodLFb1+JXkzJixVZ
6Leaw/gBFjQkV/thqFlOlzFYls+/EmIzR7C3fvNKDt/wmchXH+0Kp+A9XKujm4pBDL5iJpY8UC37
aUnd9KhwEcK/C0Jh/veqVCdSj6NyT5fHXqPeMXNIz9FtTZG4S9xb76fLnUWYJNipCNWYq2Oc2HQU
bVi8J1GT6/wc1tARjZ08n4gsTTnqEBguJVDxtjIMpnddaz/1wvT/2id93193LS/qx3i4kP3knfH3
Y5Kj5Ic8NDh2pL9dBB46rDGjnABnZtiqOEw3XcwxwDvW2tQL897RVn2KQYOb/Ct6Gi6tJeu83G4P
h7h7PSLOU9a4+641kSyFuLYcupGWCG7/2FcX6By3w527AUzV3dyr/CulfSPW0JdE1U3fL6GChCcY
siQHDf8FBXrThDJ05AaMOgnlbo6EcBJmTpgqsfj2Vl+ArnXfPWWBec3iCIoH6sbv5cCsxqQL8Nzg
0S52lqxc1j7PYShn3TQJ18gWMz/0yS30xxkFL9XZpEMHGqBhwjUgFilPZoL8kzfn0Rid0HkemoL6
ViQFoa4TgMfdOtTajbpE/+k8t52vsNz8iVq/N96oj8kd3sBYq0Kjg37gfFyvW0rxv0MBg48DnAo1
c2TVOSdp5HbHEU7mp8dWFnOn3UClJ1K0mGmZkEcvM+JUogQyB3eYORSkwu9jND1KvLT3oGARKyQ7
jFUcUk/4LaPga9rvQeI5Rfn1jC/aDuR792eVDyAekmxjofGsY6/ptC06SC5JapLiq3F/ESQgb5g1
ZRGXTOjtKNNazr88TrAs6ZxmxYgzjTC1uajt95a//2743edd4LbXYFmawmAEks987deQCi/P0K1B
ocDMSYUIryjk9JQzRgLldhnAwAVgW7CaMqtAQvluIg7Fw5s84y77yY6lwjKQJKNekbKOSuLnlluh
bJfv1UBGYN9VsQFV0Vc7M2bjlosGAeKg4iVzmURth5djPh45A2doAStVMj0WwPPBRAlXfBf8eWxY
7INL6BkNJTZF0qDZxlQ3+PRUFzxrwv79aXGHgwmURfjGZeMF7U0jHFuWZgkCPqVbX9ia5oQR9wYd
M+bv0LPlgWTmdQm3GK2Y9ddDA+jlFVy9RtcKgUjrs5J2qR0JgPhMDP7ibIkGTawu6ZDsIFks2LFM
mtEM3OvJ4cZBW8Lyim2Oqj8FSNPUbEmWGOuyjhK9KlV3xdC2Rsm8JOutnNO/rr4kFycraQai2+iM
kGAcrkvYADN8bphJJymcTQCd/0Q1uRjQoQriPZUFhUDdx6lSperGsmHq/FTdBSs/QwNBe5POCYY8
8XjN0Qc5cJZ8m3CjvdgKS7s61QiEhJOKBxwfIv0hnlUzaFJaa4j8JQ5vJFjM2n97qNvbZIrPL1+w
eXjst7yxX90TdMvhUFlQDQafl66UAg+zJkZg1oRbEMVuhHPdQYESa9XZIj0M3T2CE+iHQmFqWsAw
RyNb/eYKWXEaZxIqisvTW7D7oxdDKdBaQL4hZwhxsrIafv41+6nMLlC+2lVZMwtm6Xn0eJCzQeHZ
Opfpnnv6U+3sW2lobPnupOltSdK4E/TMsQd8v0yCDjGUX1smRiTTPyT1g7vhy/AfLFPzYMR3NgFB
A9/1sdVljnbGm7/XMXYWr5PuDCEz0aNbfc3UQe3fpUS0Nw4FxbQxfmKe98CibYs6BGhEpnqHplWm
1iXbjNzrkG+GJG7EN6g0txMJLY2uVmTA4W0Th7P6kijt8RcO3Fmi4Vk6MAEw4KcT568pxGeGY2D6
OTKRLRYaD+KPHmbw/jOmG56PRvxa5rXdImd2kCZhh/vNs5nTilIi9JmzAqM9Tw6OvUSGvJy5cPjM
92BOTAMq5CPLKqez9UiEFDMhVn3YJjwGSVS1cEGhn1v24iCC650ei0SmDIvK8eP4YXbin1642xCF
j+NQF9uGe7VK1hb8DPNP2CQpMc6UdPcdZMPU6CN1jdAGnHznLCP9Bd8CuNKdLyQVcfIw8ZPX90aN
Xua4Ogp76ZVjPqCikdeEdX+QhCMx/oTGfgbjiVHSls6zDTk8Uukhzsdrop0J3sP/ud/JV3qxMSgS
u79EBmoBgXomt1mSUfIhaxMnGE/0wTOVw34McdNOniaNX/g5+r+R1IB0Xtu7ut5Rd8lkpyEGAyo8
nLyMxin0MWM=
`pragma protect end_protected
