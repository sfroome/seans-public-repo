/********************************************************
EE 456 Lab 5 LFSR
Used for EE 465 Deliverable 1
********************************************************/
`default_nettype none
module LFSR(
				input wire clk,
				input load_data,
				input wire signed [17:0] x_in,
				output wire[17:0]q);

//	reg [15:0]q_int;
		reg [17:0]q_int;
		always@ (posedge clk)
		begin
			if (load_data == 1'b1)
//				q_int <= {16{1'b1}};
				q_int <= x_in; // Wait. This actually does absolutely nothing since
												// the key press is actually a negative reset....
												// so it only triggers on a low not a high.
												// Either way you're not driving this with a sinusoid.
			else
				begin
					q_int <= {q_int[14:0], (q_int[1] ^ (q_int[2] ^ (q_int[4] ^ q_int[15])))};
				end
		end

	assign q = q_int[17:0];
endmodule
`default_nettype wire
