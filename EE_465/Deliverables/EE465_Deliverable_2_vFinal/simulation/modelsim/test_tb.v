/**********************************************************************
EE465 Deliverable 2 Testbench.
Sean Froome
**********************************************************************/
//`timescale 1 ns / 100 ps
module test_tb;
reg CLOCK_50;
reg sys_clk;
wire sym_clk_enable, sam_clk_enable;

localparam PERIOD = 20;
localparam RESET_DELAY = 2;
localparam RESET_LENGTH = 10;

wire [17:0] input_mapper_out;  // ie: the input to the DUT.
wire [17:0] reference_level;
wire [17:0] error_from_dv;
wire [17:0] error_from_DUT;
wire [17:0] errorless_decision_variable;
wire [17:0] accumulated_squared_error;
wire [17:0] accumulated_error;
wire [17:0] mapper_out;
wire [17:0] mapper_out_power;
wire [1:0] slicer_out;
wire sym_correct, sym_error;

reg [17:0] decision_variable;
reg clear_accumulator;
//reg [21:0] counter;
reg [22:0] counter;

wire [17:0] isi_power;
//assign isi_power = 18'sd9268; // isi_power = 20dB
assign isi_power = 18'sd293;
//initial
//#32000 $stop;

/**********************************************************
 GENERATE CLOCKS
**********************************************************/
initial
  begin
    CLOCK_50 = 0;
    forever
      begin
        #(PERIOD/4);
        CLOCK_50 = ~CLOCK_50;
      end
  end

initial begin
  sys_clk = 0;
  forever
    begin
      #(PERIOD/2)
      sys_clk = ~sys_clk;
    end
end

/**********************************************************
  RESET GENERATION
**********************************************************/
reg reset;
initial
  begin
    reset = 0;
    #(RESET_DELAY);
    reset = 1;
    #(RESET_LENGTH);
    #(RESET_LENGTH);
    #(RESET_LENGTH);
    #(RESET_LENGTH);
    reset = 0;
  end
/**********************************************************
Test Conditions
**********************************************************/
wire [1:0] q_out;
wire [21:0] q;

/**********************************************************
  Clock setup test
**********************************************************/
Clock_Setup Clocks(
    .reset(reset),
    .sam_clk_enable(sam_clk_enable),
    .sym_clk_enable(sym_clk_enable),
    .sys_clk(sys_clk));
/**********************************************************
  LFSR Test
**********************************************************/
reg LFSR_SET;
  initial begin
    LFSR_SET = 1'b1;
    #(RESET_LENGTH);
    #(RESET_LENGTH);
    #(RESET_LENGTH);
    #(RESET_LENGTH);
    #(RESET_LENGTH);
    #(RESET_LENGTH);
    LFSR_SET = 1'b0;
    end

LFSR LFSR_TEST(
  .sys_clk(sys_clk),
  .sam_clk_enable(sam_clk_enable),
  .LFSR_SET(LFSR_SET),
  .reset(reset),
  .q(q),
	.q_out(q_out));

/**********************************************************
  END OF LFSR TEST
***********************************************************
  Counter and Accumulator Conditions
**********************************************************/
always  @ (posedge sys_clk)
  if(reset == 1'b1)
    counter = 23'd0;
//else if ((sym_clk_enable == 1'b1)  &&(counter == 22'd1048576))//&& counter == 22'd1024)
else if ((sym_clk_enable == 1'b1)  &&(counter == 23'd4194304))
    counter = 23'd0;
  else if(sym_clk_enable == 1'b1)
  	counter = counter + 23'd1;
  else
    counter = counter;

always @ *
  if(reset)
    clear_accumulator = 1'b1;
  else if(counter == 23'd4194304)//(counter == 22'd1024)
  	clear_accumulator = 1'b1;
  else
  	clear_accumulator = 1'b0;

/**********************************************************
END of Counter and Accumulator Conditions
**********************************************************/

Input_Mapper Input_Mapper_Test_I(
  .q_out(q_out),
  .input_mapper_out(input_mapper_out));

DUT_for_MER_measurement DUT_TEST_I(
		.clk(sys_clk),
		.clk_en(sym_clk_enable),
		.reset(~reset),
		.isi_power(isi_power),
		.in_data(input_mapper_out),
		.decision_variable(decision_variable),
		.errorless_decision_variable(errorless_decision_variable),
		.error(error_from_DUT));

reference_level_chunk_modelsim reference_level_test(
    .reset(reset),
		//.decision_variable(decision_variable),
    .decision_variable(decision_variable),
		.clear_accumulator(clear_accumulator),
		.sym_clk_enable(sym_clk_enable),
    .sys_clk(sys_clk),
		.reference_level(reference_level),
		.mapper_out_power(mapper_out_power));

accumulated_squared_error_chunk_modelsim Squared_Error_Test(
    .reset(reset),
  	.error(error_from_dv),
  	.clear_accumulator(clear_accumulator),
  	.sym_clk_enable(sym_clk_enable),
    .sys_clk(sys_clk),
  	.accumulated_squared_error(accumulated_squared_error));

accumulated_error_chunk_modelsim Accum_Error_Test(
    .reset(reset),
		.sym_clk_enable(sym_clk_enable),
    .sys_clk(sys_clk),
		.clear_accumulator(clear_accumulator),
    .error(error_from_dv),
		.accumulated_error(accumulated_error));

slicer slice_Test(
	 .reference_level(reference_level),
	 .decision_variable(decision_variable),
	 .slicer_out(slicer_out));

Output_Mapper map_Test (
	.slicer_out(slicer_out),
	.reference_level(reference_level),
	.mapper_out(mapper_out));

error_chunk error_circuit_test(
  .reset(reset),
  .sym_clk_enable(sym_clk_enable),
  .sys_clk(sys_clk),
  .decision_variable(decision_variable),
  .mapper_out(mapper_out),
  .slicer_out(slicer_out),
  .q_out(q_out),
	.error_from_dv(error_from_dv),
  .sym_correct(sym_correct),
  .sym_error(sym_error));

endmodule
