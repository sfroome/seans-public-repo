onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /test_tb/Clocks/reset
add wave -noupdate -radix decimal /test_tb/CLOCK_50
add wave -noupdate /test_tb/Clocks/sys_clk
add wave -noupdate -radix decimal /test_tb/q_out
add wave -noupdate -radix decimal /test_tb/q
add wave -noupdate -radix decimal /test_tb/reset
add wave -noupdate -radix decimal /test_tb/sym_clk_enable
add wave -noupdate -radix decimal /test_tb/sam_clk_enable
add wave -noupdate -radix decimal /test_tb/LFSR_SET
add wave -noupdate -radix decimal /test_tb/clear_accumulator
add wave -noupdate -radix decimal /test_tb/counter
add wave -noupdate -radix decimal /test_tb/input_mapper_out
add wave -noupdate -radix decimal /test_tb/reference_level
add wave -noupdate -radix decimal /test_tb/isi_power
add wave -noupdate -radix decimal /test_tb/decision_variable
add wave -noupdate -radix decimal /test_tb/errorless_decision_variable
add wave -noupdate -radix decimal -childformat {{{/test_tb/mapper_out_power[17]} -radix decimal} {{/test_tb/mapper_out_power[16]} -radix decimal} {{/test_tb/mapper_out_power[15]} -radix decimal} {{/test_tb/mapper_out_power[14]} -radix decimal} {{/test_tb/mapper_out_power[13]} -radix decimal} {{/test_tb/mapper_out_power[12]} -radix decimal} {{/test_tb/mapper_out_power[11]} -radix decimal} {{/test_tb/mapper_out_power[10]} -radix decimal} {{/test_tb/mapper_out_power[9]} -radix decimal} {{/test_tb/mapper_out_power[8]} -radix decimal} {{/test_tb/mapper_out_power[7]} -radix decimal} {{/test_tb/mapper_out_power[6]} -radix decimal} {{/test_tb/mapper_out_power[5]} -radix decimal} {{/test_tb/mapper_out_power[4]} -radix decimal} {{/test_tb/mapper_out_power[3]} -radix decimal} {{/test_tb/mapper_out_power[2]} -radix decimal} {{/test_tb/mapper_out_power[1]} -radix decimal} {{/test_tb/mapper_out_power[0]} -radix decimal}} -subitemconfig {{/test_tb/mapper_out_power[17]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[16]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[15]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[14]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[13]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[12]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[11]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[10]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[9]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[8]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[7]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[6]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[5]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[4]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[3]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[2]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[1]} {-height 16 -radix decimal} {/test_tb/mapper_out_power[0]} {-height 16 -radix decimal}} /test_tb/mapper_out_power
add wave -noupdate -radix decimal -childformat {{{/test_tb/accumulated_squared_error[17]} -radix decimal} {{/test_tb/accumulated_squared_error[16]} -radix decimal} {{/test_tb/accumulated_squared_error[15]} -radix decimal} {{/test_tb/accumulated_squared_error[14]} -radix decimal} {{/test_tb/accumulated_squared_error[13]} -radix decimal} {{/test_tb/accumulated_squared_error[12]} -radix decimal} {{/test_tb/accumulated_squared_error[11]} -radix decimal} {{/test_tb/accumulated_squared_error[10]} -radix decimal} {{/test_tb/accumulated_squared_error[9]} -radix decimal} {{/test_tb/accumulated_squared_error[8]} -radix decimal} {{/test_tb/accumulated_squared_error[7]} -radix decimal} {{/test_tb/accumulated_squared_error[6]} -radix decimal} {{/test_tb/accumulated_squared_error[5]} -radix decimal} {{/test_tb/accumulated_squared_error[4]} -radix decimal} {{/test_tb/accumulated_squared_error[3]} -radix decimal} {{/test_tb/accumulated_squared_error[2]} -radix decimal} {{/test_tb/accumulated_squared_error[1]} -radix decimal} {{/test_tb/accumulated_squared_error[0]} -radix decimal}} -subitemconfig {{/test_tb/accumulated_squared_error[17]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[16]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[15]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[14]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[13]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[12]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[11]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[10]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[9]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[8]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[7]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[6]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[5]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[4]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[3]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[2]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[1]} {-height 16 -radix decimal} {/test_tb/accumulated_squared_error[0]} {-height 16 -radix decimal}} /test_tb/accumulated_squared_error
add wave -noupdate -radix decimal /test_tb/accumulated_error
add wave -noupdate -radix decimal /test_tb/slicer_out
add wave -noupdate -radix decimal /test_tb/mapper_out
add wave -noupdate -radix binary /test_tb/sym_correct
add wave -noupdate -radix binary /test_tb/sym_error
add wave -noupdate -divider Clocks
add wave -noupdate /test_tb/Clocks/sam_clk_enable
add wave -noupdate /test_tb/Clocks/sym_clk_enable
add wave -noupdate /test_tb/Clocks/four_bit_counter
add wave -noupdate /test_tb/Clocks/clk_phase
add wave -noupdate -divider LFSR
add wave -noupdate /test_tb/LFSR_TEST/sys_clk
add wave -noupdate /test_tb/LFSR_TEST/sam_clk_enable
add wave -noupdate /test_tb/LFSR_TEST/reset
add wave -noupdate /test_tb/LFSR_TEST/LFSR_SET
add wave -noupdate /test_tb/LFSR_TEST/q
add wave -noupdate -radix binary /test_tb/LFSR_TEST/q_out
add wave -noupdate /test_tb/LFSR_TEST/q_int
add wave -noupdate -divider {Input Mapper}
add wave -noupdate -radix binary /test_tb/Input_Mapper_Test_I/q_out
add wave -noupdate /test_tb/Input_Mapper_Test_I/input_mapper_out
add wave -noupdate -divider DUT
add wave -noupdate /test_tb/DUT_TEST_I/clk
add wave -noupdate /test_tb/DUT_TEST_I/clk_en
add wave -noupdate /test_tb/DUT_TEST_I/reset
add wave -noupdate /test_tb/DUT_TEST_I/isi_power
add wave -noupdate /test_tb/DUT_TEST_I/in_data
add wave -noupdate /test_tb/DUT_TEST_I/decision_variable
add wave -noupdate /test_tb/DUT_TEST_I/errorless_decision_variable
add wave -noupdate /test_tb/DUT_TEST_I/error
add wave -noupdate /test_tb/DUT_TEST_I/sum_level_one
add wave -noupdate /test_tb/DUT_TEST_I/isi_term_hp
add wave -noupdate /test_tb/DUT_TEST_I/isi_term_lp
add wave -noupdate /test_tb/DUT_TEST_I/decision_variable_temp
add wave -noupdate /test_tb/DUT_TEST_I/errorless_decision_variable_temp
add wave -noupdate /test_tb/DUT_TEST_I/error_temp
add wave -noupdate -divider Slicer
add wave -noupdate /test_tb/slice_Test/reference_level
add wave -noupdate /test_tb/slice_Test/decision_variable
add wave -noupdate -radix binary /test_tb/slice_Test/slicer_out
add wave -noupdate /test_tb/slice_Test/reference_level_negative
add wave -noupdate -divider {Output Mapper}
add wave -noupdate -radix binary /test_tb/map_Test/slicer_out
add wave -noupdate /test_tb/map_Test/reference_level
add wave -noupdate /test_tb/map_Test/mapper_out
add wave -noupdate /test_tb/map_Test/reference_level_a
add wave -noupdate -divider {Output Error Circuit}
add wave -noupdate -radix binary /test_tb/error_circuit_test/reset
add wave -noupdate -radix binary /test_tb/error_circuit_test/sym_clk_enable
add wave -noupdate -radix binary /test_tb/error_circuit_test/sys_clk
add wave -noupdate /test_tb/error_circuit_test/decision_variable
add wave -noupdate -radix binary /test_tb/error_circuit_test/q_out
add wave -noupdate /test_tb/error_circuit_test/mapper_out
add wave -noupdate -radix binary /test_tb/error_circuit_test/slicer_out
add wave -noupdate /test_tb/error_circuit_test/sym_correct
add wave -noupdate /test_tb/error_circuit_test/sym_error
add wave -noupdate -radix binary /test_tb/error_circuit_test/delayed_q1
add wave -noupdate -radix binary /test_tb/error_circuit_test/delayed_q2
add wave -noupdate -radix binary /test_tb/error_circuit_test/delayed_q3
add wave -noupdate -divider {Accumulated Error Circuit}
add wave -noupdate /test_tb/Accum_Error_Test/sym_clk_enable
add wave -noupdate /test_tb/Accum_Error_Test/sys_clk
add wave -noupdate /test_tb/Accum_Error_Test/clear_accumulator
add wave -noupdate /test_tb/Accum_Error_Test/reset
add wave -noupdate /test_tb/Accum_Error_Test/error
add wave -noupdate /test_tb/Accum_Error_Test/accumulator_out
add wave -noupdate /test_tb/Accum_Error_Test/accumulated_error_u
add wave -noupdate /test_tb/Accum_Error_Test/accumulated_error
add wave -noupdate /test_tb/Accum_Error_Test/error_reg
add wave -noupdate -divider {Squared Error Circuit}
add wave -noupdate /test_tb/Squared_Error_Test/clear_accumulator
add wave -noupdate /test_tb/Squared_Error_Test/sym_clk_enable
add wave -noupdate /test_tb/Squared_Error_Test/sys_clk
add wave -noupdate /test_tb/Squared_Error_Test/reset
add wave -noupdate /test_tb/Squared_Error_Test/error_squared
add wave -noupdate /test_tb/Squared_Error_Test/error_sq_reg
add wave -noupdate /test_tb/Squared_Error_Test/accumulator_out
add wave -noupdate /test_tb/Squared_Error_Test/error
add wave -noupdate /test_tb/Squared_Error_Test/accumulated_squared_error_u
add wave -noupdate /test_tb/Squared_Error_Test/accumulated_squared_error
add wave -noupdate -divider {Reference Level}
add wave -noupdate /test_tb/reference_level_test/sys_clk
add wave -noupdate /test_tb/reference_level_test/sym_clk_enable
add wave -noupdate /test_tb/reference_level_test/reset
add wave -noupdate /test_tb/reference_level_test/clear_accumulator
add wave -noupdate /test_tb/reference_level_test/decision_variable
add wave -noupdate /test_tb/reference_level_test/abs_decision_variable
add wave -noupdate /test_tb/reference_level_test/accumulator_out
add wave -noupdate /test_tb/reference_level_test/reference_level_u
add wave -noupdate /test_tb/reference_level_test/reference_level
add wave -noupdate /test_tb/reference_level_test/reference_level_mult
add wave -noupdate /test_tb/reference_level_test/mapper_out_power_untrimmed
add wave -noupdate /test_tb/reference_level_test/mapper_out_power
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1071156437 ns} 0} {{Cursor 2} {671089498 ns} 0}
quietly wave cursor active 2
configure wave -namecolwidth 442
configure wave -valuecolwidth 187
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ms
update
WaveRestoreZoom {671088704 ns} {671089876 ns}
