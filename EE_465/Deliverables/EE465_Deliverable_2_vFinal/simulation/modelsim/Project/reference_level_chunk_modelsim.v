/********************************************************
EE 465 Deliverable 2 Reference Level Chunk
Written by: Sean Froome
********************************************************/
`default_nettype none
module reference_level_chunk_modelsim(
      input wire sys_clk,
  		input wire signed [17:0] decision_variable, //1s17
      input wire reset,
      input wire clear_accumulator,
      input wire sym_clk_enable,
      output reg signed [17:0] reference_level, // 1s17
      output reg unsigned [17:0] mapper_out_power); // 5s17

(* noprune *) reg signed [17:0] abs_decision_variable;
(* noprune *) reg signed [39:0] accumulator_out;
(* preserve*) reg signed [39:0] reference_level_u;
(* noprune *) reg signed [35:0] reference_level_mult;
(* noprune *) reg unsigned [53:0] mapper_out_power_untrimmed; //1s17+1s17+2s16 = 4s50

//localparam a = 19'd163840; //aka 1.25 (1.25*2^17 = 163840), 2s17.
localparam a = 18'd81920; // 2s16

always @ (posedge sys_clk)
  if(reset  == 1'b1)
    abs_decision_variable = 17'd0; //1s17
  //else if((sym_clk_enable == 1'b1) && (decision_variable[17] == 1'b1))
  else if(decision_variable[17] == 1'b1)
    abs_decision_variable = -decision_variable;
  //else if(sym_clk_enable == 1'b1)
//    abs_decision_variable = decision_variable;
  else
    //abs_decision_variable = abs_decision_variable;
    abs_decision_variable = decision_variable;
always @ (posedge sys_clk)
  if(reset == 1'b1)
      accumulator_out = 40'b0; //21s17 (but with a few extra bits so 23s17)
  else if(sym_clk_enable == 1'b1 && clear_accumulator == 1'b1)
      accumulator_out = abs_decision_variable;
  else if(sym_clk_enable == 1'b1)
      accumulator_out = accumulator_out + abs_decision_variable;
  else
      accumulator_out = accumulator_out;

always @ *
  if(reset)
      reference_level_u = 40'd0;
  else if(sym_clk_enable == 1'b1 && clear_accumulator == 1'b1 )
    reference_level_u = accumulator_out >> 22;
 else
    reference_level_u = reference_level_u;

  always @ *
    reference_level = reference_level_u[17:0];

always @ *
  if(reset)
    reference_level_mult = 36'd0;
  else
    reference_level_mult = reference_level*reference_level;

always @ *
  if(reset)
    mapper_out_power_untrimmed = 54'd0;
  else
    mapper_out_power_untrimmed = a*reference_level_mult;


always @ *
  mapper_out_power = mapper_out_power_untrimmed[47:30]; //1s17+1s17+2s16 = 4s50

//always @ *
//  mapper_out_power = mapper_out_power

endmodule
`default_nettype wire
