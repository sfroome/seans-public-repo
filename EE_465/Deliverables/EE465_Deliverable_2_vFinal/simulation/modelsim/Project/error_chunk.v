/********************************************************
EE 465 Deliverable 2 MER Measurement Circuit
Written by: Sean Froome
********************************************************/
`default_nettype none
module error_chunk(
input wire reset,
input wire sym_clk_enable,
input wire sys_clk,
input wire signed [17:0] decision_variable,
input wire signed [1:0] q_out,
input wire signed [17:0] mapper_out,
input wire signed [1:0] slicer_out,
output reg signed [17:0] error_from_dv, // Called error_from_dv in top module and testbench
output reg  sym_correct,
output reg  sym_error);

(* noprune *) reg signed [1:0] delayed_q1, delayed_q2, delayed_q3;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q1 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q1 <= q_out;
  else
    delayed_q1 <= delayed_q1;
always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q2 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q2 <= delayed_q1;
  else
    delayed_q2 <= delayed_q2;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    delayed_q3 <= 2'b0;
  else if(sym_clk_enable == 1'b1)
    delayed_q3 <= delayed_q2;
  else
    delayed_q3 <= delayed_q3;

always @ (posedge sys_clk)
  if(reset == 1'b1)
    error_from_dv = 18'd0; // Called error_from_dv in top module and testbench
  else if(sym_clk_enable == 1'b1)
    error_from_dv = decision_variable - mapper_out;
  else
    error_from_dv = error_from_dv;

always @ (posedge sys_clk)
	if( delayed_q3 == slicer_out)
		sym_correct = 1'b1;
	else
		sym_correct = 1'b0;

always @ *
	sym_error = ~sym_correct;

endmodule
`default_nettype wire
