/*************************************************************
EE 465 Deliverable 2 Top Module.
Written by: Sean Froome

Original Code for the top module
borrowed heavily from EE 461 Lab Code.
NCO is generated and instantiated using
an Altera/Intel IP Block.
LFSR is from EE456 Lab 5 code, and the mapper was originally written for that
lab as well.
*************************************************************/
`default_nettype none
module EE465_Deliverable_2_vFinal(
		input wire CLOCK_50,
		input wire [17:0]SW,
		input wire [3:0] KEY,
		output reg [3:0]  LEDG,
		output reg [17:0] LEDR,
		output wire sym_error,
		output wire sym_correct);

		//wire sym_error;
		//wire sym_correct;
		wire signed [21:0] q;
		wire signed [1:0] q_out;

		wire signed [17:0] input_mapper_out;

		wire signed [17:0] error_from_DUT;
		wire signed [17:0] error_from_dv;

		wire signed [51:0] mapper_out_power;
		wire signed [17:0] reference_level;
		wire signed [17:0] accumulated_squared_error;
		wire signed [17:0] decision_variable, errorless_decision_variable;
		wire signed [17:0] accumulated_error;
		wire signed [17:0] mapper_out;
		wire signed [1:0] slicer_out;
		reg signed [17:0] isi_power;

		always @ *
			isi_power = 18'sd9268;


/************************************************************
Simple but useful. Set the LEDs to light up
when the switches are set high.
************************************************************/
always @ *
	LEDR = SW;
always @ *
	LEDG = KEY;
/************************************************************
Clock Generation and Clock Enables

25MHz Clock setup
************************************************************/
reg reset;
always @ *
	reset = SW[0];

reg sys_clk;
wire sam_clk_enable, sym_clk_enable;

always @ (posedge CLOCK_50)
	sys_clk = ~sys_clk;

Clock_Setup Clocks(
.reset(reset),
.sam_clk_enable(sam_clk_enable),
.sym_clk_enable(sym_clk_enable),
.sys_clk(sys_clk));//,

/************************************************************
End Clock Generation and Clock Enables
*************************************************************
Conditions for Clear Accumulator
************************************************************
reg clear_accumulator; // May be easier to make this an input later.
reg[7:0] counter;
always  @ (posedge sys_clk)
  if(reset == 1'b1)
    counter = 8'd0;
  else if ((sym_clk_enable == 1'b1) && (counter == 8'd127)) //
	//else if ((sym_clk_enable == 1'b1) && (counter == 22'd1048576)) //
    counter = 8'd0;
  else if(sym_clk_enable == 1'b1)
  	counter = counter + 8'd1;

always @ *
	if(reset == 1'b1)
		clear_accumulator = 1'b1;
	//else if(counter == 22'd1048576) // From 0 to 127.
	else if(counter == 8'd127)
		clear_accumulator = 1'b1;
	else
		clear_accumulator = 1'b0;
*/
reg clear_accumulator;
reg[21:0] counter;
always  @ (posedge sys_clk)
  if(reset == 1'b1)
    counter = 22'd0;
  else if ((sym_clk_enable == 1'b1)  &&(counter == 22'd1048576))//&& counter == 22'd1024)
    counter = 22'd0;
  else if(sym_clk_enable == 1'b1)
  	counter = counter + 22'd1;
  else
    counter = counter;

always @ *
  if(reset)
    clear_accumulator = 1'b1;
  else if(counter == 22'd1048576)//(counter == 22'd1024)
  	clear_accumulator = 1'b1;
  else
  	clear_accumulator = 1'b0;
/**************************************************************
Input Mapper and LFSR Instantiation
************************************************************/
reg LFSR_SET;
always @ *
LFSR_SET= SW[1];
LFSR LFSR1(
		.sys_clk(sys_clk),
		.sam_clk_enable(sam_clk_enable),
		.reset(reset),
		.q(q), // 22 bit output
		.q_out(q_out));//,

Input_Mapper Map_In(
		.q_out(q_out),
		.input_mapper_out(input_mapper_out)); // ie: Input to DUT
/*************************************************************
MER DUT Instantiation
*************************************************************/
wire signed [36:0] isi_term_hp;
wire signed [17:0] isi_term_lp;
DUT_for_MER_measurement DUT_I(
		.clk(sys_clk),
		.clk_en(sym_clk_enable),
		.reset(~reset),
		.isi_power(isi_power), // Basically... this is the inverse of the MER error part?
		.in_data(input_mapper_out), //Check later.
		.decision_variable(decision_variable),
		.errorless_decision_variable(errorless_decision_variable),
		.error(error_from_DUT));
	//	.isi_term_hp(isi_term_hp),
//		.isi_term_lp(isi_term_lp));
/*************************************************************
SLICER
*************************************************************/
slicer slice(
	.reference_level(reference_level),
	.decision_variable(decision_variable),
	.slicer_out(slicer_out));
/*************************************************************
Output MER Measurement MAPPER
************************************************************/
Output_Mapper Output_Map(
	.slicer_out(slicer_out),
	.reference_level(reference_level),
	.mapper_out(mapper_out));
/*************************************************************
Error Chunk and Comparison
*************************************************************/
error_chunk error_circuit(
	.reset(reset),
	.sys_clk(sys_clk),
	.sym_clk_enable(sym_clk_enable),
	.decision_variable(decision_variable),
	.mapper_out(mapper_out),
	.slicer_out(slicer_out),
	.q_out(q_out),
	.error_from_dv(error_from_dv),
	.sym_correct(sym_correct),
	.sym_error(sym_error));
/*************************************************************
End Comparison
*************************************************************/
/*************************************************************
REFERENCE LEVEL CHUNK
*************************************************************/
reference_level_chunk reference(
		.reset(reset),
		.decision_variable(decision_variable),
		.clear_accumulator(clear_accumulator),
		.sym_clk_enable(sym_clk_enable),
		.sys_clk(sys_clk),
		.reference_level(reference_level),
		.mapper_out_power(mapper_out_power));
/*************************************************************
SQUARED ACCUMULATED ERROR CHUNK
*************************************************************/
accumulated_squared_error_chunk Squared_Error(
		.sys_clk(sys_clk),
		.reset(reset),
		.error(error_from_dv),
		.clear_accumulator(clear_accumulator),
		.sym_clk_enable(sym_clk_enable),
		.accumulated_squared_error(accumulated_squared_error));
/*************************************************************
ACCUMULATED ERROR CHUNK
*************************************************************/
accumulated_error_chunk Error1(
		.sys_clk(sys_clk),
		.reset(reset),
		.sym_clk_enable(sym_clk_enable),
		.clear_accumulator(clear_accumulator),
		.error(error_from_dv),
		.accumulated_error(accumulated_error));

endmodule
`default_nettype wire
