/********************************************************
Used for EE 465  Deliverable 2 Mapper (MER circuit)
Written by: Sean Froome
********************************************************/
`default_nettype none
module Input_Mapper (
					input wire signed [1:0] q_out,
					output reg signed [17:0] input_mapper_out);

		always @ *
			case(q_out)
				2'b00: begin
								input_mapper_out = -18'd98304; // -0.75
							 end
				2'b01:begin
								input_mapper_out = -18'd32768; // -0.25
							end
				2'b10: begin
								input_mapper_out =  18'd98304; //  0.75
							 end
				2'b11: begin
								input_mapper_out =  18'd32768; //  0.25
						 	 end
			endcase
endmodule
`default_nettype wire
