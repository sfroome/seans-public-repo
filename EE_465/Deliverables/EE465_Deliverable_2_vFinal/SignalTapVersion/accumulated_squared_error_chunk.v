/********************************************************
EE 465 Deliverable 2 Accumulated Squared Error Chunk
Written by: Sean Froome
********************************************************/
`default_nettype none
module accumulated_squared_error_chunk(
  input wire clear_accumulator,
  input wire sym_clk_enable,
  input wire sys_clk,
  input wire reset,
  input wire signed [17:0] error, //1s17
  output reg signed [34:0] accumulated_squared_error); //1s34

(* noprune *) reg signed [35:0] error_squared; //2s34
(* noprune *) reg signed [35:0] error_sq_reg;
(* noprune *) reg signed [57:0] accumulator_out; //24s34 (36 to 58 bits) // Need to extend this for higher MER measurement...
(* noprune *) reg signed [57:0] accumulated_squared_error_u; // 2s34, but with a bunch of extra bits...

always@ *
  error_squared = error*error;

always @ *//(posedge sys_clk)
  if(reset == 1'b1)
      error_sq_reg= 36'd0;
  else
      error_sq_reg = error_squared;

always @ (posedge sys_clk)
  if(reset)
    accumulator_out = 58'd0;
  else if((sym_clk_enable == 1'b1) && (clear_accumulator == 1'b1))
    accumulator_out = error_sq_reg;
  else if(sym_clk_enable == 1'b1)
    accumulator_out = accumulator_out + error_sq_reg;
  else
    accumulator_out = accumulator_out;

always @ (posedge sys_clk)//*
  if(reset == 1'b1)
    accumulated_squared_error_u = 58'd0;
  else if((sym_clk_enable == 1'b1) && (clear_accumulator == 1'b1))
    accumulated_squared_error_u = accumulator_out >> 22; // 24s34 to 2s34.
  else
    accumulated_squared_error_u = accumulated_squared_error_u;

always @ *
  accumulated_squared_error = accumulated_squared_error_u[34:0];

endmodule
`default_nettype wire
