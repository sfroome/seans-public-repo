/********************************************************
EE 465 Deliverable 2 Accumuled Error Chunk
Written by: Sean Froome
********************************************************/
`default_nettype none
module accumulated_error_chunk(
      input wire sym_clk_enable,
      input wire sys_clk,
      input wire clear_accumulator,
      input wire reset,
      input wire signed [17:0] error, //1s17
      output reg signed [17:0] accumulated_error); //1s17

//(* noprune *) reg signed [37:0] accumulator_out; // 21s17
//(* noprune *) reg signed [37:0] accumulated_error_u; //1s17

(* noprune *) reg signed [39:0] accumulator_out; // 21s17
(* noprune *) reg signed [39:0] accumulated_error_u; //1s17

reg signed [17:0] error_reg; //1s17

always @ * //(posedge sys_clk)
  if(reset == 1'b1)
    error_reg = 18'd0;
  else
    error_reg = error;

always @ (posedge sys_clk)
  if(reset == 1'b1)
      accumulator_out = 40'd0;
  else if((sym_clk_enable == 1'b1) && (clear_accumulator == 1'b1))
      accumulator_out = error_reg;
  else if(sym_clk_enable == 1'b1)
    accumulator_out = accumulator_out + error_reg;
  else
    accumulator_out = accumulator_out;

always @ (posedge sys_clk)//*
  if(reset == 1'b1)
    accumulated_error_u = 18'd0;
  else if((clear_accumulator == 1'b1) && sym_clk_enable == 1'b1)
    accumulated_error_u =   accumulator_out >> 22; //21s17 to 1s17
    //accumulated_error_u =   accumulator_out >> 7; //21s17 to 1s17
  else
    accumulated_error_u = accumulated_error_u;

always @ *
  accumulated_error = accumulated_error_u[17:0]; //1s17

endmodule
`default_nettype wire
