-- ------------------------------------------------------------------------- 
-- High Level Design Compiler for Intel(R) FPGAs Version 17.0 (Release Build #595)
-- Quartus Prime development tool and MATLAB/Simulink Interface
-- 
-- Legal Notice: Copyright 2017 Intel Corporation.  All rights reserved.
-- Your use of  Intel Corporation's design tools,  logic functions and other
-- software and  tools, and its AMPP partner logic functions, and any output
-- files any  of the foregoing (including  device programming  or simulation
-- files), and  any associated  documentation  or information  are expressly
-- subject  to the terms and  conditions of the  Intel FPGA Software License
-- Agreement, Intel MegaCore Function License Agreement, or other applicable
-- license agreement,  including,  without limitation,  that your use is for
-- the  sole  purpose of  programming  logic devices  manufactured by  Intel
-- and  sold by Intel  or its authorized  distributors. Please refer  to the
-- applicable agreement for further details.
-- ---------------------------------------------------------------------------

-- VHDL created from log10_0002
-- VHDL created on Fri Mar 16 13:23:12 2018


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.MATH_REAL.all;
use std.TextIO.all;
use work.dspba_library_package.all;

LIBRARY altera_mf;
USE altera_mf.altera_mf_components.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity log10_0002 is
    port (
        a : in std_logic_vector(31 downto 0);  -- float32_m23
        q : out std_logic_vector(31 downto 0);  -- float32_m23
        clk : in std_logic;
        areset : in std_logic
    );
end log10_0002;

architecture normal of log10_0002 is

    attribute altera_attribute : string;
    attribute altera_attribute of normal : architecture is "-name AUTO_SHIFT_REGISTER_RECOGNITION OFF; -name PHYSICAL_SYNTHESIS_REGISTER_DUPLICATION ON; -name MESSAGE_DISABLE 10036; -name MESSAGE_DISABLE 10037; -name MESSAGE_DISABLE 14130; -name MESSAGE_DISABLE 14320; -name MESSAGE_DISABLE 15400; -name MESSAGE_DISABLE 14130; -name MESSAGE_DISABLE 10036; -name MESSAGE_DISABLE 12020; -name MESSAGE_DISABLE 12030; -name MESSAGE_DISABLE 12010; -name MESSAGE_DISABLE 12110; -name MESSAGE_DISABLE 14320; -name MESSAGE_DISABLE 13410; -name MESSAGE_DISABLE 113007";
    
    signal GND_q : STD_LOGIC_VECTOR (0 downto 0);
    signal VCC_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expX_uid6_fpLog10Test_b : STD_LOGIC_VECTOR (7 downto 0);
    signal signX_uid7_fpLog10Test_b : STD_LOGIC_VECTOR (0 downto 0);
    signal cstAllZWF_uid8_fpLog10Test_q : STD_LOGIC_VECTOR (22 downto 0);
    signal cstBias_uid9_fpLog10Test_q : STD_LOGIC_VECTOR (7 downto 0);
    signal cstBiasMO_uid10_fpLog10Test_q : STD_LOGIC_VECTOR (7 downto 0);
    signal cstAllOWE_uid12_fpLog10Test_q : STD_LOGIC_VECTOR (7 downto 0);
    signal cstAllZWE_uid14_fpLog10Test_q : STD_LOGIC_VECTOR (7 downto 0);
    signal frac_x_uid16_fpLog10Test_b : STD_LOGIC_VECTOR (22 downto 0);
    signal excZ_x_uid17_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal expXIsMax_uid18_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsZero_uid19_fpLog10Test_qi : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsZero_uid19_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal fracXIsNotZero_uid20_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excI_x_uid21_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excN_x_uid22_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invExpXIsMax_uid23_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal InvExpXIsZero_uid24_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_x_uid25_fpLog10Test_qi : STD_LOGIC_VECTOR (0 downto 0);
    signal excR_x_uid25_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal e_uid28_fpLog10Test_a : STD_LOGIC_VECTOR (8 downto 0);
    signal e_uid28_fpLog10Test_b : STD_LOGIC_VECTOR (8 downto 0);
    signal e_uid28_fpLog10Test_o : STD_LOGIC_VECTOR (8 downto 0);
    signal e_uid28_fpLog10Test_q : STD_LOGIC_VECTOR (8 downto 0);
    signal c_uid30_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal addrFull_uid31_fpLog10Test_q : STD_LOGIC_VECTOR (23 downto 0);
    signal yAddr_uid33_fpLog10Test_b : STD_LOGIC_VECTOR (8 downto 0);
    signal zPPolyEval_uid34_fpLog10Test_in : STD_LOGIC_VECTOR (14 downto 0);
    signal zPPolyEval_uid34_fpLog10Test_b : STD_LOGIC_VECTOR (14 downto 0);
    signal peOR_uid36_fpLog10Test_in : STD_LOGIC_VECTOR (30 downto 0);
    signal peOR_uid36_fpLog10Test_b : STD_LOGIC_VECTOR (24 downto 0);
    signal aPostPad_uid38_fpLog10Test_q : STD_LOGIC_VECTOR (23 downto 0);
    signal oMz_uid39_fpLog10Test_a : STD_LOGIC_VECTOR (24 downto 0);
    signal oMz_uid39_fpLog10Test_b : STD_LOGIC_VECTOR (24 downto 0);
    signal oMz_uid39_fpLog10Test_o : STD_LOGIC_VECTOR (24 downto 0);
    signal oMz_uid39_fpLog10Test_q : STD_LOGIC_VECTOR (24 downto 0);
    signal z2_uid40_fpLog10Test_q : STD_LOGIC_VECTOR (1 downto 0);
    signal sEz_uid41_fpLog10Test_q : STD_LOGIC_VECTOR (24 downto 0);
    signal multTermOne_uid43_fpLog10Test_s : STD_LOGIC_VECTOR (0 downto 0);
    signal multTermOne_uid43_fpLog10Test_q : STD_LOGIC_VECTOR (24 downto 0);
    signal lowRangeB_uid47_fpLog10Test_in : STD_LOGIC_VECTOR (21 downto 0);
    signal lowRangeB_uid47_fpLog10Test_b : STD_LOGIC_VECTOR (21 downto 0);
    signal highBBits_uid48_fpLog10Test_in : STD_LOGIC_VECTOR (49 downto 0);
    signal highBBits_uid48_fpLog10Test_b : STD_LOGIC_VECTOR (27 downto 0);
    signal finalSumsumAHighB_uid49_fpLog10Test_a : STD_LOGIC_VECTOR (35 downto 0);
    signal finalSumsumAHighB_uid49_fpLog10Test_b : STD_LOGIC_VECTOR (35 downto 0);
    signal finalSumsumAHighB_uid49_fpLog10Test_i : STD_LOGIC_VECTOR (35 downto 0);
    signal finalSumsumAHighB_uid49_fpLog10Test_o : STD_LOGIC_VECTOR (35 downto 0);
    signal finalSumsumAHighB_uid49_fpLog10Test_q : STD_LOGIC_VECTOR (35 downto 0);
    signal finalSum_uid50_fpLog10Test_q : STD_LOGIC_VECTOR (57 downto 0);
    signal msbUFinalSum_uid51_fpLog10Test_b : STD_LOGIC_VECTOR (0 downto 0);
    signal finalSumOneComp_uid53_fpLog10Test_b : STD_LOGIC_VECTOR (57 downto 0);
    signal finalSumOneComp_uid53_fpLog10Test_qi : STD_LOGIC_VECTOR (57 downto 0);
    signal finalSumOneComp_uid53_fpLog10Test_q : STD_LOGIC_VECTOR (57 downto 0);
    signal cstMSBFinalSumPBias_uid57_fpLog10Test_q : STD_LOGIC_VECTOR (8 downto 0);
    signal expRExt_uid58_fpLog10Test_a : STD_LOGIC_VECTOR (9 downto 0);
    signal expRExt_uid58_fpLog10Test_b : STD_LOGIC_VECTOR (9 downto 0);
    signal expRExt_uid58_fpLog10Test_o : STD_LOGIC_VECTOR (9 downto 0);
    signal expRExt_uid58_fpLog10Test_q : STD_LOGIC_VECTOR (9 downto 0);
    signal fracR_uid59_fpLog10Test_in : STD_LOGIC_VECTOR (57 downto 0);
    signal fracR_uid59_fpLog10Test_b : STD_LOGIC_VECTOR (23 downto 0);
    signal expFracConc_uid60_fpLog10Test_q : STD_LOGIC_VECTOR (33 downto 0);
    signal expFracPostRnd_uid62_fpLog10Test_a : STD_LOGIC_VECTOR (34 downto 0);
    signal expFracPostRnd_uid62_fpLog10Test_b : STD_LOGIC_VECTOR (34 downto 0);
    signal expFracPostRnd_uid62_fpLog10Test_o : STD_LOGIC_VECTOR (34 downto 0);
    signal expFracPostRnd_uid62_fpLog10Test_q : STD_LOGIC_VECTOR (34 downto 0);
    signal FPOne_uid65_fpLog10Test_q : STD_LOGIC_VECTOR (31 downto 0);
    signal excRZero_uid67_fpLog10Test_qi : STD_LOGIC_VECTOR (0 downto 0);
    signal excRZero_uid67_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invSignX_uid68_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excRInfC1_uid69_fpLog10Test_qi : STD_LOGIC_VECTOR (0 downto 0);
    signal excRInfC1_uid69_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excRInf_uid70_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal negNonZero_uid72_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN_uid73_fpLog10Test_qi : STD_LOGIC_VECTOR (0 downto 0);
    signal excRNaN_uid73_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal notC_uid74_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signTerm2_uid75_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signRC1_uid76_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signRC11_uid77_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signR_uid78_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal invExcRNaN_uid79_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal signRFull_uid80_fpLog10Test_qi : STD_LOGIC_VECTOR (0 downto 0);
    signal signRFull_uid80_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal concExc_uid81_fpLog10Test_q : STD_LOGIC_VECTOR (2 downto 0);
    signal excREnc_uid82_fpLog10Test_q : STD_LOGIC_VECTOR (1 downto 0);
    signal oneFracRPostExc2_uid83_fpLog10Test_q : STD_LOGIC_VECTOR (22 downto 0);
    signal fracRPostExc_uid86_fpLog10Test_s : STD_LOGIC_VECTOR (1 downto 0);
    signal fracRPostExc_uid86_fpLog10Test_q : STD_LOGIC_VECTOR (22 downto 0);
    signal expRPostExc_uid90_fpLog10Test_s : STD_LOGIC_VECTOR (1 downto 0);
    signal expRPostExc_uid90_fpLog10Test_q : STD_LOGIC_VECTOR (7 downto 0);
    signal RLog10_uid91_fpLog10Test_q : STD_LOGIC_VECTOR (31 downto 0);
    signal xv0_uid93_eLog102_uid29_fpLog10Test_in : STD_LOGIC_VECTOR (3 downto 0);
    signal xv0_uid93_eLog102_uid29_fpLog10Test_b : STD_LOGIC_VECTOR (3 downto 0);
    signal xv1_uid94_eLog102_uid29_fpLog10Test_in : STD_LOGIC_VECTOR (7 downto 0);
    signal xv1_uid94_eLog102_uid29_fpLog10Test_b : STD_LOGIC_VECTOR (3 downto 0);
    signal xv2_uid95_eLog102_uid29_fpLog10Test_b : STD_LOGIC_VECTOR (0 downto 0);
    signal p2_uid96_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (37 downto 0);
    signal p1_uid97_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (36 downto 0);
    signal p0_uid98_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (32 downto 0);
    signal lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_a : STD_LOGIC_VECTOR (39 downto 0);
    signal lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_b : STD_LOGIC_VECTOR (39 downto 0);
    signal lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_o : STD_LOGIC_VECTOR (39 downto 0);
    signal lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (38 downto 0);
    signal lev1_a0_uid102_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (39 downto 0);
    signal lev2_a0_uid103_eLog102_uid29_fpLog10Test_a : STD_LOGIC_VECTOR (41 downto 0);
    signal lev2_a0_uid103_eLog102_uid29_fpLog10Test_b : STD_LOGIC_VECTOR (41 downto 0);
    signal lev2_a0_uid103_eLog102_uid29_fpLog10Test_o : STD_LOGIC_VECTOR (41 downto 0);
    signal lev2_a0_uid103_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (40 downto 0);
    signal maxValInOutFormat_uid104_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (34 downto 0);
    signal minValueInFormat_uid105_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (34 downto 0);
    signal paddingX_uid106_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (2 downto 0);
    signal updatedX_uid107_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (37 downto 0);
    signal ovf_uid106_eLog102_uid29_fpLog10Test_a : STD_LOGIC_VECTOR (42 downto 0);
    signal ovf_uid106_eLog102_uid29_fpLog10Test_b : STD_LOGIC_VECTOR (42 downto 0);
    signal ovf_uid106_eLog102_uid29_fpLog10Test_o : STD_LOGIC_VECTOR (42 downto 0);
    signal ovf_uid106_eLog102_uid29_fpLog10Test_c : STD_LOGIC_VECTOR (0 downto 0);
    signal updatedY_uid110_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (37 downto 0);
    signal udf_uid109_eLog102_uid29_fpLog10Test_a : STD_LOGIC_VECTOR (42 downto 0);
    signal udf_uid109_eLog102_uid29_fpLog10Test_b : STD_LOGIC_VECTOR (42 downto 0);
    signal udf_uid109_eLog102_uid29_fpLog10Test_o : STD_LOGIC_VECTOR (42 downto 0);
    signal udf_uid109_eLog102_uid29_fpLog10Test_c : STD_LOGIC_VECTOR (0 downto 0);
    signal ovfudfcond_uid112_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (1 downto 0);
    signal sR_uid113_eLog102_uid29_fpLog10Test_in : STD_LOGIC_VECTOR (37 downto 0);
    signal sR_uid113_eLog102_uid29_fpLog10Test_b : STD_LOGIC_VECTOR (34 downto 0);
    signal sRA0_uid114_eLog102_uid29_fpLog10Test_s : STD_LOGIC_VECTOR (1 downto 0);
    signal sRA0_uid114_eLog102_uid29_fpLog10Test_q : STD_LOGIC_VECTOR (34 downto 0);
    signal os_uid140_lnTables_q : STD_LOGIC_VECTOR (29 downto 0);
    signal os_uid144_lnTables_q : STD_LOGIC_VECTOR (20 downto 0);
    signal yT1_uid152_invPolyEval_b : STD_LOGIC_VECTOR (12 downto 0);
    signal rndBit_uid154_invPolyEval_q : STD_LOGIC_VECTOR (1 downto 0);
    signal cIncludingRoundingBit_uid155_invPolyEval_q : STD_LOGIC_VECTOR (22 downto 0);
    signal ts1_uid157_invPolyEval_a : STD_LOGIC_VECTOR (23 downto 0);
    signal ts1_uid157_invPolyEval_b : STD_LOGIC_VECTOR (23 downto 0);
    signal ts1_uid157_invPolyEval_o : STD_LOGIC_VECTOR (23 downto 0);
    signal ts1_uid157_invPolyEval_q : STD_LOGIC_VECTOR (23 downto 0);
    signal s1_uid158_invPolyEval_b : STD_LOGIC_VECTOR (22 downto 0);
    signal rndBit_uid161_invPolyEval_q : STD_LOGIC_VECTOR (2 downto 0);
    signal cIncludingRoundingBit_uid162_invPolyEval_q : STD_LOGIC_VECTOR (32 downto 0);
    signal ts2_uid164_invPolyEval_a : STD_LOGIC_VECTOR (33 downto 0);
    signal ts2_uid164_invPolyEval_b : STD_LOGIC_VECTOR (33 downto 0);
    signal ts2_uid164_invPolyEval_o : STD_LOGIC_VECTOR (33 downto 0);
    signal ts2_uid164_invPolyEval_q : STD_LOGIC_VECTOR (33 downto 0);
    signal s2_uid165_invPolyEval_b : STD_LOGIC_VECTOR (32 downto 0);
    signal zs_uid167_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (31 downto 0);
    signal rVStage_uid168_countZ_uid55_fpLog10Test_b : STD_LOGIC_VECTOR (31 downto 0);
    signal vCount_uid169_countZ_uid55_fpLog10Test_qi : STD_LOGIC_VECTOR (0 downto 0);
    signal vCount_uid169_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal mO_uid170_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (4 downto 0);
    signal vStage_uid171_countZ_uid55_fpLog10Test_in : STD_LOGIC_VECTOR (26 downto 0);
    signal vStage_uid171_countZ_uid55_fpLog10Test_b : STD_LOGIC_VECTOR (26 downto 0);
    signal cStage_uid172_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (31 downto 0);
    signal vStagei_uid174_countZ_uid55_fpLog10Test_s : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid174_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (31 downto 0);
    signal zs_uid175_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (15 downto 0);
    signal vCount_uid177_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid180_countZ_uid55_fpLog10Test_s : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid180_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (15 downto 0);
    signal vCount_uid183_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid186_countZ_uid55_fpLog10Test_s : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid186_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (7 downto 0);
    signal zs_uid187_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (3 downto 0);
    signal vCount_uid189_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid192_countZ_uid55_fpLog10Test_s : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid192_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (3 downto 0);
    signal vCount_uid195_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid198_countZ_uid55_fpLog10Test_s : STD_LOGIC_VECTOR (0 downto 0);
    signal vStagei_uid198_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (1 downto 0);
    signal rVStage_uid200_countZ_uid55_fpLog10Test_b : STD_LOGIC_VECTOR (0 downto 0);
    signal vCount_uid201_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (0 downto 0);
    signal r_uid202_countZ_uid55_fpLog10Test_q : STD_LOGIC_VECTOR (5 downto 0);
    signal nx_mergedSignalTM_uid206_pT1_uid153_invPolyEval_q : STD_LOGIC_VECTOR (13 downto 0);
    signal topRangeX_mergedSignalTM_uid218_pT1_uid153_invPolyEval_q : STD_LOGIC_VECTOR (16 downto 0);
    signal topRangeY_mergedSignalTM_uid222_pT1_uid153_invPolyEval_q : STD_LOGIC_VECTOR (16 downto 0);
    signal sm0_uid224_pT1_uid153_invPolyEval_a0 : STD_LOGIC_VECTOR (16 downto 0);
    signal sm0_uid224_pT1_uid153_invPolyEval_b0 : STD_LOGIC_VECTOR (16 downto 0);
    signal sm0_uid224_pT1_uid153_invPolyEval_s1 : STD_LOGIC_VECTOR (33 downto 0);
    signal sm0_uid224_pT1_uid153_invPolyEval_reset : std_logic;
    signal sm0_uid224_pT1_uid153_invPolyEval_q : STD_LOGIC_VECTOR (33 downto 0);
    signal osig_uid225_pT1_uid153_invPolyEval_in : STD_LOGIC_VECTOR (32 downto 0);
    signal osig_uid225_pT1_uid153_invPolyEval_b : STD_LOGIC_VECTOR (14 downto 0);
    signal nx_mergedSignalTM_uid229_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (15 downto 0);
    signal topRangeX_mergedSignalTM_uid241_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (16 downto 0);
    signal topRangeY_uid243_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (16 downto 0);
    signal aboveLeftX_uid249_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (7 downto 0);
    signal aboveLeftY_bottomRange_uid251_pT2_uid160_invPolyEval_in : STD_LOGIC_VECTOR (5 downto 0);
    signal aboveLeftY_bottomRange_uid251_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (5 downto 0);
    signal aboveLeftY_mergedSignalTM_uid252_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (7 downto 0);
    signal rightBottomX_uid260_pT2_uid160_invPolyEval_in : STD_LOGIC_VECTOR (7 downto 0);
    signal rightBottomX_uid260_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (4 downto 0);
    signal rightBottomY_uid261_pT2_uid160_invPolyEval_in : STD_LOGIC_VECTOR (5 downto 0);
    signal rightBottomY_uid261_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (4 downto 0);
    signal n0_uid270_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (3 downto 0);
    signal n1_uid271_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (3 downto 0);
    signal n0_uid278_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (2 downto 0);
    signal n1_uid279_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (2 downto 0);
    signal sm0_uid292_pT2_uid160_invPolyEval_a0 : STD_LOGIC_VECTOR (16 downto 0);
    signal sm0_uid292_pT2_uid160_invPolyEval_b0 : STD_LOGIC_VECTOR (16 downto 0);
    signal sm0_uid292_pT2_uid160_invPolyEval_s1 : STD_LOGIC_VECTOR (33 downto 0);
    signal sm0_uid292_pT2_uid160_invPolyEval_reset : std_logic;
    signal sm0_uid292_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (33 downto 0);
    signal sm0_uid293_pT2_uid160_invPolyEval_a0 : STD_LOGIC_VECTOR (7 downto 0);
    signal sm0_uid293_pT2_uid160_invPolyEval_b0 : STD_LOGIC_VECTOR (8 downto 0);
    signal sm0_uid293_pT2_uid160_invPolyEval_s1 : STD_LOGIC_VECTOR (16 downto 0);
    signal sm0_uid293_pT2_uid160_invPolyEval_reset : std_logic;
    signal sm0_uid293_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (15 downto 0);
    signal sm0_uid294_pT2_uid160_invPolyEval_a0 : STD_LOGIC_VECTOR (2 downto 0);
    signal sm0_uid294_pT2_uid160_invPolyEval_b0 : STD_LOGIC_VECTOR (2 downto 0);
    signal sm0_uid294_pT2_uid160_invPolyEval_s1 : STD_LOGIC_VECTOR (5 downto 0);
    signal sm0_uid294_pT2_uid160_invPolyEval_reset : std_logic;
    signal sm0_uid294_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (5 downto 0);
    signal lowRangeA_uid295_pT2_uid160_invPolyEval_in : STD_LOGIC_VECTOR (0 downto 0);
    signal lowRangeA_uid295_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (0 downto 0);
    signal highABits_uid296_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (32 downto 0);
    signal lev1_a0high_uid297_pT2_uid160_invPolyEval_a : STD_LOGIC_VECTOR (33 downto 0);
    signal lev1_a0high_uid297_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (33 downto 0);
    signal lev1_a0high_uid297_pT2_uid160_invPolyEval_o : STD_LOGIC_VECTOR (33 downto 0);
    signal lev1_a0high_uid297_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (33 downto 0);
    signal lev1_a0_uid298_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (34 downto 0);
    signal lowRangeA_uid299_pT2_uid160_invPolyEval_in : STD_LOGIC_VECTOR (2 downto 0);
    signal lowRangeA_uid299_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (2 downto 0);
    signal highABits_uid300_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (31 downto 0);
    signal lev2_a0high_uid301_pT2_uid160_invPolyEval_a : STD_LOGIC_VECTOR (33 downto 0);
    signal lev2_a0high_uid301_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (33 downto 0);
    signal lev2_a0high_uid301_pT2_uid160_invPolyEval_o : STD_LOGIC_VECTOR (33 downto 0);
    signal lev2_a0high_uid301_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (32 downto 0);
    signal lev2_a0_uid302_pT2_uid160_invPolyEval_q : STD_LOGIC_VECTOR (35 downto 0);
    signal osig_uid303_pT2_uid160_invPolyEval_in : STD_LOGIC_VECTOR (32 downto 0);
    signal osig_uid303_pT2_uid160_invPolyEval_b : STD_LOGIC_VECTOR (24 downto 0);
    signal postPEMul_uid44_fpLog10Test_im0_a0 : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_im0_b0 : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_im0_s1 : STD_LOGIC_VECTOR (33 downto 0);
    signal postPEMul_uid44_fpLog10Test_im0_reset : std_logic;
    signal postPEMul_uid44_fpLog10Test_im0_q : STD_LOGIC_VECTOR (33 downto 0);
    signal postPEMul_uid44_fpLog10Test_bs1_in : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_bs1_b : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_bs2_in : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_bs2_b : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_im3_a0 : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_im3_b0 : STD_LOGIC_VECTOR (7 downto 0);
    signal postPEMul_uid44_fpLog10Test_im3_s1 : STD_LOGIC_VECTOR (24 downto 0);
    signal postPEMul_uid44_fpLog10Test_im3_reset : std_logic;
    signal postPEMul_uid44_fpLog10Test_im3_q : STD_LOGIC_VECTOR (24 downto 0);
    signal postPEMul_uid44_fpLog10Test_bs5_b : STD_LOGIC_VECTOR (7 downto 0);
    signal postPEMul_uid44_fpLog10Test_im6_a0 : STD_LOGIC_VECTOR (17 downto 0);
    signal postPEMul_uid44_fpLog10Test_im6_b0 : STD_LOGIC_VECTOR (7 downto 0);
    signal postPEMul_uid44_fpLog10Test_im6_s1 : STD_LOGIC_VECTOR (25 downto 0);
    signal postPEMul_uid44_fpLog10Test_im6_reset : std_logic;
    signal postPEMul_uid44_fpLog10Test_im6_q : STD_LOGIC_VECTOR (25 downto 0);
    signal postPEMul_uid44_fpLog10Test_bs7_b : STD_LOGIC_VECTOR (7 downto 0);
    signal postPEMul_uid44_fpLog10Test_bs8_in : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_bs8_b : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_bjB9_q : STD_LOGIC_VECTOR (17 downto 0);
    signal postPEMul_uid44_fpLog10Test_im10_a0 : STD_LOGIC_VECTOR (7 downto 0);
    signal postPEMul_uid44_fpLog10Test_im10_b0 : STD_LOGIC_VECTOR (8 downto 0);
    signal postPEMul_uid44_fpLog10Test_im10_s1 : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_im10_reset : std_logic;
    signal postPEMul_uid44_fpLog10Test_im10_q : STD_LOGIC_VECTOR (16 downto 0);
    signal postPEMul_uid44_fpLog10Test_bs12_b : STD_LOGIC_VECTOR (7 downto 0);
    signal postPEMul_uid44_fpLog10Test_bjB13_q : STD_LOGIC_VECTOR (8 downto 0);
    signal postPEMul_uid44_fpLog10Test_align_15_q : STD_LOGIC_VECTOR (41 downto 0);
    signal postPEMul_uid44_fpLog10Test_align_15_qint : STD_LOGIC_VECTOR (41 downto 0);
    signal postPEMul_uid44_fpLog10Test_align_17_q : STD_LOGIC_VECTOR (42 downto 0);
    signal postPEMul_uid44_fpLog10Test_align_17_qint : STD_LOGIC_VECTOR (42 downto 0);
    signal leftShiftStage0Idx1Rng8_uid329_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (50 downto 0);
    signal leftShiftStage0Idx1Rng8_uid329_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (50 downto 0);
    signal leftShiftStage0Idx1_uid330_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage0Idx2Rng16_uid332_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (42 downto 0);
    signal leftShiftStage0Idx2Rng16_uid332_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (42 downto 0);
    signal leftShiftStage0Idx2_uid333_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage0Idx3Pad24_uid334_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (23 downto 0);
    signal leftShiftStage0Idx3Rng24_uid335_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (34 downto 0);
    signal leftShiftStage0Idx3Rng24_uid335_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (34 downto 0);
    signal leftShiftStage0Idx3_uid336_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage0Idx4_uid339_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage0Idx5Pad40_uid340_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (39 downto 0);
    signal leftShiftStage0Idx5Rng40_uid341_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (18 downto 0);
    signal leftShiftStage0Idx5Rng40_uid341_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (18 downto 0);
    signal leftShiftStage0Idx5_uid342_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage0Idx6Pad48_uid343_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (47 downto 0);
    signal leftShiftStage0Idx6Rng48_uid344_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (10 downto 0);
    signal leftShiftStage0Idx6Rng48_uid344_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (10 downto 0);
    signal leftShiftStage0Idx6_uid345_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage0Idx7Pad56_uid346_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (55 downto 0);
    signal leftShiftStage0Idx7Rng56_uid347_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (2 downto 0);
    signal leftShiftStage0Idx7Rng56_uid347_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (2 downto 0);
    signal leftShiftStage0Idx7_uid348_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1Idx1Rng1_uid352_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (57 downto 0);
    signal leftShiftStage1Idx1Rng1_uid352_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (57 downto 0);
    signal leftShiftStage1Idx1_uid353_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1Idx2Rng2_uid355_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (56 downto 0);
    signal leftShiftStage1Idx2Rng2_uid355_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (56 downto 0);
    signal leftShiftStage1Idx2_uid356_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1Idx3Rng3_uid358_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (55 downto 0);
    signal leftShiftStage1Idx3Rng3_uid358_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (55 downto 0);
    signal leftShiftStage1Idx3_uid359_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1Idx4Rng4_uid361_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (54 downto 0);
    signal leftShiftStage1Idx4Rng4_uid361_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (54 downto 0);
    signal leftShiftStage1Idx4_uid362_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1Idx5Pad5_uid363_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (4 downto 0);
    signal leftShiftStage1Idx5Rng5_uid364_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (53 downto 0);
    signal leftShiftStage1Idx5Rng5_uid364_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (53 downto 0);
    signal leftShiftStage1Idx5_uid365_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1Idx6Pad6_uid366_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (5 downto 0);
    signal leftShiftStage1Idx6Rng6_uid367_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (52 downto 0);
    signal leftShiftStage1Idx6Rng6_uid367_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (52 downto 0);
    signal leftShiftStage1Idx6_uid368_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1Idx7Pad7_uid369_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (6 downto 0);
    signal leftShiftStage1Idx7Rng7_uid370_normVal_uid56_fpLog10Test_in : STD_LOGIC_VECTOR (51 downto 0);
    signal leftShiftStage1Idx7Rng7_uid370_normVal_uid56_fpLog10Test_b : STD_LOGIC_VECTOR (51 downto 0);
    signal leftShiftStage1Idx7_uid371_normVal_uid56_fpLog10Test_q : STD_LOGIC_VECTOR (58 downto 0);
    signal memoryC0_uid138_lnTables_lutmem_reset0 : std_logic;
    signal memoryC0_uid138_lnTables_lutmem_ia : STD_LOGIC_VECTOR (17 downto 0);
    signal memoryC0_uid138_lnTables_lutmem_aa : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC0_uid138_lnTables_lutmem_ab : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC0_uid138_lnTables_lutmem_ir : STD_LOGIC_VECTOR (17 downto 0);
    signal memoryC0_uid138_lnTables_lutmem_r : STD_LOGIC_VECTOR (17 downto 0);
    signal memoryC0_uid139_lnTables_lutmem_reset0 : std_logic;
    signal memoryC0_uid139_lnTables_lutmem_ia : STD_LOGIC_VECTOR (11 downto 0);
    signal memoryC0_uid139_lnTables_lutmem_aa : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC0_uid139_lnTables_lutmem_ab : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC0_uid139_lnTables_lutmem_ir : STD_LOGIC_VECTOR (11 downto 0);
    signal memoryC0_uid139_lnTables_lutmem_r : STD_LOGIC_VECTOR (11 downto 0);
    signal memoryC1_uid142_lnTables_lutmem_reset0 : std_logic;
    signal memoryC1_uid142_lnTables_lutmem_ia : STD_LOGIC_VECTOR (17 downto 0);
    signal memoryC1_uid142_lnTables_lutmem_aa : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC1_uid142_lnTables_lutmem_ab : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC1_uid142_lnTables_lutmem_ir : STD_LOGIC_VECTOR (17 downto 0);
    signal memoryC1_uid142_lnTables_lutmem_r : STD_LOGIC_VECTOR (17 downto 0);
    signal memoryC1_uid143_lnTables_lutmem_reset0 : std_logic;
    signal memoryC1_uid143_lnTables_lutmem_ia : STD_LOGIC_VECTOR (2 downto 0);
    signal memoryC1_uid143_lnTables_lutmem_aa : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC1_uid143_lnTables_lutmem_ab : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC1_uid143_lnTables_lutmem_ir : STD_LOGIC_VECTOR (2 downto 0);
    signal memoryC1_uid143_lnTables_lutmem_r : STD_LOGIC_VECTOR (2 downto 0);
    signal memoryC2_uid146_lnTables_lutmem_reset0 : std_logic;
    signal memoryC2_uid146_lnTables_lutmem_ia : STD_LOGIC_VECTOR (12 downto 0);
    signal memoryC2_uid146_lnTables_lutmem_aa : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC2_uid146_lnTables_lutmem_ab : STD_LOGIC_VECTOR (8 downto 0);
    signal memoryC2_uid146_lnTables_lutmem_ir : STD_LOGIC_VECTOR (12 downto 0);
    signal memoryC2_uid146_lnTables_lutmem_r : STD_LOGIC_VECTOR (12 downto 0);
    signal leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_s : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_s : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_s : STD_LOGIC_VECTOR (0 downto 0);
    signal leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_s : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_s : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_q : STD_LOGIC_VECTOR (58 downto 0);
    signal leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_s : STD_LOGIC_VECTOR (0 downto 0);
    signal leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_q : STD_LOGIC_VECTOR (58 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_UpperBits_for_b_q : STD_LOGIC_VECTOR (57 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p1_of_2_a : STD_LOGIC_VECTOR (46 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p1_of_2_b : STD_LOGIC_VECTOR (46 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p1_of_2_o : STD_LOGIC_VECTOR (46 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p1_of_2_c : STD_LOGIC_VECTOR (0 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p1_of_2_q : STD_LOGIC_VECTOR (45 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p2_of_2_a : STD_LOGIC_VECTOR (14 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p2_of_2_b : STD_LOGIC_VECTOR (14 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p2_of_2_o : STD_LOGIC_VECTOR (14 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p2_of_2_cin : STD_LOGIC_VECTOR (0 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_p2_of_2_q : STD_LOGIC_VECTOR (12 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q : STD_LOGIC_VECTOR (58 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_UpperBits_for_b_q : STD_LOGIC_VECTOR (9 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_a : STD_LOGIC_VECTOR (46 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_b : STD_LOGIC_VECTOR (46 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_o : STD_LOGIC_VECTOR (46 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_c : STD_LOGIC_VECTOR (0 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_q : STD_LOGIC_VECTOR (45 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_a : STD_LOGIC_VECTOR (7 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_b : STD_LOGIC_VECTOR (7 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_o : STD_LOGIC_VECTOR (7 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_cin : STD_LOGIC_VECTOR (0 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_q : STD_LOGIC_VECTOR (5 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_a : STD_LOGIC_VECTOR (46 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_b : STD_LOGIC_VECTOR (46 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_o : STD_LOGIC_VECTOR (46 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_c : STD_LOGIC_VECTOR (0 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_q : STD_LOGIC_VECTOR (45 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_a : STD_LOGIC_VECTOR (8 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_b : STD_LOGIC_VECTOR (8 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_o : STD_LOGIC_VECTOR (8 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_cin : STD_LOGIC_VECTOR (0 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_q : STD_LOGIC_VECTOR (6 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_BitJoin_for_q_q : STD_LOGIC_VECTOR (52 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_BitJoin_for_c_q : STD_LOGIC_VECTOR (12 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_0_b : STD_LOGIC_VECTOR (0 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_BitJoin_for_b_q : STD_LOGIC_VECTOR (45 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel0_1_b : STD_LOGIC_VECTOR (11 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_BitJoin_for_b_q : STD_LOGIC_VECTOR (45 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_0_b : STD_LOGIC_VECTOR (4 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_1_b : STD_LOGIC_VECTOR (0 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_BitJoin_for_c_q : STD_LOGIC_VECTOR (5 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_BitJoin_for_b_q : STD_LOGIC_VECTOR (45 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_a_tessel1_1_b : STD_LOGIC_VECTOR (0 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_a_BitJoin_for_c_q : STD_LOGIC_VECTOR (6 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel0_1_b : STD_LOGIC_VECTOR (0 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_BitJoin_for_b_q : STD_LOGIC_VECTOR (45 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_BitJoin_for_c_q : STD_LOGIC_VECTOR (6 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_b : STD_LOGIC_VECTOR (45 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_c : STD_LOGIC_VECTOR (11 downto 0);
    signal fracR_uid63_fpLog10Test_merged_bit_select_in : STD_LOGIC_VECTOR (31 downto 0);
    signal fracR_uid63_fpLog10Test_merged_bit_select_b : STD_LOGIC_VECTOR (22 downto 0);
    signal fracR_uid63_fpLog10Test_merged_bit_select_c : STD_LOGIC_VECTOR (7 downto 0);
    signal lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_b : STD_LOGIC_VECTOR (0 downto 0);
    signal lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_c : STD_LOGIC_VECTOR (35 downto 0);
    signal rVStage_uid176_countZ_uid55_fpLog10Test_merged_bit_select_b : STD_LOGIC_VECTOR (15 downto 0);
    signal rVStage_uid176_countZ_uid55_fpLog10Test_merged_bit_select_c : STD_LOGIC_VECTOR (15 downto 0);
    signal rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select_b : STD_LOGIC_VECTOR (7 downto 0);
    signal rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select_c : STD_LOGIC_VECTOR (7 downto 0);
    signal rVStage_uid188_countZ_uid55_fpLog10Test_merged_bit_select_b : STD_LOGIC_VECTOR (3 downto 0);
    signal rVStage_uid188_countZ_uid55_fpLog10Test_merged_bit_select_c : STD_LOGIC_VECTOR (3 downto 0);
    signal rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select_b : STD_LOGIC_VECTOR (1 downto 0);
    signal rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select_c : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_b : STD_LOGIC_VECTOR (2 downto 0);
    signal leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_c : STD_LOGIC_VECTOR (2 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_1_merged_bit_select_b : STD_LOGIC_VECTOR (44 downto 0);
    signal finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_1_merged_bit_select_c : STD_LOGIC_VECTOR (12 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_tessel0_1_merged_bit_select_b : STD_LOGIC_VECTOR (3 downto 0);
    signal postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_tessel0_1_merged_bit_select_c : STD_LOGIC_VECTOR (5 downto 0);
    signal leftShiftStage0_uid350_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_b : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStage0_uid350_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_c : STD_LOGIC_VECTOR (0 downto 0);
    signal leftShiftStage1_uid373_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_b : STD_LOGIC_VECTOR (1 downto 0);
    signal leftShiftStage1_uid373_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_c : STD_LOGIC_VECTOR (0 downto 0);
    signal redist0_leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_c_1_q : STD_LOGIC_VECTOR (2 downto 0);
    signal redist1_lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_b_1_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist2_finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_c_1_q : STD_LOGIC_VECTOR (11 downto 0);
    signal redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist10_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_1_b_1_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist11_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_0_b_1_q : STD_LOGIC_VECTOR (4 downto 0);
    signal redist12_finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_0_b_1_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist13_postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_q_1_q : STD_LOGIC_VECTOR (45 downto 0);
    signal redist14_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_1_q : STD_LOGIC_VECTOR (58 downto 0);
    signal redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q : STD_LOGIC_VECTOR (58 downto 0);
    signal redist16_finalSumAbs_uid54_fpLog10Test_p1_of_2_q_1_q : STD_LOGIC_VECTOR (45 downto 0);
    signal redist17_memoryC0_uid139_lnTables_lutmem_r_1_q : STD_LOGIC_VECTOR (11 downto 0);
    signal redist18_memoryC0_uid138_lnTables_lutmem_r_1_q : STD_LOGIC_VECTOR (17 downto 0);
    signal redist19_postPEMul_uid44_fpLog10Test_im10_q_1_q : STD_LOGIC_VECTOR (16 downto 0);
    signal redist20_postPEMul_uid44_fpLog10Test_bs8_b_1_q : STD_LOGIC_VECTOR (16 downto 0);
    signal redist21_postPEMul_uid44_fpLog10Test_bs7_b_1_q : STD_LOGIC_VECTOR (7 downto 0);
    signal redist22_postPEMul_uid44_fpLog10Test_im6_q_1_q : STD_LOGIC_VECTOR (25 downto 0);
    signal redist23_postPEMul_uid44_fpLog10Test_im3_q_1_q : STD_LOGIC_VECTOR (24 downto 0);
    signal redist24_postPEMul_uid44_fpLog10Test_im0_q_1_q : STD_LOGIC_VECTOR (33 downto 0);
    signal redist25_osig_uid303_pT2_uid160_invPolyEval_b_1_q : STD_LOGIC_VECTOR (24 downto 0);
    signal redist26_lowRangeA_uid295_pT2_uid160_invPolyEval_b_1_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist27_n1_uid279_pT2_uid160_invPolyEval_b_1_q : STD_LOGIC_VECTOR (2 downto 0);
    signal redist28_n0_uid278_pT2_uid160_invPolyEval_b_1_q : STD_LOGIC_VECTOR (2 downto 0);
    signal redist29_r_uid202_countZ_uid55_fpLog10Test_q_1_q : STD_LOGIC_VECTOR (5 downto 0);
    signal redist30_vCount_uid189_countZ_uid55_fpLog10Test_q_1_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist31_vCount_uid183_countZ_uid55_fpLog10Test_q_1_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist32_vCount_uid177_countZ_uid55_fpLog10Test_q_2_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist33_vStage_uid171_countZ_uid55_fpLog10Test_b_2_q : STD_LOGIC_VECTOR (26 downto 0);
    signal redist34_vCount_uid169_countZ_uid55_fpLog10Test_q_3_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist35_rVStage_uid168_countZ_uid55_fpLog10Test_b_1_q : STD_LOGIC_VECTOR (31 downto 0);
    signal redist36_sR_uid113_eLog102_uid29_fpLog10Test_b_1_q : STD_LOGIC_VECTOR (34 downto 0);
    signal redist37_xv2_uid95_eLog102_uid29_fpLog10Test_b_1_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist38_xv0_uid93_eLog102_uid29_fpLog10Test_b_1_q : STD_LOGIC_VECTOR (3 downto 0);
    signal redist39_excREnc_uid82_fpLog10Test_q_8_q : STD_LOGIC_VECTOR (1 downto 0);
    signal redist40_signRFull_uid80_fpLog10Test_q_8_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist41_excRNaN_uid73_fpLog10Test_q_5_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist42_excRInfC1_uid69_fpLog10Test_q_5_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist43_excRZero_uid67_fpLog10Test_q_15_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist44_fracR_uid59_fpLog10Test_b_1_q : STD_LOGIC_VECTOR (23 downto 0);
    signal redist45_lowRangeB_uid47_fpLog10Test_b_1_q : STD_LOGIC_VECTOR (21 downto 0);
    signal redist46_zPPolyEval_uid34_fpLog10Test_b_2_q : STD_LOGIC_VECTOR (14 downto 0);
    signal redist47_yAddr_uid33_fpLog10Test_b_2_q : STD_LOGIC_VECTOR (8 downto 0);
    signal redist48_yAddr_uid33_fpLog10Test_b_5_q : STD_LOGIC_VECTOR (8 downto 0);
    signal redist49_c_uid30_fpLog10Test_q_8_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist50_c_uid30_fpLog10Test_q_14_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist51_c_uid30_fpLog10Test_q_15_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist52_excR_x_uid25_fpLog10Test_q_5_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist53_fracXIsZero_uid19_fpLog10Test_q_2_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist54_excZ_x_uid17_fpLog10Test_q_5_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist55_frac_x_uid16_fpLog10Test_b_2_q : STD_LOGIC_VECTOR (22 downto 0);
    signal redist56_frac_x_uid16_fpLog10Test_b_8_q : STD_LOGIC_VECTOR (22 downto 0);
    signal redist57_signX_uid7_fpLog10Test_b_10_q : STD_LOGIC_VECTOR (0 downto 0);
    signal redist58_expX_uid6_fpLog10Test_b_10_q : STD_LOGIC_VECTOR (7 downto 0);

begin


    -- VCC(CONSTANT,1)
    VCC_q <= "1";

    -- cstBiasMO_uid10_fpLog10Test(CONSTANT,9)
    cstBiasMO_uid10_fpLog10Test_q <= "01111110";

    -- expX_uid6_fpLog10Test(BITSELECT,5)@0
    expX_uid6_fpLog10Test_b <= a(30 downto 23);

    -- c_uid30_fpLog10Test(LOGICAL,29)@0
    c_uid30_fpLog10Test_q <= "1" WHEN expX_uid6_fpLog10Test_b = cstBiasMO_uid10_fpLog10Test_q ELSE "0";

    -- redist49_c_uid30_fpLog10Test_q_8(DELAY,519)
    redist49_c_uid30_fpLog10Test_q_8 : dspba_delay
    GENERIC MAP ( width => 1, depth => 8, reset_kind => "ASYNC" )
    PORT MAP ( xin => c_uid30_fpLog10Test_q, xout => redist49_c_uid30_fpLog10Test_q_8_q, clk => clk, aclr => areset );

    -- redist50_c_uid30_fpLog10Test_q_14(DELAY,520)
    redist50_c_uid30_fpLog10Test_q_14 : dspba_delay
    GENERIC MAP ( width => 1, depth => 6, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist49_c_uid30_fpLog10Test_q_8_q, xout => redist50_c_uid30_fpLog10Test_q_14_q, clk => clk, aclr => areset );

    -- GND(CONSTANT,0)
    GND_q <= "0";

    -- postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel0_1(BITSELECT,446)@12
    postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel0_1_b <= STD_LOGIC_VECTOR(postPEMul_uid44_fpLog10Test_align_17_q(42 downto 42));

    -- frac_x_uid16_fpLog10Test(BITSELECT,15)@0
    frac_x_uid16_fpLog10Test_b <= a(22 downto 0);

    -- redist55_frac_x_uid16_fpLog10Test_b_2(DELAY,525)
    redist55_frac_x_uid16_fpLog10Test_b_2 : dspba_delay
    GENERIC MAP ( width => 23, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => frac_x_uid16_fpLog10Test_b, xout => redist55_frac_x_uid16_fpLog10Test_b_2_q, clk => clk, aclr => areset );

    -- redist56_frac_x_uid16_fpLog10Test_b_8(DELAY,526)
    redist56_frac_x_uid16_fpLog10Test_b_8 : dspba_delay
    GENERIC MAP ( width => 23, depth => 6, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist55_frac_x_uid16_fpLog10Test_b_2_q, xout => redist56_frac_x_uid16_fpLog10Test_b_8_q, clk => clk, aclr => areset );

    -- cstAllZWF_uid8_fpLog10Test(CONSTANT,7)
    cstAllZWF_uid8_fpLog10Test_q <= "00000000000000000000000";

    -- aPostPad_uid38_fpLog10Test(BITJOIN,37)@8
    aPostPad_uid38_fpLog10Test_q <= VCC_q & cstAllZWF_uid8_fpLog10Test_q;

    -- oMz_uid39_fpLog10Test(SUB,38)@8
    oMz_uid39_fpLog10Test_a <= STD_LOGIC_VECTOR("0" & aPostPad_uid38_fpLog10Test_q);
    oMz_uid39_fpLog10Test_b <= STD_LOGIC_VECTOR("00" & redist56_frac_x_uid16_fpLog10Test_b_8_q);
    oMz_uid39_fpLog10Test_o <= STD_LOGIC_VECTOR(UNSIGNED(oMz_uid39_fpLog10Test_a) - UNSIGNED(oMz_uid39_fpLog10Test_b));
    oMz_uid39_fpLog10Test_q <= oMz_uid39_fpLog10Test_o(24 downto 0);

    -- z2_uid40_fpLog10Test(CONSTANT,39)
    z2_uid40_fpLog10Test_q <= "00";

    -- sEz_uid41_fpLog10Test(BITJOIN,40)@8
    sEz_uid41_fpLog10Test_q <= z2_uid40_fpLog10Test_q & redist56_frac_x_uid16_fpLog10Test_b_8_q;

    -- multTermOne_uid43_fpLog10Test(MUX,42)@8
    multTermOne_uid43_fpLog10Test_s <= redist49_c_uid30_fpLog10Test_q_8_q;
    multTermOne_uid43_fpLog10Test_combproc: PROCESS (multTermOne_uid43_fpLog10Test_s, sEz_uid41_fpLog10Test_q, oMz_uid39_fpLog10Test_q)
    BEGIN
        CASE (multTermOne_uid43_fpLog10Test_s) IS
            WHEN "0" => multTermOne_uid43_fpLog10Test_q <= sEz_uid41_fpLog10Test_q;
            WHEN "1" => multTermOne_uid43_fpLog10Test_q <= oMz_uid39_fpLog10Test_q;
            WHEN OTHERS => multTermOne_uid43_fpLog10Test_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- postPEMul_uid44_fpLog10Test_bs7(BITSELECT,311)@8
    postPEMul_uid44_fpLog10Test_bs7_b <= STD_LOGIC_VECTOR(multTermOne_uid43_fpLog10Test_q(24 downto 17));

    -- redist21_postPEMul_uid44_fpLog10Test_bs7_b_1(DELAY,491)
    redist21_postPEMul_uid44_fpLog10Test_bs7_b_1 : dspba_delay
    GENERIC MAP ( width => 8, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_bs7_b, xout => redist21_postPEMul_uid44_fpLog10Test_bs7_b_1_q, clk => clk, aclr => areset );

    -- addrFull_uid31_fpLog10Test(BITJOIN,30)@0
    addrFull_uid31_fpLog10Test_q <= c_uid30_fpLog10Test_q & frac_x_uid16_fpLog10Test_b;

    -- yAddr_uid33_fpLog10Test(BITSELECT,32)@0
    yAddr_uid33_fpLog10Test_b <= addrFull_uid31_fpLog10Test_q(23 downto 15);

    -- memoryC2_uid146_lnTables_lutmem(DUALMEM,378)@0 + 2
    memoryC2_uid146_lnTables_lutmem_aa <= yAddr_uid33_fpLog10Test_b;
    memoryC2_uid146_lnTables_lutmem_reset0 <= areset;
    memoryC2_uid146_lnTables_lutmem_dmem : altsyncram
    GENERIC MAP (
        ram_block_type => "M9K",
        operation_mode => "ROM",
        width_a => 13,
        widthad_a => 9,
        numwords_a => 512,
        lpm_type => "altsyncram",
        width_byteena_a => 1,
        outdata_reg_a => "CLOCK0",
        outdata_aclr_a => "CLEAR0",
        clock_enable_input_a => "NORMAL",
        power_up_uninitialized => "FALSE",
        init_file => "log10_0002_memoryC2_uid146_lnTables_lutmem.hex",
        init_file_layout => "PORT_A",
        intended_device_family => "Cyclone IV E"
    )
    PORT MAP (
        clocken0 => VCC_q(0),
        aclr0 => memoryC2_uid146_lnTables_lutmem_reset0,
        clock0 => clk,
        address_a => memoryC2_uid146_lnTables_lutmem_aa,
        q_a => memoryC2_uid146_lnTables_lutmem_ir
    );
    memoryC2_uid146_lnTables_lutmem_r <= memoryC2_uid146_lnTables_lutmem_ir(12 downto 0);

    -- zs_uid187_countZ_uid55_fpLog10Test(CONSTANT,186)
    zs_uid187_countZ_uid55_fpLog10Test_q <= "0000";

    -- topRangeY_mergedSignalTM_uid222_pT1_uid153_invPolyEval(BITJOIN,221)@2
    topRangeY_mergedSignalTM_uid222_pT1_uid153_invPolyEval_q <= memoryC2_uid146_lnTables_lutmem_r & zs_uid187_countZ_uid55_fpLog10Test_q;

    -- zPPolyEval_uid34_fpLog10Test(BITSELECT,33)@2
    zPPolyEval_uid34_fpLog10Test_in <= redist55_frac_x_uid16_fpLog10Test_b_2_q(14 downto 0);
    zPPolyEval_uid34_fpLog10Test_b <= zPPolyEval_uid34_fpLog10Test_in(14 downto 0);

    -- yT1_uid152_invPolyEval(BITSELECT,151)@2
    yT1_uid152_invPolyEval_b <= zPPolyEval_uid34_fpLog10Test_b(14 downto 2);

    -- nx_mergedSignalTM_uid206_pT1_uid153_invPolyEval(BITJOIN,205)@2
    nx_mergedSignalTM_uid206_pT1_uid153_invPolyEval_q <= GND_q & yT1_uid152_invPolyEval_b;

    -- paddingX_uid106_eLog102_uid29_fpLog10Test(CONSTANT,105)
    paddingX_uid106_eLog102_uid29_fpLog10Test_q <= "000";

    -- topRangeX_mergedSignalTM_uid218_pT1_uid153_invPolyEval(BITJOIN,217)@2
    topRangeX_mergedSignalTM_uid218_pT1_uid153_invPolyEval_q <= nx_mergedSignalTM_uid206_pT1_uid153_invPolyEval_q & paddingX_uid106_eLog102_uid29_fpLog10Test_q;

    -- sm0_uid224_pT1_uid153_invPolyEval(MULT,223)@2 + 2
    sm0_uid224_pT1_uid153_invPolyEval_a0 <= STD_LOGIC_VECTOR(topRangeX_mergedSignalTM_uid218_pT1_uid153_invPolyEval_q);
    sm0_uid224_pT1_uid153_invPolyEval_b0 <= STD_LOGIC_VECTOR(topRangeY_mergedSignalTM_uid222_pT1_uid153_invPolyEval_q);
    sm0_uid224_pT1_uid153_invPolyEval_reset <= areset;
    sm0_uid224_pT1_uid153_invPolyEval_component : lpm_mult
    GENERIC MAP (
        lpm_widtha => 17,
        lpm_widthb => 17,
        lpm_widthp => 34,
        lpm_widths => 1,
        lpm_type => "LPM_MULT",
        lpm_representation => "SIGNED",
        lpm_hint => "DEDICATED_MULTIPLIER_CIRCUITRY=YES, MAXIMIZE_SPEED=5",
        lpm_pipeline => 2
    )
    PORT MAP (
        dataa => sm0_uid224_pT1_uid153_invPolyEval_a0,
        datab => sm0_uid224_pT1_uid153_invPolyEval_b0,
        clken => VCC_q(0),
        aclr => sm0_uid224_pT1_uid153_invPolyEval_reset,
        clock => clk,
        result => sm0_uid224_pT1_uid153_invPolyEval_s1
    );
    sm0_uid224_pT1_uid153_invPolyEval_q <= sm0_uid224_pT1_uid153_invPolyEval_s1;

    -- osig_uid225_pT1_uid153_invPolyEval(BITSELECT,224)@4
    osig_uid225_pT1_uid153_invPolyEval_in <= STD_LOGIC_VECTOR(sm0_uid224_pT1_uid153_invPolyEval_q(32 downto 0));
    osig_uid225_pT1_uid153_invPolyEval_b <= STD_LOGIC_VECTOR(osig_uid225_pT1_uid153_invPolyEval_in(32 downto 18));

    -- redist47_yAddr_uid33_fpLog10Test_b_2(DELAY,517)
    redist47_yAddr_uid33_fpLog10Test_b_2 : dspba_delay
    GENERIC MAP ( width => 9, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => yAddr_uid33_fpLog10Test_b, xout => redist47_yAddr_uid33_fpLog10Test_b_2_q, clk => clk, aclr => areset );

    -- memoryC1_uid143_lnTables_lutmem(DUALMEM,377)@2 + 2
    memoryC1_uid143_lnTables_lutmem_aa <= redist47_yAddr_uid33_fpLog10Test_b_2_q;
    memoryC1_uid143_lnTables_lutmem_reset0 <= areset;
    memoryC1_uid143_lnTables_lutmem_dmem : altsyncram
    GENERIC MAP (
        ram_block_type => "M9K",
        operation_mode => "ROM",
        width_a => 3,
        widthad_a => 9,
        numwords_a => 512,
        lpm_type => "altsyncram",
        width_byteena_a => 1,
        outdata_reg_a => "CLOCK0",
        outdata_aclr_a => "CLEAR0",
        clock_enable_input_a => "NORMAL",
        power_up_uninitialized => "FALSE",
        init_file => "log10_0002_memoryC1_uid143_lnTables_lutmem.hex",
        init_file_layout => "PORT_A",
        intended_device_family => "Cyclone IV E"
    )
    PORT MAP (
        clocken0 => VCC_q(0),
        aclr0 => memoryC1_uid143_lnTables_lutmem_reset0,
        clock0 => clk,
        address_a => memoryC1_uid143_lnTables_lutmem_aa,
        q_a => memoryC1_uid143_lnTables_lutmem_ir
    );
    memoryC1_uid143_lnTables_lutmem_r <= memoryC1_uid143_lnTables_lutmem_ir(2 downto 0);

    -- memoryC1_uid142_lnTables_lutmem(DUALMEM,376)@2 + 2
    memoryC1_uid142_lnTables_lutmem_aa <= redist47_yAddr_uid33_fpLog10Test_b_2_q;
    memoryC1_uid142_lnTables_lutmem_reset0 <= areset;
    memoryC1_uid142_lnTables_lutmem_dmem : altsyncram
    GENERIC MAP (
        ram_block_type => "M9K",
        operation_mode => "ROM",
        width_a => 18,
        widthad_a => 9,
        numwords_a => 512,
        lpm_type => "altsyncram",
        width_byteena_a => 1,
        outdata_reg_a => "CLOCK0",
        outdata_aclr_a => "CLEAR0",
        clock_enable_input_a => "NORMAL",
        power_up_uninitialized => "FALSE",
        init_file => "log10_0002_memoryC1_uid142_lnTables_lutmem.hex",
        init_file_layout => "PORT_A",
        intended_device_family => "Cyclone IV E"
    )
    PORT MAP (
        clocken0 => VCC_q(0),
        aclr0 => memoryC1_uid142_lnTables_lutmem_reset0,
        clock0 => clk,
        address_a => memoryC1_uid142_lnTables_lutmem_aa,
        q_a => memoryC1_uid142_lnTables_lutmem_ir
    );
    memoryC1_uid142_lnTables_lutmem_r <= memoryC1_uid142_lnTables_lutmem_ir(17 downto 0);

    -- os_uid144_lnTables(BITJOIN,143)@4
    os_uid144_lnTables_q <= memoryC1_uid143_lnTables_lutmem_r & memoryC1_uid142_lnTables_lutmem_r;

    -- rndBit_uid154_invPolyEval(CONSTANT,153)
    rndBit_uid154_invPolyEval_q <= "01";

    -- cIncludingRoundingBit_uid155_invPolyEval(BITJOIN,154)@4
    cIncludingRoundingBit_uid155_invPolyEval_q <= os_uid144_lnTables_q & rndBit_uid154_invPolyEval_q;

    -- ts1_uid157_invPolyEval(ADD,156)@4
    ts1_uid157_invPolyEval_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((23 downto 23 => cIncludingRoundingBit_uid155_invPolyEval_q(22)) & cIncludingRoundingBit_uid155_invPolyEval_q));
    ts1_uid157_invPolyEval_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((23 downto 15 => osig_uid225_pT1_uid153_invPolyEval_b(14)) & osig_uid225_pT1_uid153_invPolyEval_b));
    ts1_uid157_invPolyEval_o <= STD_LOGIC_VECTOR(SIGNED(ts1_uid157_invPolyEval_a) + SIGNED(ts1_uid157_invPolyEval_b));
    ts1_uid157_invPolyEval_q <= ts1_uid157_invPolyEval_o(23 downto 0);

    -- s1_uid158_invPolyEval(BITSELECT,157)@4
    s1_uid158_invPolyEval_b <= STD_LOGIC_VECTOR(ts1_uid157_invPolyEval_q(23 downto 1));

    -- rightBottomY_uid261_pT2_uid160_invPolyEval(BITSELECT,260)@4
    rightBottomY_uid261_pT2_uid160_invPolyEval_in <= s1_uid158_invPolyEval_b(5 downto 0);
    rightBottomY_uid261_pT2_uid160_invPolyEval_b <= rightBottomY_uid261_pT2_uid160_invPolyEval_in(5 downto 1);

    -- n1_uid271_pT2_uid160_invPolyEval(BITSELECT,270)@4
    n1_uid271_pT2_uid160_invPolyEval_b <= rightBottomY_uid261_pT2_uid160_invPolyEval_b(4 downto 1);

    -- n1_uid279_pT2_uid160_invPolyEval(BITSELECT,278)@4
    n1_uid279_pT2_uid160_invPolyEval_b <= n1_uid271_pT2_uid160_invPolyEval_b(3 downto 1);

    -- redist27_n1_uid279_pT2_uid160_invPolyEval_b_1(DELAY,497)
    redist27_n1_uid279_pT2_uid160_invPolyEval_b_1 : dspba_delay
    GENERIC MAP ( width => 3, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => n1_uid279_pT2_uid160_invPolyEval_b, xout => redist27_n1_uid279_pT2_uid160_invPolyEval_b_1_q, clk => clk, aclr => areset );

    -- redist46_zPPolyEval_uid34_fpLog10Test_b_2(DELAY,516)
    redist46_zPPolyEval_uid34_fpLog10Test_b_2 : dspba_delay
    GENERIC MAP ( width => 15, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => zPPolyEval_uid34_fpLog10Test_b, xout => redist46_zPPolyEval_uid34_fpLog10Test_b_2_q, clk => clk, aclr => areset );

    -- nx_mergedSignalTM_uid229_pT2_uid160_invPolyEval(BITJOIN,228)@4
    nx_mergedSignalTM_uid229_pT2_uid160_invPolyEval_q <= GND_q & redist46_zPPolyEval_uid34_fpLog10Test_b_2_q;

    -- rightBottomX_uid260_pT2_uid160_invPolyEval(BITSELECT,259)@4
    rightBottomX_uid260_pT2_uid160_invPolyEval_in <= nx_mergedSignalTM_uid229_pT2_uid160_invPolyEval_q(7 downto 0);
    rightBottomX_uid260_pT2_uid160_invPolyEval_b <= rightBottomX_uid260_pT2_uid160_invPolyEval_in(7 downto 3);

    -- n0_uid270_pT2_uid160_invPolyEval(BITSELECT,269)@4
    n0_uid270_pT2_uid160_invPolyEval_b <= rightBottomX_uid260_pT2_uid160_invPolyEval_b(4 downto 1);

    -- n0_uid278_pT2_uid160_invPolyEval(BITSELECT,277)@4
    n0_uid278_pT2_uid160_invPolyEval_b <= n0_uid270_pT2_uid160_invPolyEval_b(3 downto 1);

    -- redist28_n0_uid278_pT2_uid160_invPolyEval_b_1(DELAY,498)
    redist28_n0_uid278_pT2_uid160_invPolyEval_b_1 : dspba_delay
    GENERIC MAP ( width => 3, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => n0_uid278_pT2_uid160_invPolyEval_b, xout => redist28_n0_uid278_pT2_uid160_invPolyEval_b_1_q, clk => clk, aclr => areset );

    -- sm0_uid294_pT2_uid160_invPolyEval(MULT,293)@5 + 2
    sm0_uid294_pT2_uid160_invPolyEval_a0 <= redist28_n0_uid278_pT2_uid160_invPolyEval_b_1_q;
    sm0_uid294_pT2_uid160_invPolyEval_b0 <= redist27_n1_uid279_pT2_uid160_invPolyEval_b_1_q;
    sm0_uid294_pT2_uid160_invPolyEval_reset <= areset;
    sm0_uid294_pT2_uid160_invPolyEval_component : lpm_mult
    GENERIC MAP (
        lpm_widtha => 3,
        lpm_widthb => 3,
        lpm_widthp => 6,
        lpm_widths => 1,
        lpm_type => "LPM_MULT",
        lpm_representation => "UNSIGNED",
        lpm_hint => "DEDICATED_MULTIPLIER_CIRCUITRY=NO, MAXIMIZE_SPEED=5",
        lpm_pipeline => 2
    )
    PORT MAP (
        dataa => sm0_uid294_pT2_uid160_invPolyEval_a0,
        datab => sm0_uid294_pT2_uid160_invPolyEval_b0,
        clken => VCC_q(0),
        aclr => sm0_uid294_pT2_uid160_invPolyEval_reset,
        clock => clk,
        result => sm0_uid294_pT2_uid160_invPolyEval_s1
    );
    sm0_uid294_pT2_uid160_invPolyEval_q <= sm0_uid294_pT2_uid160_invPolyEval_s1;

    -- aboveLeftY_bottomRange_uid251_pT2_uid160_invPolyEval(BITSELECT,250)@4
    aboveLeftY_bottomRange_uid251_pT2_uid160_invPolyEval_in <= STD_LOGIC_VECTOR(s1_uid158_invPolyEval_b(5 downto 0));
    aboveLeftY_bottomRange_uid251_pT2_uid160_invPolyEval_b <= STD_LOGIC_VECTOR(aboveLeftY_bottomRange_uid251_pT2_uid160_invPolyEval_in(5 downto 0));

    -- aboveLeftY_mergedSignalTM_uid252_pT2_uid160_invPolyEval(BITJOIN,251)@4
    aboveLeftY_mergedSignalTM_uid252_pT2_uid160_invPolyEval_q <= aboveLeftY_bottomRange_uid251_pT2_uid160_invPolyEval_b & z2_uid40_fpLog10Test_q;

    -- aboveLeftX_uid249_pT2_uid160_invPolyEval(BITSELECT,248)@4
    aboveLeftX_uid249_pT2_uid160_invPolyEval_b <= STD_LOGIC_VECTOR(nx_mergedSignalTM_uid229_pT2_uid160_invPolyEval_q(15 downto 8));

    -- sm0_uid293_pT2_uid160_invPolyEval(MULT,292)@4 + 2
    sm0_uid293_pT2_uid160_invPolyEval_a0 <= STD_LOGIC_VECTOR(aboveLeftX_uid249_pT2_uid160_invPolyEval_b);
    sm0_uid293_pT2_uid160_invPolyEval_b0 <= '0' & aboveLeftY_mergedSignalTM_uid252_pT2_uid160_invPolyEval_q;
    sm0_uid293_pT2_uid160_invPolyEval_reset <= areset;
    sm0_uid293_pT2_uid160_invPolyEval_component : lpm_mult
    GENERIC MAP (
        lpm_widtha => 8,
        lpm_widthb => 9,
        lpm_widthp => 17,
        lpm_widths => 1,
        lpm_type => "LPM_MULT",
        lpm_representation => "SIGNED",
        lpm_hint => "DEDICATED_MULTIPLIER_CIRCUITRY=YES, MAXIMIZE_SPEED=5",
        lpm_pipeline => 2
    )
    PORT MAP (
        dataa => sm0_uid293_pT2_uid160_invPolyEval_a0,
        datab => sm0_uid293_pT2_uid160_invPolyEval_b0,
        clken => VCC_q(0),
        aclr => sm0_uid293_pT2_uid160_invPolyEval_reset,
        clock => clk,
        result => sm0_uid293_pT2_uid160_invPolyEval_s1
    );
    sm0_uid293_pT2_uid160_invPolyEval_q <= sm0_uid293_pT2_uid160_invPolyEval_s1(15 downto 0);

    -- topRangeY_uid243_pT2_uid160_invPolyEval(BITSELECT,242)@4
    topRangeY_uid243_pT2_uid160_invPolyEval_b <= STD_LOGIC_VECTOR(s1_uid158_invPolyEval_b(22 downto 6));

    -- topRangeX_mergedSignalTM_uid241_pT2_uid160_invPolyEval(BITJOIN,240)@4
    topRangeX_mergedSignalTM_uid241_pT2_uid160_invPolyEval_q <= nx_mergedSignalTM_uid229_pT2_uid160_invPolyEval_q & GND_q;

    -- sm0_uid292_pT2_uid160_invPolyEval(MULT,291)@4 + 2
    sm0_uid292_pT2_uid160_invPolyEval_a0 <= STD_LOGIC_VECTOR(topRangeX_mergedSignalTM_uid241_pT2_uid160_invPolyEval_q);
    sm0_uid292_pT2_uid160_invPolyEval_b0 <= STD_LOGIC_VECTOR(topRangeY_uid243_pT2_uid160_invPolyEval_b);
    sm0_uid292_pT2_uid160_invPolyEval_reset <= areset;
    sm0_uid292_pT2_uid160_invPolyEval_component : lpm_mult
    GENERIC MAP (
        lpm_widtha => 17,
        lpm_widthb => 17,
        lpm_widthp => 34,
        lpm_widths => 1,
        lpm_type => "LPM_MULT",
        lpm_representation => "SIGNED",
        lpm_hint => "DEDICATED_MULTIPLIER_CIRCUITRY=YES, MAXIMIZE_SPEED=5",
        lpm_pipeline => 2
    )
    PORT MAP (
        dataa => sm0_uid292_pT2_uid160_invPolyEval_a0,
        datab => sm0_uid292_pT2_uid160_invPolyEval_b0,
        clken => VCC_q(0),
        aclr => sm0_uid292_pT2_uid160_invPolyEval_reset,
        clock => clk,
        result => sm0_uid292_pT2_uid160_invPolyEval_s1
    );
    sm0_uid292_pT2_uid160_invPolyEval_q <= sm0_uid292_pT2_uid160_invPolyEval_s1;

    -- highABits_uid296_pT2_uid160_invPolyEval(BITSELECT,295)@6
    highABits_uid296_pT2_uid160_invPolyEval_b <= STD_LOGIC_VECTOR(sm0_uid292_pT2_uid160_invPolyEval_q(33 downto 1));

    -- lev1_a0high_uid297_pT2_uid160_invPolyEval(ADD,296)@6 + 1
    lev1_a0high_uid297_pT2_uid160_invPolyEval_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((33 downto 33 => highABits_uid296_pT2_uid160_invPolyEval_b(32)) & highABits_uid296_pT2_uid160_invPolyEval_b));
    lev1_a0high_uid297_pT2_uid160_invPolyEval_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((33 downto 16 => sm0_uid293_pT2_uid160_invPolyEval_q(15)) & sm0_uid293_pT2_uid160_invPolyEval_q));
    lev1_a0high_uid297_pT2_uid160_invPolyEval_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            lev1_a0high_uid297_pT2_uid160_invPolyEval_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            lev1_a0high_uid297_pT2_uid160_invPolyEval_o <= STD_LOGIC_VECTOR(SIGNED(lev1_a0high_uid297_pT2_uid160_invPolyEval_a) + SIGNED(lev1_a0high_uid297_pT2_uid160_invPolyEval_b));
        END IF;
    END PROCESS;
    lev1_a0high_uid297_pT2_uid160_invPolyEval_q <= lev1_a0high_uid297_pT2_uid160_invPolyEval_o(33 downto 0);

    -- lowRangeA_uid295_pT2_uid160_invPolyEval(BITSELECT,294)@6
    lowRangeA_uid295_pT2_uid160_invPolyEval_in <= sm0_uid292_pT2_uid160_invPolyEval_q(0 downto 0);
    lowRangeA_uid295_pT2_uid160_invPolyEval_b <= lowRangeA_uid295_pT2_uid160_invPolyEval_in(0 downto 0);

    -- redist26_lowRangeA_uid295_pT2_uid160_invPolyEval_b_1(DELAY,496)
    redist26_lowRangeA_uid295_pT2_uid160_invPolyEval_b_1 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => lowRangeA_uid295_pT2_uid160_invPolyEval_b, xout => redist26_lowRangeA_uid295_pT2_uid160_invPolyEval_b_1_q, clk => clk, aclr => areset );

    -- lev1_a0_uid298_pT2_uid160_invPolyEval(BITJOIN,297)@7
    lev1_a0_uid298_pT2_uid160_invPolyEval_q <= lev1_a0high_uid297_pT2_uid160_invPolyEval_q & redist26_lowRangeA_uid295_pT2_uid160_invPolyEval_b_1_q;

    -- highABits_uid300_pT2_uid160_invPolyEval(BITSELECT,299)@7
    highABits_uid300_pT2_uid160_invPolyEval_b <= STD_LOGIC_VECTOR(lev1_a0_uid298_pT2_uid160_invPolyEval_q(34 downto 3));

    -- lev2_a0high_uid301_pT2_uid160_invPolyEval(ADD,300)@7
    lev2_a0high_uid301_pT2_uid160_invPolyEval_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((33 downto 32 => highABits_uid300_pT2_uid160_invPolyEval_b(31)) & highABits_uid300_pT2_uid160_invPolyEval_b));
    lev2_a0high_uid301_pT2_uid160_invPolyEval_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR("0" & "000000000000000000000000000" & sm0_uid294_pT2_uid160_invPolyEval_q));
    lev2_a0high_uid301_pT2_uid160_invPolyEval_o <= STD_LOGIC_VECTOR(SIGNED(lev2_a0high_uid301_pT2_uid160_invPolyEval_a) + SIGNED(lev2_a0high_uid301_pT2_uid160_invPolyEval_b));
    lev2_a0high_uid301_pT2_uid160_invPolyEval_q <= lev2_a0high_uid301_pT2_uid160_invPolyEval_o(32 downto 0);

    -- lowRangeA_uid299_pT2_uid160_invPolyEval(BITSELECT,298)@7
    lowRangeA_uid299_pT2_uid160_invPolyEval_in <= lev1_a0_uid298_pT2_uid160_invPolyEval_q(2 downto 0);
    lowRangeA_uid299_pT2_uid160_invPolyEval_b <= lowRangeA_uid299_pT2_uid160_invPolyEval_in(2 downto 0);

    -- lev2_a0_uid302_pT2_uid160_invPolyEval(BITJOIN,301)@7
    lev2_a0_uid302_pT2_uid160_invPolyEval_q <= lev2_a0high_uid301_pT2_uid160_invPolyEval_q & lowRangeA_uid299_pT2_uid160_invPolyEval_b;

    -- osig_uid303_pT2_uid160_invPolyEval(BITSELECT,302)@7
    osig_uid303_pT2_uid160_invPolyEval_in <= STD_LOGIC_VECTOR(lev2_a0_uid302_pT2_uid160_invPolyEval_q(32 downto 0));
    osig_uid303_pT2_uid160_invPolyEval_b <= STD_LOGIC_VECTOR(osig_uid303_pT2_uid160_invPolyEval_in(32 downto 8));

    -- redist25_osig_uid303_pT2_uid160_invPolyEval_b_1(DELAY,495)
    redist25_osig_uid303_pT2_uid160_invPolyEval_b_1 : dspba_delay
    GENERIC MAP ( width => 25, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => osig_uid303_pT2_uid160_invPolyEval_b, xout => redist25_osig_uid303_pT2_uid160_invPolyEval_b_1_q, clk => clk, aclr => areset );

    -- redist48_yAddr_uid33_fpLog10Test_b_5(DELAY,518)
    redist48_yAddr_uid33_fpLog10Test_b_5 : dspba_delay
    GENERIC MAP ( width => 9, depth => 3, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist47_yAddr_uid33_fpLog10Test_b_2_q, xout => redist48_yAddr_uid33_fpLog10Test_b_5_q, clk => clk, aclr => areset );

    -- memoryC0_uid139_lnTables_lutmem(DUALMEM,375)@5 + 2
    memoryC0_uid139_lnTables_lutmem_aa <= redist48_yAddr_uid33_fpLog10Test_b_5_q;
    memoryC0_uid139_lnTables_lutmem_reset0 <= areset;
    memoryC0_uid139_lnTables_lutmem_dmem : altsyncram
    GENERIC MAP (
        ram_block_type => "M9K",
        operation_mode => "ROM",
        width_a => 12,
        widthad_a => 9,
        numwords_a => 512,
        lpm_type => "altsyncram",
        width_byteena_a => 1,
        outdata_reg_a => "CLOCK0",
        outdata_aclr_a => "CLEAR0",
        clock_enable_input_a => "NORMAL",
        power_up_uninitialized => "FALSE",
        init_file => "log10_0002_memoryC0_uid139_lnTables_lutmem.hex",
        init_file_layout => "PORT_A",
        intended_device_family => "Cyclone IV E"
    )
    PORT MAP (
        clocken0 => VCC_q(0),
        aclr0 => memoryC0_uid139_lnTables_lutmem_reset0,
        clock0 => clk,
        address_a => memoryC0_uid139_lnTables_lutmem_aa,
        q_a => memoryC0_uid139_lnTables_lutmem_ir
    );
    memoryC0_uid139_lnTables_lutmem_r <= memoryC0_uid139_lnTables_lutmem_ir(11 downto 0);

    -- redist17_memoryC0_uid139_lnTables_lutmem_r_1(DELAY,487)
    redist17_memoryC0_uid139_lnTables_lutmem_r_1 : dspba_delay
    GENERIC MAP ( width => 12, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => memoryC0_uid139_lnTables_lutmem_r, xout => redist17_memoryC0_uid139_lnTables_lutmem_r_1_q, clk => clk, aclr => areset );

    -- memoryC0_uid138_lnTables_lutmem(DUALMEM,374)@5 + 2
    memoryC0_uid138_lnTables_lutmem_aa <= redist48_yAddr_uid33_fpLog10Test_b_5_q;
    memoryC0_uid138_lnTables_lutmem_reset0 <= areset;
    memoryC0_uid138_lnTables_lutmem_dmem : altsyncram
    GENERIC MAP (
        ram_block_type => "M9K",
        operation_mode => "ROM",
        width_a => 18,
        widthad_a => 9,
        numwords_a => 512,
        lpm_type => "altsyncram",
        width_byteena_a => 1,
        outdata_reg_a => "CLOCK0",
        outdata_aclr_a => "CLEAR0",
        clock_enable_input_a => "NORMAL",
        power_up_uninitialized => "FALSE",
        init_file => "log10_0002_memoryC0_uid138_lnTables_lutmem.hex",
        init_file_layout => "PORT_A",
        intended_device_family => "Cyclone IV E"
    )
    PORT MAP (
        clocken0 => VCC_q(0),
        aclr0 => memoryC0_uid138_lnTables_lutmem_reset0,
        clock0 => clk,
        address_a => memoryC0_uid138_lnTables_lutmem_aa,
        q_a => memoryC0_uid138_lnTables_lutmem_ir
    );
    memoryC0_uid138_lnTables_lutmem_r <= memoryC0_uid138_lnTables_lutmem_ir(17 downto 0);

    -- redist18_memoryC0_uid138_lnTables_lutmem_r_1(DELAY,488)
    redist18_memoryC0_uid138_lnTables_lutmem_r_1 : dspba_delay
    GENERIC MAP ( width => 18, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => memoryC0_uid138_lnTables_lutmem_r, xout => redist18_memoryC0_uid138_lnTables_lutmem_r_1_q, clk => clk, aclr => areset );

    -- os_uid140_lnTables(BITJOIN,139)@8
    os_uid140_lnTables_q <= redist17_memoryC0_uid139_lnTables_lutmem_r_1_q & redist18_memoryC0_uid138_lnTables_lutmem_r_1_q;

    -- rndBit_uid161_invPolyEval(CONSTANT,160)
    rndBit_uid161_invPolyEval_q <= "001";

    -- cIncludingRoundingBit_uid162_invPolyEval(BITJOIN,161)@8
    cIncludingRoundingBit_uid162_invPolyEval_q <= os_uid140_lnTables_q & rndBit_uid161_invPolyEval_q;

    -- ts2_uid164_invPolyEval(ADD,163)@8
    ts2_uid164_invPolyEval_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((33 downto 33 => cIncludingRoundingBit_uid162_invPolyEval_q(32)) & cIncludingRoundingBit_uid162_invPolyEval_q));
    ts2_uid164_invPolyEval_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((33 downto 25 => redist25_osig_uid303_pT2_uid160_invPolyEval_b_1_q(24)) & redist25_osig_uid303_pT2_uid160_invPolyEval_b_1_q));
    ts2_uid164_invPolyEval_o <= STD_LOGIC_VECTOR(SIGNED(ts2_uid164_invPolyEval_a) + SIGNED(ts2_uid164_invPolyEval_b));
    ts2_uid164_invPolyEval_q <= ts2_uid164_invPolyEval_o(33 downto 0);

    -- s2_uid165_invPolyEval(BITSELECT,164)@8
    s2_uid165_invPolyEval_b <= STD_LOGIC_VECTOR(ts2_uid164_invPolyEval_q(33 downto 1));

    -- peOR_uid36_fpLog10Test(BITSELECT,35)@8
    peOR_uid36_fpLog10Test_in <= s2_uid165_invPolyEval_b(30 downto 0);
    peOR_uid36_fpLog10Test_b <= peOR_uid36_fpLog10Test_in(30 downto 6);

    -- postPEMul_uid44_fpLog10Test_bs8(BITSELECT,312)@8
    postPEMul_uid44_fpLog10Test_bs8_in <= STD_LOGIC_VECTOR(peOR_uid36_fpLog10Test_b(16 downto 0));
    postPEMul_uid44_fpLog10Test_bs8_b <= STD_LOGIC_VECTOR(postPEMul_uid44_fpLog10Test_bs8_in(16 downto 0));

    -- redist20_postPEMul_uid44_fpLog10Test_bs8_b_1(DELAY,490)
    redist20_postPEMul_uid44_fpLog10Test_bs8_b_1 : dspba_delay
    GENERIC MAP ( width => 17, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_bs8_b, xout => redist20_postPEMul_uid44_fpLog10Test_bs8_b_1_q, clk => clk, aclr => areset );

    -- postPEMul_uid44_fpLog10Test_bjB9(BITJOIN,313)@9
    postPEMul_uid44_fpLog10Test_bjB9_q <= GND_q & redist20_postPEMul_uid44_fpLog10Test_bs8_b_1_q;

    -- postPEMul_uid44_fpLog10Test_im6(MULT,310)@9 + 2
    postPEMul_uid44_fpLog10Test_im6_a0 <= STD_LOGIC_VECTOR(postPEMul_uid44_fpLog10Test_bjB9_q);
    postPEMul_uid44_fpLog10Test_im6_b0 <= STD_LOGIC_VECTOR(redist21_postPEMul_uid44_fpLog10Test_bs7_b_1_q);
    postPEMul_uid44_fpLog10Test_im6_reset <= areset;
    postPEMul_uid44_fpLog10Test_im6_component : lpm_mult
    GENERIC MAP (
        lpm_widtha => 18,
        lpm_widthb => 8,
        lpm_widthp => 26,
        lpm_widths => 1,
        lpm_type => "LPM_MULT",
        lpm_representation => "SIGNED",
        lpm_hint => "DEDICATED_MULTIPLIER_CIRCUITRY=YES, MAXIMIZE_SPEED=5",
        lpm_pipeline => 2
    )
    PORT MAP (
        dataa => postPEMul_uid44_fpLog10Test_im6_a0,
        datab => postPEMul_uid44_fpLog10Test_im6_b0,
        clken => VCC_q(0),
        aclr => postPEMul_uid44_fpLog10Test_im6_reset,
        clock => clk,
        result => postPEMul_uid44_fpLog10Test_im6_s1
    );
    postPEMul_uid44_fpLog10Test_im6_q <= postPEMul_uid44_fpLog10Test_im6_s1;

    -- redist22_postPEMul_uid44_fpLog10Test_im6_q_1(DELAY,492)
    redist22_postPEMul_uid44_fpLog10Test_im6_q_1 : dspba_delay
    GENERIC MAP ( width => 26, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_im6_q, xout => redist22_postPEMul_uid44_fpLog10Test_im6_q_1_q, clk => clk, aclr => areset );

    -- postPEMul_uid44_fpLog10Test_align_17(BITSHIFT,321)@12
    postPEMul_uid44_fpLog10Test_align_17_qint <= redist22_postPEMul_uid44_fpLog10Test_im6_q_1_q & "00000000000000000";
    postPEMul_uid44_fpLog10Test_align_17_q <= postPEMul_uid44_fpLog10Test_align_17_qint(42 downto 0);

    -- postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_BitJoin_for_b(BITJOIN,449)@12
    postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_BitJoin_for_b_q <= postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel0_1_b & postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel0_1_b & postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel0_1_b & postPEMul_uid44_fpLog10Test_align_17_q;

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_UpperBits_for_b(CONSTANT,402)
    postPEMul_uid44_fpLog10Test_result_add_0_0_UpperBits_for_b_q <= "0000000000";

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_tessel0_1_merged_bit_select(BITSELECT,467)
    postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_tessel0_1_merged_bit_select_b <= STD_LOGIC_VECTOR(postPEMul_uid44_fpLog10Test_result_add_0_0_UpperBits_for_b_q(3 downto 0));
    postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_tessel0_1_merged_bit_select_c <= STD_LOGIC_VECTOR(postPEMul_uid44_fpLog10Test_result_add_0_0_UpperBits_for_b_q(9 downto 4));

    -- postPEMul_uid44_fpLog10Test_bs5(BITSELECT,309)@8
    postPEMul_uid44_fpLog10Test_bs5_b <= peOR_uid36_fpLog10Test_b(24 downto 17);

    -- postPEMul_uid44_fpLog10Test_bs1(BITSELECT,305)@8
    postPEMul_uid44_fpLog10Test_bs1_in <= multTermOne_uid43_fpLog10Test_q(16 downto 0);
    postPEMul_uid44_fpLog10Test_bs1_b <= postPEMul_uid44_fpLog10Test_bs1_in(16 downto 0);

    -- postPEMul_uid44_fpLog10Test_im3(MULT,307)@8 + 2
    postPEMul_uid44_fpLog10Test_im3_a0 <= postPEMul_uid44_fpLog10Test_bs1_b;
    postPEMul_uid44_fpLog10Test_im3_b0 <= postPEMul_uid44_fpLog10Test_bs5_b;
    postPEMul_uid44_fpLog10Test_im3_reset <= areset;
    postPEMul_uid44_fpLog10Test_im3_component : lpm_mult
    GENERIC MAP (
        lpm_widtha => 17,
        lpm_widthb => 8,
        lpm_widthp => 25,
        lpm_widths => 1,
        lpm_type => "LPM_MULT",
        lpm_representation => "UNSIGNED",
        lpm_hint => "DEDICATED_MULTIPLIER_CIRCUITRY=YES, MAXIMIZE_SPEED=5",
        lpm_pipeline => 2
    )
    PORT MAP (
        dataa => postPEMul_uid44_fpLog10Test_im3_a0,
        datab => postPEMul_uid44_fpLog10Test_im3_b0,
        clken => VCC_q(0),
        aclr => postPEMul_uid44_fpLog10Test_im3_reset,
        clock => clk,
        result => postPEMul_uid44_fpLog10Test_im3_s1
    );
    postPEMul_uid44_fpLog10Test_im3_q <= postPEMul_uid44_fpLog10Test_im3_s1;

    -- redist23_postPEMul_uid44_fpLog10Test_im3_q_1(DELAY,493)
    redist23_postPEMul_uid44_fpLog10Test_im3_q_1 : dspba_delay
    GENERIC MAP ( width => 25, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_im3_q, xout => redist23_postPEMul_uid44_fpLog10Test_im3_q_1_q, clk => clk, aclr => areset );

    -- postPEMul_uid44_fpLog10Test_align_15(BITSHIFT,319)@11
    postPEMul_uid44_fpLog10Test_align_15_qint <= redist23_postPEMul_uid44_fpLog10Test_im3_q_1_q & "00000000000000000";
    postPEMul_uid44_fpLog10Test_align_15_q <= postPEMul_uid44_fpLog10Test_align_15_qint(41 downto 0);

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_BitJoin_for_b(BITJOIN,437)@11
    postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_BitJoin_for_b_q <= postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_tessel0_1_merged_bit_select_b & postPEMul_uid44_fpLog10Test_align_15_q;

    -- postPEMul_uid44_fpLog10Test_bs12(BITSELECT,316)@8
    postPEMul_uid44_fpLog10Test_bs12_b <= STD_LOGIC_VECTOR(peOR_uid36_fpLog10Test_b(24 downto 17));

    -- postPEMul_uid44_fpLog10Test_bjB13(BITJOIN,317)@8
    postPEMul_uid44_fpLog10Test_bjB13_q <= GND_q & postPEMul_uid44_fpLog10Test_bs12_b;

    -- postPEMul_uid44_fpLog10Test_im10(MULT,314)@8 + 2
    postPEMul_uid44_fpLog10Test_im10_a0 <= STD_LOGIC_VECTOR(postPEMul_uid44_fpLog10Test_bs7_b);
    postPEMul_uid44_fpLog10Test_im10_b0 <= STD_LOGIC_VECTOR(postPEMul_uid44_fpLog10Test_bjB13_q);
    postPEMul_uid44_fpLog10Test_im10_reset <= areset;
    postPEMul_uid44_fpLog10Test_im10_component : lpm_mult
    GENERIC MAP (
        lpm_widtha => 8,
        lpm_widthb => 9,
        lpm_widthp => 17,
        lpm_widths => 1,
        lpm_type => "LPM_MULT",
        lpm_representation => "SIGNED",
        lpm_hint => "DEDICATED_MULTIPLIER_CIRCUITRY=YES, MAXIMIZE_SPEED=5",
        lpm_pipeline => 2
    )
    PORT MAP (
        dataa => postPEMul_uid44_fpLog10Test_im10_a0,
        datab => postPEMul_uid44_fpLog10Test_im10_b0,
        clken => VCC_q(0),
        aclr => postPEMul_uid44_fpLog10Test_im10_reset,
        clock => clk,
        result => postPEMul_uid44_fpLog10Test_im10_s1
    );
    postPEMul_uid44_fpLog10Test_im10_q <= postPEMul_uid44_fpLog10Test_im10_s1;

    -- redist19_postPEMul_uid44_fpLog10Test_im10_q_1(DELAY,489)
    redist19_postPEMul_uid44_fpLog10Test_im10_q_1 : dspba_delay
    GENERIC MAP ( width => 17, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_im10_q, xout => redist19_postPEMul_uid44_fpLog10Test_im10_q_1_q, clk => clk, aclr => areset );

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel0_1(BITSELECT,430)@11
    postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel0_1_b <= STD_LOGIC_VECTOR(redist19_postPEMul_uid44_fpLog10Test_im10_q_1_q(11 downto 0));

    -- postPEMul_uid44_fpLog10Test_bs2(BITSELECT,306)@8
    postPEMul_uid44_fpLog10Test_bs2_in <= peOR_uid36_fpLog10Test_b(16 downto 0);
    postPEMul_uid44_fpLog10Test_bs2_b <= postPEMul_uid44_fpLog10Test_bs2_in(16 downto 0);

    -- postPEMul_uid44_fpLog10Test_im0(MULT,304)@8 + 2
    postPEMul_uid44_fpLog10Test_im0_a0 <= postPEMul_uid44_fpLog10Test_bs1_b;
    postPEMul_uid44_fpLog10Test_im0_b0 <= postPEMul_uid44_fpLog10Test_bs2_b;
    postPEMul_uid44_fpLog10Test_im0_reset <= areset;
    postPEMul_uid44_fpLog10Test_im0_component : lpm_mult
    GENERIC MAP (
        lpm_widtha => 17,
        lpm_widthb => 17,
        lpm_widthp => 34,
        lpm_widths => 1,
        lpm_type => "LPM_MULT",
        lpm_representation => "UNSIGNED",
        lpm_hint => "DEDICATED_MULTIPLIER_CIRCUITRY=YES, MAXIMIZE_SPEED=5",
        lpm_pipeline => 2
    )
    PORT MAP (
        dataa => postPEMul_uid44_fpLog10Test_im0_a0,
        datab => postPEMul_uid44_fpLog10Test_im0_b0,
        clken => VCC_q(0),
        aclr => postPEMul_uid44_fpLog10Test_im0_reset,
        clock => clk,
        result => postPEMul_uid44_fpLog10Test_im0_s1
    );
    postPEMul_uid44_fpLog10Test_im0_q <= postPEMul_uid44_fpLog10Test_im0_s1;

    -- redist24_postPEMul_uid44_fpLog10Test_im0_q_1(DELAY,494)
    redist24_postPEMul_uid44_fpLog10Test_im0_q_1 : dspba_delay
    GENERIC MAP ( width => 34, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_im0_q, xout => redist24_postPEMul_uid44_fpLog10Test_im0_q_1_q, clk => clk, aclr => areset );

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_BitJoin_for_b(BITJOIN,431)@11
    postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_BitJoin_for_b_q <= postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel0_1_b & redist24_postPEMul_uid44_fpLog10Test_im0_q_1_q;

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2(ADD,405)@11 + 1
    postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_a <= STD_LOGIC_VECTOR("0" & postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_BitJoin_for_b_q);
    postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_b <= STD_LOGIC_VECTOR("0" & postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_BitJoin_for_b_q);
    postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_o <= STD_LOGIC_VECTOR(UNSIGNED(postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_a) + UNSIGNED(postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_b));
        END IF;
    END PROCESS;
    postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_c(0) <= postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_o(46);
    postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_q <= postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_o(45 downto 0);

    -- postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2(ADD,416)@12 + 1
    postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_a <= STD_LOGIC_VECTOR("0" & postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_q);
    postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_b <= STD_LOGIC_VECTOR("0" & postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_BitJoin_for_b_q);
    postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_o <= STD_LOGIC_VECTOR(UNSIGNED(postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_a) + UNSIGNED(postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_b));
        END IF;
    END PROCESS;
    postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_c(0) <= postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_o(46);
    postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_q <= postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_o(45 downto 0);

    -- redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1(DELAY,473)
    redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel0_1_b, xout => redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1_q, clk => clk, aclr => areset );

    -- postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_BitJoin_for_c(BITJOIN,457)@13
    postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_BitJoin_for_c_q <= redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1_q & redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1_q & redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1_q & redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1_q & redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1_q & redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1_q & redist3_postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_tessel1_6_b_1_q;

    -- postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_a_tessel1_1(BITSELECT,443)@13
    postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_a_tessel1_1_b <= STD_LOGIC_VECTOR(postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_q(5 downto 5));

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_1(BITSELECT,433)@11
    postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_1_b <= STD_LOGIC_VECTOR(redist19_postPEMul_uid44_fpLog10Test_im10_q_1_q(16 downto 16));

    -- redist10_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_1_b_1(DELAY,480)
    redist10_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_1_b_1 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_1_b, xout => redist10_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_1_b_1_q, clk => clk, aclr => areset );

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_0(BITSELECT,432)@11
    postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_0_b <= STD_LOGIC_VECTOR(redist19_postPEMul_uid44_fpLog10Test_im10_q_1_q(16 downto 12));

    -- redist11_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_0_b_1(DELAY,481)
    redist11_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_0_b_1 : dspba_delay
    GENERIC MAP ( width => 5, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_0_b, xout => redist11_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_0_b_1_q, clk => clk, aclr => areset );

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_BitJoin_for_c(BITJOIN,434)@12
    postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_BitJoin_for_c_q <= redist10_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_1_b_1_q & redist11_postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_tessel1_0_b_1_q;

    -- postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2(ADD,406)@12 + 1
    postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_cin <= postPEMul_uid44_fpLog10Test_result_add_0_0_p1_of_2_c;
    postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((6 downto 6 => postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_BitJoin_for_c_q(5)) & postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_a_BitJoin_for_c_q) & '1');
    postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR("0" & postPEMul_uid44_fpLog10Test_result_add_0_0_BitSelect_for_b_tessel0_1_merged_bit_select_c) & postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_cin(0));
    postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_o <= STD_LOGIC_VECTOR(SIGNED(postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_a) + SIGNED(postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_b));
        END IF;
    END PROCESS;
    postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_q <= postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_o(6 downto 1);

    -- postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_a_BitJoin_for_c(BITJOIN,444)@13
    postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_a_BitJoin_for_c_q <= postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_a_tessel1_1_b & postPEMul_uid44_fpLog10Test_result_add_0_0_p2_of_2_q;

    -- postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2(ADD,417)@13 + 1
    postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_cin <= postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_c;
    postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((7 downto 7 => postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_a_BitJoin_for_c_q(6)) & postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_a_BitJoin_for_c_q) & '1');
    postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((7 downto 7 => postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_BitJoin_for_c_q(6)) & postPEMul_uid44_fpLog10Test_result_add_1_0_BitSelect_for_b_BitJoin_for_c_q) & postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_cin(0));
    postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_o <= STD_LOGIC_VECTOR(SIGNED(postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_a) + SIGNED(postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_b));
        END IF;
    END PROCESS;
    postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_q <= postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_o(7 downto 1);

    -- redist13_postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_q_1(DELAY,483)
    redist13_postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_q_1 : dspba_delay
    GENERIC MAP ( width => 46, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_q, xout => redist13_postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_q_1_q, clk => clk, aclr => areset );

    -- postPEMul_uid44_fpLog10Test_result_add_1_0_BitJoin_for_q(BITJOIN,418)@14
    postPEMul_uid44_fpLog10Test_result_add_1_0_BitJoin_for_q_q <= postPEMul_uid44_fpLog10Test_result_add_1_0_p2_of_2_q & redist13_postPEMul_uid44_fpLog10Test_result_add_1_0_p1_of_2_q_1_q;

    -- highBBits_uid48_fpLog10Test(BITSELECT,47)@14
    highBBits_uid48_fpLog10Test_in <= STD_LOGIC_VECTOR(postPEMul_uid44_fpLog10Test_result_add_1_0_BitJoin_for_q_q(49 downto 0));
    highBBits_uid48_fpLog10Test_b <= STD_LOGIC_VECTOR(highBBits_uid48_fpLog10Test_in(49 downto 22));

    -- maxValInOutFormat_uid104_eLog102_uid29_fpLog10Test(CONSTANT,103)
    maxValInOutFormat_uid104_eLog102_uid29_fpLog10Test_q <= "01111111111111111111111111111111111";

    -- minValueInFormat_uid105_eLog102_uid29_fpLog10Test(CONSTANT,104)
    minValueInFormat_uid105_eLog102_uid29_fpLog10Test_q <= "10000000000000000000000000000000000";

    -- cstBias_uid9_fpLog10Test(CONSTANT,8)
    cstBias_uid9_fpLog10Test_q <= "01111111";

    -- redist58_expX_uid6_fpLog10Test_b_10(DELAY,528)
    redist58_expX_uid6_fpLog10Test_b_10 : dspba_delay
    GENERIC MAP ( width => 8, depth => 10, reset_kind => "ASYNC" )
    PORT MAP ( xin => expX_uid6_fpLog10Test_b, xout => redist58_expX_uid6_fpLog10Test_b_10_q, clk => clk, aclr => areset );

    -- e_uid28_fpLog10Test(SUB,27)@10
    e_uid28_fpLog10Test_a <= STD_LOGIC_VECTOR("0" & redist58_expX_uid6_fpLog10Test_b_10_q);
    e_uid28_fpLog10Test_b <= STD_LOGIC_VECTOR("0" & cstBias_uid9_fpLog10Test_q);
    e_uid28_fpLog10Test_o <= STD_LOGIC_VECTOR(UNSIGNED(e_uid28_fpLog10Test_a) - UNSIGNED(e_uid28_fpLog10Test_b));
    e_uid28_fpLog10Test_q <= e_uid28_fpLog10Test_o(8 downto 0);

    -- xv0_uid93_eLog102_uid29_fpLog10Test(BITSELECT,92)@10
    xv0_uid93_eLog102_uid29_fpLog10Test_in <= e_uid28_fpLog10Test_q(3 downto 0);
    xv0_uid93_eLog102_uid29_fpLog10Test_b <= xv0_uid93_eLog102_uid29_fpLog10Test_in(3 downto 0);

    -- redist38_xv0_uid93_eLog102_uid29_fpLog10Test_b_1(DELAY,508)
    redist38_xv0_uid93_eLog102_uid29_fpLog10Test_b_1 : dspba_delay
    GENERIC MAP ( width => 4, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => xv0_uid93_eLog102_uid29_fpLog10Test_b, xout => redist38_xv0_uid93_eLog102_uid29_fpLog10Test_b_1_q, clk => clk, aclr => areset );

    -- p0_uid98_eLog102_uid29_fpLog10Test(LOOKUP,97)@11 + 1
    p0_uid98_eLog102_uid29_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            p0_uid98_eLog102_uid29_fpLog10Test_q <= "000000000000000000000000000000000";
        ELSIF (clk'EVENT AND clk = '1') THEN
            CASE (redist38_xv0_uid93_eLog102_uid29_fpLog10Test_b_1_q) IS
                WHEN "0000" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "000000000000000000000000000000000";
                WHEN "0001" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "000010011010001000001001101010000";
                WHEN "0010" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "000100110100010000010011010100000";
                WHEN "0011" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "000111001110011000011100111110000";
                WHEN "0100" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "001001101000100000100110101000000";
                WHEN "0101" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "001100000010101000110000010010000";
                WHEN "0110" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "001110011100110000111001111100000";
                WHEN "0111" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "010000110110111001000011100110000";
                WHEN "1000" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "010011010001000001001101010000000";
                WHEN "1001" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "010101101011001001010110111010000";
                WHEN "1010" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "011000000101010001100000100100000";
                WHEN "1011" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "011010011111011001101010001110000";
                WHEN "1100" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "011100111001100001110011111000000";
                WHEN "1101" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "011111010011101001111101100010000";
                WHEN "1110" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "100001101101110010000111001100000";
                WHEN "1111" => p0_uid98_eLog102_uid29_fpLog10Test_q <= "100100000111111010010000110110000";
                WHEN OTHERS => -- unreachable
                               p0_uid98_eLog102_uid29_fpLog10Test_q <= (others => '-');
            END CASE;
        END IF;
    END PROCESS;

    -- xv1_uid94_eLog102_uid29_fpLog10Test(BITSELECT,93)@10
    xv1_uid94_eLog102_uid29_fpLog10Test_in <= e_uid28_fpLog10Test_q(7 downto 0);
    xv1_uid94_eLog102_uid29_fpLog10Test_b <= xv1_uid94_eLog102_uid29_fpLog10Test_in(7 downto 4);

    -- p1_uid97_eLog102_uid29_fpLog10Test(LOOKUP,96)@10 + 1
    p1_uid97_eLog102_uid29_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            p1_uid97_eLog102_uid29_fpLog10Test_q <= "0000000000000000000000000000000000000";
        ELSIF (clk'EVENT AND clk = '1') THEN
            CASE (xv1_uid94_eLog102_uid29_fpLog10Test_b) IS
                WHEN "0000" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0000000000000000000000000000000000000";
                WHEN "0001" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0000100110100010000010011010100000000";
                WHEN "0010" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0001001101000100000100110101000000000";
                WHEN "0011" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0001110011100110000111001111100000000";
                WHEN "0100" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0010011010001000001001101010000000000";
                WHEN "0101" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0011000000101010001100000100100000000";
                WHEN "0110" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0011100111001100001110011111000000000";
                WHEN "0111" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0100001101101110010000111001100000000";
                WHEN "1000" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0100110100010000010011010100000000000";
                WHEN "1001" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0101011010110010010101101110100000000";
                WHEN "1010" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0110000001010100011000001001000000000";
                WHEN "1011" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0110100111110110011010100011100000000";
                WHEN "1100" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0111001110011000011100111110000000000";
                WHEN "1101" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "0111110100111010011111011000100000000";
                WHEN "1110" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "1000011011011100100001110011000000000";
                WHEN "1111" => p1_uid97_eLog102_uid29_fpLog10Test_q <= "1001000001111110100100001101100000000";
                WHEN OTHERS => -- unreachable
                               p1_uid97_eLog102_uid29_fpLog10Test_q <= (others => '-');
            END CASE;
        END IF;
    END PROCESS;

    -- lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select(BITSELECT,460)@11
    lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_b <= p1_uid97_eLog102_uid29_fpLog10Test_q(0 downto 0);
    lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_c <= p1_uid97_eLog102_uid29_fpLog10Test_q(36 downto 1);

    -- xv2_uid95_eLog102_uid29_fpLog10Test(BITSELECT,94)@10
    xv2_uid95_eLog102_uid29_fpLog10Test_b <= STD_LOGIC_VECTOR(e_uid28_fpLog10Test_q(8 downto 8));

    -- redist37_xv2_uid95_eLog102_uid29_fpLog10Test_b_1(DELAY,507)
    redist37_xv2_uid95_eLog102_uid29_fpLog10Test_b_1 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => xv2_uid95_eLog102_uid29_fpLog10Test_b, xout => redist37_xv2_uid95_eLog102_uid29_fpLog10Test_b_1_q, clk => clk, aclr => areset );

    -- p2_uid96_eLog102_uid29_fpLog10Test(LOOKUP,95)@11
    p2_uid96_eLog102_uid29_fpLog10Test_combproc: PROCESS (redist37_xv2_uid95_eLog102_uid29_fpLog10Test_b_1_q)
    BEGIN
        -- Begin reserved scope level
        CASE (redist37_xv2_uid95_eLog102_uid29_fpLog10Test_b_1_q) IS
            WHEN "0" => p2_uid96_eLog102_uid29_fpLog10Test_q <= "00000000000000000000000000000000000010";
            WHEN "1" => p2_uid96_eLog102_uid29_fpLog10Test_q <= "11011001011101111101100101100000000010";
            WHEN OTHERS => -- unreachable
                           p2_uid96_eLog102_uid29_fpLog10Test_q <= (others => '-');
        END CASE;
        -- End reserved scope level
    END PROCESS;

    -- lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test(ADD,100)@11 + 1
    lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((39 downto 38 => p2_uid96_eLog102_uid29_fpLog10Test_q(37)) & p2_uid96_eLog102_uid29_fpLog10Test_q));
    lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR("0" & "000" & lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_c));
    lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_o <= STD_LOGIC_VECTOR(SIGNED(lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_a) + SIGNED(lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_b));
        END IF;
    END PROCESS;
    lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_q <= lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_o(38 downto 0);

    -- redist1_lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_b_1(DELAY,471)
    redist1_lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_b_1 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_b, xout => redist1_lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_b_1_q, clk => clk, aclr => areset );

    -- lev1_a0_uid102_eLog102_uid29_fpLog10Test(BITJOIN,101)@12
    lev1_a0_uid102_eLog102_uid29_fpLog10Test_q <= lev1_a0sumAHighB_uid101_eLog102_uid29_fpLog10Test_q & redist1_lowRangeB_uid99_eLog102_uid29_fpLog10Test_merged_bit_select_b_1_q;

    -- lev2_a0_uid103_eLog102_uid29_fpLog10Test(ADD,102)@12 + 1
    lev2_a0_uid103_eLog102_uid29_fpLog10Test_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((41 downto 40 => lev1_a0_uid102_eLog102_uid29_fpLog10Test_q(39)) & lev1_a0_uid102_eLog102_uid29_fpLog10Test_q));
    lev2_a0_uid103_eLog102_uid29_fpLog10Test_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR("0" & "00000000" & p0_uid98_eLog102_uid29_fpLog10Test_q));
    lev2_a0_uid103_eLog102_uid29_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            lev2_a0_uid103_eLog102_uid29_fpLog10Test_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            lev2_a0_uid103_eLog102_uid29_fpLog10Test_o <= STD_LOGIC_VECTOR(SIGNED(lev2_a0_uid103_eLog102_uid29_fpLog10Test_a) + SIGNED(lev2_a0_uid103_eLog102_uid29_fpLog10Test_b));
        END IF;
    END PROCESS;
    lev2_a0_uid103_eLog102_uid29_fpLog10Test_q <= lev2_a0_uid103_eLog102_uid29_fpLog10Test_o(40 downto 0);

    -- sR_uid113_eLog102_uid29_fpLog10Test(BITSELECT,112)@13
    sR_uid113_eLog102_uid29_fpLog10Test_in <= STD_LOGIC_VECTOR(lev2_a0_uid103_eLog102_uid29_fpLog10Test_q(37 downto 0));
    sR_uid113_eLog102_uid29_fpLog10Test_b <= STD_LOGIC_VECTOR(sR_uid113_eLog102_uid29_fpLog10Test_in(37 downto 3));

    -- redist36_sR_uid113_eLog102_uid29_fpLog10Test_b_1(DELAY,506)
    redist36_sR_uid113_eLog102_uid29_fpLog10Test_b_1 : dspba_delay
    GENERIC MAP ( width => 35, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => sR_uid113_eLog102_uid29_fpLog10Test_b, xout => redist36_sR_uid113_eLog102_uid29_fpLog10Test_b_1_q, clk => clk, aclr => areset );

    -- updatedX_uid107_eLog102_uid29_fpLog10Test(BITJOIN,106)@13
    updatedX_uid107_eLog102_uid29_fpLog10Test_q <= maxValInOutFormat_uid104_eLog102_uid29_fpLog10Test_q & paddingX_uid106_eLog102_uid29_fpLog10Test_q;

    -- ovf_uid106_eLog102_uid29_fpLog10Test(COMPARE,107)@13 + 1
    ovf_uid106_eLog102_uid29_fpLog10Test_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((42 downto 38 => updatedX_uid107_eLog102_uid29_fpLog10Test_q(37)) & updatedX_uid107_eLog102_uid29_fpLog10Test_q));
    ovf_uid106_eLog102_uid29_fpLog10Test_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((42 downto 41 => lev2_a0_uid103_eLog102_uid29_fpLog10Test_q(40)) & lev2_a0_uid103_eLog102_uid29_fpLog10Test_q));
    ovf_uid106_eLog102_uid29_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            ovf_uid106_eLog102_uid29_fpLog10Test_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            ovf_uid106_eLog102_uid29_fpLog10Test_o <= STD_LOGIC_VECTOR(SIGNED(ovf_uid106_eLog102_uid29_fpLog10Test_a) - SIGNED(ovf_uid106_eLog102_uid29_fpLog10Test_b));
        END IF;
    END PROCESS;
    ovf_uid106_eLog102_uid29_fpLog10Test_c(0) <= ovf_uid106_eLog102_uid29_fpLog10Test_o(42);

    -- updatedY_uid110_eLog102_uid29_fpLog10Test(BITJOIN,109)@13
    updatedY_uid110_eLog102_uid29_fpLog10Test_q <= minValueInFormat_uid105_eLog102_uid29_fpLog10Test_q & paddingX_uid106_eLog102_uid29_fpLog10Test_q;

    -- udf_uid109_eLog102_uid29_fpLog10Test(COMPARE,110)@13 + 1
    udf_uid109_eLog102_uid29_fpLog10Test_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((42 downto 41 => lev2_a0_uid103_eLog102_uid29_fpLog10Test_q(40)) & lev2_a0_uid103_eLog102_uid29_fpLog10Test_q));
    udf_uid109_eLog102_uid29_fpLog10Test_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((42 downto 38 => updatedY_uid110_eLog102_uid29_fpLog10Test_q(37)) & updatedY_uid110_eLog102_uid29_fpLog10Test_q));
    udf_uid109_eLog102_uid29_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            udf_uid109_eLog102_uid29_fpLog10Test_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            udf_uid109_eLog102_uid29_fpLog10Test_o <= STD_LOGIC_VECTOR(SIGNED(udf_uid109_eLog102_uid29_fpLog10Test_a) - SIGNED(udf_uid109_eLog102_uid29_fpLog10Test_b));
        END IF;
    END PROCESS;
    udf_uid109_eLog102_uid29_fpLog10Test_c(0) <= udf_uid109_eLog102_uid29_fpLog10Test_o(42);

    -- ovfudfcond_uid112_eLog102_uid29_fpLog10Test(BITJOIN,111)@14
    ovfudfcond_uid112_eLog102_uid29_fpLog10Test_q <= ovf_uid106_eLog102_uid29_fpLog10Test_c & udf_uid109_eLog102_uid29_fpLog10Test_c;

    -- sRA0_uid114_eLog102_uid29_fpLog10Test(MUX,113)@14
    sRA0_uid114_eLog102_uid29_fpLog10Test_s <= ovfudfcond_uid112_eLog102_uid29_fpLog10Test_q;
    sRA0_uid114_eLog102_uid29_fpLog10Test_combproc: PROCESS (sRA0_uid114_eLog102_uid29_fpLog10Test_s, redist36_sR_uid113_eLog102_uid29_fpLog10Test_b_1_q, minValueInFormat_uid105_eLog102_uid29_fpLog10Test_q, maxValInOutFormat_uid104_eLog102_uid29_fpLog10Test_q)
    BEGIN
        CASE (sRA0_uid114_eLog102_uid29_fpLog10Test_s) IS
            WHEN "00" => sRA0_uid114_eLog102_uid29_fpLog10Test_q <= redist36_sR_uid113_eLog102_uid29_fpLog10Test_b_1_q;
            WHEN "01" => sRA0_uid114_eLog102_uid29_fpLog10Test_q <= minValueInFormat_uid105_eLog102_uid29_fpLog10Test_q;
            WHEN "10" => sRA0_uid114_eLog102_uid29_fpLog10Test_q <= maxValInOutFormat_uid104_eLog102_uid29_fpLog10Test_q;
            WHEN "11" => sRA0_uid114_eLog102_uid29_fpLog10Test_q <= maxValInOutFormat_uid104_eLog102_uid29_fpLog10Test_q;
            WHEN OTHERS => sRA0_uid114_eLog102_uid29_fpLog10Test_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- finalSumsumAHighB_uid49_fpLog10Test(ADD,48)@14 + 1
    finalSumsumAHighB_uid49_fpLog10Test_a <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((35 downto 35 => sRA0_uid114_eLog102_uid29_fpLog10Test_q(34)) & sRA0_uid114_eLog102_uid29_fpLog10Test_q));
    finalSumsumAHighB_uid49_fpLog10Test_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((35 downto 28 => highBBits_uid48_fpLog10Test_b(27)) & highBBits_uid48_fpLog10Test_b));
    finalSumsumAHighB_uid49_fpLog10Test_i <= finalSumsumAHighB_uid49_fpLog10Test_b;
    finalSumsumAHighB_uid49_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            finalSumsumAHighB_uid49_fpLog10Test_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            IF (redist50_c_uid30_fpLog10Test_q_14_q = "1") THEN
                finalSumsumAHighB_uid49_fpLog10Test_o <= finalSumsumAHighB_uid49_fpLog10Test_i;
            ELSE
                finalSumsumAHighB_uid49_fpLog10Test_o <= STD_LOGIC_VECTOR(SIGNED(finalSumsumAHighB_uid49_fpLog10Test_a) + SIGNED(finalSumsumAHighB_uid49_fpLog10Test_b));
            END IF;
        END IF;
    END PROCESS;
    finalSumsumAHighB_uid49_fpLog10Test_q <= finalSumsumAHighB_uid49_fpLog10Test_o(35 downto 0);

    -- lowRangeB_uid47_fpLog10Test(BITSELECT,46)@14
    lowRangeB_uid47_fpLog10Test_in <= postPEMul_uid44_fpLog10Test_result_add_1_0_BitJoin_for_q_q(21 downto 0);
    lowRangeB_uid47_fpLog10Test_b <= lowRangeB_uid47_fpLog10Test_in(21 downto 0);

    -- redist45_lowRangeB_uid47_fpLog10Test_b_1(DELAY,515)
    redist45_lowRangeB_uid47_fpLog10Test_b_1 : dspba_delay
    GENERIC MAP ( width => 22, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => lowRangeB_uid47_fpLog10Test_b, xout => redist45_lowRangeB_uid47_fpLog10Test_b_1_q, clk => clk, aclr => areset );

    -- finalSum_uid50_fpLog10Test(BITJOIN,49)@15
    finalSum_uid50_fpLog10Test_q <= finalSumsumAHighB_uid49_fpLog10Test_q & redist45_lowRangeB_uid47_fpLog10Test_b_1_q;

    -- msbUFinalSum_uid51_fpLog10Test(BITSELECT,50)@15
    msbUFinalSum_uid51_fpLog10Test_b <= STD_LOGIC_VECTOR(finalSum_uid50_fpLog10Test_q(57 downto 57));

    -- notC_uid74_fpLog10Test(LOGICAL,73)@15
    notC_uid74_fpLog10Test_q <= not (redist51_c_uid30_fpLog10Test_q_15_q);

    -- signTerm2_uid75_fpLog10Test(LOGICAL,74)@15
    signTerm2_uid75_fpLog10Test_q <= notC_uid74_fpLog10Test_q and msbUFinalSum_uid51_fpLog10Test_b;

    -- redist51_c_uid30_fpLog10Test_q_15(DELAY,521)
    redist51_c_uid30_fpLog10Test_q_15 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist50_c_uid30_fpLog10Test_q_14_q, xout => redist51_c_uid30_fpLog10Test_q_15_q, clk => clk, aclr => areset );

    -- signRC1_uid76_fpLog10Test(LOGICAL,75)@15
    signRC1_uid76_fpLog10Test_q <= redist51_c_uid30_fpLog10Test_q_15_q or signTerm2_uid75_fpLog10Test_q;

    -- cstAllOWE_uid12_fpLog10Test(CONSTANT,11)
    cstAllOWE_uid12_fpLog10Test_q <= "11111111";

    -- expXIsMax_uid18_fpLog10Test(LOGICAL,17)@10
    expXIsMax_uid18_fpLog10Test_q <= "1" WHEN redist58_expX_uid6_fpLog10Test_b_10_q = cstAllOWE_uid12_fpLog10Test_q ELSE "0";

    -- invExpXIsMax_uid23_fpLog10Test(LOGICAL,22)@10
    invExpXIsMax_uid23_fpLog10Test_q <= not (expXIsMax_uid18_fpLog10Test_q);

    -- cstAllZWE_uid14_fpLog10Test(CONSTANT,13)
    cstAllZWE_uid14_fpLog10Test_q <= "00000000";

    -- excZ_x_uid17_fpLog10Test(LOGICAL,16)@10
    excZ_x_uid17_fpLog10Test_q <= "1" WHEN redist58_expX_uid6_fpLog10Test_b_10_q = cstAllZWE_uid14_fpLog10Test_q ELSE "0";

    -- InvExpXIsZero_uid24_fpLog10Test(LOGICAL,23)@10
    InvExpXIsZero_uid24_fpLog10Test_q <= not (excZ_x_uid17_fpLog10Test_q);

    -- excR_x_uid25_fpLog10Test(LOGICAL,24)@10 + 1
    excR_x_uid25_fpLog10Test_qi <= InvExpXIsZero_uid24_fpLog10Test_q and invExpXIsMax_uid23_fpLog10Test_q;
    excR_x_uid25_fpLog10Test_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excR_x_uid25_fpLog10Test_qi, xout => excR_x_uid25_fpLog10Test_q, clk => clk, aclr => areset );

    -- redist52_excR_x_uid25_fpLog10Test_q_5(DELAY,522)
    redist52_excR_x_uid25_fpLog10Test_q_5 : dspba_delay
    GENERIC MAP ( width => 1, depth => 4, reset_kind => "ASYNC" )
    PORT MAP ( xin => excR_x_uid25_fpLog10Test_q, xout => redist52_excR_x_uid25_fpLog10Test_q_5_q, clk => clk, aclr => areset );

    -- signRC11_uid77_fpLog10Test(LOGICAL,76)@15
    signRC11_uid77_fpLog10Test_q <= redist52_excR_x_uid25_fpLog10Test_q_5_q and signRC1_uid76_fpLog10Test_q;

    -- redist54_excZ_x_uid17_fpLog10Test_q_5(DELAY,524)
    redist54_excZ_x_uid17_fpLog10Test_q_5 : dspba_delay
    GENERIC MAP ( width => 1, depth => 5, reset_kind => "ASYNC" )
    PORT MAP ( xin => excZ_x_uid17_fpLog10Test_q, xout => redist54_excZ_x_uid17_fpLog10Test_q_5_q, clk => clk, aclr => areset );

    -- signR_uid78_fpLog10Test(LOGICAL,77)@15
    signR_uid78_fpLog10Test_q <= redist54_excZ_x_uid17_fpLog10Test_q_5_q or signRC11_uid77_fpLog10Test_q;

    -- fracXIsZero_uid19_fpLog10Test(LOGICAL,18)@8 + 1
    fracXIsZero_uid19_fpLog10Test_qi <= "1" WHEN cstAllZWF_uid8_fpLog10Test_q = redist56_frac_x_uid16_fpLog10Test_b_8_q ELSE "0";
    fracXIsZero_uid19_fpLog10Test_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracXIsZero_uid19_fpLog10Test_qi, xout => fracXIsZero_uid19_fpLog10Test_q, clk => clk, aclr => areset );

    -- redist53_fracXIsZero_uid19_fpLog10Test_q_2(DELAY,523)
    redist53_fracXIsZero_uid19_fpLog10Test_q_2 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracXIsZero_uid19_fpLog10Test_q, xout => redist53_fracXIsZero_uid19_fpLog10Test_q_2_q, clk => clk, aclr => areset );

    -- fracXIsNotZero_uid20_fpLog10Test(LOGICAL,19)@10
    fracXIsNotZero_uid20_fpLog10Test_q <= not (redist53_fracXIsZero_uid19_fpLog10Test_q_2_q);

    -- excN_x_uid22_fpLog10Test(LOGICAL,21)@10
    excN_x_uid22_fpLog10Test_q <= expXIsMax_uid18_fpLog10Test_q and fracXIsNotZero_uid20_fpLog10Test_q;

    -- signX_uid7_fpLog10Test(BITSELECT,6)@0
    signX_uid7_fpLog10Test_b <= STD_LOGIC_VECTOR(a(31 downto 31));

    -- redist57_signX_uid7_fpLog10Test_b_10(DELAY,527)
    redist57_signX_uid7_fpLog10Test_b_10 : dspba_delay
    GENERIC MAP ( width => 1, depth => 10, reset_kind => "ASYNC" )
    PORT MAP ( xin => signX_uid7_fpLog10Test_b, xout => redist57_signX_uid7_fpLog10Test_b_10_q, clk => clk, aclr => areset );

    -- negNonZero_uid72_fpLog10Test(LOGICAL,71)@10
    negNonZero_uid72_fpLog10Test_q <= InvExpXIsZero_uid24_fpLog10Test_q and redist57_signX_uid7_fpLog10Test_b_10_q;

    -- excRNaN_uid73_fpLog10Test(LOGICAL,72)@10 + 1
    excRNaN_uid73_fpLog10Test_qi <= negNonZero_uid72_fpLog10Test_q or excN_x_uid22_fpLog10Test_q;
    excRNaN_uid73_fpLog10Test_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excRNaN_uid73_fpLog10Test_qi, xout => excRNaN_uid73_fpLog10Test_q, clk => clk, aclr => areset );

    -- redist41_excRNaN_uid73_fpLog10Test_q_5(DELAY,511)
    redist41_excRNaN_uid73_fpLog10Test_q_5 : dspba_delay
    GENERIC MAP ( width => 1, depth => 4, reset_kind => "ASYNC" )
    PORT MAP ( xin => excRNaN_uid73_fpLog10Test_q, xout => redist41_excRNaN_uid73_fpLog10Test_q_5_q, clk => clk, aclr => areset );

    -- invExcRNaN_uid79_fpLog10Test(LOGICAL,78)@15
    invExcRNaN_uid79_fpLog10Test_q <= not (redist41_excRNaN_uid73_fpLog10Test_q_5_q);

    -- signRFull_uid80_fpLog10Test(LOGICAL,79)@15 + 1
    signRFull_uid80_fpLog10Test_qi <= invExcRNaN_uid79_fpLog10Test_q and signR_uid78_fpLog10Test_q;
    signRFull_uid80_fpLog10Test_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => signRFull_uid80_fpLog10Test_qi, xout => signRFull_uid80_fpLog10Test_q, clk => clk, aclr => areset );

    -- redist40_signRFull_uid80_fpLog10Test_q_8(DELAY,510)
    redist40_signRFull_uid80_fpLog10Test_q_8 : dspba_delay
    GENERIC MAP ( width => 1, depth => 7, reset_kind => "ASYNC" )
    PORT MAP ( xin => signRFull_uid80_fpLog10Test_q, xout => redist40_signRFull_uid80_fpLog10Test_q_8_q, clk => clk, aclr => areset );

    -- zs_uid167_countZ_uid55_fpLog10Test(CONSTANT,166)
    zs_uid167_countZ_uid55_fpLog10Test_q <= "00000000000000000000000000000000";

    -- finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_0(BITSELECT,424)@15
    finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_0_b <= STD_LOGIC_VECTOR(finalSumsumAHighB_uid49_fpLog10Test_q(35 downto 35));

    -- redist12_finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_0_b_1(DELAY,482)
    redist12_finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_0_b_1 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_0_b, xout => redist12_finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_0_b_1_q, clk => clk, aclr => areset );

    -- finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_BitJoin_for_b(BITJOIN,426)@16
    finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_BitJoin_for_b_q <= finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_1_merged_bit_select_b & redist12_finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_0_b_1_q;

    -- finalSumOneComp_uid53_fpLog10Test(LOGICAL,52)@15 + 1
    finalSumOneComp_uid53_fpLog10Test_b <= STD_LOGIC_VECTOR(STD_LOGIC_VECTOR((57 downto 1 => msbUFinalSum_uid51_fpLog10Test_b(0)) & msbUFinalSum_uid51_fpLog10Test_b));
    finalSumOneComp_uid53_fpLog10Test_qi <= finalSum_uid50_fpLog10Test_q xor finalSumOneComp_uid53_fpLog10Test_b;
    finalSumOneComp_uid53_fpLog10Test_delay : dspba_delay
    GENERIC MAP ( width => 58, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => finalSumOneComp_uid53_fpLog10Test_qi, xout => finalSumOneComp_uid53_fpLog10Test_q, clk => clk, aclr => areset );

    -- finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select(BITSELECT,458)@16
    finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_b <= STD_LOGIC_VECTOR(finalSumOneComp_uid53_fpLog10Test_q(45 downto 0));
    finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_c <= STD_LOGIC_VECTOR(finalSumOneComp_uid53_fpLog10Test_q(57 downto 46));

    -- finalSumAbs_uid54_fpLog10Test_p1_of_2(ADD,395)@16 + 1
    finalSumAbs_uid54_fpLog10Test_p1_of_2_a <= STD_LOGIC_VECTOR("0" & finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_b);
    finalSumAbs_uid54_fpLog10Test_p1_of_2_b <= STD_LOGIC_VECTOR("0" & finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_BitJoin_for_b_q);
    finalSumAbs_uid54_fpLog10Test_p1_of_2_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            finalSumAbs_uid54_fpLog10Test_p1_of_2_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            finalSumAbs_uid54_fpLog10Test_p1_of_2_o <= STD_LOGIC_VECTOR(UNSIGNED(finalSumAbs_uid54_fpLog10Test_p1_of_2_a) + UNSIGNED(finalSumAbs_uid54_fpLog10Test_p1_of_2_b));
        END IF;
    END PROCESS;
    finalSumAbs_uid54_fpLog10Test_p1_of_2_c(0) <= finalSumAbs_uid54_fpLog10Test_p1_of_2_o(46);
    finalSumAbs_uid54_fpLog10Test_p1_of_2_q <= finalSumAbs_uid54_fpLog10Test_p1_of_2_o(45 downto 0);

    -- finalSumAbs_uid54_fpLog10Test_UpperBits_for_b(CONSTANT,392)
    finalSumAbs_uid54_fpLog10Test_UpperBits_for_b_q <= "0000000000000000000000000000000000000000000000000000000000";

    -- finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_1_merged_bit_select(BITSELECT,466)
    finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_1_merged_bit_select_b <= STD_LOGIC_VECTOR(finalSumAbs_uid54_fpLog10Test_UpperBits_for_b_q(44 downto 0));
    finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_1_merged_bit_select_c <= STD_LOGIC_VECTOR(finalSumAbs_uid54_fpLog10Test_UpperBits_for_b_q(57 downto 45));

    -- redist2_finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_c_1(DELAY,472)
    redist2_finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_c_1 : dspba_delay
    GENERIC MAP ( width => 12, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_c, xout => redist2_finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_c_1_q, clk => clk, aclr => areset );

    -- finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_BitJoin_for_c(BITJOIN,423)@17
    finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_BitJoin_for_c_q <= GND_q & redist2_finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_tessel0_0_merged_bit_select_c_1_q;

    -- finalSumAbs_uid54_fpLog10Test_p2_of_2(ADD,396)@17 + 1
    finalSumAbs_uid54_fpLog10Test_p2_of_2_cin <= finalSumAbs_uid54_fpLog10Test_p1_of_2_c;
    finalSumAbs_uid54_fpLog10Test_p2_of_2_a <= STD_LOGIC_VECTOR("0" & finalSumAbs_uid54_fpLog10Test_BitSelect_for_a_BitJoin_for_c_q) & '1';
    finalSumAbs_uid54_fpLog10Test_p2_of_2_b <= STD_LOGIC_VECTOR("0" & finalSumAbs_uid54_fpLog10Test_BitSelect_for_b_tessel0_1_merged_bit_select_c) & finalSumAbs_uid54_fpLog10Test_p2_of_2_cin(0);
    finalSumAbs_uid54_fpLog10Test_p2_of_2_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            finalSumAbs_uid54_fpLog10Test_p2_of_2_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            finalSumAbs_uid54_fpLog10Test_p2_of_2_o <= STD_LOGIC_VECTOR(UNSIGNED(finalSumAbs_uid54_fpLog10Test_p2_of_2_a) + UNSIGNED(finalSumAbs_uid54_fpLog10Test_p2_of_2_b));
        END IF;
    END PROCESS;
    finalSumAbs_uid54_fpLog10Test_p2_of_2_q <= finalSumAbs_uid54_fpLog10Test_p2_of_2_o(13 downto 1);

    -- redist16_finalSumAbs_uid54_fpLog10Test_p1_of_2_q_1(DELAY,486)
    redist16_finalSumAbs_uid54_fpLog10Test_p1_of_2_q_1 : dspba_delay
    GENERIC MAP ( width => 46, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => finalSumAbs_uid54_fpLog10Test_p1_of_2_q, xout => redist16_finalSumAbs_uid54_fpLog10Test_p1_of_2_q_1_q, clk => clk, aclr => areset );

    -- finalSumAbs_uid54_fpLog10Test_BitJoin_for_q(BITJOIN,397)@18
    finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q <= finalSumAbs_uid54_fpLog10Test_p2_of_2_q & redist16_finalSumAbs_uid54_fpLog10Test_p1_of_2_q_1_q;

    -- rVStage_uid168_countZ_uid55_fpLog10Test(BITSELECT,167)@18
    rVStage_uid168_countZ_uid55_fpLog10Test_b <= finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q(58 downto 27);

    -- vCount_uid169_countZ_uid55_fpLog10Test(LOGICAL,168)@18 + 1
    vCount_uid169_countZ_uid55_fpLog10Test_qi <= "1" WHEN rVStage_uid168_countZ_uid55_fpLog10Test_b = zs_uid167_countZ_uid55_fpLog10Test_q ELSE "0";
    vCount_uid169_countZ_uid55_fpLog10Test_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => vCount_uid169_countZ_uid55_fpLog10Test_qi, xout => vCount_uid169_countZ_uid55_fpLog10Test_q, clk => clk, aclr => areset );

    -- redist34_vCount_uid169_countZ_uid55_fpLog10Test_q_3(DELAY,504)
    redist34_vCount_uid169_countZ_uid55_fpLog10Test_q_3 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => vCount_uid169_countZ_uid55_fpLog10Test_q, xout => redist34_vCount_uid169_countZ_uid55_fpLog10Test_q_3_q, clk => clk, aclr => areset );

    -- zs_uid175_countZ_uid55_fpLog10Test(CONSTANT,174)
    zs_uid175_countZ_uid55_fpLog10Test_q <= "0000000000000000";

    -- redist14_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_1(DELAY,484)
    redist14_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_1 : dspba_delay
    GENERIC MAP ( width => 59, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q, xout => redist14_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_1_q, clk => clk, aclr => areset );

    -- vStage_uid171_countZ_uid55_fpLog10Test(BITSELECT,170)@19
    vStage_uid171_countZ_uid55_fpLog10Test_in <= redist14_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_1_q(26 downto 0);
    vStage_uid171_countZ_uid55_fpLog10Test_b <= vStage_uid171_countZ_uid55_fpLog10Test_in(26 downto 0);

    -- mO_uid170_countZ_uid55_fpLog10Test(CONSTANT,169)
    mO_uid170_countZ_uid55_fpLog10Test_q <= "11111";

    -- cStage_uid172_countZ_uid55_fpLog10Test(BITJOIN,171)@19
    cStage_uid172_countZ_uid55_fpLog10Test_q <= vStage_uid171_countZ_uid55_fpLog10Test_b & mO_uid170_countZ_uid55_fpLog10Test_q;

    -- redist35_rVStage_uid168_countZ_uid55_fpLog10Test_b_1(DELAY,505)
    redist35_rVStage_uid168_countZ_uid55_fpLog10Test_b_1 : dspba_delay
    GENERIC MAP ( width => 32, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => rVStage_uid168_countZ_uid55_fpLog10Test_b, xout => redist35_rVStage_uid168_countZ_uid55_fpLog10Test_b_1_q, clk => clk, aclr => areset );

    -- vStagei_uid174_countZ_uid55_fpLog10Test(MUX,173)@19
    vStagei_uid174_countZ_uid55_fpLog10Test_s <= vCount_uid169_countZ_uid55_fpLog10Test_q;
    vStagei_uid174_countZ_uid55_fpLog10Test_combproc: PROCESS (vStagei_uid174_countZ_uid55_fpLog10Test_s, redist35_rVStage_uid168_countZ_uid55_fpLog10Test_b_1_q, cStage_uid172_countZ_uid55_fpLog10Test_q)
    BEGIN
        CASE (vStagei_uid174_countZ_uid55_fpLog10Test_s) IS
            WHEN "0" => vStagei_uid174_countZ_uid55_fpLog10Test_q <= redist35_rVStage_uid168_countZ_uid55_fpLog10Test_b_1_q;
            WHEN "1" => vStagei_uid174_countZ_uid55_fpLog10Test_q <= cStage_uid172_countZ_uid55_fpLog10Test_q;
            WHEN OTHERS => vStagei_uid174_countZ_uid55_fpLog10Test_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- rVStage_uid176_countZ_uid55_fpLog10Test_merged_bit_select(BITSELECT,461)@19
    rVStage_uid176_countZ_uid55_fpLog10Test_merged_bit_select_b <= vStagei_uid174_countZ_uid55_fpLog10Test_q(31 downto 16);
    rVStage_uid176_countZ_uid55_fpLog10Test_merged_bit_select_c <= vStagei_uid174_countZ_uid55_fpLog10Test_q(15 downto 0);

    -- vCount_uid177_countZ_uid55_fpLog10Test(LOGICAL,176)@19
    vCount_uid177_countZ_uid55_fpLog10Test_q <= "1" WHEN rVStage_uid176_countZ_uid55_fpLog10Test_merged_bit_select_b = zs_uid175_countZ_uid55_fpLog10Test_q ELSE "0";

    -- redist32_vCount_uid177_countZ_uid55_fpLog10Test_q_2(DELAY,502)
    redist32_vCount_uid177_countZ_uid55_fpLog10Test_q_2 : dspba_delay
    GENERIC MAP ( width => 1, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => vCount_uid177_countZ_uid55_fpLog10Test_q, xout => redist32_vCount_uid177_countZ_uid55_fpLog10Test_q_2_q, clk => clk, aclr => areset );

    -- vStagei_uid180_countZ_uid55_fpLog10Test(MUX,179)@19 + 1
    vStagei_uid180_countZ_uid55_fpLog10Test_s <= vCount_uid177_countZ_uid55_fpLog10Test_q;
    vStagei_uid180_countZ_uid55_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            vStagei_uid180_countZ_uid55_fpLog10Test_q <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            CASE (vStagei_uid180_countZ_uid55_fpLog10Test_s) IS
                WHEN "0" => vStagei_uid180_countZ_uid55_fpLog10Test_q <= rVStage_uid176_countZ_uid55_fpLog10Test_merged_bit_select_b;
                WHEN "1" => vStagei_uid180_countZ_uid55_fpLog10Test_q <= rVStage_uid176_countZ_uid55_fpLog10Test_merged_bit_select_c;
                WHEN OTHERS => vStagei_uid180_countZ_uid55_fpLog10Test_q <= (others => '0');
            END CASE;
        END IF;
    END PROCESS;

    -- rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select(BITSELECT,462)@20
    rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select_b <= vStagei_uid180_countZ_uid55_fpLog10Test_q(15 downto 8);
    rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select_c <= vStagei_uid180_countZ_uid55_fpLog10Test_q(7 downto 0);

    -- vCount_uid183_countZ_uid55_fpLog10Test(LOGICAL,182)@20
    vCount_uid183_countZ_uid55_fpLog10Test_q <= "1" WHEN rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select_b = cstAllZWE_uid14_fpLog10Test_q ELSE "0";

    -- redist31_vCount_uid183_countZ_uid55_fpLog10Test_q_1(DELAY,501)
    redist31_vCount_uid183_countZ_uid55_fpLog10Test_q_1 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => vCount_uid183_countZ_uid55_fpLog10Test_q, xout => redist31_vCount_uid183_countZ_uid55_fpLog10Test_q_1_q, clk => clk, aclr => areset );

    -- vStagei_uid186_countZ_uid55_fpLog10Test(MUX,185)@20
    vStagei_uid186_countZ_uid55_fpLog10Test_s <= vCount_uid183_countZ_uid55_fpLog10Test_q;
    vStagei_uid186_countZ_uid55_fpLog10Test_combproc: PROCESS (vStagei_uid186_countZ_uid55_fpLog10Test_s, rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select_b, rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select_c)
    BEGIN
        CASE (vStagei_uid186_countZ_uid55_fpLog10Test_s) IS
            WHEN "0" => vStagei_uid186_countZ_uid55_fpLog10Test_q <= rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select_b;
            WHEN "1" => vStagei_uid186_countZ_uid55_fpLog10Test_q <= rVStage_uid182_countZ_uid55_fpLog10Test_merged_bit_select_c;
            WHEN OTHERS => vStagei_uid186_countZ_uid55_fpLog10Test_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- rVStage_uid188_countZ_uid55_fpLog10Test_merged_bit_select(BITSELECT,463)@20
    rVStage_uid188_countZ_uid55_fpLog10Test_merged_bit_select_b <= vStagei_uid186_countZ_uid55_fpLog10Test_q(7 downto 4);
    rVStage_uid188_countZ_uid55_fpLog10Test_merged_bit_select_c <= vStagei_uid186_countZ_uid55_fpLog10Test_q(3 downto 0);

    -- vCount_uid189_countZ_uid55_fpLog10Test(LOGICAL,188)@20
    vCount_uid189_countZ_uid55_fpLog10Test_q <= "1" WHEN rVStage_uid188_countZ_uid55_fpLog10Test_merged_bit_select_b = zs_uid187_countZ_uid55_fpLog10Test_q ELSE "0";

    -- redist30_vCount_uid189_countZ_uid55_fpLog10Test_q_1(DELAY,500)
    redist30_vCount_uid189_countZ_uid55_fpLog10Test_q_1 : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => vCount_uid189_countZ_uid55_fpLog10Test_q, xout => redist30_vCount_uid189_countZ_uid55_fpLog10Test_q_1_q, clk => clk, aclr => areset );

    -- vStagei_uid192_countZ_uid55_fpLog10Test(MUX,191)@20 + 1
    vStagei_uid192_countZ_uid55_fpLog10Test_s <= vCount_uid189_countZ_uid55_fpLog10Test_q;
    vStagei_uid192_countZ_uid55_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            vStagei_uid192_countZ_uid55_fpLog10Test_q <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            CASE (vStagei_uid192_countZ_uid55_fpLog10Test_s) IS
                WHEN "0" => vStagei_uid192_countZ_uid55_fpLog10Test_q <= rVStage_uid188_countZ_uid55_fpLog10Test_merged_bit_select_b;
                WHEN "1" => vStagei_uid192_countZ_uid55_fpLog10Test_q <= rVStage_uid188_countZ_uid55_fpLog10Test_merged_bit_select_c;
                WHEN OTHERS => vStagei_uid192_countZ_uid55_fpLog10Test_q <= (others => '0');
            END CASE;
        END IF;
    END PROCESS;

    -- rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select(BITSELECT,464)@21
    rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select_b <= vStagei_uid192_countZ_uid55_fpLog10Test_q(3 downto 2);
    rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select_c <= vStagei_uid192_countZ_uid55_fpLog10Test_q(1 downto 0);

    -- vCount_uid195_countZ_uid55_fpLog10Test(LOGICAL,194)@21
    vCount_uid195_countZ_uid55_fpLog10Test_q <= "1" WHEN rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select_b = z2_uid40_fpLog10Test_q ELSE "0";

    -- vStagei_uid198_countZ_uid55_fpLog10Test(MUX,197)@21
    vStagei_uid198_countZ_uid55_fpLog10Test_s <= vCount_uid195_countZ_uid55_fpLog10Test_q;
    vStagei_uid198_countZ_uid55_fpLog10Test_combproc: PROCESS (vStagei_uid198_countZ_uid55_fpLog10Test_s, rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select_b, rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select_c)
    BEGIN
        CASE (vStagei_uid198_countZ_uid55_fpLog10Test_s) IS
            WHEN "0" => vStagei_uid198_countZ_uid55_fpLog10Test_q <= rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select_b;
            WHEN "1" => vStagei_uid198_countZ_uid55_fpLog10Test_q <= rVStage_uid194_countZ_uid55_fpLog10Test_merged_bit_select_c;
            WHEN OTHERS => vStagei_uid198_countZ_uid55_fpLog10Test_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- rVStage_uid200_countZ_uid55_fpLog10Test(BITSELECT,199)@21
    rVStage_uid200_countZ_uid55_fpLog10Test_b <= vStagei_uid198_countZ_uid55_fpLog10Test_q(1 downto 1);

    -- vCount_uid201_countZ_uid55_fpLog10Test(LOGICAL,200)@21
    vCount_uid201_countZ_uid55_fpLog10Test_q <= "1" WHEN rVStage_uid200_countZ_uid55_fpLog10Test_b = GND_q ELSE "0";

    -- r_uid202_countZ_uid55_fpLog10Test(BITJOIN,201)@21
    r_uid202_countZ_uid55_fpLog10Test_q <= redist34_vCount_uid169_countZ_uid55_fpLog10Test_q_3_q & redist32_vCount_uid177_countZ_uid55_fpLog10Test_q_2_q & redist31_vCount_uid183_countZ_uid55_fpLog10Test_q_1_q & redist30_vCount_uid189_countZ_uid55_fpLog10Test_q_1_q & vCount_uid195_countZ_uid55_fpLog10Test_q & vCount_uid201_countZ_uid55_fpLog10Test_q;

    -- redist29_r_uid202_countZ_uid55_fpLog10Test_q_1(DELAY,499)
    redist29_r_uid202_countZ_uid55_fpLog10Test_q_1 : dspba_delay
    GENERIC MAP ( width => 6, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => r_uid202_countZ_uid55_fpLog10Test_q, xout => redist29_r_uid202_countZ_uid55_fpLog10Test_q_1_q, clk => clk, aclr => areset );

    -- cstMSBFinalSumPBias_uid57_fpLog10Test(CONSTANT,56)
    cstMSBFinalSumPBias_uid57_fpLog10Test_q <= "010001000";

    -- expRExt_uid58_fpLog10Test(SUB,57)@22 + 1
    expRExt_uid58_fpLog10Test_a <= STD_LOGIC_VECTOR("0" & cstMSBFinalSumPBias_uid57_fpLog10Test_q);
    expRExt_uid58_fpLog10Test_b <= STD_LOGIC_VECTOR("0000" & redist29_r_uid202_countZ_uid55_fpLog10Test_q_1_q);
    expRExt_uid58_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            expRExt_uid58_fpLog10Test_o <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            expRExt_uid58_fpLog10Test_o <= STD_LOGIC_VECTOR(UNSIGNED(expRExt_uid58_fpLog10Test_a) - UNSIGNED(expRExt_uid58_fpLog10Test_b));
        END IF;
    END PROCESS;
    expRExt_uid58_fpLog10Test_q <= expRExt_uid58_fpLog10Test_o(9 downto 0);

    -- redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3(DELAY,485)
    redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3 : dspba_delay
    GENERIC MAP ( width => 59, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => redist14_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_1_q, xout => redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q, clk => clk, aclr => areset );

    -- leftShiftStage0Idx7Rng56_uid347_normVal_uid56_fpLog10Test(BITSELECT,346)@21
    leftShiftStage0Idx7Rng56_uid347_normVal_uid56_fpLog10Test_in <= redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q(2 downto 0);
    leftShiftStage0Idx7Rng56_uid347_normVal_uid56_fpLog10Test_b <= leftShiftStage0Idx7Rng56_uid347_normVal_uid56_fpLog10Test_in(2 downto 0);

    -- leftShiftStage0Idx7Pad56_uid346_normVal_uid56_fpLog10Test(CONSTANT,345)
    leftShiftStage0Idx7Pad56_uid346_normVal_uid56_fpLog10Test_q <= "00000000000000000000000000000000000000000000000000000000";

    -- leftShiftStage0Idx7_uid348_normVal_uid56_fpLog10Test(BITJOIN,347)@21
    leftShiftStage0Idx7_uid348_normVal_uid56_fpLog10Test_q <= leftShiftStage0Idx7Rng56_uid347_normVal_uid56_fpLog10Test_b & leftShiftStage0Idx7Pad56_uid346_normVal_uid56_fpLog10Test_q;

    -- leftShiftStage0Idx6Rng48_uid344_normVal_uid56_fpLog10Test(BITSELECT,343)@21
    leftShiftStage0Idx6Rng48_uid344_normVal_uid56_fpLog10Test_in <= redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q(10 downto 0);
    leftShiftStage0Idx6Rng48_uid344_normVal_uid56_fpLog10Test_b <= leftShiftStage0Idx6Rng48_uid344_normVal_uid56_fpLog10Test_in(10 downto 0);

    -- leftShiftStage0Idx6Pad48_uid343_normVal_uid56_fpLog10Test(CONSTANT,342)
    leftShiftStage0Idx6Pad48_uid343_normVal_uid56_fpLog10Test_q <= "000000000000000000000000000000000000000000000000";

    -- leftShiftStage0Idx6_uid345_normVal_uid56_fpLog10Test(BITJOIN,344)@21
    leftShiftStage0Idx6_uid345_normVal_uid56_fpLog10Test_q <= leftShiftStage0Idx6Rng48_uid344_normVal_uid56_fpLog10Test_b & leftShiftStage0Idx6Pad48_uid343_normVal_uid56_fpLog10Test_q;

    -- leftShiftStage0Idx5Rng40_uid341_normVal_uid56_fpLog10Test(BITSELECT,340)@21
    leftShiftStage0Idx5Rng40_uid341_normVal_uid56_fpLog10Test_in <= redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q(18 downto 0);
    leftShiftStage0Idx5Rng40_uid341_normVal_uid56_fpLog10Test_b <= leftShiftStage0Idx5Rng40_uid341_normVal_uid56_fpLog10Test_in(18 downto 0);

    -- leftShiftStage0Idx5Pad40_uid340_normVal_uid56_fpLog10Test(CONSTANT,339)
    leftShiftStage0Idx5Pad40_uid340_normVal_uid56_fpLog10Test_q <= "0000000000000000000000000000000000000000";

    -- leftShiftStage0Idx5_uid342_normVal_uid56_fpLog10Test(BITJOIN,341)@21
    leftShiftStage0Idx5_uid342_normVal_uid56_fpLog10Test_q <= leftShiftStage0Idx5Rng40_uid341_normVal_uid56_fpLog10Test_b & leftShiftStage0Idx5Pad40_uid340_normVal_uid56_fpLog10Test_q;

    -- redist33_vStage_uid171_countZ_uid55_fpLog10Test_b_2(DELAY,503)
    redist33_vStage_uid171_countZ_uid55_fpLog10Test_b_2 : dspba_delay
    GENERIC MAP ( width => 27, depth => 2, reset_kind => "ASYNC" )
    PORT MAP ( xin => vStage_uid171_countZ_uid55_fpLog10Test_b, xout => redist33_vStage_uid171_countZ_uid55_fpLog10Test_b_2_q, clk => clk, aclr => areset );

    -- leftShiftStage0Idx4_uid339_normVal_uid56_fpLog10Test(BITJOIN,338)@21
    leftShiftStage0Idx4_uid339_normVal_uid56_fpLog10Test_q <= redist33_vStage_uid171_countZ_uid55_fpLog10Test_b_2_q & zs_uid167_countZ_uid55_fpLog10Test_q;

    -- leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1(MUX,382)@21
    leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_s <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_b;
    leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_combproc: PROCESS (leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_s, leftShiftStage0Idx4_uid339_normVal_uid56_fpLog10Test_q, leftShiftStage0Idx5_uid342_normVal_uid56_fpLog10Test_q, leftShiftStage0Idx6_uid345_normVal_uid56_fpLog10Test_q, leftShiftStage0Idx7_uid348_normVal_uid56_fpLog10Test_q)
    BEGIN
        CASE (leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_s) IS
            WHEN "00" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_q <= leftShiftStage0Idx4_uid339_normVal_uid56_fpLog10Test_q;
            WHEN "01" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_q <= leftShiftStage0Idx5_uid342_normVal_uid56_fpLog10Test_q;
            WHEN "10" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_q <= leftShiftStage0Idx6_uid345_normVal_uid56_fpLog10Test_q;
            WHEN "11" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_q <= leftShiftStage0Idx7_uid348_normVal_uid56_fpLog10Test_q;
            WHEN OTHERS => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- leftShiftStage0Idx3Rng24_uid335_normVal_uid56_fpLog10Test(BITSELECT,334)@21
    leftShiftStage0Idx3Rng24_uid335_normVal_uid56_fpLog10Test_in <= redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q(34 downto 0);
    leftShiftStage0Idx3Rng24_uid335_normVal_uid56_fpLog10Test_b <= leftShiftStage0Idx3Rng24_uid335_normVal_uid56_fpLog10Test_in(34 downto 0);

    -- leftShiftStage0Idx3Pad24_uid334_normVal_uid56_fpLog10Test(CONSTANT,333)
    leftShiftStage0Idx3Pad24_uid334_normVal_uid56_fpLog10Test_q <= "000000000000000000000000";

    -- leftShiftStage0Idx3_uid336_normVal_uid56_fpLog10Test(BITJOIN,335)@21
    leftShiftStage0Idx3_uid336_normVal_uid56_fpLog10Test_q <= leftShiftStage0Idx3Rng24_uid335_normVal_uid56_fpLog10Test_b & leftShiftStage0Idx3Pad24_uid334_normVal_uid56_fpLog10Test_q;

    -- leftShiftStage0Idx2Rng16_uid332_normVal_uid56_fpLog10Test(BITSELECT,331)@21
    leftShiftStage0Idx2Rng16_uid332_normVal_uid56_fpLog10Test_in <= redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q(42 downto 0);
    leftShiftStage0Idx2Rng16_uid332_normVal_uid56_fpLog10Test_b <= leftShiftStage0Idx2Rng16_uid332_normVal_uid56_fpLog10Test_in(42 downto 0);

    -- leftShiftStage0Idx2_uid333_normVal_uid56_fpLog10Test(BITJOIN,332)@21
    leftShiftStage0Idx2_uid333_normVal_uid56_fpLog10Test_q <= leftShiftStage0Idx2Rng16_uid332_normVal_uid56_fpLog10Test_b & zs_uid175_countZ_uid55_fpLog10Test_q;

    -- leftShiftStage0Idx1Rng8_uid329_normVal_uid56_fpLog10Test(BITSELECT,328)@21
    leftShiftStage0Idx1Rng8_uid329_normVal_uid56_fpLog10Test_in <= redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q(50 downto 0);
    leftShiftStage0Idx1Rng8_uid329_normVal_uid56_fpLog10Test_b <= leftShiftStage0Idx1Rng8_uid329_normVal_uid56_fpLog10Test_in(50 downto 0);

    -- leftShiftStage0Idx1_uid330_normVal_uid56_fpLog10Test(BITJOIN,329)@21
    leftShiftStage0Idx1_uid330_normVal_uid56_fpLog10Test_q <= leftShiftStage0Idx1Rng8_uid329_normVal_uid56_fpLog10Test_b & cstAllZWE_uid14_fpLog10Test_q;

    -- leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0(MUX,381)@21
    leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_s <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_b;
    leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_combproc: PROCESS (leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_s, redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q, leftShiftStage0Idx1_uid330_normVal_uid56_fpLog10Test_q, leftShiftStage0Idx2_uid333_normVal_uid56_fpLog10Test_q, leftShiftStage0Idx3_uid336_normVal_uid56_fpLog10Test_q)
    BEGIN
        CASE (leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_s) IS
            WHEN "00" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_q <= redist15_finalSumAbs_uid54_fpLog10Test_BitJoin_for_q_q_3_q;
            WHEN "01" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_q <= leftShiftStage0Idx1_uid330_normVal_uid56_fpLog10Test_q;
            WHEN "10" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_q <= leftShiftStage0Idx2_uid333_normVal_uid56_fpLog10Test_q;
            WHEN "11" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_q <= leftShiftStage0Idx3_uid336_normVal_uid56_fpLog10Test_q;
            WHEN OTHERS => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select(BITSELECT,465)@21
    leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_b <= r_uid202_countZ_uid55_fpLog10Test_q(5 downto 3);
    leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_c <= r_uid202_countZ_uid55_fpLog10Test_q(2 downto 0);

    -- leftShiftStage0_uid350_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select(BITSELECT,468)@21
    leftShiftStage0_uid350_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_b <= leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_b(1 downto 0);
    leftShiftStage0_uid350_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_c <= leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_b(2 downto 2);

    -- leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal(MUX,383)@21 + 1
    leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_s <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_c;
    leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q <= (others => '0');
        ELSIF (clk'EVENT AND clk = '1') THEN
            CASE (leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_s) IS
                WHEN "0" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_0_q;
                WHEN "1" => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_msplit_1_q;
                WHEN OTHERS => leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q <= (others => '0');
            END CASE;
        END IF;
    END PROCESS;

    -- leftShiftStage1Idx7Rng7_uid370_normVal_uid56_fpLog10Test(BITSELECT,369)@22
    leftShiftStage1Idx7Rng7_uid370_normVal_uid56_fpLog10Test_in <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q(51 downto 0);
    leftShiftStage1Idx7Rng7_uid370_normVal_uid56_fpLog10Test_b <= leftShiftStage1Idx7Rng7_uid370_normVal_uid56_fpLog10Test_in(51 downto 0);

    -- leftShiftStage1Idx7Pad7_uid369_normVal_uid56_fpLog10Test(CONSTANT,368)
    leftShiftStage1Idx7Pad7_uid369_normVal_uid56_fpLog10Test_q <= "0000000";

    -- leftShiftStage1Idx7_uid371_normVal_uid56_fpLog10Test(BITJOIN,370)@22
    leftShiftStage1Idx7_uid371_normVal_uid56_fpLog10Test_q <= leftShiftStage1Idx7Rng7_uid370_normVal_uid56_fpLog10Test_b & leftShiftStage1Idx7Pad7_uid369_normVal_uid56_fpLog10Test_q;

    -- leftShiftStage1Idx6Rng6_uid367_normVal_uid56_fpLog10Test(BITSELECT,366)@22
    leftShiftStage1Idx6Rng6_uid367_normVal_uid56_fpLog10Test_in <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q(52 downto 0);
    leftShiftStage1Idx6Rng6_uid367_normVal_uid56_fpLog10Test_b <= leftShiftStage1Idx6Rng6_uid367_normVal_uid56_fpLog10Test_in(52 downto 0);

    -- leftShiftStage1Idx6Pad6_uid366_normVal_uid56_fpLog10Test(CONSTANT,365)
    leftShiftStage1Idx6Pad6_uid366_normVal_uid56_fpLog10Test_q <= "000000";

    -- leftShiftStage1Idx6_uid368_normVal_uid56_fpLog10Test(BITJOIN,367)@22
    leftShiftStage1Idx6_uid368_normVal_uid56_fpLog10Test_q <= leftShiftStage1Idx6Rng6_uid367_normVal_uid56_fpLog10Test_b & leftShiftStage1Idx6Pad6_uid366_normVal_uid56_fpLog10Test_q;

    -- leftShiftStage1Idx5Rng5_uid364_normVal_uid56_fpLog10Test(BITSELECT,363)@22
    leftShiftStage1Idx5Rng5_uid364_normVal_uid56_fpLog10Test_in <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q(53 downto 0);
    leftShiftStage1Idx5Rng5_uid364_normVal_uid56_fpLog10Test_b <= leftShiftStage1Idx5Rng5_uid364_normVal_uid56_fpLog10Test_in(53 downto 0);

    -- leftShiftStage1Idx5Pad5_uid363_normVal_uid56_fpLog10Test(CONSTANT,362)
    leftShiftStage1Idx5Pad5_uid363_normVal_uid56_fpLog10Test_q <= "00000";

    -- leftShiftStage1Idx5_uid365_normVal_uid56_fpLog10Test(BITJOIN,364)@22
    leftShiftStage1Idx5_uid365_normVal_uid56_fpLog10Test_q <= leftShiftStage1Idx5Rng5_uid364_normVal_uid56_fpLog10Test_b & leftShiftStage1Idx5Pad5_uid363_normVal_uid56_fpLog10Test_q;

    -- leftShiftStage1Idx4Rng4_uid361_normVal_uid56_fpLog10Test(BITSELECT,360)@22
    leftShiftStage1Idx4Rng4_uid361_normVal_uid56_fpLog10Test_in <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q(54 downto 0);
    leftShiftStage1Idx4Rng4_uid361_normVal_uid56_fpLog10Test_b <= leftShiftStage1Idx4Rng4_uid361_normVal_uid56_fpLog10Test_in(54 downto 0);

    -- leftShiftStage1Idx4_uid362_normVal_uid56_fpLog10Test(BITJOIN,361)@22
    leftShiftStage1Idx4_uid362_normVal_uid56_fpLog10Test_q <= leftShiftStage1Idx4Rng4_uid361_normVal_uid56_fpLog10Test_b & zs_uid187_countZ_uid55_fpLog10Test_q;

    -- leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1(MUX,387)@22
    leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_s <= leftShiftStage1_uid373_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_b;
    leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_combproc: PROCESS (leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_s, leftShiftStage1Idx4_uid362_normVal_uid56_fpLog10Test_q, leftShiftStage1Idx5_uid365_normVal_uid56_fpLog10Test_q, leftShiftStage1Idx6_uid368_normVal_uid56_fpLog10Test_q, leftShiftStage1Idx7_uid371_normVal_uid56_fpLog10Test_q)
    BEGIN
        CASE (leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_s) IS
            WHEN "00" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_q <= leftShiftStage1Idx4_uid362_normVal_uid56_fpLog10Test_q;
            WHEN "01" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_q <= leftShiftStage1Idx5_uid365_normVal_uid56_fpLog10Test_q;
            WHEN "10" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_q <= leftShiftStage1Idx6_uid368_normVal_uid56_fpLog10Test_q;
            WHEN "11" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_q <= leftShiftStage1Idx7_uid371_normVal_uid56_fpLog10Test_q;
            WHEN OTHERS => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- leftShiftStage1Idx3Rng3_uid358_normVal_uid56_fpLog10Test(BITSELECT,357)@22
    leftShiftStage1Idx3Rng3_uid358_normVal_uid56_fpLog10Test_in <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q(55 downto 0);
    leftShiftStage1Idx3Rng3_uid358_normVal_uid56_fpLog10Test_b <= leftShiftStage1Idx3Rng3_uid358_normVal_uid56_fpLog10Test_in(55 downto 0);

    -- leftShiftStage1Idx3_uid359_normVal_uid56_fpLog10Test(BITJOIN,358)@22
    leftShiftStage1Idx3_uid359_normVal_uid56_fpLog10Test_q <= leftShiftStage1Idx3Rng3_uid358_normVal_uid56_fpLog10Test_b & paddingX_uid106_eLog102_uid29_fpLog10Test_q;

    -- leftShiftStage1Idx2Rng2_uid355_normVal_uid56_fpLog10Test(BITSELECT,354)@22
    leftShiftStage1Idx2Rng2_uid355_normVal_uid56_fpLog10Test_in <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q(56 downto 0);
    leftShiftStage1Idx2Rng2_uid355_normVal_uid56_fpLog10Test_b <= leftShiftStage1Idx2Rng2_uid355_normVal_uid56_fpLog10Test_in(56 downto 0);

    -- leftShiftStage1Idx2_uid356_normVal_uid56_fpLog10Test(BITJOIN,355)@22
    leftShiftStage1Idx2_uid356_normVal_uid56_fpLog10Test_q <= leftShiftStage1Idx2Rng2_uid355_normVal_uid56_fpLog10Test_b & z2_uid40_fpLog10Test_q;

    -- leftShiftStage1Idx1Rng1_uid352_normVal_uid56_fpLog10Test(BITSELECT,351)@22
    leftShiftStage1Idx1Rng1_uid352_normVal_uid56_fpLog10Test_in <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q(57 downto 0);
    leftShiftStage1Idx1Rng1_uid352_normVal_uid56_fpLog10Test_b <= leftShiftStage1Idx1Rng1_uid352_normVal_uid56_fpLog10Test_in(57 downto 0);

    -- leftShiftStage1Idx1_uid353_normVal_uid56_fpLog10Test(BITJOIN,352)@22
    leftShiftStage1Idx1_uid353_normVal_uid56_fpLog10Test_q <= leftShiftStage1Idx1Rng1_uid352_normVal_uid56_fpLog10Test_b & GND_q;

    -- leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0(MUX,386)@22
    leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_s <= leftShiftStage1_uid373_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_b;
    leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_combproc: PROCESS (leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_s, leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q, leftShiftStage1Idx1_uid353_normVal_uid56_fpLog10Test_q, leftShiftStage1Idx2_uid356_normVal_uid56_fpLog10Test_q, leftShiftStage1Idx3_uid359_normVal_uid56_fpLog10Test_q)
    BEGIN
        CASE (leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_s) IS
            WHEN "00" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_q <= leftShiftStage0_uid350_normVal_uid56_fpLog10Test_mfinal_q;
            WHEN "01" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_q <= leftShiftStage1Idx1_uid353_normVal_uid56_fpLog10Test_q;
            WHEN "10" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_q <= leftShiftStage1Idx2_uid356_normVal_uid56_fpLog10Test_q;
            WHEN "11" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_q <= leftShiftStage1Idx3_uid359_normVal_uid56_fpLog10Test_q;
            WHEN OTHERS => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- redist0_leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_c_1(DELAY,470)
    redist0_leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_c_1 : dspba_delay
    GENERIC MAP ( width => 3, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_c, xout => redist0_leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_c_1_q, clk => clk, aclr => areset );

    -- leftShiftStage1_uid373_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select(BITSELECT,469)@22
    leftShiftStage1_uid373_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_b <= redist0_leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_c_1_q(1 downto 0);
    leftShiftStage1_uid373_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_c <= redist0_leftShiftStageSel5Dto3_uid349_normVal_uid56_fpLog10Test_merged_bit_select_c_1_q(2 downto 2);

    -- leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal(MUX,388)@22
    leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_s <= leftShiftStage1_uid373_normVal_uid56_fpLog10Test_selLSBs_merged_bit_select_c;
    leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_combproc: PROCESS (leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_s, leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_q, leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_q)
    BEGIN
        CASE (leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_s) IS
            WHEN "0" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_q <= leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_0_q;
            WHEN "1" => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_q <= leftShiftStage1_uid373_normVal_uid56_fpLog10Test_msplit_1_q;
            WHEN OTHERS => leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- fracR_uid59_fpLog10Test(BITSELECT,58)@22
    fracR_uid59_fpLog10Test_in <= leftShiftStage1_uid373_normVal_uid56_fpLog10Test_mfinal_q(57 downto 0);
    fracR_uid59_fpLog10Test_b <= fracR_uid59_fpLog10Test_in(57 downto 34);

    -- redist44_fracR_uid59_fpLog10Test_b_1(DELAY,514)
    redist44_fracR_uid59_fpLog10Test_b_1 : dspba_delay
    GENERIC MAP ( width => 24, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => fracR_uid59_fpLog10Test_b, xout => redist44_fracR_uid59_fpLog10Test_b_1_q, clk => clk, aclr => areset );

    -- expFracConc_uid60_fpLog10Test(BITJOIN,59)@23
    expFracConc_uid60_fpLog10Test_q <= expRExt_uid58_fpLog10Test_q & redist44_fracR_uid59_fpLog10Test_b_1_q;

    -- expFracPostRnd_uid62_fpLog10Test(ADD,61)@23
    expFracPostRnd_uid62_fpLog10Test_a <= STD_LOGIC_VECTOR("0" & expFracConc_uid60_fpLog10Test_q);
    expFracPostRnd_uid62_fpLog10Test_b <= STD_LOGIC_VECTOR("0000000000000000000000000000000000" & VCC_q);
    expFracPostRnd_uid62_fpLog10Test_o <= STD_LOGIC_VECTOR(UNSIGNED(expFracPostRnd_uid62_fpLog10Test_a) + UNSIGNED(expFracPostRnd_uid62_fpLog10Test_b));
    expFracPostRnd_uid62_fpLog10Test_q <= expFracPostRnd_uid62_fpLog10Test_o(34 downto 0);

    -- fracR_uid63_fpLog10Test_merged_bit_select(BITSELECT,459)@23
    fracR_uid63_fpLog10Test_merged_bit_select_in <= expFracPostRnd_uid62_fpLog10Test_q(31 downto 0);
    fracR_uid63_fpLog10Test_merged_bit_select_b <= fracR_uid63_fpLog10Test_merged_bit_select_in(23 downto 1);
    fracR_uid63_fpLog10Test_merged_bit_select_c <= fracR_uid63_fpLog10Test_merged_bit_select_in(31 downto 24);

    -- invSignX_uid68_fpLog10Test(LOGICAL,67)@10
    invSignX_uid68_fpLog10Test_q <= not (redist57_signX_uid7_fpLog10Test_b_10_q);

    -- excI_x_uid21_fpLog10Test(LOGICAL,20)@10
    excI_x_uid21_fpLog10Test_q <= expXIsMax_uid18_fpLog10Test_q and redist53_fracXIsZero_uid19_fpLog10Test_q_2_q;

    -- excRInfC1_uid69_fpLog10Test(LOGICAL,68)@10 + 1
    excRInfC1_uid69_fpLog10Test_qi <= excI_x_uid21_fpLog10Test_q and invSignX_uid68_fpLog10Test_q;
    excRInfC1_uid69_fpLog10Test_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excRInfC1_uid69_fpLog10Test_qi, xout => excRInfC1_uid69_fpLog10Test_q, clk => clk, aclr => areset );

    -- redist42_excRInfC1_uid69_fpLog10Test_q_5(DELAY,512)
    redist42_excRInfC1_uid69_fpLog10Test_q_5 : dspba_delay
    GENERIC MAP ( width => 1, depth => 4, reset_kind => "ASYNC" )
    PORT MAP ( xin => excRInfC1_uid69_fpLog10Test_q, xout => redist42_excRInfC1_uid69_fpLog10Test_q_5_q, clk => clk, aclr => areset );

    -- excRInf_uid70_fpLog10Test(LOGICAL,69)@15
    excRInf_uid70_fpLog10Test_q <= redist42_excRInfC1_uid69_fpLog10Test_q_5_q or redist54_excZ_x_uid17_fpLog10Test_q_5_q;

    -- FPOne_uid65_fpLog10Test(BITJOIN,64)@0
    FPOne_uid65_fpLog10Test_q <= GND_q & cstBias_uid9_fpLog10Test_q & cstAllZWF_uid8_fpLog10Test_q;

    -- excRZero_uid67_fpLog10Test(LOGICAL,66)@0 + 1
    excRZero_uid67_fpLog10Test_qi <= "1" WHEN a = FPOne_uid65_fpLog10Test_q ELSE "0";
    excRZero_uid67_fpLog10Test_delay : dspba_delay
    GENERIC MAP ( width => 1, depth => 1, reset_kind => "ASYNC" )
    PORT MAP ( xin => excRZero_uid67_fpLog10Test_qi, xout => excRZero_uid67_fpLog10Test_q, clk => clk, aclr => areset );

    -- redist43_excRZero_uid67_fpLog10Test_q_15(DELAY,513)
    redist43_excRZero_uid67_fpLog10Test_q_15 : dspba_delay
    GENERIC MAP ( width => 1, depth => 14, reset_kind => "ASYNC" )
    PORT MAP ( xin => excRZero_uid67_fpLog10Test_q, xout => redist43_excRZero_uid67_fpLog10Test_q_15_q, clk => clk, aclr => areset );

    -- concExc_uid81_fpLog10Test(BITJOIN,80)@15
    concExc_uid81_fpLog10Test_q <= redist41_excRNaN_uid73_fpLog10Test_q_5_q & excRInf_uid70_fpLog10Test_q & redist43_excRZero_uid67_fpLog10Test_q_15_q;

    -- excREnc_uid82_fpLog10Test(LOOKUP,81)@15 + 1
    excREnc_uid82_fpLog10Test_clkproc: PROCESS (clk, areset)
    BEGIN
        IF (areset = '1') THEN
            excREnc_uid82_fpLog10Test_q <= "01";
        ELSIF (clk'EVENT AND clk = '1') THEN
            CASE (concExc_uid81_fpLog10Test_q) IS
                WHEN "000" => excREnc_uid82_fpLog10Test_q <= "01";
                WHEN "001" => excREnc_uid82_fpLog10Test_q <= "00";
                WHEN "010" => excREnc_uid82_fpLog10Test_q <= "10";
                WHEN "011" => excREnc_uid82_fpLog10Test_q <= "00";
                WHEN "100" => excREnc_uid82_fpLog10Test_q <= "11";
                WHEN "101" => excREnc_uid82_fpLog10Test_q <= "00";
                WHEN "110" => excREnc_uid82_fpLog10Test_q <= "00";
                WHEN "111" => excREnc_uid82_fpLog10Test_q <= "00";
                WHEN OTHERS => -- unreachable
                               excREnc_uid82_fpLog10Test_q <= (others => '-');
            END CASE;
        END IF;
    END PROCESS;

    -- redist39_excREnc_uid82_fpLog10Test_q_8(DELAY,509)
    redist39_excREnc_uid82_fpLog10Test_q_8 : dspba_delay
    GENERIC MAP ( width => 2, depth => 7, reset_kind => "ASYNC" )
    PORT MAP ( xin => excREnc_uid82_fpLog10Test_q, xout => redist39_excREnc_uid82_fpLog10Test_q_8_q, clk => clk, aclr => areset );

    -- expRPostExc_uid90_fpLog10Test(MUX,89)@23
    expRPostExc_uid90_fpLog10Test_s <= redist39_excREnc_uid82_fpLog10Test_q_8_q;
    expRPostExc_uid90_fpLog10Test_combproc: PROCESS (expRPostExc_uid90_fpLog10Test_s, cstAllZWE_uid14_fpLog10Test_q, fracR_uid63_fpLog10Test_merged_bit_select_c, cstAllOWE_uid12_fpLog10Test_q)
    BEGIN
        CASE (expRPostExc_uid90_fpLog10Test_s) IS
            WHEN "00" => expRPostExc_uid90_fpLog10Test_q <= cstAllZWE_uid14_fpLog10Test_q;
            WHEN "01" => expRPostExc_uid90_fpLog10Test_q <= fracR_uid63_fpLog10Test_merged_bit_select_c;
            WHEN "10" => expRPostExc_uid90_fpLog10Test_q <= cstAllOWE_uid12_fpLog10Test_q;
            WHEN "11" => expRPostExc_uid90_fpLog10Test_q <= cstAllOWE_uid12_fpLog10Test_q;
            WHEN OTHERS => expRPostExc_uid90_fpLog10Test_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- oneFracRPostExc2_uid83_fpLog10Test(CONSTANT,82)
    oneFracRPostExc2_uid83_fpLog10Test_q <= "00000000000000000000001";

    -- fracRPostExc_uid86_fpLog10Test(MUX,85)@23
    fracRPostExc_uid86_fpLog10Test_s <= redist39_excREnc_uid82_fpLog10Test_q_8_q;
    fracRPostExc_uid86_fpLog10Test_combproc: PROCESS (fracRPostExc_uid86_fpLog10Test_s, cstAllZWF_uid8_fpLog10Test_q, fracR_uid63_fpLog10Test_merged_bit_select_b, oneFracRPostExc2_uid83_fpLog10Test_q)
    BEGIN
        CASE (fracRPostExc_uid86_fpLog10Test_s) IS
            WHEN "00" => fracRPostExc_uid86_fpLog10Test_q <= cstAllZWF_uid8_fpLog10Test_q;
            WHEN "01" => fracRPostExc_uid86_fpLog10Test_q <= fracR_uid63_fpLog10Test_merged_bit_select_b;
            WHEN "10" => fracRPostExc_uid86_fpLog10Test_q <= cstAllZWF_uid8_fpLog10Test_q;
            WHEN "11" => fracRPostExc_uid86_fpLog10Test_q <= oneFracRPostExc2_uid83_fpLog10Test_q;
            WHEN OTHERS => fracRPostExc_uid86_fpLog10Test_q <= (others => '0');
        END CASE;
    END PROCESS;

    -- RLog10_uid91_fpLog10Test(BITJOIN,90)@23
    RLog10_uid91_fpLog10Test_q <= redist40_signRFull_uid80_fpLog10Test_q_8_q & expRPostExc_uid90_fpLog10Test_q & fracRPostExc_uid86_fpLog10Test_q;

    -- xOut(GPOUT,4)@23
    q <= RLog10_uid91_fpLog10Test_q;

END normal;
