/*************************************************************
EE 465 Deliverable 2 Top Module.
Written by: Sean Froome

Original Code for the top module
borrowed heavily from EE 461 Lab Code.
NCO is generated and instantiated using
an Altera/Intel IP Block.
LFSR is from EE456 Lab 5 code, and the mapper was originally written for that
lab as well.
*************************************************************/
`default_nettype none
module EE465_Deliverable_2_vFinal(
		input wire CLOCK_50,
		input wire [17:0]SW,
		input wire [3:0] KEY,
		output reg [3:0]  LEDG,
		output reg [17:0] LEDR,
		output wire sym_error_I,
		output wire sym_correct_I,
		output wire sym_error_Q,
		output wire sym_correct_Q);

		reg signed [17:0] isi_power;

		always @ *
		//isi_power = 18'sd16293; // aka about 15 dB of MER  at this value.
		isi_power = 18'sd9268; // aka 20dB of MER  at this value.
		//isi_power = 18'sd293;

		wire signed [21:0] q;

		wire signed [1:0]  q_out_I;
		wire signed [17:0] input_mapper_out_I;
		wire signed [17:0] decision_variable_I;
		wire signed [17:0] errorless_decision_variable_I;
		wire signed [17:0] error_from_DUT_I;
		wire signed [17:0] reference_level_I;
		wire signed [34:0] mapper_out_power_I;
		wire signed [1:0]  slicer_out_I;
		wire signed [17:0] mapper_out_I;
		wire signed [17:0] error_from_dv_I;
		wire signed [34:0] accumulated_squared_error_I;
		wire signed [17:0] accumulated_error_I;

		wire signed [1:0]  q_out_Q;
		wire signed [17:0] input_mapper_out_Q;
		wire signed [17:0] decision_variable_Q;
		wire signed [17:0] errorless_decision_variable_Q;
		wire signed [17:0] error_from_DUT_Q;
		wire signed [17:0] reference_level_Q;
		wire signed [17:0] mapper_out_power_Q;
		wire signed [1:0]  slicer_out_Q;
		wire signed [17:0] mapper_out_Q;
		wire signed [17:0] error_from_dv_Q;
		wire signed [17:0] accumulated_squared_error_Q;
		wire signed [17:0] accumulated_error_Q;

/************************************************************
Simple but useful. Set the LEDs to light up
when the switches are set high.
************************************************************/
always @ *
	LEDR = SW;
always @ *
	LEDG = KEY;
/************************************************************
Clock Generation and Clock Enables

25MHz Clock setup
************************************************************/
reg reset;
always @ *
	reset = SW[0];

reg sys_clk;
wire sam_clk_enable, sym_clk_enable;

always @ (posedge CLOCK_50)
	sys_clk = ~sys_clk;

Clock_Setup Clocks(
.reset(reset),
.sam_clk_enable(sam_clk_enable),
.sym_clk_enable(sym_clk_enable),
.sys_clk(sys_clk));//,
/************************************************************
End Clock Generation and Clock Enables
*************************************************************
Conditions for Clear Accumulator
************************************************************/
reg clear_accumulator;
reg[22:0] counter;
always  @ (posedge sys_clk)
  if(reset == 1'b1)
    counter = 22'd0;
  //else if ((sym_clk_enable == 1'b1)  &&(counter == 22'd1048576))//&& counter == 22'd1024)
	else if ((sym_clk_enable == 1'b1)  &&(counter == 23'd4194304))
    counter = 23'd0;
  else if(sym_clk_enable == 1'b1)
  	counter = counter + 23'd1;
  else
    counter = counter;

always @ *
  if(reset)
    clear_accumulator = 1'b1;
  //else if(counter == 22'd1048576)//(counter == 22'd1024)
	else if(counter == 23'd4194304)//(counter == 22'd1024)
  	clear_accumulator = 1'b1;
  else
  	clear_accumulator = 1'b0;
/**************************************************************
Input Mapper and LFSR Instantiation
************************************************************/
reg LFSR_SET;
always @ *
	LFSR_SET= SW[1];

LFSR LFSR1(
		.sys_clk(sys_clk),
		.sam_clk_enable(sam_clk_enable),
		.reset(reset),
		.q(q), // 22 bit output
		.q_out_I(q_out_I),
		.q_out_Q(q_out_Q));//,

Input_Mapper Map_In_I(
		.q_out(q_out_I),
		.input_mapper_out(input_mapper_out_I)); // ie: Input to DUT

Input_Mapper Map_In_Q(
		.q_out(q_out_Q),
		.input_mapper_out(input_mapper_out_Q)); // ie: Input to DUT
/*************************************************************
MER DUT Instantiation
*************************************************************/
DUT_for_MER_measurement DUT_I(
		.clk(sys_clk),
		.clk_en(sym_clk_enable),
		.reset(~reset),
		.isi_power(isi_power), // Basically... this is the inverse of the MER error part?
		.in_data(input_mapper_out_I), //Check later.
		.decision_variable(decision_variable_I),
		.errorless_decision_variable(errorless_decision_variable_I),
		.error(error_from_DUT_I));

DUT_for_MER_measurement DUT_Q(
		.clk(sys_clk),
		.clk_en(sym_clk_enable),
		.reset(~reset),
		.isi_power(isi_power), // Basically... this is the inverse of the MER error part?
		.in_data(input_mapper_out_Q), //Check later.
		.decision_variable(decision_variable_Q),
		.errorless_decision_variable(errorless_decision_variable_Q),
		.error(error_from_DUT_Q));
/*************************************************************
REFERENCE LEVEL CHUNK
*************************************************************/
reference_level_chunk reference_I(
		.reset(reset),
		.decision_variable(decision_variable_I),
		.clear_accumulator(clear_accumulator),
		.sym_clk_enable(sym_clk_enable),
		.sys_clk(sys_clk),
		.reference_level(reference_level_I),
		.mapper_out_power(mapper_out_power_I));

reference_level_chunk reference_Q(
		.reset(reset),
		.decision_variable(decision_variable_Q),
		.clear_accumulator(clear_accumulator),
		.sym_clk_enable(sym_clk_enable),
		.sys_clk(sys_clk),
		.reference_level(reference_level_Q),
		.mapper_out_power(mapper_out_power_Q));
/*************************************************************
SLICER
*************************************************************/
slicer slicer_I(
	.reference_level(reference_level_I),
	.decision_variable(decision_variable_I),
	.slicer_out(slicer_out_I));

slicer slicer_Q(
	.reference_level(reference_level_Q),
	.decision_variable(decision_variable_Q),
	.slicer_out(slicer_out_Q));
/*************************************************************
Output MER Measurement MAPPER
************************************************************/
Output_Mapper Output_Map_I(
	.slicer_out(slicer_out_I),
	.reference_level(reference_level_I),
	.mapper_out(mapper_out_I));

Output_Mapper Output_Map_Q(
	.slicer_out(slicer_out_Q),
	.reference_level(reference_level_Q),
	.mapper_out(mapper_out_Q));
/*************************************************************
Error Chunk and Comparison
*************************************************************/
error_chunk error_circuit_I(
	.reset(reset),
	.sys_clk(sys_clk),
	.sym_clk_enable(sym_clk_enable),
	.decision_variable(decision_variable_I),
	.mapper_out(mapper_out_I),
	.slicer_out(slicer_out_I),
	.q_out(q_out_I),
	.error_from_dv(error_from_dv_I),
	.sym_correct(sym_correct_I),
	.sym_error(sym_error_I));

error_chunk error_circuit_Q(
	.reset(reset),
	.sys_clk(sys_clk),
	.sym_clk_enable(sym_clk_enable),
	.decision_variable(decision_variable_Q),
	.mapper_out(mapper_out_Q),
	.slicer_out(slicer_out_Q),
	.q_out(q_out_Q),
	.error_from_dv(error_from_dv_Q),
	.sym_correct(sym_correct_Q),
	.sym_error(sym_error_Q));
/**************************************************************
SQUARED ACCUMULATED ERROR CHUNK
*************************************************************/
accumulated_squared_error_chunk Acc_Squared_Error_I(
		.sys_clk(sys_clk),
		.reset(reset),
		.error(error_from_dv_I),
		.clear_accumulator(clear_accumulator),
		.sym_clk_enable(sym_clk_enable),
		.accumulated_squared_error(accumulated_squared_error_I));

accumulated_squared_error_chunk Acc_Squared_Error_Q(
		.sys_clk(sys_clk),
		.reset(reset),
		.error(error_from_dv_Q),
		.clear_accumulator(clear_accumulator),
		.sym_clk_enable(sym_clk_enable),
		.accumulated_squared_error(accumulated_squared_error_Q));
/*************************************************************
ACCUMULATED ERROR CHUNK
*************************************************************/
accumulated_error_chunk Error_I(
		.sys_clk(sys_clk),
		.reset(reset),
		.sym_clk_enable(sym_clk_enable),
		.clear_accumulator(clear_accumulator),
		.error(error_from_dv_I),
		.accumulated_error(accumulated_error_I));

accumulated_error_chunk Error_Q(
		.sys_clk(sys_clk),
		.reset(reset),
		.sym_clk_enable(sym_clk_enable),
		.clear_accumulator(clear_accumulator),
		.error(error_from_dv_Q),
		.accumulated_error(accumulated_error_Q));
/**************************************************************************************************************************
END ACCUMULATED ERROR CHUNK
***************************************************************************************************************************
Fixed To Floating Point
***************************************************************************************************************************/
wire [31:0] mapper_out_power_I_Float;
wire [31:0] accumulated_squared_error_I_Float;

FPCONVERT  Fixed_To_Float_Mapper_Out_Power(
	.clock ( sys_clk ),
	.dataa ( mapper_out_power_I ),
	.result ( mapper_out_power_I_Float ));

FPCONVERT  Fixed_To_Float_Accumulated_Squared_Error(
	.clock ( sys_clk ),
	.dataa ( accumulated_squared_error_I),
	.result ( accumulated_squared_error_I_Float ));
/***************************************************************************************************************************
Floating Point Division
***************************************************************************************************************************/
wire [31:0] MER_Linear_Float;

FP_DIV	mapper_out_power_Divided_by_Accumulated_Squared_Error (
	.clock ( sys_clk ),
	.dataa (mapper_out_power_I_Float ),
	.datab ( accumulated_squared_error_I_Float  ),
	.result ( MER_Linear_Float ));
/***************************************************************************************************************************
Floating Point Log Function
***************************************************************************************************************************/
wire [31:0] MER_dB_Float;

log10 MER_dB_Float_Out(
	.clk(sys_clk),
	.areset(reset),
	.a(MER_Linear_Float),
	.q(MER_dB_Float));
/***************************************************************************************************************************
End Log MER Circuit Parts
***************************************************************************************************************************/

endmodule
`default_nettype wire
