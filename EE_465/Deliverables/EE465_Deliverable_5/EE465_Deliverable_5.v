/*************************************************************************************************************************
EE 465 Deliverable 5 Top Module.
Written by: Sean Froome

This is the top module of the Deliverable. The Deliverable contains an MER
measurement circuit, three filters, as well as a module to upsampled and pass
the transmit filter outputs to the DAC for viewing on the Spectrum Analyzer.

CURRENT SWITCH ASSIGNMENTS
RESET =  SW[17]
TX GOLD OR TX PRAC  TO FILTER TEST = SW[14]; HIGH IS Practical, LOW IS Gold
*************************************************************************************************************************/
`default_nettype none
module EE465_Deliverable_4(
		input wire clock_50,
		input wire [17:0]SW,
		input wire [3:0] KEY,
		output reg [3:0]  LEDG,
		output reg [17:0] LEDR,
		input wire [13:0]ADC_DA,
		input wire[13:0]ADC_DB,
		output reg[13:0]DAC_DA,
		output reg [13:0]DAC_DB,
		output	wire ADC_CLK_A,
		output	wire ADC_CLK_B,
		output	wire ADC_OEB_A,
		output	wire ADC_OEB_B,
		output	wire DAC_CLK_A,
		output	wire DAC_CLK_B,
		output	wire DAC_MODE,
		output	wire DAC_WRT_A,
		output	wire DAC_WRT_B);
/************************************************************************************************************************
Tie LEDs to Switches and Keys, to make it clear when the switches and keys are active
************************************************************************************************************************/
always @ *
	LEDR = SW;
always @ *
	LEDG = KEY;

reg reset;
always @ *
	reset = SW[17];
/************************************************************************************************************************
Wires for LFSRs, clocks and DACs
************************************************************************************************************************/
wire signed [1:0]  q_out_Q;
wire signed [1:0]  q_out_I;
assign q_out_Q = q_out_I; // Since I only really need one channel right now....

wire sys_clk, sam_clk_enable, sym_clk_enable;
wire signed [17:0]srrc_out, srrc_input;
reg signed [17:0] output_from_filter;
wire [13:0] DAC_out;
wire unsigned [13:0] output_to_DAC;
/**************************************************************************************************************
				Setup DACs
***************************************************************************************************************/
(* keep *) wire signed [13:0]	sin_out;
reg [4:0] NCO_freq;	// unsigned fraction with units cycles/sample
(* noprune *)reg [13:0] registered_ADC_A;
(* noprune *)reg [13:0] registered_ADC_B;
assign DAC_CLK_A = sys_clk;
assign DAC_CLK_B = sys_clk;
assign DAC_MODE = 1'b1; //treat DACs seperately
assign DAC_WRT_A = ~sys_clk;
assign DAC_WRT_B = ~sys_clk;
always@ (posedge sys_clk)// make DAC A echo ADC A
	DAC_DA = registered_ADC_A[13:0];
always@ (posedge sys_clk)
	DAC_DB = DAC_out;
/***********************************************************************************************************
End DAC setup
************************************************************************************************************
 Setup ADCs
************************************************************************************************************/
assign ADC_CLK_A = sys_clk;
assign ADC_CLK_B = sys_clk;
assign ADC_OEB_A = 1'b1;
assign ADC_OEB_B = 1'b1;
always@ (posedge sys_clk)
	registered_ADC_A <= ADC_DA;
always@ (posedge sys_clk)
	registered_ADC_B <= ADC_DB;
/***********************************************************************************************************
End ADC setup
************************************************************************************************************/
EE465_filter_test SRRC_test(
			.clock_50(clock_50),
		 //.reset(~KEY[3]),
		 .reset(reset),
		 .output_from_filter_1s17(output_from_filter),
		 //.filter_input_scale(SW[2:0]),
		 //.filter_input_scale(filter_input_scale),
		 //.input_to_filter_1s17(srrc_input), // Not using
		 .lfsr_value(q_out_I),
		 .symbol_clk_ena(sym_clk_enable),
		 .sample_clk_ena(sam_clk_enable),
		 .system_clk(sys_clk),
		 .output_to_DAC(DAC_out));
/*************************************************************************************************************************
Conditions for Clear Accumulator
**************************************************************************************************************************/
reg clear_accumulator;
reg[22:0] counter;
always  @ (posedge sys_clk)
  if(reset == 1'b1)
    counter = 23'd0;
  //else if ((sym_clk_enable == 1'b1)  &&(counter == 22'd1048576))//&& counter == 22'd1024)
	else if ((sym_clk_enable == 1'b1)  &&(counter == 23'd4194304))//&& counter == 22'd1024)
    counter = 23'd0;
  else if(sym_clk_enable == 1'b1)
  	counter = counter + 23'd1;
  else
    counter = counter;

always @ *
  if(reset)
    clear_accumulator = 1'b1;
  //else if(counter == 22'd1048576)
  else if(counter == 23'd4194304)
  	clear_accumulator = 1'b1;
  else
  	clear_accumulator = 1'b0;

/*************************************************************************************************************************
Filter Instantiation
**************************************************************************************************************************/
wire signed [17:0] output_gold_tx, output_prac_tx, output_gold_rx;

always @ *
	if(SW[14] == 1'd0)
		output_from_filter = output_gold_tx ;
	else if (SW[14] == 1'd1)
		output_from_filter = output_prac_tx;

TX_PRAC TX_Filter_Practical(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.sym_clk_enable(sym_clk_enable),
	.x_in(q_out_I),
	//.x_in(test_value),
	.y_out(output_prac_tx));
	//.y_out(output_from_filter));

TX_Gold TX_Filter_Gold(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.x_in(q_out_I),
	.y_out(output_gold_tx));

RX_Gold RX_Filter_Gold(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.x_in(output_from_filter),
	.y_out(output_gold_rx));
/************************************************************************************************************************
I Phase
***********************************************************************************************************************/
wire signed [17:0] reference_level_I;
wire signed [17:0] mapper_out_power_I;
wire signed [1:0]  slicer_out_I;
wire signed [17:0] mapper_out_I;
wire signed [17:0] error_from_dv_I;
wire signed [17:0] accumulated_squared_error_I;
wire signed [17:0] accumulated_error_I;
wire sym_error_I;
wire sym_correct_I;
/************************************************************************************************************************
Q Phase
************************************************************************************************************************/
wire signed [17:0] reference_level_Q;
wire signed [17:0] mapper_out_power_Q;
wire signed [1:0]  slicer_out_Q;
wire signed [17:0] mapper_out_Q;
wire signed [17:0] error_from_dv_Q;
wire signed [17:0] accumulated_squared_error_Q;
wire signed [17:0] accumulated_error_Q;
wire sym_error_Q;
wire sym_correct_Q;
/*************************************************************************************************************************
REFERENCE LEVEL CHUNK
*************************************************************************************************************************/
reference_level_chunk reference_I(
	.reset(reset),
	.decision_variable(output_gold_rx),
	.clear_accumulator(clear_accumulator),
	.sym_clk_enable(sym_clk_enable),
	.sys_clk(sys_clk),
	.reference_level(reference_level_I),
	.mapper_out_power(mapper_out_power_I));

reference_level_chunk reference_Q(
	.reset(reset),
	.decision_variable(output_gold_rx),
	.clear_accumulator(clear_accumulator),
	.sym_clk_enable(sym_clk_enable),
	.sys_clk(sys_clk),
	.reference_level(reference_level_Q),
	.mapper_out_power(mapper_out_power_Q));
/*************************************************************************************************************************
SLICER
*************************************************************************************************************************/
slicer slicer_I(
	.reference_level(reference_level_I),
	.decision_variable(output_gold_rx),
	.slicer_out(slicer_out_I));

slicer slicer_Q(
	.reference_level(reference_level_Q),
	.decision_variable(output_gold_rx),
	.slicer_out(slicer_out_Q));
/*************************************************************************************************************************
Output MER Measurement MAPPER
**************************************************************************************************************************/
Output_Mapper Output_Map_I(
	.slicer_out(slicer_out_I),
	.reference_level(reference_level_I),
	.mapper_out(mapper_out_I));

Output_Mapper Output_Map_Q(
	.slicer_out(slicer_out_Q),
	.reference_level(reference_level_Q),
	.mapper_out(mapper_out_Q));
/*************************************************************************************************************************
Error Chunk and Comparison
*************************************************************************************************************************/
error_chunk error_circuit_I(
	.reset(reset),
	.sys_clk(sys_clk),
	.sym_clk_enable(sym_clk_enable),
	.decision_variable(output_gold_rx),
	.mapper_out(mapper_out_I),
	.slicer_out(slicer_out_I),
	.q_out(q_out_I),
	.error_from_dv(error_from_dv_I),
	.sym_correct(sym_correct_I),
	.sym_error(sym_error_I));

error_chunk error_circuit_Q(
	.reset(reset),
	.sys_clk(sys_clk),
	.sym_clk_enable(sym_clk_enable),
	.decision_variable(output_gold_rx),
	.mapper_out(mapper_out_Q),
	.slicer_out(slicer_out_Q),
	.q_out(q_out_Q),
	.error_from_dv(error_from_dv_Q),
	.sym_correct(sym_correct_Q),
	.sym_error(sym_error_Q));
/**************************************************************************************************************************
SQUARED ACCUMULATED ERROR CHUNK
*************************************************************************************************************************/
accumulated_squared_error_chunk Acc_Squared_Error_I(
	.sys_clk(sys_clk),
	.reset(reset),
	.error(error_from_dv_I),
	.clear_accumulator(clear_accumulator),
	.sym_clk_enable(sym_clk_enable),
	.accumulated_squared_error(accumulated_squared_error_I));

accumulated_squared_error_chunk Acc_Squared_Error_Q(
	.sys_clk(sys_clk),
	.reset(reset),
	.error(error_from_dv_Q),
	.clear_accumulator(clear_accumulator),
	.sym_clk_enable(sym_clk_enable),
	.accumulated_squared_error(accumulated_squared_error_Q));
/*************************************************************************************************************************
ACCUMULATED ERROR CHUNK
*************************************************************************************************************************/
accumulated_error_chunk Error_I(
	.sys_clk(sys_clk),
	.reset(reset),
	.sym_clk_enable(sym_clk_enable),
	.clear_accumulator(clear_accumulator),
	.error(error_from_dv_I),
	.accumulated_error(accumulated_error_I));

accumulated_error_chunk Error_Q(
	.sys_clk(sys_clk),
	.reset(reset),
	.sym_clk_enable(sym_clk_enable),
	.clear_accumulator(clear_accumulator),
	.error(error_from_dv_Q),
	.accumulated_error(accumulated_error_Q));
/***************************************************************************************************************************
END ACCUMULATED ERROR CHUNK
***************************************************************************************************************************
Fixed To Floating Point
***************************************************************************************************************************/
wire [31:0] mapper_out_power_I_Float;
wire [31:0] accumulated_squared_error_I_Float;

FPCONVERT  Fixed_To_Float_Mapper_Out_Power(
	.clock ( sys_clk ),
	.dataa ( mapper_out_power_I ),
	.result ( mapper_out_power_I_Float ));

FPCONVERT  Fixed_To_Float_Accumulated_Squared_Error(
	.clock ( sys_clk ),
	.dataa ( accumulated_squared_error_I),
	.result ( accumulated_squared_error_I_Float ));
/***************************************************************************************************************************
Floating Point Division
***************************************************************************************************************************/
wire [31:0] MER_Linear_Float;

FP_DIV	mapper_out_power_Divided_by_Accumulated_Squared_Error (
	.clock ( sys_clk ),
	.dataa ( mapper_out_power_I_Float),
	.datab ( accumulated_squared_error_I_Float  ),
	.result ( MER_Linear_Float ));
/***************************************************************************************************************************
Floating Point Log Function: Keep in mind that any value on SignalTap will need to
be multiplied by 10, as this doesn't do that.
***************************************************************************************************************************/
wire [31:0] MER_dB_Float;

log10 MER_dB_Float_Out(
	.clk(sys_clk),
	.areset(reset),
	.a(MER_Linear_Float),
	.q(MER_dB_Float));
/***************************************************************************************************************************
End Log MER Circuit Parts
***************************************************************************************************************************/
endmodule
`default_nettype wire


/*For Debugging
//reg signed [1:0] test_value;
//reg [7:0] counter_test;

always @(posedge sys_clk)
	if(counter_test == 8'd127 && sam_clk_enable == 1'b1)
		counter_test = 8'd0;
	else if(sam_clk_enable == 1'b1)
		counter_test = counter_test + 8'd1;
	else
		counter_test = counter_test;


always @ *  //(posedge sys_clk)
if(counter_test == 8'd0)// || counter_test == 8'd1 || counter_test == 8'd2 || counter_test == 8'd3)
		test_value = 2'b10;
	else
		test_value = 2'd0;
*/
