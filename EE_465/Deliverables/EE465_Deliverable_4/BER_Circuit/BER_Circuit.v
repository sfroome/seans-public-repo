/************************************************************************************************************************
EE 465 BER Measurement Circuit Top Submodule
Sean Froome
Work In Progress 
***********************************************************************************************************************/
`default_nettype none
module BER_Circuit(
input wire sys_clk,
input wire sam_rate_clk_enable, // aka sam_clk_enable
input wire bit_clk_enable,
input wire sym_clk_enable,
input wire reset,
input wire initialize_key,
input wire [1:0] slicer_out_I,
input wire [1:0] slicer_out_Q,
output wire dummy);
/***********************************************************************************************************
LFSR Initialization
************************************************************************************************************/
wire signed [1:0] q_out_I, q_out_Q;

LFSR LFSR_BER(
  .sys_clk(sys_clk),
  .bit_clk_enable(bit_clk_enable),
  .initialize(initialize_position),
  .q_out_I(q_out_I),
  .q_out_Q(q_out_Q));
/***********************************************************************************************************
Counter
************************************************************************************************************/
reg [24:0] counter;
reg clear_accumulator;
always  @ (posedge sys_clk)
	else if ((sym_clk_enable == 1'b1)  && (counter == 25'd4194304))//&& counter == 22'd1024)
    counter = 25'd0;
  else if(sym_clk_enable == 1'b1)
  	counter = counter + 25'd1;
  else
    counter = counter;

always @ *
  if(reset)
    clear_accumulator = 1'b1;
  else if(counter == 25'd4194304)
  	clear_accumulator = 1'b1;
  else
  	clear_accumulator = 1'b0;
/***************************************************************************************************************************
BER Module
***************************************************************************************************************************/
reg initialize;

always @ (posedge sys_clk)
  initialize = -initialize_key;

reg initialize_position;

always @ (posedge sys_clk)
  if( (initialize == 1'b1) || (q_out_Q[1] == 1'b0))
    initialize_position = 1'b1;
  else
    initialize_position = 1'b0;
/***************************************************************************************************************************
BER Module
***************************************************************************************************************************/
reg s0, s1, s2, s3;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    s3 = slicer_out_Q[1];
  else
    s3 = s3;
always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    s2 = slicer_out_Q[0];
  else
    s2 = s2;
always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    s1 = slicer_out_I[1];
  else
    s1 = s1;
always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    s0 = slicer_out_I[0];
  else
    s0 = s0;

reg [1:0] two_bit_counter;
reg serial_bit;

always @ (posedge sys_clk)
  case(two_bit_counter)
    2'b00: serial_bit = s0;
    2'b01: serial_bit = s1;
    2'b10: serial_bit = s2;
    2'b11: serial_bit = s3;
    default: serial_bit = 1'b0;
  endcase

always  @ (posedge sys_clk)
  if(reset)
    two_bit_counter = 2'b0;
  else
    two_bit_counter = two_bit_counter + 2'b1;
/***************************************************************************************************************************
BER Module
***************************************************************************************************************************/
reg error;
always @ *
  error = serial_bit^initialize_position;
always @ *
  some_other_signal = error & bit_clk_enable;
reg [22:0] error_count;
always @ (posedge sys_clk)
  error_count = some_other_signal+ error_count;
/***************************************************************************************************************************
End
***************************************************************************************************************************/
endmodule
`default_nettype wire
