/*************************************************************************************************************************
EE 465 Deliverable 4 Top Module.
Written by: Sean Froome

This is the top module of the Deliverable. The Deliverable contains an MER
measurement circuit, three filters, as well as a module to upsampled and pass
the transmit filter outputs to the DAC for viewing on the Spectrum Analyzer.

CURRENT SWITCH ASSIGNMENTS:
RESET =  ~KEY[0]
Initialize_LFSR = ~KEY[1]
SW[16] == 1'b1 means the practical transmit filters are used for both I and Q
SW[16] == 1'b0 means the gold filtesr are used

SW[4] Controls the Gaussian Noise Generator
SW[2] Controls the Adjacent Channel
To Control The Delays:
I Select is SW[11:8];
Q Select is SW[15:12];

TODO: Make sure the MER Calculations for the Q-Channel are actually working
*************************************************************************************************************************/
`default_nettype none
module EE465_Deliverable_4(
		input wire clock_50,
		input wire [17:0]SW,
		input wire [3:0] KEY,
		output reg [3:0]  LEDG,
		output reg [17:0] LEDR,
		output reg[13:0]DAC_DA,
		output reg [13:0]DAC_DB,
		output wire DAC_CLK_A,
		output wire DAC_CLK_B,
		output wire DAC_MODE,
		output wire DAC_WRT_A,
		output wire DAC_WRT_B,
    output wire [31:0] MER_dB_Float_I,
    output wire [31:0] MER_dB_Float_Q);
/************************************************************************************************************************
Tie LEDs to Switches and Keys, to make it clear when the switches and keys are active
************************************************************************************************************************/
always @ *
	LEDR = SW;
always @ *
	LEDG = KEY;

reg reset;
always @ *
	reset = ~KEY[1];
/************************************************************************************************************************
Wires for LFSRs, clocks and DACs
************************************************************************************************************************/
wire signed [1:0]  q_out_I;
wire signed [1:0]  q_out_Q;

wire [3:0] I_Select, Q_Select;
assign I_Select = SW[11:8]; // For the Receiver before Downsampling
assign Q_Select = SW[15:12]; // For the Receiver before Downsampling

reg signed [17:0] output_from_filter;
/**************************************************************************************************************
				Setup DACs
***************************************************************************************************************/
assign DAC_CLK_A = sys_clk;
assign DAC_CLK_B = sys_clk;
assign DAC_MODE = 1'b1; //treat DACs seperately
assign DAC_WRT_A = ~sys_clk;
assign DAC_WRT_B = ~sys_clk;

reg signed [13:0] signal_to_DAC;
always @ *
  signal_to_DAC = Combined_Signal_With_Noise[17:4];

always@ (posedge sys_clk)// convert 1s13 format to 0u14 format and send it to DAC A
	DAC_DA = {~signal_to_DAC[13], signal_to_DAC[12:0]};
always@ (posedge sys_clk)
	DAC_DB = {~signal_to_DAC[13], signal_to_DAC[12:0]} ;
/************************************************************************************************************
Clocks Setup
************************************************************************************************************/
reg sys_clk, half_clk_enable, sam_clk_enable, bit_clk_enable, sym_clk_enable;
reg [3:0] four_bit_counter;
reg [3:0] clk_phase;
/***********************************************************************************************************
Counter and Clock Phase Setup
***********************************************************************************************************/
always @ (posedge sys_clk) // Might want to flip this around.
if((four_bit_counter < 4'b1111) || reset == 1'b0)
	four_bit_counter = four_bit_counter + 4'd1;
else
	four_bit_counter = 4'd0;
always @ *
	clk_phase = four_bit_counter;
/***********************************************************************************************************
Sys Clk
***********************************************************************************************************/
always @ (posedge clock_50)
  sys_clk = ~sys_clk;
/***********************************************************************************************************
Clock Enables
***********************************************************************************************************/
always @ (posedge sys_clk)
	if((clk_phase == 4'd1  || clk_phase == 4'd3  || clk_phase == 4'd5 || clk_phase == 4'd7 || clk_phase == 4'd9 ||
      clk_phase == 4'd11 || clk_phase == 4'd13 || clk_phase == 4'd15) && reset == 1'b0)
		half_clk_enable = 1'b1;
	else
		half_clk_enable = 1'b0;

always @ (posedge sys_clk)
	if((clk_phase == 4'd3 || clk_phase == 4'd7 || clk_phase == 4'd11 || clk_phase == 4'd15) && reset == 1'b0)
		sam_clk_enable = 1'b1;
	else
		sam_clk_enable = 1'b0;

always @ (posedge sys_clk)
	if((clk_phase == 4'd7 || clk_phase == 4'd15) && reset == 1'b0)
		bit_clk_enable = 1'b1;
	else
		bit_clk_enable = 1'b0;

always @ (posedge sys_clk)
	if(clk_phase == 4'd15 && reset == 1'b0)
		sym_clk_enable = 1'b1;
	else
		sym_clk_enable = 1'b0;
/***********************************************************************************************************
LFSR Initialization
************************************************************************************************************/
wire initialize_key;
assign initialize_key = ~KEY[0];

LFSR LFSR1(
  .sys_clk(sys_clk),
  .sam_clk_enable(sam_clk_enable),
  .initialize(reset),
  .q_out_I(q_out_I),
  .q_out_Q(q_out_Q));
/*************************************************************************************************************************
Filter Instantiation
**************************************************************************************************************************/
wire signed [17:0] output_gold_tx_I, output_prac_tx_I, output_gold_rx_I;
wire signed [17:0] output_gold_tx_Q, output_prac_tx_Q, output_gold_rx_Q;
reg signed [17:0] output_from_filter_I, output_from_filter_Q;

always @ *
  if(SW[16] == 1'd0)
    output_from_filter_Q = output_gold_tx_Q; // IE: OUTPUT FROM PULSE SHAPING FILTERS
  else // (SW[16] == 1'd1)
    output_from_filter_Q = output_prac_tx_Q; // IE: OUTPUT FROM PULSE SHAPING FILTERS

always @ *
	if(SW[16] == 1'd0)
	 output_from_filter_I = output_gold_tx_I; // IE: OUTPUT FROM PULSE SHAPING FILTERS
  else // (SW[16] == 1'd1)
		output_from_filter_I = output_prac_tx_I; // IE: OUTPUT FROM PULSE SHAPING FILTERS
/*************************************************************************************************************************
Filter Instantiation - I Channel
**************************************************************************************************************************/
TX_PRAC TX_Filter_Practical_I(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.sym_clk_enable(sym_clk_enable),
	.x_in(q_out_I),
	.y_out(output_prac_tx_I));

TX_Gold TX_Filter_Gold_I(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.sym_clk_enable(sym_clk_enable),
	.x_in(q_out_I),
	.y_out(output_gold_tx_I));
/*************************************************************************************************************************
Filter Instantiation - Q Channel
**************************************************************************************************************************/
TX_PRAC TX_Filter_Practical_Q(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.sym_clk_enable(sym_clk_enable),
	.x_in(q_out_Q),
	.y_out(output_prac_tx_Q));

TX_Gold TX_Filter_Gold_Q(
	.sys_clk(sys_clk),
	.reset(reset),
	.sam_clk_enable(sam_clk_enable),
	.sym_clk_enable(sym_clk_enable),
	.x_in(q_out_Q),
	.y_out(output_gold_tx_Q));
/***************************************************************************************************************************
Upsampling and Carrier Module
***************************************************************************************************************************/
wire signed [17:0] Main_Channel_Signal, Combined_Signal_With_Noise ;
wire signed [17:0] Channel_Delay_1_Q, halfband_25_output_I;

Upconversion_and_Noise Deliv_Four_Main_Block(
	.sys_clk(sys_clk),
  .half_clk_enable(half_clk_enable),
	.sam_clk_enable(sam_clk_enable),
	.reset(reset),
  .output_from_filter_I(output_from_filter_I),
	.output_from_filter_Q(output_from_filter_Q),
  .halfband_25_output_I(halfband_25_output_I),
  .Channel_Delay_1_Q(Channel_Delay_1_Q),
  .Main_Channel_Signal(Main_Channel_Signal));
/***************************************************************************************************************************
Adjacent Channel
***************************************************************************************************************************/
wire Noise_Enable, Adjacent_Channel_Enable;
wire [1:0] Gain_Level_Control;

assign Noise_Enable = SW[4];
assign Adjacent_Channel_Enable = SW[2];
assign Gain_Level_Control = SW[1:0];

Adjacent_Channel Other_Deliv_Four_Block(
	.sys_clk(sys_clk),
  .reset(reset),
  .Noise_Enable(Noise_Enable),
  .Adjacent_Channel_Enable(Adjacent_Channel_Enable),
  .Gain_Level_Control(Gain_Level_Control),
  .halfband_25_output_I(halfband_25_output_I),
  .Channel_Delay_1_Q(Channel_Delay_1_Q),
  .Main_Channel_Signal(Main_Channel_Signal),
  .Combined_Signal_With_Noise(Combined_Signal_With_Noise));
/***************************************************************************************************************************
Receiver
***************************************************************************************************************************/
wire signed [17:0] Delayed_Output_Sam_Clk_I, Delayed_Output_Sam_Clk_Q;

Receiver_Module Receiver_Circuit(
  .sys_clk(sys_clk),
  .half_clk_enable(half_clk_enable),
  .sam_clk_enable(sam_clk_enable),
  .sym_clk_enable(sym_clk_enable),
  .reset(reset),
  .I_Select(I_Select),
  .Q_Select(Q_Select),
  .Combined_Signal_With_Noise(Combined_Signal_With_Noise),
  .Delayed_Output_Sam_Clk_I(Delayed_Output_Sam_Clk_I),
  .Delayed_Output_Sam_Clk_Q(Delayed_Output_Sam_Clk_Q));
/***************************************************************************************************************************
MER Module
***************************************************************************************************************************/
wire signed [1:0] slicer_out_I, slicer_out_Q;

MER_Circuit MER_Measurement(
  .sys_clk(sys_clk),
  .sam_clk_enable(sam_clk_enable),
  .sym_clk_enable(sym_clk_enable),
  .reset(reset),
  .Delayed_Output_Sam_Clk_I(Delayed_Output_Sam_Clk_I),
  .Delayed_Output_Sam_Clk_Q(Delayed_Output_Sam_Clk_Q),
  .q_out_I(q_out_I),
  .q_out_Q(q_out_Q),
  .slicer_out_I(slicer_out_I),
  .slicer_out_Q(slicer_out_Q),
  .MER_dB_Float_I(MER_dB_Float_I),
  .MER_dB_Float_Q(MER_dB_Float_Q));
/***************************************************************************************************************************
TODO: BER Module
***************************************************************************************************************************/

/*
BER_Circuit BER_Measurement(
  .sys_clk(sys_clk),
  .sam_rate_clk_enable(sam_clk_enable),
  .bit_clk_enable(bit_clk_enable),
  .sym_clk_enable(sym_clk_enable),
//  .reset(reset),
  .initialize_key(reset),
  .slicer_out_I(slicer_out_I),
  .slicer_out_Q(slicer_out_Q),
);
*/

/***************************************************************************************************************************
End
***************************************************************************************************************************/
endmodule
`default_nettype wire
