/*************************************************************
EE 465 Deliverable 4 Slicer
Written by: Sean Froome
*************************************************************/
`default_nettype none
module slicer(
input wire signed [17:0] reference_level,
input wire signed [17:0] decision_variable,
output reg signed [1:0] slicer_out);
reg signed [17:0] reference_level_negative;

always @ *
  reference_level_negative = -reference_level;
always @ *
  if((decision_variable > reference_level) && (decision_variable > 18'sd0))
    slicer_out = 2'b10; // 0.75
  else if((decision_variable < reference_level) && (decision_variable > 18'sd0) || (decision_variable == reference_level ))
    slicer_out = 2'b11; //0.25
  else if((decision_variable < reference_level_negative) && (decision_variable < 18'sd0 ))
    slicer_out = 2'b00; // -.75
  else if((decision_variable > reference_level_negative) && (decision_variable < 18'sd0) || (decision_variable == reference_level_negative))
    slicer_out = 2'b01; // -0.25
  else
    slicer_out = 2'b00; // IT BETTER NOT HIT THIS STATE EVER!
endmodule
`default_nettype wire
