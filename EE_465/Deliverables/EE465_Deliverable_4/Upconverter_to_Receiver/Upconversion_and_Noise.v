/************************************************************************************************************************
EE 465 Upsampler Circuit Top Submodule
Sean Froome
***********************************************************************************************************************/
`default_nettype none
module Upconversion_and_Noise(
input wire sys_clk,
input wire half_clk_enable,
input wire sam_clk_enable,
input wire reset,
input wire I_Channel_Gain,
input wire Q_Channel_Gain,
input wire signed [17:0] output_from_filter_I,
input wire signed [17:0] output_from_filter_Q,
output reg signed [17:0] Channel_Delay_1_Q,
output wire signed [17:0] halfband_25_output_I,
output reg signed [17:0] Main_Channel_Signal);
/************************************************************************************************************************
UPSAMPLE BY 2 (to 12.5MHz)
***********************************************************************************************************************/
reg signed [17:0] upsampled_12pt5_filter_output_I, upsampled_12pt5_filter_output_Q;

always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1 && sam_clk_enable == 1'b1)
    upsampled_12pt5_filter_output_I = output_from_filter_I;
  else if(half_clk_enable == 1'b1)
    upsampled_12pt5_filter_output_I = 18'd0;
  else
    upsampled_12pt5_filter_output_I = upsampled_12pt5_filter_output_I;

always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1 && sam_clk_enable == 1'b1)
    upsampled_12pt5_filter_output_Q = output_from_filter_Q;
  else if(half_clk_enable == 1'b1)
    upsampled_12pt5_filter_output_Q = 18'd0;
  else
    upsampled_12pt5_filter_output_Q = upsampled_12pt5_filter_output_Q;
/************************************************************************************************************************
HALF BAND FILTERS FOR ANTI-IMAGING/INTERPOLATION
***********************************************************************************************************************
Anti-Imaging @ 12.5MHz
***********************************************************************************************************************/
wire signed [17:0] halfband_12pt5_output_I, halfband_12pt5_output_Q;

HalfBand_Filter_1 HBF1_I(
  .sys_clk(sys_clk),
  .half_clk_enable(half_clk_enable),
  .reset(reset),
  .x_in(upsampled_12pt5_filter_output_I),
  .y_out(halfband_12pt5_output_I));

HalfBand_Filter_1 HBF1_Q(
  .sys_clk(sys_clk),
  .half_clk_enable(half_clk_enable),
  .reset(reset),
  .x_in(upsampled_12pt5_filter_output_Q),
  .y_out(halfband_12pt5_output_Q));
/************************************************************************************************************************
UPSAMPLE BY 2 (to 25MHz)
***********************************************************************************************************************/
reg signed [17:0] upsampled_25_filter_output_I, upsampled_25_filter_output_Q;

always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    upsampled_25_filter_output_I = halfband_12pt5_output_I;
  else
    upsampled_25_filter_output_I = 18'd0;

always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    upsampled_25_filter_output_Q = halfband_12pt5_output_Q;
  else
    upsampled_25_filter_output_Q = 18'd0;
/************************************************************************************************************************
Anti Imaging at @25 MHz
***********************************************************************************************************************/
wire signed [17:0] halfband_25_output_Q;

HalfBand_Filter_2 HBF2_I(
  .sys_clk(sys_clk),
  .reset(reset),
  .x_in(upsampled_25_filter_output_I),
  .y_out(halfband_25_output_I));

HalfBand_Filter_2 HBF2_Q(
  .sys_clk(sys_clk),
  .reset(reset),
  .x_in(upsampled_25_filter_output_Q),
  .y_out(halfband_25_output_Q));
/************************************************************************************************************************
(FAKE) NCO
*************************************************************************************************************************/
reg signed [17:0] I_Channel, Q_Channel;
wire signed [17:0] NCO_sin, NCO_cos;
wire out_valid, clken;
assign clken = 1'b1;

wire signed [31:0] phi;
assign phi = 32'd1073741824; // 6.25 MHz

Fake_NCO Upconvert (
  .clk(sys_clk),       // clk.clk
  .reset_n(~reset),   // rst.reset_n
  .clken(clken),     //  in.clken
  .phi_inc_i(phi), //    .phi_inc_i
  .fsin_o(NCO_sin),    // out.fsin_o
  .fcos_o(NCO_cos), //    .fcos_o
  .out_valid(out_valid));  //  .out_valid

always @ (posedge sys_clk)
  Channel_Delay_1_Q = halfband_25_output_Q;

always @ (posedge sys_clk)
  if(NCO_cos == 18'sd131071)
    I_Channel = halfband_25_output_I;
  else if(NCO_cos == 18'sd0)
    I_Channel = 18'sd0;
  else if(NCO_cos == -18'sd131071)
    I_Channel = -halfband_25_output_I;
  else
    I_Channel = 18'sd0;

always @ (posedge sys_clk)
  if(NCO_sin == 18'sd131071)
    Q_Channel = Channel_Delay_1_Q;
  else if(NCO_sin == 18'sd0)
    Q_Channel = 18'sd0;
  else if(NCO_sin == -18'sd131071)
    Q_Channel = -Channel_Delay_1_Q;
  else
    Q_Channel = 18'sd0;

always @  (posedge sys_clk)
  Main_Channel_Signal = I_Channel+ Q_Channel;
/************************************************************************************************************************
END
*************************************************************************************************************************/
endmodule
`default_nettype wire
