/************************************************************************************************************************
EE 465 Adjacent Channel Submodule
Sean Froome

TODO: SW[1:0] controls Gain
SW[2] Enables Adjacent Channel
SW[4] Enables Noise Generator
***********************************************************************************************************************/
`default_nettype none
module Adjacent_Channel(
input wire sys_clk,
input wire reset,
input wire Noise_Enable,
input wire Adjacent_Channel_Enable,
input wire [1:0] Gain_Level_Control,
input wire signed [17:0] halfband_25_output_I,
input wire signed [17:0] Channel_Delay_1_Q,
input wire signed [17:0] Main_Channel_Signal,
output reg signed [17:0] Combined_Signal_With_Noise);
/************************************************************************************************************************
(FAKE) NCO for the Adjacent Channel Carrier Frequency
*************************************************************************************************************************/
wire signed [17:0] NCO_sin, NCO_cos;

wire out_valid, clken;
assign clken = 1'b1;

wire signed [31:0] phi;
assign phi = 32'd1374389535; // 6.25+1.75 = 8MHz

Fake_NCO2 Upconvert_Adj_Channel (
  .clk(sys_clk),
  .reset_n(~reset),
  .clken(clken),
  .phi_inc_i(phi),
  .fsin_o(NCO_sin),
  .fcos_o(NCO_cos),
  .out_valid(out_valid));
/************************************************************************************************************************
Adding the Adjacent Channel
*************************************************************************************************************************/
(* noprune *) reg signed [35:0] I_Channel_u, Q_Channel_u;
(* noprune *) reg signed [17:0] I_Channel, Q_Channel;
(* noprune *) reg signed [17:0] Adjacent_Channel_Signal;

always @ (posedge sys_clk)
  I_Channel_u = halfband_25_output_I*NCO_cos;
always @ (posedge sys_clk)
  Q_Channel_u = Channel_Delay_1_Q*NCO_sin;

always @ *
  I_Channel = I_Channel_u[34:17]; // Hmmm....
always @ *
  Q_Channel = Q_Channel_u[34:17];
always @  (posedge sys_clk)
  Adjacent_Channel_Signal = I_Channel + Q_Channel;
/************************************************************************************************************************
Combined Signal
*************************************************************************************************************************/
reg signed [17:0] Combined_Channel_Signals;

always @ (posedge sys_clk)
  if(Adjacent_Channel_Enable == 1'b1)
    Combined_Channel_Signals = Adjacent_Channel_Signal + Main_Channel_Signal;
  else // if(Adjacent_Channel_Signal == 1'b0)
    Combined_Channel_Signals = Main_Channel_Signal;
/************************************************************************************************************************
Gain Selector
*************************************************************************************************************************/
(* noprune *) reg signed [35:0] Amplified_Combined_Signal_u;
(* noprune *) reg signed [17:0] Amplified_Combined_Signal;

localparam  G0 = 0;
//localparam  G1 = 18'sd28498;  // 7s13 (Wait what was I typing?!)
//localparam  G2 = 18'sd52338;  // 7s13
//localparam  G3 = 18'sd77059; // 7s13
//localparam  G1 = 18'sd38022;  // 8s14
//localparam  G2 = 18'sd69829;  // 8s14
//localparam  G3 = 18'sd102811; // 8s14 (wat? they're 18 bit numbers Sean... wat?!)

localparam  G1 = 18'sd37971;  // 10s8
localparam  G2 = 18'sd69735;  // 10s8
localparam  G3 = 18'sd102671; // 10s8

always @  *
  case(Gain_Level_Control)
     2'b00: Amplified_Combined_Signal_u = 36'sd0;
     2'b01: Amplified_Combined_Signal_u = Combined_Channel_Signals*G1; // 1s17*8s10 = 9s27
     2'b10: Amplified_Combined_Signal_u = Combined_Channel_Signals*G2; // 1s17*8s10 = 9s27
     2'b11: Amplified_Combined_Signal_u = Combined_Channel_Signals*G3; // 1s17*8s10 = 9s27
     default: Amplified_Combined_Signal_u = 36'sd0;
   endcase

always @ *
  case(Gain_Level_Control)
    2'b00: Amplified_Combined_Signal = Combined_Channel_Signals; // 18'sd0;
    2'b01: Amplified_Combined_Signal = Amplified_Combined_Signal_u[32:15]; // 9s27 to 1s17, with a bit of scaling... since I probably don't entirely know what tf I'm doing....
    2'b10: Amplified_Combined_Signal = Amplified_Combined_Signal_u[32:15]; // 9s27 to 1s17
    2'b11: Amplified_Combined_Signal = Amplified_Combined_Signal_u[32:15]; // 9s27 to 1s17
    // Either 31:14 or 30:13. Stahp messing with this further....

    // Try
    default: Amplified_Combined_Signal = 18'sd0;
  endcase
/************************************************************************************************************************
Adding Noise
*************************************************************************************************************************/
wire signed [17:0] awgn_out;

awgn_generator AWGN(
 .clk(sys_clk),
 .clk_en(Noise_Enable),
 .reset_n(~reset),  //active LOW reset
 .awgn_out(awgn_out));

always @ (posedge sys_clk)
  if(Noise_Enable == 1'b1)
    Combined_Signal_With_Noise = awgn_out;// + Amplified_Combined_Signal;
  else // if (Noise_Enable == 1'b0)
    Combined_Signal_With_Noise = Amplified_Combined_Signal;
//TODO: fix these so when the gain is sorted out.
/************************************************************************************************************************
END
*************************************************************************************************************************/
endmodule
`default_nettype wire
