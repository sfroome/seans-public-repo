
/************************************************************************************************************************
EE 465 Receiver Submodule

Downsamples everything and passes it through the matched filter. Then onto the MER and BER measurement circuits
(BER circuit doesn't currently exist)
Sean Froome

To Control The Delays:
I Select is SW[11:8];
Q Select is SW[15:12];
*************************************************************************************************************************/
`default_nettype none
module Receiver_Module(
input wire sys_clk,
input wire half_clk_enable,
input wire sam_clk_enable,
input wire sym_clk_enable,
input wire reset,
input wire [3:0] I_Select,
input wire [3:0] Q_Select,
input wire signed [17:0] Combined_Signal_With_Noise,
output reg signed [17:0] Delayed_Output_Sam_Clk_I,
output reg signed [17:0] Delayed_Output_Sam_Clk_Q);
/***************************************************************************************************************************
Fake NCO 1 Again
***************************************************************************************************************************/
wire signed [17:0] NCO_sin, NCO_cos;
wire out_valid, clken;
assign clken = 1'b1;
wire signed [31:0] phi;
assign phi = 32'd1073741824; // 6.25 MHz

Fake_NCO Demodulate (
  .clk(sys_clk),       // clk.clk
  .reset_n(~reset),   // rst.reset_n
  .clken(clken),     //  in.clken
  .phi_inc_i(phi), //    .phi_inc_i
  .fsin_o(NCO_sin),    // out.fsin_o
  .fcos_o(NCO_cos), //    .fcos_o
  .out_valid(out_valid));  //  .out_valid
/***************************************************************************************************************************
Half Band Filters Again
***************************************************************************************************************************/
(* noprune *) reg signed [35:0] demod_I_u, demod_Q_u;
(* noprune *) reg signed [17:0] demod_I, demod_Q;

always @ (posedge sys_clk)
  demod_I_u = NCO_cos*Combined_Signal_With_Noise;
always @ (posedge sys_clk)
  demod_Q_u = NCO_sin*Combined_Signal_With_Noise;
always @ *
  demod_I = demod_I_u[34:17];
always @ *
  demod_Q = demod_Q_u[34:17];
/***************************************************************************************************************************
One Delay before Downsampling and Filtering
***************************************************************************************************************************/
(* noprune *) reg signed [17:0] Channel_Delay_R_I; // To match the single delay for Q back at the Transmitter side.
(* noprune *) reg signed [17:0] I_Delay_1, Q_Delay_1;
always @ (posedge sys_clk)
  Channel_Delay_R_I = demod_I;
always @ (posedge sys_clk)
  I_Delay_1 = Channel_Delay_R_I;
always @ (posedge sys_clk)
  Q_Delay_1 = demod_Q;
/***************************************************************************************************************************
Half Band Filters Again
***************************************************************************************************************************/
wire signed [17:0] halfband_12pt5_I, halfband_12pt5_Q;
HalfBand_Filter_2 to_12pt5_I(
  .sys_clk(sys_clk),
  .reset(reset),
  .x_in(I_Delay_1),
  .y_out(halfband_12pt5_I));

HalfBand_Filter_2 to_12pt5_Q(
  .sys_clk(sys_clk),
  .reset(reset),
  .x_in(Q_Delay_1),
  .y_out(halfband_12pt5_Q));
/***************************************************************************************************************************
One Round of Downsampling
***************************************************************************************************************************/
reg signed [17:0] Downsample_to_12pt5_I, Downsample_to_12pt5_Q;

always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    Downsample_to_12pt5_I = halfband_12pt5_I;
  else
    Downsample_to_12pt5_I = Downsample_to_12pt5_I;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    Downsample_to_12pt5_Q = halfband_12pt5_Q;
  else
    Downsample_to_12pt5_Q = Downsample_to_12pt5_Q;
/***************************************************************************************************************************
Half Band Filters Again
***************************************************************************************************************************/
wire signed [17:0] halfband_6pt25_I, halfband_6pt25_Q;

HalfBand_Filter_1 to_6pt5_I(
  .sys_clk(sys_clk),
  .half_clk_enable(half_clk_enable),
  .reset(reset),
  .x_in(Downsample_to_12pt5_I),
  .y_out(halfband_6pt25_I));

HalfBand_Filter_1 to_6pt5_Q(
  .sys_clk(sys_clk),
  .half_clk_enable(half_clk_enable),
  .reset(reset),
  .x_in(Downsample_to_12pt5_Q),
  .y_out(halfband_6pt25_Q));
/***************************************************************************************************************************
Second Round of Downsampling
***************************************************************************************************************************/
reg signed [17:0] Downsample_to_6pt25_I, Downsample_to_6pt25_Q;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    Downsample_to_6pt25_I = halfband_6pt25_I;
  else
    Downsample_to_6pt25_I = Downsample_to_6pt25_I;

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    Downsample_to_6pt25_Q = halfband_6pt25_Q;
  else
    Downsample_to_6pt25_Q = Downsample_to_6pt25_Q;
/***************************************************************************************************************************
RX Matched Filters
***************************************************************************************************************************/
wire signed [17:0] output_gold_rx_I, output_gold_rx_Q;

RX_Gold RX_Filter_Gold_I(
  .sys_clk(sys_clk),
  .reset(reset),
  .sam_clk_enable(sam_clk_enable),
  .x_in(Downsample_to_6pt25_I),
  .y_out(output_gold_rx_I));

RX_Gold RX_Filter_Gold_Q(
  .sys_clk(sys_clk),
  .reset(reset),
  .sam_clk_enable(sam_clk_enable),
  .x_in(Downsample_to_6pt25_Q),
  .y_out(output_gold_rx_Q));
/************************************************************************************************************************
Delay Outputs from Filters
I SELECT USES SW[11:8]
Q SELECT USES SW[15:12]
***********************************************************************************************************************/
reg signed  [17:0] delay_0_I,  delay_1_I,  delay_2_I,  delay_3_I;
reg signed  [17:0] delay_4_I,  delay_5_I,  delay_6_I,  delay_7_I;
reg signed  [17:0] delay_8_I,  delay_9_I,  delay_10_I, delay_11_I;
reg signed  [17:0] delay_12_I, delay_13_I, delay_14_I, delay_15_I;

reg signed  [17:0] delay_0_Q,  delay_1_Q,  delay_2_Q,  delay_3_Q;
reg signed  [17:0] delay_4_Q,  delay_5_Q,  delay_6_Q,  delay_7_Q;
reg signed  [17:0] delay_8_Q,  delay_9_Q,  delay_10_Q, delay_11_Q;
reg signed  [17:0] delay_12_Q, delay_13_Q, delay_14_Q, delay_15_Q;
/************************************************************************************************************************
Delays for the I-Channel
*************************************************************************************************************************/
always @ (posedge sys_clk)
  delay_0_I = output_gold_rx_I;
always @ (posedge sys_clk)
  delay_1_I = delay_0_I;
always @ (posedge sys_clk)
  delay_2_I = delay_1_I;
always @ (posedge sys_clk)
  delay_3_I = delay_2_I;
always @ (posedge sys_clk)
  delay_4_I = delay_3_I;
always @ (posedge sys_clk)
  delay_5_I = delay_4_I;
always @ (posedge sys_clk)
  delay_6_I = delay_5_I;
always @ (posedge sys_clk)
  delay_7_I = delay_6_I;
always @ (posedge sys_clk)
  delay_8_I = delay_7_I;
always @ (posedge sys_clk)
  delay_9_I = delay_8_I;
always @ (posedge sys_clk)
  delay_10_I = delay_9_I;
always @ (posedge sys_clk)
  delay_11_I = delay_10_I;
always @ (posedge sys_clk)
  delay_12_I = delay_11_I;
always @ (posedge sys_clk)
  delay_13_I = delay_12_I;
always @ (posedge sys_clk)
  delay_14_I = delay_13_I;
always @ (posedge sys_clk)
  delay_15_I = delay_14_I;
/************************************************************************************************************************
Delay Selecter for the I-Channel
*************************************************************************************************************************/
always @ (posedge sys_clk)
  case(I_Select)
    4'b0000: Delayed_Output_Sam_Clk_I =  delay_0_I;
    4'b0001: Delayed_Output_Sam_Clk_I =  delay_1_I;
    4'b0010: Delayed_Output_Sam_Clk_I =  delay_2_I;
    4'b0011: Delayed_Output_Sam_Clk_I =  delay_3_I;
    4'b0100: Delayed_Output_Sam_Clk_I =  delay_4_I;
    4'b0101: Delayed_Output_Sam_Clk_I =  delay_5_I;
    4'b0110: Delayed_Output_Sam_Clk_I =  delay_6_I;
    4'b0111: Delayed_Output_Sam_Clk_I =  delay_7_I;
    4'b1000: Delayed_Output_Sam_Clk_I =  delay_8_I;
    4'b1001: Delayed_Output_Sam_Clk_I =  delay_9_I;
    4'b1010: Delayed_Output_Sam_Clk_I =  delay_10_I;
    4'b1011: Delayed_Output_Sam_Clk_I =  delay_11_I;
    4'b1100: Delayed_Output_Sam_Clk_I =  delay_12_I;
    4'b1101: Delayed_Output_Sam_Clk_I =  delay_13_I;
    4'b1110: Delayed_Output_Sam_Clk_I =  delay_14_I;
    4'b1111: Delayed_Output_Sam_Clk_I =  delay_15_I;
    default: Delayed_Output_Sam_Clk_I = 18'sd0;
  endcase
/************************************************************************************************************************
Delays for Q-Channel
*************************************************************************************************************************/
always @ (posedge sys_clk)
  delay_0_Q = output_gold_rx_Q;
always @ (posedge sys_clk)
  delay_1_Q = delay_0_Q;
always @ (posedge sys_clk)
  delay_2_Q = delay_1_Q;
always @ (posedge sys_clk)
  delay_3_Q = delay_2_Q;
always @ (posedge sys_clk)
  delay_4_Q = delay_3_Q;
always @ (posedge sys_clk)
  delay_5_Q = delay_4_Q;
always @ (posedge sys_clk)
  delay_6_Q = delay_5_Q;
always @ (posedge sys_clk)
  delay_7_Q = delay_6_Q;
always @ (posedge sys_clk)
  delay_8_Q = delay_7_Q;
always @ (posedge sys_clk)
  delay_9_Q = delay_8_Q;
always @ (posedge sys_clk)
  delay_10_Q = delay_9_Q;
always @ (posedge sys_clk)
  delay_11_Q = delay_10_Q;
always @ (posedge sys_clk)
  delay_12_Q = delay_11_Q;
always @ (posedge sys_clk)
  delay_13_Q = delay_12_Q;
always @ (posedge sys_clk)
  delay_14_Q = delay_13_Q;
always @ (posedge sys_clk)
  delay_15_Q = delay_14_Q;
/************************************************************************************************************************
Delay Selecter for the Q-Channel
************************************************************************************************************************/
always @ (posedge sys_clk)
  case(Q_Select)
    4'b0000: Delayed_Output_Sam_Clk_Q =  delay_0_Q;
    4'b0001: Delayed_Output_Sam_Clk_Q =  delay_1_Q;
    4'b0010: Delayed_Output_Sam_Clk_Q =  delay_2_Q;
    4'b0011: Delayed_Output_Sam_Clk_Q =  delay_3_Q;
    4'b0100: Delayed_Output_Sam_Clk_Q =  delay_4_Q;
    4'b0101: Delayed_Output_Sam_Clk_Q =  delay_5_Q;
    4'b0110: Delayed_Output_Sam_Clk_Q =  delay_6_Q;
    4'b0111: Delayed_Output_Sam_Clk_Q =  delay_7_Q;
    4'b1000: Delayed_Output_Sam_Clk_Q =  delay_8_Q;
    4'b1001: Delayed_Output_Sam_Clk_Q =  delay_9_Q;
    4'b1010: Delayed_Output_Sam_Clk_Q =  delay_10_Q;
    4'b1011: Delayed_Output_Sam_Clk_Q =  delay_11_Q;
    4'b1100: Delayed_Output_Sam_Clk_Q =  delay_12_Q;
    4'b1101: Delayed_Output_Sam_Clk_Q =  delay_13_Q;
    4'b1110: Delayed_Output_Sam_Clk_Q =  delay_14_Q;
    4'b1111: Delayed_Output_Sam_Clk_Q =  delay_15_Q;
    default: Delayed_Output_Sam_Clk_Q = 18'sd0;
  endcase
/************************************************************************************************************************
END
*************************************************************************************************************************/
endmodule
`default_nettype wire
