/*************************************************************************************************************************
EE 465 Deliverable 3 Gold Matched Filter
Sean Froome

Currently using 64 multipliers.

*************************************************************************************************************************/
`default_nettype none
module RX_Gold(
input wire sys_clk,
input wire reset,
input wire sam_clk_enable,
input wire signed [17:0] x_in,
output reg signed [17:0] y_out);
integer i;
/******************************************************************************************************************
Coefficient Instantiation and Declarations
*******************************************************************************************************************/
wire signed [17:0] a[64:0];
assign a[0]  = -18'sd45;
assign a[1]  = -18'sd17;
assign a[2]  =  18'sd26;
assign a[3]  =  18'sd55;
assign a[4]  =  18'sd46;
assign a[5]  =  18'sd3;
assign a[6]  = -18'sd46;
assign a[7]  = -18'sd65;
assign a[8]  = -18'sd39;
assign a[9]  =  18'sd16;
assign a[10] =  18'sd63;
assign a[11] =  18'sd67;
assign a[12] =  18'sd24;
assign a[13] = -18'sd38;
assign a[14] = -18'sd75;
assign a[15] = -18'sd58;
assign a[16] =  18'sd2;
assign a[17] =  18'sd61;
assign a[18] =  18'sd77;
assign a[19] =  18'sd34;
assign a[20] = -18'sd37;
assign a[21] = -18'sd82;
assign a[22] = -18'sd64;
assign a[23] =  18'sd8;
assign a[24] =  18'sd82;
assign a[25] =  18'sd96;
assign a[26] =  18'sd30;
assign a[27] = -18'sd73;
assign a[28] = -18'sd136;
assign a[29] = -18'sd97;
assign a[30] =  18'sd30;
assign a[31] =  18'sd163;
assign a[32] =  18'sd195;
assign a[33] =  18'sd80;
assign a[34] = -18'sd126;
assign a[35] = -18'sd283;
assign a[36] = -18'sd260;
assign a[37] = -18'sd36;
assign a[38] =  18'sd266;
assign a[39] =  18'sd438;
assign a[40] =  18'sd326;
assign a[41] = -18'sd47;
assign a[42] = -18'sd466;
assign a[43] = -18'sd637;
assign a[44] = -18'sd389;
assign a[45] =  18'sd188;
assign a[46] =  18'sd752;
assign a[47] =  18'sd902;
assign a[48] =  18'sd447;
assign a[49] = -18'sd421;
assign a[50] = -18'sd1182;
assign a[51] = -18'sd1279;
assign a[52] = -18'sd495;
assign a[53] =  18'sd830;
assign a[54] =  18'sd1905;
assign a[55] =  18'sd1908;
assign a[56] =  18'sd532;
assign a[57] = -18'sd1685;
assign a[58] = -18'sd3471;
assign a[59] = -18'sd3380;
assign a[60] = -18'sd555;
assign a[61] =  18'sd4699;
assign a[62] =  18'sd10879;
assign a[63] =  18'sd15846;
assign a[64] =  18'sd17746;
/************************************************************************************************************************
REGISTER DECLARATIONS
************************************************************************************************************************/
reg signed [17:0] x[128:0];
reg signed [17:0] sum_level_1[64:0];
reg signed [35:0] multiplier_out[64:0];
reg signed [17:0] multiplier_out_t[64:0];
reg signed [17:0] sum_level_2[32:0];
reg signed [17:0] sum_level_3[16:0];
reg signed [17:0] sum_level_4[8:0];
reg signed [17:0] sum_level_5[4:0];
reg signed [17:0] sum_level_6[2:0];
reg signed [17:0] sum_level_7;
/************************************************************************************************************************
Top Level Tap Assignments
************************************************************************************************************************/
always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    x[0]  <= {x_in[17], x_in[17:1]};
  else
    x[0] <= x[0];

always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
		begin
			for(i=1; i<129; i=i+1)
				x[i] <= x[i-1];
		end
/************************************************************************************************************************
Summing Level 1
************************************************************************************************************************/
always @ *// Not Clocked so it should be fine
	begin
		for(i=0; i<=63; i=i+1)
			sum_level_1[i] = x[i]+x[128-i]; //2s17 - need room to add both coefficients togther
	end

always@ *
	 sum_level_1[64] = x[64]; //2s17
/************************************************************************************************************************
Multiplier Outputs
************************************************************************************************************************/
always @ *
 	begin
 		for(i=0; i<= 64; i=i+1)
 			multiplier_out[i] = a[i] * sum_level_1[i];
 	end

always @ *
	begin
    for(i=0; i <= 64; i=i+1)
  		multiplier_out_t[i] = multiplier_out[i][33:16];
   	end
/************************************************************************************************************************
Summing Level 2
************************************************************************************************************************/
always @ *//(posedge sys_clk)
	begin
		for(i=0; i < 32; i=i+1)
		  sum_level_2[i] = multiplier_out_t[i] + multiplier_out_t[64-i];
 	end
always @ *
	 sum_level_2[32] = multiplier_out_t[32];
/************************************************************************************************************************
Summing Level 3
************************************************************************************************************************/
always @ *
		begin
			for(i=0;i<16;i=i+1)
				sum_level_3[i] = sum_level_2[i] + sum_level_2[32-i];
		end
always @ *
	sum_level_3[16] = sum_level_2[16];
/************************************************************************************************************************
  Summing Level 4
************************************************************************************************************************/
always @ *
		begin
			for(i=0;i<8;i=i+1)
				sum_level_4[i] = sum_level_3[i] + sum_level_3[16-i];
		end
always @ *
	sum_level_4[8] = sum_level_3[8];

/************************************************************************************************************************
  Summing Level 5
************************************************************************************************************************/
always @ *
		begin
			for(i=0;i<4;i=i+1)
				sum_level_5[i] = sum_level_4[i] + sum_level_4[8-i];
		end
always @ *
	sum_level_5[4] = sum_level_4[4];
/************************************************************************************************************************
  Summing Level 6
************************************************************************************************************************/
always @ *
		begin
			for(i=0;i<2;i=i+1)
				sum_level_6[i] = sum_level_5[i] + sum_level_5[4-i];
		end
always @ *
	sum_level_6[2] = sum_level_5[2];
/************************************************************************************************************************
  Summing Level 7
************************************************************************************************************************/
always @ *
		begin
				sum_level_7 = sum_level_6[0] + sum_level_6[1] + sum_level_6[2];
		end
/************************************************************************************************************************
The Output
************************************************************************************************************************/
always @ (posedge sys_clk)
  if(sam_clk_enable == 1'b1)
    y_out = sum_level_7 << 1;
  else
    y_out = y_out;


//always @ (posedge sys_clk) // For Debugging only.
//  y_out = x_in;

endmodule
`default_nettype wire
