/***************************************************************
EE 465 Deliverable 4 Anti Imaging Filter

For upsampling from 6.25MHz to 25 MHz.
Sean Froome
TODO: Minimize the Multipliers
****************************************************************/
`default_nettype none
module HalfBand_Filter_1(
input wire sys_clk,
input wire half_clk_enable,
input wire reset,
input wire signed [17:0] x_in,
output reg signed [17:0] y_out);
wire signed [17:0] a0,a1,a2,a3;
assign a0 = -18'sd4744;
assign a1 =  18'sd0;
assign a2 =  18'sd37451;
assign a3 =  18'sd65536;
/************************************************************************************************************************
Wire Declarations and Assignments
*************************************************************************************************************************/
reg signed [17:0] x0, x1, x2, x3, x4, x5, x6;
reg signed [17:0] sum_level1_0, sum_level1_1;
reg signed [17:0] sum_level1_2, sum_level1_3;
reg signed [35:0] multiplier_out0, multiplier_out1, multiplier_out2;
reg signed [35:0] multiplier_out3;
reg signed [17:0] sum_out;
/************************************************************************************************************************
Register Assignments for the Top Level Taps
*************************************************************************************************************************/
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    x0 <= {x_in[17], x_in[17:1]};
  else
    x0 <= x0;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    x1 <= x0;
  else
    x1 <= x1;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    x2 <= x1;
  else
    x2 <= x2;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    x3 <= x2;
  else
    x3 <= x3;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    x4 <= x3;
  else
    x4 <= x4;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    x5 <= x4;
  else
    x5 <= x5;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    x6 <= x5;
  else
    x6 <= x6;
/************************************************************************************************************************
Summing Levels
*************************************************************************************************************************/
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    sum_level1_0 = x0+x6;
  else
    sum_level1_0 = sum_level1_0;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    sum_level1_1 = x1+x5;
  else
    sum_level1_1 = sum_level1_1;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    sum_level1_2 = x4+x2;
  else
    sum_level1_2 = sum_level1_2;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    sum_level1_3 = x3;
  else
    sum_level1_3 = sum_level1_3;
/************************************************************************************************************************
Register and Wire Declarations
*************************************************************************************************************************/
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    multiplier_out0 <= a0*sum_level1_0;
  else
    multiplier_out0 = multiplier_out0;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    multiplier_out1 <= a1*sum_level1_1;
  else
    multiplier_out1 = multiplier_out1;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    multiplier_out2 <= a2*sum_level1_2;
  else
    multiplier_out2 = multiplier_out2;
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    multiplier_out3 <= a3*sum_level1_3;
  else
    multiplier_out3 = multiplier_out3;
/************************************************************************************************************************
Sum of Multipliers
*************************************************************************************************************************/
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    sum_out = multiplier_out0[34:17] + multiplier_out1[34:17] + multiplier_out2[34:17] + multiplier_out3[34:17];
  else
    sum_out = sum_out;
/************************************************************************************************************************
Output
*************************************************************************************************************************/
always @ (posedge sys_clk)
  if(half_clk_enable == 1'b1)
    y_out = sum_out;
  else
    y_out = y_out;
/************************************************************************************************************************
End
*************************************************************************************************************************/
endmodule
`default_nettype wire
