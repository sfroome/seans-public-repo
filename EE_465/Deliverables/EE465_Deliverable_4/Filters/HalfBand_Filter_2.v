/***************************************************************
EE 465 Deliverable 4 Anti Imaging Filter 2

For upsampling from 12.5MHz to 25 MHz.

Sean Froome
TODO: Minimize the Multipliers
****************************************************************/
`default_nettype none
module HalfBand_Filter_2(
input wire sys_clk,
input wire reset,
input wire signed [17:0] x_in,
output reg signed [17:0] y_out);
/************************************************************************************************************************
Wire Declarations and Assignments
*************************************************************************************************************************/
wire signed [17:0] a0,a1,a2,a3;
assign a0 = -18'sd4248;
assign a1 =  18'sd0;
assign a2 =  18'sd37012;
assign a3 =  18'sd65536;
/************************************************************************************************************************
Register Declarations
*************************************************************************************************************************/
reg signed [17:0] x0, x1, x2, x3, x4, x5, x6;
reg signed [17:0] sum_level1_0, sum_level1_1;
reg signed [17:0] sum_level1_2, sum_level1_3;
reg signed [35:0] multiplier_out0, multiplier_out1, multiplier_out2;
reg signed [35:0] multiplier_out3;
reg signed [17:0] sum_out;
/************************************************************************************************************************
Register Assignments for the Top Level Taps
*************************************************************************************************************************/
always @ (posedge sys_clk)
  x0 = {x_in[17], x_in[17:1]};
always @ (posedge sys_clk)
  x1 <= x0;
always @ (posedge sys_clk)
  x2 <= x1;
always @ (posedge sys_clk)
  x3 <= x2;
always @ (posedge sys_clk)
  x4 <= x3;
always @ (posedge sys_clk)
  x5 <= x4;
always @ (posedge sys_clk)
  x6 <= x5;
/************************************************************************************************************************
Summing Levels
*************************************************************************************************************************/
always @ (posedge sys_clk)
  sum_level1_0 = x0 + x6;
always @ (posedge sys_clk)
  sum_level1_1 = x1 + x5;
always @ (posedge sys_clk)
  sum_level1_2 = x4 + x2;
always @ (posedge sys_clk)
  sum_level1_3 = x3;
/************************************************************************************************************************
Outputs of Multipliers
*************************************************************************************************************************/
always @ (posedge sys_clk)
  multiplier_out0 <= a0*sum_level1_0;
always @ (posedge sys_clk)
  multiplier_out1 <= a1*sum_level1_1;
always @ (posedge sys_clk)
  multiplier_out2 <= a2*sum_level1_2;
always @ (posedge sys_clk)
  multiplier_out3 <= a3*sum_level1_3;
/************************************************************************************************************************
Sum of Multipliers and Final Output
*************************************************************************************************************************/
always @ (posedge sys_clk)
  sum_out = multiplier_out0[34:17] + multiplier_out1[34:17] + multiplier_out2[34:17] + multiplier_out3[34:17];
always @ (posedge sys_clk)
  y_out = sum_out <<< 2;
/************************************************************************************************************************
End
*************************************************************************************************************************/
endmodule
`default_nettype wire
