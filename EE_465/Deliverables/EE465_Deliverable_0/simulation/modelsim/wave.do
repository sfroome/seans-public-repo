onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix binary /test_tb/sys_clk
add wave -noupdate -radix decimal /test_tb/reset
add wave -noupdate -radix decimal /test_tb/counter
add wave -noupdate -format Analog-Step -height 84 -max 131071.00000000001 -min -131072.0 -radix decimal /test_tb/x_in
add wave -noupdate -format Analog-Step -height 84 -max 131050.99999999999 -min -66951.0 -radix decimal /test_tb/y_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {752 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 356
configure wave -valuecolwidth 350
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {3150 ns}
