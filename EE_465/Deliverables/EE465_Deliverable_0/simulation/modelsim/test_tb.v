module test_tb;
// Example testbench provided to EE 465 class for the following purposes:
// - to provide a basic template to help the students get started writing testbenches
// - to illustrate the correct and incorrect way to generate input stimulus signals

reg sys_clk;
reg reset;

reg [17:0] x_in;
reg [8:0] counter;
wire [17:0] y_out;

localparam PERIOD = 10;
localparam RESET_DELAY = 2;
localparam RESET_LENGTH = 12;

// Clock generation (OK to use hardcoded delays #)
initial
begin
  sys_clk = 0;
  forever
    begin
      #(PERIOD/2);
      sys_clk = ~sys_clk;
    end
end

always @(posedge sys_clk)
  if(reset)
    begin
      counter = 8'd0;
    end
//  else if (counter == 6'd48 )
  else if (counter == 8'd18)
    begin
        counter  = 8'd1;
    end
  else
    begin
        counter = counter + 8'd1;
    end
/*************************************************
FOR PART A
This is for testing the Impulse Response.
**************************************************
always @ (posedge sys_clk)
  if(reset)
    begin
      x_in = 18'd0;
    end
  else if (counter == 8'd1)
    begin
      x_in = 18'h1FFFF;
    end
  else
    begin
      x_in = 18'd0;
    end
/*************************************************
        END OF PART A
*************************************************/

/*************************************************
Reset generation (OK to use hardcoded delays #)
*************************************************/
initial
  begin
    reset = 0;
    #(RESET_DELAY);
    reset = 1;
    #(RESET_LENGTH);
    reset = 0;
  end
/*************************************************
FOR PART B
**************************************************/
always @ (posedge sys_clk)
if(reset)
begin
x_in = 18'd0;
end
else if (counter == 8'd1) //x0
begin
x_in = 18'h1FFFF;
end
else if (counter == 8'd2) //x1
begin
x_in = 18'h20000;
end
else if (counter == 8'd3) //x2
begin
x_in = 18'h20000;
end
else if (counter == 8'd4) //x3
begin
x_in = 18'h20000;
end
else if (counter == 8'd5) //x4
begin
x_in = 18'h20000;
end
else if (counter == 8'd6) //x5
begin
x_in = 18'h1FFFF;
end
else if (counter == 8'd7) //x6
begin
x_in = 18'h1FFFF;
end
else if (counter == 8'd8) //x7
begin
x_in = 18'h1FFFF;
end
else if (counter == 8'd9) //x8 (AKA MAX/MIDDLE POINT)
begin
x_in = 18'h1FFFF;
end
else if (counter == 7'd10) //x9
begin
x_in = 18'h1FFFF;
end
else if (counter == 7'd11) //x10
begin
x_in = 18'h1FFFF;
end
else if (counter == 7'd12) //x11
begin
x_in = 18'h1FFFF;
end
else if (counter == 7'd13) //x12
begin
x_in = 18'h20000;
end
else if (counter == 7'd14) //x13
begin
x_in = 18'h20000;
end
else if (counter == 7'd15) //x14
begin
x_in = 18'h20000;
end
else if (counter == 7'd16) //x15
begin
x_in = 18'h20000;
end
else if (counter == 7'd17) //x16
begin
x_in = 18'h1FFFF;
end
//else if (counter == 7'd18) // END OF FILTER
//begin
//x_in = 18'd0;
//end
else
begin
x_in = 18'd0;
end
/**********************************************************
END OF PART B
**********************************************************/

/**********************************************************
PART A FILTER
**********************************************************
srrc_filter filter1(
  .sys_clk(sys_clk),
  .x_in(x_in),
  .y_out(y_out));
/**********************************************************
  END OF PART A FILTER
**********************************************************
PART B FILTER
**********************************************************/
srrc_filter2 filter1(
    .sys_clk(sys_clk),
    .x_in(x_in),
    .y_out(y_out));
/**********************************************************
END OF PART B FILTER.
**********************************************************/
endmodule

// This is for testing the Worst Case Input.
//  18'h2FFFF is -65537.
// 18'h1FFFF is 131071
// 18'h20000 is -131072.
