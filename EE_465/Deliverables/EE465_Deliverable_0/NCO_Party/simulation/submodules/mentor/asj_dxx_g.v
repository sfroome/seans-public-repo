// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 07:24:49 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
j9fwb5ZL5Q759aa2peXYKi+D1mzAB9dFMdc13uQb2PnqlEs7dYxeiJyEmm+RZI0z
tBPt/ZAURsT6sg9zrgwLJs83ytRXSYzjveQdBwmd7a8oYE1zqVzDhgtjzPS4P+N5
u3iefm5P7mV/zR3eur21mSnmW+gAc09W4hehjvb3Yss=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2608)
TflT3AysvpWLXwX7WvJQJC0+cRMpnT2O+PvRFTZMGbbn0Yhy9Mx6wbsClS2kDfb1
vItB412HGCX1td5wVVDoAei76WiJ2+4beuRpwfLssgNU3LO76m/gvJkN3xfy9Lpp
EFLIbWtK+WlLJ3Vghy5wHPlf/MIJgtQEBol1fZV8StNVvk8QoVzZ6bYO7OwHcvvM
J2RuFgt+HhTrnfayUia3ujahp1gC9ICzvVlKLvrCpi4O325A4F2Z1D1YecXvwmcb
w2W7X2ZIxwSFj9D3+9WQF3GnUUppN0J0sFyc1JCZTuZO9nc5pRrzu+1Yrul4yNMv
Ce6sZHWCv9VBpmDEB4kEIeYqh/JsSOPn2CGNYzooYb1pTrVJdr8gMiCeT6qaHjXE
Rd4LkkT3kxHna8dtGVY1ayTx+B+edHGoET9xLD0U8jYI9PHKzeCnODZqUQb8ndcK
7EW9M5ot4bb5oT/z1ZoeEYcmA0W6PB9FJzuyXgsTSjtXihFNLn8+JDq6cDD81hct
ibAjhm/mAltmzSSa/vfysmASQicGLh5GFd0sAeHfVxRj7iRXiF7v19PJVy3TGTE7
ItNOU1fujxWezkHffUi4/R+nWDZNc/3Ix6PjyMtFaGr5F3rm4MPqqkuHYhJYwU+g
FtS80VyW2v5VgilhSxhRody313Seja6BY28yFnczz8LYWg9IupXB6bIppS2xsCaz
TtvVDa40nSnrRVu/vVic/Z8J36a6zc3LRs7oW6D/IGZIwwYND2+UOrhylvY45DeH
Q2RXEpZJm5rgyOLXwPpJs6jsa5VVl6p6S/5KCBS7Okcd8CsW5byK4vNiWuUwYfil
lKUu8sW9/KyZkNopFZRQh1jRZa+ZvFyOrsRCxpJF/aULYyXF1M6jp2/Qn4eTvjgW
qeX85TMHnO9fZTZx2GHlJmWAYSfe9Vta4drbObxMHTgZ4AeogKJVv+WiZHtML5zN
xUk+faZG7c+riIWZMolm/BVorO6lF9exgRWkZaz2Hti9kMAC9Zku3uTncYu65a+D
b9XEaK30cW7YqRPzXjiXbGn2mJYuu93dmqUv1DOMLertzppShbLXmBo8xT74+vTZ
ZtWBO662RsX6a9Nci8H/BXoQEsn2vAp76vXIqj2XXkv61FzC1xIjj3wCvkGJ5JDp
ZwgkNKc7SHmj5RUvQ8XkQjgZIYMjvBPAK82tIhWPtT4FQPkRXhm7QDMMKpjegmws
zX5vjhe1AsLqcy9mQsIuow1zM7ON11n7Zlo4TWE5D26wDEtdOz0VAp34Gs+I8V+i
lgSfuEHUzDBW0MY/PdsvA9V3vyJFJnP4q4DMsLL1Iq9FMXaSvRphIsNyfvzdJzfd
FeL+2DovEI6e/VK3jue++tTITOH2cdzItvnFDSlOwGdkjC9Ev1saWswBKbwGzXp/
STHhvmHI0cWsRmVrzSByx6q/tdlsn8u7rVX8wzRFiK/f/nyPsLoNXAZiJYOldJjf
aL+skHzqFgBt/JtquK2ferA7oSf+l9RgHeFi893/Ne6YowGx7bhm79Mp0x4DXbVY
YbF8Uf70dEW0tN6uEYkhAUkHjK5utqcoVR3CLvj7otPbXpsI50jrHEytcrz+n8GP
IeWaLgiE5ydlzs+85dhzrxwHc6ztllNNVgCcXl5/DLnwcgh1NUcfbeNaLPOahBXZ
ghSjMB3YJvm0P6JhQdsA95AjxfOtYU9KgDD9fhD02boHZCeMgeOkatRKFS2ItFNk
/w2cRjUdjW2OYnttdIwHfVJsyAOPP37hcTETDFyyqJTR/WjxGG7rLDN8XRCJ97sZ
SXZd+3anF8utaGmQPilPbJuIORkaOCw+G1WbMmCPZsTae4Y6hYC8Y3ISd4aEysS1
SoGBNtp2uU3sBETSfS5s5wcAuHDXFheXB+2jXwcsUIQUv5IfzjaG5TX7FzzTAKhA
fnKyuwqWSEF8VaQlW3JdtqOM/4sdecBG6uQtxImQCa90APP2SQ5Ep3t57bvVaSc4
EDnIAu5tRQb+4KksJOjzGh4jX3rt5Fr626U7+ZDnogJHMsVbcmuPIkleMkkRYxPq
FJAZTMSV80NfMgE1wsJ8jDnWlpLIK9NKWivCFoCUNre+xoldV6yYGvToJk5NH+pa
Vg1mh1DlVliDYJ77cN+4qracfhJJZxBFYI7jjEywkvTvQkmqyy9rIpD7hsac/H9F
5m8xrrXmx4HCRorXrkjQi3vZTSrar9sahoxFmhxfOPljsJ6kGbzPWVH+znymnC1T
iYppFLEET3YZg93smhUOWrDujvdv2Iar+JlAn8MJxHVgT0WoulBRF7yiivTqUoLh
NXiqJlYVP6HYLTuQvLrlTtUWOgRw1NvICmtu4Kg0XHnGrnfAM4IFAgW1opepAQjD
fu6hGC6PtZJOUtmR6vkH5QT1910mByjyc7AS95Xj2JkcKnYnwCTtLrF52R3R+K9+
VaILMExgNedvN/7yLMq2ZUxTt/XMHCLcZHDzc1E4r8RAVl+iSffdKBQGUrD6491h
/Mxw7nXltA2Qp3XIDY0gykx0FhacpYqtdjP5MiKDrIVbLoO6xErq8Vp1uJdACL9i
T00w99XyBzYshv+rVRp76a9rme3gfZW81OyetegM9VWjlb5Hs37TxrTApih4lmHy
ua6oABebniY5smTsp+R/NlvPTzRIQDs/xJFvH4tb7o2qPN8HbBAYL7oUCcxi4K3t
bhT4I8l1O9aSXA4CWETJVXSaq1kehdZ+ZSGnxQBAlRskE7U9vmD9vixIyMUPkoop
hTi5cOOSSCMnj2Bl98iQGSllaMWrrMYnWJ4j1OF0oPRTlPeJyCO/Y1MoYz7iBxFd
PHuHkhPy3Bpe+QNeQbfDwQeQZwyqKSFld56ymc1WhfUaxNDnLlDbAk6WmbiFiKH6
vDYbht9cF4cdmN/ra02ipmQHX3qyd5Xu9mxGs+TvTYNAiasaEMSEiganYUDhDHZ3
v8kOsuymHo/zJQamdoexkdhPWfCtNexwe9Z0j3ZT9zVZd+o6sJBTRIuoTrPAtnSZ
5HqrEdsdT8H+BKg5s3xuzp4KmYQYeC1y278alLvh/y2nzENtndF9UVXHUT6VOrwC
hihDub960SLx4llTga7GVtRcAjS1Uklg6PoBDFnKiSp5J201UK3rgU3EfZzMVQfw
xdpG0GNFfzLx18QxCn5tI7zouBR4AgJEwBzgtVEn9mFd9+MhGtM+C5S2R5HvX0ZA
tk6uVWGivykGG4jGDmeS0JeWBV4NDqS7FrXOm+dmEQTHzMhaeUfYC2beMRVMuSmR
tOmIVBr01JalYwsZ5YOQwLwkFnc8q3R0gsxu8rzSllnY5a551XLLxdPNvB1OSaS8
rb+TIIPHDjZ0reojpvRC1upVEuuOtzZVT0gCQnEgEM73n5z9JMq9POhEGPGEXGjP
cF8tPZ/CqemtFk+5sfc44Kc7kAYQ7BRGfr9gCFJlPqiVkZhmN1EUVFulCd1tBJJG
deU7/FhGs7VQz3diO5gv7Q==
`pragma protect end_protected
