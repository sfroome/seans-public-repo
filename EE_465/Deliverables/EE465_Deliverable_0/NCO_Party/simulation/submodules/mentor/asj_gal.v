// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 07:24:49 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
iVv3ZBW984eMC8AtvqMH+lMzyJUfbC/pnQO0mb7vkHVnD52O0x3FsUfWCBMUnmyI
4hPaBK8Ghy/t2IjhNpBCW49k1pZpBmQEbYFG1mko7U1cRy1JKOMJBXifu+F+2xZf
kpHijWIbm8OhaVqU06THFeycWyOmEG6P8j3++eicaOU=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2864)
eyzzqtKLJI/yqhi/8WyFrOCXCxklVIzlEpVyX+l6jipfHgP7dXazstjmKyyRIv27
YdtNi1z3yU7fWjMFGrQuMr7RIiHVUybiSDW+ad4YEhoPhEFKbJmc1sg8cpS/VowJ
GSoldy/bLb7GQAqNiMyr5iAxxeZxysoh27EGRca0SO43avMqypIBey72HSujqgM4
C8PSt1RYfjaqgUymweXsExuh3WzqTI32ZlZcXpyRusbwNmA7Y+WN33Oagux7fwqx
qmT4VVRErOiLU2/jAv+cYNNJgkaTLjQgeyNeDqUlRQGYl9hIBLRJBIR8VMy0O5Uf
iHKKvZT1o6WIcPcJANw2wxAbd1i2kkUdGdXOUkRRZPgQNMiKPpzT8bP5tnTiV/Fl
shhJhjlhouHScJQCnkEBNISmp2yxavs4gpPfFNAUM5+v05vpnyhCNs3krjfNpbj3
hB62VXJ2iWb+ZFNa0QWnuBpeM80JP5sXDsGOIPVMHn8prI9Sw7KT7ZhuWe2VJF2f
64knmA021MgJMelwQIH6qnCBx7J0v1fg+Txm0pp4FxZB5ZDOOgKHLDznuI3ciiB8
0Jk2rFsaqkdPZLLb7MJl6fDE6MziRqG5OJIiT21EI0yqUSOiSRnLm2dSSlKUaUsR
WOVUJPlDX+VC2Xfgo7Ul+eUWbJP9ibjuk7awhxnw+POV+Lg/A67oJXG//auv3OCQ
Fot/KTo74WDDM78nulJHQy/LOZyGY0PrOQmvBWDWBL2zX21PRWCX384njNr+eAZU
ne2ao5VJ5/sbKXnC+UZDuCnqSb1vTC+7FAiMvjXBd2edM7n7slkHXrvm02mldnVi
/1GJ5+ICigfdLEBW9nS8mpu9kVue2sF0+Utgy3QJzgChMTkd930VIj7qvoSKet5Q
W9MRFJq9uGAAvl0pgw3qH3u/OL6/5z1/r+b4QI04HFHAuVwrA864outhXcIKvkky
3m41b41+US0xD3+vfeSU4uf1qMvA17RWmDn4WgjHXKK6CSye89+G50DYvSPv0Ebn
EbN/nR233pZJmxMthPCBEEotEZAAtBFrA0glIG8rgxgAJEMm9SOtlXCrCdtzCkq6
Rx7cfjND3reNe194hJSaonfNXbKdF6t2kluCAl4NNPz+mqfmIS8M7sPR8hC/BztS
9fQgLbIzofwvifMa5j41NpO0WgzgSb7CDiNokK7fh+YJ0jKpJZJpokdfvNxqJuIZ
Qf00aMfjrXDKaoowLDsGlWeXf/3X7/OtiWx+Lrkg9FczSIe68VjufuPKW+t/0/gi
uS3xzWOlGPz7nTqT8NWV+Ruh0GvEbr/x30ws0kGfCuzFNeskEAJQ9oFAE7VFQg9U
FP09gH7B0KKhpBDAYbPHjOOLc7XYMd0BuOD1Y+wVmaX/pF6iT9zQMAKHLXo+hgBk
S6fqSD8yWV4R1ZU/Kmdqy4FebkOSv1A+iBM1t46fu8b6cGmlG9K1uZsvI3Ah+MTE
ObxHCXLaRZGO1TI6OElblEZw8wAyfQg3nNrRbt6iSoEW9oetspDB+UuW2nLlMkSC
Si+E8reYW6aR/CpA9SHTwUD46AHyVE0jnfqStNgp6I1GJYCp2a2S5p+dbALjM4i6
4X+E4AJNmh1lpgMDCLQS7lx2pA3Kykoe3weV1H7VBtzE9SUGKz3jgNrGjvdaUfaN
CJzLGhLyO4HQJj74ERCN8d76NFEaqmLxGqRUOprraJ6PrNnTqa1RZlIeD1hKSMfO
IxxnloHLSqvrNU2UsuyyNisK/10XFLTlcvRKt/OfgNFSPdLq8aeJG5JvBbO9ywHR
MTCAlwpVqntkvIU9eZxI+Pa8VoeQm9A7lN2cTZi1c7fodx4zF0e1LSCrjuphMIwD
LUuqa+O8kwy20KSEujNpYXKx4fZ3xO/7rsgYgRGvYY7yPGjW1CipSdNVH4J0t8+V
Q0YYFsZkcCGlXpdPKKyMLSqRFfDY6aVJfh6r+72LOP4KLdDhP0c8N6c7TQOKwHqJ
iPZbxpVayp3/vDenPGNZzkFK+1irDEbBd4O8unxxaXJ3Uj9/l1jkTn+igcAA1peG
0iAwva4oKUPV+JEjCGtVmOO6AQ++4LWuPrAizKtbTZe0ih6ArLHjP54Lgk8d8nox
hPi8Knb9CJZrb/ATrAQVL+r4Q68j0jB170563aWbsPAHGCl1353hQRlezb4AoA3H
AvYb4R9m81lIclGwndpKsYLXYmsMW3MLP+/EJN8/G3cqOOlRDpi1XmMZEtggvgHq
Ch5bUmCakpfRzprUctnzIjOQgk/sikRd11cNA2ZOOYr9CQSiMetRVSaGMjlL+PIN
NgDI+ZFLv6JHxqZCodhbetW5U8py83OuqCuUVc90TLRKi7S47alBXunMGDdKprZT
HS0IFmZrmri46aluaqXJK/fKXZjyIGYDRJuBFx0+Qr/7zhFBmd7ic6xip/zwFvHJ
KLmC+p9iferIr9Lr/4H8i+Wq9EPtEECji/a4yKS7Rjo5sOTYoVHvA8vZ1D8tjTWo
xF1K6p1t261u4COi+S8cmvlCTIfdYB/3TyDajHDyvRgrou7hA8sa5mlUqXVR4doK
rgo6V8W6hVWpQgeIUPDdlEt8bBVoZhn1sbVoFbhDY6zrGx51jlg6j3YnfBBSQyBG
nN4DzJEq4jFjUNiBLvP+gPfJOB8LgguKNOOOcPgCEC9poTAJ0aQFEcAI2CeUSwEM
BI/jVt3ru3cjLk0uox12QAXB4DoCoh1ldYQOv76xGTEqcAY3jJ3tlez2J22qWaAD
5p3KSelFUtMvpdNBXysFjcIMbwY5RLoOR1aHlOClJ3S+ccrjGssCuguSwkDmmhHC
u1i5S/slQIX/wyrvzXeG/Rpkz+mlyMWrzhYLLv/U2aT0O1A2ysKKoa+22n0T0eT8
7YBZWnkis/z35Pcwyjs3dp0SWs9XSlfZQ70cqZx0SUiFXT4vhzxRa+z0ldbg/Uhp
cU3ZCJTT1a0xJQq/GTSEzg0IumfobNN3fciWB+wuebiaGeyS031EVsayZxfNgMu0
p48zH0tQQc4QoILFSlok3zdUonAnY6g0jlcFufHhynSxcGeaNjak3ze28TK6JYci
WxpBqVlafFopOPN4m3GKsAY8HbHwC6RnhLdPeWnfmVK/SeFeM8tHr+z0mKu0Bf1W
w79HALOSLZU4E5hvnjfaKRLvpF99tF8Uy3bijZLfdZloJ52QIEe0PdDyQMYXYPaL
eqfuA8j01yo2002p0v2rVhRgVtdYpI8VwyW2q2ENez1fazptz40rvCaRVTcxW1sj
ausIhI3yFl6BeshVT7FRvEj83A+mmHvoFRyZmYWTcTd8bhkE87yWTw+iKwqRBAOF
wUBiuNcwaKAiDdu8wIh8CQVl0fa0Xwr53fCPq03ncLYybrkuIYQjUhnQmatR7UOG
mK5YU46vhheCYA2nLStZA5t9qGowU5GctJF6teKf2hlPVEgDSaD+IiQkg3ed3Jug
9iIpSQU5EHo54NRJi6f4WwoVNcchj7b8Nr0+gO6pNc6bXt9pTTG+f39gqDA72xUW
MOwy91XRq8j4TqYYpWyoh4zk5FI3lx30a+zKod1kKwhBy6SuKQ+Wq6qQvFZ7fGlU
mvqLh2aIDb2E2UjENoEF+TtZT1MAgEyEHxoH5odjDKzkLRGo4EFVb+yd8ebT8Myk
/5qEG36152r7nv5XU0DbTLv37BqB8LtR8i+Q2wof9vaX1Ja4j/YUbLEDQSqAe5c8
hjgP/wMVnbYxUaB06RCdUWa1/1OgTqQKF67c/iGdDRevTmtTtNq0+Zbitc5ZO1KV
k6WcW0VwEhZvs8dE70ZH3BwTenEIbfIsvINqcPC5WnM=
`pragma protect end_protected
