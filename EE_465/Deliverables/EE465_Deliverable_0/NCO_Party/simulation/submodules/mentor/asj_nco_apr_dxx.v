// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 07:24:49 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
IsKadIldot3C9/W91VH6+R0eaINfC+y+HyBUyEK70fmjKmkOA383ym3RkcZtsxHJ
iI3yOw4jaOkDRM2mJwRwL+ZxB086KOZ07WnPetvbXc2T4l/rVmXPaY+nDkLfh8Uw
2TgmL+lguc3gH/6cdOQtO4M7VnUQbqXibyuZZ6cjKag=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2368)
9fXz+7GuSeaoXfbTh1EaHC/GPS2aioUZjnQx2LwOeAaZlh5+kFtpVpqIkvWWLOB0
wUdsOraloML4A/naNt7uYjZMV80h8UOp42D8I8m3p7XZQ0hWNaObbRIMsTVsSXa6
RanVOKJpjbKlkWrSdogyKUq1AFZb0+JhbuZvaz94zdkSXzh4ugFrMVkS6h3gKWKS
tp8+A+ZZdIC2wgceo7ezEvkSavzeesFlvMu2FQnU8cQNqhJd8QEsXAEp30aERgn2
G8KljNgLYNT1byNLnRwhwRrldGbThMRuP/HnN70d/W/KdT+o86+6L5amC8/FORfr
eLs6BF1l1Zp4DfIf2fQTWfwmhfPTdkvOyt/7XhMPajbt7zOwmpwaK6opz4wBPREz
jPdiX9tNBUDmt+htqYv6hGj8RPVtmlEG2aovGqGfzKt7ScX3pES3o6rVZkJMJ2O/
X2gg81zFbCjUSe5ly+/EyGjdAD0R76vvY2IDeYscgg1stS7jnHR4ArJtci8UtLjz
+0qOUS5OAd6L4sgpZrKb6QkA+luktY81gz4d7uVTG7Qdn8eKECJ/tL0ECS5uP7V0
Pxi9i2Mt/+i5XqbbirvYZOE8dwdv/xVeHiTYcr+aaXGeZL2swPjkxxXSDCld96dm
jhHT+kyfR+7Q9mBw3EnsYHGCDiWViixFN4SpuhJ+Kq2a44ExFE9MhYF8GTtqxIW1
/QMHYGbTopVnXtYj3TI5JiIz2Tu/Qig/OyTSAwgsbeHL9mKRL5QB4eSK4jPtusNF
qS5+5i7ClbULaLVcvDoI4n+ALhIGIJrfOvbDs1JX20IfmxAfNYFf8GY0IQ1EE0g0
X8XQAOCQkRxtckTXDLg+du/xG3SCKWv0wz4R0bQwpRiDieU/o5GrM8B6CUOwsJeO
DtOgWdAEoqt0KUTldtURcGQoN65Da3IQwj4NwC6qWRXdh5ZOlQeJ0+Xu019YfkU5
W2y7n1HSNTciZc2dkq3WyKm1KaXCV/5XknlJlm16TRK8D7ZuQQlECoBLu0iB/eAi
1TOSHVnmBPmnSaAax5UeaFmR3Ya1Sgd/mBc0dDIjB6Wt+yO/iY0fcdzX6bmTljqh
yJa789gPwLYknyS3ilZG/9YT+RGwWT04pSDEp98y5A6ON7zRxE1Vhb3IolMzp3IO
7OlkrH+k6X4YvMZ0eRegA7Jr+RgY4qGbA/rC949F/SoIu75ATIjQvz+C3GRSbIjn
lNVfGql6ucoNF2ntWPeB6fnwTA37S0DWH2wKFcpbIStS9XFxuewgTHDUFrYihzeQ
gLWlsSTQiwPeQSwZYyU/ERKDQhEowPfsTIaHJFVDErH+9RtYPmUTjCfS4GH5nwTn
9SLLrgp2H+gJ/1Jfi+WpoSS2sYe/n12gwDpcpQzmruVLSTo5IWBdohv86Is8YFd6
fq0lHD7K5W9EWd8vCqYDZZeJzkmrVJBXYnT1/HDtBwZ3vXC0893SXZLVqJSSP3E8
+flYbOWhAoDFkl1Esp++SqUc2HU/a/xzL3xlzW7sovkWNy6MJ5e4/MfYGengqN8m
ukAHht0w70WjibBZ61XRlUOOFxk0cX69vvBDw4rngYdSL1NNpqzVflB48ujDEl8U
bgsDciuxDPOGIYQvDyujTktEmAJpuyIUTw92Vu3sl2hpR+j/1pTiWDjFdNNeuoAZ
UCXOC+2bHaVOITzluKEdNKjMR+hElIMwVZ042rnyu0S9WZAPAlfS+GUkc9oe8IXc
8eCi+qaXBohzyoGwcQiFtS0mRJ/Jsr+8QkVLLhg+ECU3NPH0WE+8dr4n8DPPJ+dO
dbHEWHYFXTFS5ha5nkp3vF3aN1slLdKqeOss6PIIsC7VctcKXYLNLxy6IUBZCHtV
Ewuf8FqKgGdEZLAB/WA6Ht/VDlpqIJ2szDy0KPWSkoa5LF+7oYKQBLO12n94dZdX
HmRjeLGJPi9a+875isip32qojFUMtZtiRTlvLHbsnmxwyNWg+y9gALnewWVj9Hsu
L5CWgIjTfF5svIFDZbzLlw+M2KbPcZVsFTpQwPsd7pYFPDBd3bnz/9CsO6TAtIv/
GUMa/wsSGqYZPFGj967KgPTalb26PDzeIgNg13B0A6js58RdVsU2FR/c+NFy4WeU
E5qVLcONhVPusyo7MzqUSIjnsGlOAaVdEAy6rE5up9XEnOzKTS3aJ3BpGX/c2YKg
peqXyxh6ZWUnYuXeDXt31OJphhBHEUI/G1Y1RyCH0Qsspg2tnT7YKIEkkGj4S2yo
GjcmLpbbPN1kHo1Z8G/VKAh8gfT/VA+6RyptKqQp1KAyIeJZCPD453Yhyqza+nMo
/RfTTOlQ9nOMN8x+uI6ccIukmrr7xT38z+i34u1AvZooGkcFRPmBwzgGW4gSseAq
IpnQSwZvkt/w3A55zj0CrkJRqgO2EvQHmRbMT+pMxZGFtDKv5xcyVAp36C3XnL18
85GcnPSBmNu0Vzlc96BV8EDoIUAlX9EaUHdf4ZJFia7ul3XoKE0MYu8h1UgfrQDw
ZkZNstb8il2RNNJrLNQhVT5G+PQRr4jxu08Ds6kNv7GtcYQOVs5zytmAsyv8yTYO
DgEmhWwfJzzHEkzYBVkUfIsDtUaDMpqxTHYGjJcGNVnbKFmrSqFiFQ5xY+KWbRGL
x7Td//PV7iCCrFY4X5KwuQJ7KyGHGMJq0FgOzkmNOI9AuBja0m6Rgiu1vVIl6ALI
jX9xQStP+thtBeoj5dn7YL7trSFMzRgXRs3s7YwLRZe4bFaaOHWc1xcPXnlEXJLh
pBoj0OD/blr0jLxU0yOi4Zl4Ho0sFZZgowwraIM4h2AqukzuGKXJ+aJPlDvKRcZv
W6T+PCMjwlp2c2l1HGXwLTjCM54qNY9Gs4x5y/7muNZJK/C8oj/vactT0LQTUSLQ
JNLBCSHqVlMDbooalMnNgoSxOIKtEMSh02cHf7mI3QKAa2jGtO2sGvIayQHSsakp
qxa4USzjJ/E3itZZZS/WWo1HnnIXgpXQnTqLTUtbJ9XOM+z1K4qF3JK7cgmGhKzt
AVDaMtOZ5DCaAOgsN7HNHHBLtEh6CBUWkgNqWxteTjBpI3okV1D//syvJpyZjNlX
CJ2MyeKILk8s0iv4mpBCMsOql5OVyd8V6HxHbP4gWVC1iP2E7kVukkJqUrK6oV0p
zTh3eLtyQyXXJcVMNyWQEg==
`pragma protect end_protected
