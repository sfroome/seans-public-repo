// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0std
// ALTERA_TIMESTAMP:Wed Apr 26 07:24:49 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
oHGbPWyaMIBPIUjkq0QyDCXw9CS3LYeJo7F9Cbto2fQXVqf9mzeTixEkEGk+2X23
I8AZu2ytSj1bUsLEdhrcOiVYL37eUeOCfIQLSYhSpxi0eSsGSqeGTJFRjFAqr07n
tPLvdN86DBgXzoN8QLGIt+Q18b4HJH1lh03Y/quwHCY=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3056)
HC/dnfO/Gp4DMeTzlg+Q4pGi7ycE/Vu/gzYipNEqEHD1GQT6XZa7GwOPRA/lvHHf
+MeZVt9vQn/ZadHsDSb9290wiF9hLWPRhBARbLeBgy2m7C3zlOAOXxwjmWNSM11d
wg6ssD+M+z3ZE8pdGBJj0hBuQorE6Q4+NHarbaYiXplPXhrteWrfwYdlc5E74U/4
hiD+ifdPQZUFG9JJ0Pc6wl4u240f109ZametvSmXFlyWiJkDmK293GeofbmqMxFY
v5i31IAYgFb0YMyhIzPSN9boW6qg6NYnoJq77rfSFyiCyxqmMchlFVqc75mpsTVw
Ry5+2GizajCEM4VS9kO7ZT4VD56MYsgacBDMgmnFXsnpbgHw9ufo0fuQOpAMqx73
MNSg756enq3VeoQNB05S+83wwDxbR9qwbYoxAY6Y9OKhxtCmmlmwbNUQ+RqPnIeb
Mi6cF+1ooi1d5ke/X6fr0wV9870bCHPPshZvLjLShG8mMwV2XToZNj739aBTUJo+
4N7aheKlJ0/ntVW81iEN2CDxr2JQhL0u8uIY2lKE5hTMl/daeQqHRPQ/GmbDuG2W
KpxYp2+mG5gdb5LSM2Yw5wTcE/F3y/emx+olTegUSgdlbuqJHxI5YbONA8TCnjIX
32v9PmfTDqjIottC8X3+tWa44ed1GyF7rXhsh9OX2xx0qZXNifRTZhtAHTMf2tIU
P/mJs2tpqBKWRA8LYRFe7ahuA9YWKmNLzSRWAGE5hejYa+4GtpQyAwhGbcmS+o0j
Liht+qSLSnbfjAaECMvmnSyz7hHyvRCpkP4hBqz33wWWKqunAUmSnEEusY/D6nJW
baoJ/iLChMQTKGKmNITV2jM+xcjmaBJjLNAxFUhopJ9oKbGtz21r3O1H0SMV4JTI
aVMf04srcCqfyQ6xqNeWv1GheYS6zuymwnSLwyeL3C0Aors5ZUndswhRaB/H/xu2
S6UEmiw4VKvEj3NwuVGaNpqcWM1AM+zLW0Ydlth0jcNHCIoT09Y6v1xBJVO8ODe2
QkrMZXsLsY5yQX+Hk+MNjyp9GdXMzzvamzdBua43mlKSO2QgvQy+6DQ68fE/LAOa
Jfnc58KoDTsqyruYRHRXMJsBgAsPJbVYlF9shgeNeUZU4gYSzceyYqN7hqXDrLFK
iW5mx2eJtCS4rLdG5Mz+kfOp7Vy7lsoimpHaTy9vGjvDRxuS86lss0mwZ+HEMGEh
DkBYKQ5jneXI8+pGA7FHfNmqkxfkIiiK8HvzWRNyIlb96vrj9t5UP+mQgeUGOM89
aAeDJopTvgQUl9h4pa9ficyIvvxCTfkC/ch8SOC7wsh5ZDM7D4AlPOUk/9nOfn92
tMGi/wQw9Mn+vfK+xqf5HbXexjFR5L7arIMKgUqsITtdeaTLpqyCNCf4Bgo6IOCR
oOojNVUYF3muj7JtuLPGYGZ2xsHnbL3V2viYOKRNOjbgGZODyUUd+IYOnNAgJIX+
mP9YnUDw3CCxURebvYn+2KwH32QdVDRdLh14/hwLL/DDhx0hdIB9Nwfqyhe4BZtZ
oEqhzc97Yj1rvZiH9F6kUBw9E7uTauhMUIO7dn9JS23ZsETHEDRpIySY3LusbFU4
8o9p+HCxqwCyEiI7pOO29RDOtzXrs+jdW49ifkHRMHdSMEuIgnj0nPypxV557mxw
1eOhN/9UhKIyhMdJ8qvSEvzDsxNPzdPCZ5EQTGp4rMButY0o+wGbu/HEH352HmJn
W1gHbBz/gQLH0YeyFC6rrobPGhGBZwims3r1DIKThSzt3noUk03r5hxSbOB/DhzQ
66JEu06cftJgHCV7Jx/lzYppviQ0Zw/cAtSMwe3UBePspu6qvE2w1ldoaIm+got3
NthlWIoMEkQnI6smulZaIFaPSmnqdVz4BWZVU6vMrJuz0IUoCWvAef8U8ko4uHL8
9gIT8epB50x4NUU4JxOO2q+lSmHa8wqJgjd7oJb+uU0KhHNB3HWBcDOmCezhOVcr
eRvKX0mb5v9lXqwwJpO/Sy8j1kPP9EZ0Ig/Aqdftwyrm7kDsjwoYscwfAoj7uw/n
U7prYZE3+qtuFJNeByzLLMKIADCilVd8HDjPKd/9rAU8J8DGbLhDz+sKqcwsTE2d
VmseTMmXQKNNfXtFRYF9UI/uZX7m7I4rd/CmbNTiCjk9maC0SGqqWouT41r/NQHP
/+uNk7gt6rNTgHQro03fr1tphcVRdlHkbCXmTnCK+Y0Xr6Ole+1Q6tzlts0wF2EB
7zR7fc1QuEIQWmwA6qAiTo2xxkTYzYBh1w+S7YugYYg1Nsyq5DigqINgYbN0sU/z
8XmX0EAHvBN3iXoF9KzO3ZV6Xc5dfVqK9bO90PAvvn+V1g47g4e7WMOPpVMy98ql
M24Zw0eqpgX27ZZH6XyTmSmDUympRvOI9RSm6JNK0ba7YLIOr5RNtjtznBjjdu9f
Wz3czfJYRD96++uenj+yoYXBMtRZN2dLy4KEoZaQrUVjF8Est24yLvYGZXWIyODt
nyXc/yEreVm2wEYAwSJVsvnpYx6Uh+zAH8fGDTMb9QjB1k0KPLMf39igKkYrlLC1
fmhWoHtlcSha26bl27FlND2vpefmy2xy4kkCGN41uFu6bg6oiOKu/praZL3OrTsK
bAjf4pu7c2ZmFqCqcrUF/iYro8wrZmbFy2qApm3ezjMorfDqMYsQgQXHSRFmR28s
2N6bTG+RThHZF2iz+/mSYEfTRP4ye0uPP9jKo46bIIgtgP/kjRSR7My5w5PHnAk8
Ox4o/rQ3+omu0Dw0lolywQIHxoNYOJqUmLc1DR7yAOUhXRB98VBWz8+J92jqoJgO
PVl5gRTUlFCUk2psromX5F5u5+KTLxKRShgmBr4bO7U9q2ia1j1VZZei35MCeh5T
9/SaWEpN2TFsSPbradjI8qKQ2rIHik7bhtSqnhKuM3abs1Tk0aCry60G7feTQFcn
TjrS3wZVUf0r5BJFFGlq0iEReb5PWQC4qt+sIvv6q09ig1ajD59Rg3JzpN/Y5XdC
zqslNR7BTUSsNO3PifGif4vvT4NFyK4NgI9z8CWi5xV4+tlK8dwLJkFoTifM+Z9O
rXqymupgY5y2SoNHjZTlAHqeWkcO+/7Sdq27ZWQVyYVUX941ercgDQ1H7Bk5kvvr
CvTyr5zDUQ1OW0MVHffnvJ/GjUeHvQrMjtVFBSlmw1KElPoGP7NPFQYyfwz7ZXwU
4eTQl/2iEjDIY4iH+zL7uYU07zkl0t9x5XX0kcqeikXNHZkgHxGyaCWlzeP9mAzE
9+f9L8QPjNtvXu9ICH3D0Td9yes3DiiEu1Km3iy0eiMG2pOoKOUfDF6lecOvqh9F
mjyfSa5IiWbeNwf8CurCtnvoJMVAMOrOekZYSFn63Qd4s2qq37tLbwTV7jDakCMV
repFkThe3di27XatoY45pusEHUOs9tN/+tVhIUeZjtT6ddKf1U+l+/QQogAgincq
kQpS/ZtMpzYr97g+ngux1q77gTh1OqPvii3te+pnnOvs9o1TlB1NxiG5/BEfTMo0
I4nVvbOzIyyeW709fk4vGImvZrQroHqgxfvclYwCHQQayunFXrY0r6GLiaRsG6vt
Gt07gsUwoA+iurJxuKlMfX/HT8M6mSHbgBth/g2JPCbXY7P1shaWqXPaQuEAjoVS
VRWTPH9ko/aI5ui+SYSlsnCK0G7gFPIZ40PhbchPSSc7deb+/pXta2g7AM9x0b8f
NocbqPIIBSOK/HjBxB410NjDnhNZwSfpkr/vOaTl0riATrQVdFGlIj78Y15gvByt
Q+K0A96yzuTUyFlHrZTRT/3ddXieAnws7SuVcv6C87W1U6YVRcgxCvVOuCj7+g9g
J4OyHesIpCnAUTYRtftVf7wOs+mSqDN1e2705Q+j5UpHex0TLp+JxA0lMLS3m7DS
D0JAnGgiaSUMT1mSvwqxk1qiT8bWBs4v8qPiMzr5pHLKJXLmrBCav548mEVTyEgm
AqF3VqY0M4jFkQE0av2PJfaz38UmjZVrAGLonrfK26+tOoPkYk2OL8NzJjxUd6Dq
TB7qiXkH/+KfqqDCClIIIt/eLWywSK2AXmFpY+wqfWA=
`pragma protect end_protected
