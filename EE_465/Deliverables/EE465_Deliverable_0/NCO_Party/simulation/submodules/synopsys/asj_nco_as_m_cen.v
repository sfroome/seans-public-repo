// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HR$%F.O&>?).&",TI>(VR@J5[1S=$\CDPK<AIN$A=R9>BT=E?HFLO:   
H+R"4$FWL&(7>/97GZ[I9!/9I6"2UDM,B3</OMU%,K9>*UJ!MBV./+P  
H$ZH3CJ^W.[ZL$6W;8C?$,_&N]59&UROTD+J7:$14U#=&MDRI],.%H   
H(G[*10_D:Y"4KS](6Q:8:$XYJT0^,>X:&Y_9N^Z_E%H!CKQQ]PU'>   
H?_3W(K0_H><)ZBAD;O$48C9W_#E!L*CG4LO"#91@F9JJ/#%SJQUR,P  
`pragma protect encoding=(enctype="uuencode",bytes=1216        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@Y&CN,E"@_E78I^-WV"_!N<:B@Y^)_6F6IF^O_O':&]P 
@R#2UF>*H>+OK9P^@$R5836_)T'01P9+\G\^NY+@#K4< 
@S!_1+U'6X9S\GP$.1N^#!C#$;L+1BIBK-PXCQ? <6MH 
@D4%+C10]1H.IM2"#>],XJ_ P8MRE**,,TV-?CPC\S7D 
@\;DUU226GEBIU1>ZG<%UO8OI!B\5G5.W]#984*RHY7@ 
@N5TTCJ1C?"[P$0:%O1<4P=/[Z&-VK>FE?(H[8VA#=]0 
@B]AYL1IO<\OK-V%B',*;?&$F](L,:#;9U!IUC(3\U+4 
@;?5G<;)+JXJ/UYFSHQ;(3#KQ89*^( XAATCVS_4"RP8 
@!O5KP?,>:=PB^.]S8%Q6L6AE&:-!O*_)UDP7GM03T%L 
@'TZ%.LXC4,I=N5(GAQC$A6KDIY<G),B+1*%^AD3_UM< 
@#Q#3%-]%?]T? 8J(4TE4;.8 UWVZB W\;[#I?-^A;3@ 
@59)13LLE^%*1^;@JYXB)1WF5V^L;57?*20'>$1?1E.P 
@KI[M3ND=A]Z]@"Z&*I,6GFD\<_0A'WKT38=/V VHF+0 
@N;MVVLH"]'"LVSFI\B!K%R]507460K3DJ[ %6N.(ML8 
@2M#T:\<^](/3_'4=3,*@]#+3NV$JJ3$2<>V ZX?HEFH 
@4OI^[&G1]SAT\%0&I,W[9<>TI5AN)"-^;SRX #_:(,4 
@[I'@K+]ABE4GLGYWBC1>P4+)^7F*=>0Q[TI^/E:*-:L 
@@D;+2?--)UA[.(E%"D7V:@$?+;LOK5&$.+VA8&Z/BVX 
@%8&)&*ECS,SNREHH52ICBP%JY=A0W<[JZBQ\B/YU@X\ 
@H:-_H >MO1K G.R(_2J':9(8>;%KE]]',.G9+AO7*%D 
@C92T!,?X-\)O2236=UBXY)0]X<ICP?)28-L["R3YGK  
@)\S+@MA_.J[1_;UH_A#;70=6M]"$M"I1::^D,-?$$N8 
@[^:';P_:(S6;Z6GLMK:5][RET7V%=I7A@ +Z)>U&!>\ 
@##B'>L[(^S]>.BYE_2HS\#2.G)7Y]Y+,=P]=69K"W., 
@_A=_#8@*RKAXD% Z_?XVUTQE)T+JAH[G"PO^D,?AA;( 
@0;Y#P 5];/ 29E\\'IY=&E=42ZKM3Y,_&_$V.GYX/0\ 
@^SH%Z$M3+OT"XIA"P'X^C)A%B1L"2+UHJFO:'"EKK=0 
@Y3PQG>2!-1Q?R3#$!G-+:Y\\;/'UQD._3Q<AN;/ZCX\ 
@!OIZWE_[X;@=V@??E).560U32D?5'O;XY7"J>TUQS7X 
@I<@9JF=H>WW@!W5OTS_GL::ZZ_BC4\TNN2"^JC&TJ\, 
@OTTD7Z;$Z3.[@CC](2]$%76.UO'6BE04F)R19*8W-+0 
@^G#0[&=3=8CR^VR'4W_>H5ZTA_95XLH$:>;N<M?-@B8 
@/Y$09]B/"/9^&XH%UYI-"+I>7WP*QK#J4C2U4/3BMC< 
@3.RIH[AT<(L+Z;^IC%;UOK.Y.9HQM7>>S!/30/QS=Y, 
@+\VULB+L7CX]2^@BO?NDE&K(G=[I5%5[XTY*:4'O6KX 
@KQ^P\EN44GP>PR&8 S^OD(-J&"=BP;" W#KK!I*WX(D 
@^3)1)RWQ05_J :F17@4:YZA4>]Y\A^6@Z0.$/.-4B4T 
0AY1 "#UNC/OH<R4%)Y.=.0  
0YB&LDR_'2M/_G#9GBZYB5P  
`pragma protect end_protected
