// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H;?#:"3MO5/8I9>^WE<AZ5/HG%9^2E'FG3;!18]4Q,'CR*&T8'&6'U   
HGY>$35C[NP/P6YT4=V)Q$7/$ A^[IB<JR7ZEC&GFR\D;3KJSTJQXIP  
H=XNC\1X9M$1FP;;KB9\<_<$),I57<LI8HK>[<:^R.!H6HT#.!] U;0  
HU._P Q 5<^N..K /UYT,3I"T&%#GLL=!S+<ZIA138^(VN><!NWQ940  
HN!I]G9C-WB5V?'BNJI'VP0/88^?04$B/'/H($=;;Y1=[2NV>6/!HS@  
`pragma protect encoding=(enctype="uuencode",bytes=1376        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@$EFR QIV-KW09P+NPQ*\I :\2V&+%,P:KD;!5HONB_P 
@>ZRL)FC>0/"$Z;KF MW(X_?%Z( :.LCQYDZ0*2.JM"H 
@LK!(#$;G\&-PF.:'$,Q(4XS69")NHL[!(L0!#6"3_;\ 
@=V^C2<"&L]1+#RK%_-,;533_,5LB[A=;08M]T08XSYX 
@-0="F@:59-B\D*-"?BJ60L$\BIY"RCR>O*N8C'OS\9\ 
@%;TD'\9CS+SG0&K%-U A*)<SX,3/L(>>U\V%NQCC'D8 
@?XO-[H'542F&T4A/@BW6^58!?@91[%]U2 $W7@.7RPH 
@CJ.,I B0W6!EWK2?)GGV/_UH-9[B83W&H8:XLPR)MB  
@$V)E2=*ZJA-NO]@8H@_=MO$IE>+0.-GSC4J:ZLD]YN( 
@]" &_$;BN$A2K).S8=$IIO?Z/V^@M (SI;6/%F3O"   
@^6$Y<N.BNG:GX(T>+CR0A>$M@U,Q\1O\G<B@4XFAZ*P 
@-$HM+:O!K?0&O_MQ6MJ860@D![%A&0;W BA[!W'=)J( 
@7)8CHP#^"%C<*(P/XC\\/X#X ?7_&6I%QN,^^$36N"  
@M%2?Q1+F_UJ&6DOH@>QV!ELO1<H1'N 067X&O3Q2:9@ 
@%32$940UGQ$!BX!E2UVSAV;_EA3B-J>1?K\KK?11T$8 
@GH^2$+?8&O/U$?(/-Y*,S)AIUJGYL17H[J,7R*OIP>8 
@^I#K#Y=!:?4=OQ^W7>_;M,<7WC]'.$RWD R!G$I&<#P 
@R2X,:DJJHUJR*Z,8BTNC..H*8U198Y)Z,O2I8W3B; 0 
@"#NDBJH2S-;V!-1%/LPLT!?\7FX=6< XAS7331WZX5X 
@&S:[>Q:QMDH?5Q]D);[V(4 !K)G"K]S#DRQ3*TY?-QD 
@;OL'L@ZHI+[$&7,7\+8'Z_09%+\[H>3O07EGUZD-#6$ 
@@9T73F;GRW-L$L;GE4\,Y$Q&*#$RK3WSFIUL!VXK.%, 
@9HH3:)_(J%6/VS43A S_ON?+1L4]IF4X^U,\ZED8IKD 
@%?CA5-9X><ZC6I#R=8$M/O/(,W_G<MC2%DH/,0TA)2( 
@-H2TKS/?P@H-0U.]340.[)AT/#L#53,;$NXXH9K_+4\ 
@!8Y5ON7L'RB>#UEJ'?XEQ>H!R:"N,U/-Q#K2=_5#+KP 
@4SS1!BTM*'=_AB9^2-X&W?_>=JO(U=3$=^]='351_8  
@Y<Q1OQ=X]#XC];^0*#D9FBNFH@SSU@MB9#MXPT56(T@ 
@V0W'3F/CR\Q/_ZZG(B^0!=,;49<I6$2^P.!RT,O #*  
@@V(;2?OF"7R\/*/#W$_H(WFY&;P>8E6V*Z5!<Z4AUV\ 
@"52IF]#7+'G$ E(Z^.WU8.6OR6S:Q>;OM:R[,Y1;I_4 
@^NB![[H;M7J^%:,VTL8BR$C\[0KVTW@XC"PQ(!K!%$  
@Q=9# 6B&'3WR W.3^>_)9ED%D['J%3'@:Y,@0$CS+(< 
@#7\(QA[/0FO$,*Y\BR5$DF"[]W_\U!"WS1</P^#[1+< 
@ KN%X#"=U;.2=W6?X/T-IOWO?B2I%KVFGI6[SR?[BO  
@YTNN^6WN;?A#Z9"&&]B@0@;+=Q.OCKT)K)"W+[5'_#H 
@BST/+H5V\X%0V *[^+C1$3;\T"_"T7:/5_P@-#=RF,P 
@R=2?5-/D2@%_5PUK79+NEB>=92LI;8IGP%NL9#J@.G@ 
@A %V.^-.N&@HXZ1E>'\*?[C#*1]M(QRI\'&TA,5R8L( 
@GV'U$[YST9_NRNQ22.HYKXFL4F9!'NMY68,RYU_O3:$ 
@FJ0+;2:CMWC1WC02+7>&LE7^U4I>-]Z2C\!YZ#U=V($ 
@X".S*MU%"[>24A/G-F[?&9YA^3M88%D=SPQJ-!F^W]T 
0<AG,,D:=F2_NR@&+9HM7X@  
0$0P)D92[!23>+K*+; _L*   
`pragma protect end_protected
