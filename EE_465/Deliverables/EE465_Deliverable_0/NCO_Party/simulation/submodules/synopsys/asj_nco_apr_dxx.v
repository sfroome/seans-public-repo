// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HZ/<'>X-^="8,+F;Z_(*YGG^B\8PANZZ2:@']G+8<(?2% >_"[34 XP  
H%'!?WGF@<P/Y0L*2WZ<VQ02OQ[N';FJ]G4<Y.U8II@$ .QN8FSO(D@  
HTCY9OJ@K]:[M#\MM1R:DP5.UNH P&J\3>Q5X?Q) TP6KS7894YS1P   
HH)1;'S5BL:$\2(NQ<%O8SOP\%+N#PLV>RM8T+:^$,U7;1CL6)A33M@  
H*7P:Q>T[NP5M8I_J=E(3)S*JF.+TGABJ.83E%[@@3"Q:Z1_F\[V38   
`pragma protect encoding=(enctype="uuencode",bytes=240         )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@@?*20B0KB-5I@N\\6APL\^#35)<J 3Q)<5Z#<(A>3%( 
@TB\GVMJU5EA"MIG$K+I*[;U.*E)R<&\=A?6<'[5H@MP 
@J086.P3\A/Y58O3)O1IUUK5.#^YD^##H \F!U<)2PN4 
@PB./HSV$'#Q2P4Q"N%AD 1IYAF#KBR=$C@FC\N4$ /L 
@3[IRW6'TA>W 1Y4Y'\0@(^5!UFQ2%OD$(=5Y$:02+6( 
@,$UXX*WYFI8Y<V6C9CX;,F:X97"ERF'";8*5IHDT3R< 
@L:51X;Q0U5E\H2VPYP.[9V8- )XZY_6>'D?N=G&8)"$ 
06'2G.=W!Z8!E"V8T!)G$-@  
`pragma protect end_protected
