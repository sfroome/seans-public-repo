// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.0
//pragma protect begin_protected
//pragma protect key_keyowner=Cadence Design Systems.
//pragma protect key_keyname=CDS_KEY
//pragma protect key_method=RC5
//pragma protect key_block
rs15WmgtkiZ2yjkAhaZO4pGGwgR6rgzerKeCQbiqHZ7Kgq3Vj7rEJmOnAXF/hvKX
pMEYRvYvkF2mrunFw8vTeJUE/+Ahr0aYsAs1Jj0iAiUEfMT3nc/cRx/bG83FPGH4
43P7c0dLlaPjXKg2ZxTqdvStOWa/Kc3j7++eCacDWqSNtLWajRMKAQ==
//pragma protect end_key_block
//pragma protect digest_block
FEcOb6Fm5sxL+GN8F3a5AIGM1yI=
//pragma protect end_digest_block
//pragma protect data_block
TvqdFwLPOsCxnquQGmfulPeBiwe3igc77AGWPfo/nfFH0980oR8sLkJl71m5NUT5
jzux8pt8y3vfAg+6sm4o341cmU6knicErL9uGJrZGdcP6vNbcEFuqEY+5xtGE477
uK951MAlo0kw01ZoLG4YMIn3KCkF7N2PAG/gyskhAQneUq3IblFpuvcN4sHEAaP8
5NgseBsWdLG87sLRiO+1MzXMR5Ox6QovGbfzbQUGNlAuEk+wZ7szVguYkgTc8qEt
KVgX0lIy2oWxxzkJoUNmPy013l31qV922APltukWcCcWgPxaDpwscOvpe1fdN4eI
9KBYeD1iShiBVqhHSZfGdHKL67kHF6TVn6ldfeWUiFhUFBZedgi5ReVD4cxAR/cg
iHu65QwMz9liUD7thybMocrTFJKLT5NHquRBNdBCS62tdjCiabhIo+GzJzFBUQgE
ZrkvyJkCvOeLke98mH3baCN2FW2E8LsYAHJecK4pWmfeDw4LGYGmLAzQmDZO2XA0
vDSjrW8MEYsr1a1ROjllnvN1ZZAtfUopWKLxQJL8KqCZ3i51fFJtqCmZ6zutcSCB
LMEgtGinFzy0v85V0K1leW7C/ANjCyVpy0E0t/P4ZRKNf5L6FPcRwYQHRc/uEt4y
ofnLXs7l3gj7yxhxXU6GTeg1VpqAFFJdPY/9vD5akH5lr07k+99AoiCVbq85BxGq
PphyCWX3JBar303ROsNeFW+6mSvtz7lJFlluEQkKvehKMQSscjApPy+k6wLS9m9m
7YlwxhaDTWSTi7v+wzCLSbfE5X757nMAVCJ0Fyqo7uuYCE7kAZgtSSnZWp2/W405
4r9XGoa7Du6lMgMZAxG1pgC012dVgy8yJYapcMHr2aNserRefquC1ZC1Lptkz33z
9FnCyRor27ojv6JD4orW2Z0ERV9UEI601oPVfRRG1mgquC7sGgjkaFHtglugIRBj
BIx21Y6VF6/6GyQI3j6cH5MEDAKJB3w82epjAqgEAGFHRla81JwDmD3WAxnmT1q5
Iwp6HCU9qIpcBgzLA4d1YrR7EmSFDb1Y5XLLpAELhT6z4mvfHoz0YteNtONGUQLl
n2AdMUtqjUArH9qqzp/FRu04t3n96bAsYYzr8rAfGzlwdxEB8xWPkQ/szisphaov
NWGPkYFKw3z1FbF1MXmB6aWPFPF1+snHNNg47bugTaOL+ooyjHTSxpvf5mp4QDFV
Y2T3KsZfwBDboXZebQ40jr3VoKSxPwY3H1RBocgBHh09QpPVSqTXEvh3PelVuuE/
LujHAluDZxh3w+MCwy4ye0aGoi7AhahJnqZLyWhTJirVwKpQh9Dccq9UnFDaiShx
Wi0c3AAghGcxVnnjglsVNGExYZicH9EBCTYrCxt5juJDXUUvVT8FmknfARkUGZXI
nzaikcD1KWT8kfH7QsLQoyUlMm5vuptt7xWuob6b4kVuwh5GUo8xhTI4QHWr59ZU
hUmfcffARGKpD03C6S0T90nPpm30yjgV9kwJqvlixW2gD0s9WJhfF+4D9Wlin75O
nowrwi53c0mcKbqHEHz9rTnYQ0k+bb/GCpg59pd2C7r1jbLYaCI1HQhHPtI8L2Yv
6Kr7v1AnOvMr+xQr9EFJP7tPuS4xZJyPHD4DlHb8ktIJnbkrvAWY8AX0KPu22Lc5
GbakRwCVO6RUdPHhO9onH48uTIIqlyjCWeqDjjy4Ewk5PTQUrAU28GOp+YdmQh7z
EKSwNKsdf2Rq9g6+0opQqSTbDgePCTbEe3VJTx/URQOnFXX7LsnGn9yNQtEtc/n0
3OQoDRIY//qpNtz+mP77U4xk7L4aH2n4JKefs3dy3eqDvEU2LgMDHmWr23e/01w5
pIG6A2wjiJ+OjcQLaovEdN2jreqpqDpMpsCZPAf02uGYNRK7Yuj8ZxpMR9wBsNlf
+YgdAiIY5UsyV1Rfpxin2mDwrBfANNMAZxgR0LRhpqGa8/0srQ7V/KmiXzNCKXHr
BGYqUciGU6WEi+8S+XkFmcBC/YeuUdGXSoP4t5ESLAuI54/daevfNXECQD/SpSUd
6IdAeWeGBFuAAzlmBbftQeKwyC77VU2mj53ewztx2BvAAI6i/Js58WLIgRb57wC9
eW+TjUFMVHoGTrPMFR3sRO8hiBmcBrzpOTNrm3ozVSxIiHTebyLo49cvZxSVmwYH
edSDO7rRikg/mznISN5UyjuQekpg74aYECXS3DCCN9EJyS3LD6xXZF45lBJROsUk
d7Of/198/p2CcBjfCt/9otxnjtIO4sl12sLxm+xv48FAl26Qqpsy/BZb/IWdiIOb
cD71Kpws5vErB8igeIST0LZiWt3RCYp+ZzQvIk4BA/qv1f3ta9h70OxSO1hHG2nM
PT8TXa2q0y6B747l1ZAwLHqeTMCpViGR6Lo7H2QdMnIwi9i9HSEa+ELCwdGmxQ3m
Thd3aK2DmAUdXhH4mWufZ8FeiD/cuktvpuCHnuwwg7jmzPIpAplt3yaixxsPDFas
iCXaMtCqcMSu53VEYnWVo7mbLIuATS/4a6NBmanxaal+ncChIuKQiArqNT5/1ow/
uPKufYp7ac5HtBh5PEM2ay+EkRskhyEJggk0UYLqtyho2g9W05HfLsBlpqJkUPDl
mFg7lpjeqvnT4FHTYT6pA/pz5iNN8qRbMYhzdkz+ivTcm6U+lKdP4/qR5RM8deMB
xJTDsF8f0gflZviQVySaxBKlQVc0zp0aoEmC3y+APxt5TLPFGkxfpevl84nVap3J
E9Er2RZssM9flsV5ZMfbDH/KvTd0iNdiq7bYqerZJBsEGAD9KZo8DP7d4cdTxX/d
bt/oGb7lCGXXi+wSkRcco6n7jggCyMIWawmgQA3nt48A8fFYTXVAc6IS2omhFS59
VaDCqB7Bn+7cZr5gJwOMy29gB9owjrxXpJEsO4RqK6LmDpk8qGMH0EtnH775NMtF
kPJAWqez8+/w36HmPEW811hVpxDdmdw1zLvkeHcNN+hN5EB1MfLVO1vFo8TXekEX
PnArVeL6Ghf2NwmeIsYS3r56xppX9KyVxYjBSSibm8Rp1AMd1pEaVOkR8uw6KJpU
lEAgmLXuCvAUx2Z5vyH8oW4AH9nvQjfEsPxp9dNtAZIa9S9Ngd/tEzZE7MGWgei7
7CbtGAMBaKSW6Lp5wPQrkWgNTtjf4ZTxFUtEtgztImqcQevb/udNjOCPzqwHVzb5
em7Zow6fv27I2/pdFGcK7FZeuEGWLWZFCzYQhd8zxuOnfqj5ZxVExovt+ogu8BJA
qvdh2DNuHC2tmttlr0RRpaYaYa0Kxnln5LzH1fm0Culd8aAs+3KtNjcYSLWrp2hk
8XMZlDRTAi1iEjs5M9vmfzsmk7548+N7zZCJsIy3yahBOT9RS0UJYA3cANb94krt
9NqOepi3bNbi9QZf0KSXDMLnhJhyPMj1mu/mvGHk3H8TCxkdxt2EWJImVhsUxXgk
YrMZc4KaOeJ8Yme/fJCMPCA5aK61nkJGgdKQT4OUIWrNpke/hesAM+8s9ZGEqrHU
+QneQCu8jHw5+YNupha6mTsGKs1qp7xNFGQ1t8rmHYvpKbJOSDGHSqjbFZXyd0HQ
elymSKKzL0bCqgTcAwjRlxeQWGmbS7k/yiA2rh2GiMHyhM/i0PsgSfPlR33TCmsS
/xp/HmrYu2wV5T770Hwg423LEYvTo7Bey8W71dDmrZkhVVsyqE/vw9k0LbVQ3IFe
XPOoitGdPrpQgHWE4IyjtVtF4p8Ay/u3qRTLcp47O+D/HB9VmiecRMasyBPjOuW/
1PyY2qh9146KJTuew2zT+59vDlCpP9/AWr4Z+ZnMMl4WLXTHEds37F2ONpRsoe24
qKTo0Tcgn5xyfdCTEykycMDayl0wjGBZ3fh6/8aDg1VWKsgll5BD/RnoAuZQ6+1B
epMjV/WbhvUcsE7d2rm0npyWlmhOZrYD3/ZI2Sd+/FRnX2o+b2w8KzxDwEhizZra
IqrK91bEZqJaQTC4Glpke75o3ZAf2Yf/ChZ3G5DE71tSAnvan3Y5cnIv5VmEELkF

//pragma protect end_data_block
//pragma protect digest_block
tNhGNc6tIdxA5Fgd5s8i/CZ4Fs0=
//pragma protect end_digest_block
//pragma protect end_protected
