/***************************************
EE 465 Deliverable 0 Part A: SRRC Filter.
Written by: Sean Froome

****************************************/
`default_nettype none
module srrc_filter(
input wire sys_clk,
input wire signed [17:0] x_in,
output reg signed [17:0] y_out
);

/***************************************
Declare and assign Filter Coefficients
***************************************
PART A/B COEFFICIENTS
**************************************/

wire signed [17:0] b[16:0];
assign	b[0] =   18'sd1631;
assign 	b[1] =  -18'sd1691;
assign	b[2] =  -18'sd5235;
assign 	b[3] =  -18'sd6109;
assign	b[4] =  -18'sd1975;
assign	b[5] =   18'sd7312;
assign	b[6] =   18'sd19115;
assign	b[7] =   18'sd28994;
assign	b[8] =   18'sd32842;

/***************************************
17 taps on the top level.
***************************************/
reg signed [17:0] x0_level_1, x1_level_1, x2_level_1, x3_level_1, x4_level_1;
reg signed [17:0] x5_level_1, x6_level_1, x7_level_1, x8_level_1, x9_level_1;
reg signed [17:0] x10_level_1, x11_level_1, x12_level_1, x13_level_1, x14_level_1;
reg signed [17:0] x15_level_1, x16_level_1;

/***************************************
First summing level, essentially second layer of filter.
***************************************/
reg signed [17:0] sum_level_1_col0, sum_level_1_col1, sum_level_1_col2;
reg signed [17:0] sum_level_1_col3, sum_level_1_col4, sum_level_1_col5;
reg signed [17:0] sum_level_1_col6, sum_level_1_col7, sum_level_1_col8;

/***************************************
 Declare Multipler Outputs. 2s34 Numbers
***************************************/
reg signed [35:0] mult_out_col0, mult_out_col1, mult_out_col2;
reg signed [35:0] mult_out_col3, mult_out_col4, mult_out_col5;
reg signed [35:0] mult_out_col6, mult_out_col7, mult_out_col8;

/***************************************************
Declare Trimmers for the Multiplier Outputs. 1s17
**********************************A*****************/
reg signed [17:0] mult_out_col0t, mult_out_col1t, mult_out_col2t;
reg signed [17:0] mult_out_col3t, mult_out_col4t, mult_out_col5t;
reg signed [17:0] mult_out_col6t, mult_out_col7t, mult_out_col8t;

/************************************************
// Second summing level, third level of filter.
*************************************************/
reg signed [17:0] sum_level_2_col0, sum_level_2_col1, sum_level_2_col2;
reg signed [17:0] sum_level_2_col3, sum_level_2_col4;

/**************************************************
// Third summing level, fourth level of filter.
***************************************************/
reg signed [17:0] sum_level_3_col0, sum_level_3_col1, sum_level_3_col2;

/***************************************
// Fourth summing level, fifth level of filter.
***************************************/
reg signed [17:0] sum_level_4_col0, sum_level_4_col1;
reg signed [17:0] sum_level_5_col0;

/***************************************
First level of filter taps. Yes it's long because why bother with for loops?
May put in a clock enable later?
Zeroith Level basically.
***************************************/
always @ (posedge sys_clk)
x0_level_1 <= {x_in[17], x_in[17:1]};
// With the input trimmed, it works.

always @ (posedge sys_clk)
x1_level_1 <= x0_level_1 ;
always @ (posedge sys_clk)
x2_level_1 <= x1_level_1 ;
always @ (posedge sys_clk)
x3_level_1 <= x2_level_1 ;
always @ (posedge sys_clk)
x4_level_1 <= x3_level_1 ;
always @ (posedge sys_clk)
x5_level_1 <= x4_level_1 ;
always @ (posedge sys_clk)
x6_level_1 <= x5_level_1 ;
always @ (posedge sys_clk)
x7_level_1 <= x6_level_1 ;
always @ (posedge sys_clk)
x8_level_1 <= x7_level_1 ;
always @ (posedge sys_clk)
x9_level_1 <= x8_level_1 ;
always @ (posedge sys_clk)
x10_level_1 <= x9_level_1 ;
always @ (posedge sys_clk)
x11_level_1 <= x10_level_1 ;
always @ (posedge sys_clk)
x12_level_1 <= x11_level_1 ;
always @ (posedge sys_clk)
x13_level_1 <= x12_level_1 ;
always @ (posedge sys_clk)
x14_level_1 <= x13_level_1 ;
always @ (posedge sys_clk)
x15_level_1 <= x14_level_1 ;
always @ (posedge sys_clk)
x16_level_1 <= x15_level_1 ;

/***************************************
First level of Summing.
***************************************/
always @ *
sum_level_1_col0 = x0_level_1 + x16_level_1;
always @ *
sum_level_1_col1 = x1_level_1 + x15_level_1;
always @ *
sum_level_1_col2 = x2_level_1 + x14_level_1;
always @ *
sum_level_1_col3 = x3_level_1 + x13_level_1;
always @ *
sum_level_1_col4 = x4_level_1 + x12_level_1;
always @ *
sum_level_1_col5 = x5_level_1 + x11_level_1;
always @ *
sum_level_1_col6 = x6_level_1 + x10_level_1;
always @ *
sum_level_1_col7 = x7_level_1 + x9_level_1;
always @ *
sum_level_1_col8 = x8_level_1;

/***************************************
First  level, multipliers.
***************************************/
always @ *
mult_out_col0 = sum_level_1_col0*b[0];
always @ *
mult_out_col1 = sum_level_1_col1*b[1];
always @ *
mult_out_col2 = sum_level_1_col2*b[2];
always @ *
mult_out_col3 = sum_level_1_col3*b[3];
always @ *
mult_out_col4 = sum_level_1_col4*b[4];
always @ *
mult_out_col5 = sum_level_1_col5*b[5];
always @ *
mult_out_col6 = sum_level_1_col6*b[6];
always @ *
mult_out_col7 = sum_level_1_col7*b[7];
always @ *
mult_out_col8 = sum_level_1_col8*b[8];
//1s17*1s17 = 2s34

/***************************************
First Level, trimmers.
**************************************
FOR PART A
(and associated Magnitude Response)
**************************************/
always @ *
mult_out_col0t = mult_out_col0[33:16];
always @ *
mult_out_col1t = mult_out_col1[33:16];
always @ *
mult_out_col2t = mult_out_col2[33:16];
always @ *
mult_out_col3t = mult_out_col3[33:16];
always @ *
mult_out_col4t = mult_out_col4[33:16];
always @ *
mult_out_col5t = mult_out_col5[33:16];
always @ *
mult_out_col6t = mult_out_col6[33:16];
always @ *
mult_out_col7t = mult_out_col7[33:16];
always @ *
mult_out_col8t = mult_out_col8[33:16];

/***************************************
Second Level summing
***************************************/
always @ *
sum_level_2_col0 = mult_out_col0t + mult_out_col1t;
always @ *
sum_level_2_col1 = mult_out_col2t + mult_out_col3t;
always @ *
sum_level_2_col2 = mult_out_col4t + mult_out_col5t;
always @ *
sum_level_2_col3 = mult_out_col6t + mult_out_col7t;
always @ *
sum_level_2_col4 = mult_out_col8t;

/***************************************
Third level summing
***************************************/
always @ *
sum_level_3_col0 = sum_level_2_col1 + sum_level_2_col2;
always @ *
sum_level_3_col1 = sum_level_2_col3 + sum_level_2_col4;
always @ *
sum_level_3_col2 = sum_level_2_col0;

/***************************************
Fourth level summing
***************************************/
always @ *
sum_level_4_col0 = sum_level_3_col0 + sum_level_3_col1;
always @ *
sum_level_4_col1 = sum_level_3_col2;

/***************************************
Fifth level summing
***************************************/
always @ *
sum_level_5_col0 = sum_level_4_col0 + sum_level_4_col1;

/***************************************
Output
***************************************/
always @ (posedge sys_clk)
y_out =  sum_level_5_col0;

endmodule
`default_nettype wire
