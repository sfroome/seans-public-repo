clear all
close all

Na = 16;
Nd = 18;

SNR_addr = -5.17 + 6.02*Na; % 187.47
SNR_data = 1.76+6.02*Nd; % 110.12

SNR = 0.5/(((pi^2)/6)*2^(-2*Na)+(1/3)*2^(-2*Nd));

SNR_dB = 10*log10(SNR); % It's like 110dB. Complete overkill. Excellent.

% Clearly the Quartus Generated NCO is perfectly fine by default. If I want
% to save registers I can very easily shave off the number of address bits
% going in however.

Adj_Channel_Freq = 6.25+1.75;
%Linear Gain Values
%G1_Lin = 6.14; 
%G2_Lin = 11.28;
%G3_Lin = 16.61;

%G1_1s17 = round(G1_Lin*2^17)
%G2_1s17 = round(G2_Lin*2^17) 
%G3_1s17 = round(G3_Lin*2^17) 



