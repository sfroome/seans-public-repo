%Idiots Guide to Setting up The Spectrum Analyzer to measure OOB Emissions
% Meas ----> meas setup ----> AD
%Offset A
Offset_A_Freq = 1.75/2+(1/2)*0.22; 
% Aka 0.9850MHz. This is OB1, referenced to the Carrier Frequency
Integrated_A = 220;%KHz


Offset_B_Freq = 1.75/2 + 0.22 + 1.53/2;
% Aka 1.86
Integrated_B = 1.53;

Offset_C_Freq = 1.75/2 + 1.53+0.22 +1.75/2;
%Aka 3.5
Integrated_C = 1.75;
