%fdatool
clear all
format long
% This one is for 0.875 to 12.5 MHz
% ie: 0.0700 roughly

a0 = -0.036194753499698535670336951852732454427;
a1 = 0;
a2 = 0.28572703736205734559305824404873419553;
a3 = 0.5;
a4 = 0.28572703736205734559305824404873419553;
a5 = 0;
a6 = -0.036194753499698535670336951852732454427;

a = zeros(1,7);
a(1) = round(a0*2^17);
a(2) = round(a1*2^17);
a(3) = round(a2*2^17);
a(4) = round(a3*2^17);
a(5) = round(a4*2^17);
a(6) = round(a5*2^17);
a(7) = round(a6*2^17);


worst_case_input_sequence = ones(1,length(a));
worst_case_scaling_factor = (abs(a)*worst_case_input_sequence')/2^17;
%a = round(a/worst_case_scaling_factor);


fid = fopen('Halfband_Coefficients1_Verilog.txt','w');
for i=1:ceil(length(a))
    if a(i)>=0
        formatSpec = 'assign a%d =  18''sd%d;\n';
    else
        formatSpec = 'assign a%d = -18''sd%d;\n';
    end
    fprintf(fid,formatSpec,i-1,abs(a(i)));
end
fclose(fid);
open Halfband_Coefficients1_Verilog.txt

%stem(a)

f = 0:1/10000:0.6;
[H,w] = freqz(a,1,2*pi*f);

plot(f,20*log10(abs(H)))
