
%NEEDS WORK
%%%%%%%%%%%%%%%%%5
clear all
clf
close all
clc

% With multiplier-based HBFilters 
%(irrelevant since the best optimized HBFilters never worked anyway)
%Using Band Interval Power (Using Trace Average)
%P_Noise = 2.1091e-6; % Measured on Spectrum Analyzer with only AWGN enabled.
%P_Signal = 22.312e-6; % Measured on Spectrum Analyzer
%Adding the adjacent channel power barely affects my band/interval power
%VBW = 1 KHz %RBW = 10 KHz

%Using Band Interval Power (Using Trace Average)
%P_Noise = 2.0360e-6; % Measured on Spectrum Analyzer with only AWGN enabled.
%P_Signal = 337e-9; % Measured on Spectrum Analyzer

%P_Noise = 1.18e-12; % pW/Hz
%P_Signal = 200e-15; %fW/Hz

P_Noise = 1.18e-12; % pW/Hz
P_Signal = 785e-15; %fW/Hz


Fc = 6.25e6;
Fs = 25e6;
%DAC_Roll_off = 1.2150e-12; % pW/Hz
DAC_Roll_off = 10.3e-18; % aW/Hz ie: minuscule
% ie: sin(piF/Fs)/(piF/Fs) is the DAC Roll Off

beta = 0.12;
Rs = (1/16)*25e6;

N0_in_W_Per_Hz = (P_Noise/(Fs/2)) * DAC_Roll_off;

%N0_in_dBm_Per_Hz = 10*log10(((P_Noise/(Fs/2))/(1e-3))*DAC_Roll_off);
%N0_in_dBm_Per_Hz_2 = 10*log10(Poise/1e-3)-10*log10(Fs/2)+10*log10(DAC_Roll_off) 
%P_Noise_dBm = 10*log10(P_Noise/1e-3);

SNR = P_Signal/P_Noise;
SNR_dB = 10*log10(SNR);

Es = SNR*N0_in_W_Per_Hz;
Eb = Es/4;

Eb_N0 = Eb/N0_in_W_Per_Hz

Normalized_SNR_dB = 10*log10(Eb/N0_in_W_Per_Hz); % ie: Eb/N_0

G1 = 7.88  - Normalized_SNR_dB; % Amount of Gain required, given how much SNR I already have. (Eb/N0 is negative?)
G2 = 10.52 - Normalized_SNR_dB; %
G3 = 12.20 - Normalized_SNR_dB; %

%10^(7.88/10) = 6.1376. Just an idiot check on coverting back from dB to
%Linear units

G1_Linear = 10^(G1/10)
G2_Linear = 10^(G2/10)
G3_Linear = 10^(G3/10)

G1_Linear_1s17 = round(G1_Linear*2^10)
G2_Linear_1s17 = round(G2_Linear*2^10)
G3_Linear_1s17 = round(G3_Linear*2^10)


G3p = -9.093;
G2p = -12.348;
G1p = -17.618;
No_Gain= -22.44;
Noise = -25.86;

%V1 = Noise-G1p
%V2 = Noise-G2p
%V3 = Noise-G3p

%V4 = No_Gain-G1p
%V5 = No_Gain-G2p
%V6 = No_Gain-G3p


