clear all

N_sps = 4; % 4 Samples per Symbol
F_s = 1; % sampling rate in samples/second
f_6dB = .1250;

df = 1/6250;
f = 0:df:(1-df);
n = 1:length(f);
n_max = length(n);
n_min = 1;

beta_RCV = 0.12;
N_srrc_RCV = 129;
h_srrc_RCV = firrcos(N_srrc_RCV-1,f_6dB,beta_RCV,F_s,'rolloff','sqrt');
[H_RCV_Gold,w] = freqz(h_srrc_RCV,1,2*pi*f);
h_srrc_RCV = h_srrc_RCV/max(abs(H_RCV_Gold));
h_RCV_Gold = h_srrc_RCV;

h_RCV_Gold_1s17  = round(h_RCV_Gold*2^17);
worst_case_input_sequence = ones(1,length(h_RCV_Gold_1s17));
worst_case_scaling_factor = abs(h_RCV_Gold_1s17)*worst_case_input_sequence';
worst_case_scaling_factor = worst_case_scaling_factor/(2^17);

h_RCV_Gold_1s17 = round(h_RCV_Gold_1s17/worst_case_scaling_factor);
H_RX_dB = 20*log10(abs(H_RCV_Gold));

fid = fopen('filt_coeffs_verilog_code_rcv_gold.txt','w');
for i=1:ceil(length(h_RCV_Gold_1s17))
    if h_RCV_Gold_1s17(i)>=0
        formatSpec = '      a[%d] =  18''sd%d;\n';
    else
        formatSpec = '      a[%d] = -18''sd%d;\n';
    end
    fprintf(fid,formatSpec,i-1,abs(h_RCV_Gold_1s17(i)));
end
fclose(fid);
%open filt_coeffs_verilog_code_rcv_gold.txt
