% Gold Received Filter Specs (same as Gold TX)

N = 113;
beta = 0.08;
beta_kaiser = 2.01;
f_6dB = 0.1250;
N_sps = 4; % 4 Samples per Symbol
F_s = 1; % sampling rate in samples/second

df = 1/6250;
f = 0:df:(1-df);
n = 1:length(f);
n_max = length(n);
n_min = 1;


beta_RCV = 0.12;
N_srrc_RCV = 129;
h_srrc_RCV = firrcos(N_srrc_RCV-1,f_6dB,beta_RCV,F_s,'rolloff','sqrt');
[H_RCV_Gold,w] = freqz(h_srrc_RCV,1,2*pi*f);
h_srrc_RCV = h_srrc_RCV/max(abs(H_RCV_Gold));
h_RCV_Gold = h_srrc_RCV;

h_mer = conv(h_RCV_Gold, h_RCV_Gold);
h_mer_normalized = h_mer/max(abs(h_mer));
h_mer_normalized_zeros = h_mer_normalized(1:4:end);

ISI = sum( (h_mer_normalized_zeros).^2) - 1;
MER = 1/ISI;
MER_dB = 10*log10(MER);
NEW_MER_dB = MER_dB;