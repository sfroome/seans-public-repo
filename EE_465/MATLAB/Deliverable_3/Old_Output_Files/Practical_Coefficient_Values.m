clear all
clf
close all

N_sps = 4; % 4 Samples per Symbol
F_s = 1; % sampling rate in samples/second
f_6dB = .1250;

df = 1/6250;
%f = -(1-df):df:(1-df);
f = 0:df:(1-df);
n = 1:length(f);
n_max = length(n);
n_min = 1;

beta_kaiser_TX = 1.85;
beta_TX = 0.08;
N_srrc_TX = 113;

h_srrc_TX = firrcos(N_srrc_TX-1,f_6dB,beta_TX,F_s,'rolloff','sqrt');
win_TX  = kaiser( N_srrc_TX, beta_kaiser_TX)';
h_TX_PRAC = h_srrc_TX.*win_TX;
[H_TX_PRAC, n ] = freqz(h_TX_PRAC,1,2*pi*f);
h_TX_PRAC = h_TX_PRAC/max(abs(H_TX_PRAC)); % Scaling

p1 = round((1.75/2)*1e3); % Right Edge of Passband
p2 = round((1.75/2 + 0.22)*1e3); % Right Edge of OB1
p3 = round((1.75/2 + 0.22 + 1.53)*1e3); % Right Edge of OB2
p4 = round((1.75/2 + 0.22 + 1.53 + 1.75)*1e3); % Right Edge of Adjacent Channel 2


hold all
grid on
H_TX_dB = 20*log10(abs(H_TX_PRAC));
figure(1)
plot(n(1:p1), H_TX_dB(1:p1), 'r')
plot(n(p1+1:p2), H_TX_dB((p1+1):p2), 'g')
plot(n(p2+1:p3), H_TX_dB((p2+1):p3), 'b')
plot(n(p3+1:p4), H_TX_dB((p3+1):p4), 'c')
plot(n(p4+1:n_max), H_TX_dB((p4+1):n_max), 'k')
title('Practical Pulse Shaping Filter')
ylabel ('abs(H_TX_Practical) (dBm)')
xlabel('Frequency (f)')

h_TX_PRAC_1s17 = round(h_TX_PRAC*2^17)';
%worst_case_input_sequence2 = ones(1,length(h_TX_PRAC_1s17));
%worst_case_scaling_factor2 = abs(h_TX_PRAC_1s17)'*worst_case_input_sequence2';
%worst_case_scaling_factor2 = worst_case_scaling_factor2/(2^17);
%h_TX_PRAC_1s17 = (h_TX_PRAC_1s17)/worst_case_scaling_factor2;
%h_TX_PRAC_1s17 = round(h_TX_PRAC_1s17);

%figure(2)
%stem(h_TX_PRAC)
title('Practical Pulse Shaping Filter')
ylabel ('Amplitude')
xlabel('Frequency (f)')



fid = fopen('filt_coeffs_verilog_code_tx_prac.txt','w');
for i=1:ceil(length(h_TX_PRAC_1s17))
    if h_TX_PRAC_1s17(i)>=0
        formatSpec = '      a[%3.0f] =  18''sd %6.0f;\n';
    else
        formatSpec = '      a[%3.0f] = -18''sd %6.0f;\n';
    end
    fprintf(fid,formatSpec,i-1,abs(h_TX_PRAC_1s17(i)));
end
fclose(fid);
%open filt_coeffs_verilog_code_tx_prac.txt