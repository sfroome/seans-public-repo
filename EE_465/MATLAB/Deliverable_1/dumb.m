

beta = 0

df = 1/2000; % frequency increment in cycles/sample
f = [0:df:0.5-df/2]; % cycles/sample; 0 to almost 1/2

h_TX=firrcos(N_srrc-1,f_6dB,beta,F_s,'rolloff','sqrt');

figure
plot(f,20*log10(abs(freqz(h_TX,1,2*pi*f))))
grid
grid on
ylabel('Magnitude Response of Transmit Filter (dB)')
xlabel('Frequency (rad/sample)')
title('Transmit Filter')