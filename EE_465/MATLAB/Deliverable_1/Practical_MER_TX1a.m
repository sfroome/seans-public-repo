% PRACTICAL MER CALCULATION %

%Values taken from ModelSim waveform.

%Starting at Counter = 5
h00 = 0; % C =4;
h0 = 2;
h1 = -10;
h2 = -22;
h3 = 2;
h4 = 105;
h5 = 244; % Counter = 10;
h6 = 241;
h7 = -153;
h8 = -1049;
h9 = -2166;
h10 = -2778; % Counter  = 15.
h11 = -1945;
h12 = 941;
h13 = 5672;
h14 = 11071;
h15 = 15374; %Counter = 20.
h16 = 17026; % CENTER.
h17 = 15374;
h18 = 11071;
h19 = 5672;
h20 = 941; % Counter  = 25.
h21 = -1945;
h22 = -2778;
h23 = -2166;
h24 = -1049;
h25 = -153; % Counter = 30;
h26 = 241; 
h27 = 244;
h28 = 105;
h29 = 2;
h30 = -22; % Counter  = 35.
h31 = -10;
h32 = 2;
h33 = 0;

MER = h16^2/(h20^2+h24^2+h28^2+h32^2+h12^2+h8^2+h4^2+h2^2)

MER_dB = 10*log10(MER) % =   18.607843603283534
% Theoretical is 18.6031 dB.



