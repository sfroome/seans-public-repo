% LAB NOTES

% Find the mode button.... Leftmost row, second from the button. 

% 

% length 9 scale 1.25 : MER of 17.146 dB
% length 9 scale 0.8 : MER of 1.213 dB
% length 17 scale 1.25 : MER of 24.5 dB
% length 17 scale 0.8 : MER of9.5 dB
% length 25 scale 1.25 : MER of31 dB
% length 25 scale 0.8 : MER of 8.7 dB
% length 29 scale 1.25 : MER 33.4 dB
% length 29 scale 0.8 : MER of 7.8 dB

%Longer filter = better MER. Of course the two longer filters have superior
% MER to the length 9 or even the length 17 filters.
% Unless of course your scaling by 0.8 meaning your filter coefficients
% blow up and therefore your filter is useless.
