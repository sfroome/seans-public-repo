clc
clear all

format long


Rs = 1.5625e6; % Symbol Rate.
Fc = (1/2)* Rs; % Fc is cutoff frequency
r = 0.25; % r is rolloff factor

Fs = 4 * Rs ; % Fs is sampling rate, which is 4 times symbol rate 
%(4 samples per symbol)

% 4 samples per symbol.
Ts = 1/Rs;


% For a length 9 filter
M = 9; % M is length. 
N = M-1; % N is order.
firrcos(N, Fc, r, Fs, 'rolloff', 'sqrt');
coefficients_N8 = ans/0.267077471545948; % for length 9 filter.

h = firrcos(N, Fc, r, Fs, 'rolloff', 'sqrt');
freqz(h)
figure(2)
h1 = coefficients_N8;
freqz(h1)

figure(3)
h2 = coefficients_N8/1.25;
freqz(h2)

figure(4)
h3 = coefficients_N8/0.8;
freqz(h3)
