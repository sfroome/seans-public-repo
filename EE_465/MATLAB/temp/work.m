clear all
clf
close all
format long

N_sps = 4;
F_s = 1; % sampling rate in samples/second


%f_6dB = 1/(2*N_sps);
f_6dB = .1250; %1/(2*N_sps);
%df = 1/25000;
df = 1/(6250);
f = 0:df:(1-df);

%n = 1:0.5:((length(f)+1)/2);
n = 1:length(f);
n_max = length(n);
n_min = 1;

beta_TX = 1;
beta_kaiser_TX = 0.5;
N_srrc_TX = 200;

h_srrc_TX = firrcos(N_srrc_TX,f_6dB,beta_TX,F_s,'rolloff','sqrt');
win_TX  = kaiser( N_srrc_TX+1, beta_kaiser_TX)';
h_TX_Gold = h_srrc_TX.*win_TX;

[H_TX_Gold, w ] = freqz(h_TX_Gold,1,2*pi*f);%, 'whole');
H_TX_dB = 20*log10(abs(H_TX_Gold));



p1 = round((1.75/2)*1e3); % Right Edge of Passband
p2 = round((1.75/2 + 0.22)*1e3); % Right Edge of OB1
p3 = round((1.75/2 + 0.22 + 1.53)*1e3); % Right Edge of OB2
p4 = round((1.75/2 + 0.22 + 1.53 + 1.75)*1e3); % Right Edge of Adjacent Channel 2

%Power_H_TX = sum(abs(H_TX_Gold(n_min:n_max)).^2);
Power_OB1 = sum(abs(H_TX_Gold(p1:p2)).^2);
Power_OB2 = sum(abs(H_TX_Gold(p2:p3)).^2);
Power_Ajc2 = sum(abs(H_TX_Gold(p3:n_max)).^2);
% Can't actually use P4 as it's too long (and too lazy to change the lengths above)
P_Signal = sum(abs(H_TX_Gold(1:p1)).^2);

%10*log10(Power_OB1/(2*Power_H_TX)) %Same formula
Pdiff1 = (1/2)*Power_OB1/P_Signal;
Pdiff1_dB = 10*log10(Pdiff1);


Pdiff2 = (1/2)*Power_OB2/P_Signal;
Pdiff2_dB = 10*log10(Pdiff2);

Pdiff3 = (1/2)*Power_Ajc2/P_Signal;
Pdiff3_dB = 10*log10(Pdiff3);

% Watch out. At worst (I think) I'll be off by 0.001 MHz, but still. 
hold all 
figure(1) % Plots graph from 1 to 12.5
plot(n(1:p1), H_TX_dB(1:p1), 'r')
plot(n(p1+1:p2), H_TX_dB((p1+1):p2), 'g')
plot(n(p2+1:p3), H_TX_dB((p2+1):p3), 'b')
plot(n(p3+1:p4), H_TX_dB((p3+1):p4), 'c')
plot(n(p4+1:n_max), H_TX_dB((p4+1):n_max), 'k')
%figure(2)
%plot(n,H_TX_dB)
