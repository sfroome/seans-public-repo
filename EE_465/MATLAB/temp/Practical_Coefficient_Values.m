clear all

N = 113;
beta = 0.08;
beta_kaiser = 2.01;
f_6dB = 0.1250;
N_sps = 4; % 4 Samples per Symbol
F_s = 1; % sampling rate in samples/second

h_srrc_TX = firrcos(N-1,f_6dB,beta,F_s,'rolloff','sqrt');
win_TX  = kaiser( N, beta_kaiser)';
h_TX_PRAC_1s17 = round(h_srrc_TX.*win_TX*2^17)';

worst_case_input_sequence = ones(1,length(h_TX_PRAC_1s17));
worst_case_scaling_factor = abs(h_TX_PRAC_1s17)'*(worst_case_input_sequence)';
worst_case_scaling_factor = worst_case_scaling_factor/2^17

fid = fopen('filt_coeffs_multiplierless_verilog_code_tx_prac.txt','w');
for i=1:ceil(length(h_TX_PRAC_1s17))
    if h_TX_PRAC_1s17(i)>=0
        formatSpec = 'always @ (posedge sys_clk) \n';
        formatSpec1 = 'case(x%d) \n';
        formatSpec2 = ' 3''b000: multiplier_out%d = 18''sd0;  \n';
        formatSpec3 = ' 3''b001: multiplier_out%d = 18''sd0;  \n';
        formatSpec4 = ' 3''b011: multiplier_out%d = 18''sd0;  \n';
        formatSpec5 = ' 3''b010: multiplier_out%d = 18''sd0;  \n';
        formatSpec6 = ' 3''b100: multiplier_out%d = -18''sd%d; \n';
        formatSpec7 = ' 3''b101: multiplier_out%d = -18''sd%d; \n';
        formatSpec8 = ' 3''b111: multiplier_out%d = 18''sd%d;  \n';
        formatSpec9 = ' 3''b110: multiplier_out%d = 18''sd%d;  \n';
        formatSpec10 = ' default: multiplier_out%d = -18''sd0; \n';
        formatSpec11 = ' endcase \n';
        formatSpec12 = ' \n';
    else
        formatSpec = 'always @ (posedge sys_clk) \n';
        formatSpec1 = 'case(x%d) \n';
        formatSpec2 = ' 3''b000: multiplier_out%d = 18''sd0; \n';
        formatSpec3 = ' 3''b001: multiplier_out%d = 18''sd0; \n';
        formatSpec4 = ' 3''b011: multiplier_out%d = 18''sd0; \n';
        formatSpec5 = ' 3''b010: multiplier_out%d = 18''sd0; \n';
        formatSpec6 = ' 3''b100: multiplier_out%d = 18''sd%d; \n';
        formatSpec7 = ' 3''b101: multiplier_out%d = 18''sd%d; \n';
        formatSpec8 = ' 3''b111: multiplier_out%d = -18''sd%d; \n';
        formatSpec9 = ' 3''b110: multiplier_out%d = -18''sd%d; \n';
        formatSpec10 = ' default: multiplier_out%d = -18''sd0; \n';
        formatSpec11 = ' endcase \n';
        formatSpec12 = ' \n';
    end

    fprintf(fid,formatSpec,i-1,abs(h_TX_PRAC_1s17(i)));
    fprintf(fid,formatSpec1,i-1);
    fprintf(fid,formatSpec2,i-1);
    fprintf(fid,formatSpec3,i-1);
    fprintf(fid,formatSpec4,i-1);
    fprintf(fid,formatSpec5,i-1);
    fprintf(fid,formatSpec6,i-1,round(abs(h_TX_PRAC_1s17(i))*0.75));
    fprintf(fid,formatSpec7,i-1,round(abs(h_TX_PRAC_1s17(i))*0.25));
    fprintf(fid,formatSpec8,i-1,round(abs(h_TX_PRAC_1s17(i))*0.25));
    fprintf(fid,formatSpec9,i-1,round(abs(h_TX_PRAC_1s17(i))*0.75));
    fprintf(fid,formatSpec10,i-1);
    fprintf(fid,formatSpec11,i-1);
    fprintf(fid,formatSpec12,i-1);
end
fclose(fid);
%open filt_coeffs_multiplierless_verilog_code_tx_prac.txt
