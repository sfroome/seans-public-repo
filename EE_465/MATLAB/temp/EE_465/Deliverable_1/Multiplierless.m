x = [-0.75 -0.25 0.25 0.75];
% New new values. (kaiser window value of increment 0.005)
b0 =    0.001776994167463;
b1 =   -0.004995754193124;
b2 =   -0.017243420599009;
b3 =   -0.023708973462368;
b4 =   -0.007056481626620;
b5 =    0.042769328830437;
b6 =    0.115760608133232;
b7 =    0.182160172205852;
b8 =    0.209056533563791;

% New Values (because I ran using 0.01 as an increment for beta_k)
%b0 =   0.001708895000780;
%b1 =  -0.005043417947283;
%b2 =  -0.017224640545618;
%b3 =  -0.023593107887802;
%b4 =  -0.006862749059135;
%b5 =   0.042969282581550;
%b6 =   0.115887674273754;
%b7 =   0.182189597845478;
%b8 =   0.209041269717199;

%Old Values (Might still use these (beta = .16, beta_k = 3.1)
%b0 =   0.001491459110448;
%b1 =  -0.005312579213026;
%b2 =  -0.017328534755993;
%b3 =  -0.023276103690794;
%b4 =  -0.006109721144369;
%b5 =   0.043807080208864;
%%b6 =   0.116326366581783;
%b7 =   0.182040633278014;
%b8 =   0.208615044033420;

multiplier_out0 = x.*b0;
multiplier_out1 = x.*b1;
multiplier_out2 = x.*b2;
multiplier_out3 = x.*b3;
multiplier_out4 = x.*b4;
multiplier_out5 = x.*b5;
multiplier_out6 = x.*b6;
multiplier_out7 = x.*b7;
multiplier_out8 = x.*b8;


multiplier_out0_1s17 = (round(multiplier_out0*2^17))'
multiplier_out1_1s17 = (round(multiplier_out1*2^17))'
multiplier_out2_1s17 = (round(multiplier_out2*2^17))'
multiplier_out3_1s17 = (round(multiplier_out3*2^17))'
multiplier_out4_1s17 = (round(multiplier_out4*2^17))'
multiplier_out5_1s17 = (round(multiplier_out5*2^17))'
multiplier_out6_1s17 = (round(multiplier_out6*2^17))'
multiplier_out7_1s17 = (round(multiplier_out7*2^17))'
multiplier_out8_1s17 = (round(multiplier_out8*2^17))'



