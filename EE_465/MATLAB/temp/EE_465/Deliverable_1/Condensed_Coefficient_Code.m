clear all
close all

N_srrc = 17; % length of the impulse response of the
% square root raised cosine filter
N_sps = 4;
F_s = 1; % sampling rate in samples/second
f_6dB = 1/2/N_sps; % 6 dB down point in cycles/sample
%beta = 0.01;
%beta = 0.16;
%beta = 0.150;

beta =1


df = 1/2000; % frequency increment in cycles/sample
f = [0:df:0.5-df/2]; % cycles/sample; 0 to almost 1/2
h_srrc=firrcos(N_srrc-1,f_6dB,beta,F_s,'rolloff','sqrt');
% impulse response of rc filter

%beta_kaiser = 3.25;
%beta_kaiser = 3.1;
beta_kaiser = 3.0150;
win  = kaiser( N_srrc, beta_kaiser)';
h_TX = h_srrc.*win;

beta_RCV = 0.25;

h_RCV=firrcos(N_srrc-1,F_s/8,beta_RCV,F_s,'rolloff','sqrt');

h_mer = conv(h_RCV, h_TX);
h_mer_normalized = h_mer/max(abs(h_mer));
h_mer_normalized_zeros = h_mer_normalized(1:4:end);
ISI = sum( (h_mer_normalized_zeros).^2) - 1;
MER_1 = 1/ISI;
MER_1_dB = 10*log10(MER_1)

worst_case_input_sequence = [1 -1 -1 -1 -1 1 1 1 1 1 1 1 -1 -1 -1 -1 1];
Worst_Case_Scaling_Factor_TX = h_TX*worst_case_input_sequence';
Worst_Case_Scaling_Factor_RCV = h_RCV*worst_case_input_sequence';

h_TX = h_TX/Worst_Case_Scaling_Factor_TX;
h_RCV = h_RCV/Worst_Case_Scaling_Factor_RCV;


figure
plot(f,20*log10(abs(freqz(h_TX,1,2*pi*f))))
grid
grid on
ylabel('Magnitude Response of Transmit Filter (dB)')
xlabel('Frequency (rad/sample)')
title('Transmit Filter')

h_TX_1s17 = (round(h_TX*2^17))';
h_RCV_1s17 = (round(h_RCV*2^17))';

fid = fopen('filt_coeffs_verilog_code_tx.txt','w');
        for i=1:ceil(length(h_TX_1s17))
            if h_TX_1s17(i)>=0
                formatSpec = '      b[%3.0f] =  18''sd %6.0f;\n';
            else
                formatSpec = '      b[%3.0f] = -18''sd %6.0f;\n';
            end
            fprintf(fid,formatSpec,i-1,abs(h_TX_1s17(i)));
        end
        fclose(fid);
        open filt_coeffs_verilog_code_tx.txt 
        
fid = fopen('filt_coeffs_verilog_code_rcv.txt','w');
        for i=1:ceil(length(h_RCV_1s17))
            if h_RCV_1s17(i)>=0
                formatSpec = '      a[%3.0f] =  18''sd %6.0f;\n';
            else
                formatSpec = '      a[%3.0f] = -18''sd %6.0f;\n';
            end
            fprintf(fid,formatSpec,i-1,abs(h_RCV_1s17(i)));
        end
        fclose(fid);
        open filt_coeffs_verilog_code_rcv.txt