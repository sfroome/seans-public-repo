% EE 465 Deliverable 1
% Written by: Sean Froome 

clear all
close all
format long

N_srrc = 17; % length of the impulse response of the
% square root raised cosine filter
N_sps = 4;
F_s = 1; % sampling rate in samples/second
f_6dB = 1/(2*N_sps); % 6 dB down point in cycles/sample

df = 1/2000; % frequency increment in cycles/sample
f = [0:df:0.5-df/2]; % cycles/sample; 0 to almost 1/2

beta_RCV = 0.25;
h_RCV=firrcos(N_srrc-1,f_6dB,beta_RCV,F_s,'rolloff','sqrt');
spec_not_met = 0;
best_MER_dB = 0;

for beta = 0:0.005:1
%for beta = 0:0.01:1
        %for beta_kaiser = 3:0.01:4
        for beta_kaiser = 3:0.005:4
            h_srrc=firrcos(N_srrc-1,f_6dB,beta,F_s,'rolloff','sqrt');
            win  = kaiser( N_srrc, beta_kaiser)';
            h_TX = h_srrc.*win;
            [H_TX,w] = freqz(h_TX,1,2*pi*f);
            for n = 1:length(w) % n is the index.
                % Check that the filter actually meets spec.
                if (20*log10(abs(H_TX(n))) > -40) && (w(n)/(2*pi) >= 0.2000)
                    
                  %  if (20*log10(abs(H_TX(n))) > -40) && (w(n)/(2*pi) >= 0.1950)                    
                 %if (20*log10(abs(H_TX(n))) > -40) && (w(n)/(2*pi) >= 0.1945)                   
                 % If filter does NOT meet spec             
                spec_not_met = 1;
                    break;
                end
            end           
        if( spec_not_met ~= 1)    
            h_mer = conv(h_RCV, h_TX);
            h_mer_normalized = h_mer/max(abs(h_mer));
            h_mer_normalized_zeros = h_mer_normalized(1:4:end);

            ISI = sum( (h_mer_normalized_zeros).^2) - 1;
            MER = 1/ISI;
            MER_dB = 10*log10(MER);
            NEW_MER_dB = MER_dB;
            
            if NEW_MER_dB > best_MER_dB
               best_MER = MER;
               best_MER_dB = NEW_MER_dB;
               best_ISI = ISI;
               best_beta = beta;
               best_beta_kaiser = beta_kaiser; 
            end            
        end
        spec_not_met = 0;
    end
end

h_srrc=firrcos(N_srrc-1,f_6dB, best_beta,F_s,'rolloff','sqrt');
win  = kaiser( N_srrc, best_beta_kaiser)';
h_TX = h_srrc.*win;
[H_TX,w] = freqz(h_TX,1,2*pi*f);
[H_RCV,w] = freqz(h_RCV,1,2*pi*f);

worst_case_input_sequence = [1 -1 -1 -1 -1 1 1 1 1 1 1 1 -1 -1 -1 -1 1];
Worst_Case_Scaling_Factor_TX = h_TX*worst_case_input_sequence';
Worst_Case_Scaling_Factor_RCV = h_RCV*worst_case_input_sequence';

h_TX = h_TX/Worst_Case_Scaling_Factor_TX;
h_RCV = h_RCV/Worst_Case_Scaling_Factor_RCV;

h_mer = conv(h_RCV, h_TX);
h_mer_normalized = h_mer/max(abs(h_mer));
h_mer_normalized_zeros = h_mer_normalized(1:4:end);

ISI = sum( (h_mer_normalized_zeros).^2) - 1;
MER = 1/ISI;
MER_dB = 10*log10(MER);

best_MER;
best_MER_dB;
h_TX_1s17 = (round(h_TX*2^17))';
h_RCV_1s17 = (round(h_RCV*2^17))';

figure
plot(f,20*log10(abs(H_TX)))
ylabel('Magnitude Response of Transmit Filter (dB)')
xlabel('Frequency (cycles/sample)')
grid on
title('Transmit Filter')

figure
plot(f,20*log10(abs(H_RCV)))
grid on
ylabel('Magnitude Response of Received Filter (dB)')
xlabel('Frequency (cycles/sample)')
title('Received Filter')

fid = fopen('filt_coeffs_verilog_code_tx.txt','w');
        for i=1:ceil(length(h_TX_1s17))
            if h_TX_1s17(i)>=0
                formatSpec = '      b[%3.0f] =  18''sd %6.0f;\n';
            else
                formatSpec = '      b[%3.0f] = -18''sd %6.0f;\n';
            end
            fprintf(fid,formatSpec,i-1,abs(h_TX_1s17(i)));
        end
        fclose(fid);
        open filt_coeffs_verilog_code_tx.txt 
        
fid = fopen('filt_coeffs_verilog_code_rcv.txt','w');
        for i=1:ceil(length(h_RCV_1s17))
            if h_RCV_1s17(i)>=0
                formatSpec = '      a[%3.0f] =  18''sd %6.0f;\n';
            else
                formatSpec = '      a[%3.0f] = -18''sd %6.0f;\n';
            end
            fprintf(fid,formatSpec,i-1,abs(h_RCV_1s17(i)));
        end
        fclose(fid);
        open filt_coeffs_verilog_code_rcv.txt


