fid = fopen('filt_coeffs_verilog_code_tx.txt','w');
        for i=1:ceil(length(coefficients_for_verilog_tx)/2)
            if coefficients_for_verilog_tx(i)>=0
                formatSpec = '      b[%3.0f] =  18''sd %6.0f;\n';
            else
                formatSpec = '      b[%3.0f] = -18''sd %6.0f;\n';
            end
            fprintf(fid,formatSpec,i-1,abs(coefficients_for_verilog_tx(i)));
        end
        fclose(fid);
        open filt_coeffs_verilog_code_tx.txt 
        
 fid = fopen('filt_coeffs_verilog_code_rcv.txt','w');
        for i=1:ceil(length(coefficients_for_verilog_rcv)/2)
            if coefficients_for_verilog_rcv(i)>=0
                formatSpec = '      a[%3.0f] =  18''sd %6.0f;\n';
            else
                formatSpec = '      a[%3.0f] = -18''sd %6.0f;\n';
            end
            fprintf(fid,formatSpec,i-1,abs(coefficients_for_verilog_rcv(i)));
        end
        fclose(fid);
        open filt_coeffs_verilog_code_rcv.txt
