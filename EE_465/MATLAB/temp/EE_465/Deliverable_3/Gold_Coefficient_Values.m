clear all
clf
close all

N_sps = 4; % 4 Samples per Symbol
F_s = 1; % sampling rate in samples/second
f_6dB = .1250;

df = 1/6250;
f = 0:df:(1-df);
n = 1:length(f);
n_max = length(n);
n_min = 1;

beta_kaiser_TX = 1.85;
beta_TX = 0.08;
N_srrc_TX = 113;

beta_RCV = 0.12;
N_srrc_RCV = 129;
h_srrc_RCV = firrcos(N_srrc_RCV-1,f_6dB,beta_RCV,F_s,'rolloff','sqrt');
[H_RCV_Gold,w] = freqz(h_srrc_RCV,1,2*pi*f);
h_srrc_RCV = h_srrc_RCV/max(abs(H_RCV_Gold));
h_RCV_Gold = h_srrc_RCV;

h_RCV_Gold_1s17  = round(h_RCV_Gold*2^17);
worst_case_input_sequence = ones(1,length(h_RCV_Gold_1s17));
worst_case_scaling_factor = abs(h_RCV_Gold_1s17)*worst_case_input_sequence';
worst_case_scaling_factor = worst_case_scaling_factor/(2^17);

h_RCV_Gold_1s17 = round(h_RCV_Gold_1s17/worst_case_scaling_factor);

p1 = round((1.75/2)*1e3); % Right Edge of Passband
p2 = round((1.75/2 + 0.22)*1e3); % Right Edge of OB1
p3 = round((1.75/2 + 0.22 + 1.53)*1e3); % Right Edge of OB2
p4 = round((1.75/2 + 0.22 + 1.53 + 1.75)*1e3); % Right Edge of Adjacent Channel 2

hold all
grid on
%figure(1)
%plot(n(1:p1), H_TX_dB(1:p1), 'r')
%plot(n(p1+1:p2), H_TX_dB((p1+1):p2), 'g')
%plot(n(p2+1:p3), H_TX_dB((p2+1):p3), 'b')
%plot(n(p3+1:p4), H_TX_dB((p3+1):p4), 'c')
%plot(n(p4+1:n_max), H_TX_dB((p4+1):n_max), 'k')
%title('Practical Pulse Shaping Filter')
%ylabel ('abs(H_TX_Practical) (dBm)')
%xlabel('Frequency (f)')

H_RX_dB = 20*log10(abs(H_RCV_Gold));
%figure(2)
%hold all
%plot(n(1:p1), H_RX_dB(1:p1), 'r')
%plot(n(p1+1:p2), H_RX_dB((p1+1):p2), 'g')
%plot(n(p2+1:p3), H_RX_dB((p2+1):p3), 'b')
%plot(n(p3+1:p4), H_RX_dB((p3+1):p4), 'c')
%plot(n(p4+1:n_max), H_RX_dB((p4+1):n_max), 'k')

N_srrc_TX = 129;
beta_TX = 0.12;
h_srrc_TX = firrcos(N_srrc_TX-1,f_6dB,beta_TX,F_s,'rolloff','sqrt');
[H_TX_Gold, n ] = freqz(h_srrc_TX,1,2*pi*f);
h_TX_Gold = h_srrc_TX/max(abs(H_TX_Gold)); % Scaling
H_TX_dB1 = 20*log10(abs(H_TX_Gold));

%figure(3)
%hold all
%plot(n(1:p1), H_TX_dB1(1:p1), 'r')
%plot(n(p1+1:p2), H_TX_dB1((p1+1):p2), 'g')
%plot(n(p2+1:p3), H_TX_dB1((p2+1):p3), 'b')
%plot(n(p3+1:p4), H_TX_dB1((p3+1):p4), 'c')
%plot(n(p4+1:n_max), H_TX_dB1((p4+1):n_max), 'k')

h_TX_Gold_1s17 = round(h_TX_Gold*2^17)'
worst_case_input_sequence3 = ones(1,length(h_TX_Gold_1s17));
worst_case_scaling_factor3 = abs(h_TX_Gold_1s17)'*worst_case_input_sequence3';
worst_case_scaling_factor3 = worst_case_scaling_factor3/(2^17);
h_TX_Gold_1s17 = round(h_TX_Gold_1s17/worst_case_scaling_factor3);

h_TX_Gold_1s17
worst_case_scaling_factor3 = abs(h_TX_Gold_1s17)'*worst_case_input_sequence3'/(2^17);


fid = fopen('filt_coeffs_verilog_code_tx_gold.txt','w');
for i=1:ceil(length(h_TX_Gold_1s17))
    if h_TX_Gold_1s17(i)>=0
        formatSpec = '      b[%3.0f] =  18''sd %6.0f;\n';
    else
        formatSpec = '      b[%3.0f] = -18''sd %6.0f;\n';
    end
    fprintf(fid,formatSpec,i-1,abs(h_TX_Gold_1s17(i)));
end
fclose(fid);
%open filt_coeffs_verilog_code_tx_gold.txt

fid = fopen('filt_coeffs_verilog_code_rcv_gold.txt','w');
for i=1:ceil(length(h_RCV_Gold_1s17))
    if h_RCV_Gold_1s17(i)>=0
        formatSpec = '      a[%3.0f] =  18''sd %6.0f;\n';
    else
        formatSpec = '      a[%3.0f] = -18''sd %6.0f;\n';
    end
    fprintf(fid,formatSpec,i-1,abs(h_RCV_Gold_1s17(i)));
end
fclose(fid);
%open filt_coeffs_verilog_code_rcv_gold.txt


fid = fopen('filt_coeffs_multiplierless_verilog_code_tx_gold.txt','w');
for i=1:ceil(length(h_TX_Gold_1s17))
    if h_TX_Gold_1s17(i)>=0
        formatSpec = 'always @ (posedge sys_clk) \n';
        formatSpec1 = 'case(x %3.0f) \n';
        formatSpec2 = ' 3''b000: multiplier_out%3.0f = 18''sd0;  \n';
        formatSpec3 = ' 3''b001: multiplier_out%3.0f = 18''sd0;  \n';
        formatSpec4 = ' 3''b010: multiplier_out%3.0f = 18''sd0;  \n';
        formatSpec5 = ' 3''b011: multiplier_out%3.0f = 18''sd0;  \n';
        formatSpec6 = ' 3''b100: multiplier_out%3.0f = -18''sd %6.0f; \n';
        formatSpec7 = ' 3''b101: multiplier_out%3.0f = -18''sd %6.0f; \n';
        formatSpec8 = ' 3''b110: multiplier_out%3.0f = 18''sd %6.0f;  \n';
        formatSpec9 = ' 3''b111: multiplier_out%3.0f = 18''sd %6.0f;  \n';
        formatSpec10 = ' default: multiplier_out%3.0f = -18''sd0; \n';
        formatSpec11 = ' endcase \n';
        formatSpec12 = ' \n';
    else
        formatSpec = 'always @ (posedge sys_clk) \n';
        formatSpec1 = 'case(x %3.0f) \n';
        formatSpec2 = ' 3''b000: multiplier_out%3.0f = 18''sd0; \n';
        formatSpec3 = ' 3''b001: multiplier_out%3.0f = 18''sd0; \n';
        formatSpec4 = ' 3''b010: multiplier_out%3.0f = 18''sd0; \n';
        formatSpec5 = ' 3''b011: multiplier_out%3.0f = 18''sd0; \n';
        formatSpec6 = ' 3''b100: multiplier_out%3.0f = 18''sd %6.0f; \n';
        formatSpec7 = ' 3''b101: multiplier_out%3.0f = 18''sd %6.0f; \n';
        formatSpec8 = ' 3''b110: multiplier_out%3.0f = -18''sd %6.0f; \n';
        formatSpec9 = ' 3''b111: multiplier_out%3.0f = -18''sd %6.0f; \n';
        formatSpec10 = ' default: multiplier_out%3.0f = -18''sd0; \n';
        formatSpec11 = ' endcase \n';
        formatSpec12 = ' \n';
    end

    fprintf(fid,formatSpec,i-1,abs(h_TX_Gold_1s17(i)));
    fprintf(fid,formatSpec1,i-1);
    fprintf(fid,formatSpec2,i-1);
    fprintf(fid,formatSpec3,i-1);
    fprintf(fid,formatSpec4,i-1);
    fprintf(fid,formatSpec5,i-1);
    fprintf(fid,formatSpec6,i-1,abs(h_TX_Gold_1s17(i))*0.75);
    fprintf(fid,formatSpec7,i-1,abs(h_TX_Gold_1s17(i))*0.25);
    fprintf(fid,formatSpec8,i-1,abs(h_TX_Gold_1s17(i))*0.25);
    fprintf(fid,formatSpec9,i-1,abs(h_TX_Gold_1s17(i))*0.75);
    fprintf(fid,formatSpec10,i-1);
    fprintf(fid,formatSpec11,i-1);
    fprintf(fid,formatSpec12,i-1);
end
fclose(fid);
open filt_coeffs_multiplierless_verilog_code_tx_gold.txt