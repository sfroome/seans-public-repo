clear all
%reference_level = 32775;

%mapper_out_power_1s17 = 176136674344960;
%mapper_out_power = 175964812738560; %Errorless decision variable.
%Real_Value = 10*log10(mapper_out_power)

reference_level = 32765;
mapper_out_power_1s17 = reference_level*reference_level*(1.25*2^17);

mapper_out_power_1s17 = 10231;
%accumulated_squared_error = 102;
accumulated_squared_error = 316;
MER = mapper_out_power_1s17/(accumulated_squared_error)
MER_dB_1 = 10*log10(MER);

MER_dB = 50;
%10*log10(9268*2^20/2^17)
%MER_dB1 = 20;
%round(sqrt(0.5/(10^(MER_dB1/10)))*2^(18-1))
%MER/10
ISI_Power = sqrt(.5/10^(MER_dB/10))*2^17
