% Requirements:
% full scale 1s17 sinusoidal input
% 1s17 sinusoidal output
% scaled such that maximum of the magnitude reponse is exactly 1.
%
%

clc
clear all
format long

beta = 0.25; % r is rolloff factor

%Normalized because you don't have a sampling frequency (or other given
%values.)
Ts = 4; % Samples per symbol
Rs = 1/Ts; 
Fc = Rs*(1/2);
Fs = Rs*Ts;
f = [0:1:500]/1000;

%For a length 17 Filter
M = 17;
N = M - 1;
h = firrcos(N, Fc, 0.25, Fs, 'rolloff', 'sqrt');
H = freqz(h, 1, 2*pi*f);

figure(1)
%plot(f,abs(H))
plot(f,20*log10(abs(H)))
grid 
ylabel('Magnitude (dB)')
xlabel('Frequency (Hz)')

figure(2)
%h_scaled = h./max(h); 
%h_scaled = h./sum(h);
%h_scaled = h*sum(h);
%h_scaled = h/sqrt(sum(abs(h).^2))'
h_scaled = h /max(abs(H)); % Derp. 
h_scaled_2 = h_scaled * 0.9999;

H_scaled = freqz(h_scaled, 1, 2*pi*f);
%plot(f,abs(H_scaled))
plot(f,20*log10(abs(H_scaled)))

grid 
ylabel('Magnitude (dB)')
xlabel('Frequency (Hz)')
max_val = max(abs(H_scaled)) %

h_2s17 = round(h_scaled*2^17)'

figure(3)
h_scaled_2 = h_scaled * 0.9999;
H_scaled_2 = freqz(h_scaled_2, 1, 2*pi*f);
plot(f,20*log10(abs(H_scaled_2)))
grid 
ylabel('Magnitude (dB)')
xlabel('Frequency (Hz)')

h_2s17_2 = round(h_scaled_2*2^17)'