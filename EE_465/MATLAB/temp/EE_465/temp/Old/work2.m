
beta_kaiser_RCV = 3;
N_srrc_RCV = 200;
beta_RCV = 0.12;

h_srrc_RCV=firrcos(N_srrc_RCV,f_6dB,beta_RCV,F_s,'rolloff','sqrt');
win_RCV  = kaiser( N_srrc_RCV+1, beta_kaiser_RCV)';
h_RCV_Gold = h_srrc_RCV.*win_RCV;
[H_RCV_Gold,w] = freqz(h_RCV_Gold,1,2*pi*f);

h_mer = conv(h_RCV_Gold, h_TX_Gold);
h_mer_normalized = h_mer/max(abs(h_mer));
h_mer_normalized_zeros = h_mer_normalized(1:4:end);

ISI = sum( (h_mer_normalized_zeros).^2) - 1;
MER = 1/ISI;
MER_dB = 10*log10(MER);
NEW_MER_dB = MER_dB;
