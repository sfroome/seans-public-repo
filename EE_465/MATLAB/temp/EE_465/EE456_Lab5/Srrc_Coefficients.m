clc
clear all

format long


Rs = 1.5625e6; % Symbol Rate.
Fc = (1/2)* Rs; % Fc is cutoff frequency
r = 0.25; % r is rolloff factor

Fs = 4 * Rs ; % Fs is sampling rate, which is 4 times symbol rate 
%(4 samples per symbol)

% 4 samples per symbol.
Ts = 1/Rs;


% For a length 9 filter
M = 9; % M is length. 
N = M-1; % N is order.
firrcos(N, Fc, r, Fs, 'rolloff', 'sqrt');
coefficients_N8 = ans;
coefficients_N8 = coefficients_N8/0.267077471545948; % for length 9 filter.

%coefficients_set_1_N8 = ;
coefficients_set_1_1s17_N8 = coefficients_N8/1.25*2^17

%coefficients_set_2_N8 = ;
coefficients_set_2_1s17_N8 = coefficients_N8/0.8*2^17

%For a length 17 Filter
M = 17;
N = M - 1;
firrcos(N, Fc, r, Fs, 'rolloff', 'sqrt');
coefficients_N16 = ans;
coefficients_N16 = coefficients_N16/0.267077471545948;

coefficients_set_1_1s17_N16 = (coefficients_N16/1.25)*2^17
coefficients_set_2_1s17_N16 = (coefficients_N16/0.8)*2^17

%For a length 25 Filter
M = 25;
N = M - 1;
firrcos(N, Fc, r, Fs, 'rolloff', 'sqrt');
coefficients_N24 = ans;
coefficients_N24 = coefficients_N24/0.267077471545948;

coefficients_set_1_1s17_N24 = (coefficients_N24/1.25)*2^17
coefficients_set_2_1s17_N24 = (coefficients_N24/0.8)*2^17

%For a length 29 Filter
M = 29;
N = M - 1;
firrcos(N, Fc, r, Fs, 'rolloff', 'sqrt');
coefficients_N28 = ans;
coefficients_N28 = coefficients_N28/0.267077471545948;

coefficients_set_1_1s17_N28 = (coefficients_N28/1.25)*2^17
coefficients_set_2_1s17_N28 = (coefficients_N28/0.8)*2^17
