% EE 465 Deliverable 3 Coefficient Hunting Code
% Written by: Sean Froome

clear all
close all
format long

N_sps = 4; % 4 Samples per Symbol
F_s = 1; % sampling rate in samples/second

best_MER_dB = 0; % Dummy value
Three_best_MER_dB = 0;

One_best_N_RCV = 201;
One_best_N_TX = 201;

Two_best_N_RCV = 201;
Two_best_N_TX = 201;


%df = 1/25000;
df = 1/6250;
f = 0:df:(1-df)*0.5;
n = 1:length(f);
n_max = length(n);
n_min = 1;

p1 = round((1.75/2)*1e3); % Right Edge of Passband
p2 = round((1.75/2 + 0.22)*1e3); % Right Edge of OB1
p3 = round((1.75/2 + 0.22 + 1.53)*1e3); % Right Edge of OB2
p4 = round((1.75/2 + 0.22 + 1.53 + 1.75)*1e3); % Right Edge of Adjacent Channel 2
f_6dB = .1250;
for N_srrc_TX = 130:-2:100
    %for beta_TX = 0.005:0.005:1 % This one is starting WAY too small.
    for beta_TX = 1:-0.01:0.01
        for beta_kaiser_TX = 5:-0.05:1
            %for f_6dB = 0.001:.001:.02
            for counter = 1:2 %
                
                h_srrc_TX = firrcos(N_srrc_TX,f_6dB,beta_TX,F_s,'rolloff','sqrt');
                win_TX  = kaiser( N_srrc_TX+1, beta_kaiser_TX)';
                h_TX_PRAC = h_srrc_TX.*win_TX;
                [H_TX_PRAC, n ] = freqz(h_TX_PRAC,1,2*pi*f);
                
                h_TX_PRAC = h_TX_PRAC/max(abs(H_TX_PRAC)); % Scaling
                
                Power_OB1 = sum(abs(H_TX_PRAC(p1:p2)).^2);
                Power_OB2 = sum(abs(H_TX_PRAC(p2+1:p3)).^2);
                Power_Ajc2 = sum(abs(H_TX_PRAC(p3+1:n_max)).^2);
                P_Signal = sum(abs(H_TX_PRAC(1:p1)).^2);
                
                Pdiff1 = (1/2)*Power_OB1/P_Signal;
                Pdiff1_dB = 10*log10(Pdiff1);
                
                if(Pdiff1_dB  > -58)
                    spec_not_met = 1;
                    break;
                end
                
                Pdiff2 = (1/2)*Power_OB2/P_Signal;
                Pdiff2_dB = 10*log10(Pdiff2);
                
                if(Pdiff2_dB  > -60)
                    spec_not_met = 1;
                    break;
                end
                
                Pdiff3 = (1/2)*Power_Ajc2/P_Signal;
                Pdiff3_dB = 10*log10(Pdiff3);
                
                if(Pdiff3_dB  > -63)
                    spec_not_met = 1;
                    break;
                end
                
                if(spec_not_met ~= 1)
                    for N_srrc_RCV = 150:-2:100
                        for beta_RCV = 0.2:-0.01:0.1 %0.1:0.1:1
                            for beta_kaiser_RCV = 5:-0.05:1
                                for counter2 = 1:2
                                    pow_spec_met_once = 1;
                                    
                                    h_srrc_RCV = firrcos(N_srrc_RCV,f_6dB,beta_RCV,F_s,'rolloff','sqrt');
                                    win_RCV  = kaiser( N_srrc_RCV+1, beta_kaiser_RCV)';
                                    h_RCV_Gold = h_srrc_RCV.*win_RCV;
                                    [H_RCV_Gold,w] = freqz(h_RCV_Gold,1,2*pi*f);
                                    
                                    h_RCV_Gold = h_RCV_Gold/max(abs(H_RCV_Gold));
                                    
                                    h_mer = conv(h_RCV_Gold, h_TX_PRAC);
                                    h_mer_normalized = h_mer/max(abs(h_mer));
                                    h_mer_normalized_zeros = h_mer_normalized(1:4:end);
                                    
                                    ISI = sum( (h_mer_normalized_zeros).^2) - 1;
                                    MER = 1/ISI;
                                    MER_dB = 10*log10(MER);
                                    NEW_MER_dB = MER_dB;
                                    
                                    if (MER_dB <= 40)
                                        spec_not_met = 1;
                                        break;
                                    end
                                    
                                    if(MER < 0)
                                        spec_not_met = 1;
                                    end
                                    
                                    if(spec_not_met ~= 1)
                                        
                                        mer_spec_met_once = 1;
                                        
                                        NEW_MER = MER;
                                        NEW_MER_dB = MER_dB;
                                        
                                        Three_NEW_MER = MER;
                                        Three_NEW_MER_dB = MER_dB;
                                        
                                        NEW_N_TX = N_srrc_TX;
                                        One_NEW_N_TX = N_srrc_TX;
                                        Two_NEW_N_TX = N_srrc_TX;
                                        
                                        NEW_beta_TX = beta_TX;
                                        NEW_beta_kaiser_TX = beta_kaiser_TX;
                                        
                                        NEW_N_RCV = N_srrc_RCV;
                                        One_NEW_N_RCV = N_srrc_RCV;
                                        Two_NEW_N_RCV = N_srrc_RCV;
                                        
                                        NEW_beta_RCV = beta_RCV;
                                        NEW_beta_RCV_kaiser =  beta_kaiser_RCV;
                                        
                                        NEW_f6dB = f_6dB;
                                        
                                        
                                        if(One_NEW_N_TX < One_best_N_TX)% || NEW_N_RCV < best_N_RCV)
                                            
                                            One_best_N_TX = N_srrc_TX
                                            One_best_beta_TX = beta_TX;
                                            One_best_beta_kaiser_TX = beta_kaiser_TX;
                                            
                                            One_best_MER = NEW_MER;
                                            One_best_MER_dB = NEW_MER_dB;
                                            One_best_ISI = ISI;
                                            
                                            One_best_N_RCV = N_srrc_RCV;
                                            One_best_beta_RCV = beta_RCV;
                                            One_best_beta_kaiser_RCV = beta_kaiser_RCV;
                                            
                                            %best_f6dB_length = f_6dB;
                                        end
                                        
                                        if( Two_NEW_N_RCV < Two_best_N_RCV)
                                            
                                            Two_best_N_TX = N_srrc_TX;
                                            Two_best_beta_TX = beta_TX;
                                            Two_best_beta_kaiser_TX = beta_kaiser_TX;
                                            
                                            Two_best_MER = NEW_MER;
                                            Two_best_MER_dB = NEW_MER_dB;
                                            Two_best_ISI = ISI;
                                            
                                            Two_best_N_RCV = N_srrc_RCV;
                                            Two_best_beta_RCV = beta_RCV;
                                            Two_best_beta_kaiser_RCV = beta_kaiser_RCV;
                                            
                                            %best_f6dB_length_R = f_6dB;
                                        end
                                        
                                        if Three_NEW_MER_dB > Three_best_MER_dB
                                            
                                            Three_best_MER_MER = Three_NEW_MER;
                                            Three_best_MER_dB_MER = Three_NEW_MER_dB;
                                            Three_best_ISI_MER = ISI;
                                            
                                            Three_best_beta_RCV_MER = beta_RCV;
                                            Three_best_beta_kaiser_RCV_MER = beta_kaiser_RCV;
                                            Three_best_N_RCV_MER = N_srrc_RCV;
                                            
                                            Three_best_beta_TX_MER = beta_TX;
                                            Three_best_beta_kaiser_Tx_MER = beta_kaiser_TX;
                                            Three_best_N_TX_MER = N_srrc_TX;
                                            
                                            %best_f6dB_MER = f_6dB;
                                        end
                                    end
                                    spec_not_met = 0;
                                end
                            end
                        end
                    end
                end
                spec_not_met = 0;
            end
        end
    end
end
%end

H_TX_dB = 20*log10(abs(H_TX_PRAC));
figure(1)
plot(n(1:p1), H_TX_dB(1:p1), 'r')
plot(n(p1+1:p2), H_TX_dB((p1+1):p2), 'g')
plot(n(p2+1:p3), H_TX_dB((p2+1):p3), 'b')
plot(n(p3+1:p4), H_TX_dB((p3+1):n_max), 'c')
%plot(n(p3+1:p4), H_TX_dB((p3+1):p4), 'c')
%plot(n(p4+1:n_max), H_TX_dB((p4+1):n_max), 'y')

H_RX_dB = 20*log10(abs(H_RCV_Gold));
figure(2)
plot(n(1:p1), H_RX_dB(1:p1), 'r')
plot(n(p1+1:p2), H_RX_dB((p1+1):p2), 'g')
plot(n(p2+1:p3), H_RX_dB((p2+1):p3), 'b')
plot(n(p3+1:p4), H_RX_dB((p3+1):n_max), 'c')
%plot(n(p3+1:p4), H_RX_dB((p3+1):p4), 'c')
%plot(n(p4+1:n_max), H_RX_dB((p4+1):n_max), 'y')
