hold all
grid on
    H_TX_dB = 20*log10(abs(H_TX_PRAC));
    figure(1)
    plot(n(1:p1), H_TX_dB(1:p1), 'r')
    plot(n(p1+1:p2), H_TX_dB((p1+1):p2), 'g')
    plot(n(p2+1:p3), H_TX_dB((p2+1):p3), 'b')
    %plot(n(p3+1:p4), H_RX_dB((p3+1):n_max), 'c')
    plot(n(p3+1:p4), H_TX_dB((p3+1):p4), 'c')
    plot(n(p4+1:n_max), H_TX_dB((p4+1):n_max), 'k')
    
    H_RX_dB = 20*log10(abs(H_RCV_Gold));
    figure(2)
    hold all
    plot(n(1:p1), H_RX_dB(1:p1), 'r')
    plot(n(p1+1:p2), H_RX_dB((p1+1):p2), 'g')
    plot(n(p2+1:p3), H_RX_dB((p2+1):p3), 'b')
    %plot(n(p3+1:p4), H_RX_dB((p3+1):n_max), 'c')
    plot(n(p3+1:p4), H_RX_dB((p3+1):p4), 'c')
    plot(n(p4+1:n_max), H_RX_dB((p4+1):n_max), 'k')