% EE 465 Deliverable 3
% Written by: Sean Froome 

clear all
close all
format long

N_srrc = 17; % length of the impulse response of the
% square root raised cosine filter
N_sps = 4;
F_s = 1; % sampling rate in samples/second
f_6dB = 1/(2*N_sps); % 6 dB down point in cycles/sample

df = 1/2000; % frequency increment in cycles/sample
f = [0:df:0.5-df/2]; % cycles/sample; 0 to almost 1/2

spec_not_met = 0; % Flag
best_MER_dB = 0; % Dummy value


beta_RCV = 0.12;


h_RCV=firrcos(N_srrc-1,f_6dB,beta_RCV,F_s,'rolloff','sqrt');
