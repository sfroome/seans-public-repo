% EE 465 Deliverable 3
% Written by: Sean Froome

clear all
close all
format long

% square root raised cosine filter
N_sps = 4;
F_s = 1; % sampling rate in samples/second
%f_6dB = 1/(2*N_sps); % 6 dB down point in cycles/sample
% Yeah this is impossible now haha (.1250... waaaay past the actual
% bandwidth hahaaha)

df = 1/2000; % frequency increment in cycles/sample
f = [0:df:0.5-df/2]; % cycles/sample; 0 to almost 1/2

spec_not_met = 0; % Flag
best_MER_dB = 0; % Dummy value
best_N_RCV = 200;
best_N_TX = 200;

%for N_srrc_TX = 2:2:100
%for N_srrc_RCV = 2:2:100
for N_srrc_TX = 176:2:200
    for N_srrc_RCV = 176:2:200
        for f_6dB = 0.01:.01:.03
            for beta_TX = 0.1:0.1:1
                for beta_RCV = 0.1:0.1:1
                    for beta_kaiser_TX = 3:0.1:5
                        for beta_kaiser_RCV = 3:0.1:5
                            h_srrc_TX = firrcos(N_srrc_TX,f_6dB,beta_TX,F_s,'rolloff','sqrt');
                            win_TX  = kaiser( N_srrc_TX+1, beta_kaiser_TX)';
                            h_TX_Gold = h_srrc_TX.*win_TX;
                            [H_TX_Gold,w] = freqz(h_TX_Gold,1,2*pi*f);
                            
                            h_srrc_RCV=firrcos(N_srrc_RCV,f_6dB,beta_RCV,F_s,'rolloff','sqrt');
                            win_RCV  = kaiser( N_srrc_RCV+1, beta_kaiser_RCV)';
                            h_RCV_Gold = h_srrc_RCV.*win_RCV;
                            [H_RCV_Gold,w] = freqz(h_RCV_Gold,1,2*pi*f);
                            
                            h_mer = conv(h_RCV_Gold, h_TX_Gold);
                            h_mer_normalized = h_mer/max(abs(h_mer));
                            h_mer_normalized_zeros = h_mer_normalized(1:4:end);
                            
                            ISI = sum( (h_mer_normalized_zeros).^2) - 1;
                            MER = 1/ISI;
                            MER_dB = 10*log10(MER);
                            NEW_MER_dB = MER_dB;
                            
                            for n = 1:length(w) % n is the index.
                                % Check that the filter actually meets spec.
                                if (10*log10(MER) < 50)
                                    % If filter does NOT meet spec
                                    spec_not_met = 1;
                                    break;
                                elseif(ISI < 0) % Because complex dB is stupid and pointless.
                                    spec_not_met = 1;
                                    break;
                                elseif(MER < 0) % Because complex dB is stupid and pointless.
                                    spec_not_met = 1;
                                    break;
                                elseif(MER_dB < 0)
                                    spec_not_met =1;
                                    break;                                
                                elseif((20*log10(abs(H_TX_Gold(n))) > -58) && (w(n)/(2*pi) >= 0.035)) % end of channel BW and start of OB1
                                    spec_not_met = 1;
                                    break;
                                    % elseif((20*log10(abs(H_RCV_Gold(n))) > -58) && (w(n)/(2*pi) >= 0.035))
                                    %     spec_not_met = 1;
                                    %     break;
                                elseif((20*log10(abs(H_TX_Gold(n))) > -60) && (w(n)/(2*pi) >= 0.0438)) %End of OB1 and start of OB2
                                    spec_not_met = 1;
                                    break;
                                    %  elseif((20*log10(abs(H_RCV_Gold(n))) > -60) && (w(n)/(2*pi) >= 0.0438))
                                    %     spec_not_met = 1;
                                    %     break;
                                elseif((20*log10(abs(H_TX_Gold(n))) > -63) && (w(n)/(2*pi) >= 0.105)) % End of OB2 and start of Band 2.
                                    spec_not_met = 1;
                                    break;
                                    % elseif((20*log10(abs(H_RCV_Gold(n))) > -63) && (w(n)/(2*pi) >= 0.105))
                                    %      spec_not_met = 1;
                                    %      break;
                                end
                            end
                            
                            if( spec_not_met ~= 1)
                                NEW_MER = MER;
                                NEW_MER_dB = MER_dB;
                                NEW_N_RCV = N_srrc_RCV;
                                NEW_N_TX = N_srrc_TX;
                                if(NEW_N_TX < best_N_TX && NEW_N_RCV < best_N_RCV)
                                    best_N_RCV = N_srrc_RCV;
                                    best_N_TX = N_srrc_TX;
                                    
                                    bbest_MER = NEW_MER;
                                    bbest_MER_dB = NEW_MER_dB;
                                    bbest_ISI = ISI;
                                    
                                    bbest_beta_RCV = beta_RCV;
                                    bbest_beta_kaiser_RCV = beta_kaiser_RCV;
                                    bbest_beta_TX = beta_TX;
                                    bbest_beta_kaiser_Tx = beta_kaiser_TX;
                                    
                                end
                                if NEW_MER_dB > best_MER_dB
                                    
                                    best_MER = NEW_MER;
                                    best_MER_dB = NEW_MER_dB;
                                    best_ISI = ISI;
                                    
                                    best_beta_RCV = beta_RCV;
                                    best_beta_kaiser_RCV = beta_kaiser_RCV;
                                    
                                    best_beta_TX = beta_TX;
                                    best_beta_kaiser_Tx = beta_kaiser_TX;
                                end
                            end
                            spec_not_met = 0;
                        end
                    end
                end
            end
        end
    end
end




