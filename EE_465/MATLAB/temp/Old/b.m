for n = 1:length(w) % n is the index.
    % Check that the filter actually meets spec.
    if (20*log10(abs(H_RCV_Gold(n))) > -50) % && (w(n)/(2*pi) >= 0.2000))
        % If filter does NOT meet spec
        spec_not_met = 1;
        break;
    end
end
if( spec_not_met ~= 1)    %NEED TO FIX THIS!
    N_RCV_new = N_srrc
    beta_RCV_new = beta_RCV;
    beta_kaiser_RCV_new = beta_kaiser_RCV;
    
    if NEW_MER_dB > best_MER_dB
        best_MER = MER;
        best_MER_dB = NEW_MER_dB;
        best_ISI = ISI;
        best_beta = beta;
        best_beta_kaiser = beta_kaiser;
    end
end
spec_not_met = 0;
end
end
end
