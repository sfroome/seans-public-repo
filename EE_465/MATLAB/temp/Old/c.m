






h_srrc=firrcos(N_srrc-1,f_6dB, best_beta,F_s,'rolloff','sqrt');
win  = kaiser( N_srrc, best_beta_kaiser)';
h_TX = h_srrc.*win;
[H_TX,w] = freqz(h_TX,1,2*pi*f);
[H_RCV,w] = freqz(h_RCV,1,2*pi*f);

worst_case_input_sequence = [1 -1 -1 -1 -1 1 1 1 1 1 1 1 -1 -1 -1 -1 1];
Worst_Case_Scaling_Factor_TX = h_TX*worst_case_input_sequence';
Worst_Case_Scaling_Factor_RCV = h_RCV*worst_case_input_sequence';

h_TX = h_TX/Worst_Case_Scaling_Factor_TX;
h_RCV = h_RCV/Worst_Case_Scaling_Factor_RCV;

h_mer = conv(h_RCV, h_TX);
h_mer_normalized = h_mer/max(abs(h_mer));
h_mer_normalized_zeros = h_mer_normalized(1:4:end);

ISI = sum( (h_mer_normalized_zeros).^2) - 1;
MER = 1/ISI;
MER_dB = 10*log10(MER);

best_MER;
best_MER_dB;
h_TX_1s17 = (round(h_TX*2^17))';
h_RCV_1s17 = (round(h_RCV*2^17))';

figure
plot(f,20*log10(abs(H_TX)))
ylabel('Magnitude Response of Transmit Filter (dB)')
xlabel('Frequency (cycles/sample)')
grid on
title('Transmit Filter')

figure
plot(f,20*log10(abs(H_RCV)))
grid on
ylabel('Magnitude Response of Received Filter (dB)')
xlabel('Frequency (cycles/sample)')
title('Received Filter')

fid = fopen('filt_coeffs_verilog_code_tx.txt','w');
for i=1:ceil(length(h_TX_1s17))
    if h_TX_1s17(i)>=0
        formatSpec = '      b[%3.0f] =  18''sd %6.0f;\n';
    else
        formatSpec = '      b[%3.0f] = -18''sd %6.0f;\n';
    end
    fprintf(fid,formatSpec,i-1,abs(h_TX_1s17(i)));
end
fclose(fid);
open filt_coeffs_verilog_code_tx.txt

fid = fopen('filt_coeffs_verilog_code_rcv.txt','w');
for i=1:ceil(length(h_RCV_1s17))
    if h_RCV_1s17(i)>=0
        formatSpec = '      a[%3.0f] =  18''sd %6.0f;\n';
    else
        formatSpec = '      a[%3.0f] = -18''sd %6.0f;\n';
    end
    fprintf(fid,formatSpec,i-1,abs(h_RCV_1s17(i)));
end
fclose(fid);
open filt_coeffs_verilog_code_rcv.txt


%  if (20*log10(abs(H_TX(n))) > -40) && (w(n)/(2*pi) >= 0.1950)
%if (20*log10(abs(H_TX(n))) > -40) && (w(n)/(2*pi) >= 0.1945)