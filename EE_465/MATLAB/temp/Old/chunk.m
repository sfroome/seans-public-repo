for n = 1:length(w) % n is the index.
    % Check that the filter actually meets spec.
    if (10*log10(MER) < 50)
        % If filter does NOT meet spec
        spec_not_met = 1;
        break;
    elseif(ISI < 0) % Because complex dB is stupid and pointless.
        spec_not_met = 1;
        break;
    elseif(MER < 0) % Because complex dB is stupid and pointless.
        spec_not_met = 1;
        break;
    elseif(MER_dB < 0)
        spec_not_met =1;
        break;
    elseif((20*log10(abs(H_TX_Gold(n))) > -58) && (w(n)/(2*pi) >= 0.035)) % end of channel BW and start of OB1
        spec_not_met = 1;
        break;
        % elseif((20*log10(abs(H_RCV_Gold(n))) > -58) && (w(n)/(2*pi) >= 0.035))
        %     spec_not_met = 1;
        %     break;
    elseif((20*log10(abs(H_TX_Gold(n))) > -60) && (w(n)/(2*pi) >= 0.0438)) %End of OB1 and start of OB2
        spec_not_met = 1;
        break;
        %  elseif((20*log10(abs(H_RCV_Gold(n))) > -60) && (w(n)/(2*pi) >= 0.0438))
        %     spec_not_met = 1;
        %     break;
    elseif((20*log10(abs(H_TX_Gold(n))) > -63) && (w(n)/(2*pi) >= 0.105)) % End of OB2 and start of Band 2.
        spec_not_met = 1;
        break;
        % elseif((20*log10(abs(H_RCV_Gold(n))) > -63) && (w(n)/(2*pi) >= 0.105))
        %      spec_not_met = 1;
        %      break;
    end
    