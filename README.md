
The following repo is a collection of code I've written for my classes in Electrical Engineering, and as of September 2018, my classes in Computer Science.
spanning back from Fall 2012 to April 2018, as well as some Python code I've written. The python code is largely
just fragments I've found to teach myself the basics. Everything else is a mix of projects to code typed up in class as part of examples, to just using MATLAB to write my lab notes and act as a calculator.

With the exception of my "Text_Based_Angry_Birds" and "Python" folders, all of my code is sorted by the Class.

ENGR_1233 - C++ for Engineers is the majority of my C++
CME_331 is mostly C Code, used for interfacing with a Texas Instruments ARM Microcontroller.
CME_341, EE_456, EE_461, EE_365 and EE_465 are all DSP Classes, with the majority of the code being Verilog code, with some MATLAB code as well.
