
// Iniatializations
//#include "ST7735.h"
//#include "Systick.h"


//TIMERS, descriptions given from assignment hand out.

#define SYSCTL_RCGCTIMER 				(*((volatile unsigned long *) 0x400FE604)) // To enable clock for GPTM (General Purpose Timer Module
#define TIMER0_CFG 							(*((volatile unsigned long *) 0x40030000)) // Configured Timer0 in 16/32 or 32/64 bit mode
#define TIMER0_CTL							(*((volatile unsigned long *) 0x4003000C)) // "Further Configure" Timer0
#define TIMER0_TB_MR 						(*((volatile unsigned long *) 0x40030008)) // Control Operation of TimerB of Timer0
#define TIMER0_TB_PR 						(*((volatile unsigned long *) 0x4003003C)) // Sets TimerB Prescale
#define TIMER0_TB_ILR						(*((volatile unsigned long *) 0x4003002C)) // GPTM Timer B Interval Load
#define TIMER0_TB_MATCHR 				(*((volatile unsigned long *) 0x40030034)) // Match value of Timer B of Timer0 (width of PWM)


// ADC Initialization
#define SYSCTL_RCGCADC 					(*((volatile unsigned long *) 0x400FE638)) // To enable clock for ADC module
#define SYSCTL_RCGCGPIO         (*((volatile unsigned long *)0x400FE608)) // (**not used**)

#define ADC0_SSPRI 		(*((volatile unsigned long *) 0x40038020)) // to select SS priority  (sample sequencers)
#define ADC0_PC 			(*((volatile unsigned long *) 0x40038FC4)) // to select sampling rate
#define ADC0_ACTSS 		(*((volatile unsigned long *) 0x40038000)) // to enable/disable sample sequencer
#define ADC0_EMUX 		(*((volatile unsigned long *) 0x40038014)) // to set trigger mode
#define ADC0_SSMUX3 	(*((volatile unsigned long *) 0x400380A0)) //  tp select ADC Channel
#define ADC0_SSCTL3 	(*((volatile unsigned long *) 0x400380A4)) // to configure sequencer control

#define ADC0_PSSI 		(*((volatile unsigned long *) 0x40038028)) // ??
#define ADC0_SSFIFO3 	(*((volatile unsigned long *) 0x400380A8)) // ??

#define ADC0_RIS 			(*((volatile unsigned long *) 0x40038004)) // Raw Interrupt Status
#define ADC0_ISC 			(*((volatile unsigned long *) 0x4003800C)) // Clear Flag



// GPIO Registers
#define SYSCTL_RCGC2  (*((volatile unsigned long *)0x400FE108)) // Clock Gating
// Port E
#define GPIO_PORTE_DATA 	(*((volatile unsigned long *)0x400243FC))
#define GPIO_PORTE_DEN 		(*((volatile unsigned long *)0x4002451C)) 
#define GPIO_PORTE_AFSEL 	(*((volatile unsigned long *)0x40024420))
#define GPIO_PORTE_DIR 		(*((volatile unsigned long *)0x40024400))
#define GPIO_PORTE_AMSEL 	(*((volatile unsigned long *)0x40024528))

//Port F
#define GPIO_PORTF_DATA        	(*((volatile unsigned long *)0x400253FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTF_DIR        	(*((volatile unsigned long *)0x40025400)) // p.616, set data flow direction
#define GPIO_PORTF_AFSEL      	(*((volatile unsigned long *)0x40025420)) // p.625, enable alternate functions
#define GPIO_PORTF_DEN        	(*((volatile unsigned long *)0x4002551C)) // p.636, make port digital
#define GPIO_PORTF_PCTL       	(*((volatile unsigned long *)0x4002552C)) // p.641, port control (for alt. func. only)

void init_gpio(void);
void init_adc(void);
void init_timer(void);
void pot_control(void);


int main(void)
	{	
		init_gpio();
		init_adc();
		init_timer();		
		while(1)
			{
				pot_control();	
			}
	}
	
void init_gpio(void)
	{
		volatile unsigned long delay_clk;
		SYSCTL_RCGC2 |= 0x00000030; // Ports E and Port F
		delay_clk = SYSCTL_RCGC2; 		
		
		GPIO_PORTE_DIR |= 0x0F;
		GPIO_PORTE_AFSEL |= 0x10; // PORT E AFSEL is needed for PE4
		GPIO_PORTE_DEN &= ~0x10; // PE4 doesn't need DEN, but PE0-PE3 do
		GPIO_PORTE_AMSEL |= 0x10; // Analog mode select on PE4
		
		GPIO_PORTF_DEN |= 0x02; // PF1
		GPIO_PORTF_DIR |= 0x02; // Output PF1
		GPIO_PORTF_AFSEL |= 0x02; // Enable AFSEL on PF1
		GPIO_PORTF_PCTL = (GPIO_PORTF_PCTL & 0xFFFFFF0F) + 0x00000070;
	}
	
void init_adc(void) // Part One ADC Initialization Function.
	{
		volatile unsigned long delay_clk_adc;
		SYSCTL_RCGCADC = 0x00000001;
		delay_clk_adc = SYSCTL_RCGCADC;
		delay_clk_adc = SYSCTL_RCGCADC;
		
		ADC0_ACTSS &= ~0x8;
		ADC0_SSPRI |= 0x00003;
		ADC0_PC = 0x1;	
		ADC0_EMUX |= 0x0;
		ADC0_SSMUX3 |= 0x9;	
		ADC0_SSCTL3 |= 0x6;	
		ADC0_ACTSS |= 0x8;
	}
	
void init_timer(void)
	{
		volatile unsigned long delay_clk_timer;
		SYSCTL_RCGCTIMER = 0x00000001;		
		
		TIMER0_CTL &= ~0x100; // Disable Timer B first (bit 8)
		
		TIMER0_CFG |= 0x4; //Selects 16bit timer
		
		TIMER0_TB_MR |= 0x8; //TBAMS
		TIMER0_TB_MR &= ~0x4; //TBCMR
		TIMER0_TB_MR |= 0x2; //TBMR (Periodic Mode)
		
		TIMER0_CTL |= 0x4000; // Inverts PWM signal (TBPWML)
		
		TIMER0_TB_PR |= 0x00; //Default prescale value of 0.
		
		//TIMER0_TB_ILR |=  0xFFF; // Period of PWM aka max ADC (ie: FFF 12 bits)
		
		TIMER0_TB_MATCHR |=  0x7FF; // width of PWM. using 50% duty.
				
		TIMER0_CTL |= 0x100;
	}
		
		
void pot_control(void) // Code for Part One of the Lab
	{	
		ADC0_PSSI |= 0x0008;	
		while((ADC0_RIS & 0x0008) == 0);
		TIMER0_TB_MATCHR = ADC0_SSFIFO3;
		ADC0_ISC &= ~0x8;
	}


