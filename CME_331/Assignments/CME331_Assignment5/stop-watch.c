/********************
REMEMBER: MUCH OF THIS CODE IS DEMO CODE 
AND NEEDS TO BE MODIFIED TO WORK PROPERLY

***************/
#include "ST7735.h"		// Include this header file to use functions related to ST7735 display 
//#include "Systick.h"  // Include this header file to initialize Systick timer (initiate inside the code)

// p.424, clock gating for GPIO
#define SYSCTL_RCGCGPIO         (*((volatile unsigned long *)0x400FE608)) // (**not used**)
#define SYSCTL_RCGC2            (*((volatile unsigned long *)0x400FE108)) // for legacy support 

// GPIO initialization p.610



#define GPIO_PORTF_DATA        	(*((volatile unsigned long *)0x400253FC)) // p.615, bit masking, p.608
#define GPIO_PORTF_DIR        	(*((volatile unsigned long *)0x40025400)) // p.616
#define GPIO_PORTF_AFSEL      	(*((volatile unsigned long *)0x40025420)) // p.625
#define GPIO_PORTF_DEN        	(*((volatile unsigned long *)0x4002551C)) // p.636
//#define GPIO_PORTF_PCTL       	(*((volatile unsigned long *)0x4002552C)) // p.641, port control (for digital only)
#define GPIO_PORTF_PUR        	(*((volatile unsigned long *)0x40025510)) // p.631, need weak-pull-up for SW1, SW2

// General NVIC for all
#define NVIC_EN0	              (*((volatile unsigned long *)0xE000E100)) // p.137, IRQ 0 to 31 Set Enable Register
#define NVIC_PRI7		            (*((volatile unsigned long *)0xE000E41C)) // p.147, IRQ 28 to 31 Priority

// INT regsiters for GPIO PortF only
#define GPIO_PORTF_IS	         (*((volatile unsigned long *)0x40025404)) // p.617, int sense, 0 = edge, 1 = level
#define GPIO_PORTF_IBE	        (*((volatile unsigned long *)0x40025408)) // p.618, both edges
#define GPIO_PORTF_IEV	        (*((volatile unsigned long *)0x4002540C)) // p.619, int event, 0 = falling, 1 = rising

#define GPIO_PORTF_IM	          (*((volatile unsigned long *)0x40025410)) // p.620, int mask
#define GPIO_PORTF_RIS        	(*((volatile unsigned long *)0x40025414)) // p.621, raw int status, not used here
#define GPIO_PORTF_MIS        	(*((volatile unsigned long *)0x40025418)) // p.622, masked int status, used here
#define GPIO_PORTF_ICR	        (*((volatile unsigned long *)0x4002541C)) // p.623, int flag clear, **must acknowledge**

// Nested Vectored Interrupt Controller (NVIC)
// SysTick info p.118
#define NVIC_ST_CTRL      	    (*((volatile unsigned long *)0xE000E010)) // p.133, SysTick control and status
#define NVIC_ST_RELOAD  	      (*((volatile unsigned long *)0xE000E014)) // p.135, 24-bit start value of SysTick
#define NVIC_ST_CURRENT	    	  (*((volatile unsigned long *)0xE000E018)) // p.136, 24-bit current value of SysTick
#define NVIC_SYS_PRI3         	(*((volatile unsigned long *)0xE000ED20)) // p.167, System Handler Priority 3

void init_SysTick (void);
void init_gpio(void);
void init_portf_int (void);
void delay(void);
void flash_led (void);
unsigned char hex2char(unsigned int hex);

unsigned min, sec, key_flag = 0;
unsigned  time[5];

int main(void)
	{
		ST7735_InitR(INITR_BLACKTAB);	
		ST7735_FillScreen(ST7735_BLACK);  // set screen to black 
		ST7735_SetRotation(3);						// rotates display screen
		init_SysTick();
		init_gpio();
		init_portf_int ();
	//	init_SysTick();
				
		while(1)
			{
				time[0] = hex2char((int)(min / 10) % 10 );
				time[1] = hex2char((int)(min) % 10 );
				time[2] = ':';
				time[3] = hex2char((int)(sec / 10) % 10 );
				time[4] = hex2char((int)(sec) % 10 );
				
				if(key_flag == 1)
					{
						ST7735_DrawChar(90, 10, 'P', ST7735_BLACK, 0, 5);
						ST7735_DrawChar(0, 10, 'R', ST7735_GREEN, 0, 5); 
						ST7735_DrawChar(30, 10, 'U', ST7735_GREEN, 0, 5);  
						ST7735_DrawChar(60, 10, 'N', ST7735_GREEN, 0, 5);
						ST7735_DrawChar(0, 50, time[0], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...
						ST7735_DrawChar(24, 50, time[1], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...
						ST7735_DrawChar(48, 50, time[2], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...
						ST7735_DrawChar(72, 50, time[3], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...
						ST7735_DrawChar(96, 50, time[4], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...			
					}
					
				if (key_flag == 0)
					{
						ST7735_DrawChar(0, 10, 'S', ST7735_RED, 0, 5);
						ST7735_DrawChar(30, 10, 'T', ST7735_RED, 0, 5);
						ST7735_DrawChar(60, 10, 'O', ST7735_RED, 0, 5);
						ST7735_DrawChar(90, 10, 'P', ST7735_RED, 0, 5);
						ST7735_DrawChar(0, 50, time[0], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...
						ST7735_DrawChar(24, 50, time[1], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...
						ST7735_DrawChar(48, 50, time[2], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...						
						ST7735_DrawChar(72, 50, time[3], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...
						ST7735_DrawChar(96, 50, time[4], ST7735_BLUE, 0, 2); // May need to do integer division to ensure this displays correctly...
					}

			}
	
	}
	
	unsigned char hex2char(unsigned int hex) 
{
	if(hex >=0x00 && hex <=0x09)
	{
		return (hex+0x30);
	}
	// else if ( hex >=0x0A && hex <=0x0F)
	// {
		// return (hex+0x37);
	// }
	else
	{
		return 0xFF;
	}
}
	
void delay (void)
	{
		long unsigned i;
		for (i = 0; i < 999999; i++);
	
	}
void init_gpio(void)
	{
			volatile unsigned long delay_clk; // delay for clock, must have 3 sys clock delay, p.424
			SYSCTL_RCGC2 |= 0x00000020; // enable clock gating for port F, p.424	
		
			GPIO_PORTF_DIR |= 0x0E; // PF4 input, PF0 input, PF1-PF3 output
			GPIO_PORTF_AFSEL &= ~0x1F;// disable alt funct on PF0-PF4
			GPIO_PORTF_DEN |= 0x1F; // enable digital I/O on PF0-PF4
			//GPIO_PORTF_PCTL &= 0xFFF00000;
			GPIO_PORTF_PUR |= 0x11; // enable pull-up on PF4 and PF0		

	}
	
	
void init_SysTick (void)
	{
		NVIC_ST_CTRL = 0; // p.133, turn off SysTick during init
		NVIC_ST_RELOAD = 16000000-1; // p.135, clock is 16MHz
		NVIC_ST_CURRENT = 0; // any write to it clears it
		NVIC_SYS_PRI3 = (NVIC_SYS_PRI3 & 0x00FFFFFF) | 0x40000000; // p.167, set priority 2 (bits 31:29)
		NVIC_ST_CTRL = 0x07; // enable SysTick with system clock and interrupt  (aka clk src, inten, and enable are all high)
	}

	
void init_portf_int (void)
	{
			GPIO_PORTF_IM &= ~0x10 ; // clear, disable (mask) int when config, p.609
			GPIO_PORTF_IS &= ~0x10; // edge sense for PF4
			GPIO_PORTF_IBE &= ~0x10; // not both edges for PF4
			GPIO_PORTF_IEV &= ~0x10; // falling edge event for PF4 
			GPIO_PORTF_ICR = 0x10; // clear int flag
			GPIO_PORTF_IM |= 0x10; // arm interrupt on PF4 after configured
		
			// we want INT30, i.e., 4n+2, 4*7+2, bits 23:21, we set priority level to say '5' ==> 101 => 1010 = A
			NVIC_PRI7 = (NVIC_PRI7 & 0xFF0FFFFF)|0x00A00000; // priority 5, lower means higher priority
			NVIC_EN0 = 0x40000000;  // finally enable Interrupt # 30 (i.e., IRQ30 ==> bit 30 of NVIC_EN0), GPIO PortF
	}
	
	// **************ISR for SysTick*********************
// Initialize SysTick periodic interrupts, (see startup.s and startup.lst)
// Input: none
// Output: none
// Executes every time SysTick count is 0
void SysTick_Handler(void)
{
	// see value of i at the entry point in "Watch 1" window
	//i = 100; // when Interrupt triggers, the soft returns with this value
	//i = 101;
	//NVIC_ST_CTRL &= ~0x01; // turn off SysTick, keep rest unchanged
	//NVIC_ST_CTRL |= 0x01; // turn on SysTick so that it (CURRENT) reloads with RELOAD
		//NVIC_ST_CURRENT = 0x30; // clears both COUNT flag and CURRENT (no matter what you write)
	// see what happens if you do not clear COUNT flag???
	
	if(key_flag == 1)
		{
			sec = sec + 1;
		}
	
	if(sec == 60)
		{
			min = min + 1,
			sec = 0;
		}
	
	if(min == 60)
		{
			min = 0;
		}
}

void GPIOPortF_Handler (void)
{
	delay();
	if(key_flag == 0)
		key_flag = 1;
	else
		key_flag = 0;

	GPIO_PORTF_ICR = 0x10; // ***must clear flag for PF4***
	// see what happens if do not clear it
	// ***sometimes, you may see two LEDs on when running ISR, why?????***
}

void flash_led (void) {
	// flashing RGB
	GPIO_PORTF_DATA |= 0x02; // make PF1 high
	delay ();
	GPIO_PORTF_DATA &= ~0x02; // make PF1 low
	delay ();
	
	GPIO_PORTF_DATA |= 0x04; // make PF2 high
	delay ();
	GPIO_PORTF_DATA &= ~0x04; // make PF2 low
	delay ();
	
	GPIO_PORTF_DATA |= 0x08; // make PF3 high
	delay ();
	GPIO_PORTF_DATA &= ~0x08; // make PF3 low
	delay ();	
}
