/**********************************************************************
Assignment 1 Question 4
CME 331 
Sean Froome
11178121
September 20, 2015
**********************************************************************/
//Clock Gating
#define SYSCTL_RCGC2          (*((volatile unsigned long *)0x400FE108)) 

//GPIO Initialization
#define GPIO_PORTA_DATA 				(*((volatile unsigned long *)0x400043C0)) //Enable Data Bits
#define GPIO_PORTA_DIR        	(*((volatile unsigned long *)0x40004400)) // p.616, set data flow direction
#define GPIO_PORTA_AFSEL      	(*((volatile unsigned long *)0x40004420)) // p.625, enable alternate functions
#define GPIO_PORTA_DEN        	(*((volatile unsigned long *)0x4000451C)) // p.636, make port digital
#define GPIO_PORTA_PCTL       	(*((volatile unsigned long *)0x4000452C)) // p.641, port control (for alt. func. only)

#define GPIO_PORTF_DATA        	(*((volatile unsigned long *)0x40025060)) // p.615, data port, bit masking, p.608
#define GPIO_PORTF_PUR        	(*((volatile unsigned long *)0x40025510)) // p.631, enable weak-pull-up, needed for SW1 and SW2
#define GPIO_PORTF_DIR 					(*((volatile unsigned long *)0x40025400)) // p.616, set data flow direction
#define GPIO_PORTF_AFSEL      	(*((volatile unsigned long *)0x40025420)) // p.625, enable alternate functions
#define GPIO_PORTF_DEN        	(*((volatile unsigned long *)0x4002551C)) // p.636, make port digital

void init_gpio (void);
void wait_for_sw1(void);
void delay(void);

void init_gpio (void) 
	{
		volatile unsigned long delay_clk; // delay for clock, must have 3 sys clock delay, p.424
		SYSCTL_RCGC2 |= 0x00000021; // enable clock gating for port F, p.424
		delay_clk = SYSCTL_RCGC2; // this is a dummy instruction to allow clock to settle, no-operation
	
		// Initializing Control Registers for Port F and Port A	
		GPIO_PORTA_DIR |= 0xF0; // Bits 7 through 4 are enabled
		GPIO_PORTA_DEN |= 0xF0; // Digital enable on Pins A4 through A7
		GPIO_PORTA_PCTL |=0xF0; // Enabling Port Control on Pins PA4-PA7
		GPIO_PORTA_AFSEL &= ~0xF0; // Disabling Alternate Functions on PA4-PA7
		
		GPIO_PORTF_DIR &= ~0x10; // PF1-PF3 is output (ports set to 1), PF4 is input (set to 0)
		GPIO_PORTF_AFSEL &= ~0x10; // Disabling alternate functions on PF1-PF4
		GPIO_PORTF_DEN |= 0x10; // Enabling digital functionality on PF1-PF4
		GPIO_PORTF_PUR |= 0x10; // enabling pull-up on PF4
	}
		
int main(void)
	{
		init_gpio();
		GPIO_PORTA_DATA &= ~0xF0;	
		//Ensuring PA4-PA7 are initially low
		// They probably should be set low anyways
		// This ensures it.
		while(1) // Infinite loop to keep the MicroController busy
			{
				wait_for_sw1();
			}
	}	
	

	
void delay (void) 
	{
		long unsigned i; // A very approximate time delay
		for (i = 0; i < 1000000; i++);
	}

	
void wait_for_sw1(void) //Program will wait for switch press
	{ 
		delay();
		while( (GPIO_PORTF_DATA & 0x10) == 0)
		{
			delay(); // This is so that it only takes
			// One press for one bit instead of multiple increments
			GPIO_PORTA_DATA += 0x10; 
			// Because adding 00010000 
			//each time is easier than nested for loops
			// Or nested if statements.
		}
	}	
