/****************************************************************************
CME 331
Assignment 2, Quesiton 2
October 4, 1015
Sean Froome
NOTE: Assignment question says PC3, but since PC3 isn't actually accessible
I will use PC4 instead both to test, and to use in my code.
Also, I used the ADM's Static I/O to test.
****************************************************************************/


// Clock Gating
#define SYSCTL_RCGC2 		(*((volatile unsigned long *) 0x400FE108))

//GPIO Initialization

// Port A
#define GPIO_PORTA_DATA 	(*((volatile unsigned long *)0x40004100)) // PA6 only
#define GPIO_PORTA_DIR		(*((volatile unsigned long *)0x40004400))
#define GPIO_PORTA_AFSEL	(*((volatile unsigned long *)0x40004420))
#define GPIO_PORTA_DEN		(*((volatile unsigned long *)0x4000451C))

//Port C
#define GPIO_PORTC_DATA		(*((volatile unsigned long *)0x40006040)) // PC4 only
#define GPIO_PORTC_DIR		(*((volatile unsigned long *)0x40006400))
#define GPIO_PORTC_AFSEL	(*((volatile unsigned long *)0x40006420))
#define GPIO_PORTC_DEN		(*((volatile unsigned long *)0x4000651C))

// Function Declarations
void init_gpio (void);
int main (void);
void delay(void);
void wait_for_switch(void);


void init_gpio(void)
	{
		volatile unsigned long delay_clk;
		SYSCTL_RCGC2 |= 0x00000005; // Port A = 1, Port C = 4.
		delay_clk = SYSCTL_RCGC2;
	
		GPIO_PORTA_DIR &= ~0x40; // PA6 is input therefore negative
		GPIO_PORTA_DEN |= 0x40;
		GPIO_PORTA_AFSEL &= ~0x40;
	
		GPIO_PORTC_DIR |= 0x10; // PC4
		GPIO_PORTC_DEN |= 0x10;
		GPIO_PORTC_AFSEL &= ~0x10;
	}

int main(void)
	{
		init_gpio();
		while(1)
			{
				wait_for_switch();
			}	
	}

void wait_for_switch(void)
	{
		while((GPIO_PORTA_DATA & 0x40)) //
		{
			GPIO_PORTC_DATA |= 0x10;
			delay();
			GPIO_PORTC_DATA &= ~0x10;
			delay();
		}	
	}

	
void delay (void) 
	{
		long unsigned i;
		for (i = 0; i < 2000000; i++);
	}
	