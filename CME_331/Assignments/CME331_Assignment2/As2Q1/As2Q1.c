/***********************************************
CME 331
Assignment 2, Question 1
Sean Froome
October 4, 2015
************************************************/

// Clock Gating
#define SYSCTL_RCGC2 		(*((volatile unsigned long *) 0x400FE108))

//GPIO Initialization
// Port A
//#define GPIO_PORTA_DATA 	(*((volatile unsigned long *)0x400043FC))
// Only the required bits of Port A are defined here.
#define GPIO_PORTA_DATA 	(*((volatile unsigned long *)0x400041C0))
#define GPIO_PORTA_DIR		(*((volatile unsigned long *)0x40004400))
#define GPIO_PORTA_AFSEL	(*((volatile unsigned long *)0x40004420))
#define GPIO_PORTA_DEN		(*((volatile unsigned long *)0x4000451C))

//Port F
#define GPIO_PORTF_DATA		(*((volatile unsigned long *)0x400253FC))
#define GPIO_PORTF_DIR		(*((volatile unsigned long *)0x40025400))
#define GPIO_PORTF_AFSEL	(*((volatile unsigned long *)0x40025420))
#define GPIO_PORTF_DEN		(*((volatile unsigned long *)0x4002551C))

// Function Declarations
void init_gpio (void);
int main (void);
void checker(void);
void delay(void);
void flash_red(void);

// Need PA4,PA5,PA6 for input and PF1 (LED) for output
// Initialize GPIO pins
void init_gpio(void)
	{
		volatile unsigned long delay_clk;
		SYSCTL_RCGC2 |= 0x00000021; // Port F is the sixth bit. A is the first bit
		delay_clk = SYSCTL_RCGC2;
	
		GPIO_PORTA_DIR &= ~0x70; // Negative because Port A is input
		GPIO_PORTA_DEN |= 0x70;
		GPIO_PORTA_AFSEL &= ~0x70;
	
		GPIO_PORTF_DIR |= 0x02; // Positive because it's output; PF1 = bit 2
		GPIO_PORTF_DEN |= 0x02;
		GPIO_PORTF_AFSEL &= ~0x02;
	}

int main(void)
	{
		init_gpio();
		flash_red();
		while(1)
		{
			checker();
		}
	}
	
void delay (void) 
	{
		long unsigned i;
		for (i = 0; i < 1000000; i++);
	}
	
void checker(void) // Use Logic Analyzer to test this statement
	{				// Remember to disable unused bits on port A at the top or else this won't work
		if((GPIO_PORTA_DATA & 0x20) && (GPIO_PORTA_DATA & 0x10) )
			{
				flash_red();
			}
		else if ((GPIO_PORTA_DATA & 0x40) && (GPIO_PORTA_DATA & 0x10) )
			{
				flash_red();
			}		
		else if ((GPIO_PORTA_DATA & 0x40) && (GPIO_PORTA_DATA & 0x20) ) 
			{
				flash_red();
			}		
		else if	((GPIO_PORTA_DATA & 0x40) && (GPIO_PORTA_DATA & 0x20) && (GPIO_PORTA_DATA & 0x10) )	
			{
				flash_red();
			}	
		else
			GPIO_PORTF_DATA &= ~0x02; // Simply keeps the Red LED off, since I needed 
									  // to put something in here.
	}


void flash_red(void)
	{
		GPIO_PORTF_DATA |= 0x02; 	//Turns on Red LED
		delay(); 					// Wouldn't see LED turn on without this delay 
									//because the action would be near instantaneous, and impossible to see.
		GPIO_PORTF_DATA &= ~0x02;   // Turns off Red LED, so it actually flashes instead of remaining on.
		
	}
