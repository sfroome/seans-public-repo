/***********************************************************************************
CME 331
Assignment 2, Question 3
Sean Froome 
October 4, 2015

Modified from PLL_test_setup.c
Sets Clock to 20MHz instead of 50MHz

For the last part of this question, since I couldn't
figure out how to precisely measure the clock, I verified
the wavelenths of each signal on Pin PA6, and compared the relative 
lengths at each clock frequency.
All wavelengths are approximate.
@ 1.6MHz, the wave length was 4.35 4us
@ 20MHz it was about 350ns.
@ 16MHz it was about 860ns.
@ 50MHz it was about 130ns.
They're not at all proportional to eachother, (ie: 50MHz isn't 2.5 
times smaller than 20MHz), but they're still larger and smaller, relative to 
eachother. In other words, the bigger the frequency,
the smaller the wavelength/period, which makes sense.
Unfortunately there are other variables at play
that affect the length of each wave at the particular clockspeed.
**************************************************************************************/

// Registers Needed for PLL
#define SYSCTL_RIS	            (*((volatile unsigned long *)0x400FE050)) // p.235
#define SYSCTL_RCC 	           	(*((volatile unsigned long *)0x400FE060)) // p.243
#define SYSCTL_RCC2          		(*((volatile unsigned long *)0x400FE070)) // p.249

// GPIO Clock Gating
#define SYSCTL_RCGC2          	(*((volatile unsigned long *)0x400FE108)) 

// GPIO Initialization

//Port A
#define GPIO_PORTA_DATA        	(*((volatile unsigned long *)0x400043FC)) 
#define GPIO_PORTA_DIR        	(*((volatile unsigned long *)0x40004400))
#define GPIO_PORTA_AFSEL      	(*((volatile unsigned long *)0x40004420))
#define GPIO_PORTA_DEN        	(*((volatile unsigned long *)0x4000451C))
//#define GPIO_PORTA_PCTL       	(*((volatile unsigned long *)0x4000452C))


//declare functions
void init_pll (void);
void init_gpio (void);
void check_clock(void);
int main(void);


// configure the system to get its clock from the PLL
// PLL init, p.224


int main(void)
	{  
		init_pll(); // initializes PLL
		init_gpio(); //initializes GPIO
	
		while(1)
			{
				check_clock();
			}
	}
	
void init_pll(void)
	{
		// Most of this code was in the template from the notes
		// 1) bypass PLL and system clock divider while initializing
		SYSCTL_RCC |= 0x00000800; // BYPASS = 1 
		// While this doesn't activate Bypass, Bypass will be set to zero at the
		// end of the function.
		SYSCTL_RCC &= ~0x00400000; // clear USESYSDIV	
		// Ensures there aren't any unwanted bits set 
		// In USESYSDIV
		// 2) select the crystal value, oscillator source and power down (enable) PLL
		SYSCTL_RCC &= ~0x000007C0;   // clear XTAL field
		SYSCTL_RCC += 0x00000540; // configure for 16 MHz crystal (XTAL = 0x15, p.245)
		// Ensures you're using the 16MHz crystal
			
		SYSCTL_RCC &= ~0x00000030; // clear oscillator source field
		SYSCTL_RCC += 0x00000000;// configure for main oscillator source (default setting)
		// Aka the Main Oscillator source uses an external, single ended clock source aaka PLL.
		
		SYSCTL_RCC &= ~0x00002000; // turn on PLL
		// Self Explanatory
		
		
		// 3) set the desired system divider (SYSDIV) and the USESYSDIV bit
		SYSCTL_RCC &= ~0x07800000; // clear SYSDIV field
		//SYSCTL_RCC += 0x01800000;  // configure at 50 MHz clock
		SYSCTL_RCC += 0x04800000; // Should set it to 20Mhz clock

		
		SYSCTL_RCC |= 0x00400000; // set USESYSDIV
		// Shouls be pretty obvious. SYSDIV is set, USESYSDIV allows the 
		//SYSDIV value to be used.
		
		// 4a) wait for the PLL to lock by polling PLLLRIS, p.221
		// below is a good technique to implement busy-wait loop
		while((SYSCTL_RIS & 0x00000040)==0){}; // SYSCTL_RIS on p.235 (bit 6)

		// 4b) NOW finally connect PLL to main system by clearing BYPASS
		SYSCTL_RCC &= ~0x00000800;
		// Disabling BYPASS will mean that the clock will be set to the new Clock Speed in PLL.
	}

void init_gpio(void) // Will use PA6 to test the clockspeed
	{
		volatile unsigned long delay_clk;
		SYSCTL_RCGC2 |= 0x00000001; //Port A = 1
		delay_clk = SYSCTL_RCGC2;
		GPIO_PORTA_DIR |= 0x40;
		GPIO_PORTA_DEN |= 0x40;
		GPIO_PORTA_AFSEL &= ~0x40;	
	}

void check_clock()
	{
		// Use the ADM to check how fast the value on PA6 changes
		// Or don't.... because it doesn't measure it correctly.
		GPIO_PORTA_DATA |= 0x40;
		GPIO_PORTA_DATA &= ~0x40;
	}
