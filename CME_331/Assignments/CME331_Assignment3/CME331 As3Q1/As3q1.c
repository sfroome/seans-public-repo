/*
CME 331 Assignment 3, Question 1
Sean Froome
October 12, 2015
Uses SysTick_16.h and SysTick_16.c
This code generates a pwm on four output pins.
Each wave goes high for 1ms, and only one is high at a time.
Each one goes high in sequence.
*/
#include "SysTick_16.h"

//Clock gating

#define SYSCTL_RCGC2 (*((volatile unsigned long *) 0x400FE108))


//GPIO Initialization (Need PD3-PD0)

#define GPIO_PORTD_DATA 	(*((volatile unsigned long *)0x400073FC))
#define GPIO_PORTD_DIR		(*((volatile unsigned long *)0x40007400))
#define GPIO_PORTD_AFSEL	(*((volatile unsigned long *)0x40007420))
#define GPIO_PORTD_DEN		(*((volatile unsigned long *)0x4000751C))



// Function Declarations

int main(void);
void init_gpio(void);
void pwm_gen(void);

int main(void)
	{
		SysTick_Init();
		init_gpio();
		GPIO_PORTD_DATA &= ~0x0F; // Ensure no pins are high at start.
		while(1)
		{
			pwm_gen();
		}

	}

void init_gpio(void)
	{
		volatile unsigned long delay_clk;
		SYSCTL_RCGC2 |= 0x00000008; // Port D
		delay_clk = SYSCTL_RCGC2;

		GPIO_PORTD_DIR |= 0x0F; // Pins 0 through 3 are input
		GPIO_PORTD_DEN |= 0x0F;
		GPIO_PORTD_AFSEL &= ~0x0F;
	}

void pwm_gen(void)
	{
		GPIO_PORTD_DATA |= 0x01;
		SysTick_Wait1ms(1);
		GPIO_PORTD_DATA &= ~0x0F;
		GPIO_PORTD_DATA |= 0x02;
		SysTick_Wait1ms(1);
		GPIO_PORTD_DATA &= ~0x0F;
		GPIO_PORTD_DATA |= 0x04;
		SysTick_Wait1ms(1);
		GPIO_PORTD_DATA &= ~0x0F;
		GPIO_PORTD_DATA |= 0x08;
		SysTick_Wait1ms(1);
		GPIO_PORTD_DATA &= ~0x0F;
	}






//	_ _ _ _ _ _ _ _
//					D	C	B	A
