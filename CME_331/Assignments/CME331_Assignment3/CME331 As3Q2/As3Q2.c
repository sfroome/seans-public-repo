/******************************************************************
CME 331 Assignment 3, Question 2
Sean Froome
October 12, 2015

This program uses SysTick_16.c and SysTick_16.h
This program flashes the red LED five times, when switch 1 is
pressed and then released.
******************************************************************/

#include "SysTick_16.h" // Since I'm using SysTick_16.c I need this header file as well to be able to call functions from SysTick_16.c

#define SYSCTL_RCGC2 					(*((volatile unsigned long*) 0x400FE108))
#define GPIO_PORTF_DATA       (*((volatile unsigned long *)0x400253FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTF_DIR        (*((volatile unsigned long *)0x40025400)) // p.616, set data flow direction
#define GPIO_PORTF_AFSEL      (*((volatile unsigned long *)0x40025420)) // p.625, enable alternate functions
#define GPIO_PORTF_DEN        (*((volatile unsigned long *)0x4002551C)) // p.636, make port digital
#define GPIO_PORTF_PCTL       (*((volatile unsigned long *)0x4002552C)) // p.641, port control (for alt. func. only)
#define GPIO_PORTF_PUR        (*((volatile unsigned long *)0x40025510)) // p.631, enable weak-pull-up, needed for SW1 and SW2

void init_gpio(void);
int main(void);
void wait_for_sw1(void);
void flash_red(void);
unsigned sw_flag = 0;

int main(void)
	{
		init_gpio(); // GPIO Initialization
		SysTick_Init(); // Initializes SysTick inside SysTick_16.c
		while(1)
		{
			wait_for_sw1(); // Waits for Switch 1 to be pressed.
			flash_red(); // Won't flash unless switch is pressed
									// Due to flag variable.
		}
	}
	
void init_gpio(void)
	{
		volatile unsigned long delay_clk; // delay for clock, must have 3 sys clock delay, p.424
		SYSCTL_RCGC2 |= 0x00000020; // enable clock gating for port F, p.424		
		delay_clk = SYSCTL_RCGC2; // this is a dummy instruction to allow clock to settle, no-operation
		// initialize control registers for Port F		
		GPIO_PORTF_DIR |=0x0E; // PF1-PF3 is output (output ports are set to 1), PF4 is input (set to 0)
		GPIO_PORTF_AFSEL &= ~0x1E; // disabling alternate functions on PF1-PF4 since I want GPIO.
		GPIO_PORTF_DEN |= 0x1E; // Enabling digital functionality on PF1-PF4
		GPIO_PORTF_PUR |= 0x10; // enabling pull-up on PF4 so that it acts as negative logic instead of floating	
	}
	
void wait_for_sw1(void)
	{
		if((GPIO_PORTF_DATA & 0x10) == 0) // Wait until sw is pressed. Function will be continuously called.
		{
			while((GPIO_PORTF_DATA & 0x10)== 0); // Once pressed, wait until switch is released before continuing.
			sw_flag = !sw_flag; // Sets the flag (should be positive)
		}
	}
	
void flash_red(void)
	{
		unsigned i;
		if(sw_flag == 1) // Should only be called if switch was pressed
			{				
				sw_flag = !sw_flag; // Resets flag so that it can be called again
				for(i = 0; i < 5; i++) // Flashes on/off five times
					{
						GPIO_PORTF_DATA |= 0x02; //Turns on Red LED
						SysTick_Wait_500ns(1000); // I made a mini delay function in SysTick_16, based on 0.5ms delay so x1000 = 0.5s
						GPIO_PORTF_DATA &= ~0x02; // Turns off Red LED
						SysTick_Wait_500ns(1000); // Ensures it's also turned off for half a second as well.
					}			
			}	
		else
			GPIO_PORTF_DATA &= ~0x02; // Needed to put something in the else. This ensures the LED won't turn on by fluke.
	}
