/**********************************************************
Code tamplate for CME331 Lab 2 (2014-15) - using LaunchPad, LM4F120
Khan Wahid, Sept 17, 2014
Modified by Sean Froome, September 29, 2015
using quad 7-segment and switches, simple display
*****************************************************/
//Notes:
// Display connected to PB0-PB7, 8-bits, Latch enable to PA4-PA7
// see schematics: https://www.engr.usask.ca/classes/CME/331/WebNotes_2014/Drawing_dev_board.jpg
// Uses SW1 (connected to pin PF4)
//**********************************Mapping registers************************************

// p.424, clock gating for GPIO
#define SYSCTL_RCGC2          	(*((volatile unsigned long *)0x400FE108)) 

// GPIO initialization p.610

// Port A
#define GPIO_PORTA_DATA			(*((volatile unsigned long *)0x400043FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTA_DIR        	(*((volatile unsigned long *)0x40004400)) // p.616, set data flow direction
#define GPIO_PORTA_AFSEL      	(*((volatile unsigned long *)0x40004420)) // p.625, enable alternate functions
#define GPIO_PORTA_DEN        	(*((volatile unsigned long *)0x4000451C)) // p.636, make port digital
//#define GPIO_PORTA_PCTL       	(*((volatile unsigned long *)0x4000452C)) // p.641, port control (for alt. func. only)
// Disabled because it's not really needed.


// Port B
#define GPIO_PORTB_DATA        	(*((volatile unsigned long *)0x400053FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTB_DIR        	(*((volatile unsigned long *)0x40005400)) // p.616, set data flow direction
#define GPIO_PORTB_AFSEL      	(*((volatile unsigned long *)0x40005420)) // p.625, enable alternate functions
#define GPIO_PORTB_DEN        	(*((volatile unsigned long *)0x4000551C)) // p.636, make port digital
//#define GPIO_PORTB_PCTL       (*((volatile unsigned long *)0x4000552C)) // p.641, port control (for alt. func. only)
// Again, disabled because it's not really needed.

//Port F
#define GPIO_PORTF_DATA        	(*((volatile unsigned long *)0x400253FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTF_DIR        	(*((volatile unsigned long *)0x40025400)) // p.616, set data flow direction
#define GPIO_PORTF_AFSEL      	(*((volatile unsigned long *)0x40025420)) // p.625, enable alternate functions
#define GPIO_PORTF_DEN        	(*((volatile unsigned long *)0x4002551C)) // p.636, make port digital
//#define GPIO_PORTF_PCTL       (*((volatile unsigned long *)0x4002552C)) // p.641, port control (for alt. func. only)
// Not needed, so I commented it out.
#define GPIO_PORTF_PUR       		(*((volatile unsigned long *)0x40025510)) // p.641, port control (for alt. func. only)
// PUR needed for switch

//Function Declarations
void init_gpio (void); // Initializes GPIO ports
void wait_for_sw1 (void); // Waits until the switch is pressed.
void display_uofs (void); // Displays UofS on the hex display 
void display_hello (void); // Displays Hello, one letter at at time, on the hex display
void delay (void); //  Delay function

// Look-up table (LUT) for 7-segment, ***active low***
// This LUT effectively becomes the driver for the 7-segment display
// PortB pins		:	 	7-6-5-4-3-2-1-0
// led segments	:   1-e-d-c-b-a-f-g 	
// VERY USEFUL INFORMATION ^^^^^^

const char lut_uofs [4] =	
	{
			0x85,  // U  10000101 (lut entry 0 <=> lut_uofs [0])
			0x81,  // O  10000001 (lut entry 1)  
			0xB8,  // F  10111000 (lut entry 2)
			0xC8,  // S  11001000 (lut entry 3)
	};
	
// add lut_hello for HELLO below
const char lut_hello [5] = 
{ 
			// Remember it's NEGATIVE LOGIC YOU MUPPET!
			0xA4,	// H		10110010				 	
			0x98,	// E		10011000
			0x9D,	// L		10011101
			0x9D, 	// L		10011101
			0x81, 	// 0		10000001
};



//  initialize ports
void init_gpio (void) 
{
	volatile unsigned long delay_clk; // delay for clock, must have 3 sys clock delay, p.424
	SYSCTL_RCGC2 |= 0x00000023; // find value for clock gating enable for ports A, B, F, p.424
	// Check that number above
	delay_clk = SYSCTL_RCGC2; // just a delay for the clock to settle, no-operation
	
	GPIO_PORTA_DIR |= 0xF0; // Bits 7 through 4 are enabled
	// Remember 1 is output, 0 is input for DIR
	GPIO_PORTA_DEN |= 0xF0; // Digital enable on Pins A4 through A7
	//GPIO_PORTA_PCTL |=0xF0; // Enabling Port Control on Pins PA4-PA7
	GPIO_PORTA_AFSEL &= ~0xF0; // Disabling Alternate Functions on PA4-PA7
		
	GPIO_PORTB_DIR |= 0xFF; // Bits 7 through 4 are enabled
	// Remember 1 is output, 0 is input for DIR
	GPIO_PORTB_DEN |= 0xFF; // Digital enable on Pins A4 through A7
	//GPIO_PORTB_PCTL |=0xF0; // Enabling Port Control on Pins PA4-PA7
	GPIO_PORTB_AFSEL &= ~0xFF; // Disabling Alternate Functions on PA4-PA7
	
	GPIO_PORTF_DIR &= ~0x10;
	// (ports PF0-PF4 set to 1 although they're not really in use for this lab), PF4 is input (set to 0)
	GPIO_PORTF_AFSEL &= ~0x10; // Disabling alternate functions on PF1-PF4
	GPIO_PORTF_DEN |= 0x10; // Enabling digital functionality on PF4
	GPIO_PORTF_PUR |= 0x10; // enabling pull-up on PF4
}

// displays UOFS on display board
void display_uofs (void)
	{
		// Note: you may also use GPIO_PORTB_DATA |= 0xFF; but here "=" is preferred over "|="...
		// ...because you will be using the entire portB (all 8-bits) to update the data value...
		// ...so there is no risk of changing other pins unintentionally when "=" is used
		// That is, if you use |= any that were set low for the previous display that aren't 
		// explicitly turned off for the next display will still turn on.
		
		GPIO_PORTB_DATA = 0xFF; // turns off all 7-segment, LED
		GPIO_PORTA_DATA |= 0xF0; // latch enable for all 4 displays, make PA4-PA7 high	
		GPIO_PORTA_DATA &= ~0xF0; // latch disable for all 4 displays, make PA4-PA7 low
		delay();
		
		GPIO_PORTB_DATA = lut_uofs [0]; // it will write lut entry 0 to Port B so 'U' is displayed..
		// ...for the same reason as above "=" is used instead of "|="
		GPIO_PORTA_DATA |= 0x10; // make PA4 (LE0) high	to write to display0
		GPIO_PORTA_DATA &= ~0x10; // make PA4 low so that no change on display0	
		delay();
		GPIO_PORTB_DATA = lut_uofs [1];
		GPIO_PORTA_DATA |= 0x20; //PA5
		GPIO_PORTA_DATA &= ~0x20;
		GPIO_PORTB_DATA = lut_uofs [2];
		GPIO_PORTA_DATA |= 0x40; //PA6
		GPIO_PORTA_DATA &= ~0x40;
		delay();
		GPIO_PORTB_DATA = lut_uofs [3];
		GPIO_PORTA_DATA |= 0x80; //PA7
		GPIO_PORTA_DATA &= ~0x80;
		delay();
	}



// displays flashing HELLO on display board
void display_hello (void) 
//Displays Hello one letter at a time on Display0
	{
		GPIO_PORTB_DATA = 0xFF; // turns off all 7-segment, LED
		GPIO_PORTA_DATA |= 0xF0; // latch enable for all 4 displays, make PA4-PA7 high	
		GPIO_PORTA_DATA &= ~0xF0; // latch disable for all 4 displays, make PA4-PA7 low
		delay (); 
		GPIO_PORTB_DATA = lut_hello [0]; // Calls the first value in lut_hello
		GPIO_PORTA_DATA = 0x10; // make PA4 (LE0) high	to write to display0
		GPIO_PORTA_DATA &= ~0x10; // make PA4 low so that no change on display0	
		delay();
		GPIO_PORTB_DATA = lut_hello [1]; //
		GPIO_PORTA_DATA = 0x10; // Writes to PA4 again, only sending the second value from lut_hello
		GPIO_PORTA_DATA &= ~0x10;
		delay();
		GPIO_PORTB_DATA = lut_hello [2];
		GPIO_PORTA_DATA = 0x10; // And again
		GPIO_PORTA_DATA &= ~0x10;
		delay();
		GPIO_PORTB_DATA = lut_hello [2];
		GPIO_PORTA_DATA = 0x10; //PA4
		GPIO_PORTA_DATA &= ~0x10;
		delay();
		GPIO_PORTB_DATA = lut_hello [3];
		GPIO_PORTA_DATA = 0x10; //PA
		GPIO_PORTA_DATA &= ~0x10;
		delay();
		GPIO_PORTB_DATA = lut_hello [4];
		GPIO_PORTA_DATA = 0x10; //PA
		GPIO_PORTA_DATA &= ~0x10;
		delay(); // Without this Delay, the last letter it will flash so fast a human isn't going to see it.
	}


// this is polling SW 1
void wait_for_sw1 (void)	
	{
		display_uofs(); // Displays UofS until the switch is pressed.
		while( (GPIO_PORTF_DATA & 0x10) == 0) // This is for the second part
			{
				display_hello();
			}
		// Probably not needed to call display_UofS again, but it's there just in case.	
		display_uofs();
	}

void delay (void) 
	{
		long unsigned i; // The lazy delay function in all its glory.
		for (i = 0; i < 1000000; i++);
	}


int main (void) 
	{
		init_gpio ();
		
		while (1) // With an infinite loop, MCU will continuously call 
				  // the function wait_for_sw1()
			{
				wait_for_sw1();

			}
	}
