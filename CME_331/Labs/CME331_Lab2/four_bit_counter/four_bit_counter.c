/*******************************************************************************
code tamplate for CME331 Lab 2 (2014-15) - using LaunchPad, LM4F120
Khan Wahid, Sept 17, 2014
Modified by: Sean Froome, September 29, 2015
using quad 7-segment and switches, simple display
******************************************************************************
Display connected to PB0-PB7, 8-bits, Latch enable to PA4-PA7
see schematics: https://www.engr.usask.ca/classes/CME/331/WebNotes_2014/Drawing_dev_board.jpg
Uses SW1 (connected to pin PF4)
**********************************Mapping registers************************************/
// p.424, clock gating for GPIO
#define SYSCTL_RCGC2          	(*((volatile unsigned long *)0x400FE108)) 

// GPIO initialization p.610
// add definitions for Port A below (may import them from asgn 1)
#define GPIO_PORTA_DATA					(*((volatile unsigned long *)0x400043FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTA_DIR        	(*((volatile unsigned long *)0x40004400)) // p.616, set data flow direction
#define GPIO_PORTA_AFSEL      	(*((volatile unsigned long *)0x40004420)) // p.625, enable alternate functions
#define GPIO_PORTA_DEN        	(*((volatile unsigned long *)0x4000451C)) // p.636, make port digital
//#define GPIO_PORTA_PCTL       	(*((volatile unsigned long *)0x4000452C)) // p.641, port control (for alt. func. only)


// add definitions for Port B below
#define GPIO_PORTB_DATA        	(*((volatile unsigned long *)0x400053FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTB_DIR        	(*((volatile unsigned long *)0x40005400)) // p.616, set data flow direction
#define GPIO_PORTB_AFSEL      	(*((volatile unsigned long *)0x40005420)) // p.625, enable alternate functions
#define GPIO_PORTB_DEN        	(*((volatile unsigned long *)0x4000551C)) // p.636, make port digital
//#define GPIO_PORTB_PCTL       	(*((volatile unsigned long *)0x4000552C)) // p.641, port control (for alt. func. only)


//  definitions for Port F (needed for SW1)
#define GPIO_PORTF_DATA        	(*((volatile unsigned long *)0x400253FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTF_DIR        	(*((volatile unsigned long *)0x40025400)) // p.616, set data flow direction
#define GPIO_PORTF_AFSEL      	(*((volatile unsigned long *)0x40025420)) // p.625, enable alternate functions
#define GPIO_PORTF_DEN        	(*((volatile unsigned long *)0x4002551C)) // p.636, make port digital
//#define GPIO_PORTF_PCTL       	(*((volatile unsigned long *)0x4002552C)) // p.641, port control (for alt. func. only)
#define GPIO_PORTF_PUR       		(*((volatile unsigned long *)0x40025510)) // p.641, port control (for alt. func. only)

//declare functions
void init_gpio (void);
int wait_for_sw1 (int);
void display_counter (int);
int counter(int);
void delay (void);

// Look-up table (LUT) for 7-segment, ***active low***
// This LUT effectively becomes the driver for the 7-segment display
// PortB pins		:	 	7-6-5-4-3-2-1-0
// led segments	:   1-e-d-c-b-a-f-g 	

const char lut_7seg [16] = 
{ 
		0x81, // 0
		0xE7, // 1
		0x92, // 2
 		0xC2, // 3
		0xE4, // 4
		0xC8, // 5
		0x88, // 6
		0xE3, // 7
		0x80, // 8
		0xC0, // 9
		0xA0, // A
		0x8C, // b
		0x99, // C
		0x86, // d
		0x98, // E
		0xB8, // F
};

//  initialize ports
void init_gpio (void) 
{
	volatile unsigned long delay_clk; // delay for clock, must have 3 sys clock delay, p.424
	SYSCTL_RCGC2 |= 0x00000023; // find value for clock gating enable for ports A, B, F, p.424
	// Check that number above
	delay_clk = SYSCTL_RCGC2; // just a delay for the clock to settle, no-operation
	
	GPIO_PORTA_DIR |= 0xF0; // Bits 7 through 4 are enabled
	// Remember 1 is output, 0 is input for DIR
	GPIO_PORTA_DEN |= 0xF0; // Digital enable on Pins A4 through A7
	//GPIO_PORTA_PCTL |=0xF0; // Enabling Port Control on Pins PA4-PA7
	GPIO_PORTA_AFSEL &= ~0xF0; // Disabling Alternate Functions on PA4-PA7
		
	GPIO_PORTB_DIR |= 0xFF; // Bits 7 through 4 are enabled
	// Remember 1 is output, 0 is input for DIR
	GPIO_PORTB_DEN |= 0xFF; // Digital enable on Pins A4 through A7
	//GPIO_PORTB_PCTL |=0xF0; // Enabling Port Control on Pins PA4-PA7
	GPIO_PORTB_AFSEL &= ~0xFF; // Disabling Alternate Functions on PA4-PA7
	
	GPIO_PORTF_DIR &= ~0x10;
	// (ports PF0-PF4 set to 1 although they're not really in use for this lab), PF4 is input (set to 0)
	GPIO_PORTF_AFSEL &= ~0x10; // Disabling alternate functions on PF1-PF4
	GPIO_PORTF_DEN |= 0x10; // Enabling digital functionality on PF4
	GPIO_PORTF_PUR |= 0x10; // enabling pull-up on PF4
}


void delay (void) 
	{
		long unsigned i;
		for (i = 0; i < 1000000; i++);
	}

int main (void) 
	{
		int i = 0;
		init_gpio ();
		GPIO_PORTB_DATA = 0xFF; // turns off all 7-segment, LED
		GPIO_PORTA_DATA |= 0xF0; // latch enable for all 4 displays, make PA4-PA7 high	
		GPIO_PORTA_DATA &= ~0xF0; // latch disable for all 4 displays, make PA4-PA7 low
		while (1) 
			{
				i = wait_for_sw1(i); // passes i to wait_for_Sw1	
			}
	}
	
int wait_for_sw1(int i) // int because I want to return i when done.
	{
		while( (GPIO_PORTF_DATA & 0x10) == 0) 
		{
			i = counter(i);	// i is equal to the value returned to it by int counter 
			// Which happens to be i.
		}
		return i; // returns the new value of i to main
	}
	
	
	int counter(int i)
	{ 	
			delay();
			display_counter(i);	
			i++;
			if(i == 16)
				{
					i=0;
				}
			else
				{
					i=i;
				}
			return i; // Will return new value of i to wait_for_sw1.
	}
	
	void display_counter (int i) 
	// no point making it an int function, since it doesn't need to return anything.
	{
		GPIO_PORTB_DATA = lut_7seg[i]; // sets port b to the ith value of lut_7seg.
		GPIO_PORTA_DATA |= 0x10; // latch enable, make PA4 high	
		GPIO_PORTA_DATA &= ~0x10; // latch disable for all just the first display, make PA4 low		
		delay();
	}
