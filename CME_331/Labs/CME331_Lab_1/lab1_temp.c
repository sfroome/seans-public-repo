/* Code Template for CME331 Lab 1- using LM4F120
Khan Wahid, Sept 6, 2014
Edited and Modified by Sean Froome, September 21, 2015 */

//I only printed off one set of code because the 
//biggest differences between the parts are which functions are called.
//This is indicated in my comments in int main()


//*****************Mapping registers*********************/
// p.424, clock gating for GPIO. In future programs, 
// Make sure the ports required are enabled before checking
//for other problems.
#define SYSCTL_RCGC2          	(*((volatile unsigned long *)0x400FE108)) 
// GPIO initialization p.610
#define GPIO_PORTF_DATA        	(*((volatile unsigned long *)0x400253FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTF_DIR        	(*((volatile unsigned long *)0x40025400)) // p.616, set data flow direction
#define GPIO_PORTF_AFSEL      	(*((volatile unsigned long *)0x40025420)) // p.625, enable alternate functions
#define GPIO_PORTF_DEN        	(*((volatile unsigned long *)0x4002551C)) // p.636, make port digital
#define GPIO_PORTF_PCTL       	(*((volatile unsigned long *)0x4002552C)) // p.641, port control (for alt. func. only)
#define GPIO_PORTF_PUR        	(*((volatile unsigned long *)0x40025510)) // p.631, enable weak-pull-up, needed for SW1 and SW2
//******Mapping registers END****************************//


//Declaring Functions
void init_gpio (void);
void delay (void); // Delay function forces the program to wait
//so that things like LEDs actually turn on and off 
// And human beings can actually see it happen, instead of them switching on and off instantaneously.
void flash_red(void); // Function is called when Red LED is to be switched on/off
void flash_blue(void); // Function is called when Blue LED is to be switched on/off
void flash_green(void); // Function is called when Green LED is to be switched on/off
void wait_for_sw1(void); // Function for controlling SW1


void init_gpio (void) 
	{
		volatile unsigned long delay_clk; // delay for clock, must have 3 sys clock delay, p.424
		SYSCTL_RCGC2 |= 0x00000020; // enable clock gating for port F, p.424 
		// If other ports were needed, this value would be changed according to which ports are needed.
		
		delay_clk = SYSCTL_RCGC2; // this is a dummy instruction to allow clock to settle, no-operation
		
		// initialize control registers for Port F		
		GPIO_PORTF_DIR |=0x0E; // PF1-PF3 is output (output ports are set to 1), PF4 is input (set to 0)
		GPIO_PORTF_AFSEL &= ~0x1E; // disabling alternate functions on PF1-PF4 since I want GPIO.
		GPIO_PORTF_DEN |= 0x1E; // Enabling digital functionality on PF1-PF4
		GPIO_PORTF_PUR |= 0x10; // enabling pull-up on PF4 so that it acts as negative logic instead of floating
	}

void delay (void) 
	{
		long unsigned i;
		for (i = 0; i < 1000000; i++); //Delay of 1000000  Clock Cycles(ish)
		// Dependent on number of operations
		// Dependent on Frequency
		// Delay is VERY approximate (around a second or so)
	}

int main (void) // The most important function of them all
	{
		init_gpio();	// Call to initialize GPIO
		while (1) // Yay for infinite loops. This way the MicroController is always doing something
			{ 
				wait_for_sw1(); // For Part III only. 
				// Comment out if you want to see Part I or two 
				// and uncomment the function calls below.
				//flash_red(); //Include for Part I and II only. 
				//flash_blue(); // Include for Part II only
				//flash_green(); // Include for Part II only
			}
	}

void flash_red(void) // Flashes Red LED on and off
	{
		GPIO_PORTF_DATA |= 0x02; //Turns on Red LED
		delay(); // Wouldn't see LED turn on without this delay 
		//because the action would be near instantaneous
		GPIO_PORTF_DATA &= ~0x02; // Turns off Red LED
	}
	
	void flash_blue(void) // Flashes Blue LED on and off
	{
		GPIO_PORTF_DATA |= 0x04; //Turns on Blue LED
		delay(); // Wouldn't see LED turn on without this delay 
		//because the action would be near instantaneous
		GPIO_PORTF_DATA &= ~0x04; // Turns off
	}

	void flash_green(void) // Flashes Green LED on and off
	{
		GPIO_PORTF_DATA |= 0x08; //Turns on
		delay();// Wouldn't see LED turn on without this delay 
		//because the action would be near instantaneous
		GPIO_PORTF_DATA &= ~0x08; // Turns off Green LED
	}
	
void wait_for_sw1(void) // The Controller will always wait for the switch to be pressed
	{											// Function will be called continuously due to the infinite loop.
		while( (GPIO_PORTF_DATA & 0x10) == 0) // When the Switch is pressed
			{																		// Remember the switch is Negative logic
					flash_red(); //If the switch isn't held down 
												// It won't  exit the second loop 
												// If the switch is still pressed the Blue LED will switch on
					while( (GPIO_PORTF_DATA & 0x10) == 0) // Unless the switch is continuously held
						{																			
							flash_blue();											
							while( (GPIO_PORTF_DATA & 0x10) == 0) // Like with the Blue LED, it won't enter the last while loop
								{								// Unless the switch is continuously held
									flash_green();											
									break; // Not the cleanest way of doing this
								}				//But this break statement ensures that the program will exit and
												//cycle through the LED colours again instead of continuously 
							break;		
						}						// entering the flash_green function
							//The Second break statement is the same as the first break statement,
							//but this one applies to the while loop controlling the Blue LED.
			}
	}

