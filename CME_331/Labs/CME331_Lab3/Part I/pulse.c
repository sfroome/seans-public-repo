/*********************************************
CME331 Lab 3 Part I
Sean Froome
October 12, 2015
Uses SysTick_16.h and SysTick_16.c
This code generates a pwm on four output pins.
Each wave is goes high for 1ms, and only one is high at a time.
Each one goes high in sequence.
********************************************/
#include "SysTick_16.h"

//Clock gating

#define SYSCTL_RCGC2 (*((volatile unsigned long *) 0x400FE108))


//GPIO Initialization (Need PD3-PD0)

#define GPIO_PORTD_DATA 	(*((volatile unsigned long *)0x400073FC)) // Just port D as output here, so only port D initialized.
#define GPIO_PORTD_DIR		(*((volatile unsigned long *)0x40007400))
#define GPIO_PORTD_AFSEL	(*((volatile unsigned long *)0x40007420))
#define GPIO_PORTD_DEN		(*((volatile unsigned long *)0x4000751C))



// Function Declarations
int main(void);
void init_gpio(void);
void pwm_gen(void);
void one_rotation(int);


int main(void)
	{
		int j=0;
		SysTick_Init();
		init_gpio();
		GPIO_PORTD_DATA &= ~0x0F; // Ensure no pins are high at start.
		while(1)
			{
				//pwm_gen(); Not calling pwm because one rotation is active instead.
				one_rotation(j); // Literally runs for one rotation and stops.
			}
		
	}
	
void init_gpio(void)
	{
		volatile unsigned long delay_clk;	
		SYSCTL_RCGC2 |= 0x00000008; // Port D
		delay_clk = SYSCTL_RCGC2;
				
		GPIO_PORTD_DIR |= 0x0F; // Pins 0 through 3 are input
		GPIO_PORTD_DEN |= 0x0F; 
		GPIO_PORTD_AFSEL &= ~0x0F;		
	}

void pwm_gen(void)
	{
		
// Counter-Clockwise
		GPIO_PORTD_DATA |= 0x01;
		SysTick_Wait1ms(3);
//		SysTick_Wait1ms(1); //To vary the speed of the motor by adding an extra 1ms delay b/w pulses
		GPIO_PORTD_DATA &= ~0x0F;		
		GPIO_PORTD_DATA |= 0x02;
		SysTick_Wait1ms(3);
//		SysTick_Wait1ms(1);
		GPIO_PORTD_DATA &= ~0x0F;		
		GPIO_PORTD_DATA |= 0x04;
		SysTick_Wait1ms(3);
//		SysTick_Wait1ms(1);
		GPIO_PORTD_DATA &= ~0x0F;			
		GPIO_PORTD_DATA |= 0x08;
		SysTick_Wait1ms(3);
//		SysTick_Wait1ms(1);
		GPIO_PORTD_DATA &= ~0x0F;
	// To turn the motor the other direction
				GPIO_PORTD_DATA |= 0x01;
		SysTick_Wait1ms(3);
//		SysTick_Wait1ms(1); //To vary the speed of the motor


 /*	// Clockwise. 
		GPIO_PORTD_DATA |= 0x08;
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;		
		GPIO_PORTD_DATA |= 0x04;
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;		
		GPIO_PORTD_DATA |= 0x02;
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;			
		GPIO_PORTD_DATA |= 0x01;
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;
*/	
	}
	
void one_rotation(int j)
	{
		while(j==0)
		{ 
			int i;
			for(i = 0; i<50; i++) // 1.8 degrees per pulse (not 4 pulses) 200 pulses = 1 rotation
				{	
					// Should be Counter-Clockwise
					GPIO_PORTD_DATA |= 0x01;
					GPIO_PORTD_DATA &= ~0x0F;
					GPIO_PORTD_DATA &= ~0x0F;		
					GPIO_PORTD_DATA |= 0x02;
					GPIO_PORTD_DATA &= ~0x0F;		
					GPIO_PORTD_DATA |= 0x04;
					GPIO_PORTD_DATA &= ~0x0F;			
					GPIO_PORTD_DATA |= 0x08;
				}
			j = 1;
		}
		
		
		
	}
	
	

	
//	_ _ _ _ _ _ _ _
//					D	C	B	A


