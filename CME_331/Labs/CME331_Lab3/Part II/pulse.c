/*********************************************
CME331 Lab 3 Part II
Sean Froome
October 12, 2015
Uses SysTick_16.h and SysTick_16.c
This is essentially part I, but with switches to both
control whether the motor is on/off, and a second switch
to change the rotation of the motor.
********************************************/
#include "SysTick_16.h"
#include "cme331_LM4F120.h"

// Function Declarations
int main(void);
void init_gpio(void);
void pwm_gen_ccw(void);
void pwm_gen_cw(void);
void on_off_switch1(void);
void wait_for_sw1(void);
void wait_for_sw2(void);

// Global Variables
unsigned sw_flag1 = 0;
unsigned sw_flag2 = 0;

int main(void)
	{
		SysTick_Init();
		init_gpio();
		GPIO_PORTD_DATA &= ~0x0F; // Ensure no pins are high at start.
		while(1)
			{
				wait_for_sw1();
				on_off_switch1();
			}	
	}

void init_gpio(void)
	{
		volatile unsigned long delay_clk;	
		SYSCTL_RCGC2 |= 0x00000028; // Port D and Port F
		delay_clk = SYSCTL_RCGC2;
		
		GPIO_PORTF_LOCK = 0x4C4F434B; // a write to GPIO_PORTF_CR auto re-applies lock, p.637
		GPIO_PORTF_CR |= 0x1; // enable write access for PF0, p.638

		GPIO_PORTD_DIR |= 0x0F; // Pins 0 through 3 are output to the motors.
		GPIO_PORTD_DEN |= 0x0F; 
		GPIO_PORTD_AFSEL &= ~0x0F;		
		
		GPIO_PORTF_DIR &= ~0x11; // Ports F aka the switches are input.
		GPIO_PORTF_DEN |= 0x11;
		GPIO_PORTF_AFSEL &= ~0x11;
		GPIO_PORTF_PUR |= 0x11 ;		
	}

void pwm_gen_ccw(void)
	{
		// Counter-Clockwise Full Step 
		GPIO_PORTD_DATA |= 0x01; // start at bit 0 
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;		
		GPIO_PORTD_DATA |= 0x02; // bit 1
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;	
		GPIO_PORTD_DATA |= 0x04; // bit 2
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;
		GPIO_PORTD_DATA |= 0x08; // bit 3
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;
	}
	
	
void pwm_gen_cw(void)
	{
		// Clockwise Full Step
		
		GPIO_PORTD_DATA |= 0x08; // Instead of starting at bit 1 start at bit 3
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;		
		GPIO_PORTD_DATA |= 0x04; // 2
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;		
		GPIO_PORTD_DATA |= 0x02; // 1
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;			
		GPIO_PORTD_DATA |= 0x01; // 0
		SysTick_Wait1ms(3);
		GPIO_PORTD_DATA &= ~0x0F;	
	}


	
void on_off_switch1(void)
	{
		if(sw_flag1 == 1) // Should only be called if switch was pressed once to turn on, ie: sw1 flag is set to on.
			{	
				wait_for_sw2(); // check direction
				if(sw_flag2 == 0)
				{
					pwm_gen_ccw(); // Will go ccw in one case, cw in the other.
				}
				else
					pwm_gen_cw();
			}
		else
			sw_flag1 = sw_flag1;
			GPIO_PORTD_DATA &= ~0x0F;
	}

void wait_for_sw1(void)
	{
		if((GPIO_PORTF_DATA & 0x10) == 0) // Wait until sw1 is pressed. Function will be continuously called.
			{
				while((GPIO_PORTF_DATA & 0x10)== 0); // Once pressed, wait until switch is released before continuing.
				sw_flag1 = !sw_flag1; // Sets the flag (should be positive)
				// Setting it to inverse ensures that it can go from 1 to 0 as well as 0 to 1.
			}
		else 
			sw_flag1 = sw_flag1;
	}

void wait_for_sw2(void) // This won't change if sw1 flag is set off. Its state flag won't change to default (0) if sw1 turns off the motor, but if the MCU is reset it will default to 0.
	{
		if((GPIO_PORTF_DATA &0x01) == 0)
			{
				while((GPIO_PORTF_DATA & 0x01) == 0);
				sw_flag2 = !sw_flag2;							
			}
		else
			sw_flag2 = sw_flag2;	
	}
