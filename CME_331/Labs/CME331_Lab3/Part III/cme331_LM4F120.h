//*******************************************************
// cme331_LM4F120.h - LM4F120EXL / TM4C123GXL Register Definitions
// What does this file contain: all GPIO and some important control register definitions..
// ..such as, PLL, ADC0, Timer0, and interrupt registers for Port F
// How to use this file: add in your main code as [  #include "cme331_LM4F120.h" ]
// Course website: https://www.engr.usask.ca/classes/CME/331
// Last update: Oct 6, 2014 by Khan Wahid
//*******************************************************

#ifndef __LM4F120_H__
#define __LM4F120_H__
// #include <stdint.h>

//*****************Mapping registers*********************//
//****Do not make any changes to these definitions....***//
//****simply use them in your program********************//

// p.424, clock gating for GPIO
#define SYSCTL_RCGC2          (*((volatile unsigned long *)0x400FE108)) 

// GPIO initialization p.610
// GPIO register map, Table 10.6, p.613
#define GPIO_PORTA_DATA        	(*((volatile unsigned long *)0x400043FC)) 
#define GPIO_PORTA_DIR        	(*((volatile unsigned long *)0x40004400))
#define GPIO_PORTA_AFSEL      	(*((volatile unsigned long *)0x40004420))
#define GPIO_PORTA_DEN        	(*((volatile unsigned long *)0x4000451C))
#define GPIO_PORTA_PCTL       	(*((volatile unsigned long *)0x4000452C))

#define GPIO_PORTB_DATA        	(*((volatile unsigned long *)0x400053FC)) // p.615, bit masking, p.608
#define GPIO_PORTB_DIR        	(*((volatile unsigned long *)0x40005400)) // p.616
#define GPIO_PORTB_AFSEL      	(*((volatile unsigned long *)0x40005420)) // p.625
#define GPIO_PORTB_DEN        	(*((volatile unsigned long *)0x4000551C)) // p.636
#define GPIO_PORTB_PCTL       	(*((volatile unsigned long *)0x4000552C)) // p.641, port control (for digital only)

// ******CAUTION: PORT C is also used for JTAG, apply caution when configuring it****
// PC4-PC7 as input, so, only configure DEN and PUR, leave rest "as is"
#define GPIO_PORTC_DATA        	(*((volatile unsigned long *)0x400063FC)) // p.615,
#define GPIO_PORTC_DEN        	(*((volatile unsigned long *)0x4000651C)) // p.636
#define GPIO_PORTC_PUR			(*((volatile unsigned long *)0x40006510)) // p.631

// GPIO initialization p.610, Register map p.613
#define GPIO_PORTD_DATA        	(*((volatile unsigned long *)0x400073FC)) // p.615,
#define GPIO_PORTD_DIR        	(*((volatile unsigned long *)0x40007400)) // p.616
#define GPIO_PORTD_AFSEL      	(*((volatile unsigned long *)0x40007420)) // p.625
#define GPIO_PORTD_DEN        	(*((volatile unsigned long *)0x4000751C)) // p.636
#define GPIO_PORTD_PCTL       	(*((volatile unsigned long *)0x4000752C)) // p.641, port control (for digital only)

#define GPIO_PORTE_DATA        	(*((volatile unsigned long *)0x400243FC)) 
#define GPIO_PORTE_DIR        	(*((volatile unsigned long *)0x40024400))
#define GPIO_PORTE_AFSEL      	(*((volatile unsigned long *)0x40024420))
#define GPIO_PORTE_DEN        	(*((volatile unsigned long *)0x4002451C))
#define GPIO_PORTE_PCTL       	(*((volatile unsigned long *)0x4002452C))
#define GPIO_PORTE_AMSEL      	(*((volatile unsigned long *)0x40024528)) // p.640

#define GPIO_PORTF_DATA        	(*((volatile unsigned long *)0x400253FC)) // p.615, data port, bit masking, p.608
#define GPIO_PORTF_DIR        	(*((volatile unsigned long *)0x40025400)) // p.616, set data flow direction
#define GPIO_PORTF_AFSEL      	(*((volatile unsigned long *)0x40025420)) // p.625, enable alternate functions
#define GPIO_PORTF_DEN        	(*((volatile unsigned long *)0x4002551C)) // p.636, make port digital
#define GPIO_PORTF_PCTL       	(*((volatile unsigned long *)0x4002552C)) // p.641, port control (for alt. func. only)
#define GPIO_PORTF_PUR        	(*((volatile unsigned long *)0x40025510)) // p.631, enable weak-pull-up, needed for SW1 and SW2

// register declaration: special code to activate SW2 (PF0)
#define GPIO_PORTF_LOCK       	(*((volatile unsigned long *)0x40025520)) // p.637, enable write access to GPIO_PORTF_CR
#define GPIO_PORTF_CR	        	(*((volatile unsigned long *)0x40025524)) // p.638, commit reg, lock-unlocks AFSEL, PUR, PDR, DEN
#define GPIO_LOCK_KEY           0x4C4F434B  // p. 637, unlocks the GPIO_CR register

// PLL registers
#define SYSCTL_RIS	            (*((volatile unsigned long *)0x400FE050)) // p.235
#define SYSCTL_RCC 	           	(*((volatile unsigned long *)0x400FE060)) // p.243
#define SYSCTL_RCGC2          	(*((volatile unsigned long *)0x400FE108)) // p.249

// ADC initialization p.770
#define SYSCTL_RCGCADC          (*((volatile unsigned long *)0x400FE638)) // p.322
#define SYSCTL_RCGC0          	(*((volatile unsigned long *)0x400FE100)) // p.419 (redundant, **not needed here**)
#define ADC0_PC					(*((volatile unsigned long *)0x40038FC4)) // p.840, ADC sample rate
#define ADC0_ACTSS             	(*((volatile unsigned long *)0x40038000)) // p.774, sample sequencer
#define ADC0_EMUX           	(*((volatile unsigned long *)0x40038014)) // p.785
#define ADC0_SSMUX3           	(*((volatile unsigned long *)0x400380A0)) // p.825
#define ADC0_SSCTL3          	(*((volatile unsigned long *)0x400380A4)) // p.826
#define ADC0_IM            		(*((volatile unsigned long *)0x40038008)) // p.777, interrupt
#define ADC0_PSSI            	(*((volatile unsigned long *)0x40038028)) // p.795, ADC start
#define ADC0_SSPRI     		    (*((volatile unsigned long *)0x40038020)) // p.791, priority

// ADC result, description of how it works -> p.756
#define ADC0_SSFIFO3          	(*((volatile unsigned long *)0x400380A8)) // p.810, ADC result
#define ADC0_RIS              	(*((volatile unsigned long *)0x40038004)) // p.775, raw interrupt status
#define ADC0_ISC          		(*((volatile unsigned long *)0x4003800C)) // p.780, interrupt status clear

// GPTM used as PWM p.657 (in PWM, it works as down counter)
//Timer register map on p.679
#define SYSCTL_RCGCTIMER        	(*((volatile unsigned long *)0x400FE604)) // p.308
#define TIMER0_CTL        			(*((volatile unsigned long *)0x4003000C)) // p.690, Timer0 control
#define TIMER0_CFG        			(*((volatile unsigned long *)0x40030000)) // p.680, set 16/32/64 bit, golbal operation of Timer0

#define TIMER0_TB_MR        		(*((volatile unsigned long *)0x40030008)) // p.686, configure Timer0B
#define TIMER0_TB_ILR        		(*((volatile unsigned long *)0x4003002C)) // p.710, Timer0B upper (start) value, i.e., period of PWM
#define TIMER0_TB_MATCHR   			(*((volatile unsigned long *)0x40030034)) // p.712, Timer0B match value, i.e., width of PWM
#define TIMER0_TB_PR        		(*((volatile unsigned long *)0x4003003C)) // p.714, Timer0B prescale

// To use Port F interrupt 
#define NVIC_EN0	            	(*((volatile unsigned long *)0xE000E100)) // p.137, IRQ 0 to 31 Set Enable Register
#define NVIC_PRI7		            (*((volatile unsigned long *)0xE000E41C)) // p.147, IRQ 28 to 31 Priority

#define GPIO_PORTF_IS	          (*((volatile unsigned long *)0x40025404)) // p.617, int sense, 0 = edge, 1 = level
#define GPIO_PORTF_IBE	          (*((volatile unsigned long *)0x40025408)) // p.618, both edges
#define GPIO_PORTF_IEV	          (*((volatile unsigned long *)0x4002540C)) // p.619, int event, 0 = falling, 1 = rising
#define GPIO_PORTF_IM	          (*((volatile unsigned long *)0x40025410)) // p.620, int mask
#define GPIO_PORTF_RIS        	(*((volatile unsigned long *)0x40025414)) // p.621, raw int status, not used here
#define GPIO_PORTF_MIS        	(*((volatile unsigned long *)0x40025418)) // p.622, masked int status, used here
#define GPIO_PORTF_ICR	        (*((volatile unsigned long *)0x4002541C)) // p.623, int flag clear, **must acknowledge**


#endif 

