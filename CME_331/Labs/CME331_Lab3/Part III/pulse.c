/*********************************************
CME331 Lab 3 Part III
Sean Froome
October 12, 2015
Uses SysTick_16.h and SysTick_16.c, 
as well as cme331_LM4F120.h

This program drives a stepper motor in either 
full wave, or half wave mode, depending 
on which setting is enabled. 
********************************************/
#include "SysTick_16.h"
#include "cme331_LM4F120.h"

// Function Declarations
int main(void);
void init_gpio(void);
void pwm_gen_ccw(void);
void pwm_gen_cw(void);
void one_rotation(int);
void on_off_switch1(void);
void wait_for_sw1(void);
void wait_for_sw2(void);

// Global Variables
unsigned sw_flag1 = 0;
unsigned sw_flag2 = 0;

int main(void)
	{
		SysTick_Init();
		init_gpio();
		GPIO_PORTD_DATA &= ~0x0F; // Ensure no pins are high at start.
		while(1)
			{
				wait_for_sw1();
				on_off_switch1();
			}	
	}

void init_gpio(void)
	{
		volatile unsigned long delay_clk;	
		SYSCTL_RCGC2 |= 0x00000028; // Port D and Port F
		delay_clk = SYSCTL_RCGC2;
		
		GPIO_PORTF_LOCK = 0x4C4F434B; // a write to GPIO_PORTF_CR auto re-applies lock, p.637
		GPIO_PORTF_CR |= 0x1; // enable write access for PF0, p.638

		GPIO_PORTD_DIR |= 0x0F; // Pins 0 through 3 are input
		GPIO_PORTD_DEN |= 0x0F; 
		GPIO_PORTD_AFSEL &= ~0x0F;		
		
		GPIO_PORTF_DIR &= ~0x11;
		GPIO_PORTF_DEN |= 0x11;
		GPIO_PORTF_AFSEL &= ~0x11;
		GPIO_PORTF_PUR |= 0x11 ;		
	}

	
void pwm_gen_ccw(void)
	{
		while ((GPIO_PORTF_DATA &0x01) && (GPIO_PORTF_DATA &0x10) )
			{
				// Counter-Clockwise Half Step
				GPIO_PORTD_DATA |= 0x01; // start with bit 0 enabled.
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA |= 0x02; // bit 0 and 1
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x01; // Just bit 1.
				SysTick_Wait500us(3);			
				GPIO_PORTD_DATA |= 0x04; // Bit 2 and 1
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x02; // Bit 2 only
				SysTick_Wait500us(3);				
				GPIO_PORTD_DATA |= 0x08; // Bit 3 and 2
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x04; // Bit 3 only
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA |= 0x01; // Bit 3 and 1
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x08; // just bit 1
			}
	}
	
	
void pwm_gen_cw(void)
	{
		// Clockwise Half Step
		while ((GPIO_PORTF_DATA &0x01) && (GPIO_PORTF_DATA &0x10) )
			{
				GPIO_PORTD_DATA |= 0x08; // Start with bit 3
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA |= 0x04; // bit 3 and bit 2
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x08; // just bit 2
				SysTick_Wait500us(3);			
				GPIO_PORTD_DATA |= 0x02; // bit 1 and 2
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x04; // bit 1 only
				SysTick_Wait500us(3);				
				GPIO_PORTD_DATA |= 0x01; //bit 0 and bit 1
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x02; // bit 0 only
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA |= 0x08; // bit 0 and 3
 				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x01; // just bit 3
			}
	}

/*
	void pwm_gen_ccw(void)
	{
		while ((GPIO_PORTF_DATA &0x01) && (GPIO_PORTF_DATA &0x10) )
			{
				// Counter-Clockwise Full Wave
				GPIO_PORTD_DATA |= 0x02;
				GPIO_PORTD_DATA &= ~0x01;
				GPIO_PORTD_DATA |= 0x04;
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x02;
				GPIO_PORTD_DATA |= 0x08;
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x04;
				GPIO_PORTD_DATA |= 0x01;
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x08;	
				GPIO_PORTD_DATA |= 0x02;
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x01;
			}
	}
// Full wave delays should've been 3ms and not 1.5ms long. 1.5ms isn't quite long enough for the motor,
// and it essentially is just long enough for the motor to actually operate.
//The SysTick_Wait500ns is simply the SysTick_Wait1ms but with a delay that's half as long, making a half millisecond 
// delay times the constant sent to the SysTick Wait function.
	
	
void pwm_gen_cw(void)
	{
		// Clockwise Full Wave
		while ((GPIO_PORTF_DATA &0x01) && (GPIO_PORTF_DATA &0x10) )
			{
				GPIO_PORTD_DATA |= 0x08;
				GPIO_PORTD_DATA &= ~0x01;
				GPIO_PORTD_DATA |= 0x04;
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x08;
				GPIO_PORTD_DATA |= 0x02;
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x04;
				GPIO_PORTD_DATA |= 0x01;
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x02;	
				GPIO_PORTD_DATA |= 0x08;
				SysTick_Wait500us(3);
				GPIO_PORTD_DATA &= ~0x01;	
			}
	}
*/

// See Part II comments for discussion on how the switch functions work.
void on_off_switch1(void)
	{
		if(sw_flag1 == 1) // Should only be called if switch was pressed
			{	
				wait_for_sw2();
				if(sw_flag2 == 0)
				{
					pwm_gen_ccw();
				}
				else
					pwm_gen_cw();
			}
		else
			sw_flag1 = sw_flag1;
			GPIO_PORTD_DATA &= ~0x0F;
	}

	
void wait_for_sw1(void)
	{
		if((GPIO_PORTF_DATA & 0x10) == 0) // Wait until sw1 is pressed. Function will be continuously called.
			{
				while((GPIO_PORTF_DATA & 0x10)== 0); // Once pressed, wait until switch is released before continuing.
				sw_flag1 = !sw_flag1; // Sets the flag (should be positive)
			}
		else 
			sw_flag1 = sw_flag1;
	}

void wait_for_sw2(void)
	{
		if((GPIO_PORTF_DATA &0x01) == 0)
			{
				while((GPIO_PORTF_DATA & 0x01) == 0);
				sw_flag2 = !sw_flag2;							
			}
		else
			sw_flag2 = sw_flag2;	
	}
