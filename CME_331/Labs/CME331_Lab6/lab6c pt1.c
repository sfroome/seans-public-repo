
// Iniatializations
#include "ST7735.h"
#include "Systick.h"

// ADC Initialization
#define SYSCTL_RCGCADC 					(*((volatile unsigned long *) 0x400FE638)) // To enable clock for ADC module
#define SYSCTL_RCGCGPIO         (*((volatile unsigned long *)0x400FE608)) // (**not used**)

#define ADC0_SSPRI 		(*((volatile unsigned long *) 0x40038020)) // to select SS priority  (sample sequencers)
#define ADC0_PC 			(*((volatile unsigned long *) 0x40038FC4)) // to select sampling rate
#define ADC0_ACTSS 		(*((volatile unsigned long *) 0x40038000)) // to enable/disable sample sequencer
#define ADC0_EMUX 		(*((volatile unsigned long *) 0x40038014)) // to set trigger mode
#define ADC0_SSMUX3 	(*((volatile unsigned long *) 0x400380A0)) //  tp select ADC Channel
#define ADC0_SSCTL3 	(*((volatile unsigned long *) 0x400380A4)) // to configure sequencer control

#define ADC0_PSSI 		(*((volatile unsigned long *) 0x40038028)) // ??
#define ADC0_SSFIFO3 	(*((volatile unsigned long *) 0x400380A8)) // ??


#define ADC0_RIS 			(*((volatile unsigned long *) 0x40038004)) // Raw Interrupt Status
#define ADC0_ISC 			(*((volatile unsigned long *) 0x4003800C)) // ??



// GPIO Registers
#define SYSCTL_RCGC2  (*((volatile unsigned long *)0x400FE108)) // Clock Gating

// Port A
//#define GPIO_PORTA_DATA 	(*((volatile unsigned long *)0x400043FC))
//#define GPIO_PORTA_DIR 		(*((volatile unsigned long *)0x40004400))
//#define GPIO_PORTA_AFSEL 	(*((volatile unsigned long *)0x40004420))
//#define GPIO_PORTA_DEN 		(*((volatile unsigned long *)0x4000451C))

// Port B
//#define GPIO_PORTB_DATA 	(*((volatile unsigned long *)0x400053FC))
//#define GPIO_PORTB_DIR 		(*((volatile unsigned long *)0x40005400))
//#define GPIO_PORTB_AFSEL 	(*((volatile unsigned long *)0x40005420))
//#define GPIO_PORTB_DEN 		(*((volatile unsigned long *)0x4000551C))

// Port E
#define GPIO_PORTE_DATA 	(*((volatile unsigned long *)0x400243FC))
#define GPIO_PORTE_DEN 		(*((volatile unsigned long *)0x4002451C)) 
#define GPIO_PORTE_AFSEL 	(*((volatile unsigned long *)0x40024420))
#define GPIO_PORTE_DIR 		(*((volatile unsigned long *)0x40024400))
#define GPIO_PORTE_AMSEL 	(*((volatile unsigned long *)0x40024528))


void init_gpio(void);
void init_adc(void);
void lab_part1(void);
unsigned char hex2char(unsigned int hex);

void delay(void);

// unsigned result1 = 0;
// unsigned result2 = 0;
// unsigned result3 = 0;

 unsigned int result1;
 unsigned int result2;
 unsigned int result3;

//int result;
//int display_result[3] = {0,0,0};

int main(void)
	{	
		init_gpio();
		init_adc();
		ST7735_InitR(INITR_BLACKTAB);
		ST7735_FillScreen(ST7735_BLACK);  // set screen to black 
		ST7735_SetRotation(3);			// rotates display screen
		
		while(1)
			{
				lab_part1();	
				ST7735_DrawChar(26, 10, hex2char(result3), ST7735_GREEN, 0 ,3);
				ST7735_DrawChar(42, 10, hex2char(result2), ST7735_GREEN, 0 ,3);
				ST7735_DrawChar(58, 10, hex2char(result1), ST7735_GREEN, 0 ,3);
				delay();
				delay();
				
				
//			ST7735_DrawChar(26, 10, display_result[0], ST7735_GREEN, 0 ,3);
//			ST7735_DrawChar(42, 10, display_result[1], ST7735_GREEN, 0 ,3);
//			ST7735_DrawChar(58, 10, display_result[2], ST7735_GREEN, 0 ,3);			


			}
	}
	
void init_gpio(void)
	{
		volatile unsigned long delay_clk;
		SYSCTL_RCGC2 |= 0x00000010;
		delay_clk = SYSCTL_RCGC2; 		
		
		GPIO_PORTE_DIR |= 0x0F;
		GPIO_PORTE_AFSEL |= 0x10; // PORT E AFSEL is needed for PE4
		GPIO_PORTE_DEN &= ~0x10; // PE4 doesn't need DEN, but PE0-PE3 do
		GPIO_PORTE_AMSEL |= 0x10; // Analog mode select on PE4
	}
	
void init_adc(void) // Part One ADC Initialization Function.
		{
			volatile unsigned long delay_clk_adc;
			SYSCTL_RCGCADC = 0x00000001;
			delay_clk_adc = SYSCTL_RCGCADC;
			delay_clk_adc = SYSCTL_RCGCADC;
			
			ADC0_ACTSS &= ~0x8;
			ADC0_SSPRI |= 0x00003;
			ADC0_PC = 0x1;	
			ADC0_EMUX |= 0x0;

			ADC0_SSMUX3 |= 0x9;	
			ADC0_SSCTL3 |= 0x6;	
			ADC0_ACTSS |= 0x8;
		}
	
	
void lab_part1(void) // Code for Part One of the Lab
	{	
		ADC0_PSSI |= 0x0008;	
		while((ADC0_RIS & 0x0008) == 0);
		result1 = ADC0_SSFIFO3 & 0xF; // Not shifted at all. This would be the LSB (truncation)
		result2 = (ADC0_SSFIFO3 >> 4 ) & 0xF;
		result3 = (ADC0_SSFIFO3 >> 8 ) & 0xF;	// Shifted right by 8 bits... this is the MSB.
		ADC0_ISC &= ~0x8;

		//result = ADC0_SSFIFO3 & 0xFFF;
		//display_result[0] = hex2char((result / 100) % 10 ); // MSB
		//display_result[1] = hex2char((result / 10) % 10 ); // 
		//display_result[2] = hex2char((result) % 10 ); // LSB			
	}

unsigned char hex2char(unsigned int hex) 
	{
		if(hex >=0x00 && hex <=0x09)
			{
				return (hex+0x30);
			}
		else if ( hex >=0x0A && hex <=0x0F)
			{
				return (hex+0x37);
			}
		else
			{
				return 0xFF;
			}
	}

void delay (void)
	{
		long unsigned i;
		for (i = 0; i < 999999; i++);
	
	}

void SysTick_Handler(void)
	{
		
		
	}
	
