print('This sentence is output to the screen')

#Output: This sentence is output to the screen

a = 5

print('The value of a is', a)
#Output: The value of a is 5


x = 5; y = 10

print('The value of x is {} and y is {}' .format(x,y))
# {} act as placeholders, and format dictates what goes in which squiggly bracket.

print('I love {0} and {1}'.format('bread','butter'))
# Output: I love bread and butter

print('I love {1} and {0}'.format('bread','butter'))
# Output: I love butter and bread
#You can even dictate the ordering of the different strings.
#str.format() method
