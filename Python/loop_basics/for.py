# Program to find the sum of all numbers stored in a list

# List of numbers
numbers = [6, 5, 3, 8, 4, 2, 5, 4, 11]

# variable to store the sum
sum = 0

#val is  the variable that takes the value of the item inside the sequence on each iteration.
#Loop continues until the last item in the sequence is reached. The body of for loop is separated from
#the rest of the code using indentation.

# iterate over the list
for val in numbers:
	sum = sum+val

# Output: The sum is 48
print("The sum is", sum)
	
