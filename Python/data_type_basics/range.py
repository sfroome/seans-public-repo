#We can generate a sequence of numbers using range() function.
# range(10) will generate numbers from 0 to 9 (10 numbers).
#We can also define the start, stop and step size as range(start,stop,step size). 
#step size defaults to 1 if not provided.


# Output: range(0, 10)
print(range(10))

# Output: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
print(list(range(10)))

# Output: [2, 3, 4, 5, 6, 7]
print(list(range(2, 8)))

# Output: [2, 5, 8, 11, 14, 17]
print(list(range(2, 20, 3)))	
