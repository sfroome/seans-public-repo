# Integer, Float and a string
a = 5
b = 3.2
c = "Hello"


#Can assign values to multiple variables at once
a,b,c = 5, 3.2, "Hello"

print(a)
print(b)
print(c)

#Can also print out the type of variable an object is

print(a, "is of type", type(a))
print(b, "is of type", type(b))
print(c, "is of type", type(c))


#Complex Numbers

d = 1+2j

print(d, "is complex number?", isinstance(1+2j, complex))
print(d, "is of type", type(d))

#Floats are accurate up to 15 decimal places
# (Run this in the python command window)
a = 1234567890123456789
1234567890123456789
b = 0.123456789012345678
b
0.12345678901234568
c = 1+2j
c
(1+2j)


a = [5,10,15,20,25,30,35,40]
# a[2] = 15
print("a[2] = ", a[2])

# a[0:3] = [5, 10, 15]
print("a[0:3] = ", a[0:3])

# a[5:] = [30, 35, 40]
print("a[5:] = ", a[5:])
