# CMPT 141 Lecture Code
# Written by Sean Froome
# Demo Code from the lecture demonstrating Print Statements
#

price_per_blanket = 8
price_per_pillow = 12
cash_in_wallet = 50

print("One blanket costs", price_per_blanket, "dollars.")
print("If I buy four pillows, that will cost " + str(price_per_pillow*4), "dollars.")
print("If I buy one blanket and two pillows, that will cost $" + str(price_per_blanket+2*price_per_pillow),
      ". If I pay with $ " + str(cash_in_wallet), ", I will have $" + str(cash_in_wallet - price_per_blanket - 2*price_per_pillow), " left.", sep="")