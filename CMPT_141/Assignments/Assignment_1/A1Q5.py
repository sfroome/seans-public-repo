# CMPT 141 Assignment 1 Question 5
# Written By: Sean Froome
# This program asks for an integer value
# and calculates the time it takes for an object to fall
#
#
#Test Case
# input 50
#
#From what height (in meters) do you wish to drop your computer?50
#It takes 3.1943828249996997 seconds for the computer to fall 50 meters.
#There is an enormous crash as the case smashes upon impact with the ground.

import math as m

Height = input("From what height (in meters) do you wish to drop your computer?")
#print(Height)

time = m.sqrt((2*int(Height))/9.8)

print("It takes", str(time), "seconds for the computer to fall", str(Height), "meters.")

print("There is an enormous crash as the case smashes upon impact with the ground.")