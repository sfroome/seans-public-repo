# CMPT 141 Assignment 1 Question 4
# Written By: Sean Froome
# This program functions like a Mad-Lib.
# It prompts the user for several verbs, nouns, and adjectives,
# and outputs the mad lib at the end.
#
#Test Case
#inputs: jumped, cookies, smelly, destroyed, aircraft carrier
#
#Mario and Luigi jumped after their latest battle with Bowser.
#They were given cookies by Princess Peach for their smelly efforts.
#The Mushroom Kingdom was destroyed by Bowser's evil aircraft carrier.

verb_1 = input("Enter a past tense verb.")
noun_1 = input("Enter a noun")
adjective_1 = input("Enter an adjective")
verb_2 = input("Enter another past tense verb")
noun_2 = input("Enter a noun")

print("Mario and Luigi", verb_1, "after their latest battle with Bowser.")
print("They were given", noun_1, "by Princess Peach for their", adjective_1, "efforts.")
print("The Mushroom Kingdom was", verb_2, "by Bowser's evil", noun_2 + ".")