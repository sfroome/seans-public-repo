# CMPT 141 Assignment 1 Question 6
# Written By: Sean Froome
# This program asks for an integer value
# and calculates the time it takes for an object to fall

#Test Case
# input 50
#
#Enter the total value of all of Captain Tractor's Booty in Canadian Dollars5000
#How many members are in Captain Tractor's Crew?23
#The Captain gets $1500.0 Dollars of booty.
#The Crews Share of the Booty is $3500.0 Dollars.
#This means that each crew member gets $152.17391304347825 Dollars.
#The Captain donates $750.0 Dollars to the Saskatoon Food Bank.

Total_Booty = input("Enter the total value of all of Captain Tractor's Booty in Canadian Dollars")

Crew_Size = input("How many members are in Captain Tractor's Crew?")

Captains_Share = 0.30*int(Total_Booty)
print("The Captain gets $" + str(Captains_Share), "Dollars of booty.")

Crews_Share = 0.70*int(Total_Booty)
Crew_Share_Per_Person = Crews_Share/int(Crew_Size)
print("The Crews Share of the Booty is $" + str(Crews_Share), "Dollars.")
print("This means that each crew member gets $" + str(Crew_Share_Per_Person), "Dollars.")
Captains_Donated_Share = Captains_Share/2
print("The Captain donates $" + str(Captains_Donated_Share), "Dollars to the Saskatoon Food Bank.")