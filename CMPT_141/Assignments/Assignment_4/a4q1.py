#CMPT 141 AS4Q1
#Written by: Sean Froome
#Original paragraph string written by intructors

paragraph = """  I am a Canadian, a free Canadian, free to speak without fear, free to worship in my own way, free to
stand for what I think right, free to oppose what I believe wrong, or free to choose those who shall govern my country.
This heritage of freedom I pledge to uphold for myself and all mankind!!....  """

# PART A
print(paragraph)
paragraph_mod = paragraph.lstrip("   ")
print(paragraph_mod)
print()

paragraph_mod_mod = paragraph_mod.rstrip("   ")
print(paragraph_mod_mod)
print()

paragraph_mod_mod_mod = paragraph_mod_mod.replace(',','')
paragraph_mod_mod_mod = paragraph_mod_mod_mod.replace('!','')
paragraph_mod_mod_mod = paragraph_mod_mod_mod.replace('.','')
print(paragraph_mod_mod_mod)
print()

paragraph = paragraph_mod_mod_mod
print(paragraph)

# PART B
list = paragraph.split()
print(list)

#PART C
def count_words(word, word_list):
    '''
    Given a string, and a list of strings, this function
    counts the number of times the first string appears in the list of strings.
    param word  A string that is to be searched for in word_count
    param word_count: a list of strings that is to be searched
    :returns: count, the number of times word appears in word_count.
    '''
    count = 0
    for x in range(0, len(word_list), 1):
        if word_list[x] == word:
            count += 1
        else:
            count = count
    return count

word_count = count_words('free', list)
print(word_count)

#PART D

def unique_words(word_list):
    '''
    Given a word_list, determine what words are unique.
    param word_list: a list of words that will be searched
    :returns: unique_wordss, a list of words that are unique in word_list.
    '''

    unique_wordss = []
    for x in range(0, len(word_list), 1):
        is_unique = count_words(word_list[x], word_list)
        if is_unique == 1:
            unique_wordss.append(word_list[x])
        else:
            for y in range(0, len(unique_wordss),1):
                is_on_list = count_words(word_list[x],unique_wordss)
                if is_on_list == 0:
                    unique_wordss.append(word_list[x])
    unique_wordss.sort()
    return unique_wordss


#PART E

#print()
#print(Unique_LIST)

unique_list = unique_words(list)
for x in range(0, len(unique_list), 1):
    number_of_times = count_words(unique_list[x], list)
    print("The word", unique_list[x], "appears", number_of_times, "times in the paragraph.")
