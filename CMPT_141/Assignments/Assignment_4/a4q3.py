#CMPT 141 AS4Q3
#Written by: Sean Froome
#Original Quest list provided by Instructors.

quests = [
    ["Destroy the Rats in the Farmer's Basement", "Westhaven Farms", [1, 5], [1.0, 0.2]],
    ["Infiltrate the Bandit's Lair", "The Badlands", [2, 4], [40.0, 1]],
    ["Defeat Goliad", "Candy Kingdom", [20, 25], [10.0, 1.5]],
    ["Locate the Lich's Lair", "Costal Wasteland", [35, 40], [100.0, 10.0]],
    ["Atone for Shoko's Sins", "Finn's Treehouse", [15, 17], [0.0, 0.0]],
    ["Make an Amazing Sandwich", "Finn's Treehouse", [7, 11], [0.0, 0.0]],
    ["Find the Ice King's Wizard Eye", "Ice Kingdom", [9, 15], [25.0, 2.0]],
    ["Get some pickles from Prismo", "Prismo's Home", [27, 31], [42000000.0, 0.1]],
    ["Rescue Wildberry Princess from the Ice King", "Ice Kingdom", [3, 11], [25.0, 2.0]],
    ["Win Wizard Battle", "Wizard Battle Arena", [13, 18], [90.0, 7.0]],
    ["Eat Marceline's Fries", "Marceline's House", [3, 6], [16.0, 2.5]],
    ["Rescue Marceline from the Nightosphere", "The Nightosphere", [21, 27], [200.0, 1.5]],
    ["Discover Peppermint Butler's Secrets", "Candy Kingdom", [35, 40], [10.0, 1.5]],
    ["Defeat the Ice King's Penguin Army", "Candy Kingdom", [27, 29], [10.0, 1.5]],
    ["Discover the Ice King's Secret Past", "The Past", [10, 35], [0.5, 21.0]],
    ["Watch what Beemo Does When He Is Alone", "Finn's Treehouse", [1, 50], [0.0, 0.0]]
]

# Code parts (a) through (g) of question 4 here.
 #PART A

def create_quest(quest_name, quest_location, min_reqs, max_reqs, quest_dist, time_needed):
    '''
    given the parameters, creates a quest out of the provided information.
    paramters:
    quest_name a string which is the name of the quest.
    quest_location the starting location of the quest (also a string).
    min_reqs aka the minimum (recommended) level to do the quest (it's an integer)
    max_reqs aka the max level for receiving quest rewards (an integer)
    quest_dist a float which is the distance needed to travel to the quest location.
    time_needed another float which is the time required to travel to the quest location.
    returns quest information, which is a list of lists of the information provided.
    '''

    quest_information = []
    quest_level = []
    quest_level.append(min_reqs)
    quest_level.append(max_reqs)

    quest_space_time = []
    quest_space_time.append(quest_dist)
    quest_space_time.append(time_needed)

    quest_information.append(quest_name)
    quest_information.append(quest_location)
    quest_information.append(quest_level)
    quest_information.append(quest_space_time)

    return quest_information


#PART B (and yes I'm a Oblivion/Skyrim nerd with the names, but not the numerical values

# Quest 1
quest_name1 = "A Night to Remember"
quest_location1 = "White Run"
min_reqs1 = 1
max_reqs1 = 100
quest_dist1 = 50.0
time_needed1 = 3.0

Quest_1 = create_quest(quest_name1, quest_location1, min_reqs1, max_reqs1, quest_dist1, time_needed1)
#print(Quest_1)


# Quest 2
quest_name2 = "Blood on the Ice"
quest_location2 = "Windhelm"
min_reqs2 = 5
max_reqs2 = 20
quest_dist2 = 10.0
time_needed2 = 1.0

Quest_2 = create_quest(quest_name2, quest_location2, min_reqs2, max_reqs2, quest_dist2, time_needed2)
#print(Quest_2)

# Quest 3
quest_name3 = "Taking Care of Lex"
quest_location3 = "The Imperial City"
min_reqs3 = 10
max_reqs3 = 15
quest_dist3 = 1.0
time_needed3 = 4.0

Quest_3 = create_quest(quest_name3, quest_location3, min_reqs3, max_reqs3, quest_dist3, time_needed3)
#print(Quest_3)

#PART C
quests.append(Quest_1)
quests.append(Quest_2)
quests.append(Quest_3)

#print(quests)

#PART D
#print(quests)
def travel_time(query_quest, current_quests):
    '''
    given the parameters, prints out to the console the travel time for the quest "queried"
    In addition, checks whether the quest being queried is in the list current_quests as well.
    paramters:
    query_quets a list of lists including all the quest information.
    current_quest a list of quests (list of lists)
    returns nothing
    '''
    quest_exists = 0
    quest_number = 0
    for i in range(0, len(current_quests), 1):
        if query_quest[0] == current_quests[i][0]:
            quest_exists = 1
        else:
            quest_number += 1

    if quest_exists == 1:
        print("The amount of time required to journey to", query_quest[1], "is", query_quest[3][1], "hours.")
    else:
        print("There is no current quest with the name", str(query_quest[0]) + ".")
    return 0

#PART E
travel_time(Quest_1, quests)
Dummy_Quest =["The Quest for the Holy Grail", "Camelot", [7, 8], [101.0, 10.0]]
travel_time(Dummy_Quest, quests)

#PART F

def print_nearby_quests(max_distance, max_level, current_quests):
    '''
    given the parameters, checks what quests are in current_quests that both are under the
    max level and max distance away. Prints result to the screen.
    paramters:
    max_distance, a float that as above, is the character's distance away from the quest start location.
    max_level, an integer, that as above is the max level the character can get quest rewards.
    current_quest a list of quests (list of lists) that lists a bunch of quests and the related information  inside the lists.
    returns nothing
    '''
    nearby_quests = []
    for i in range(0, len(current_quests), 1):
        if current_quests[i][3][0] <= max_distance and current_quests[i][2][1] <= max_level:
            nearby_quests.append(current_quests[i])
            print(current_quests[i][0], "at", current_quests[i][1],"is nearby.", "You can get there in", current_quests[i][3][1], "hours.")
    return 0

#PART G
distance = 100.0
level = 15
print_nearby_quests(distance, level, quests)
