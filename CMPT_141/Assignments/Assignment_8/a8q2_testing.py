#A8Q2 Testing
#Sean Froome

def ThreeSUM(in_list):
    """
    Determines whether or not there are three integers in the input list that sum to zero.
    :param : in_list a list of integers, any length
    :return: True if in_list contains three integers that sum to zero.
    """
    in_list.sort()

    for first in range(1,len(in_list) - 3):
        a = in_list[first]
        second = first + 1
        third = len(in_list) - 2
        while second < third:
            b = in_list[second]
            c = in_list[third]
            if a + b + c == 0:
                return True
            if a + b + c > 0:
                third -= 1
            else:
                second += 1
        return False

#############################################################################
# TEST DRIVER FOR ThreeSUM()
#
#
# Blackbox and Whitebox Tests
#############################################################################

# dictionary of test case suite to feed into test driver loop
test_suite1 = [

    # call for default case from question
    { "inputs" : [[1,2,3]],
      "outputs" : False,
      "reason"  : "Blackbox: Verify addition. It shouldn't not sum to zero" },

    # call for default case from question
    {"inputs": [[1, 2, 3,0,0,0,0,0,0]],
     "outputs": True,
     "reason": "Whitebox: Test zeros, and length of list."},

    # call for second default case from question
    { "inputs"  : [[0,3,-3]],
      "outputs" : True,
      "reason"  : "Blackbox: Check combination of numbers. Should Sum to Zero" },

    # call for second default case from question
    {"inputs": [[3, -3]],
     "outputs": False,
     "reason": "Blackbox: Check behaviour of a list wit less than three elements. It should return False"
               "because it lacks three elements."},

    # call for second default case from question
    {"inputs": [[0, 3, -3,0,0,0,0,0]],
     "outputs": True,
     "reason": "Whitebox: More index testing. Should Sum to Zero"
               "although unclear without debugger which combination will sum to zero."},

    # call for all values divisible by 3
    { "inputs"  : [[1,2,-3]],
      "outputs" : True,
      "reason"  : "Blackbox: Test Negative numbers. Should Sum to Zero" },

    # call for all ones
    { "inputs"  : [[1,1,1]],
      "outputs" : False,
      "reason"  : "Blackbox. Test all ones. Should not Sum to Zero" },

    { "inputs"  : [[0,2,-2,0,0,0,0,0]],
      "outputs" : True,
      "reason"  : "Whitebox. Should sum to Zero. As above,"
                  "without debugger, it won't be clear which combination of numbers "
                  "results in it returning True." },

    { "inputs"  : [[1,-1,0]],
      "outputs" : True,
      "reason"  : "Blackbox. Check Negative values. Should sum to zero" },

    {"inputs": [[1, -1,0,0,0,0,0,0,0]],
     "outputs": True,
     "reason": "Whitebox. More messing with padding zeroes. Should sum to zero"},

    {"inputs": [[ 0, 0, 0, 0, 0, 0, 0]],
     "outputs": True,
     "reason": "Blackbox. Check all zeros. Should sum to zero"},

    {"inputs": [[0]],
     "outputs": False,
     "reason": "Blackbox. Check list with one Value.Should sum to zero"},

    {"inputs": [[-3,3,3,-3,0]],
     "outputs": True,
     "reason": "Should sum to zero"},

    {"inputs": [[-1, -2, -3, 1, 2, 3]],
     "outputs": True,
     "reason": "Blackbox. Test a larger list than the ones above. Should sum to zero"},

    {"inputs": [[1, 2, -3, 0]],
     "outputs": True,
     "reason": "Whitebox. Last three tests were to verify the "
               "size of the list that actually will return True. Should Sum to Zero"},

    {"inputs": [[1, 2, -3, 0,0]],
     "outputs": True,
     "reason": "Whitebox. Last three tests were to verify the "
               "size of the list that actually will return True. Should Sum to Zero"},

    {"inputs": [[1, 2, -3, 0,0,0]],
     "outputs": True,
     "reason": "Whitebox. Last three tests were to verify the "
               "size of the list that actually will return True. Should Sum to Zero"}
]

for test in test_suite1:
    inputs = test["inputs"]
    result = ThreeSUM(inputs[0])
    if result != test["outputs"]:
        print("Testing fault: ThreeSUM() returned",result,"on inputs",inputs,
              "(",test["reason"],")")


