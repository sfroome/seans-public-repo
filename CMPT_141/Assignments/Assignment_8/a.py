
def ThreeSUM(in_list):
    """
        Determines whether or not there are three integers in the input list that sum to zero.
        :param : in_list a list of integers, any length
        :return: True if in_list contains three integers that sum to zero.
        """
    in_list.sort()
    # for first in range(1,len(in_list) - 3):
    for first in range(0, len(in_list) - 1):
        a = in_list[first]
        second = first + 1
        if len(in_list) > 3:
            third = len(in_list) - 2
        else:
            third = second + 1
        # third  = len(in_list) - first
        while second < third:
            b = in_list[second]
            if len(in_list) > 2:
                c = in_list[third]
            else:
                c = 0
            if a + b + c == 0:
                return True
            if a + b + c > 0:
                third -= 1
            if a + b + c < 0:
                first += 1
            else:
                second += 1
        return False


test_suite1 = [
    # call for default case from question
    { "inputs" : [[1,2,3]],
      "outputs" : False,
      "reason"  : "Will not sum to zero" },

    # call for default case from question
    {"inputs": [[1, 2, 3,0,0,0,0,0,0]],
     "outputs": True,
     "reason": "Technically, there are three integers that will sum to zero"},

    # call for second default case from question
    { "inputs"  : [[0,3,-3]],
      "outputs" : True,
      "reason"  : "Should Sum to Zero" },

    # call for second default case from question
    {"inputs": [[3, -3]],
     "outputs": True,
     "reason": "Should Sum to Zero"},

    # call for second default case from question
    {"inputs": [[0, 3, -3,0,0,0,0,0]],
     "outputs": True,
     "reason": "Should Sum to Zero"},

    # call for all values divisible by 3
    { "inputs"  : [[1,2,-3]],
      "outputs" : True,
      "reason"  : "Should Sum to Zero" },

    # call for all ones
    { "inputs"  : [[1,1,1]],
      "outputs" : False,
      "reason"  : "Will not Sum to Zero" },

    # call for all zeros
    { "inputs"  : [[0,2,-2,0,0,0,0,0]],
      "outputs" : True,
      "reason"  : "Should sum to zero" },

    # call for all values between 1 and 12
    { "inputs"  : [[1,-1,0]],
      "outputs" : True,
      "reason"  : "Should sum to zero" },

    # call for all values between 1 and 12
    {"inputs": [[1, -1,0,0,0,0,0,0,0]],
     "outputs": True,
     "reason": "Should sum to zero"},

    # call for all values between 1 and 12
    {"inputs": [[ 0, 0, 0, 0, 0, 0, 0]],
     "outputs": True,
     "reason": "Should sum to zero"},

    # call for all values between 1 and 12
    {"inputs": [[0]],
     "outputs": True,
     "reason": "Should sum to zero"},

    # call for all values between 1 and 12
    {"inputs": [[-3,3,3,-3]],
     "outputs": True,
     "reason": "Should sum to zero"},

    # call for all values between 1 and 12
    {"inputs": [[-1, -2, -3, 1, 2, 3]],
     "outputs": True,
     "reason": "Should sum to zero"},

    # call for all values divisible by 3
    {"inputs": [[1, 2, -3, 0]],
     "outputs": True,
     "reason": "Should Sum to Zero"},

    # call for all values divisible by 3
    {"inputs": [[1, 2, -3, 0,0]],
     "outputs": True,
     "reason": "Should Sum to Zero"},

    # call for all values divisible by 3
    {"inputs": [[1, 2, -3, 0,0,0]],
     "outputs": True,
     "reason": "Should Sum to Zero"}
]


for test in test_suite1:
    inputs = test["inputs"]
    result = ThreeSUM(inputs[0])
    if result != test["outputs"]:
        print("Testing fault: ThreeSUM() returned",result,"on inputs",inputs,
              "(",test["reason"],")")