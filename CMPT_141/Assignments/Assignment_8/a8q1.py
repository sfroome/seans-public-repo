# CMPT 141, Assignment 8, Question 1
# Written by Sean Froome



def between(num1, num2, num3):
    '''
    This function takes three numbers, and determines whether the first value (num1) is between the values of
    num2 and num3. For the function to return True, num1 must either be smaller than num3 and greater than num2, OR
    smaller than num2 but greater than num3. Otherwise, the function will return False.
    :param num1: an integer
    :param num2: an integer
    :param num3: an integer
    :return: True or False
    '''

    if num1 < num2 and num1 > num3:
        return True
    elif num1 > num2 and num1 < num3:
        return True
    else:
        return False

##############################################################################
# TEST DRIVER FOR between()                                                  #
##############################################################################

# Original Test Cases
# test1 = between(1,2,3)
# test2 = between(2,3,1)
# test3 = between(3,1,2)
# test4 = between(2,1,3)
# test5 = between(1,1,1)

# print(test1)
# print(test2)
# print(test3)
# print(test4)
# print(test5)


test_suite = [

    # call for num 1 < num 2 and num 1 < num 3
    { "input1"  : [1],
      "input2"  : [2],
      "input3"  : [3],
      "outputs" : False,
      "reason"  : "num 1 is smaller than both num 1 and num 3" },

    # call for num 1 between num 2 and num 3
    { "input1"  : [2],
      "input2"  : [3],
      "input3"  : [1],
      "outputs" : True,
      "reason"  : "num 1 is bigger than num3 and smaller than num 2" },

    # call for num 1 > num 2 and num 3
    { "input1"  : [3],
      "input2"  : [1],
      "input3"  : [2],
      "outputs" : False,
      "reason"  : "num 1 is bigger than both num 2 and num 3" },

    # call for num 1 < num 3, num 1 > num 2
    { "input1"  : [2],
      "input2"  : [1],
      "input3"  : [3],
      "outputs" : True,
      "reason"  : " num 1 is between num 2 and num 3 " },

    # call for num 1 < num 3, num 1 > num 2
    {"input1": [2],
     "input2": [3],
     "input3": [1],
     "outputs": True,
     "reason": " num 1 is between num 2 and num 3 "},

    # call for num 1 = num 2 = num3
    { "input1"  : [1],
      "input2"  : [1],
      "input3"  : [1],
      "outputs" : False,
      "reason"  : "Because num 1 is not between num 2 and num 3" },

    # call for tests with Negative numbers
    { "input1"  : [-1],
      "input2"  : [-2],
      "input3"  : [-3],
      "outputs" : False,
      "reason"  : "num 1 is biggest of the three" },

    # call for test with Negative and Positive numbers
    { "input1"  : [-1],
      "input2"  : [2],
      "input3"  : [-3],
      "outputs" : True,
      "reason"  : "num 1 is between num 2 and num 3" },

    # call for test with Negative and Positive numbers
    {"input1": [-3],
     "input2": [2],
     "input3": [-1],
     "outputs": False,
     "reason": "num 1 is smaller than both num 2 and num 3"}


]

for test in test_suite:
    input1 = test["input1"]
    input2 = test["input2"]
    input3 = test["input3"]
    result = between(input1[0], input2[0], input3[0])
    if result != test["outputs"]:
        print("Testing fault: between() returned",result,"on inputs",input1,
              "(",test["reason"],")")



def majority3(num_list):
    '''
    This function takes in a list of integers, determines how many are divisible by 3, and then returns either True or False
    depending on how many values in the list are divisible by 3. If more than half are divisible by 3, it will return True.
    Otherwise, it will return False.
    :param num_list: a List of integers
    :return: True or False
    '''

    number_divisible = 0
    for val in num_list:
        if val%3 == 0:
            number_divisible += 1
        else:
            number_divisible = number_divisible

    #print("The number of values divisible by 3 is",number_divisible)
    #print("length of list divided by 2 is", len(num_list)//2)
    #print("the length of num_list is", len(num_list))
    #check = number_divisible > len(num_list)//2
    #print("The idiot check says", check)

    if number_divisible > len(num_list)//2:
        return True
    else:
        return False

#############################################################################
# TEST DRIVER FOR majority3()                                               #
#############################################################################

# Original Test Cases
# Test1_List = [1,2,3,6]
# Test1 = majority3(Test1_List)
# print(Test1)

# Test2_List = [0,1,-3]
# Test2 = majority3(Test2_List)
# print(Test2)

# dictionary of test case suite to feed into test driver loop
test_suite1 = [
    # call for default case from question
    { "inputs"  : [[1,2,3,6]],
      "outputs" : False,
      "reason"  : "Only two values are divisible" },

    # call for second default case from question
    { "inputs"  : [[0,1,-3]],
      "outputs" : True,
      "reason"  : "Two values divisible by three (since zero is divisible b/c it leaves no remainder" },

    # call for all values divisible by 3
    { "inputs"  : [[3,6,9, 12, 15]],
      "outputs" : True,
      "reason"  : "All values divisible by 3" },

    # call for all ones
    { "inputs"  : [[1,1,1,1,1]],
      "outputs" : False,
      "reason"  : "No values divisible by 3" },

    # call for all zeros
    { "inputs"  : [[0,0,0,0,0]],
      "outputs" : True, #Technically true since 0/3 = 0, ie no remainder
      "reason"  : "All zero input" },

    # call for all values between 1 and 12
    { "inputs"  : [[1,2,3,4,5,6,7,8,9,10,11,12]],
      "outputs" : False,
      "reason"  : "Only Four values divisible out of 12" },

    # call for eight values, five of which are divisible
    { "inputs"  : [[3,6,9,12,15,1,2,4,5]],
      "outputs" : True,
      "reason"  : "Five out of eight values are divisible." },

    # call for eight values, four of which are divisible
    { "inputs": [[3, 6, 9, 12, 1, 2, 4, 5]],
     "outputs": False,
     "reason": "Only half (four) out of eight values are divisible."},

    # call for all threes
    {"inputs": [[3, 3, 3, 3, 3, 3, 3, 3]],
     "outputs": True,
     "reason": "Three should be divisible by itself"}
]


for test in test_suite1:
    inputs = test["inputs"]
    result = majority3(inputs[0])
    if result != test["outputs"]:
        print("Testing fault: majority3() returned",result,"on inputs",inputs,
              "(",test["reason"],")")


