# CMPT 141 - Testing, Tracing, Debugging
# Topic(s): Testing
# Assignment 8 Question 3


def ThreeSUM(in_list):
    """
    Determines whether or not there are three integers in the input list that sum to zero.
    :param : in_list a list of integers, any length
    :return: True if in_list contains three integers that sum to zero.
    """
    in_list.sort()

    for first in range(1,len(in_list) - 3):
        a = in_list[first]
        second = first + 1
        third = len(in_list) - 2
        while second < third:
            b = in_list[second]
            c = in_list[third]
            if a + b + c == 0:
                return True
            if a + b + c > 0:
                third -= 1
            else:
                second += 1
        return False


