# CMPT 141, Assignment 9, Question 1
# Written by Sean Froome

import numpy as np # Need numpy arrays
import random as rand # need to randomly generate numbers


def import_data():
    '''
    This function collects the battlefield data from a text file and returns it as a two dimensional grid.
    :param fname: None
    :return: data, a list of lists.
    '''
    # Read data in from file.
    print("Choose 1 for FistOfTheFirstMen, 2 for Hardhome, 3 for CastleBlack, or 4 for TheWall")
    print("The 3x3 example scenario from the notes will run otherwise.")
    f0 = input("Which Data file would you like to open? (ie: example.txt, etc)")
    if f0 == str(1):
        f1 = open('FistOfTheFirstMen.txt', 'r')
    elif f0 == str(2):
        f1 = open('Hardhome.txt', 'r')
    elif f0 == str(3):
        f1 = open('CastleBlack.txt', 'r')
    elif f0 == str(4):
        f1 = open('TheWall.txt', 'r')
    else:
        f1 = open('example.txt', 'r')

    data = [] # Make an empty list to read files into.
    for row in f1:
        line = [int(x) for x in row.rstrip().split(',')] # Make sure to splice everything properly.
        data.append(line) # Append each line of the file into the list named data.
    # print("The data file looks as follows:")
    # print(data)
    # print(" ")
    # Tested by checking console output.
    return data


def make_grid(data1):
    '''
    This function takes the N*N grid from the list of lists, and makes it into a numpy array
    :param data1: the battlefield data, is a list of lists.
    :return: grid_square, an N*N numpy array of the static defenses of the battlefield
    '''
    grid_square = np.array(data1[1:]) # Make a numpy array and take only the line of the battlefield.
    # Should take everything from data list EXCEPT the first row.

    # print("The array of the N*N battlefield looks as follows:")
    # print(grid_square)
    # print(" ")
    # Also tested by checking console output.
    return grid_square


def archer_grid(N, firepower):
    '''
    This function creates an N*N grid for the % of wight's destroyed by archers in each square.
    :param N: The length of one side of the square battlefield.
    :param firepower: The max % probability of wight's destroyed.
    :return: fire_grid, an N*N numpy array of the battlefield containing the % of wights
    that will be killed in each square.
    '''
    fire_grid = np.ones([N, N]) # Make a numpy array of ones.
    for i in range(N):
        for j in range(N):
            fire_grid[i][j] = rand.randint(0, firepower)

            # modify each square with a random value between 0 and the max value from the input file.

    # Below print statements not used except for debugging.
    # Impractical as it will output every single time this is called.
    # Meaning 100 times for every possible value of wights.
    # print("The grid for the archers looks as follows:")
    # print(fire_grid)
    # print(" ")

    return fire_grid


def battle(defense, archers, total_wights,N):
    '''
    This function calculates how many wights will be lost in the battle. This is where
    the majority of the battle simulation takes place.
    :param defense: the static defense array, an N*N numpy array
    :param archers: the archer's attack array, another N*N numpy array
    :param total_wights: the total number of wights being sent into battle.
    :param N: the length of one side of the square shaped battlefield. an integer.
    :return: battlefield, the result of the battle with the wights. An N*N numpy array.
    '''
    battlefield = np.ones([N, N]) # Once again make an array of ones.
    for i in range(0,1):
        for j in range(0,N,1):
            battlefield[i][j] = total_wights/N
            # Initially divide up the wights across the entire first row of the battlefield.
    for k in range(1, N, 1):
        for l in range(0,N,1):
            battlefield[k][l] = battlefield[k-1][l] - defense[k-1][l]
            battlefield[k][l] -= archers[k-1][l]/100*battlefield[k][l]
            # Need to remove the wights killed by static defenses first.
            # THEN remove the ones killed by archers.
            # k-1 because it's subtracting from the previous row's total.

            #print(defense[k][l])
            #print(archers[k][l]/100)
    for m in range(0,N,1):
        battlefield[k][m] -= defense[k][m]
        battlefield[k][m] -= archers[k][m]/100*battlefield[k][m]
        # Need to run one last time to account for the k-1.
        # aka this is the last volley from the fort.
        if battlefield[k][m] < 0:
            battlefield[k][m] = 0
    # print(battlefield)
    return battlefield


def survivors(battlefield_wights):
    """
    This function takes the numpy array from the battlefield, and simply sums the result of the last row to determine
    how many wights made it to the fortress.
    :param battlefield_wights: a numpy array containing the results from the battle simulation
    :return: remaining wights, an integer of the sum of the last row of the numpy array, aka
    the amount of wights that weren't destroyed.
    """
    remaining_wights = sum(battlefield_wights[len(battlefield_wights)-1])
    # Very simple, almost not necessary to make a function for it, but it sums the last row of the
    # battlefield array, giving a grand total number of surviving wights.
    return remaining_wights


def test_case(N1,archer_skill1,wight_subj1, static_defense1,Number_of_wights1):
    '''
    This function simply automates the entire process to determine the optimal number of wights that should be required.
    By default, it runs a simulation with a set number of wights 100 times.
    It's not exactly a test driver, but it does automatically run the program 100 times for a certain number of wights.
    :param N1: Size of one side of the battlefield.
    :param archer_skill1: number between 0 and 100 that determines max percent of wights that can be killed on a single square
    :param wight_subj1: Number of wights needed to take a fortress. Ie: Number needed to REACH a fort
    :param static_defense1: The static defenses on the battlefield. N*N numpy array.
    :param Number_of_wights1: Number of wights being sent into battle. An integer
    :return:
    '''
    test_data = [] # Empty list of results
    number_of_runs = 0 # Start with zero runs.
    success = []  # Make another empty list for a success
    while number_of_runs < 100: # Run for 100 tests.
        #power_grid = np.array([[4,0,2],[10,2,0],[6,20,15]]) # For testing, from example from handout.
        power_grid = archer_grid(N1, archer_skill1)  # Call the function to determine the archer's kills.
        the_battle = battle(static_defense1, power_grid, Number_of_wights1, N) # Run the battle simulation.
        surviving_wights = survivors(the_battle)  # Determine number of surviving wights.
        number_of_runs += 1 # Increase the number of runs by one.
        test_data.append(surviving_wights) # append the result to test data.
        # print("The number of surviving wights is:", surviving_wights)

    for successes in test_data:
        if successes > wight_subj1:# If it's greater than minimum needed to reach the fort
            success.append(successes) #Append it to the list.
            # print(len(success))
            # print(successes)
        # print("The number of surviving wights is:", surviving_wights)

    return len(success)/1  # Originally I had planned on running this for more than one hundred simulations.
    # length of list success is still percent, so this works.

d = import_data() # Get the data.
N = d[0][0] # Get the size of the battlefield.
archer_skill = d[0][1]  # The max percent of wights that can be killed in a single square.
wight_subj = d[0][2]  # Number of wights that must reach the fort to actually take it.
static_defense = make_grid(d)  # The NxN Battlefield grid (Static defense data included)

Number_of_wights = 1 # Start with a single wight. Gotta start with something.
percent_success = 0 # By default need it set to a low enough number to enter the while loop.
                    # Note to self. Google if do-while loops are a thing.

while percent_success < 95:
    # I added some additional conditions to increase the number of wights
    # Knowing that increasing the number of wights one at a time would take an eternity.
    # In a lot of ways, this was the toughest part to program, knowing that it's
    # far, far too easy to unintentionally to increase the number of wights by far too much.
    # In general though, it seems to work reasonably well.
    percent_success = test_case(N, archer_skill, wight_subj, static_defense, Number_of_wights)
    if percent_success < 65 and N >= 20:
        Number_of_wights *= 1.35
    elif percent_success < 50 and N > 20:
        Number_of_wights *= 5
    elif percent_success >= 50 and percent_success < 65 and N > 20:
        Number_of_wights *= 1.25
    elif percent_success >= 65 and percent_success < 85 and N > 20:
        Number_of_wights *= 1.05
    if percent_success < 15 and N > 50:
        Number_of_wights *= 100
    elif percent_success < 50 and N > 50:
        Number_of_wights *= 2
    elif percent_success < 85 and N > 50:
        Number_of_wights *= 1.018
    # print("The current chance of success is:", str(percent_success) + "%.")
    # print("The Current Number of Wights being sent into battle is:", Number_of_wights)
    # print(" ")
    if percent_success < 95:
        Number_of_wights += 1
        # This is probably made redundant by the above, but I keep it just to prevent an
        # unintentional infinite loop. At least it will eventually finish.

print("The chance of success is:", str(percent_success) + "%.")
print("The number of wights required to send into battle is:", Number_of_wights)
print("The number of wights required to successfully take the fort with a "
      "95 percent or greater chance of success is:", Number_of_wights)
if Number_of_wights <= 1000000:
    print("The Simulation is a success.")
    #chance_with_three_hundred_thousand = test_case(N,archer_skill,wight_subj,static_defense, 400000)
else:
    print("The simulation fails. You require more than one million wights to succeed.")
    #chance_with_one_million_wights = test_case(N,archer_skill,wight_subj,static_defense,1000000)
    #print("The chance of success with one million wights is:", str(chance_with_one_million_wights) + "%.")
    if(N >= 50):
        print("Hire an undead dragon to do the work for you.")

