import numpy as np

def read_scenario(filename):
    '''
    Read scenario information from a file.

    :param filename: Name of file containing scenario information.
    :return: dictionary with integer fields 'gridsize', 'archer strength',
             'wights needed', and a gridsize x gridsize array of
             fortification strengths in the field 'fortifications'.
    '''

    infile = open(filename, 'r')
    scenario = {}

    # Read data from the first line and unpack it into dictionary fields.
    first_line_data = [int(x) for x in infile.readline().rstrip().split(',')]
    scenario['gridsize'], scenario['archer strength'], scenario['wights needed'] = first_line_data

    # Read the remainder of the file (the fortifications for the grid squares) and
    # pack it into an array.
    scenario['fortifications'] = np.array([line.rstrip().split(',') for line in list(infile)]).astype(np.float)
    return scenario

def run_simulation(scenario, starting_wights_per_column):
    '''
    Run a single battle simulation.

    :param scenario: The scenario information as returned by read_scenario()
    :param starting_wights_per_column:  Number of wights to start in *each* column
    :return: True if the wights captured the fortress, False otherwise.
    '''
    # initialize an array to keep track of how many wights
    # get to each grid square.
    wights = np.zeros([scenario['gridsize']+1, scenario['gridsize']])
    wights[0,:] = starting_wights_per_column

    # Determine random percentages of wights destroyed by fire arrows
    # generate random percentages of wights destroyed by fire arrows based on the distance from fortress

    N = scenario['gridsize']
    p = scenario['archer strength']
    random_pct = np.zeros((N, N))

    # Use the highest percentage of the row above as the lowest percentage of the current row
    # low_p = 0
    # for i in range(N):
    #     random_pct[i, :] = np.random.randint(low_p, p // (N - i) + 1, N)
    #     low_p = p // (N - i)

    # Use 0 as the lowest percentage of the current row
    for i in range(0, N):

        pct_high = p - ((N - 1) - i) if ((N - 1) - i) < p else 0  # if distance is larger than p, then percentage is 0
        random_pct[i, :] = np.random.randint(0, pct_high + 1, N)  # +1 is needed is because numpy randint(
        # low, high, size), high is excluded.

        # or
        # random_pct[i, :] = np.random.randint(0, max(0, p - ((N - 1) - i)) + 1, N)

    random_pct = random_pct / 100


    # Perform simulation

    # For each row compute the new number of wights.
    for row in range(0, scenario['gridsize']):
        # subtract wights that are repelled by fortifications
        wights[row+1, :] = (wights[row,:] - scenario['fortifications'][row,:])

        # subtract wights that are destroyed by fire arrows
        temp = wights[row+1, :]
        temp[temp<0] = 0  # if the numbers are already negative, then replace it with zeros
        wights[row+1, :] = wights[row+1, :] - (temp * random_pct[row, :])

    # Replace negative numbers of wights with zero.
    wights[wights < 0] = 0
    return np.sum(wights[-1,:]) >= scenario['wights needed']

def success_probability(scenario, starting_wights, number_of_simulations):
    '''
    Determine the probability of success when attacking with a *specific*
    number of wights by running multiple simulations.

    :param scenario:  Scenario information as returned by read_scenario()
    :param starting_wights: *Total* number of starting wights.
    :param number_of_simulations:  Number of simulations to run.
    :return: Percentage of the number_of_simulations simulations for which
             the wights were successful (expressed as a float between 0.0 and 1.0)
    '''
    # For recording number of successes
    successes = 0

    # Run the required number of simulations
    for i in range(number_of_simulations):
        # If it was successful, record it.
        if run_simulation(scenario, starting_wights / scenario['gridsize']):
            successes += 1

    # Return the percentage of successful simulations.
    return successes / number_of_simulations




def minimum_wights(scenario):
    '''
    Determine the minimum number of wights needed to capture the fortress
    for a given scenario.
    :param scenario: The scenario infomration as returned by read_scenario()
    :return: The estimated minimum number of wights required.
    '''

    # Since we know the Night King can muster at most 1,000,000 wights,
    # we know that the minimum number of wights is somehwere between 1 and
    # one million.

    # do a binary search until abs(low-high) <= 10
    high = 1000000
    low = 1

    while abs(low-high) > 10:

        # Determine success probability for the number of wights
        # halfway between low and high.
        mid = (high+low)//2
        prob = success_probability(scenario, mid, 1000)

        # If the probability was more than 95%, then the minimum
        # number of wights needed, then our guess was too large.
        # Eliminate larger guesses high to mid and keep searching.
        # set high to mid.
        if prob >= .95:
            high = mid
        # Otherwise our guess was too low; eliminate smaller guesses
        # by setting low to mid and keep searching.
        else:
            low = mid

    print('Linear search starts....')
    # Once the difference between low and high is
    # less than 200, do a linear search of the remaining range.
    num_wights = low
    while success_probability(scenario, num_wights, 1000) < 0.95 and num_wights <= high:
        num_wights+=1

    return num_wights



######## Main Program ########


# Information about scenarios.
fortresses = [
    {
        'filename': 'FistOfTheFirstMen.txt',
        'description': 'The Fist of the First Men'
    },
    {
        'filename': 'Hardhome.txt',
        'description': 'Hardhome, Stronghold of the Free Folk'
    },
    {
        'filename': 'CastleBlack.txt',
        'description': 'Castle Black, A Storm of the Swords'
    },
    {
        'filename': 'TheWall.txt',
        'description': 'The Wall, The Last Defence from the Others'
    }
]


# For each scenario, run it.
for f in fortresses:
    scenario = read_scenario(f['filename'])
    print('Minimum number of wights needed to capture', f['description'], 'is:',
          minimum_wights(scenario))