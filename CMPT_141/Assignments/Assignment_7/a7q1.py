# CMPT 141, Assignment 7, Question 1
# Written by Sean Froome


def spaceTime(distance_travelled):
    '''
    This function recursively calculates the time required for Team Rockets spaceship to travel the distance given to it.
    :param distance_travelled: the distance the spaceship is going to travel
    :return: the time required for the ship to travel the required distance.
    '''
    # If the distance remaining (ie: base case) is less than or equal to 1m it will take a minute
    if distance_travelled <= 1.0:
        return 1.0 #minutes
    else: #Otherwise, divide the distance by half, call the function again and add one minute to travel time.
        return 1.0 + spaceTime(distance_travelled/2)

User_input_distance = input("How far would you like to travel (in meteres) on Team Rocket's Spaceship today?")
time_required_user = spaceTime(float(User_input_distance))
print("It will take", time_required_user, "minutes to complete your journey.")

#Test Examples

#Pokestop
time_required_PokeStop = spaceTime(37)
print("For a trip to the nearest pokestop it will take", time_required_PokeStop, "minutes.")

#Trip around the Earth
time_required_Earth = spaceTime(40075000)
print("For a trip around the Earth it will take", time_required_Earth, "minutes.")

#Trip to the Sun
time_required_Sun = spaceTime(1.49e11)
print("For a trip to the Sun it will take", time_required_Sun, "minutes.")

#Trip to the nearest star
time_required_StarSystem = spaceTime(4.0e16)
print("For a trip to the next nearest star it will take", time_required_StarSystem, "minutes.")

#Trip to the edge of the Universe
time_required_Universe = spaceTime(8.8e26)
print("For a trip to the edge of the observable universe it will take", time_required_Universe, "minutes.")




