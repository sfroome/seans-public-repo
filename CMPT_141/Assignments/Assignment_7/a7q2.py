# CMPT 141, Assignment 7, Question 2
# Written by Sean Froome

import matplotlib.pyplot as plot


def pop_growth(P1, n, r, K):
    '''
    This function recursively calculates the population from year 1 to year n
    :param P1: Population in year 1.
    :param n: The last year required to be calculated
    :param r: rate of population growth
    :param K: max population
    :return: the population P2.
    '''

    if n == 1:
        P2 = P1 + r*P1*(1-P1/K)
        return P2
    else:
        P2 = P1 + r*P1*(1-P1/K)
        print( "Population size:", P2)
        return pop_growth(P2, n-1, r, K)


# Test Case 1
r0 = 0.1
K0 = 10000
P0 = 600
n0 = 75

test_case = pop_growth(P0, n0, r0, K0)
print(test_case)

#Due to the fact that the population is calculated in reverse order, if I plot it within the recursive function, the
#graph is mirrored (year 1 is year n, year 2 is year n-1, etc.) While horribly inefficient, the for loop calls the function
# and handles the graphing specifically. Of course this means the function is called n^2 times.
for i in range(1,n0):
    plot.plot(i,pop_growth(P0, i, r0, K0), 'xr')
plot.show()

# Population of 9.9 Billion
r9 = 0.11
P9 = 7e9
K9 = 10e9
n9 = 35

Pop_9pt9Bill = pop_growth(P9, n9, r9, K9)
print(Pop_9pt9Bill)
for j in range(1,n9):
    plot.plot(j,pop_growth(P9, j, r9, K9), 'xr')
    print("Year:", j)
plot.show()

