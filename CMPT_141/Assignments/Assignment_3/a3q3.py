# CMPT 141 Assignment 3, Question 3
# Written by Sean Froome


points_player_1 = 0
points_player_2 = 0
round = 1
#for round in range(1,6,1):
while points_player_1 < 3 and points_player_2 < 3:
    print("Round Number:", round)
    round += 1
    player_1 = input("(P1) How many fingers are you going to play?")
    player_2 = input("(P2) How many fingers are you going to play?")
    player_1_guess = input("(P1) What is your guess at the sum?")
    player_2_guess = input("(P2) What is your guess at the sum?")
    sum = int(player_1) + int(player_2)
    if sum == int(player_1_guess):
        points_player_1 += 1
        print("You guessed correctly, Player 1 gets a point.")
    if sum == int(player_2_guess):
        points_player_2 += 1
        print("You guessed correctly, Player 2 gets a point.")

    if points_player_1 == 2 and points_player_2 != 3:
        print("Player 1 now has two points. Player 1 only needs one more point to win.")
    elif points_player_2 == 2 and points_player_1 != 3:
        print("Player 2 now has two points. Player 2 only needs one more point to win.")
    elif points_player_1 == 3 and points_player_2 == 0:
        print("Player 1 wins a glorious victory!")
    elif points_player_2 == 3 and points_player_1 == 0:
        print("Player 2 wins a glorious victory!")
    elif points_player_1 == 3 and points_player_2 != 3:
        print("Player 1 wins!")
    elif points_player_2 == 3 and points_player_1 != 3:
            print("Player 2 wins!")
    elif points_player_1 == 3 and points_player_1 == points_player_2:
        print("It's a tie!")
print("The final score was Player 1:", points_player_1, "to Player 2:", points_player_2)