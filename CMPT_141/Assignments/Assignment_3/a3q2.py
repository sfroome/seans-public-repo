# CMPT 141 Assignment 3, Question 2
# Written by Sean Froome

# Supply S(P) = 500 + 90*P
# Demand D(P) = 10000 - 35*P

import matplotlib.pyplot as plot

def supply(P):

    '''
Calculates the Supply Function
P: Price of product
Returns: The Supply Function
    '''

    Supply_Function = 500 + 90*P
    return Supply_Function

def demand(P):


    '''
Calculates the Demand Function
P: Price of product
Returns: The Demand Function
    '''

    Demand_Function = 10000 - 35*P
    return Demand_Function


optimal_price = 0

for P in range(10, 161, 1):
    SP = supply(P)
    DP = demand(P)
    #print(str(P), DP, SP)
    plot.plot(P, SP, 'og')
    plot.plot(P, DP, 'ob')
    if DP == SP:
        optimal_price = P
        plot.plot(P, SP, 'ro')


print("The optimal selling price is $" + str(optimal_price), "which will result in the sale of", supply(optimal_price), "copies of the game.")
#plot.figure()
plot.xlim(0, 170)
plot.ylim(0, 16000)
plot.xlabel("Price in Dollars")
plot.ylabel("Quantity of Product")
plot.title("Supply and Demand Functions")
plot.annotate('Optimal Price', xy=(optimal_price, supply(optimal_price)))
plot.show()

