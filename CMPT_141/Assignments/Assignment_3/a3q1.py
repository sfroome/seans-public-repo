# CMPT141: Assignment 3 Question 1
# Modified by Sean Froome
# Dragons! (and turtle graphics)

# Python modules
import turtle as turtle

# Module supplied by CMPT 141
import makedragon as dragonsegg
import random as random

def draw(dragon, x, y, segsize, heading):
    """
    Given a string describing a dragon, use Turtle graphics to
    draw its shape.
    :param dragon:  A string that describes the shape ofthe dragon.
    :param x, y: the Turtle coordinates to start drawing the dragon
    :param segsize: How big each segment of the dragon is, in Turtle pixels.
    :param heading: the orientation of the first segment of the Dragon
    :return: None
    """
    turtle.up()
    turtle.goto(x, y)
    turtle.down()
    turtle.setheading(heading)

    # ADD CODE TO THIS FUNCTION
    # this is where you draw the dragon!
    # use a loop!

    for x in range(0, len(dragon)):
        random_1 = random.random()
        random_2 = random.random()
        random_3 = random.random()
        if str(dragon[x]) == 'F':
            turtle.fd(25)
        elif str(dragon[x]) == 'R':
            turtle.right(90)
            turtle.pencolor(random_1, random_2, random_3)
        elif str(dragon[x]) == 'L':
            turtle.left(90)

            turtle.pencolor(random_1, random_2, random_3)
        else:
            turtle.fd(25)
        # It shouldn't ever enter this state, but it's a default behaviour

###############################################################
# main program
###############################################################

# set up the turtle to draw as fast as possible
turtle.shape("turtle")
turtle.delay(1)
turtle.speed(10)
#turtle.hideturtle()

# A dragon's shape is defined by its level
# Try different values here.
# Don't try anything above 20 here!
level = 4


# Now we can decide how big the dragon should be
# from nose to tail in screen pixels
# 500 pixels fills my default turtle window nicely
# Adjust to taste!
dragon_size = 500


# The length of a dragon segment is determined by
# its level, and its size
segment_size = dragon_size / (2 ** (level / 2))


# to make low level dragons colourful
# use a wider pen for smaller dragons
turtle.pensize(15 // level)


# To draw a dragon from upper left to lower right
# we have to set the turtle to the appropriate direction
# Play with this value to orient your dragon differently!
heading = 45 * (level - 1)


# make a dragon of the desired level
my_dragon = dragonsegg.dragon(level)


# now at long last, draw the dragon!
# starting a little to the left and a little up
# adjust to taste!
draw(my_dragon, -140, 140, segment_size, heading)

# tell the turtle we're done!
turtle.done()

