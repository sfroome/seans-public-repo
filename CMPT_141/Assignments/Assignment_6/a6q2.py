# CMPT 141, Assignment 6, Question 2
# Written by Sean Froome (minus the sample code)


import numpy as np

######### PROVIDED FUNCTION, DO NOT MODIFY ############################################
def read_field(fname):
    '''
    Read data from a battlefield file.
    :param fname: The filename of the file containing battlefield data.
    :return: A 2D array of integers with penguin counts for each battlefield position.
    '''
    f = open(fname, 'r')
    data = []
    for row in f:
        line = [int(x) for x in row.rstrip().split(',')]
        data.append(line)
    return np.array(data)
########################################################################################

#
# TODO: Write the find_target_location function here.
#
def find_target_location(battlefield, fish_weight):
    '''
    This function determines the optimal target location to launch fish that will affect the maximum number of penguins.
    :param battlefield: The grid that indicates where the penguins are and how many. (A 2-D array of integers)
    :param fish_weight: A single integer, indicating the weight of fish to be launched.
    :return: the coordinates of the optimal target location.
    '''
    optimal_count = 0
    fish_weight_array = np.zeros([2*fish_weight+1,2*fish_weight+1], dtype= int)
    shape_fw = np.shape(fish_weight_array)
    shape_bf = np.shape(battlefield)
    if shape_bf < shape_fw:
        return "None"
    for i in range(0, battlefield.shape[0], 1):
        for j in range(0, battlefield.shape[1], 1):
            array_of_explosion = np.array(battlefield[i:2*fish_weight+1+i, j:2*fish_weight+1+j])
            # Not really sure if this technically counts as checking the out of bounds area or not....
            # If statement verifies the array of the explosion is in fact within bounds of the explosion
            # or else nothing happens and the out of bounds explosion array isn't used.
            if np.shape(array_of_explosion) == np.shape(fish_weight_array):
                number_of_penguins_fished = sum(sum(array_of_explosion))
                if number_of_penguins_fished > optimal_count:
                    optimal_count = number_of_penguins_fished
                    optimal_coordinate = (i+fish_weight, j+fish_weight)
    print("The number of penguins that will be affected is", str(optimal_count) + ".")
    return optimal_coordinate

#
# TODO: Test your function by calling it here using the arrays constructed by read_field().
# Consult the assignment description for expected results.

field1 = read_field('field1.csv')
field2 = read_field('field2.csv')

best_spot_field_1 = find_target_location(field1, 1)
best_spot_field_2 = find_target_location(field2, 4)

print("The best target location that will affect the maximum number of penguins is", str(best_spot_field_1) + ".")
print("The best target location that will affect the maximum number of penguins is", str(best_spot_field_2) + ".")
