# CMPT 141, Assignment 6, Question 1
# Written by Sean Froome (minus the sample code)

import numpy as np
import csv

# put the csv file in the same folder as your program
f = open('age_statistics.csv', 'r')
csvreader = csv.reader(f, delimiter=',')
data = []
for row in csvreader:
    row1 = [item.replace(',', '') for item in row]  # This is used to remove the thousand separator , in each row
    data.append(row1)

data1 = data[9:len(data)]

print("data1 array is as follows:")
print(data1)

row_age_dct = {}
for i in range(0, len(data1),1):
    for j in range (0, len(data1[i]), 1):
        if j == 0:
            row_age_dct[i] = data1[i][0]

data2 = data1
for row in data2:
    del row[0]

data2 = [[int(j) for j in i] for i in data2]
data_array = np.array(data2)
print("data2 array is as follows:")
print(data2)

print("row_age_dct is as follows:")
print(row_age_dct)


print("data_array is as follows:")
print(data_array)

print("data_array has", str(data_array.ndim) + " dimensions.")
print("data_array's shape is", str(data_array.shape) + ".")
print("data_array has", str(data_array.size) + " elements.")
print("The elements in data_array are of type", str(data_array.dtype) + ".")

print("The total population in year 2013 is", np.sum(data_array[0:21,0]))
print("The total population in year 2014 is", np.sum(data_array[:,1]))
print("The total population in year 2015 is", np.sum(data_array[:,2]))
print("The total population in year 2016 is", np.sum(data_array[:,3]))
print("The total population in year 2017 is", np.sum(data_array[:,4]))


def percentage_change(population_data, row_index):
    '''
    This function takes in two elements, and determines the year over year population change
    of a given age group, and returns an array with that information.
    :param population_data: the complete array of population data.
    :param row_index: the index that determines which age group's population change should be calculated.
    :return: year_change_array, a 1-D array of percent year over year population chaange data
    '''

    year_over_year_percent_change_2014 = (population_data[row_index,1] -
                                          population_data[row_index,0])/population_data[row_index,0]*100

    year_over_year_percent_change_2015 = (population_data[row_index,2] -
                                          population_data[row_index,1])/population_data[row_index,1]*100

    year_over_year_percent_change_2016 = (population_data[row_index,3] -
                                          population_data[row_index,2])/population_data[row_index,2]*100

    year_over_year_percent_change_2017 = (population_data[row_index,4] -
                                          population_data[row_index,3])/population_data[row_index,3]*100

    year_change_array = np.array([year_over_year_percent_change_2014, year_over_year_percent_change_2015,
                                  year_over_year_percent_change_2016, year_over_year_percent_change_2017])
    return year_change_array


print("The percentage change for the age group of",  row_age_dct[0], "is:", percentage_change(data_array, 0))
print("The percentage change for the age group of", row_age_dct[10], "is:",percentage_change(data_array, 10))
print("The percentage change for the age group of", row_age_dct[19], "is:", percentage_change(data_array, 19))
print("The percentage change for the age group of", row_age_dct[20], "is:", percentage_change(data_array, 20))


largest_percent_change = 0

for key in row_age_dct:
    percentage_changes = percentage_change(data_array, key)
    for changes in percentage_changes:
        if abs(largest_percent_change) < abs(changes):
            largest_percent_change = changes
            key_of_relevance = key

print("The age group with the largest year over year percent change was for the age group", str(row_age_dct[key_of_relevance]) + ".")
