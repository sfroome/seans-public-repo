# CMPT 141 AS5Q6
# Written by: Sean Froome


def read_response_file(File_Input):
    '''
    This function simply reads a file and converts the strings into a list of lists.
    :param File_Input: the file input being read
    :returns lists, aka my list of lists
    '''

    file = open(File_Input, 'r')
    lists = []

    for line in file:
        new_line = line.rstrip('\n')
        sublist = new_line.split(',')
        print(sublist)
        lists.append(sublist)
    print(lists)
    file.close()
    return lists


print('Testing read_response_file')
data = read_response_file('cmpt181_midterm.txt')
print(data[0:3])