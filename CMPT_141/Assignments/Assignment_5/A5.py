# CMPT 141 AS5Q9
# Written by: Sean Froome
# See Docstrings of each function for what this does exactly but
# the main purpose of this program is to take student test data including
# ID number, Name, and responses, grades it, outputs the class' statistical data
# such as the mean, and also saves the scores to a text file.

import statistics as stat


def index_responses(responses):
    '''
    given a list of responses(that are strings), converts the list to a dictionary
    and ties the response to a question number.
    param responses: A list of strings (test responses)
    returns: question_response_pairs, aka a dictionary with question number
    associated with a multiple choice answer value.
    '''

    count = []
    for i in range(1, len(responses)+1, 1):
        count.append("Q" + str(i))
    question_response_pairs = dict(zip(count, responses))
    return question_response_pairs


def index_student(student_data_list):
    '''
    given a list of responses, names, and ID numbers,(that are strings), converts the list to a dictionary.
    Each function call converts the list to a dictionary for a single student.
    param student_data_list: A list of strings (test responses, student ID numbers, and student name)
    returns: student_data_dictionary, aka a dictionary with question number
    associated with a multiple choice answer value, student name with  key 'name', and ID number with key 'ID'
    for a single student.
    '''

    list_of_responses = student_data_list[2:len(student_data_list)]
    student_responses_values = index_responses(list_of_responses)
    student_data_dictionary = {'ID': student_data_list[0], 'Name' : student_data_list[1],'Responses': student_responses_values }
    return student_data_dictionary


def index_class(list_of_list_of_responses):
    '''
    given a list of lists containing a student ID, student name and test responses, this function converts the list into
    a dictionary that can be searched.
    param list_of_list_of_responses, a list of list of student data (strings)
    returns data_base_of_responses a dictionary with question number associated with a multiple choice answer value,
    student name with  key 'name', and ID number with key 'ID', but for more than one student.
    '''

    database_of_responses = {}
    for i in range(0,len(list_of_list_of_responses),1):
        single_student_value = index_student(list_of_list_of_responses[i])
        database_of_responses[list_of_list_of_responses[i][0]] = single_student_value
    return database_of_responses


def grade_student(correct_answers, student_answers):
    '''
    Given a list of correct answers, and a list of student answers, this function compares the two,
    and outputs the number of reponses for a single student.
    param correct_answers a list of strings containing the correct answers
    param student_answers a list of strings containing the student's responses
    returns Number_Correct, an integer of the correct number of responses for the student.
    '''

    Number_Correct = 0
    for key in correct_answers:
        if correct_answers[key] == student_answers[key]:
            Number_Correct += 1
        else:
            Number_Correct = Number_Correct
    return Number_Correct


def grade(correct_answers, student_information):
    '''
    This function takes a database of student test data, and a correct list of answers and adds the test score to the
    database.
    param correct_answers
    param student_information the dictionary database containing student name, ID #, and test responses
    returns nothing
    '''
    for key in student_information:
        number_correct = grade_student(correct_answers, student_information[key]['Responses'])
        student_information[key]['Score'] = number_correct


def read_response_file(file_input):
    '''
    This function simply reads a file and converts the strings into a list of lists.
    :param file_input: the file input being read
    :returns lists, aka my list of lists
    '''

    file = open(file_input, 'r')
    lists = []
    for line in file:
        new_line = line.rstrip('\n')
        sublist = new_line.split(',')
        lists.append(sublist)
    file.close()
    return lists


def write_score_file(testfile, database):
    '''
    This function takes a database of student data, and writes it to disk in a text file.
    param testfile aka the file that will be written to
    param database aka the data to be used to write the file
    returns nothing
    '''

    new_list = []
    for i in database:
        sub_list = []
        sub_list.append(database[i]['ID'])
        sub_list.append(database[i]['Name'])
        sub_list.append(database[i]['Score'])
        new_list.append(sub_list)
    f = open(testfile, 'w')

    for j in range(0, len(new_list),1):
        new_string = new_list[j][0] + "," + new_list[j][1]+ ","+ str(new_list[j][2])
        f.write(new_string)
        f.write('\n')
    f.close()


def print_scores_report(number_of_questions, student_scores):
    '''
    This function given the inputs, outputs some statistics about the grade data
    param number_of_questions: The number of test questions (an integer)
    param student_scores: a list of student scores (a list of integers)
    returns Nothing
    '''

    print("*****Scores summary*****")
    print("Number of students:", len(student_scores))
    print("Number of questions:", number_of_questions)
    print("Minimum score:", min(student_scores))
    print("Average score:", stat.mean(student_scores))
    print("Median score:", stat.median(student_scores))
    print("Maximum score:", max(student_scores))
    students_above_80 = [x for x in scores if x/number_of_questions > 0.8]
    students_below_40 = [x for x in scores if x/number_of_questions < 0.4]
    print("There were", len(students_above_80),"students who received a grade above 80%.")
    print("There were", len(students_below_40),"students who received a grade below 40%.")


data = read_response_file('cmpt181_midterm.txt')
answers = index_responses(data[0][2:len(data[0])])
response_db = index_class(data)
grade(answers,response_db)
scores = [response_db[i]['Score'] for i in response_db]
scores.remove(scores[0])  # Because the first entry is the answer key itself and not a student
print_scores_report(30, scores)
write_score_file('score_file_A5.txt', response_db)
