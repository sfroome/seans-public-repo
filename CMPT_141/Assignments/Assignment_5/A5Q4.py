# CMPT 141 AS5Q4
# Written by: Sean Froome


def index_responses(responses):
    '''
    given a list of responses(that are strings), converts the list to a dictionary
    and ties the response to a question number.
    param responses: A list of strings (test responses)
    returns: question_response_pairs, aka a dictionary with question number
    associated with a multiple choice answer value.
    '''

    count = []
    for i in range(1, len(responses)+1, 1):
        count.append("Q" + str(i))
        #print(count)
    question_response_pairs = dict(zip(count, responses))
    return question_response_pairs


def grade_student(correct_answers, student_answers):
    '''
    Given a list of correct answers, and a list of student answers, this function compares the two,
    and outputs the number of reponses for a single student.
    param correct_answers a list of strings containing the correct answers
    param student_answers a list of strings containing the student's responses
    returns Number_Correct, an integer of the correct number of responses for the student.
    '''

    Number_Correct = 0
    for key in correct_answers:
        if correct_answers[key] == student_answers[key]:
            Number_Correct +=1
        else:
            Number_Correct = Number_Correct
    return Number_Correct

print('Testing grade_student')
answers = index_responses(['a','b','c'])
resp1 = index_responses(['a','b','b'])
resp2 = index_responses(['a','b','c'])
print('Correct responses for first example:', grade_student(answers,resp1))
print('Correct responses for second example:', grade_student(answers,resp2))