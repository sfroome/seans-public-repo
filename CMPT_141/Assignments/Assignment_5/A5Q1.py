# CMPT 141 AS5Q1
# Written by: Sean Froome

def index_responses(responses):
    '''
    given a list of responses(that are strings), converts the list to a dictionary
    and ties the response to a question number.
    param responses: A list of strings (test responses)
    returns: question_response_pairs, aka a dictionary with question number
    associated with a multiple choice answer value.
    '''

    count = []
    for i in range(1, len(responses)+1, 1):
        count.append("Q" + str(i))
        #print(count)
    question_response_pairs = dict(zip(count, responses))
    return question_response_pairs


print ('Testing index_responses () ')
print ( index_responses ([ 'a', 'b', 'c']))
print ( index_responses ([ 'a', 'a', 'c', 'b']))
print ( index_responses ([ 'd', 'd', 'b', 'e', 'e', 'e', 'd', 'a']))