# CMPT 141 AS5Q7
# Written by: Sean Froome


def index_responses(responses):
    '''
    given a list of responses(that are strings), converts the list to a dictionary
    and ties the response to a question number.
    param responses: A list of strings (test responses)
    returns: question_response_pairs, aka a dictionary with question number
    associated with a multiple choice answer value.
    '''

    count = []
    for i in range(1, len(responses)+1, 1):
        count.append("Q" + str(i))
    question_response_pairs = dict(zip(count, responses))
    return question_response_pairs


def index_student(ze_data_as_a_list):
    '''
    given a list of responses, names, and ID numbers,(that are strings), converts the list to a dictionary.
    Each function call converts the list to a dictionary for a single student.
    param ze_data_as_a_list: A list of strings (test responses, student ID numbers, and student name)
    returns: ze_data_as_a_dictionary, aka a dictionary with question number
    associated with a multiple choice answer value, student name with  key 'name', and ID number with key 'ID'
    for a single student.
    '''

    list_of_responses = ze_data_as_a_list[2:len(ze_data_as_a_list)]
    student_responses_values = index_responses(list_of_responses)
    ze_data_as_a_dictionary = {'ID': ze_data_as_a_list[0], 'Name' : ze_data_as_a_list[1],'Responses': student_responses_values }
    return ze_data_as_a_dictionary


def index_class(list_of_list_of_responses):
    '''
    given a list of lists containing a student ID, student name and test responses, this function converts the list into
    a dictionary that can be searched.
    param list_of_list_of_responses, a list of list of student data (strings)
    returns data_base_of_responses a dictionary with question number associated with a multiple choice answer value,
    student name with  key 'name', and ID number with key 'ID', but for more than one student.
    '''

    database_of_responses = {}
    for i in range(0,len(list_of_list_of_responses),1):
        single_student_value = index_student(list_of_list_of_responses[i])
        database_of_responses[list_of_list_of_responses[i][0]] = single_student_value
    return database_of_responses


def grade_student(correct_answers, student_answers):
    '''
    Given a list of correct answers, and a list of student answers, this function compares the two,
    and outputs the number of reponses for a single student.
    param correct_answers a list of strings containing the correct answers
    param student_answers a list of strings containing the student's responses
    returns Number_Correct, an integer of the correct number of responses for the student.
    '''

    Number_Correct = 0
    for key in correct_answers:
        if correct_answers[key] == student_answers[key]:
            Number_Correct +=1
        else:
            Number_Correct = Number_Correct
    return Number_Correct


def grade(correct_answers, student_information):
    '''
    This function takes a database of student test data, and a correct list of answers and adds the test score to the
    database.
    param correct_answers
    param student_information the dictionary database containing student name, ID #, and test responses
    returns nothing
    '''
    for key in student_information:
        # print(student_information[key]['Responses'])
        Number_Correct = grade_student(correct_answers, student_information[key]['Responses'])
        student_information[key]['Score'] = Number_Correct
        # print(Number_Correct)
    return 0


def write_score_file(testfile, database):
    '''
    This function takes a database of student data, and writes it to disk in a text file.
    param testfile aka the file that will be written to
    param database aka the data to be used to write the file
    returns nothing
    '''

    new_list = []

    for i in database:
        sub_list = []
        sub_list.append(database[i]['ID'])
        sub_list.append(database[i]['Name'])
        sub_list.append(database[i]['Score'])
        new_list.append(sub_list)
    #print(new_list)

    f = open(testfile, 'w')

    for j in range(0, len(new_list),1):
        new_string = new_list[j][0] + "," +  new_list[j][1]+ ","+ str(new_list[j][2])
        print(new_string)
        f.write(new_string)
        f.write('\n')
    f.close()
    return 0

print('Testing write_score_file')
answers = index_responses(['a','b','c','b'])
response_db = index_class([['123','foo', 'a','b','c','a'],
                           ['234','bar', 'a','b','c','b'],
                           ['345','xyzzy','a','a','c','b']])
grade(answers,response_db)
write_score_file('score_file_example.txt', response_db)