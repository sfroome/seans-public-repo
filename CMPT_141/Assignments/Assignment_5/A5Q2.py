# CMPT 141 AS5Q2
# Written by: Sean Froome


def index_responses(responses):
    '''
    given a list of responses(that are strings), converts the list to a dictionary
    and ties the response to a question number.
    param responses: A list of strings (test responses)
    returns: question_response_pairs, aka a dictionary with question number
    associated with a multiple choice answer value.
    '''

    count = []
    for i in range(1, len(responses)+1, 1):
        count.append("Q" + str(i))
        #print(count)
    question_response_pairs = dict(zip(count, responses))
    return question_response_pairs


def index_student(ze_data_as_a_list):
    '''
    given a list of responses, names, and ID numbers,(that are strings), converts the list to a dictionary.
    Each function call converts the list to a dictionary for a single student.
    param ze_data_as_a_list: A list of strings (test responses, student ID numbers, and student name)
    returns: ze_data_as_a_dictionary, aka a dictionary with question number
    associated with a multiple choice answer value, student name with  key 'name', and ID number with key 'ID'
    for a single student.
    '''

    list_of_responses = ze_data_as_a_list[2:len(ze_data_as_a_list)]
    student_responses_values = index_responses(list_of_responses)
    ze_data_as_a_dictionary = {'ID': ze_data_as_a_list[0], 'Name' : ze_data_as_a_list[1],'Responses': student_responses_values }
    return ze_data_as_a_dictionary


print('Testing index_student()')
print(index_student(['345','xyzzy','a','a','c','b']))
print(index_student(['10021795','Samden Cross', 'd','d','b','e','e','e','d','a']))