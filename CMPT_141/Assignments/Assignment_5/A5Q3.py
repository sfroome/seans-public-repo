# CMPT 141 AS5Q3
# Written by: Sean Froome


def index_responses(responses):
    '''
    given a list of responses(that are strings), converts the list to a dictionary
    and ties the response to a question number.
    param responses: A list of strings (test responses)
    returns: question_response_pairs, aka a dictionary with question number
    associated with a multiple choice answer value.
    '''

    count = []
    for i in range(1, len(responses)+1, 1):
        count.append("Q" + str(i))
    question_response_pairs = dict(zip(count, responses))
    return question_response_pairs


def index_student(ze_data_as_a_list):
    '''
    given a list of responses, names, and ID numbers,(that are strings), converts the list to a dictionary.
    Each function call converts the list to a dictionary for a single student.
    param ze_data_as_a_list: A list of strings (test responses, student ID numbers, and student name)
    returns: ze_data_as_a_dictionary, aka a dictionary with question number
    associated with a multiple choice answer value, student name with  key 'name', and ID number with key 'ID'
    for a single student.
    '''

    list_of_responses = ze_data_as_a_list[2:len(ze_data_as_a_list)]
    student_responses_values = index_responses(list_of_responses)
    ze_data_as_a_dictionary = {'ID': ze_data_as_a_list[0], 'Name' : ze_data_as_a_list[1],'Responses': student_responses_values }
    return ze_data_as_a_dictionary


def index_class(list_of_list_of_responses):
    '''
    given a list of lists containing a student ID, student name and test responses, this function converts the list into
    a dictionary that can be searched.
    param list_of_list_of_responses, a list of list of student data (strings)
    returns data_base_of_responses a dictionary with question number associated with a multiple choice answer value,
    student name with  key 'name', and ID number with key 'ID', but for more than one student.
    '''

    database_of_responses = {}
    for i in range(0,len(list_of_list_of_responses),1):
        single_student_value = index_student(list_of_list_of_responses[i])
        database_of_responses[list_of_list_of_responses[i][0]] = single_student_value
    return database_of_responses


print('Testing index_class()')
print(index_class([['123','foo', 'a','b','c','a'],
                   ['234','bar', 'a','b','c','b'],
                   ['345','xyzzy','a','a','c','b']]))
print(index_class([['10021795','Samden Cross', 'd','d','b','e','e','e','d','a'],
                   ['11051158','Jenni Nuxulon','d','d','b','e','e','d','d','a']]))