# CMPT 141.2018T1.A5
# File I/O & Dictionaries
# This file contains the testing code mentioned in
# the Assignment 5 Description.
# Copy/paste from this file to your working a5.py program.

# version history:
#   11:15am 20 Oct 2016: corrected second test for Q2


# Question 1
print('Testing index_responses()')
print(index_responses(['a', 'b', 'c']))
print(index_responses(['a','a','c','b']))
print(index_responses(['d','d','b','e','e','e','d','a']))


# Question 2
# 11:15am 20 Oct 2016: corrected second test
print('Testing index_student()')
print(index_student(['345','xyzzy','a','a','c','b']))
print(index_student(['10021795','Samden Cross', 'd','d','b','e','e','e','d','a']))


# Question 3
print('Testing index_class()')
print(index_class([['123','foo', 'a','b','c','a'],
                   ['234','bar', 'a','b','c','b'],
                   ['345','xyzzy','a','a','c','b']]))
print(index_class([['10021795','Samden Cross', 'd','d','b','e','e','e','d','a'],
                   ['11051158','Jenni Nuxulon','d','d','b','e','e','d','d','a']]))


# Question 4
print('Testing grade_student')
answers = index_responses(['a','b','c'])
resp1 = index_responses(['a','b','b'])
resp2 = index_responses(['a','b','c'])
print('Correct responses for first example:', grade_student(answers,resp1))
print('Correct responses for second example:', grade_student(answers,resp2))


# Question 5
print('Testing grade')
answers = index_responses(['a','b','c','b'])
response_db = index_class([['123','foo', 'a','b','c','a'],
                           ['234','bar', 'a','b','c','b'],
                           ['345','xyzzy','a','a','c','b']])


# Question 5
print('Response DB before')
print(response_db)
grade(answers,response_db)
print('Response DB after')
print(response_db)


# Question 6
print('Testing read_response_file')
data = read_response_file('cmpt181_midterm.txt')
print(data[0:3])


# Question 7
print('Testing write_score_file')
answers = index_responses(['a','b','c','b'])
response_db = index_class([['123','foo', 'a','b','c','a'],
                           ['234','bar', 'a','b','c','b'],
                           ['345','xyzzy','a','a','c','b']])
grade(answers,response_db)
write_score_file('score_file_example.txt', response_db)



