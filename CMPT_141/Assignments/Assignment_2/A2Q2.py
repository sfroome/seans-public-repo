# CMPT 141 Assignment 2, Question 2
# Written by Sean Froome


def fuel_efficiency(liters, distance):
    """
Calculates the fuel efficiency (liters per 100km)

liters: a float containing the number of liters of fuel used
distance: a float containing the number of kilometers travelled in vehicle

Returns: The fuel efficiency in liters per hundred kilometers.

    """
    efficiency = float(distance) / float(liters)
    efficiency_per_100 = 100 / efficiency
    print("The fuel efficiency of a car that uses", liters, " liters of fuel and travels", distance, "kilometers is",
          efficiency_per_100, "Liters per 100 km.")
    return efficiency_per_100


print("Calculate Fuel Efficiency")
liters = input("Enter the Fuel used (liters)")
distance = input("Enter the distance travelled (kilometers)")
efficiency = fuel_efficiency(liters, distance)
