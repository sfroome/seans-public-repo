#CMPT 141 Assignment 2, Question 4
#Written by Sean Froome


def baseboard(width, length):
    """
Calculates the total cost of the baseboard

width: an integer value of the width of the room.
length: an integer value of the length of the room.

Returns: total cost of the baseboard.

    """
    cost_per_foot = input("What is the cost of baseboard per foot? (in dollars)")
    perimeter = 2 * int(width) + 2 * int(length)
    total_cost_baseboard = perimeter * int(cost_per_foot)

    return total_cost_baseboard


def carpet(width, length):
    """
Calculates the total cost of the carpet

width: an integer value of the width of the room.
length: an integer value of the length of the room.

Returns: Total cost of the carpet.

    """
    cost_per_square_foot = input("What is the cost per square foot of carpet? (in dollars)")
    area = int(width) * int(length)
    total_cost_carpet = area * int(cost_per_square_foot)
    return total_cost_carpet


def cost(total_cost_carpet, total_cost_baseboard):
    """
Calculates the total cost of renovations.

total_cost_carpet: an integer value for the cost of the carpet.
total_cost_baseboard: an integer value for the cost of the baseboard.

Returns: the total cost of renovations including labor.

    """
    labor = 500
    total_cost = total_cost_baseboard + total_cost_carpet + labor
    return total_cost


print("Calculate Cost of Renovations")
width = input("What is the width of the room (in feet)?")
length = input("What is the length of the room (in feet)?")
cost_of_carpet = carpet(width, length)
cost_of_baseboard = baseboard(width, length)
complete_cost = cost(cost_of_carpet, cost_of_baseboard)
print("The total cost of the renovation is $" + str(complete_cost), "Dollars")