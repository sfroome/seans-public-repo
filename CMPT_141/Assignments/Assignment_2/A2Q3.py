#CMPT 141 Assignment 2, Question 3
#Written by Sean Froome


def trip_cost(flight_cost, hotel_cost, number_of_people, number_of_nights ):
    """
Calculates the total cost of a trip, including hotel and flights.

flight_cost: A float containing the cost of a flight for one person.
hotel_cost: A float containing the cost of a two person hotel room for one night.
number_of_people: An integer containing the number of people travelling on the trip.
number_of_nights: An integer containing the number of nights group will be vacationing for.

Returns: The total cost of the trip as an integer.
    """
    number_of_rooms_required = int(number_of_people) // 2 + int(number_of_people) % 2
    cost_of_rooms = int(number_of_rooms_required)*float(hotel_cost)*int(number_of_nights)
    cost_of_flights = int(number_of_people)*float(flight_cost)
    total_cost = cost_of_rooms + cost_of_flights
    return total_cost


print("Calculate Trip Cost")
flight_cost = input("Enter the cost of flights, in dollars")
hotel_cost = input("Enter the cost of the two person hotel room per night")
number_of_people = input("Enter the number of people")
number_of_nights = input("Enter the number of nights")
cost_total = trip_cost(flight_cost, hotel_cost, number_of_people, number_of_nights)
print("The total cost of the trip is $" + str(cost_total), ".")