#h = [0] * 6
#print(h)

#
# def histogram(question):
# 	h = [0] * 6
# 	for q in question: # Remember it doesn't index like using range does.
# 	# for q in range(0,len(question)):
# 		h[q] = h[q] + 1
# 		print("q", q)
# 		print("h[q]", h[q])
# 	print("No response: ", h[0])
# 	for i in range(1,6):
# 		print("Choice ", i, ": ", h[i], sep="")
#
# question = [2, 2, 5, 0, 2, 5]
#
# histogram(question)

#
# def boostTime(D, boost):
# 	if D <= 1.0:
# 		return 1
# 	elif boost:
# 		# Careful here! I expect many might write D * (3/4).
# 		# It's /4 and not *3/4 because it's the REMAINING DISTANCE that's being passed.
# 		return 1 + boostTime(D/4.0, False)
# 	else:
# 		return 1 + boostTime(D/2.0, True)
#
# time = boostTime(1000, False)
#
# print(time)


def insertion_sort(U):
	S = []
	for item in U:
		i = 0
		while i < len(S) and item >= S[i]:
			i = i + 1
		S.insert(i, item)
	return S


# CMPT 141 - Sorting Algorithms
# Topic(s): Merge Sort


def merge(S1, S2):
    """
    combines two sorted sequences into single sorted sequence
    S1: sorted sequence to combine
    S2: other sorted sequence to combine
    return: single sorted sequence of S1, S2 combined
    """
    # let S be an empty sequence
    S = []

    # repeatedly move the smallest item to S
    while len(S1) > 0 and len(S2) > 0:
        if S1[0] < S2[0]:
            S.append(S1[0])
            del S1[0]
        else:
            S.append(S2[0])
            del S2[0]

    # once one of S1 or S2 is empty, append the remaining
    # non-empty sequence to S.
    if len(S1) > 0:
        S.extend(S1)
    else:
        S.extend(S2)

    return S

def merge_sort(S):
    """
    sorts sequence S using merge sort
    S: a sequence of data items
    return: new sorted sequence of S
    """

    # if S is empty, or has only one item,
    # it's already sorted; just return it.
    if len(S) <= 1:
        return S

    # otherwise, divide problem into two sub-problems.
    # note use of integer division here to find the offset
    # of the "middle" data item in S.
    S1 = S[0:len(S)//2]
    S2 = S[len(S)//2:len(S)]

    # recursively solve sub-problems of sorting S1 and S2
    S1 = merge_sort(S1)
    S2 = merge_sort(S2)

    # conquer! (solve the original problem using the
    # solutions for the sub-problems)
    S = merge(S1, S2)
    return S


L =