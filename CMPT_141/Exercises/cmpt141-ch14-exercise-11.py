# CMPT 141 - Recursion
# Topic(s): Recursion


def infected( n ):
    """
    returns the total number people infected by flu strain on given day
    n: number of days since the first infection
    return: the number of people infected
    """

    # base case
    # only one person is infected by the flu on day one
    if n == 1:
        return 1

    # recursive case
    # every person who caught the flu the day before infects two more
    # thus, total number infected is the sum of those who had the flu
    # from previous days plus all the newly infected from today
    else:
        infected_prior = infected( n-1 )  # previously infected count
        return 2**(n-1) + infected_prior

