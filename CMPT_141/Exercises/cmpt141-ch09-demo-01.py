# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Tracing Loop Flow
# DEMO


def show_AN_chessboard():
    """
    Prints the named squares of a chessboard in Algebraic Notation
    from upper left to lower right.
    """

    # start printing chessboard on new line
    print()

    # iterate upper-to-lower row notation of 8 to 1
    for row in range(8,0,-1):
        # iterate left-to-right column notation of 'a' to 'h'
        for col in "abcdefgh":
            # print the current coordinate on same line
            print(col,row,sep="",end=" ")  # formatting
        # done this row's coordinates, start on new line
        print()

# run function to print chessboard
show_AN_chessboard()
