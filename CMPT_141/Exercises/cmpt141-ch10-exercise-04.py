# CMPT 141 - Lists and Tuples
# Topic: Lists of Lists


# lists of provinces (alphabetical abbreviations) & their population counts
# (rounded population counts are from Statistics Canada 2015 listing)
prov_pops = ["AB", 4200, "BC", 4680, "MB", 1290, "NB", 750, "NL", 530,
             "NT", 40, "NS", 940, "NU", 40, "ON", 13790, "PE", 150,
             "QC", 8260, "SK", 1130, "YT", 40]

# create list of sublists containing province data
prov_props = []  # to contain per province data as sublists
for p in range(0,len(prov_pops),2):
    # create sublist of province data before to add to province_props
    prov_data = [ prov_pops[p], prov_pops[p+1] ]
    prov_props.append( prov_data )

# uncomment to see province_props contents
# print(prov_props)
