# CMPT 141 - Arrays
# Topic(s): 2D Arrays


import numpy as np

# create simulated city of its citizens happiness levels
n = 10  # number of city divisions along horizontal, vertical axes
happiness = np.random.rand(n, n)  # 10x10 city grid

# Part (a)
avg_happiness = np.sum(happiness) / happiness.size
print("Average Happiness (All Citizens):",avg_happiness)

# Part (b)
east_happiness = happiness[5:, 3:7]  # eastern happiness levels of city
avg_happiness_east = np.sum(east_happiness)/east_happiness.size
print("Average Happiness (Eastern Citizens):",avg_happiness_east)

# Part (c)
unhappy = np.zeros(happiness.shape)  # unhappiest areas of city
unhappy_lvl = 0.5  # unhappiness is this level or less
# iterate through each row, column of grid to check if it's an unhappy area
for r in range(happiness.shape[0]):
    for c in range(happiness.shape[1]):
        # unhappy area? set map to unhappy level
        if happiness[r, c] <= unhappy_lvl:
            unhappy[r, c] = happiness[r, c]

# Part (c) alternate answer
unhappy = np.copy(happiness)  # copy of original happiness map
unhappy_lvl = 0.5  # unhappiness is this level or less
# use logical indexing to set all happy areas to 0.0
unhappy[unhappy > unhappy_lvl] = 0.0
