# CMPT 141 - Functions and Objects
# Topic: Calling Methods
# DEMO


# initialize some variables to play with
url = "www.cs.usask.ca"

# find the location of the first period in the url
url.find(".")

# count the number of periods in the url
# (function not covered in readings)
url.count(".")

# count the number of characters in the url after removing "www."
# (gotta be careful- don't just assume a function does what you think!)
print(len(url.lstrip("w.")))
