# CMPT 141 - Control Flow: Branching and Repetition
# Topic: For-Loops


def count_vowels(s):
    """
    Prints the number of occurrences of 'a','e','i','o','u' in s
    s: string to count vowels for
    """
    a_count = 0  # number of 'a' characters in s
    e_count = 0  # number of 'e' characters in s
    i_count = 0  # number of 'i' characters in s
    o_count = 0  # number of 'o' characters in s
    u_count = 0  # number of 'u' characters in s

    # found lowercase vowel? add to appropriate counter
    for c in s:
        if c == 'a':
            a_count = a_count + 1
        elif c == 'e':
            e_count = e_count + 1
        elif c == 'i':
            i_count = i_count + 1
        elif c == 'o':
            o_count = o_count + 1
        elif c == 'u':
            u_count = u_count + 1

    print("a:",a_count,"e:",e_count,"i:",i_count,"o:",o_count,"u:",u_count)
