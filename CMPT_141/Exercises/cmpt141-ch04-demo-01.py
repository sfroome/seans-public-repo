def print_smaller_absolute(num1,num2):
    """
    This function calculates the absolute value
    of the smaller of its inputs.
    Prints a message indicating this value;
    then the value is returned.


    num1: integer; one of the numbers to compare
    num2: integer; one of the numbers to compare
    return: the absolute value of the smaller number
    """
    small_abs = abs(min(num1,num2))
    print("Absolute value of smaller number: ",small_abs)
    return small_abs


answer = print_smaller_absolute(-20, 2)
print(answer)