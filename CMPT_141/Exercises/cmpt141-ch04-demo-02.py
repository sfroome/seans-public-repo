# CMPT 141 - Functions and Objects
# Topic: Program Tracing
# DEMO


def str_len_diff(s1,s2):
    """
    Returns the length difference between the strings.
    s1: string to compare length of
    s2: other string to compare length of
    return: difference in length between strings
    """
    return abs(len(s1)-len(s2))

home_team = "Canada"
visiting_team = "Scotland"
print("The difference in string length between",
    home_team,"and",visiting_team,"is",
    str_len_diff(home_team,visiting_team))
