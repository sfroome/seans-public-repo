# CMPT 141 - Lists and Tuples
# Topic: List Comprehensions
# DEMO


# list of social media messages as sublists (message, days since posted)
msgs = [ ["Forgot the recipe again! #nofood",3],
         ["such a gluten #food #fivestar",10],
         ["International #Food day! Hooray!",1],
         ["Apple pie is bestest food",5],
         ["lunchtime? #food #craving hits again",9] ]


# Part (a)
food_msgs = [ msg for msg in msgs if "#food" in msg[0] ]
print(food_msgs)


#



#  Part (b)
food_msgs = [ [msg[0]+" #yawn", msg[1]] for msg in food_msgs ]
