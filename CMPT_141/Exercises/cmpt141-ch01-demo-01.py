# CMPT 141 - Algorithms & Computer Programs
# Topic: Algorithms
# DEMO

def Average(S):
    # you should be able to see that this code here
    # looks a lot like the pseudocode algorithm
    total = 0
    for x in S:
        total = total + x
    average = total / len(S)
    return average

# Print out some example uses of Average
print("Average of 0 to 5 is ",Average(range(5)))
print("Average of 0 to 10 is ",Average(range(10)))
print("Average of 0 to 100 is ",Average(range(100)))
