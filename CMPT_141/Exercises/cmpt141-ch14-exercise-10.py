# CMPT 141 - Recursion
# Topic(s): Recursion


def reverse( msg ):
    """
    returns the string in reverse order
    (also works for lists)
    msg: the string to reverse the order of
    return: the same characters as in msg, but in reverse order
    """

    # base case
    # the empty string reversed is itself (the empty string)
    if len(msg) == 0:
        return msg

    # recursive case
    # the string reversed is the reverse of everything but the first character
    # concatenated with the first character.
    else:
        return reverse(msg[1:]) + msg[0]

def rev_1( msg ):
    """variant 1: move the last to the beginning"""
    if len(msg) == 0:
        return msg
    else:
        return msg[-1] + rev_1(msg[0:-1]) 

def rev_2( msg ):
    """"variant 2: split the string in half, roughly"""
    if len(msg) == 0 or len(msg) == 1:
        return msg
    else:
        mid = len(msg) // 2
        return rev_2(msg[mid:]) + rev_2(msg[:mid])


# simple testing script
strings = ["We are the egg men", 
           "They are the egg men", 
           "I am the walrus", 
           "Goo goo ga joob"]

for s in strings:
    if reverse(reverse(s)) != s:
        print("error: reverse() failed on:", s)
    if rev_1(rev_1(s)) != s:
        print("error: rev_1() failed on:", s)
    if rev_2(rev_2(s)) != s:
        print("error: rev_2() failed on:", s)

