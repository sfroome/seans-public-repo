# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Loop Classification

# Part (d)
divisor = 2
dividend = 49
while dividend % divisor != 0:
    divisor = divisor + 1
