# CMPT 141 - Testing and Debugging
# Topic(s): Debugging


import math as m

def pika_jumps_in():
    print("pika")
    print("PIKA!!!!")

# number of blocks away bus stops are from current location
# (west distance is positive, north distance is positive)
bus_stops = [(-3,4),(5,10),(1,0),(-6,-1),(3,-9)]
close_bus_stops = []  # to hold nearby bus stops

pika_jumps_in()

# for every bus stop, determine if it is both within a distance
# of eight blocks and whether the longest single block direction
# to the stop is less than five blocks
for bstop in bus_stops:
    dist_bus_stop = abs(bstop[0]) + abs(bstop[1])
    longest_block = abs(max( bstop[0],bstop[1] ))
    if dist_bus_stop < 8 and longest_block < 5:
        close_bus_stops.append(bstop)

print(close_bus_stops)
