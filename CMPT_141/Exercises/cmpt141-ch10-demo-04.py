# CMPT 141 - Dictionaries
# Topic(s): Lists as Arguments
# DEMO


def garbled_print( msg ):
    """
    Modifies message string to make less sense, then prints it.
    msg: The message (as a string) to make less comprehensible
    """

    # have message make less sense by replacing 'e''s with a number
    msg = msg.replace("e","1")
    print( msg )

def garbled_lprint( msg ):
    """
    Modifies message list to make less sense, then prints it.
    msg: The message (as a list) to make less comprehensible
    """
    # have message make less sense by replacing 'e''s with a number
    for i in range(0,len(msg)):
        if msg[i] == "e":
            msg[i] = "1"
    print( msg )


# test out our garble function
headline = "everything is erupting!"  # message as a string
print(headline)             # original message
garbled_print(headline)     # muddle the message & print it
print(headline)             # print original message again

print()  # newline to seperate test outputs

# test out our garble function for lists
headline = list("everything is erupting!")  # message as a list
print(headline)             # original message
garbled_lprint(headline)    # muddle the message & print it
print(headline)             # print original message again
