# CMPT 141 - Dictionaries
# Topic(s): Dictionary Basics
# DEMO


# create dictionary mapping tower names to their locations in the world
towers = {
    "Tokyo Skytree": "Japan",
    "Canton Tower": "China",
    "CN Tower": "Canada"
}

print(towers)

del towers["CN Tower"]

print(towers)

towers["Eiffel Tower"] = "Paris"

print(towers)

towers["Eiffel Tower"] = "France"

print(towers)


