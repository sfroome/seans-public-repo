# CMPT 141 - Functions and Objects
# Topic: Generalization


# Part (a)
def cards_per_player(n_cards,n_players):
    return n_cards // n_players

# Part (b)
def total_watch_time(n_episodes,min_per_episode):
    return n_episodes * min_per_episode / 60
