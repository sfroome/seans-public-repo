# CMPT 141 - Recursion
# Topic(s): Recursion


def min_failure(failures):
    """
    returns the smallest value from the list of failire rates
    failures: list of failure rates (floating-point from 0.0 to 1.0)
    return: the value of the smallest failure rate
    """

    # base case
    # list of one failure rate means that value is the smallest
    if len(failures) == 1:
        return failures[0]

    # recursive case
    # smallest failure rate is the smaller of the last item in failures and
    # the smallest from the list of failures with the last item removed
    else:
        return min(failures[-1], min_failure(failures[:-1]))
