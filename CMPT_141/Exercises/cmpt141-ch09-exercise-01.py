# CMPT 141 - Control Flow: Branching and Repetition
# Topic: While Loops


passcode = "cmpt141"    # passcode to finish the loop
n_attempts = 0          # total attempts until correct passcode entered
is_correct = False      # Boolean of whether or not valid usercode entered

while not is_correct:
    usercode = input("Enter passcode:")
    is_correct = passcode == usercode
    n_attempts = n_attempts + 1

print(n_attempts,"attempt(s) made.")
