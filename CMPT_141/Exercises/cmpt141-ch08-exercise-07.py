# CMPT 141 - Control Flow: Branching and Repetition
# Topic: If-Else Statements


def print_str_parity(s):
    """
    Prints whether incoming string has an odd/even character length
    s: String whose length to print the parity (odd or even status) of
    """
    if (len(s) % 2 == 0):
        print('"'+s+'"',"has an even number of characters.")
    else:
        print('"'+s+'"',"has an odd number of characters.")
