# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Loop Classification

# Part (e)
low = -100
high = 100
num = 0
msg = "Enter int between "+str(low)+" to "+str(high)+":"
while num >= low or num <= high:
    num = int(input(msg))
