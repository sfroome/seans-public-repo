# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Infinite Loops
# DEMO


# iterate through one to ten and print their squares

is_done = False     # whether or not we're done counting numbers
cur = 1             # current number to print square of (starts at one)
last = 10           # last number to print square of

# for each number from 1 to 10 print its squares
while not is_done:
    # check to make sure we are below or at last number before printing
    if cur <= last:
        print(cur*cur)
        cur = cur+1

