
# If we really want to modify the variable 'status' that is not in the function,
# we should do it like he code below, but in this example it makes the function unnecessary.
# The function could be useful if we wanted to make sure that s was a valid status 
# string.  But we can't demonstrate that yet as we have not covered if statements.
# But imagine that set_status() returned s if it was a valid string, or issued an 
# error if it was not.  then this would be a sensible approach.

status = "None"

def set_status(s):
    return s

status = set_status('Poisoned')
print(status)

# Alternatively we *could* do the following to make the variable 'status' inside of the
# function actually be the same variable as the 'status' variable *outside* the function:

status = "None"

def set_status(s):
	global status
	status = s
	
set_status('Poisoned')
print(status)

# Now the output will be "Posioned".  But use of the keyword "global" variables is
# normally considered extremely bad programming practice, and you should not use it.
# It can cause confusion and nasty side-effect bugs when the technique is used to 
# modify variables that are at a glance seemingly unrelated to the function.  