# CMPT 141 - Indexing and Slicing of Sequences
# Topic: Indexing


# Variables initialized for example
s = "A Song Of Ice And Fire"

# Part (a)
s[0]

# Part (b)
s[10]

# Part (c)
s[-1]

# Part (d)
s[0] + s[2] + s[7] + s[10] + s[14] +s[18]

# Part (e)
s[-22] + s[-20] + s[-15] + s[-12] + s[-8] + s[-4]
