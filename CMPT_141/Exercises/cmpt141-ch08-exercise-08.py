# CMPT 141 - Control Flow: Branching and Repetition
# Topic: If-Elif-Else Statements


def print_letter_grade(grade):
    """
    Prints incoming grade in letter equivalent.
    grade: int grade value out of 100
    """
    if (grade >= 95 and grade <= 100):
        print("A+")
    elif (grade >= 85 and grade < 95):
        print("A")
    elif (grade >= 75 and grade < 85):
        print("B")
    elif (grade >= 65 and grade < 75):
        print("C")
    elif (grade >= 50 and grade < 65):
        print("D")
    else:
        print("F")
