# CMPT 141 - Lists and Tuples
# Topic: Heterogeneous Lists


# lists of provinces (alphabetical abbreviations) & their population counts
# (rounded population counts are from Statistics Canada 2015 listing)
provs = ["AB","BC","MB","NB","NL","NT","NS","NU","ON","PE","QC","SK","YT"]
pops = [4200,4680,1290,750,530,40,940,40,13790,150,8260,1130,40]

# consolidate province and population counts into single flat list
prov_pops = []  # flat list to contain province, population count
for p in range(0,len(provs)):
    prov_pops.extend([ provs[p],pops[p] ])

# uncomment to see province_pops contents
# print(prov_pops)
