
import matplotlib.pyplot as mplt

# list of the number of subscribers per month
monthly_sub_count = [10,20,35, 50,95,125, 200,350,500, 550,600,620,         # two years back
                     630,630,590, 620,700,750, 765,770,790, 800,900,1000]   # one year back

# plot the trend of subscribers
mplt.plot( range(1,25),  monthly_sub_count,  "ro" ) # x, y, style
#mplt.plot( range(1,25),  monthly_sub_count) # x, y, style

mplt.title("Number of Subscribers Per Month")   # plot title
mplt.xlabel("Months Since Service Launch")      # x-axis label
mplt.ylabel("Number of Subscribers")            # y-axis label
mplt.show()                                     # display plot to screen
