# CMPT 141 - File I/O
# Topic(s): Reading Files


# open file for reading
f = open("scientists_data.txt","r")

# read in all scientist data as list of records
scientists = []  # to contain listing of scientist records
for line in f:

    # removing trailing newline & parse data as list
    record = line.rstrip()
    record = record.split(',')

    # create scientist record
    scientist = {
        "name":         record[0],
        "birth_year":   int(record[1]),
        "death_year":   int(record[2])
    }

    # add scientist record to scientists listing
    scientists.append( scientist )

# done reading, close file
f.close()
