# CMPT 141 - Dictionaries
# Topic(s): Dictionary Basics (Selective Create)


# mapping of suitcases to their weight in pounds (lbs)
mom_suitcases = {
    "Baby Blue Backpack": 3.2,
    "Red Hiker Pack": 5.1,
    "Purple Suitcase": 8.3,
    "Black Backpack": 4.6,
    "White Shoulder Bag": 2.2,
    "Navy Blue Hiker Pack": 4.8
}

belongings = 10.5  # weight of traveller's belongings to pack
max_weight = 15.2  # max bag weight allowed on planes

def eligible_suitcases(suitcases, belongings, max_weight):
    """
    Return mapping of suitcases to weight which meet weight restrictions
    once packed with a traveller's belongings

    suitcases: mapping of suitcases to their weight in lbs
    belongings: the weight of a traveller's items in lbs
    max_weight: maximum bag weight allowed

    return: new mapping of suitcases to weights which can carry
    traveller's items and meet weight restrictions simultaneously
    """

    eligible_suitcases = {}  # luggage pieces meeting weight restriction
    for suitcase in suitcases:
        if (suitcases[suitcase] + belongings <= max_weight):
             # packed belongings meets weight restrictions- add to eligible
            eligible_suitcases[suitcase] = suitcases[suitcase]

    return eligible_suitcases
