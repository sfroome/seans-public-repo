# CMPT 141 - Testing and Debugging
# Topic(s): White-Box Test Case Generation


def is_valid_password( password ):
    """
    determines if password meets these constraints:
      - 9-18 characters long
      - can be letters and/or numbers
    password: string to check validity of
    return: True if password satisfies constraints
    """
    is_valid = True  # whether constraints are satisfied

    if len(password) < 9 or len(password) > 18:
        is_valid = False

    else:
        # check validity of each character in password
        for p in password:
            if not p.isalnum():
                is_valid = False

    return is_valid
