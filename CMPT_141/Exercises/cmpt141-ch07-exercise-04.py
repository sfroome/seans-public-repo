# CMPT 141 - Indexing and Slicing of Sequences
# Topic: Slicing


# Variables initialized for example
s = "Green Arrow"

# Part (a)
s[0:5]

# Part (b)
s[3:8]

# Part (c)
s[-5:len(s)]

# Part (d)
s[1:len(s):3]

# Part (e)
s[len(s):5:-1]

# Part (e) alternate answer
s[len(s):-6:-1]
