# CMPT 141 - Search Algorithms
# Topic(s): Linear Search


def match_by_breed( dogs,target_breed ):
    """
    perform linear retrieval search on dog records to find matching breeds
    dogs: list of dog records to search
    target_breed: dog "breed" to match/search for
    return: list of records which have matching "breed" key
    """
    matching_breeds = []  # list of dog records with matching breed

    for dog in dogs:
        if dog["breed"] == target_breed:
            matching_breeds.append(dog)

    return matching_breeds
