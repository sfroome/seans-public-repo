# CMPT 141 - Modules
# Topic: math Module
# DEMO

import math as m

# call some funcs
m.sqrt(100)
m.log10(1000)
m.factorial(5)
m.radians(180)  # pi




# access some math consts
m.pi
m.e



# can use module funcs with themselves, built-in, or other module's funcs
m.cos(m.radians(180))  # use with math funcs
abs(-m.log10(1000))  # use with built-in funcs
