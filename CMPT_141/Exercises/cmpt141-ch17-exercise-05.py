# CMPT141 - Sorting Algorithms
# Topic(s): Quick Sort


def quick_sort(S):
    """
    sorts sequence S using quick sort
    S: array of data items to be sorted
    return: sorted sequence of S
    """

    # if S contains 0 or 1 items, it's already sorted
    if len(S) <= 1:
        return S

    # divide step of quick sort
    L = []      # for items smaller than the pivot
    E = []      # for items equal to the pivot
    G = []      # for items greater than the pivot
    p = S[0]    # choose the first item as the pivot

    for x in S:
        if x < p:
            L.append(x)
        elif x > p:
            G.append(x)
        else:
            E.append(x)

    # recursively solve subproblems of sorting L and G
    L = quick_sort(L)
    G = quick_sort(G)

    # conquer!
    S = L + E + G
    return S
