# CMPT 141 - Lists and Tuples
# Topic: List Operations (Append, Remove, len, sort)


# list of package weights (in lbs)
pkg_weights = [5.53, 3.92, 15.93, 5.65, 8.14, 3.77, 3.51, 3.62, 13.04,
               5.63, 16.94, 8.37, 15.62, 15.10, 16.55, 15.36, 4.29, 7.36,
               10.16, 14.50, 3.89, 2.58, 4.07, 1.77, 9.25, 1.73, 10.70,
               14.33, 6.06, 7.74]
# count of total packages 
n_total_pkgs = len(pkg_weights)

# Parts (a), (b) combined
# create list of lightweight packages
light_pkgs = []
# for every package in assembly line...
for pw in pkg_weights:
    # lightweight packages.
    if pw < 5.0:
        light_pkgs.append(pw)

heavy_pkgs = []
for pw in pkg_weights:
    # heavyweight packages.
    if pw > 15.0:
        heavy_pkgs.append(pw)

for pw in heavy_pkgs:
    pkg_weights.remove(pw)
    
# the following loop will not work
# because it tries to change the structure of the list 
# while iterating over it.  
# DON'T DO THAT
# for p in pkg_weights:
#     if p > 15.0:
#         pkg_weights.remove(p)




# sort lightweight in ascending order
light_pkgs.sort()

# Part (c)
# outputs eligible pkg count, eligible pkg weights, erroneous pkgs count
print("Lightweight Package Weights:", light_pkgs)
print("# of Lightweight Packages:", len(light_pkgs))
print("# of Packages Exceeding Max Weight:",n_total_pkgs-len(pkg_weights))
