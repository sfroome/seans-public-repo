# CMPT 141 - Dictionaries
# Topic(s): Dictionary Usage (Mapping), Dict of Lists


# mapping of friends' names to days of the week they're available to play
availability = {
    "Caesar":      ["Monday"],
    "Boudica": ["Sunday"],
    "Cleopatra":    ["Monday","Tuesday"],
    "Alexander":  ["Sunday","Monday","Saturday"],
    "Hannibal":     ["Sunday","Monday","Saturday"]
}


# Part (a)

def reverse_availability( availability_friends ):
    """
    Creates reverse mapping of days to friends available based on input
    availability_friends: map of friends to days free to reverse
    return: reverse mapping of availability_friends
    """

    # for each day the friend is free, add them to new mapping
    days_available = {}  # to contain mapping of days to friends available
    for friend in availability:
        days = availability[friend]  # list of days the friend can play
        for day in days:
            # key exists? just add friend to that day's available friends
            if day in days_available:
                days_available[day].append( friend )
            # key d.n.e.? create key with friend as only friend available
            else:
                days_available[day] = [ friend ]

    return days_available


# Part (b)

# obtain mapping of days to friends available on those days
friends_available = reverse_availability(availability)

# find best day to play
best_day = ""  # the day of the week most friends can play
most_free_friends = 0  # number of friends free on best day
for day in friends_available:
    # found a better day to play (more can attend), update best day
    if len(friends_available[day]) > most_free_friends:
        best_day = day
        most_free_friends = len(friends_available[day])

# print info about the day most friends are available
print("Day of the week most friends can attend:",best_day)
print(most_free_friends,"friend(s) attending:",
      friends_available[best_day])
print(len(availability)-len(friends_available[best_day]),
      "friend(s) not attending:",
      [ f for f in availability if f not in friends_available[best_day] ])
