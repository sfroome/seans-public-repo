# CMPT 141 - Arrays
# Topic(s): Logical Indexing


import numpy as np

def defective_goods( defects,goods_processed ):
    """
    returns defect counts where equipment is faulty (10% or more goods
    reported defective at that station)
    defects: array of number of defective goods processed
    goods_processed: number of goods processed per station
    return: defect counts where faulty equipment is True, otherwise False
    """
    faulty_equip = defects >= goods_processed * 0.1
    return defects[faulty_equip]
