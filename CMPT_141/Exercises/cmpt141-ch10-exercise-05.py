# CMPT 141 - Lists and Tuples
# Topic: List Comprehensions (Selection)


# list of sublists of a user's songs containing [title, rating, genre]
library = [ ["Pokemon!",5,"Metal"],
            ["The Pokerap",4,"Rap"],
            ["On the Road to Viridian City",4,"Hard Rock"],
            ["Ghost Love Score",1,"Metal"],
            ["So Cold",1,"Hard Rock"] ]

best_playlist = [ song for song in library if song[1] >= 4 and
                  (song[2] == "Metal" or song[2] == "Hard Rock")]

print(best_playlist)