# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Choosing Loop Techniques
# DEMO

# Allow the user three attempts to enter a password
secret_password = "oak"
word = input("enter password ")
count = 1
max_trials = 3
while word != secret_password and count < max_trials:
    word = input("enter password ")
    count = count + 1

if count == max_trials:
    print("You have made too many invalid attempts!")
elif word == secret_password:
    print("Welcome to the secret system!")
