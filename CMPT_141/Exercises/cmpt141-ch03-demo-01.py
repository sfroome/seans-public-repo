# CMPT 141 - Functions and Objects
# Topic: Function Calls
# DEMO

# NOTE: This script purposely contains errors (pedagogical) when executed via Python's non-interactive mode.


# initialize some variables to play with
first_name = "Ash"
middle_name = "Red"
last_name = "Ketchum"
nickname = "The Pokemon Trainer"
age = 10

# calling functions with (one, none, some) number of arguments
# calling functions with return values & with no return values
print("Who is", nickname, "?")
print()
print(nickname, "is", first_name, middle_name, last_name)

# calling functions other than print
len_first_name = len(first_name)    # number of chars in first name
len_last_name = len(last_name)      # number of chars in last name
max(len_first_name, len_last_name)   # longest string

# nested function calls
max(len_first_name, len_last_name, len(middle_name), len(nickname))

# type conversions covered in chapter on data types are also function calls!
headline = nickname + " is " + age + " years old."          # FAILS because can't implicitly convert int
headline = nickname + " is " + str(age) + " years old."     # okay because we type-converted the string
