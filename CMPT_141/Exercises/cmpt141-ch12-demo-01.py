
# open file for reading
f = open("ext_directory_data.txt","r")

# read in all phone extension to employee mappings
extensions = {}  # to contain mapping of phone extensions to employees
for line in f:
    # remove trailing newline and parse file data
    # into list data
    mapping = line.rstrip()

    mapping = mapping.split(',')

    # assign mapping to phone extension directory data
    # phone ext is first data item, employee name second
    extensions[ int(mapping[0])] = mapping[1]

# done reading, close file
f.close()

print(extensions)
