# CMPT 141 - Dictionaries
# Topic(s): Dictionary Basics (Create, Update)


# dictionary mapping students to their favourite ice cream flavour
survey = {
    "Howlett": "vanilla",
    "Grey": "chocolate",
    "Munroe": "vanilla",
    "Summers": "strawberry",
    "Xavier": "strawberry",
    "Drake": "strawberry"
}

def survey_results( survey ):
    """
    Create dictionary mapping favourite flavours to the count of students
    survey: dictionary of survey with students mapped to flavours
    return: new dictionary mapping flavours to number of students
    """
    results = {}

    # create all flavours as keys and set their counts to zero
    for flavour in survey.values():
        results[flavour] = 0

    # for each student, add their flavour to the correct count
    for student in survey:
        results[ survey[student] ] = results[ survey[student] ] + 1

    return results
