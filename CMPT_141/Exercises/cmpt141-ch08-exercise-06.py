# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Boolean Expressions


# Variables initialized for the exercises
n_fins      # int of animal's number of fins
n_wings     # int of animal's number of wings
n_stripes   # int of animal's number of stripes
n_spots     # int of animal's number of spots
has_tail    # Boolean whether animal has tail

# Part (a)
not n_wings > 0

# Part (b)
n_stripes > n_spots or n_spots == 5

# Part (c)
n_stripes <= 5 and not n_stripes > n_spots

# Part (d)
not(n_fins > 0 or n_spots > 0)

# Part (e)
has_tail or (n_fins == 2 and n_wings == 2)

# Part (f)
n_wings > 0 and not n_fins > 0

# Part (g)
((n_stripes >= n_spots and n_stripes > 10) or
 (n_stripes < n_spots and n_spots <= 10))
