# CMPT 141 - Lists and Tuples
# Topic: List Comprehensions (Computation)


# list of drop rates for defeating the first boss in the first area
drops = [["pearl Scale", 0.45],  # [item name, probability]
         ["Glossy Hide",0.35],
         ["Crimson Claw",0.15],
         ["Ivory horn",0.04],
         ["Crimson Gem",0.01]]

# boost all drop rates for event by applying multiplier (initial test)
event_modifier = 1.5

# use list comprehensions to create sublists of new item names, drop rates
event_names = [drop[0].title() for drop in drops]  # proper case names
event_rates = [drop[1] * event_modifier for drop in drops]  # event drop rates

# create drop rates table w/ item names capitalized & event modifier applied
event_drops = []  # to contain new items' names and event chances
# for every item, append its new data as sublist to event drop list
for d in range(0,len(event_rates)):
    item = [ event_names[d],event_rates[d] ]
    event_drops.append( item )
