# CMPT 141 - Lists and Tuples
# Topic: Heterogenous Lists, Lists of Lists
# DEMO


import matplotlib.pyplot as mplt

# list of sublists of students' average hours of sleep a night per week
semester_sleeps = [ ["Student A",[8,7.5,7.5,7, 8,6.5,7,8, 7.5,7,7,7]],
                    ["Student B",[7,6.5,6,7.5, 7,7,6.5,6, 6.5,6.5,6,5]],
                    ["Student C",[7.5,8,7,9, 7,7,9,7.5, 8.5,7,7.5,7]] ]

# plot each student's twelve week sleep schedule onto same graph
weeks_range = range(1,13)  # number of semester weeks to plot
# for each students' semester's sleep schedule, plot against the weeks so far
for student_data in semester_sleeps:
    # what is student_data? it is a list
    # what is in the list for each student?: name, ANOTHER LIST
    mplt.plot(weeks_range,student_data[1],label=student_data[0])

mplt.axis([0,15,0,10])  # make sure we can see all the data
mplt.legend()
mplt.xlabel("Number of Weeks Into Semester")
mplt.ylabel("Average Hours of Sleep a Night (One Week)")
mplt.title("Average Hours of Sleep a Night Per Week Over A Semester")
mplt.show()
