# CMPT 141 - Functions and Objects
# Topic: Calling Methods


# Variables initialized for the examples
nation = "Canada !!!"
nation_motto = "From Sea to Sea"

# Part (a)
nation.isalnum()

# Part (b)
nation.rstrip("! ")

# Part (c)
nation_motto.find("Sea")

# Part (d)
nation_motto.rfind("Sea")

# Part (e)
nation_motto.replace(" ","_").lower()

# Part (e) alternate answer (order switched)
nation_motto.lower().replace(" ","_")
