# CMPT 141 - Data, Expressions, Variables, and Console I/O
# Topic: Input
# DEMO
#
# write a little blurb about what the user would do if they were very wealthy
# based on information they provide about themselves


# title
print("If I Were Super Wealthy Simulator")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print()

# prompt user for their name
name = input("What is your name?: ")
print("Hello",name,"! We're going to gather some data on you and predict what you would do if you were very wealthy.")
print()

# prompt user for some more data about themselves
animal = input("What is your favourite animal?: ")
number = int(input("What is your favourite number (please give an integer)?: "))
float_number = float(input("What is another number you like (this time, give a float)?: "))
print()

animal_breeding = number+2

# print out the scenario
print("Hello",name,". If you were super wealthy, my guess is you would buy",number,animal+"(s).")
print("You would make sure that they live in",float_number,"degrees celsius weather.")
print("Should we be concerned? Possibly.")
print()
