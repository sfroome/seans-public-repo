# CMPT 141 - Arrays
# Topic(s): Array Applications
# DEMO


import numpy as np
import skimage.data as skdata
import matplotlib.pyplot as mplt

def draw_histogram( image ):
    """
    draw histogram data of 8-bit grayscale image to new figure
    image: 8-bit grayscale image to compute histogram for
    """

    # create 1D array of histogram data (maps intensities to counts)
    # 8-bit images have intensities from 0 to 255 inclusive
    histogram = np.array([ image[image==i].size for i in range(256) ])

    # draw the histogram
    mplt.figure("Histogram")
    mplt.xlabel("Intensity")
    mplt.ylabel("Number of Pixels")
    mplt.bar( range(256),histogram )

# load image & draw image
image = skdata.camera()
mplt.gray()
mplt.imshow(image)

# compute & draw image's histogram
draw_histogram(image)

# actually display all figures
mplt.show()
