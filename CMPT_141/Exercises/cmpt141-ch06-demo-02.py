# CMPT 141 - Modules
# Topic: squirtle, random Modules
# DEMO

import turtle as squirtle
import random as random

squirtle.shape("turtle")
squirtle.speed(0)
squirtle.up()

def drawCircle(x, y):
    # Draw a circle there, and fill it with a random color.
    squirtle.fillcolor(random.random(), random.random(), random.random())
    squirtle.goto(x, y)
    squirtle.down()
    squirtle.begin_fill()
    squirtle.circle(30)
    squirtle.end_fill()
    squirtle.up()

squirtle.title('Click on the canvas!')

# bind left mouse button to drawCircle function
squirtle.onscreenclick(drawCircle)

# Initiate main loop
squirtle.mainloop()