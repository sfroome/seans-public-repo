# CMPT 141 - Functions and Objects
# Topic: Abstraction and Encapsulation via Functions


# Notice in the original code that block installation and reporting repeats
# Define a function for installing blocks and reporting progress
# The function can be called instead of repeatedly typing out the statements

def install_blocks(blocks_installing,blocks_installed,blocks_total):
    """
    Installs some program blocks and prints progress report to user.

    blocks_installing: int amount of blocks being installed
    blocks_installed: int amount of blocks installed so far
    blocks_total: int amount of total blocks to be installed

    return: number of blocks installed this iteration
    """
    blocks_installed = blocks_installed + blocks_installing
    print(blocks_installed/blocks_total*100.0,"% complete")
    return blocks_installed


# Original Code with new function replacing the repeated portions
blocks_total = 12
print("Installing Programs...")
blocks_installed = 0
blocks_installed = install_blocks(3,blocks_installed,blocks_total)
blocks_installed = install_blocks(3,blocks_installed,blocks_total)
blocks_installed = install_blocks(6,blocks_installed,blocks_total)
print("Program installation complete!")
