# CMPT 141 - Arrays
# Topic(s): 2D Arrays


import numpy as np

# example wind farm as a 5x5 grid of turbines
turbines = [ [2.5, 2.2, 2.3, 2.5, 2.6],
             [2.6, 2.4, 2.5, 2.5, 2.6],
             [2.7, 2.5, 2.5, 2.6, 2.7],
             [2.7, 2.4, 2.5, 2.5, 2.6],
             [2.2, 2.3, 2.4, 2.5, 2.5] ]

# Part (a)
turbines = np.array(turbines)

# Part (b)
print(turbines.shape)

# Part (c)
print(turbines[4,0])

# Part (d)
section = turbines[2:4,:3]
print(np.sum(section)/section.size)

# Part (e)
for row in turbines:
    for turbine in row:
        print(turbine)
