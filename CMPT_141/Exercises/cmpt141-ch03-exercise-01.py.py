# CMPT 141 - Functions and Objects
# Topic: Function Calls


# Variables initialized for this example
char_limit = 5
default_pet = "Dog"
pet_choice_1 = "Ox"
pet_choice_2 = "Platypus"

# Part (a)
print("")

# Part (b)
print(min( len(default_pet), len(pet_choice_1), len(pet_choice_2) ))

# Part (c)
new_char_limit = 2*max( len(default_pet), len(pet_choice_1),
                        len( (pet_choice_2)) )

# Part (d)
info = "The sum of all string lengths is " + str(
    len(default_pet)+len(pet_choice_1)+len(pet_choice_2) )

# Part (e)
experimental_char_limit = char_limit ** abs(len(default_pet)-char_limit)
