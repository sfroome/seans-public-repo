# CMPT 141 - Sorting Algorithms
# Topic(s): Merge Sort


def merge(S1, S2):
    """
    combines two sorted sequences into single sorted sequence
    S1: sorted sequence to combine
    S2: other sorted sequence to combine
    return: single sorted sequence of S1, S2 combined
    """
    # let S be an empty sequence
    S = []

    # repeatedly move the smallest item to S
    while len(S1) > 0 and len(S2) > 0:
        if S1[0] < S2[0]:
            S.append(S1[0])
            del S1[0]
        else:
            S.append(S2[0])
            del S2[0]

    # once one of S1 or S2 is empty, append the remaining
    # non-empty sequence to S.
    if len(S1) > 0:
        S.extend(S1)
    else:
        S.extend(S2)

    return S

def merge_sort(S):
    """
    sorts sequence S using merge sort
    S: a sequence of data items
    return: new sorted sequence of S
    """

    # if S is empty, or has only one item,
    # it's already sorted; just return it.
    if len(S) <= 1:
        return S

    # otherwise, divide problem into two sub-problems.
    # note use of integer division here to find the offset
    # of the "middle" data item in S.
    S1 = S[0:len(S)//2]
    S2 = S[len(S)//2:len(S)]

    # recursively solve sub-problems of sorting S1 and S2
    S1 = merge_sort(S1)
    S2 = merge_sort(S2)

    # conquer! (solve the original problem using the
    # solutions for the sub-problems)
    S = merge(S1, S2)
    return S
