# CMPT 141 - Data, Expressions, Variables, and Console I/O
# Topic: Variable Initialization


# Part (a)
w = 5

# Part (b)
w = w + 7

# Part (c)
l = w * 2 - 10
