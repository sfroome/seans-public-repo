# CMPT 141 - Search Algorithms
# Topic(s): Timing Comparisons

import time as time


def linear_membership_search(C, target_key):
    """
    a linear membership search for the target key

	C:          a sequence of numbers or strings
	target_key: the target key for the search
	return:     a list containing items from C whose search
                 keys match target_key
    """
    for i in C:
        if i == target_key:
            return True

    return False

def binary_membership_search(C, target_key, start, end):
    """
    A binary search for membership

	C:       a collection of data items ordered by their search keys
	target:  the target key
	start:   first offset of S to be searched
	end:     last offset of S to be searched
	return:  true if S contains an item whose search key
		     matches the target, false otherwise
    """
    if end < start or start >= len(C):
        return False

    mid = (start + end) // 2

    if C[mid] == target_key:
        return True
    elif C[mid] < target_key:
        return binary_membership_search( C,target_key,mid+1,end )
    else:
        return binary_membership_search( C,target_key,start,mid-1 )


# search parameters
n_data = 100000000 # number of data items in array
target_key = 10000000000000000   # target key to search for

# construct list of ints 0 to n_data-1 to search
start = time.time()
nums = list(range(0,n_data)) # ascending order
time_generate = time.time()-start
print("It took ", time_generate, 'seconds just to generate the data.')
print("nums = [" + ",".join([str(i) for i in nums[:3]]) + ", ..." + ",".join([str(i) for i in nums[-3:]])+"]")

# time linear membership search
time_start = time.time()
found_item = linear_membership_search(nums,target_key)
time_linear = time.time() - time_start

# time binary membership search
time_start = time.time()
found_item = binary_membership_search(nums,target_key,0,len(nums))
time_search = time.time() - time_start

# Display the results.
print( "linear_membership_search for", target_key, ":", time_linear, "(s)" )
print( "binary_member_search for", target_key, ":", time_search, "(s)" )
print()
