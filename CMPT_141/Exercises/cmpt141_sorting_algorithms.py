# CMPT 141 - Sorting Algorithms
# contains implementations of sorting algorithms from the readings:
#  - insertion sort
#  - merge sort
#  - quick sort
# for use with "cmpt141-ch17-demo-02.py"


#########################
# INSERTION SORT
#########################

def insertion_sort(U):
    """
    creates new sequence containing sorted data of U where
    data is sorted using insertion sort
    U: sequence to sort
    return: new sequence where U is sorted
    """

    # create an empty sequences for S
    S = []

    # insert each item in U into S
    for item in U:
        i = 0

        # search for the offset at which to insert the
        # item into S.
        while i < len(S) and item >= S[i]:
            i = i + 1

        # insert the item immediately before the item at offset i.
        # this also shifts the items at offset i and higher
        # to the right to make space for the new item.
        S.insert(i, item)

    return S


#########################
# MERGE SORT
#########################

def merge(S1, S2):
    """
    combines two sorted sequences into single sorted sequence
    S1: sorted sequence to combine
    S2: other sorted sequence to combine
    return: single sorted sequence of S1, S2 combined
    """
    # let S be an empty sequence
    S = []

    # repeatedly move the smallest item to S
    while len(S1) > 0 and len(S2) > 0:
        if S1[0] < S2[0]:
            S.append(S1[0])
            del S1[0]
        else:
            S.append(S2[0])
            del S2[0]

    # once one of S1 or S2 is empty, append the remaining
    # non-empty sequence to S.
    if len(S1) > 0:
        S.extend(S1)
    else:
        S.extend(S2)

    return S

def merge_sort(S):
    """
    sorts sequence S using merge sort
    S: a sequence of data items
    return: new sorted sequence of S
    """

    # if S is empty, or has only one item,
    # it's already sorted; just return it.
    if len(S) <= 1:
        return S

    # otherwise, divide problem into two sub-problems.
    # note use of integer division here to find the offset
    # of the "middle" data item in S.
    S1 = S[0:len(S)//2]
    S2 = S[len(S)//2:len(S)]

    # recursively solve sub-problems of sorting S1 and S2
    S1 = merge_sort(S1)
    S2 = merge_sort(S2)

    # conquer! (solve the original problem using the
    # solutions for the sub-problems)
    S = merge(S1, S2)
    return S


#################################
# Iterative MergeSort
# NOTE: SORTS IN PLACE
#################################

def imerge_sorted(S):
    """
    Sorts sequence S using merge sort.  When the sort is done, S has changed.
    *** Sorts in place will change the input list***
    S: a sequence of data items
    return: None
    """
    seg_len = 1
    while seg_len < len(S):
        for seg_start in range(0,len(S),2*seg_len):
            T = merge(S[seg_start         : seg_start+seg_len],
                      S[seg_start+seg_len : seg_start+2*seg_len])
            for i in range(len(T)):
                S[seg_start+i] = T[i]
        seg_len = seg_len * 2


#########################
# QUICK SORT
#########################

def quick_sort(S):
    """
    sorts sequence S using quick sort
    S: array of data items to be sorted
    return: sorted sequence of S
    """

    # if S contains 0 or 1 items, it's already sorted
    if len(S) <= 1:
        return S

    # divide step of quick sort
    L = []      # for items smaller than the pivot
    E = []      # for items equal to the pivot
    G = []      # for items greater than the pivot
    p = S[0]    # first data item is the pivot

    for x in S:
        if x < p:
            L.append(x)
        elif x > p:
            G.append(x)
        else:
            E.append(x)

    # recursively solve subproblems of sorting L and G
    L = quick_sort(L)
    G = quick_sort(G)

    # conquer!
    S = L + E + G
    return S
