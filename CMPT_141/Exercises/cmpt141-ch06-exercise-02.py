# CMPT 141 - Modules
# Topic: turtle Module

import turtle as tg  # short for 'turtle graphics'


# set turtle cursor for cuteness & amusement
# set speed so we can see what we're drawing
tg.shape("turtle")
tg.speed(2)

# set turtle's orientation facing north
tg.setheading(90)

# draw blue square with side length 200, bottom-right at (0,0)
tg.fillcolor("blue")
tg.begin_fill()
tg.forward(200)
tg.right(90)
tg.forward(200)
tg.right(90)
tg.forward(200)
tg.right(90)
tg.forward(200)
tg.end_fill()

# bind quitting to mouse click so turtle doesn't exit after drawing
tg.exitonclick()
