# CMPT 141 - Data, Expressions, Variables, and Console I/O
# Topic: Output
#
# NOTE: print() inserts spaces before & after expressions by default.
# You can disable the spaces by adding named argument 'sep=""' at the end of the print call.
# This sets the default seperator value before & after expressions to the empty string. Voila!


# Variables initialized for the examples
price_per_blanket = 8
price_per_pillow = 12
cash_in_wallet = 50
my_char = 'o'
n_repeat = 10


# Part (d)
# "If I buy one blanket for $(cost of blanket) with the $(amount of cash in wallet) in my wallet, I will have $(amount of change) left."
print("If I buy one blanket for $"+str(price_per_blanket), "with the $"+str(cash_in_wallet), "in my wallet, I will have $"+str(cash_in_wallet-price_per_blanket), "left.")
#or
print("If I buy one blanket for $",price_per_blanket," with the $",cash_in_wallet," in my wallet, I will have $",cash_in_wallet-price_per_blanket," left.",sep="")

# Part (e)
# ”Google” with n repeat number of o’s instead of just two
print("G"+my_char*n_repeat+"gle")
#or
print("G",my_char*n_repeat,"gle",sep="")
