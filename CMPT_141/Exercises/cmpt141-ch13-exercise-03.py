# CMPT 141 - Arrays
# Topic(s): Operations on Arrays


import numpy as np

# Part (a)
def field_readings( sensors ):
    """
    returns new copy of sensors where readings are converted from F to C
    nums: array of sensor values stored in Fahrenheit
    return: copy of sensors where values are converted to Celsius
    """
    sensors_c = (sensors - 32) * (5/9)
    return sensors_c

# Part (b)
def defective_goods( defects,goods_processed ):
    """
    returns copy of defects where equipment is faulty (10% or more goods
    reported defective at that station)
    defects: array of number of defective goods processed
    goods_processed: number of goods processed per station
    return: copy of defects where faulty equipment is True, otherwise False
    """
    faulty_equip = defects >= goods_processed * 0.1
    return faulty_equip
