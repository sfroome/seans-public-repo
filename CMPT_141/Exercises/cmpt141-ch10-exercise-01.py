import random
import matplotlib.pyplot as mplt

# initialize game_records with randomized win/loss values for 100 games
game_records = []  # win/loss records where 1 means win, 0 means loss
n_games = 100  # number of games to simulate
# for every game, simulate and mark win or loss and mark it in the records
for x in range(0,n_games):
    game_records.append( random.randint(0,1) )


# generate running average of first x records in increments of ten
avg_records = []  # to contain running win ratio for first x games
for g in range(10,n_games+1,10):
    avg_records.append( sum(game_records[0:g])/g )
    print("Win Ratio after first",g,"games:",avg_records[-1])



# plot running win ratio to matplotlib
mplt.plot(range(10,n_games+1,10),avg_records,'bo')
mplt.axis([0,n_games+10,0.0,1.0])
mplt.xlabel("Number of Games Played")
mplt.ylabel("Win Ratio")
mplt.title("Running Win Ratio Over Games Played")
mplt.show()
