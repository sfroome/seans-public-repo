# CMPT 141 - Functions and Objects
# Topic: Function Headers


# Part (a)
def say_hello():
    """
    Prints a form of hello in French ("Bonjour")
    """
    print("Bonjour")

# Part (b)
def copycat(phrase):
    """
    Prints the given phrase five times in a row to the screen.

    phrase: string to print
    """
    print(phrase*5)

# Part (c)
def high_score(score1,score2,score3):
    """
    Returns the highest score of the given numbers.

    score1: Score to check
    score2: Another score to check against
    score3: Yet another score to check against

    return: The highest score of all the numbers.
    """
    return max(score1,score2,score3)
