# CMPT 141 - Arrays
# Topic(s): 1D Arrays


import numpy as np

# distances from current location to all coffee shop chain locations in city
coffee_shops = [5.5, 2.6, 12.5, 22.2, 0.45, 1.32, 3.3, 8.3, 6.2, 9.1]


# Part (a)
distances = np.array(sorted(coffee_shops))

# Part (b)
print("Number of coffee shops in the city:",distances.size)

# Part (c)
print("The furthest coffee shop is",distances[-1],"km away.")

# Part (d)
for d in distances[:5]:
    print(d)

