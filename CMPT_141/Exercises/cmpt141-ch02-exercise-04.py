# CMPT 141 - Data, Expressions, Variables, and Console I/O
# Topic: Variable Initialization

# Part (a)
0.5 * 3.5 * 2

# Part (b)
-(-2 ** 5)

# Part (c)
(33 % 2 + 4.5) * 2 / (-8-3) + -2 ** 3/3
