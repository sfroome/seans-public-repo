# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Relational Operators


# Variables initialized for the exercises
animal_a        # string of animal A's species
animal_b        # string of animal B's species
max_length      # int of max chars animal name can be

# Part (a)
animal_a < animal_b

# Part (b)
animal_a != animal_b

# Part (c)
len(animal_a) > max_length

# Part (d)
max(len(animal_a),len(animal_b)) <= max_length
