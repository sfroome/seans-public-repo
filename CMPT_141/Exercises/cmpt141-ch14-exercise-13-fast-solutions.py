# CMPT 141 - Recursion
# Topic(s): Recursion Efficiency
# Iterative Solution
# 
# The interative solution keeps track of two ancestor totals
# representing level L and level L+1.  
# From these two totals, we can compute the ancestor totals 
# representing level L+1 and level L+2.
# 
# The algorithm can be implemented as a for-loop, or a recursive function
  

def fastbees(N):
    """
    returns the number of ancestors a male bee has at its Nth generation
    N: number of generations back to count ancestors for the bee
    return: number of male bee's ancestors at generation N
    """
    # always remember 2 levels' total # bees
    # initially level 0 and level 1
    this_level = 1
    next_level = 1

    # repeatedly compute the next pair of totals
    for generation in range(N):
        # remember the previous level
        prev_level = this_level
        # copy the current level
        this_level = next_level
        # calculate the next level
        next_level = prev_level + this_level
    
    return this_level


def fastbees_rec(L, N, this_level, next_level):
    """
    returns the number of ancestors a male bee has at its Nth generation
    N: number of generations back to count ancestors for the bee
    l: the current generation
    this_level: the number of ancestors for generation L
    next_level: the number of ancestors for generation L+1
    return: number of male bee's ancestors at generation N
    """
    if L == N:
        return this_level
    else:
        return fastbees_rec(L+1, N, next_level, this_level+next_level)

