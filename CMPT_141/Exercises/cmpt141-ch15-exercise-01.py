# CMPT 141 - Testing and Debugging
# Topic(s): Black-Box Test Case Generation


def is_valid_username( username ):
    """
    determines if username meets these constraints:
      - 1-18 characters long
      - can be letters and/or numbers
      - underscore is allowed if it's not the first character
    username: string to check validity of
    return: True if username satisfies constraints
    """
    is_valid = True  # whether constraints are satisfied

    # check length requirements
    if len(username) == 0 or len(username) > 18:
        is_valid = False

    # check character requirements
    else:
        i = 0  # current character index
        while is_valid and i < len(username):
            u = username[i]  # current character being examined
            if not (u.isalpha() or u.isdigit() or (u=="_" and username[0]!="_")):
                is_valid = False
            i = i + 1

    return is_valid
