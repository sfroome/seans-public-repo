# CMPT 141 - Control Flow: Branching and Repetition
# Topic: For-Loops


n_divisible = 0  # count of divisible numbers found

for num in range(300,601):
    # found divisible number, so add to count & print
    if (num % 3 == 0) and (num % 5 == 0):
        n_divisible = n_divisible + 1
        print(num)

print("Divisible numbers found:",n_divisible)
