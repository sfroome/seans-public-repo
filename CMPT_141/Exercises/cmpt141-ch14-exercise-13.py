# CMPT 141 - Recursion
# Topic(s): Recursion Efficiency

# Fibonacci Bees
#
# Why are the number of ancestors of a male bee described by the
# Fibonacci numbers?
#   
#  1.  Assumption: Level zero is the male bee itself.  It is included
#      in the number of ancestors, by definitional convenience.
#      Assumption: None of the male bees is Eric, the half-a-bee.
#  2.  A male bee has exactly one parent, its mother.
#  3.  For every L >= 0, we define:
#         let M(L) be the number of male ancestors at level L
#         let F(L) be the number of female ancestors at level L
#         let T(L) = M(L) + F(L) be the total ancestors at level L
#  4.  The number of female bees at level L
#         F(L) = M(L-1) + F(L-1) = T(L-1)
#      because each bee has a female parent
#  5.  The number of male bees at level L
#         M(L) = F(L-1)
#      because each female bee has a male parent.
#  6.  Since F(L) = T(L-1) 
#         M(L) = F(L-1) = T(L-2)
#  7.  Therefore the total number of bees at level L
#         T(L) = M(L) + F(L) 
#              = T(L-2) + T(L-1)
#  8.  Insight: The formula for T(L) describes the Fibonacci numbers.


def bees( N ):
    """
    returns the number of ancestors a male bee has at its Nth generation
    N: number of generations 
    return: number of male bee's ancestors 
    """

    # base cases
    # at the zeroth generation, a male bee has one ancestor (itself)
    # at the first generation, a male bee has one ancestor (its mother)
    if N == 0 or N == 1:
        return 1

    # recursive case
    # the number of male ancestors at level N is bees(N-2)
    # the number of female ancestors at level N is bees(N-1)
    else:
        return bees(N-1) + bees(N-2)

