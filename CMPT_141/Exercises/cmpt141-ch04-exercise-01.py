# CMPT 141 - Functions and Objects
# Topic: Function Composition


# Part (a)
def say_hello():
    print("Bonjour")

# Part (b)
def copycat(phrase):
    print(phrase*5)

# Part (c)
def high_score(score1,score2,score3):
    return max(score1,score2,score3)
