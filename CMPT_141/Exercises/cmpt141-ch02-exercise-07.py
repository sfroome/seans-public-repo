# CMPT 141 - Data, Expressions, Variables, and Console I/O
# Topic: Output
#
# NOTE: print() inserts spaces before & after expressions by default.
# You can disable the spaces by adding named argument 'sep=""' at the end of the print call.
# This sets the default seperator value before & after expressions to the empty string. Voila!


# Variables initialized for the examples
price_per_blanket = 8
price_per_pillow = 12
cash_in_wallet = 50
my_char = 'o'
n_repeat = 10

# Part (a)
# "One blanket costs (price) dollars."
print("One blanket costs"+str(price_per_blanket), "dollars.")
# or
print("One blanket costs ", price_per_blanket, " dollars.", sep="")

# Part (b)
# "If I buy four pillows, that will cost $(cost of blankets.)"
print("If I buy four pillows, that will cost $"+str(price_per_blanket*4)+".")
#or
print("If I buy four pillows, that will cost $",price_per_blanket*4,".",sep="")

# Part (c)
# "If I buy one blanket and two pillows, that will cost $(total cost of all items).
#  If I pay with $(amount of cash in wallet), I will have $(amount of change left)."
print("If I buy one blanket and two pillows, that will cost $"+str(price_per_blanket+price_per_pillow*2)+". If I pay with $"+str(cash_in_wallet)+", I will have $"+str(cash_in_wallet-(price_per_blanket+price_per_pillow*2)), "left.")
#or
print("If I buy one blanket and two pillows, that will cost $",price_per_blanket+price_per_pillow*2,". If I pay with $",cash_in_wallet,", I will have $",cash_in_wallet-(price_per_blanket+price_per_pillow*2)," left.",sep="")

# Part (d)
# "If I buy one blanket for $(cost of blanket) with the $(amount of cash in wallet) in my wallet, I will have $(amount of change) left."
print("If I buy one blanket for $"+str(price_per_blanket), "with the $"+str(cash_in_wallet), "in my wallet, I will have $"+str(cash_in_wallet-price_per_blanket), "left.")
#or
print("If I buy one blanket for $",price_per_blanket," with the $",cash_in_wallet," in my wallet, I will have $",cash_in_wallet-price_per_blanket," left.",sep="")

# Part (e)
# ”Google” with n repeat number of o’s instead of just two
print("G"+my_char*n_repeat+"gle")
#or
print("G",my_char*n_repeat,"gle",sep="")
