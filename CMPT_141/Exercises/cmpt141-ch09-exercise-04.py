# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Nested Loops


def print_image_coords( M,N ):
    """
    Prints an M x N image's coordinates.
    M: number of columns in image (width)
    N: number of rows in image (height)
    """

    # start canvas coordinates on new line
    print()

    # for every row
    for r in range(0,N):
        # for every column
        for c in range(0,M):
            print("(",c,",",r,")",sep="",end=" ")
        print()
