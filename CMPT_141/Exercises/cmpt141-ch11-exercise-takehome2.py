# CMPT 141 - Dictionaries
# Topic(s): Dictionary Usage (Records), List of Dicts


# pool of participants for study
study_pool = [
    { "id": 100303, "age": 21 },
    { "id": 100319, "age": 34 },
    { "id": 101318, "age": 19 },
    { "id": 100304, "age": 22 },
    { "id": 100296, "age": 29 },
    { "id": 100222, "age": 23 },
    { "id": 100265, "age": 34 }
]

# assign participants to receive actual or placebo drug based on list order
# (alternate between drug and placebo starting with giving placebo)
give_placebo = True  # whether or not to give participant placebo
for participant in study_pool:
    participant["on_placebo"] = give_placebo
    give_placebo = not give_placebo  # next participant gets other option
