# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Logical Operators


# Variables initialized for the exercises
has_fins    # whether animal has fins
has_wings   # whether animal has wings
has_stripes # whether animal has stripes
has_spots   # whether animal has spots
has_tail    # whether animal has tail

# Part (a)
not has_tail

# Part (b)
has_tail and has_stripes

# Part (c)
has_stripes or has_spots

# Part (d)
(has_stripes or has_spots) and has_tail

# Part (e)
has_fins or has_wings or has_tail

# Part (f)
not(has_wings and has_fins)

# Part (g)
not(has_wings or has_fins)

# Part (h)
((has_wings or has_fins) and not has_tail) and (has_stripes or has_spots)
