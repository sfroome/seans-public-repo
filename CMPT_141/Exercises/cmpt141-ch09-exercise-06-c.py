# CMPT 141 - Control Flow: Branching and Repetition
# Topic: Loop Classification

# Part (c)
x = 5
goal = 16
while x != goal:
    x = x + 2
