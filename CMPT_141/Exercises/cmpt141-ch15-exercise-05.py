# CMPT 141 - Testing and Debugging
# Topic(s): Debugging (with Loops)


# mapping of TV provider's channel numbers to genre
all_channels = { 100: "movies", 101: "movies", 102: "movies", 133: "music",
                 200: "basic", 202: "basic", 203: "basic", 205: "basic",
                 260: "comedy", 261: "comedy", 263: "comedy", 270: "food",
                 281: "youth", 283: "youth", 285: "special", 286: "special",
                 300: "sports", 301: "sports", 302: "sports", 303: "sports",
                 350: "multicultural", 360: "multicultural", 390: "special" }

def channel_listing( subscription ):
    """
    yields channel numbers listing for genres in subscription
    subscriptions: list of channel genres user is subscribed to
    return: list of channel numbers for subscribed channels
    """
    channels = []  # list of channel numbers subscribed to

    # for each genre subscribed to, add its corresponding channels to listing
    for genre in subscription:
        genre_channels = [ c for c in all_channels if all_channels[c]==genre ]
        channels.extend( genre_channels )

    # return listing of channels
    return channels

print(channel_listing(['basic', 'special', 'movies']))