#include <iostream>

using namespace std;

void dispArray(const int primes[], int length);

int main()

{
    const int MAX=100;
    int primes[MAX] ={};


    bool isprime;
    int k=0;

    int start_time = time(NULL);
    int end_time;

    for (int i=2; i < MAX; i++)
    {
	isprime =true;
	for (int j=0; j < k && isprime==true; j++)
	{
	    if ( i%primes[j] ==0)
	    {
		isprime=false;
	    }
	}
	if (isprime)

	{
	    primes[k++]=i;
	    dispArray (primes, k);
	}

    }
    end_time = time(NULL)-start_time;
    cout << "time: " << end_time << endl;
    cin.get();
}


void dispArray(const int primes[], int length)
{
    for ( int i=0; i < length-1; i++)
    {
	cout << primes[i] << ",";
    }
    cout << primes[length-1] << endl;
}
