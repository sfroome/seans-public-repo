#include <iostream>
using namespace std;

bool IsStrictInc(const int vals[], int len, int & errpos );

int main()

{
    int vals[] = {1,2,3,4,5}, len = 5, errpos;
    
    if( IsStrictInc(vals,len,errpos) == true)
    {
	cout << "Strictly Increasing Sequence" << endl;
    }
    else 
    {
	cout << "Stops increasing at position " << errpos << endl;
    }

}


bool IsStrictInc(const int vals[], int len, int & errpos)
{
    bool  inc = true;
    for(int i = 1; i < len-1 && inc == true; i++)
    {
	if(vals[i] <= vals[i-1])
	{
	    inc = false;
	    errpos = i;
	}
    }
    return inc;
}
