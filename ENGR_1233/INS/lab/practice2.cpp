#include <iostream>
#include <iomanip>

using namespace std;


int main()
{
    const int rows=4, cols =4;
    int array[rows][cols] = {};

    for( int i = 0; i < rows; i++)
    {
	for( int j =0; j < cols; j++)
	{

	    array[i][j]= i*j;
	    cout << setw(5)<< array[i][j];
	}
	cout << endl;
    }

 

}
