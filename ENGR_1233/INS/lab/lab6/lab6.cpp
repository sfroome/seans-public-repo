#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
using namespace std;

int main()
{
    cout << fixed << setprecision(0);
    double time, x, y, z;
    double vel;
    double oldx = 0, oldy = 0, oldz =  0, oldtime = 0;
    double maxvel = 0;
    ifstream fin;
    ofstream fout;
    string line1, line2, line3, line4;
    fin.open("/users/library/engr/ENGR1233/W2013/VehicleTrajectory.txt");
    if (fin.fail())
    {
	cout << "Could not open file" << endl;
	exit(-1);
    }
    fin.ignore(10000, '\n');

    getline(fin,line1, '\n'); 
    getline(fin,line2, '\n');
    getline(fin,line3, '\n');
    getline(fin,line4, '\n');

    for(int i = 0; i < 9; i++)
    {
	fin.ignore(10000, '\n');  
    }   

    fin >> time >> x >> y >> z;
    fin.ignore(1000, '\n');
    oldx = x;
    oldy = y;
    oldz = z;
    oldtime = time;
    
    while( fin >> time >> x >> y >> z )
    {
	fin.ignore(1000, '\n');
	vel = (sqrt((pow(x - oldx, 2))
		    + (pow(y - oldy, 2))
		    + (pow(z - oldz, 2))))
	            /(time - oldtime);
	if( vel > maxvel)
	{
	    maxvel = vel;
	}
	oldx = x;
	oldy = y;
	oldz = z;
	oldtime = time;
    }		  
    fin.close();
    maxvel = maxvel*3600/1000;


    fout.open("output.txt");
    if(fout.fail())
    {
	cout <<"Could not open or create output file" << endl;
	exit(-1);
    }

    fout << line1 << endl;
    fout << line2 << endl;
    fout << line3 << endl;
    fout << line4 << endl;
    fout << "Maximum Velocity: " << maxvel << " km/hr" << endl;
    fout.close();
}
