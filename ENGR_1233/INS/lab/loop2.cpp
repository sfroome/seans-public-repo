#include <iostream>
#include <cmath>
using namespace std;


int main()
{
    double low = 0, high = 2, mid = 1, mid_squ = exp(mid) - mid - 2;
    const double EPSILON = 1e-8;

    while(abs(mid_squ) > EPSILON)
    {
	if (mid_squ < 0 )
	{
	    cout << "too low" << endl;
	    low = mid;
	}
	else if ( mid_squ > 0 )
	{
	    cout << "too high" << endl;
	    high = mid;
	}
	mid = ( low + high )/2;
	cout << "next guess " << mid << endl;
	mid_squ = exp(mid) - mid - 2;
    }
    cout << "The root of e^x - x - 2 is equal to " << mid << endl;
}
