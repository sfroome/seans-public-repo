#include <iomanip>
#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;




struct Marks
{
    double test1, test2, average;
};

struct Student
{
    string name;
    int id;
    Marks marks;
};

void ReadMarks( ifstream & fin, Student list[], int totalmarks );  // reads name, id, and marks
void CalcAve( Student & s);
void WriteMarks( ofstream & fout, Student list[], int totalmarks ); // writes id and average to file

int main()
{
    ifstream fin( "infile.dat");
    ofstream fout( "outfile.dat" );
    if ( fin.fail() || fout.fail() )
    {
	cout << "Couldn't open file" << endl;
	exit(-1);
    }
    Student list[3];
    ReadMarks( fin, list, 3 );
    for ( int i = 0; i < 3; i++ )
    {
	CalcAve( list[i]);  
    }
    fin.close();
    WriteMarks( fout, list, 3 );
    fout.close();
}

void ReadMarks( ifstream & fin, Student list[], int totalmarks )
{
    for(int i = 0; i < totalmarks; i++)
    {
	getline(fin, list[i].name, '\t'); //'\t' is tabbed white space
	fin >> list[i].id >> list[i].marks.test1 >> list[i].marks.test2;
	fin.ignore(1000, '\n');
    }
}

void CalcAve( Student & s)
{
    s.marks.average = (s.marks.test1+s.marks.test2)/2;
}

void  WriteMarks( ofstream & fout, Student list[], int totalmarks )
{
    for(int i = 0;  i < totalmarks; i++)
    {
	fout << left;
	fout << setw(30) << list[i].name 
	     << setw(30) << list[i].id 
	     << setw(30) << list[i].marks.test1
	     << setw(30) << list[i].marks.test2
	     << endl;
	fout << "The average is: "
	     << list[i].marks.average
	     << endl;
	fout << endl;    
    }
}
