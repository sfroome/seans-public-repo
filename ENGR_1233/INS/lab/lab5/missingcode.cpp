#include <iostream>
using namespace std;

const int SIZE = 3;

void multiply( int  A[][SIZE], int  x[], int  b[] );
void add( int  x[], int  b[] );
void displayA( const int A[][SIZE] ); // display SIZE by SIZE matrix 
void displayxb( const int xb[] );   //display vector of length SIZE

int main()
{
    int A[SIZE][SIZE] =  {{1,2,1},{2,3,2},{3,4,3}};
    int x[SIZE] = {1,0,2};
    int b[SIZE];
    multiply( A, x, b );  // multiplies A with x and stores the result in b
    displayxb( b );
    add(x, b );   // adds x and b and stores the result in b
    displayA(A);
    displayxb( x );
    displayxb( b );
}
void multiply(int  A[][SIZE], int  x[], int  b[])
{
    int sum = 0;
    for( int i = 0; i < SIZE; i++)
    {
	for (int j = 0; j < SIZE; j++)
	{
	    b[i] = A[i][j]*x[j];
	    sum += b[i];   
	    
	}
	b[i] = sum;
	sum = 0;
    }
}
void displayA( const int A[][SIZE] )
{
    cout << endl;
    for(int i = 0; i < SIZE; i++)
    {
	for(int j = 0; j < SIZE; j++)
	{
	    cout << A[i][j];
	}
	cout << endl;
    }
}

void displayxb( const int vec[] ) 
{
    cout << endl;
    for(int i = 0; i < SIZE; i++)
    {
	cout << vec[i] << endl;
    }
}

void add( int  x[], int  b[] )
{
    for ( int i = 0; i < SIZE; i++)
    {
	b[i] = x[i]+b[i];
    }

}
