#include <iostream>
using namespace std;

int getkelvin();

int kelvintocelsius(int degk);

void displayresult(int degk, int degc);


int main()
{
    int degk, degc;
    degk = getkelvin();
    degc = kelvintocelsius(degk);
    displayresult(degk, degc);

}

int getkelvin()
{
    int degk;
    cout << "Enter a temperture in kelvin" << endl;
    cin >> degk;
    return degk;
}

int kelvintocelsius(int degk)
{
    int degc;
    degc = degk - 273;
    return degc;
}

void displayresult( int degk, int degc)
{
    cout << "The temperature in kelvin is " << degk << endl;
    cout << "The temperature in celsius is " << degc << endl;
}
