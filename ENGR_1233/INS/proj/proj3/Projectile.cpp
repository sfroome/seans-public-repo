/************************************************************
File: Projectile.cpp
Author: Sean Froome
Description: Program calculates the motion of a projectile
using position and time calculations. The program gets 
an initial velocity and angle from the user and outputs
position at each second after object is released until it 
hits the ground.

TEST CASES
Case1: User enters a bad value for the angle
Input given: -1 ("no", 180, 120, 91 also tested)
Expected Output:
Negative velocity, or non numeric input entered
Please try again:
(User is prompted to enter a different angle)

Case2: User enters a very large initial velocity
Input Given: 123456 (angle 45)
Expected Output:

(Max and min values are cut off.
Tested on a laptop running Ubuntu Linux
On INS, values might not have been cut off)
(1-499 values are okay, and all display)
500                 0.00                87296.57            

Program failed to find the correct values
Object in air longer than max bound of program.
(Any angle should give a proper answer, as long 
as the initial velocity isn't too big)

X position at 500 is 0.00 because program gets cut
off, and a value can't be assigned to it
87296.57 appears only after calcvelocities is called.
Until then, posy[total_time] is 0.00 as it should be.
I have no idea why it changes, as the array isn't
modified after calcpositions. 
The value changes depending on the initial values entered, 
but only appears if it takes longer than 500 seconds for
the object to hit the ground

Case3: User enters reasonable values
Input given: 45 (degrees), 200(meters/second squared)
Expected Output:

Initial Velocity: 200.00m/s

Angle: 45.00 degrees

Max Height: 1019.37m

Max Distance: 4077.47m

Time (seconds)      Position (x)        Position (y)        
-------------------------------------------------------
0                   0.00                0.00                
1                   141.42              136.52              
2                   282.84              263.24              
3                   424.26              380.16              
4                   565.69              487.29              
5                   707.11              584.61              
6                   848.53              672.13              
7                   989.95              749.85              
8                   1131.37             817.77              
9                   1272.79             875.89              
10                  1414.21             924.21              
11                  1555.63             962.73              
12                  1697.06             991.46              
13                  1838.48             1010.38             
14                  1979.90             1019.50             
15                  2121.32             1018.82             
16                  2262.74             1008.34             
17                  2404.16             988.06              
18                  2545.58             957.98              
19                  2687.01             918.11              
20                  2828.43             868.43              
21                  2969.85             808.95              
22                  3111.27             739.67              
23                  3252.69             660.59              
24                  3394.11             571.71              
25                  3535.53             473.03              
26                  3676.96             364.56              
27                  3818.38             246.28              
28                  3959.80             118.20              
29                  4101.22             0.00     

************************************************************/

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

double GetAngle();
double GetVelocity();
int CalcPositions(double posx[], double posy[], double v0, double angle);
void CalcVelocities( double velx[], double vely[],
		     double v0, double angle, int total_time );
void MaxPos(double v0, double angle, double & maxx, double & maxy);
void ShowTrajectory( const double posx[], const double posy[],
		     int total_time, double velocity, double angle);
const double g = -9.81; //Acceleration due to Gravity
const int SIZE = 500; //Size of all Arrays

int main()
{
  double angle, v0; // angle and initial (magnitude) velocity 
  double posx[SIZE] = {};
  double posy[SIZE] = {};
  double velx[SIZE] = {};
  double vely[SIZE] = {};
  int total_time;
  
  // Sets all values to have two decimal places when outputted  
  cout << fixed << setprecision(2);
  
  // Function calls
  angle = GetAngle();
  v0 = GetVelocity();
  total_time = CalcPositions(posx, posy, v0, angle);
  CalcVelocities(velx,vely, v0, angle, total_time);
  ShowTrajectory( posx, posy, total_time, v0, angle);
}

//Gets an angle (in degrees) from user, (converts it to radians) and returns it
double GetAngle()
{
  double angle;
  cout << "Enter an angle between 0 and 90 degrees" << endl;
  cin >> angle;
  while(angle <=0 || angle > 90 || cin.fail())
    {
      cout << "Angle isn't between 0 and 90, "
	   << " or bad input entered. " << endl;
      cout << "Please Reenter:" << endl;
      cin.clear();
      cin.ignore(100,'\n');
      cin >> angle;
    }
  angle = angle*M_PI/180;
  return angle;
}

//Gets an initial velocity and returns it
double GetVelocity()
{
  double velocity;
  cout << "Enter an initial velocity" << endl;
  cin >> velocity;
  while( velocity <= 0 || cin.fail())
    {
      cout << "Negative velocity, or non numeric input entered." << endl;
      cout << "Please try again:" << endl;
      cin.clear();
      cin.ignore(100,'\n');
      cin >> velocity;
    }
  cout << endl;
  return velocity;
}

//Calculates x and y position at time t and stores the values in arrays
int CalcPositions(double posx[], double posy[], double v0, double angle)
{
  int time;
  for(time = 0; time < SIZE; time++)
    {
      posx[time] = cos(angle)*v0*time;
      posy[time] = -4.9*pow(time,2) 
	+ sin(angle)* v0*time;
      if(posy[time] < 0)
	{	
	  posy[time] = 0;
	  break;
	}
    }
  return time; 
}

//Calculates horizontal and vertical velocity and stores the values in arrays
void CalcVelocities( double velx[], double vely[],
		     double v0, double angle, int time)
{
  for(int i = 0; i <= time; i++) 
    {
      velx[i] = cos(angle)*v0;     
      vely[i] = g*i + sin(angle)*v0;
    }
}

//Calculates max height and max distance
void MaxPos(double v0, double angle, double & maxx, double & maxy)
{
  maxx = abs((pow(v0,2)*sin(2*angle)/g)); 
  maxy = pow((sin(angle)*v0),2)/(-2*g);
}

//Displays position at time "t", initial velocity and angle
void ShowTrajectory( const double posx[], const double posy[],
		     int total_time, double v0, double angle)
{
  double maxx, maxy;
  MaxPos(v0, angle, maxx, maxy);
  cout << "Initial Velocity: " 
       << v0 << "m/s" << endl << endl;
  cout << "Angle: " 
       << angle*180/M_PI 
       << " degrees" << endl << endl;
  cout << "Max Height: " 
       << maxy << "m" << endl << endl;
  cout << "Max Distance: "
       << maxx 
       << "m"<< endl << endl; 
  cout << left << setw(20) << "Time (seconds)" 
       << setw(20) << "Position (x)" 
       << setw(20) << "Position (y)" << endl;
  cout << left << setw(55) << setfill('-') << "-"<< endl;
  cout << setfill(' ') << setw(20);
  
  cout << total_time << "&&" << posx[total_time] 
       << "&&" << posy[total_time] << endl;
  for (int i = 0; i <= total_time; i++)
    {
      cout << left  <<setw(20)<< i  
  	   << setw(20) << posx[i]  
	   << setw(20) << posy[i] << endl;
    }
  
  if( posy[total_time-1] > 0 && total_time == SIZE)
    {
      cout << endl;
      cout << "Program failed to find the correct values" << endl;
      cout << "Object in air longer than max bound of program." << endl;
    } 
}
