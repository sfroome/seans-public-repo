#include <iostream>
#include <cmath>

using namespace std;

int main()

{
  const int r =100, MAX_ITER = 15;
  const double EPSILON = 1e-8;
  double R, a;
  double fa, fpa; // f(a) and f'(a)

do
  {
  cout << "Enter an initial guess for theta in degrees" << endl;
  cin >> a;

  while (a <= 0 || a > 90 || cin.fail()) 
    {
      cout << "Bad input detected."
	   << "Angle must be smaller than 0, "
	   << "or larger than 90. " 
	   << "Please reenter" 
	   << endl;
      cin.clear();
      cin.ignore(100,'\n');
      cin >> a;
    }


  a = a * M_PI/180; // Converting to radians from degrees



  for (int i = 0; i < MAX_ITER; i++)
    {
      // Function
      fa = 4*a*pow(cos(a),2)     
	+ M_PI/2 - 2*a - sin(2*a);

      //Derivative of Function
      fpa = - 8*cos(a)*sin(a);

      // Obtaining a new angle
      a = a - fa/fpa;

      //      cout << a << endl;   
    }

  // Formula to find length of rope
  R = 2*r*cos(a); 
  
  // Converting back to degrees
  a = a * 180/M_PI; 
  
  // Checks to confirm Newton's Method is accurate
  if (a < 0 || a > 90)
    {
      cout << "Newton's Method failed to find the length of the rope." << endl;
      cout << "Try again with a different initial guess." << endl;	 	 
    }
  
  if (abs(4*a*pow(cos(a),2) + M_PI/2 - 2*a - sin(2*a)) < EPSILON)
    {
      cout << "Newton's Method failed to find the length of the rope." << endl;
      cout << "Try again with a different initial guess." << endl;
    }
  
  }
 
// Program will prompt user for another guess if Newton's Method failed.
 while (a <= 0 || a > 90); 
 
 cout << "The angle is " << a << " degrees." << endl;
 
 cout << " The length of the rope is " << R << endl;
 
 
}
