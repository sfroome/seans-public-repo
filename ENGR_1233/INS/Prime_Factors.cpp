/***********************************************************************************************
Filename: Prime_Factors.cpp
Author: Sean Froome
Description: This program determines all the prime factors of a user's inputted integer

TEST CASES

Case1: User doesn't enter a number (input given: a)
Expected Output:
Improper input detected.Please enter an integer between 1 and 100:
(User is prompted to enter a number)

Case2: User enters a number bigger than 100 or smaller than 1 (tested: 101, 0)
Expected Output:
Improper input detected.Please enter an integer between 1 and 100:
(User is again prompted to enter a number, within the bounds)

Case3: User enters a number between 1 and 100, that's not a prime number (input given: 90)
Expected Output:
90 = 2*3*3*5

Case4: User enters a number that is prime (input given: 5)
Expected Output:
5 = 5

*************************************************************************************************/
#include <iostream>
#include <cmath> 
using namespace std;

int main()   
{
  
  int i, j, k = 0, l = 0, val, oldval; // integers used in the various arrays
  bool isprime; // bool to prove if a number is a prime number or not
  const int SIZE = 100; // Maximum size of the arrays
  int primes[SIZE]={};// array that stores all prime numbers between 1 and 100
  int factors[SIZE] = {}; // array that stores all the prime factors that make up the user's inputted number
  
  cout << "Enter an integer between 1 and 100" << endl;
  cin >> val;
  
  // If non numeric input is given or if an integer not between 0 and 100
  while(val < 1 || val > 100 || cin.fail())
    {
      cin.clear();
      cin.ignore(100,'\n');
      cout << "Improper input detected."
	   << "Please enter an integer between 1 and 100:" 
	   << endl;
      cin >> val;
    }
  
  oldval = val;
  
  // This for loop determines which numbers between 1 and 100 are prime
  for (i = 2; i <= val; i++)
    {   
      // i is the number being checked
      // Bool is set to be true 
      isprime = true; 
      for (j = 2; j <= sqrt(i) && isprime == true; j++)
	{
	  // If the remainder of i/j leaves no remainder the number can't be prime
	  // j increases until i%j  proven false, or j can't increase anymore.
	  if(i%j == 0)
	    {
	      isprime = false;
	    }
	}  
      
      if(isprime) 
	{
	  // If i is prime, it's added to the primes[] array
	  primes[k] = i; 
	  // k serves as a counter.
	  k++;
	}
      
    }
  
  cout << val << " = ";
  
  // This loop determines which prime numbers are factors of the user's number
  for (i = 0; i < k; i++)
    {
      while(val%primes[i] == 0)
	{
	  val = val/primes[i];	
	  factors[l] = primes[i];
	  l++;	 
	  // if a prime number from primes[] is a prime number of the user's number
	  // it is added to the factors[] array. 
	  // l is serves as a counter 
	}
    }
  
  // This loop outputs the factors to the screen
  for( int n = 0; n < l-1; n++)
    {   
      cout << factors[n] << "*";
    }
  
  // This cout statement shows the last factor
  if (factors[l-1] != 0)
    {
      cout << factors[l-1]; 
    } 

  if( val != 1)
    {
      // if the number has no factors, this ensures it will show itself (ex: 5 = 5)
      cout << val;
    }

  // if the original val = 1, then it will cout here. Otherwise, it will be some random value.
  else if(oldval == 1)
    {
      cout << oldval; 
    }
  cout << endl;  
 
}

