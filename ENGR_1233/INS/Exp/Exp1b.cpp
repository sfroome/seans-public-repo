#include <iostream>
#include <cmath>


using namespace std;



int main()
{
    const double L = 3;
    const double E = 7e10;
    const double I = 5.29e-5;
    const double w0 = 15000;
    double x; 
    double f;

    cout << "Enter a Number" << endl;
    cin >> x;

    f = ((w0*L)/(3*pow(M_PI,4)*E*I))*
	(48*pow(L,3)*cos((M_PI/2*L)*x)
	 - 48*pow(L,3)
	 +3*pow(M_PI,3)*L*pow(x,2)
	 -pow(M_PI,3)*pow(x,3));
    cout << f << endl;

}
