#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main()

{
    int n, denom=1, sign =1;
    double x, num, term, sum;

    cout << "Enter x in degrees "
	 << "and the number of terms to approximate arctan(x)" << endl;
    cin >> x >> n;

    x = x*M_PI/180;

    cout << setprecision(10) << endl;
    cout << "The actual value of arctan(x) is " << atan(x) << endl;

    num = x;
    sum = x;
    term = x;

    //  cout << "The first term is " << term << endl;

    for (int i =3; i <= 2*n - 1; i+=2)

	{
	    denom = i;
	    num = pow(x,i);
	    sign *= -1;
	    term = sign*num/denom;

//    cout << "Next term is " << term << endl;
	    sum += term;
	}

    cout << "The approximate value of arctan(x) is " << sum << endl;









}
