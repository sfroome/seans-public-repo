/*************************************************************************
Filename:Newtons_Method_v2.cpp
Author: Sean Froome
Description: Program uses Newton's Method to solve the "Gobbling Goat" 
problem. Unlike the previous version, this version includes functions
for better organization.

TEST CASES:
Case1:
Case2:
Case3:
Case4:
Case5:

*************************************************************************/
#include <iostream>
#include <cmath>
using namespace std;

//Function Definitions
double GetGuess();  
double DegToRad(double angle_rad);
double Newton(double angle_newton); 
double CalcRope(double angle_rope);
double RadtoDeg(double angle_deg);
void DisplayResult(double angle_final, double rope_final);
double f(double angle_f);
double fprime(double angle_fp);

//Global Constants
const int RADIUS =  100, MAX_ITER = 15;
const double EPSILON = 1e-8;

int main()
{
  double angle;
  double rope;
  angle = GetGuess();  
  angle = DegToRad(angle);
  angle = Newton(angle); 
  rope = CalcRope(angle); 
  angle = RadtoDeg(angle);
  DisplayResult(angle, rope);
}

//Getting initial guess from user in degrees
double GetGuess()  
{
  double guess; 
  cout << "Enter an initial guess for theta in degrees: " 
       << "The angle needs to be between 0 and 90"
       << endl;
  cin >> guess;

  while(guess < 0 || guess > 90 || cin.fail())
    {
      cin.clear();
      cin.ignore(100, '\n');
      cout << "Bad input detected."
	   << "Please reenter an angle"
	   << "Between 0 and 90"
	   << endl;
      cin >> guess;
    }
  return guess;
}

//Converting from Degrees to Radians
double DegToRad(double angle_rad) 
{
  angle_rad = angle_rad * M_PI/180;
  return angle_rad;
}

//Converting back to Degrees
double RadtoDeg(double angle_deg)
{
  angle_deg = angle_deg * 180/M_PI;
  return angle_deg;
}

//Newton's Method formula
double Newton(double angle_newton)
{
  for(int i = 0; i < MAX_ITER; i++)
    {
      angle_newton = angle_newton - f(angle_newton)/fprime(angle_newton);
    }
  return angle_newton;
}

//Displays results of Newton's Method
void DisplayResult(double angle_final, double rope_final)
{
  //If Newton failed, this will be the output.
  if( angle_final <= 0 || angle_final > 90 || f(angle_final*M_PI/180) > EPSILON )
    {
      cout << "Newton's Method failed to find the length of the rope" << endl;
    }
  //Angle and rope output from here
  else
    {
      cout << "The rope length is " << rope_final << endl;
      cout << "The angle is " << angle_final << endl;
    }
}

// This function will be called by Newton()
double f(double angle_f) 
{
  double fangle = 0;
  fangle = 4*angle_f*pow(cos(angle_f),2)
    + M_PI/2 - 2*angle_f - sin(2*angle_f);
  return fangle;
}

// This function will be called by Newton()
//This function is the derivative of the above function
double fprime(double angle_fp)
{
  double fprimeangle = 0;
  fprimeangle = 4*pow(cos(angle_fp),2)
    - 8*cos(angle_fp)*sin(angle_fp) - 2 - 2*cos(2*angle_fp);
  return fprimeangle;
}

//Calculates length of Rope when angle is found
double CalcRope(double angle_rope)
{
  double rope;
  rope = 2*RADIUS*cos(angle_rope);
  return rope;
}
