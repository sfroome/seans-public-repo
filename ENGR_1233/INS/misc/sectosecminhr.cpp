#include<iostream>
#include<iomanip>
using namespace std;

void sectosecminhr(int & sec, int & min, int & hr);
void displayvalues(int sec, int min, int hr);
void addsec(int & sec, int & min, int & hr);


int main()
{
    int sec, min, hr;

    cout << "Enter the number of seconds " << endl;
    cin >> sec;
    while(1)   
    {
	sectosecminhr(sec, min, hr);
	addsec(sec, min, hr);
	displayvalues(sec, min, hr);  
	 //cout << setfill('0')<< hr << ":" << min << ":" << sec << endl;
	
    }
}

void sectosecminhr(int & s, int & m, int & h)
{
    h = s/3600;
    s = s % 3600;
    m = s/60;
    s = s%60;
}

void addsec(int & sec, int & min, int & hr)
{  
    sec++;	    
    if(sec == 60)
    {
	sec = 0;
	min++;
	if(min == 60)
	{
	    min =0;
	    hr++;
	    if(hr == 13)
	    {
		hr = 1;
	    }
	}
    }
}


void displayvalues(int seconds, int minutes, int hours)
{
    // cout << "The number of hours: " << hours << endl;
    // cout << "The number of minutes: " << minutes << endl;
    //cout << "The number of seconds: " << seconds << endl;
    cout << setfill('0')<< hours << ":" << minutes << ":" << seconds << endl;
    sleep(1);
}
