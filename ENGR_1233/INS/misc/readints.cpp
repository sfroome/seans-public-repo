#include <iostream> // for user input/output
#include <fstream> // for filestreams input/output
#include <cstdlib> //for exit
using namespace std;

int main()
{
    
    ifstream fin;
    ofstream fout;
    int anint;
    const string file_in = "ints.in" , file_out = "ints.out";
    
    fin.open( file_in.c_str() );
    if(fin.fail())
    {
	cout << "Could not open input file" << endl;
	exit(-1);
    }
    
    fout.open( file_out.c_str() );
    if(fout.fail())
    {
	cout << "Could not open output file" << endl;
	exit(-1);
    }
 
    while( fin >> anint ) // returns false when fails (fins anint until no more values)
    {
	fout << anint << endl;
    }
    
    fin.close();
    fout.close();
}
