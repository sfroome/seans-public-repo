/******************************************************************************************************************
Author: <Name goes here>
File: testcases.cpp
Purpose: Get three points from user, and say whether they are collinear. (ie: On the same line) 
Test Cases:

Test Case 1: Three Collinear Points
User Input: 1 1 2 2 3 3
Expected Output: "The three points are collinear"

Test Case 2: Three non Collinear Points
User Input: 1 1 2 2 0 4
Expected Output: "The three points aren't collinear"

Test Case 3: Non numeric Value Entered
User Input: 1 1 A 2 3 4
Expected Output: "Invalid Input." Program prompts for three new points.

Test Case 4: Too many Numbers Entered
User Input: 1 1 2 2 3 3 4 4 
Expected Output: Only first three points will be used. "The three points are collinear."

Test Case 5: Enter the same point multiple times.
User Input: 1 1 2 2 1 1 
Expected Output: "The three points are Collinear."

Test Case 6: Points on Vertical Line
User Input: 1 0 2 0 3 0
Expected Output: : "These Points are Collinear"

Test Case 7: Two Points Vertical
User Input: 1 0 2 0 3 4
Expected Output: "The three points are not collinear"
********************************************************************************************************************/
#include <iostream>

using namespace std;

int main()

{
    double x1,x2,x3,y1,y2,y3;

    cout << "Enter three points."
	 << "x1 y1 x2 y2 x3 y3"
	 << endl;

    cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
    cout << (x1-x2)/(y1-y2) << endl;
    while (cin.fail())
    {
	cin.clear();//unfail
	cin.ignore(100, '\n'); //Ignores up to 100 characters, or to the next line
	cout << "Invalid input, please reenter three new points." << endl; 
// In an actual assignment, ensure this is identical to what's written in testcases
	cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
    }

    if( x1 == x2 && x2 == x3) //vertical line
    {
	cout << "These points are collinear" << endl;
    }
// If slope between p1 and p2 is same between p2 and p3
    else if ((y1-y2)/(x1-x2) == (y2-y3)/(x2-x3))
    {
	cout << "These points are collinear" << endl;
    }

    else
    {
	cout << "These points aren't collinear" << endl;
    }



}
