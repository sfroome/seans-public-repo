//Version 4

#include <iostream>
#include <iomanip>
using namespace std;

void display(int array[], int size);

int main()
{
    int array[]= {1,2,3,4,7,6,5};
    int size = sizeof(array)/sizeof(int);   
    bool swap_occured = true;
  
    cout <<"SIZEOF(array)" << sizeof(array) << endl;
    cout << "SIZEOF(int) " << sizeof(int) << endl;
    for(int steps = size -2; steps >= 0 && swap_occured == true; steps--)
    {
	swap_occured = false;
	for(int i = 0; i <= steps; i++)
	{   
// if in wrong order, swap
	    if(array[i+1] < array[i])
	    {
	    int temp = array[i];
	    array[i] = array[i+1];
	    array[i+1] = temp;
	    cout << "swap occured" << endl;
	    swap_occured = true;
	    }
	    display(array, size);
	}
	cout<< "-------------------" << endl;
    }

}

void display(int array[], int size)
{
    for(int i = 0; i < size; i++)
    {
	cout << setw(5)  << array[i];
    }
    cout << endl;

}
