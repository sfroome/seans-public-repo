//Version 4 (version 2 never existed) 
//Version 4 uses input file streams

#include <fstream> // FILE STREAM!!!!!!!! FILES INSTEAD OF USER INPUT!!!!
#include <iostream> // INPUT OUTPUT FROM/TO USER 
#include <cstdlib> // for exit()
#include <cmath>
#include <climits>
using namespace std;

void ReadThreeSides(double & A, double & B, double & C);
double RadToDeg(double angle_rad);
double CalcAngle(double A, double B, double C);
void WriteLargestAngle(double largest_angle);

int main()
{
    double angle1, angle2, angle3;
    double A, B, C;
    double largest_angle;

    ReadThreeSides(A, B, C);
    angle1 = CalcAngle( A, B, C );
    angle2 = CalcAngle( A, C, B );
    angle3 = 180 - angle1 - angle2;
    largest_angle = max( angle1, max( angle2, angle3 ));
    WriteLargestAngle(largest_angle);
}

void ReadThreeSides(double & A, double & B, double & C)
{
    ifstream fin; // a stream that gets data from a file
    fin.open("triangle.dat"); // opens file
    fin >> A >> B >> C;

    if(fin.fail() )// fails if file does not open.
    {
	cout << "Couldn't open file" << endl;
	cout << "File was corrupt" << endl;
	exit(-1); // exit the program - no point in continuing
    }
    cout << "The file entered " << A << ", " << B << ", " << C << endl;
    fin.close(); // close file.
}

double CalcAngle(double A, double B, double C)
{
    // return RadToDeg(acos((A*A + B*B +C*C)/(2*A*B)));
    double angle;
    angle = acos((A*A + B*B - C*C)/(2*A*B));
    angle = RadToDeg(angle);
    return angle;
}

double RadToDeg(double angle_rad)
{
    angle_rad = angle_rad * 180 / M_PI;
    return angle_rad;
    //return angle_rad * 180 / M_PI;
}

void WriteLargestAngle(double largest_angle)
{
    ofstream fout; // construct output filestream
    fout.open( "triangle.out" ); // We used fout to open triangle.out
    if(fout.fail())
    {
	cout << "Could not open Output File" << endl;
	exit(-1);
    }
    fout << "The Largest Angle is " << largest_angle << endl;
    fout.close(); // closes file that was open.
}
