#include <iostream>

using namespace std;

struct Vec3D
{
    double x, y, z;
};

void GetVec3D( Vec3D & v1);
Vec3D GetVec();
double dot(const Vec3D& v1, const Vec3D& v2);
void displayVec3D(double dp, const  Vec3D &v1, const  Vec3D &v2);  

int main()
{
    double dp;
    Vec3D v1, v2;
    GetVec3D(v1);
    v2 = GetVec();
    dp = dot( v1, v2);
    displayVec3D(dp, v1, v2);

}
void GetVec3D(Vec3D & v1)
{
    cout << "Enter values for the first vector" << endl;
    cin >> v1.x >> v1.y >> v1.z;
}

Vec3D GetVec()
{
    Vec3D v2;
    cout << "Enter values for the second vector" << endl;
    cin >> v2.x >> v2.y >> v2.z;
    return v2;
}

double dot(const Vec3D& v1, const Vec3D& v2)
{
    double dotproduct;
    dotproduct = (v1.x*v2.x) + (v1.y*v2.y) + (v1.z*v2.z);
    return dotproduct;
}

void displayVec3D(double dp, const Vec3D & v1, const Vec3D & v2)
{
    cout << "Vector 1 is: (" << v1.x << ", " << v1.y << ", " << v1.z << " )"<< endl;
    cout << "Vector 2 is: (" << v2.x << ", " << v2.y << ", " << v2.z << " )"<< endl;
    cout << "The dot product of these two vectors is: " << dp << endl;
}
