#include <iostream>
#include <stdint.h>

using namespace std;

int main()
{

    int64_t a, b, d; //c
    int64_t old_a, old_b;

    cout << "Enter two integers."
	 << "Please enter the larger number first." << endl;
    cin >> a >> b;
    cout << "You entered " << a << " and " << b << endl;

    old_a = a;
    old_b = b;
    
    do
    {

	d = a%b;
	a = b;
	b = d;
	d = a%b;
    }
    
    while (d != 0);

    cout << "The gcd of " 
	 << old_a << " and " 
	 << old_b << " is "
	 << b << endl;

    
}
