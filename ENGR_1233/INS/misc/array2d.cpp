#include <iostream>
#include <iomanip>
using namespace std;

const int SIZE = 3;
void display( const int array[][SIZE] );
void transpose (int array[][SIZE]);

int main()
{
    int array[SIZE][SIZE] =
    {
	{1,1,2},
	{3,3,4},
	{5,5,6}
    };
    display(array);
    cout << endl;
    transpose(array);
    display(array);
}

void display( const int array[][SIZE] )
{
    for( int i = 0; i < SIZE; i++)
    {
	for( int j = 0; j < SIZE; j++ ) 
	{
	    cout <<setw(5) << array[i][j];
	}
	cout << endl;
    }
    cout << endl;
}

void transpose( int array[][SIZE] )
{
    for( int i = 0; i < SIZE-1; i++) // go up to but not include the last row
    {
	for( int j = SIZE-1; j > i ; j-- ) 
	{
//	    cout << setw(5) << array[j][i];
	    swap(array[i][j] , array[j][i] );
	    cout << "swapping " 
		 << array[i][j]
		 << " and "
		 << array[j][i] 
		 << endl;
	    display(array);
	    cin.get();
	}
	cout << endl;
    }
    cout << endl;
}
