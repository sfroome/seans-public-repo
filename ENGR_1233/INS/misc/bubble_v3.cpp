//Version 3

#include <iostream>
#include <iomanip>
using namespace std;

void display(int array[]);
const int SIZE = 7;


int main()
{
    int array[SIZE]= {1,2,3,4,7,6,5};
    bool swap_occured = true;
//ALL OF TEH BUBBLES

    for(int steps = SIZE -2; steps >= 0 && swap_occured == true; steps--)
    {
	swap_occured = false;
	for(int i = 0; i <= steps; i++)
	{   
// if in wrong order, swap
	    if(array[i+1] < array[i])
	    {
	    int temp = array[i];
	    array[i] = array[i+1];
	    array[i+1] = temp;
	    cout << "swap occured" << endl;
	    swap_occured = true;
	    }
	    display(array);
	}
	cout<< "-------------------" << endl;
    }

}

void display(int array[])
{
    for(int i = 0; i < SIZE; i++)
    {
	cout << setw(5)  << array[i];
    }
    cout << endl;

}
