#include <iostream>
#include <cmath>


using namespace std;

int main()
{
   const  int ARRAY_SIZE = 5;
   double array[ARRAY_SIZE];
   double std = 0, ave = 0;

   cout << "This program can calculate the average "
	<< "and the standard deviation of five values"
	<< endl;
   cout << "Enter " << ARRAY_SIZE << " values" << endl;

   for (int i = 1; i < ARRAY_SIZE+1; i++)
   {
       cin >> array[i];
       ave += array[i];
   }

   ave /= ARRAY_SIZE;

   cout << "Average is " << ave << endl;
   
   for( int i = 1; i < ARRAY_SIZE+1; i++)
   {
       std += (pow((array[i]-ave),2));
   }

   std /= ARRAY_SIZE;
   std = sqrt(std);

   cout << "The standard deviation is " << std << endl;


}
