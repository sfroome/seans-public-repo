#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    const int ROWS =2, COLS =3;
    int vals[ROWS][COLS] = {{1,2,3},{4,5,5}};

    int rowsum[ROWS] = {}; // start a vector with zeroes

    for ( int r = 0; r < ROWS; r++ )
    {
	for ( int c = 0; c < COLS; c++ )
	{
	    rowsum[r] += vals[r][c];
	}
	cout << "Row " << r << " has sum " << rowsum[r] << endl;
    }






}
