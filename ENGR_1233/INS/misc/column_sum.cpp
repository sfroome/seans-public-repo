
#include <iostream>
#include <iomanip>
#include <cmath>


using namespace std;

int main()
{
    double x, y, z;
    double sum;
    int maxdig;

    cout << "Enter three values of money" << endl;
    cin >> x >>  y >> z;
    sum = x + y + z;
    maxdig =
	max( int(log10(x))+1,
	     int(log10(y) +1 ));
	     maxdig = max(maxdig,
			  int(log10(z))+1);
	     maxdig = int(log10(sum)) + 4;
	     cout << " Max Dig = " << maxdig << endl;
	     
	     
	     cout << fixed << setprecision(2)<<endl;
	     cout << setw(maxdig) << x << endl;
	     cout << setw(maxdig) << y << endl;
	     cout << setw(maxdig) << z << endl;
	     cout << setfill('-')<< setw(maxdig +1)  <<"-" <<  endl;
	     cout << setw(maxdig) << sum << endl;



}
