#include <iostream>
#include <stdint.h>

using namespace std;

int main()
{

    int64_t a, b, d; //c
    int64_t old_a, old_b;

    cout << "Enter two integers."
	 << "Please enter the larger number first." << endl;
    cin >> a >> b;
    cout << "You entered " << a << " and " << b << endl;

    d = a%b;
    old_a = a;
    old_b = b;
    
    while (d != 0)
    {

	a = b;
	b = d;
	d = a%b;

    }
    cout << "The gcd of " 
	 << old_a << " and " 
	 << old_b << " is "
	 << b << endl;

    
}
