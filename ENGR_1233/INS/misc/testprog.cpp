#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

int main()
{
    char c;
    double d;
    int i;
    string s;
    
    ifstream fin("testfile.dat"); // Another way to open
    if(fin.fail())
    {
	cout << "couldn't open testfile.dat" << endl;
	exit(-1);
    }
    

    
    fin >> i;
    cout << "First: " << i << endl;
    
    fin >> i;
    cout << "Second: " << i << endl;
    
    fin >> c;
    cout << "Third: " << c << endl;
    
    fin >> i;
    cout << "Fourth: " << i << endl;
    if(fin.fail()) 
    {
	fin.clear(); // sets fail to false
	cout << "fin failed" << endl;
    }  

    fin >> d;
    cout << "Fifth: " << d << endl;
    
    fin >> d;
    cout << "Sixth: " << d << endl;

    fin >> s;
    cout << "Seventh: " << s << endl;

    getline( fin, s, '\n'); // get a string from fin into s, stop at a new line 
    cout << "Eighth: " << s << endl;

    fin.close();
}
