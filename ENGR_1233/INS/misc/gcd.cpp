#include <iostream>

using namespace std;

int main()
{
    int checknum;
    int n, m;

    cout << "Enter two integers."
	 << "Please enter the larger number first." << endl;
    cin >> m >> n;
    cout << "You entered " << m << "and " << n << endl;

    checknum = min(m,n);


    while(m % checknum !=0 || n % checknum != 0)
    {

	checknum--;

    }
    
    cout << "The gcd is of " << m << " and " << n 
	 << " is " << checknum << endl;


}
