#include <iomanip>
#include <iostream>

using namespace std;

int main()
{
    float f=0.1;
    double d= 0.1;
    
    cout << fixed;
    cout << setprecision(20) << f << endl;
    cout << setprecision(20) << d << endl;


    double x1 = 0.6;
    double x2 = 1 - 0.2 - 0.2;
    cout << boolalpha << (x1 == x2) << endl;
    cout << setprecision(20) << x1 << endl;
    cout << setprecision(20) << x2 << endl;


}
