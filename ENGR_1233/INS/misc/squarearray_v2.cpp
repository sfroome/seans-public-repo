#include <iostream>
using namespace std;

void getarray( int array[], int size );
void squarearray( int array[], int size );
void displayarray( const int array[], int size );

const int SIZE = 5;
int main()
{
    int array[SIZE] = {};
    getarray(array, SIZE); // no brackets on array argument
    squarearray(array, SIZE);
    cout << endl;
    displayarray(array, SIZE);
}

void getarray( int array[], int size )
{
    int val;
    cout << "Enter " << SIZE << " values:" << endl;
    for(int i = 0; i < size; i++)
    {
	cin >> val;
	array[i]= val;
    }
}

void squarearray( int array[], int size )
{
    for(int i = 0; i < size; i++)
    {
	array[i] *= array[i];
    }
}

void displayarray( const int array[], int size ) // void cannot modify array
{
    cout << "The values of the squared array are: " << endl;
    for(int i = 0; i < size-1; i++)
    {
	cout << array[i] << ", ";
    }
    cout << array[size - 1] << endl;
}
