#include <iostream>
#include <iomanip>
using namespace std;

void display(int array[]);
const int SIZE = 5;


int main()
{
    int array[SIZE]= {1,5,3,4,2};

//single bubble
// if in wrong order, swap
    for(int i = 0; i < SIZE - 1; i++)
    {   
	if(array[i+1] < array[i])
	{
	    int temp = array[i];
	    array[i] = array[i+1];
	    array[i+1] = temp;
	}
	display(array);
    }


}

void display(int array[])
{
    for(int i = 0; i < SIZE; i++)
    {
	cout << setw(5)  << array[i];
    }
    cout << endl;

}
