#include<iostream>
#include <iomanip>

using namespace std;


int main()
{
    const int ROWS = 4, COLS = 2;
    int temp_table[ROWS][COLS] =
	{ 
	    {14,16},
	    {23,25},
	    {18,18},
	    {-5,-10}
	};
 
    
    for( int i = 0; i < ROWS; i++)
    {

	for (int j = 0; j < COLS; j++)
	{
	    cout << setw(10) << temp_table[i][j];	 

	}
	cout  << endl;
    }

 


}
