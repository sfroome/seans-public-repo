#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
using namespace std;

int Int5to13();
double Double5to13();

int main()
{

    int val;
    cout << time(0)
	 << " is the number since January 1, 1970 at 12:00 AM"
	 << endl;

    cout << "Random number will be generated from 0 to "
	 << RAND_MAX
	 << endl;


    srand(time(0));
    val = rand();
    cout << val << endl;

    cout << "DICE THROWS" << endl;

    int dice1, dice2;
    dice1 = rand()%6 +1;
    cout << dice1 << endl;
    dice2 = rand()%6 +1;
    cout << dice2 << endl;

    cout << "RANDOM ANGLES BETWEEN 0 AND 2PI" << endl;

    cout << double(rand())/RAND_MAX*2*M_PI << endl;  
    cout << double(rand())/RAND_MAX*2*M_PI << endl;      
    cout << double(rand())/RAND_MAX*2*M_PI << endl;     
    cout << double(rand())/RAND_MAX*2*M_PI << endl;  

    cout << "Integers between 5 and 13" << endl;
    cout <<  Int5to13() << endl; 
    cout <<  Int5to13() << endl;   
    cout <<  Int5to13() << endl;  
    cout <<  Int5to13() << endl;

    cout << "Doubles between 5 and 13" << endl;
    cout << Double5to13() << endl;  
    cout << Double5to13() << endl;  
    cout << Double5to13() << endl;   
    cout << Double5to13() << endl;

}


int Int5to13()
{
    return rand()%9+5;

}

double Double5to13()
{
    return 8.0 * rand()/RAND_MAX + 5;

}
