#include <iostream>
using namespace std;



const int NUM_SIZE = 3;
struct Marks
{
    double mt1, mt2, final;
};

struct Student // don't use square brackets
{
    string name;
    int id;
    Marks marks;
    //   double weighted_ave;
};

int main()
{
    Student students[NUM_SIZE]  = 
	{
	    {"bob", 234567890,{70, 80, 90}},
	    {"lana", 123456789, {30, 20, 10}},
	    {"Mickey", 892084001, {30,40,50}}
	};   
//    cout << "Student Name: " <<bob.name << endl;
//    cout << "Student ID: " << bob.id << endl;
//    cout << "Midterm 1 Mark: " << bob.marks.mt1 << endl;
//    cout << "Midterm 2 Mark: " << bob.marks.mt2 << endl;
//    cout << "Final Exame Mark: " << bob.marks.final << endl;

    for (int i = 0; i < NUM_SIZE; i++)
    {
	double weighted_ave;
	weighted_ave = 0.3 * students[i].marks.mt1 
	    + 0.3 * students[i].marks.mt2 
	    + 0.4 * students[i].marks.final;
	cout << students[i].name << "'s ave = " << weighted_ave << endl;
    }


}
