#include <iostream>
#include <cmath>
#include <iomanip>


using namespace std;

int main()

{
    double x;
    int n;   
    int denom = 1;
    double num = 1, term = 1, sum = 1;
    //first term value is hard coded

    cout <<" Enter x and n (the number of terms)" << endl;
    cin >> x >> n ;
    cout << "The actual value of e^x is " << pow(M_E, x) << endl;
    //  cout << "Also " << exp(x) << endl; Alternative to calculating e^x
    

    for (int i = 1  ; i < n ; i++ )
    {
	denom *= i;
	num = pow(x,i); // Easy to read; ineffective
        //num *= x;  More effective, less easy to read
	term = num/denom;
	sum += term;
    }

    cout << setprecision(10)<< "The approximate value is " << sum << endl;





}
