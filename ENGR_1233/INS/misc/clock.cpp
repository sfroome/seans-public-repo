#include <iostream>

#include <iomanip>
using namespace std;

struct Time
{
    int hour, min, sec;

};

void get_time( Time & t);
void display_time(const Time & t);
void increment(Time & t);


int main()
{
    Time clock;
    get_time( clock );
    display_time( clock );
    while(1)
    {
//	usleep(30000); //microseconds
	sleep(1);
	increment( clock );
	display_time( clock );
    
    }
}

void get_time( Time & t)
{
    cout << "Enter the current time" << endl;
    cin >> t.hour >> t.min >> t.sec;
}

void display_time(const Time & t)
{
    cout << setfill('0');
    cout << setw(2) << t.hour;
    cout << ":";
    cout << setw(2) << t.min;
    cout << ":";
    cout << setw(2) << t.sec;
    cout << endl;
}

void increment(Time & t)
{
    t.sec++;
    if(t.sec > 59)
    {
    t.sec =0;
    t.min++;
    if(t.min > 59)
    {
	t.min = 0;
	t.hour++;
	if(t.hour > 12)
	{
	    t.hour = 1;
	}
    }
}
    

}
