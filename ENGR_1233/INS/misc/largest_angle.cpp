#include <iostream>
#include <cmath>
#include <climits>
using namespace std;

void ReadThreeSides(double & A, double & B, double & C);
double RadToDeg(double angle_rad);
double CalcAngle(double A, double B, double C);
void WriteLargestAngle(double largest_angle);

int main()
{
    double angle1, angle2, angle3;
    double A, B, C;
    double largest_angle;

    ReadThreeSides(A, B, C);
    angle1 = CalcAngle( A, B, C );
    angle2 = CalcAngle( A, C, B );
    angle3 = 180 - angle1 - angle2;
    largest_angle = max( angle1, max( angle2, angle3 ));
    WriteLargestAngle(largest_angle);
}

void ReadThreeSides(double & A, double & B, double & C)
{
    cout << "Enter three sides of a triangle" << endl;
    cin >> A >> B >> C;
    while (A < 0 || B < 0 || C < 0 || cin.fail())
    {
	cout << "You entered bad input" << endl;
	cout << "Please reenter the three sides" << endl;
	cin.clear();
	cin.ignore(100,'\n');
	cin >> A >> B >> C;
    }
}

double CalcAngle(double A, double B, double C)
{
    // return RadToDeg(acos((A*A + B*B +C*C)/(2*A*B)));
    double angle;
    angle = acos((A*A + B*B - C*C)/(2*A*B));
    angle = RadToDeg(angle);
    return angle;
}

double RadToDeg(double angle_rad)
{
    angle_rad = angle_rad * 180 / M_PI;
    return angle_rad;
    //return angle_rad * 180 / M_PI;
}

void WriteLargestAngle(double largest_angle)
{
    cout << "The largest angle on this triangle is " 
	 << largest_angle << endl;
}
