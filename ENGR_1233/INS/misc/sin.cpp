#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main()

{
    int n, denom = 1 , sign = 1;
    double x, num, term, sum;

    cout << "Enter x in degrees" 
	 << " and the number of terms to approximate sin(x)" << endl;
    cin >> x >> n;

    x = x * M_PI/180; // change to radians
    cout << setprecision(10) << endl;
    cout << "The actual value of sin(x) is " << sin(x) << endl;
    
    num = x;
    term = x;
    sum = x;
    //  cout << "First term is " << term << endl;

    for (int i = 3; i <=  2*n - 1; i+=2)
    {
	denom *= i*(i-1); // Multiply by next two numbers
	num = pow(x,i); // OR  num *= x*x
	sign *= -1;
	term = sign*num/denom;

//	cout << "Next term is " << term << endl;

	sum += term;


       
    }

    cout << "The approximate value of sin(x) is " << sum << endl;






}
