#include <iostream>
using namespace std;

int getperf();
bool isperfect( int);


int main()
{
    
    int result = getperf();
    cout << "The first perfect number is " << result << endl;
}

int getperf()
{
    int i;
    for( i = 2; ; i++)
    {
	if(isperfect(i))
	{
	    break;
	}
    }
    return i;
}

bool isperfect(int num)
{
    int sum_of_factors = 0;

    for( int i = 1; i < num; i++)
    {
	if( num%i == 0)
	{
	    sum_of_factors += i;
	}
    }

    if ( num == sum_of_factors )
    {
	return true;
    }

    else
    {
	return false;
    }
}
