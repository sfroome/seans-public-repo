#include <iostream>
#include <iomanip>


using namespace std;

int main()

{

    int value;
    bool alleven = true;


    cout << "Enter values" << endl;

    for (int i = 0; i < 10; i++)
    {

	cin >> value;
	
	if ( value % 2 == 1)
	{
	    alleven = false;	
	}
    }
    cout << "Are they all even?" << endl;
    cout << boolalpha << alleven << endl; //boolalpha prints out true or false (as opposed to 1 or 0 -- found in iomanip)


}
