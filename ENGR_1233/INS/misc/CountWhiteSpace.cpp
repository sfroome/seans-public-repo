#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int main()
{

    ifstream fin("infile.dat");
    int numwords = 0;
    string str;
//    char c;
    if (fin.fail())
    {
	cout << "couldn't open file!" << endl;
	exit(-1);
    }


//    while(getline(fin, str)) // gets number of lines
     while(fin >> str) // words
    //  while (fin >> c) // letters (not including whitespace)
	// while(fin.get(c)) // characters (including spaces)
    {
	cout << "string: " << str << endl;
	numwords++;
    }

    cout << "The number of words is " << numwords << endl;
    fin.close();

}
