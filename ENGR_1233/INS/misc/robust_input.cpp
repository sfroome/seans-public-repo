#include <iostream>

using namespace std;

int main()
{
    double angle;

    cout << "Enter an angle between 0 and 180" << endl;
    cin >> angle;


    while(cin.fail() || angle < 0 || angle > 180)  
    {
	cin.clear(); //unfail cin
	cin.ignore(30, '\n'); //igonore 30 characters or when they hit enter
	cout << "Invalid input: must be number between zero and 180" << endl; 
	cout << " Please enter a valid angle" 
	     << endl;
	cin >> angle;
	
    }

    cout << angle << endl;



}
