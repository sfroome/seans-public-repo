/*
Filename:
Author: Sean Froome
Description:
*/


#include<iostream>
#include<cmath>
#include<iomanip>


using namespace std;

int main()

{
    
  int i, j, k = 0, val;
  bool isprime;
  const int SIZE = 30;
  int primes[SIZE] = {};
  cout << "Enter an integer between 1 and 100" << endl;
  cin >> val;

   while(val < 1 || val > 100 || cin.fail())
    {
      cin.clear();
      cin.ignore(100,'\n');
      cout << "Improper input detected."
	   << "Please enter an integer between 1 and 100" 
	   << endl;
      cin >> val;
    }
  
  for (i = 2; i < 100; i++)
    {   
      isprime = true;
     
      for (j = 2; j <= sqrt(i) && isprime == true; j++)
	{
	    if(i%j == 0)
	    {
	     isprime = false;
	    }
	}
      
      if(isprime == true)
	{
	  primes[k] = i;
	  k++;
	}
//      cout << setw(2) << primes[k];     
    }
//  cout << endl;

/*
  for(int m = 0; m < SIZE; m++)
  {
  cout << setw(3) << primes[m] << endl; 
  }
  cout << primes[20] << endl;
*/

 cout << val << " = ";

  for (i = 0; i < k; i++)
    {
      while( val%primes[i] == 0)
	{
	  cout <<primes[i] << "*"; 	
	  val = val/primes[i];	 
	 }
    }
  if ( val != 1)
  { 
 cout << val << endl; 
  }
}

