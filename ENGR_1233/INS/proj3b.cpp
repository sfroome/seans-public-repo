/************************************************************
File:
Author: Sean Froome
Description:


************************************************************/

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

double GetAngle();
double GetVelocity();
int CalcPositions(double posx[], double posy[], double v0, double angle);
void CalcVelocities( double velx[], double vely[], double v0, double angle, int time );
void MaxPos(double v0, double angle, double & maxx, double & maxy);
void ShowTrajectory(const double posx[], const double posy[], int total_time, double velocity, double angle, double maxx, double maxy);

const double g = -9.81;
const int SIZE = 100;

int main()
{
  double maxx, maxy;
  double angle, v0;
  double posx[SIZE] = {};
  double posy[SIZE] = {};
  double velx[SIZE] = {};
  double vely[SIZE] = {};
  int total_time, time;
  
  cout << fixed << setprecision(2);
  angle = GetAngle();
  v0 = GetVelocity();
  time = CalcPositions(posx, posy, v0, angle);
  CalcVelocities(velx,vely, v0, angle, time);
  total_time = time;
  MaxPos( v0, angle, maxx, maxy);
  ShowTrajectory( posx, posy, total_time, v0, angle, maxx, maxy);
}

double GetAngle()
{
  double angle;
  cout << "Enter an angle between 0 and 90 degrees" << endl;
  cin >> angle;
  while(angle <=0 || angle > 90 || cin.fail())
    {
      cout << "Angle isn't between 0 and 90, or bad input entered. " << endl;
      cout << "Please Reenter:" << endl;
      cin.clear();
      cin.ignore(100,'\n');
      cin >> angle;
    }
  angle = angle*M_PI/180;
  return angle;
}

double GetVelocity()
{
  double velocity;
  cout << "Enter an initial velocity" << endl;
  cin >> velocity;
  while( velocity <= 0 || cin.fail())
    {
      cout << "Negative velocity, or non numeric input entered. " << endl;
      cout << "Please try again:" << endl;
      cin.clear();
      cin.ignore(100,'\n');
      cin >> velocity;
    }
  cout << endl;
  return velocity;
}

int CalcPositions(double posx[], double posy[], double v0, double angle)
{
  int time;
  for(time = 0; time < SIZE; time++)
    {
      posx[time] = cos(angle)*v0*time;
      posy[time] = -4.9*pow(time,2) 
	+ sin(angle)* v0*time;
      if(posy[time] < 0)
	{	    
	  posy[time] = 0;
	  break;
	}
    }
   return time; 
}

void CalcVelocities( double velx[], double vely[], double v0, double angle, int time)
{
  for(int i = 0; i <= time; i++) 
    {
      velx[i] = cos(angle)*v0;     
      vely[i] = g*i + sin(angle)*v0;
    }
}

void MaxPos(double v0, double angle, double & maxx, double & maxy)
{
  maxx = -1*(pow(v0,2)*sin(2*angle)/g);
  maxy = pow((sin(angle)*v0),2)/(-2*g);
}


void ShowTrajectory(const double posx[], const double posy[], int total_time, double v0, double angle, double maxx, double maxy)
{
  cout << "Initial Velocity: " << v0 << endl;
  cout << endl;
  cout << "Angle: " << angle*180/M_PI << endl;
  cout << endl;
  cout << "Max Height: " << maxy << endl; 
  cout << endl;
  cout << "Max Distance: " << maxx << endl;
  cout << endl; 
  cout << left << setw(20) << "Time (seconds)" << setw(20) << "Position (x)" << setw(20) << "Position (y)" << endl;
  cout << left << setw(55) << setfill('-') << "-"<< endl;
  cout << setfill(' ') << setw(20);
  
  for (int i = 0; i <= total_time; i++)
    {
      cout << left  <<setw(20)<< i  << setw(20) << posx[i] << setw(20) << posy[i] << endl;
    }
 if( posy[total_time] > 0 && total_time == 100)
    {
      cout << endl;
      cout << "Program failed to find the correct values" << endl;
      cout << "Object in air longer than max bound of program." << endl;
    }

}


