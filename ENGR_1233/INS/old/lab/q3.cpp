
#include <iostream>

using namespace std;

void printArray(int A[], int length);
void reverseArray (int A[], int length);


int main()
{
    int length=10;
    int A[]={1,2,3,4,5,6,7,8,9,10 };
    printArray( A, length);
    reverseArray( A, length);
    printArray (A, length);
    return 0;
 }

void printArray(int A[], int length)

{
    int i;
    for ( i=0; i < length-1; i++)

    {
	cout << A[i] << ", " << endl;

    }
    cout << A[length-1] << endl;
}


void reverseArray ( int A[],int length)

{
    int i;
    int j;


    for( i=0, j=length-1; i < (length)/2  ; i++, j--)
    {

	swap( A[i], A[j]);
	
    }   
    
}
