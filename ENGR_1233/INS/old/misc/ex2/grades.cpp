#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main()

{

    double sum =0;
    double grade;
    int num_grade;
    double mean;
    double GPA;
    double max=0;
    double min =100;
    double range;


    cout << "Enter the number of grades" << endl;
    cin >> num_grade;
    for (int i=0; i < num_grade; i++)
    {
	cout<< "Input grade" << endl;
	cin >> grade;

	if (grade < min)
	{
	    min = grade;
	}

	if (grade > max)
	{

	    max = grade;
	} 
	sum += grade;
    }      

    range = max-min;
    mean = sum/num_grade;
    GPA = (mean/100) *4; 

    cout << "The Min Grade is " << min <<  endl;
    cout << "The Max Grade is " << max << endl;
    cout << "The Range is " << range << endl;
    cout << "The Mean Average is " << mean << endl;
    cout << fixed << setprecision (4);  
    cout << "The GPA is " << GPA << endl;

}






