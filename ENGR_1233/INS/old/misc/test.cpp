#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main()
{
    int numterms;
    double x;
    double sum, term;
    double denom = 1;
    cout << "Enter the number of terms and the value of x" << endl;
    cin >> numterms >> x;
    for ( double n = 1; n <= numterms;)
    {
	denom *= n;
	term = x^n/denom;
	sum += term;
	    
    }
    cout << setprecision(2)
	 << "The approximate value of sinh(" << x << ") is "
	 <<sum << endl;

    

}



