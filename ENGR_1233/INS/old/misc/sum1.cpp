#include <iostream>

using namespace std;

int main()
{
    int n;
    int result;

    cout << "Please enter a value for n";
    cin >> n;

    if ( n < 0 )
    {
	cout << "ENTER A POSITIVE NUMBER" << endl;
    }
    else
    {
	cout << "You have entered " << n << endl;
	result = 2*n+1;
	
	cout << "The answer is " << result << endl;
    }

}
