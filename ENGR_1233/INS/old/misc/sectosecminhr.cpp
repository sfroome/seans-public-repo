#include <iostream>

using namespace std;

void sectosecminhr (int &sec, int &min, int &hr);

int main()

{

    int sec, min, hr;
    cout << "Enter the number of seconds" << endl;
    cin >> sec;

    sectosecminhr (sec, min, hr);

    cout << "Answer: "
	 << sec
	 <<" "
	 << min
	  << " "
	  << hr
	  << endl;
}

void sectosecminhr(int &sec, int &min, int &hr)
{
    hr=sec/3600;
    sec =sec%3600;
    min = sec/60;
    sec = sec % 60;
}
