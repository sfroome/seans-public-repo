#include <iostream>
#include <cmath>

using namespace std; //because cout is really std::cout
int main()

{

    double result; //doubles are real numbers 

    double angle;

    double angle_rad;

    cout << "Please enter an angle in degrees " << endl;
  
    cin >> angle;

    cout << "You have entered " << angle << endl;

    angle_rad = angle * M_PI / 180; //M_PI is pi in cmath
 
    cout << "sin (" <<angle << ") = " << sin(angle_rad) << endl;  


}
