#include <iostream>
#include <iomanip>

using namespace std;

int main()

{

    const int ROWS = 3, COLS = 3;
    int sum = 0;
    for ( int i = 0; i < ROWS; i++ )
    {
	for ( int j = 0; j < COLS; j++ )
	{
	    cout <<setw(4) << i - j;
 	    sum += i - j;
	    sum =-1*(i+j)+1;

	}

	cout << setw(4) << sum << endl;
	cout << setw(4*COLS+4) << setfill('-') << '-' << endl;
    }







}
