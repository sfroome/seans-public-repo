/***************************************************************         
Pseudocode:                                                               
Get coefficients A, B, C from the user.                                   
Calculate the two roots: root1 and root2.                                 
Display the two roots for the user.                                       
***************************************************************/
// File: Practice.cpp
//Author: Sean Froome
//Purpose: To calculate the two roots of a parabola
//Date: January 21, 2013


#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double A, B, C; //coefficients of the parabola                       
    double root1, root2;

    cout << "This program will find the roots "
	 << "for a parabola"
	 <<endl;

    cout << "Enter the three coefficients A, B and C"
	 <<endl;

    cin >> A;
    cin >> B;
    cin >> C;

    //Calculate the two roots                                             
    root1 = (-B + sqrt(B*B -4*A*C ))/(2*A);
    root2 = (-B - sqrt(B*B -4*A*C ))/(2*A);

    cout << "The two roots are "
	 << root1
	 << " and "
	 << root2
	 << endl;
}	
/**************************************************************
User Manual
Program Description: This program calculates the two roots of a parabola.
Inputs: the coefficients of A, B and C of the parabola
Outputs: The two roots of the parabola

Instructions
1. Run Program
2.Enter three real numbers after the prompt
"Enter the three coefficients A, B and C"
3. The two roots of the parabola will be displayed.

TEST RUN
Enter the three coefficients A, B, and C
1 2 1
The two roots are -1 and 1 
 *************************************************************/
