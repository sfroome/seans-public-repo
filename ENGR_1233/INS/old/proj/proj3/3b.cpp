//File: 3b.cpp
//Author: Sean Froome
//Purpose: To use Jacobi's Method to find a Vector
//Date: March 15, 2013
#include <iostream>
#include <cmath>


using namespace std;

const int SIZE = 4;
const int  MAX_ITER =1000;
const double EPSILON = 1e-8;

int main()
{

    double matA[SIZE][SIZE] =
	{{9,-4, -2, 0},
	 {-4, 17, -6, -3},
	 { -2, -6, 14, -6},
	 {0, -3, -6, 11}};
    

    double vecb[SIZE]={24, -16, 0, 18};
    double vecx[SIZE]= {1,1,1,1};
    double tempx[SIZE] = {};


    bool converged =false;
    
    for(int i = 0; converged == false &&  i < MAX_ITER; i++)
    {
	tempx[0] = (1/matA[0][0])*
	    (vecb[0]
	     -( matA[0][1]*vecx[1]
		+ matA[0][2]*vecx[2]
		+ matA[0][3]*vecx[3]));
	
	tempx[1] = (1/matA[1][1])*
	    (vecb[1] 
	     -(matA[1][0]*vecx[0]
	       + matA[1][2]*vecx[2]
	       + matA[1][3]*vecx[3]));
	
	tempx[2] =(1/matA[2][2])*
	    (vecb[2]
	     - (matA[2][0]*vecx[0]
		+ matA[2][1]*vecx[1]
		+ matA[2][3]*vecx[3]));
	tempx[3] = (1/matA[3][3])*
	    (vecb[3]
	     - (matA[3][0]*vecx[0]
		+ matA[3][1]*vecx[1]
		+ matA[3][2]*vecx[2]));
	
	converged =(fabs(vecx[0]- tempx[0]) < EPSILON &&
		    fabs(vecx[1]- tempx[1]) < EPSILON && 
		    fabs(vecx[2]- tempx[2]) < EPSILON &&
		    fabs(vecx[3]- tempx[3]) < EPSILON);
	vecx[0]=tempx[0];
	vecx[1]=tempx[1];
	vecx[2]=tempx[2];
	vecx[3]=tempx[3];
    }	
	for( int z=0; z < SIZE; z++)
	{
	    cout << vecx[z] << " ";
	}
	cout << endl;

    
    

    
    
}




/*************************************************************************
User Manual:
Program Desription: This program will find the inverse matrix
Inputs:None
Outputs: Vector x
Instructions:
Execute Program
Program will display vector.
 ************************************************************************/
