3b.cpp                                                                                              0000644 2241516 0023420 00000003336 12120665437 012142  0                                                                                                    ustar   sfroo699                        students                                                                                                                                                                                                               //File: 3b.cpp
//Author: Sean Froome
//Purpose: To use Jacobi's Method to find a Vector
//Date: March 15, 2013
#include <iostream>
#include <cmath>


using namespace std;

const int SIZE = 4;
const int  MAX_ITER =1000;
const double EPSILON = 1e-8;

int main()
{

    double matA[SIZE][SIZE] =
	{{9,-4, -2, 0},
	 {-4, 17, -6, -3},
	 { -2, -6, 14, -6},
	 {0, -3, -6, 11}};
    

    double vecb[SIZE]={24, -16, 0, 18};
    double vecx[SIZE]= {1,1,1,1};
    double tempx[SIZE] = {};


    bool converged =false;
    
    for(int i = 0; converged == false &&  i < MAX_ITER; i++)
    {
	tempx[0] = (1/matA[0][0])*
	    (vecb[0]
	     -( matA[0][1]*vecx[1]
		+ matA[0][2]*vecx[2]
		+ matA[0][3]*vecx[3]));
	
	tempx[1] = (1/matA[1][1])*
	    (vecb[1] 
	     -(matA[1][0]*vecx[0]
	       + matA[1][2]*vecx[2]
	       + matA[1][3]*vecx[3]));
	
	tempx[2] =(1/matA[2][2])*
	    (vecb[2]
	     - (matA[2][0]*vecx[0]
		+ matA[2][1]*vecx[1]
		+ matA[2][3]*vecx[3]));
	tempx[3] = (1/matA[3][3])*
	    (vecb[3]
	     - (matA[3][0]*vecx[0]
		+ matA[3][1]*vecx[1]
		+ matA[3][2]*vecx[2]));
	
	converged =(fabs(vecx[0]- tempx[0]) < EPSILON &&
		    fabs(vecx[1]- tempx[1]) < EPSILON && 
		    fabs(vecx[2]- tempx[2]) < EPSILON &&
		    fabs(vecx[3]- tempx[3]) < EPSILON);
	vecx[0]=tempx[0];
	vecx[1]=tempx[1];
	vecx[2]=tempx[2];
	vecx[3]=tempx[3];
    }	
	for( int z=0; z < SIZE; z++)
	{
	    cout << vecx[z] << " ";
	}
	cout << endl;

    
    

    
    
}




/*************************************************************************
User Manual:
Program Desription: This program will find the inverse matrix
Inputs:None
Outputs: Vector x
Instructions:
Execute Program
Program will display vector.
 ************************************************************************/
                                                                                                                                                                                                                                                                                                  3c.cpp                                                                                              0000644 2241516 0023420 00000006603 12120675271 012140  0                                                                                                    ustar   sfroo699                        students                                                                                                                                                                                                               #include <iostream>
#include <cmath>

using namespace std;


const int SIZE =4;


void diff(const double V1[SIZE], const double V2[SIZE], double DiffV1V2[SIZE]);
void prod(const double Mat[][SIZE],const double Vec[SIZE], double prodVec[SIZE]);
void remMatrix(const double Mat[][SIZE], double diagMat[][SIZE], double remMat[][SIZE]);
bool converged(const double Mat[][SIZE],const double Vec1[SIZE], const double Vec2[SIZE]);
double printVec(const double Vec[SIZE]);
double printMatrix(const double Mat[][SIZE]);



int main()
{


    const int MAX_ITER = 100;
    double Mat[SIZE][SIZE]=
	{{9,-4,-2,0},
	 {-4,17,-6,-3},
	 {-2,-6,14,-6},
	 {0,-3,-6,11}};
    double Vec[SIZE] = {24,-16,0,18};
    double vecx[SIZE]= {1,1,1,1};
    double remx[SIZE];
    double sub[SIZE];
    double DiffV1V2[SIZE];
    double prodVec[SIZE]; 
    double remMat[SIZE][SIZE];
    bool converged;
    
    for(int i=0; i < MAX_ITER; i++)   
    {
	prod(remMat, vecx, remx);
	diff(Vec, remx,sub);
	prod(diagMat, sub, vecx);
	printVec(vecx);
	cout << endl; 
	if(converged (Mat, vecx, Vec))
	    break;



    }


    
    
    
}






void diff(const double V1[SIZE], const double V2[SIZE], double DiffV1V2[SIZE])
{
// this function subtracts vector V1 from vector V2 and stores the result in DiffV1V2
    for(int i=0; i < SIZE;i++)
	DiffV1V2[i]=abs(V1[i]-V2[i]);
}



void prod(const double Mat[SIZE][SIZE],const double Vec[SIZE], double prodVec[SIZE])
{
// this function multiplies matrix Mat and vector Vec, and stores the result in prodVec.
    for (int i =0; i <SIZE; i++)
    {

	prodVec[i]=0;
	for (int j=0; j <SIZE; j++)
	{
	    prodVec[i] += Mat[i][j]*Vec[j];
	}
    }
    
}


void diagInv(double Mat[][SIZE])
{
//this function calculates the inverse of a diagonal matrix Mat, and stores the result in the same matrix Mat.
    
    for (int i=0; i < SIZE; i++)
    {
	for (int j=i; j < SIZE; j++)
	{	
	    Mat[i][i]=1/Mat[i][i];
	}
    }
    
}



void remMatrix(double Mat[][SIZE], double diagMat[][SIZE], 
	       double remMat[][SIZE])
{
// this function forms 
//a) a diagonal matrix from diagonal elements of Mat and stores in diagMat.
//b) a matrix with all off-diagonal elements equal to those of Mat, and with diagonal elements equal to zero.
    for(int  i=0; i < SIZE; i++)
    {
	diagMat[i][i]=Mat[i][i];
	
	for(int j=0; j < SIZE; j++)
	{
	    remMat[i][j]=Mat[i][j]-diagMat[i][i];
	}
    }
}



bool converged( const double Mat[][SIZE],const double Vec1[SIZE], const double Vec2[SIZE])
{
// this function returns True (1); if |(|RI-V|)|<err and False (0) otherwise. .err. is a small //number e.g. 1e-8.

/*    converged =(fabs(vecx[0]- tempx[0]) < EPSILON &&
		fabs(vecx[1]- tempx[1]) < EPSILON &&
		fabs(vecx[2]- tempx[2]) < EPSILON &&
		fabs(vecx[3]- tempx[3]) < EPSILON);
*/

    const double EPSILON = 1e-8;

    bool converged = 
	abs(Vec2[0] - Vec1[0]) < EPSILON &&
	abs(Vec2[1] - Vec1[1]) < EPSILON &&
	abs(Vec2[2] - Vec1[2]) < EPSILON &&
	abs(Vec2[3] - Vec1[3]) < EPSILON;
    
    return converged;
}




double printVec(const double vec[SIZE])
{
// this function displays the vector Vec . primarily for debugging
    for (int i=0; i < SIZE; i++)
    {

    cout << vec[i] << endl;
    }

    return 0;

}


double printMatrix(const double Mat[][SIZE])
{
//this function displays the matrix Mat . primarily for debugging
    for (int i=0; i <SIZE; i++)
    {
	for (int j=0; j<SIZE; j++)
	    cout << Mat[i][j];
    }
    cout << endl;
    return 0;
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             