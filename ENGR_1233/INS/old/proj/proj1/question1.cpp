/************************************************************************
Pseudocode:
Prompt user for two sides of a triangle.
Prompt user for angle between the two sides.
Calculate the two sides and the angle to find the third side.
Display the unknown side for the user.
************************************************************************/
// File: a1.cpp
//Author: Sean Froome
//Purpose: To find the third side of a triangle. 
//Date: January 22, 2013

#include <iostream>
#include <cmath>
using namespace std;

int main()

{

    double a; // the first side of the triangle
    double b; // the second side of the triangle
    double c; // the unknown side of the triangle
    double angle_rad; // angle in radians
    double angle_deg; // angle in degrees

    cout << "This program will find the unknown side of a triangle" << endl; 
    cout << "Please enter two real numbers" << endl;
    cin >> a;
    cin >> b;

    cout << "You have entered " << a << " and " << b << endl;
    cout << "Enter the angle between the two sides" << endl;
    cin >> angle_deg;

    cout << "You have entered " << angle_deg << endl;
    angle_rad =  angle_deg*M_PI/180; // Conversion from degrees to radians
    c = sqrt((a*a) + (b*b)- 2*a*b*(cos(angle_rad))); //solving for unknown side
    cout << "The unknown side is " <<  c << endl;

}
/************************************************************************
User Manual
Program Description: This program calculates the third side of a triangle

Inputs: Two sides of a triangle, and the angle between them.

Outputs: The unknown side of the triangle.

Instructions:
1. Run program.
2. Enter two sides of a triangle after the prompt.
"Please enter two real numbers "
3. Enter the angle between the two sides after the prompt.
"Enter teh angle between the two sides"
4. The unknown side will be displayed.
*************************************************************************
TEST RUN(S)


Run 1
Please enter two real numbers
3 4
You have entered 3 and 4
Enter the angle between the two sides
90
You have entered 90
The unknown side is 5

Run 2
Please enter two real numbers
6 8
You have entered 6 and 8
Enter the angle between the two sides
45
You have entered 45
The unknown side is 5.66725
************************************************************************/
