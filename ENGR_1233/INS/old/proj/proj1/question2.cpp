/*************************************************************************
Pseudocode:
Prompt user for how many cents they have.
Calculate the number of Toonies, Loonies, Quarters, Dimes, Nickels, and
Pennies required for this amount of change.
Display the resulting amount of change for the user.
************************************************************************/
//File: a2.cpp
//Author: Sean Froome
//Purpose: To find the correct amount of Change 
//Date: January 22, 2013

#include <iostream>
#include <cmath>

using namespace std;

int main()

{

    int cents; //The total amount of change that user is inputting
    int too; // Number of Toonies
    int loo; // Number of Loonies
    int quart; // Number of Quarters
    int dime; // Number of Dimes
    int nick; // Number of Nickels
    int penn; // Number of Pennies

    cout << "This program will find the least number of coins required "
	 << "for the amount of cents entered" << endl;
    cout << "Enter how much money you have in cents" << endl;
    cin >> cents;

    cout << "You have entered " << cents << endl;
 
    too = cents/200; 
    cents = cents%200;

    loo = cents/100;
    cents = cents%100;

    quart = cents/25;
    cents = cents%25;

    dime = cents/10;
    cents = cents%10;

    nick = cents/5;
    cents = cents%5;

    penn = cents/1;
    cents = cents%1;

    cout << "You have " << endl;      
    //if statement so that coins that aren't present in the
    //final result won't display in the output
    if ( too > 0) 
      
    {
	cout << too << " Toonie(s), " << endl;
    }
    if (loo > 0)
    {
	cout << loo << " Loonie(s), " << endl;
    }
    if (quart >0)
    {
	cout << quart << " Quarter(s), " << endl;
    }
    if ( dime > 0)
    {
	cout << dime << " Dime(s), " << endl;
    }
    if ( nick > 0)
    {
	cout << nick << " Nickel(s), and " << endl;
    }
    if (penn > 0)
    {
	cout << penn << " Pennie(s)" << endl;
    }
}


/*************************************************************************
User Manual
Program Description: This program will calculate the number of Toonies,
Loonies, Quarters, Dimes, Nickels and Pennies that the user would need for
the amount of cents they input.

Inputs: Amount of Cents

Outputs: The correct amount of coinage.

Instructions:
1. Run Program.
2. Enter the amount of cents you have.
3: The number of Toonies, Loonies, Quarters, Dimes, Nickels, and Pennies
will be displayed.
*************************************************************************
TEST RUNS

Run 1
Enter how much money you have in cents
500
You have entered 500
You have
2 Toonie(s),
1 Loonie(s),

Run 2
Enter how much money you have in cents
568
You have entered 568
You have
2 Toonie(s),
1 Loonie(s),
2 Quarter(s)
1 Dime(s)
1 Nickel(s), and
3 Pennie(s)
************************************************************************/
