/*******************************************************************************************************
Filename:
Author: Sean Froome
Description: This program uses Newton's Method to find the root of a function. The function
determines the shortest length of rope needed for a goat to have access to half of a circular field.
Note: If Newton's Method fails, the program will continue to run until an initial guess that will give
a correct answer is given, allowing the user multiple attempts to get the answer.

TEST CASES

Case1: User Enters 1 degree.(Other numbers that fail include 90, 89, 3)
Expected Output:
Newton's Method failed to find the length of the rope.
Try again with a different initial guess.
Enter an initial guess for theta in degrees
(Program will prompt user for another guess.)

Case2: User Enters 2 degrees: (other numbers tested: 30, 45, 60, 50, among others)
Expected Output:
The angle is 54.5942 degrees.
The length of the rope is 115.873 (if Newton doesn't fail, these are the values that should be obtained,
regardless of initial guess.)

Case3: User Enters an angle bigger than 90 (or smaller than or equal to 0) (input tested: 0, -1, 91)
Expected Output:
Bad input detected. Angle must be smaller than 0, or larger than 90. Please reenter:
(User will be prompted to enter an angle within the bounds suggested.0

Case4: User doesn't give a numerical value at all (input tested: no)
Expected Output:
Bad input detected.Angle must be smaller than 0, or larger than 90. Please reenter:
(User will be prompted to enter a number)

*******************************************************************************************************/
#include <iostream>
#include <cmath>

using namespace std;

int main()  
{
  const int r =100, MAX_ITER = 15; // radius of field and max number of times for loop will run
  const double EPSILON = 1e-8;
  double R, a; // length of rope and angle of rope
  double fa, fpa; // f(a) and f'(a)

  // Program has a do-while loop that will prompt user for another guess if Newton's Method fails.
  do
    {
      cout << "Enter an initial guess for theta in degrees:" << endl;
      cin >> a;
      
      // If an angle is too big or small, or if a non numeric input is given
      // 0 will always evaluate to -nan, therefore it's bad input	
      while (a < 0 || a > 90 || cin.fail()) 
	{
	  cout << "Bad input detected. "
	       << "Angle must be smaller than 0, "
	       << "or larger than 90. " 
	       << "Please reenter:" 
	       << endl;
	  cin.clear();
	  cin.ignore(100,'\n');
	  cin >> a;
	}   
      
      // Converting to radians from degrees
      a = a * M_PI/180;    
      
      // Program loops until finished (MAX_ITER times), regardless if root is found.
      for (int i = 0; i < MAX_ITER; i++)
	{
	  // Function
	  fa = 4*a*pow(cos(a),2)     
	    + M_PI/2 - 2*a - sin(2*a);
	  
	  //Derivative of Function
	  fpa = 4*pow(cos(a),2)   
	    - 8*cos(a)*sin(a) - 2 - 2*cos(2*a);
	  
	  // Obtaining a new angle
	  a = a - fa/fpa;
	  
	}
      
      // Formula to find length of rope
      R = 2*r*cos(a); 
      
      // Converting back to degrees (for cout statement)
      a = a * 180/M_PI; 
      
      // Checks to confirm Newton's Method is accurate

      // if the angle isn't between 0 and 90
      if (a <= 0 || a > 90 || isnan(a))
	{
	  cout << "Newton's Method failed to find the length of the rope." << endl;
	  cout << "Try again with a different initial guess." << endl;	 	 
	}
    
      // if the abs of the function is less than 1e-8
      else if (abs(fa) > EPSILON)
	{
	  cout << "Newton's Method failed to find the length of the rope." << endl;
	  cout << "Try again with a different initial guess." << endl;
	}
    }
  while (a <= 0 || a > 90 || isnan(a) || abs(fa) > EPSILON); 
  
  cout << "The angle is " << a << " degrees." << endl;
  cout << "The length of the rope is " << R << endl;  
}
