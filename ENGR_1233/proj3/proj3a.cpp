/*************************************************************************
Filename:Newtons_Method_v2.cpp
Author: Sean Froome
Description: Program uses Newton's Method to solve the "Gobbling Goat" 
problem. Unlike the previous version, this version includes functions
for better organization and efficiency.

TEST CASES:
Case1:Case1: User Enters 5 degrees.
Numbers tested that fail: 90, 89, 3, among others.
Expected Output:         
You have entered 5 degrees                                                
Newton's Method failed to find the length of the rope.                    
Please run the program again to try again.

Case2:User Enters 57 degrees: 
Numbers tested: 30, 45, 60, 50
Expected Output:
You have entered 57 degrees
The angle is 54.5942 degrees.                                             
The length of the rope is 115.873 
(if Newton's doesn't fail, these are the values that should be obtained,
regardless of initial guess.)

Case3: User Enters an angle bigger than 90 
(or smaller than or equal to 0) 
Input tested: 0, -1, 91)                                                
Expected Output:                                                          
Bad input detected.
Please reenter an angle between 0 and 90
(Program will prompt until a proper value is given)

Case4: User doesn't give a numerical value at all
Input tested: yes, !, $, no     
Expected Output:                                                          
Bad input detected.
Please reenter an angle between 0 and 90
(User will be prompted until a proper value is given)
*************************************************************************/

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

double GetGuess();  
//Function: double GetGuess()
//Input: No parameters are passed to it. Input is given by user 
//       inside the function. 
//Output: The angle (guess) is returned to main. The units are in degrees.
//        guess must be between 0 and 90 (degrees).
//Purpose: The purpose is to get an angle from the user as an initial  
//         guess for the program, so Newton's Method has an angle to
//         start looking from.

double DegToRad(double angle_rad);
//Function: double DegtoRad(double)
//Input: angle_rad, which is passed from main
//Output: angle_rad is returned, in radians
//Purpose: Converts the users inputted angle from degrees to Radians, 
//         as C++ uses radians by default

double Newton(double angle_newton); 
//Function: double Newton(double)
//Input: double angle is passed from main to newton. 
//       Newton also calls the functions f and fprime.
//Output: it returns angle_newton of the function, which is 
//        the angle Newton's method solves for. angle_newton is 
//        in radians.
//        It also passes angle_newton to f() and fprime(). 
//Purpose: To calculate the exact value of the angle 
//         in the gobbling goat problem. It finds the angle
//         that is equal to f(angle_newton) == 0
//         (Within a tolerance interval EPSILON).


double CalcRope(double angle_rope);
//Function: double CalcRope(double)
//Input: double angle from main
//Output: double rope is returned to main.
//        rope could technically be in any unit of measurement
//        that measures distance. Assume it's in meters.
//Purpose: To calculate the length of the rope in the problem

double RadtoDeg(double angle_deg);
//Function: RadtoDeg(double)
//Input: double angle is passed from main to RadtoDeg
//Output: angle_deg is returned to main
//Purpose: To convert the angle from radians to degrees
//         The user entered the original guess in degrees
//         so the calculated angle should be in degrees too

void DisplayResult(double angle_final, double rope_final);
//Function: void DisplayResult(double, double)
//Input: double angle and double rope are passed by main
//Output: Nothing is returned. The angle and rope length  
//        are outputted to the screen. angle is in degrees, 
//        and rope is (presumably) in meters.
//Purpose: To display the rope length and correct angle to screen

double f(double angle_f);
//Function: double f(double)
//Input: angle_newton is passed from newton. in f(), it's called angle_f
//Output: fa is passed back to Newton, once it's calculated 
//        the value of the function fa.
//Purpose: double f calculates the value of the function at the angle

double fprime(double angle_fp);
//Function:fprime(double)
//Input: angle_newton is passed to fprime()
//Output: fpa is passed back to newton. 
//Purpose:  fprime calculates the derivative of the function fa 
//          at the angle.

//Global Constants
const int RADIUS =  100, MAX_ITER = 15;
const double EPSILON = 1e-8;

int main()
{
  double angle, rope;
  //Function Calls
  angle = GetGuess();  
  angle = DegToRad(angle);
  angle = Newton(angle); 
  rope = CalcRope(angle); 
  angle = RadtoDeg(angle);
  DisplayResult(angle, rope);
}

//Getting initial guess from user in degrees
double GetGuess()  
{
  double guess; 
  cout << "Enter an initial guess for theta in degrees." << endl; 
  cout << "The angle needs to be between 0 and 90." << endl;
  cin >> guess;
  
  while( guess < 0 || guess > 90 || cin.fail() )
    {
      cin.clear();
      cin.ignore(100, '\n');
      cout << "Bad input detected." << endl;
      cout << "Please reenter an angle between 0 and 90" << endl;
      cin >> guess;
    }
  cout << "You have entered: " << guess << " degrees" << endl;
  return guess;
}

//Converting from Degrees to Radians
double DegToRad(double angle_rad) 
{
  angle_rad = angle_rad * M_PI/180;
  return angle_rad;
}

//Converting back to Degrees
double RadtoDeg(double angle_deg)
{
  angle_deg = angle_deg * 180/M_PI;
  return angle_deg;
}

//Newton's Method formula
double Newton(double angle_newton)
{
  for(int i = 0; i < MAX_ITER; i++)
    {
      //Function calls f() and fprime() to calculate the
      // new angle each time
      angle_newton = angle_newton 
	- f(angle_newton)/fprime(angle_newton);
    }
  return angle_newton;
}

//Displays results of Newton's Method
void DisplayResult(double angle_final, double rope_final)
{
  cout << fixed << setprecision(2);
  //If Newton failed, this will be the output.
  if( angle_final <= 0 || angle_final > 90 ||
      f(angle_final*M_PI/180) > EPSILON )
    {
      cout << "Newton's Method failed to find "
	   << "the length of the rope" << endl;
      cout << "Please run the program again and try again." << endl;
    }
  //Angle and rope output from here
  else
    {
      cout << "The rope length is " << rope_final << endl;
      cout << "The angle is " << angle_final << endl;
    }
}

// This function will be called by Newton()
double f(double angle_f) 
{
  double fa = 0;
  fa = 4*angle_f*pow(cos(angle_f),2)
    + M_PI/2 - 2*angle_f - sin(2*angle_f);
  return fa;
}

// This function will be called by Newton()
//This function is the derivative of the above function
double fprime(double angle_fp)
{
  double fpa = 0;
  fpa = 4*pow(cos(angle_fp),2)
    - 8*cos(angle_fp)*sin(angle_fp) 
    - 2 - 2*cos(2*angle_fp);
  return fpa;
}

//Calculates length of Rope when angle is found
double CalcRope(double angle_rope)
{
  double rope;
  rope = 2*RADIUS*cos(angle_rope);
  return rope;
}
