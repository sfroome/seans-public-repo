// Author: Laura Marik
// File: WriteData.cpp
// Purpose:  a simple example of an output file stream

#include <iostream>
#include <fstream>
using namespace std;

int main()
{
  const int SIZE = 5;
  int temperatures[SIZE] = {32,14,100,40,-40};
  ofstream fout;  // construct an output file stream
  string filename = "/outfile.dat";  // this is its name

  // the function "open" requires an array of characters
  // you get this from a string by calling the function c_str()
  fout.open( filename.c_str() );  // open the file
  if ( fout.fail() )
    {
      cout << "Could not open " << filename << endl;
    }
  else
    {
      for ( int i = 0; i < SIZE; i++ )
        {
          fout << temperatures[i] << endl;  // write temperature to file
        }
      fout.close();  // close the file
    }

}
