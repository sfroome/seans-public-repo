// Author: Laura Marik
// File: ReadData.cpp
// Purpose: a first look at ifstreams (input file streams)

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
  const int SIZE = 5;
  int temperatures[SIZE];

  ifstream fin;  // construct an ifstream object called fin
  fin.open("infile.dat");  // open the file named infile.dat for reading
  if ( fin.fail() )
    {
      cout << "infile.dat could not be opened" << endl;
    }
  else
    {
      for ( int i = 0; i < SIZE && !fin.fail(); i++ )
        {
          fin >> temperatures[i];  // read a temperature from the file
	  if(fin.fail())
	    {
	      fin.clear();
	      fin.ignore(1);
	    }
	  else
          cout << temperatures[i] << endl;
        }

    }
  fin.close();  // close the file named infile.dat
}
