#include<iostream>
#include<cmath>


using namespace std;

int main()
{
  double x, n;
  double num = 1, denom =1,  sum = 1, term =0;


  cout << "Enter the nth term and the x value:" << endl;
  cin >> n >> x;

  if(n < 2)
    {
      cout << 1 << endl;
    }

  for(int i = 2; i < n; i++)
    {
      num = i*pow(x, i-1);
      denom = i-1;
      term = num/denom;
      sum += term;
    }

  cout << sum << endl;



}
