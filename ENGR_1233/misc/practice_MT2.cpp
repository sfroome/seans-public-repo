#include <iostream>
#include <iomanip>
using namespace std;

void readbeam(double & f0, double & f1);
double difference(double emittedBeam, double reflectedBeam);
double add(double emittedBeam, double reflectedBeam);
double speed(double x, double y);
void display_result(double v);

int main()
{
  double f0, f1, v, x, y, z;
  readbeam(f0, f1);
  x = difference(f0, f1);
  y = add(f0, f1);
  v = speed(x, y);
  display_result(v);
}

void readbeam(double & f0, double & f1)
{
 cout << "Enter two values " << endl;
  cin >> f0 >> f1; 
  while(cin.fail() || f0 <= 0 || f1 < 0)
   { 
     cout << "Bad input. Try again moron." << endl;
     cin.clear();
     cin.ignore(100, '\n');
     cin >> f0 >> f1;
    }
}

double difference(double emittedBeam, double reflectedBeam)
{
  double x;

  x = emittedBeam - reflectedBeam;
  return x;
}

double add(double emittedBeam, double reflectedBeam)
{
  double y;
  y = reflectedBeam + emittedBeam;
  return y;
}

double speed(double x, double y)
{
  double v;
  v = (6.685*108)*x/y;
  return v;
}

void display_result(double v)
{
  cout << setw(12) << fixed << setprecision(2)
       << "The speed of the vehicle is "
       << v
       << endl;
}
