#include<iostream>
#include<cmath>

using namespace std;

int main()
{
  double num = 1, denom = 1, term = 1, sum = 0;
  double x;  
 
  int n, sign = -1;
  

  cout << "Enter a value for x and the number of iterations" << endl;
  cin >> x >> n;
 
 


  for(int i = 1; i <= n; i++)
    {
      sign *= -1;
      num = pow(x, i);
      denom = i;
      term = num/denom;
      sum += term;
    }

  cout << "The actual value of ln(1+x) is: " << log(x+1) << endl;
  cout << "The approximate value of it is: " << sum << endl;





}
