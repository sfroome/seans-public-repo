#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
  double x = 1.2;
  cout <<  setprecision(4) << x << endl;
  cout <<  x << endl;
  cout << scientific << x << endl;
  cout << fixed << x << endl;

}
