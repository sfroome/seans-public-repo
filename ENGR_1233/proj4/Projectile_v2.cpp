/************************************************************
File: Projectile_v2.cpp
Author: Sean Froome
Description: Updated from version 1. In this version, a pig appears
randomly on the board. User wins when the pig is bumped off the
board.

TEST CASES
NOTE: My grid is slightly larger than the original specified grid (125x50 instead
of 100x40). This means some larger velocities are possible without the bird going off
the top of the grid. My grid should still display fine on any of the monitors in the lab.

Case1:User enters a non-digit (tested: l)
Expected Output:
"Please Reenter:"
(User prompted again until a digit is entered)

Case2: User enters values that will shoot the bird above the max
height of the grid (tested: 75 deg, 50 m/s)
Expected Output:
"Initial values are too large
Enter an angle between 0 and 90 degrees:"
(User is prompted to reenter until the bird won't go above the grid)

Case3: User enters values that will shoot the bird off the
right side of the grid (tested: 50, 40)
(Bird will go off the right side. This is permitted to make it
easier to hit the pig when the pig is close to the right edge
of the grid.)

Case4: User enters values that hit the pig (pig is set at [30,30])
User enters 65, 27
Expected Output:
"Take another shot
Last angle entered: 65.00
Last velocity entered: 27.00
Enter an angle between 0 and 90 degrees:"
(This will show up regardless of whether the pig is hit or not)

Case5: User enters a sequence of values that knock the pig off
the screen: (Pig is again set at [30, 30])
User enters the following:
65, 27
51, 30
56, 31
42.5, 33
36.5, 35
Expected output after the last set of values are entered:

"The bird has been knocked off the grid.
YOU WIN!"

************************************************************/
#include <iostream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <unistd.h>


using namespace std;

struct vec
{
    double x,y;
};

void CreatePig( vec & pigpos);
void GetValues(double & angle, double & v0);
void GetMaxPos(double v0, double angle, vec & maxpos);
void DrawGrid(const vec & birdpos,  vec & pigpos, double & time, const vec & birdvel);
void CalcBirdPos(double time, double v0, double angle, vec & birdpos);
void CalcBirdVel(double time, double v0, double angle, vec & birdvel);
void MovePig( vec & pigpos, const vec & birdvel);

const double g = -9.81;
const double EPSILON = 1e-8;

int main()
{
    vec birdpos ={0,0}, birdvel, pigpos;
    vec maxpos;
    double time = 0;
    double angle, v0;

    cout << fixed << setprecision(2);
    CreatePig(pigpos);

    while(pigpos.x <= 125 - EPSILON && pigpos.y <= 50 - EPSILON && pigpos.y > 0 - EPSILON )
    {
	GetValues(angle, v0);
	while (angle > 90 || angle <= 0   || v0 <= 0)
	{
	    cout << "Bad input detected" << endl;
	    GetValues(angle, v0);
	}

	GetMaxPos( v0, angle, maxpos);

	while( maxpos.y > 50)
	{
	    cout << "Initial values are too large" << endl;
	    GetValues(angle, v0);
	    GetMaxPos(v0, angle, maxpos);
	}

	do
	{
	    CalcBirdVel( time, v0, angle, birdvel );
	    CalcBirdPos( time, v0, angle, birdpos );
	    DrawGrid( birdpos, pigpos, time, birdvel );
	    usleep(60000);
	    cout << endl;
	}
	while(birdpos.y >= 0 && birdpos.x <= 125);

	if(pigpos.x > 125 || pigpos.y > 50 || pigpos.y < 0 )
	{
	    cout << "The bird has been knocked off the grid." << endl;
	    cout << "YOU WIN!" << endl;
	}

	else
	{
	    cout << "Take another shot" << endl;
	    cout << "Last angle entered: "
		 << angle * 180/M_PI
		 << endl;
	    cout << "Last velocity entered: "
		 << v0
		 << endl;
	    time = 0;
	}
    }
}

void CreatePig(vec & pigpos)
{
    srand(time(0));
    pigpos.x = rand()%100+1;
    pigpos.y = rand()%40+1;
//    pigpos.x = 30;
//    pigpos.y = 30;
}

void DrawGrid( const vec & birdpos,  vec & pigpos, double & time, const vec & birdvel)
{
    const int rows = 50, cols = 125;
    char Grid[rows][cols] ={};

    for(int i = rows-1; i > 0; i--)
    {
	for(int j = 0;  j < cols; j++)
	{
	    Grid[i][j] = '-';

	    if(int(birdpos.x) == j && int(birdpos.y) == i)
	    {
		Grid[i][j] = '>';
	    }
	    if(int(pigpos.x) == j && int(pigpos.y) == i)
	    {
		Grid[i][j] = '@';
	    }
	    if(int(pigpos.x) == int(birdpos.x) && int(pigpos.y) == int(birdpos.y))
	    {
		MovePig( pigpos, birdvel);
	    }
	    cout << Grid[i][j];
	}
	cout << endl;
    }
    time += 0.1-EPSILON;
}

void GetValues(double & angle, double & velocity)
{
    cout << "Enter an angle between 0 and 90 degrees:" << endl;
    cin >> angle;
    while(cin.fail())
    {
	cout << "Please Reenter:" << endl;
	cin.clear();
	cin.ignore(100,'\n');
	cin >> angle;
    }
    angle = angle*M_PI/180;

    cout << "Enter an initial velocity:" << endl;
    cin >> velocity;

    while ( cin.fail() )
    {
	cout << "Please Reenter:" << endl;
	cin.clear();
	cin.ignore(100, '\n');
	cin >> velocity;
    }
}

void CalcBirdPos(double time, double v0, double angle, vec & birdpos)
{
    birdpos.x = cos(angle)*v0*time;
    birdpos.y = -4.9*pow(time,2)
	+ sin(angle)* v0*time;
}

void CalcBirdVel(double time, double v0, double angle, vec & birdvel)
{
    birdvel.x = cos(angle)*v0;
    birdvel.y = g*time + sin(angle)*v0;
}

void GetMaxPos(double v0, double angle, vec & MaxPos)
{
    MaxPos.x = abs((pow(v0,2)*sin(2*angle)/g));
    MaxPos.y = pow((sin(angle)*v0),2)/(-2*g);
}

void MovePig(vec & pigpos, const vec & birdvel)
{
    pigpos.x = pigpos.x + birdvel.x;
    pigpos.y = pigpos.y + birdvel.y;
}
