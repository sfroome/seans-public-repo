/*****************************************************************************************
Written by: Sean Froome
Filename: geometry.cpp
Program Description: Finds the area, perimeter (or circumference), and the Bounding Box
of either a square, triangle, or circle.
Test Cases:

          Case1: User Enters T for shape; user is then prompted to enter corner points of triangle 
                 User Inputs: 1 1 4 5 -2 5
                 Expected Output:
                "The Area is:" 12.00
                "The Perimeter is:" 16.00
                "The Bounding Box Corners are:" (4.00,5.00),(4.00,1.00),(-2.00,5.00), and (-2.00,1.00)

          Case2: User Enters C for shape: user is then prompted to enter center point and radius
                 User Inputs 1 1 5
                 Expected Output:               
                "The Area is:" 31.42 
                "The Circumference is:" 78.54
                "The Bounding Box Corners are:" (6.00,6.00),(6.00,-4.00),(-4.00,6.00), and (-4.00,-4.00)

          Case3: User Enters S for shape; user is then prompted to enter two opposite corners of a square
                 User Inputs: 5 4 1 1
                 Expected Outputs:                
                 "The Area is:" 14.14
                 "The Perimeter is:" 12.50
                 "The Bounding Box Corners are:"(5.00,4.50),(5.00,0.50),(1.00,4.50),(1.00,0.50)

          Case4: User Enters S for shape; user is then prompted to enter two opposite corners of a square
                 User Inputs: 1 1 1 1   (User has given two identical points)
                 Expected Outputs:                
                 "The Area is:" 0.00
                 "The Perimeter is:" 0.00
                 "The Bounding Box Corners are:" (1.00,1.00),(1.00,1.00),(1.00,1.00),(1.00,1.00)
         
          Case5: User Enters Bad Input; an incorrect letter (ie: an f), followed by a number (ie: 5)
                 Expected Output: "You have given an incorrect Response. Please enter either  c or C for circle, 
                 t or T for Triangle, or S or s for a Circle."
                 User is given this error message repeatedly if they continues to enter bad input
                 until the user enters a correct input.

          Case6: User Enters T for shape; user then prompted to enter corner points of triangle
                 User Inputs: 1 0 2 0 3 0 (Collinear Points)
                 Expected Outputs:                
                 "The Area is:" 0.00
                 "The Perimeter is:" 4.00
                 "The Bounding Box Corners are:" (3.00,0.00),(3.00,0.00),(1.00,0.00),(1.00,0.00)

          Case7: User Enters C for shape; user then prompted to enter center point and radius of circle 
                 User Inputs 0 0 0 (Origin center, and 0 radius)
                 Expected Outputs:
                 "The Area is:" 0.00
                 "The Circumference is:" 0.00
                 "The Bounding Box Corners are:" (0.00,1.00),(0.00,0.00),(0.00,1.00),(0.00,0.00)
          Case8: User chooses t/T, s/S, or c/C  but enters non numerical input, or a negative number
                 for the radius (in the case of a circle) 
                 Expected Output: "Bad output detected. Please try again"
                 User types in different input. If input is good, the program will continue, and 
                 should behave like one of the above cases (except of course case5).
****************************************************************************************************/
#include<iostream>
#include<cmath>
#include<iomanip>

using namespace std;

int main()
  
{
  char answer; // User's answer for which shape to  calculate. 
  double x1,x2, x3,x4, y1, y2, y3, y4; // User inputted coordinates (center points, or corners, depending on shape)
  double r, a, p, s; //Radius, Area, Perimter, and Half the Perimeter (for Heron's Formula)
  double sidea, sideb, sidec; // Lengths of the sides of the Triangle
  double csq, asq; // Diagonal length of the Square, and the length of the side of the square
  double d1x, d2x, d1y, d2y; //direction vector components
  double maxx, maxy, minx, miny; // max and min values of shapes
  
  cout << "This program can find the perimeter, "
       << "area, and bounding box of a circle, a  triangle or a square. " << endl;
  cout << "Please enter the letter corresponding to the type of shape you wish to find: " << endl;
  cout << "c for Circle, t for Triangle, or s for square. "
       << "Input is not case sensitive." <<  endl;
  cout << fixed << setprecision(2) << endl; //Ensuring only two sigdigs after the decimal
  
  do
    {
      cin >> answer;
      
      switch (answer)
	{
	  
	case 'c': // If user declares a circle
	case 'C':  
	  
	  cout << "Please enter the center point and the radius of your circle." << endl;
	  cout << "Enter your values in the following order:" << endl;
	  cout << "x, y, followed by the radius." << endl;
	  
	  cin >> x1 >> y1 >> r;

	  while(cin.fail() || r < 0)
	  {
	      cout << "Bad input detected. Please try again." << endl;
	      cin.clear();
	      cin.ignore(100, '\n');
	      cin >> x1 >> y1 >> r;
	  }
	  
	  a = M_PI*r*r; // Area and Circumference Calculations
	  p = 2*M_PI*r;
	  maxx = x1+r; // X and Y  maximum and minimum values
	  maxy = 1+r;
	  minx = x1-r;
	  miny = y1-r;  
	  
	  cout << "The circumference of the circle is: " << p << endl;
	  cout << "The area of the circle is: " << a << endl;
	  cout << "The corners of the bounding box are: "  // Coordinates of Bounding Box
	       << "(" << maxx << "," << maxy << "), " 
	       << "(" << maxx << "," << miny << "), "
	       << "(" << minx << "," << maxy << "), and "
	       << "(" << minx << "," << miny << ")" 
	       << endl;
	  break;
	  
	case 't': // If user declares a Triangle
	case 'T':
	  
	  cout << "Please enter the coordinates of the corners of your triangle" <<endl;
	  cout << "Please enter your coordinates in the following order:" << endl;
	  cout << "(x1,y1), (x2,y2), (x3,y3). Don't include brackets or commas."
	       << "Leave spaces between each number."
	       << endl;

	  cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;

	  while (cin.fail())
	  {
	      cin.clear();
	      cin.ignore(100,'\n');
	      cout << "Bad input detected. Please try again." << endl;
	      cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
	  }	  

	  sidea = sqrt((pow((x1-x2),2)) + (pow((y1-y2),2))); // Calculating sides of Triangle
	  sideb = sqrt((pow((x2-x3),2)) + (pow((y2-y3),2)));
	  sidec = sqrt((pow((x3-x1),2)) + (pow((y3-y1),2)));
	  p = sidea + sideb + sidec; // Perimeter
	  s = p/2; // Half of perimeter; needed for following formula
	  a = sqrt(s*(s-sidea)*(s-sideb)*(s-sidec)); // Heron's Formula used to calculate triangles area
	  
	  maxx = max(max(x1,x2),max(x2,x3)); // Max and Min calculations 
	  maxy = max(max(y1,y2),max(y2,y3));
	  minx = min(min(x1,x2),min(x2,x3));
	  miny = min(min(y1,y2),min(y2,y3));
	  
	  cout << "The Perimeter of this triangle is " << p << endl;
	  cout << "The Area of this triangle is " << a << endl;
	  cout << "The corners of the bounding box are: " << endl; // Bounding Box Coordinates
	  cout << "(" << maxx << "," << maxy << "), "
	       << "(" << maxx << "," << miny << "), "
	       << "(" << minx << "," << maxy << "), and "
	       << "(" << minx << "," << miny << ")"
	       << endl;     
	  break; 
	  
	case 's': // If user declares a Square
	case 'S':
	  
	  cout << "Please enter the coordinates of two opposing corners of your square" <<endl;
	  cout << "Enter your coordinates in the following order:" << endl;  
	  cout << "(x1,y1) (x2,y2)"
	       << "Leave out brackets and commas, "
	       << "and leave a space between each number entered." 
	       << endl;
	  
	  cin >> x1 >> y1 >> x2 >> y2;

	  while (cin.fail())
	  {
	      cout << "Bad input detected. Please try again." << endl;
	      cin.clear();
	      cin.ignore(100,'\n');
	      cin >> x1 >> y1 >> x2 >> y2;
	  }
	  
	  csq = sqrt(pow((x2-x1),2) + pow((y2-y1),2)); // Pythagoras theorem used to calculate area of both halfs of square
	  asq = sqrt(pow(csq,2)/2);  
	  
	  a = asq*asq; // Calculations for Area and Perimeter
	  p = 4*asq;
	  
	  d1x = x2 - x1; // Points of First Position Vector
	  d1y = y2 - y1;
	  d2x = y2-y1; // Points of Second Position Vector
	  d2y = (x2-x1)*-1;
	  
	  x3 = x1 + d1x/2 + d2x/2; // 3rd and 4th Points of Square
	  y3 = y1 + d1y/2 + d2y/2;
	  x4 = x1 + d1x/2 - d2x/2;
	  y4 = y1 + d1y/2 - d2y/2;
	  
	  maxx = max(max(x1,x2),max(x3,x4)); // Max and Min Values
	  maxy = max(max(y1,y2),max(y3,y4));
	  minx = min(min(x1,x2),min(x3,x4));
	  miny = min(min(y1,y2),min(y3,y4));
	  
	  cout << "The perimeter is: " << p << endl;
	  cout << "The area is: " << a << endl;
	  cout << "The coordinates of the bounding box corners are: " // Coordinates of Bounding Box 
	       << "(" << maxx << "," << maxy << "), "
	       << "(" << maxx << "," << miny << "), "
	       << "(" << minx << "," << maxy << "), and "
	       << "(" << minx << "," << miny << ") "
	       << endl; 
	  break;
	  
	default: // If user doesn't give proper input
	  
	  cout << "You gave an incorrect response. Please type either:" << endl;
	  cout << "c or C for a Circle, "
	       << "t or T for a Triangle, or lastly "
	       << "s or S for a Square."
	       << endl;     
	  cout << "Please enter your response now." << endl;
	  break; 
	}
    }
  
  while (answer != 'c' && 
	 answer != 'C' &&
	 answer != 'T' &&
	 answer != 't' &&
	 answer != 's' &&
	 answer != 'S');
}
