module filter_3 (
          input clk,
			 input signed [17:0] x,
			 output reg signed [17:0] y
			         );					

//assuming output only depends on max of entire system 
//max output value = 0.5537 

	wire signed [17:0] y1; //3s15
	
	//first stage of filter is filter_1 unmodified
	filter_1 stage1 (
		.clk(clk),
		.x(x),
		.y(y1)
	);
	
	reg signed [17:0] x1, x2; //3s15
	reg signed [35:0] x1_times_cosphi; 
	reg signed [17:0] adder_out; //1s17
	
	//declare registers
	always @ (posedge clk)
		x1 = y1;
	always @ (posedge clk)
		x2 = x1;

		
	//declare multipliers
	wire signed [17:0] a = -18'sd92682; //2s16 aka 2*cos(pi/4)
	always @ *
		x1_times_cosphi = x1 * a; //5s31
	
	
	//declare adders
	always @ *
		adder_out = x1_times_cosphi[31:14] + {x2[15:0], 2'b00}; //1s17
	
	
	//shape the output
	always @ *
		y = {y1[15:0], 2'b00} + adder_out; //1s17
	

endmodule

