module SUT (
     input clk,
	  input [17:0] x,
	  output reg [17:0] y
	         );
always @ *
    y = x;
	 
endmodule
