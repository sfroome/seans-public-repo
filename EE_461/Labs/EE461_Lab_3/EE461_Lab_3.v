module EE461_Lab_3(
			input CLOCK_50,
			input [17:0]SW,
			input [3:0] KEY,
			input [13:0]ADC_DA,
			input [13:0]ADC_DB,
			output reg [3:0] LEDG,
			output reg [17:0] LEDR,
			output reg[13:0]DAC_DA,
			output reg [13:0]DAC_DB,
			// output	I2C_SCLK,
			// inout		I2C_SDAT,
			output	ADC_CLK_A,
			output	ADC_CLK_B,
			output	ADC_OEB_A,
			output	ADC_OEB_B,
			// input 	ADC_OTR_A,
			// input 	ADC_OTR_B,
			output	DAC_CLK_A,
			output	DAC_CLK_B,
			output	DAC_MODE,
			output	DAC_WRT_A,
			output	DAC_WRT_B
			// ,input 	OSC_SMA_ADC4,
			// input 	SMA_DAC4
			 );
			 
	localparam  //DATA_MASK = 18'H3FE00, // must be 18 bits,
	                          // initially truncate to keep 9 bits

	          // COEF_MASK = 18'H3FFFF, // must be 18 bits,
	                          // initially don't truncate
				  //NCO_1_FREQ = 12'H1A0; // Step 18 Part 1 of Lab 3
				 // NCO_1_FREQ = 12'H1A3; // must be 12 bits, 
				                // frequency in cycles/sample in 0u12 format
									 // Use this NCO for the rest of Part 1
											  
	         	DATA_MASK = 18'H3FFFF, // For Part 2
					COEF_MASK = 18'H3FE00, // For Part 2			
					NCO_1_FREQ = 12'H1A3; //  For Part 2	
					// In part 1, the Coefficients were not masked at all.
					// You can tell that by the fact that every single bit 
					// was anded with a 1. Only coefficient bits that are 0
					// would result in a bit being truncated/masked.
					
					//The data bits in Part one as stated above, had the 9 LSB's
					// truncated at the output. In part 2, none are truncated as stated.
					
	reg clk;

	reg [11:0]  phase_1;
	reg [11:0] quantization_noise_for_8_bit_sinusoid;
	reg signed [13:0] signal_to_DAC,
                     full_scale_random_sequence;
	reg signed [17:0] noise_in_SUT_out;
	reg signed [17:0] stimulus,
                  	randomized_sinusoid;
	reg [21:0] LFSR;
		
	wire dwell_pulse;
	wire [17:0] sweep_gen_freq;
	wire signed [17:0] gold_std_out, SUT_out;
	wire signed [11:0] NCO_1_out;

	/******************************
	*	generate the clock used for sampling ADC and
	*  driving the DAC, i.e. generate the sampling clock
	*/
	always @ (posedge CLOCK_50)
	clk = ~clk;
	// end generatating sampling clock
	
	/************************
			  Set up DACs
			*/
			assign DAC_CLK_A = clk;
			assign DAC_CLK_B = clk;
			
			
			assign DAC_MODE = 1'b1; //treat DACs seperately
			
			assign DAC_WRT_A = ~clk;
			assign DAC_WRT_B = ~clk;
			
		always@ (posedge clk)// convert 1s13 format to 0u14
								//format and send it to DAC A
		DAC_DA = {~signal_to_DAC[13],
						signal_to_DAC[12:0]};		
		
				
		always@ (posedge clk) // DAC B is not used in this
					 // lab so makes it the same as DAC A
				 
			DAC_DB = {~signal_to_DAC[13],
						signal_to_DAC[12:0]} ;	
			/*  End DAC setup
			************************/
	
	/********************
	*	set up ADCs, which are not used in this lab 
	*/
	(* noprune *) reg [13:0] registered_ADC_A;
	(* noprune *) reg [13:0] registered_ADC_B;
	
	assign ADC_CLK_A = clk;
			assign ADC_CLK_B = clk;
			
			assign ADC_OEB_A = 1'b1;
			assign ADC_OEB_B = 1'b1;

			
			always@ (posedge clk)
				registered_ADC_A <= ADC_DA;
				
			always@ (posedge clk)
				registered_ADC_B <= ADC_DB;
				
			/*  End ADC setup
			************************/	
	
	
	/******************************
			Set up switches and LEDs
			*/
			always @ *
			LEDR = SW;
			always @ *
			LEDG = {dwell_pulse, KEY[2:0]};
			
	//end setting up switches and LEDs

	// instantiate the sweep generator	
		SweepGenerator sweep_gen_1(.clock(clk),
							  .sweep_rate_adj(3'b010),
							  .dwell_pulse(dwell_pulse),
							  .reset(~KEY[3]),
							  .lock_stop(~KEY[1]),
							  .lock_start(~KEY[0]),
							  .freq(sweep_gen_freq));
	// dwell pulse is assigned to LEDG[3] so that it is not optimized out
	
							  
	//  make the phase accumulator for the NCOs 
   //  The frequency for NCO_1 is selected between
   //  a swept or fixed frequency under the
   // control of switch	SW[0]
	always @ (posedge clk)
	if (SW[0]==1'b0)
			phase_1 = phase_1 + sweep_gen_freq[17:6];
	else 
	      phase_1 = phase_1 + NCO_1_FREQ;
	

							 
// extract the quantization noise of a sinusoid by extracting
// the 4 least significant bits from a sinusoid with  12 bits 
// precision. The shift the extracted bits 5 bits to the left
// to introduce a gain of 5*6.02=30.1dB 

   always @ (posedge clk)
	quantization_noise_for_8_bit_sinusoid =
	     { 3'b0, NCO_1_out[3:0], 5'b0};
							 
// calculate the noise resulting from the quantization of the
// data and the coefficients by taking the difference between
// the outputs of the SUT and gold standard.
	
	always @ (posedge clk)
	noise_in_SUT_out = {($signed(SUT_out[12:0]) - $signed(gold_std_out[12:0]) ),
	                      5'H0};
	


// contruct a randomized sinusoid to use as a broadband signal
// This randomized sinusoid has a flat power spectrum and its
// total power is equal to that of the sinusoid

   always @ (posedge clk)
	if (full_scale_random_sequence[13] == 1'b1)
   randomized_sinusoid = {NCO_1_out,6'b0};
   else	
	randomized_sinusoid = {-NCO_1_out,6'b0};
	/**********************************
	 make the binary full scale random signal
	***********************************/
	//make 22 bit LFSR
	always @ (posedge clk)
	LFSR = {LFSR[20:0], LFSR[21]~^LFSR[20]};
	
	always @ *
	if (LFSR[0]==1'b1)
	  full_scale_random_sequence = 14'b01_1111_1111_1111;
	else
	  full_scale_random_sequence = 14'b10_0000_0000_0001;
	

	

	
	// make the data selector for the input to
	// the filters

	always @ (posedge clk)
	case(SW[7:6])
	2'b00: stimulus = {NCO_1_out,6'b0};
	2'b01: stimulus = {NCO_1_out[11], NCO_1_out,5'b0}; 
	                     // NCO_1_out divided by 2
	2'b10: stimulus = randomized_sinusoid;							
	2'b11: stimulus = {full_scale_random_sequence, 4'H0};
	endcase

	// make the data selector for signal_to_DAC
	// as well as a sample rate reducer under the
	// the control of SW[10]
	
	reg [2:0] counter;
	always @ (posedge clk)
	counter = counter + 3'b1;//counter used to 
	                       // i.e. hold the data
								  // for 8 samples effectively
								  // reducing the sample rate
								  // by a factor of 8
	
	always @ (posedge clk) 
	if ( (counter == 3'b0) || (SW[10] == 1'b0)) // condition
	                               // for sending next sample
											 // sample to the DAC
											 // if SW[0] == 1'b0 every 
											 // sample is sent to the DAC
	case (SW[3:1])
	3'b000:	 signal_to_DAC = gold_std_out[17:4];
	3'b001:   signal_to_DAC = SUT_out[17:4];
   3'b010:   signal_to_DAC = noise_in_SUT_out[17:4];
	3'b011:   signal_to_DAC = 
	              {quantization_noise_for_8_bit_sinusoid, 2'b0};
	                  // quantization noise for an  8 bit
							// sinusoidal output that has been
						   //	shifted by 5 bits to introduce a 
							// gain of 30.1 dB

	3'b100: 	 signal_to_DAC = stimulus[17:4];					
	3'b101:   signal_to_DAC = stimulus[17:4];
	3'b110:   signal_to_DAC = stimulus[17:4];
	3'b111:   signal_to_DAC = stimulus[17:4];
	endcase	
   else // hold the previously sampled value
	    signal_to_DAC = signal_to_DAC;
	

	
	// intantiate the phase-to-voltage ROM 
	// for NCOs 1 and 2
	ROM_for_12_x_12_NCO NCO_ROM_1 (
		  .address_a(phase_1),
		  .address_b(12'H000),
		  .clock(clk),
		  .q_a(NCO_1_out),
		  .q_b());
		  
	// Instantiate the system under test
/*	system_under_test SUT_1 (.clk(clk),
	                         .SUT_in(NCO_1_out),
									 .SUT_out(SUT_out) );
									 
*/
approx_brick_wall_filt_with_masks filt_without_mask (.clk(clk),
                            .data_mask(18'H3FFFF), //don't mask
									 .coef_mask(18'H3FFFF), // don't mask
	                         .x_in(stimulus),
									 .y(gold_std_out) );
approx_brick_wall_filt_with_masks filt_with_mask (.clk(clk),
                            .data_mask(DATA_MASK), 
									 .coef_mask(COEF_MASK), 
	                         .x_in(stimulus),
									 .y(SUT_out) );
		
		
endmodule	