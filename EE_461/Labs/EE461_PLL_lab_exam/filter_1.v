module filter_1 (
          input clk,
			 input signed [17:0] x,
			 output reg signed [17:0] y 
			         );
	reg signed [17:0] y1, y2;						
	(* noprune *) reg signed [35:0] y1_times_cosphi, y2_times_r2;
	(* noprune *) reg signed [17:0] adder_out;
	
	wire signed [17:0] a; // 2*r*cos(phi)
	wire signed [17:0] a1; // -r^2
	
	//debugging registers
//	(*noprune*) reg signed [35:0] times_a_out, times_b_out;
//	(*noprune*) reg signed [17:0] adder_out_tap, x_input_tap;
	
//	always @ (posedge clk) 
//		times_a_out = y1_times_a;
//	always @ (posedge clk)
//		times_b_out = y2_times_b;
//	always @ (posedge clk)
//		adder_out_tap = adder_out;
//	always @ (posedge clk)
//		x_input_tap = {x[17], x[17], x[17:2]};
	
	// declare registers
	always @ (posedge clk)
		y1 = y;
	
	always @ (posedge clk)
		y2 = y1;
		 
	
	/*
	// filter constants (18 bits)
	// max gain is 7.4432
	// input cosine, deigning for half scale, max value is 3.7216
	// output needs to be 3s15
	*/
	
	assign a = 18'sd83414; //2s16  // 2*r*cos(phi)
	assign a1 = -18'sd106168;	//1s17  // -r^2
	
	// declare multipliers
	always @ *
		y1_times_cosphi = y1 * a; // 5s31
	
	always @ *
		y2_times_r2 = y2 * a1; // 4s32
	
	//declare adders
	always @ *
	adder_out = y1_times_cosphi[33:16] + y2_times_r2[34:17]; // 3s15
	//adder_out = y1_times_cosphi[31:14] + y2_times_r2[32:15]; // 1s17	
	
	// shape the output
	always @ *
	y = {x[17], x[17], x[17:2]} + adder_out; // 3s15 (remember x is a 1s17 initially)
	//y = {x[17], x[17:1]} + adder_out;
		
endmodule

