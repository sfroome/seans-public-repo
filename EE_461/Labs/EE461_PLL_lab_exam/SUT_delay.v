module SUT_delay (
     input clk,
	  input [17:0] x,
	  output reg [17:0] y
	         );
always @ (posedge clk)
    y = x;
	 
endmodule
