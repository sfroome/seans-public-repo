module filter_4 (
	input clk,
	input signed [17:0] x,
	output reg signed [17:0] y //1s17
);

//assuming output only depends on max of entire system 
//max output value = 0.5537 therefore output needs to be 1s17

	wire signed [17:0] y1; //2s16 from filter_2
	reg signed [17:0] y11, y12;						
	reg signed [35:0] y1_times_cosphi, y2_times_r2;
	reg signed [17:0] adder_out;
	
	wire signed [17:0] a; // 2*r*cos(phi)
	wire signed [17:0] a1; // -r^2
	
	//first stage of filter
	filter_2 stage1 ( 					//max val = 1.707
		.clk(clk),
		.x(x),
		.y(y1)
	);
												//max output value of entire filter = 0.5537 
	//declare registers
	always @ (posedge clk)
		y11 = y;
	always @ (posedge clk)
		y12 = y11;

	// delcare multipliers
	assign a = 18'sd83414; //2s16  // 2*r*cos(phi)
	assign a1 = -18'sd106168;	//1s17  // -r^2
	
	always @ *
		y1_times_cosphi = y11 * a; // 3s33
	
	always @ *
		y2_times_r2 = y12 * a1; // 2s34
	
	//declare adders
	always @ *
		adder_out = y1_times_cosphi[33:16] + y2_times_r2[34:17]; // 1s17
		
	// shape the output
	always @ *
		y = {y1[16:0], 1'b0} + adder_out; //1s17

endmodule


