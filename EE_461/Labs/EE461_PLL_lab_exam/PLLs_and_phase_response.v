module PLLs_and_phase_response
(
    input               clock_50,
    input [17:0]        SW,
    input [3:0]         KEY,
    input [13:0]        ADC_DA,
    input [13:0]        ADC_DB,
    
    output reg [3:0]    LEDG,
    output reg [17:0]   LEDR,
    output reg[13:0]    DAC_DA,
    output reg [13:0]   DAC_DB,
    // output           I2C_SCLK,
    // inout            I2C_SDAT,
    output              ADC_CLK_A,
    output              ADC_CLK_B,
    output              ADC_OEB_A,
    output              ADC_OEB_B,
    // input            ADC_OTR_A,
    // input            ADC_OTR_B,
    output              DAC_CLK_A,
    output              DAC_CLK_B,
    output              DAC_MODE,
    output              DAC_WRT_A,
    output              DAC_WRT_B
    // ,input           OSC_SMA_ADC4,
    // input            SMA_DAC4
);


            

    //************************************************************              
    //  DECLARATIONS                    
    //------------------------------------------------------------
    
    reg                                 clk; // sampling rate clock
                                        
    (* noprune *)reg [13:0]             registered_ADC_A;
    (* noprune *)reg [13:0]             registered_ADC_B;
	 

        
  
    
                    
    //************************************************************              
    //  Make the sampling rate clock from the input 50 MHz clock
    //------------------------------------------------------------
    
    always @ ( posedge clock_50 )
        clk = ~clk;                 // this is the system clock
                                    // it is half the frequency of
                                    // the 50 MHz clock 
                                    
    //************************************************************              
    //  Set up switches and LEDs
    //------------------------------------------------------------
    
    always @ *
        LEDR = SW;
        
    always @ *
        LEDG = KEY;
                    
    //************************************************************              
    //  Set up DACs
    //------------------------------------------------------------
    
    assign DAC_CLK_A = clk;
    assign DAC_CLK_B = clk;
                    
    assign DAC_MODE = 1'b1;         //treat DACs seperately
                    
    assign DAC_WRT_A = ~clk;
    assign DAC_WRT_B = ~clk;
                    
    always @ (posedge clk)          // make DAC A echo ADC A
        DAC_DA = registered_ADC_A[13:0];
                                
    always @ ( posedge clk ) 
         /* Connect the output multiplexer,  which is called
            output_mux, to the register (i.e. D-flip flops)
            that drive the pins for DAC B. 
            In the process convert the signed output of
            the NCO to an unsigned number, which is required
            by DAC B, by inverting the sign bit. */                 
        DAC_DB = { ~output_mux[13], output_mux[12:0] };

    //************************************************************              
    //  Set up ADCs
    //------------------------------------------------------------
    
    assign ADC_CLK_A = clk;
    assign ADC_CLK_B = clk;
                    
    assign ADC_OEB_A = 1'b1;
    assign ADC_OEB_B = 1'b1;
    
    always @ ( posedge clk )
        registered_ADC_A <= ADC_DA;
                        
    always @ ( posedge clk )
        registered_ADC_B <= ADC_DB;
	
                        
    //************************************************************              
    // Start making the circuit for this lab    
    //************************************************************  

	 
    //************************************************************              
    // Make the signal generator  cicuit    
    //************************************************************
	  (* noprune *) reg [16:0]                 seventeen_bit_counter;
	  (* noprune *) reg signed [15:0]           x, x_for_model, ramp; // 0s16	
      reg signed [15:0]                        phase_signal; // 0s16
      wire signed [13:0]                       NCO_a, NCO_b; // 1s13		
	   reg signed [20:0]	                       two_fo_noise; // 1s20
 
	 
     always @ (negedge clk)
	  seventeen_bit_counter = seventeen_bit_counter + 17'b1;
	  
	  
	  always @ (posedge clk)
	  ramp = ramp + {4'H0, SW[7:0], 4'H0};
	  
	  
     // make ROM for the two NCO in the signal generator	  
	  NCO_ROM_Na14_Nd14 signal_NCO_ROM (
	        	.address_a(ramp[15:2]),
	         .address_b( (phase_acc_1[30:17]+14'H2000) ), // twice the phase of NCO_1
				                                // therefore frequency is 2fo
														  // phase is offset by 1/2 cycle 
														  // to match phase of the detector being
														  // modelled_detector_out
	         .clock(clk),
	         .q_a(NCO_a),
	         .q_b(NCO_b));

	  // make the two fo noise
	  always @ *
	  two_fo_noise = NCO_b * 7'sd41; // 1s13 x 0s7 = 1s20
	                                 // the 0s7 constant 7'sd41
												// is approximately 1/pi
	  	  
	  // make phase_signal
	  always @ *
	      case (SW[10:9])
			2'd0: phase_signal = 16'H0; // 0s16
			2'd1: phase_signal = {4'H0, SW[7:0], 4'H0}; // 0s16
			2'd2: phase_signal = ramp; // 0s16
			2'd3: phase_signal = { {3{NCO_a[13]}}, NCO_a[13:1] } ; // 0s16, NCO_a is divided by 8
			                                           // but since it has format 1s13
																	 // and x has format 0s15
																	 // the is another divide by 2
																	 // in the format change
	      endcase
			

	  
	  // make x_for_model
	  always @ (posedge clk)
	  if (SW[11]==1'b0)
	       x_for_model = phase_signal; //0s16
	  else
	       x_for_model = phase_signal + two_fo_noise[20:5]; // 0s16 
			                                        // Note: the 1s20 noise is
			                                        // truncated to 1s15 and then
																 // treated as 0s16 for adding
																 // which divides the noise by 2 
	   // make x
	  always @ (posedge clk)
	  x = phase_signal;
	  
	  
	 /*************************************************************
    *
	 * Make the linear model for the Phase Locked Loop
	 *
	**************************************************************/ 
	localparam                              //k_D = 16'sH0200; // 0s16  1/128 
														 // k_D of 1/128 is For parts 1 to 4, and part 5 Questions 1 and 2.
														 k_D = 16'sH0040; // 0s16  1/1024; for part 5 Questions 3 onward

														  
	localparam                              PI = 6'sb011_001; // 3s3, 3.125 approx pi (Also 25/2^3 = 3.125)
   (* preserve *) reg signed [15:0]        modelled_error; // 0s16
	(* noprune *)  reg signed [15:0]        x_minus_modelled_y; // 0s16 

	(* preserve *) reg signed [15:0]         modelled_y;
   (* noprune *) reg signed [31:0] 	       acc_in_model; // 0s32 accumulator used in the model
	(* preserve *) reg signed [31:0]        acc_in_model_d; // 0s32 - d input to accumulator
	(* noprune *) reg signed [21:0]        modelled_detector_out; // 3s19
	reg signed [31:0]                       k_D_x_modelled_detector_out; // 3s29
	
	
	// model the detectors with a small signal gain of pi
	always @ *
	modelled_error = x_for_model - modelled_y;
	
	always @ (posedge clk)
	modelled_detector_out = PI * modelled_error; // 3s3 X 0s16 = 3s19
	
	
	always @ *
	k_D_x_modelled_detector_out = k_D * $signed(modelled_detector_out[21:6]);
                                   // modelled_detector_out is truncated to 3s13
											  // prior to multiplication. Therefore, the
											  // format of output is 0s16 X 3s13 = 3s29
	
	always @ *
	acc_in_model_d = acc_in_model + {k_D_x_modelled_detector_out[28:0],3'b0};
                              // 0s32+0s32=0s32
									   // 3 MS bits of k_D_x_modelled_detector_out are
										// removed to change format from 3s29 to 0s32
	
	always @ (posedge clk)
	 acc_in_model = acc_in_model_d; // 0s32 accumulator for model error
	
	always @ * 
	modelled_y = acc_in_model_d[31:16]; // 16 MSBs of 0s32 = 0s16
	
	always @ (posedge clk)
	x_minus_modelled_y = x_for_model - modelled_y; //0s16 --  for debugging only
	                                     // declared with (*noprune*) to ensure
													 // its availability to SignalTap
	
	/*************************************************************
    *
	 * Make the Phase Locked Loop
	 *
	**************************************************************/ 
    //************************************************************              
    //  Make the two NCOs
    //------------------------------------------------------------

	 
	 reg [31:0]                          phase_acc_1; // 0u32
    reg [15:0]                          phase_1, phase_2; //0u16
     
    (* keep *) wire signed [17:0]       NCO_1, NCO_2,SUT_out; // 1s17

    // First make the phase accumulators
    always @ ( posedge clk )
        phase_acc_1 = phase_acc_1 + fo;


    // Next make the phase inputs to the ROM
    always @ *
      phase_1 = phase_acc_1[31:16] + x ; 


    always @ *
        phase_2 = phase_acc_1[31:16] + y ;
				
   
   // *******************************************
	// make the phase locked loop
	 
	 	 
	 reg signed [35:0]                   SUT_out_x_NCO_2; // 1s17 X 1s17 = 2s34
	 (* preserve *) reg signed [15:0]    detector_out;  //1s15 -- need the integer bit
	                                                 // to hold the double frequency noise
    (* preserve *) reg signed [31:0]    k_D_x_detector_out , // 0s16 X 1s15 = 1s31 
	                                     k_D_x_detector_out_reformatted, // 0s32                                   
                                        acc_d; //0u32
	 (* noprune *) reg signed [31:0]		 acc; // 0u32										 
    (* noprune *) reg signed [15:0]     x_minus_y, y;	// 0s16 

	 always @ *
	    SUT_out_x_NCO_2 = SUT_out * NCO_2; // 1s17 x 1s17 = 2s34
	 always @ *
	    detector_out = SUT_out_x_NCO_2[34:19];  //1s15 - needs the integer bit to handle the detector
		                                       // double frequency noise
	 
	 always @ * 
	    k_D_x_detector_out = k_D *  detector_out; // 0s16 X 1s15 = 1s31
	 
	 always @ * 
	    k_D_x_detector_out_reformatted = {k_D_x_detector_out[30:0],1'b0}; // reformatted to 0s32
	
	always @ *
	    acc_d = acc + k_D_x_detector_out_reformatted; //0s32
	  always @ (posedge clk)
	    acc = acc_d;
		 
	 always @ *
	    y = acc_d[31:16];
		 
	
	always @ (posedge clk)
	x_minus_y = x - y;
	
	
	 
	 
    // Make the rom fir the NCOs in the PLL
    NCO_ROM NCO_ROM_1 
    (
        .address_a( phase_1 ),
        .address_b( (phase_2+16'sH4000) ), // add 1/4 to convert the quadrature
		                                    // detector to an inphase detector
        .clock( clk ),
        .q_a( NCO_1 ),
        .q_b( NCO_2 )
    );
    
   

    //************************************************************              
    //  Instantiate the system under test.
    //------------------------------------------------------------

     //SUT SUT_1             //for parts 1 to 4 of lab
	  //SUT_delay SUT_1 	  // for part 5 of lab 
	  
	  exam_filter SUT_1
	  //filter_1 SUT_1 //For the last part of part 5 of the lab
	  //filter_2 SUT_1
	  //filter_3 SUT_1
	  //filter_4 SUT_1
	  //filter_5 SUT_1
	  (
        .clk( clk ),
		  //.x(NCO_1), //Use this one if not using Filters		          
		  .x({NCO_1[17],NCO_1[17:1]}), // Use this one if using Filters
		  .y( SUT_out )
		  
		 
    );//------------------------------------------------------------
             
    //************************************************************              
    //  Send signals to DAC B under control of switches SW[17:15]
    //------------------------------------------------------------
    reg signed [13:0] output_mux; //trimmed and connected to DAC B
    always @ *
       case (SW[17:15])
		 3'd0: output_mux = modelled_detector_out[19:6]; // 1s13
		 3'd1: output_mux = modelled_y[15:2];            // 0s14
		 3'd2: output_mux = detector_out[15:2];          // 1s13
		 3'd3: output_mux = y[15:2];                     // 0s14
		 3'd4: output_mux = x_for_model[15:2];                     // 0s14
		 3'd5: output_mux = x[15:2];                     // 0s14
		 3'd6: output_mux = x[15:2];                     // 0s14
		 3'd7: output_mux = x[15:2];                     // 0s14
		 endcase
    
    //************************************************************              
    //  Frequency control circuit
    //------------------------------------------------------------

    localparam  //fixed_frequency = 32'H1000_0000, // if set to zero
	                                              // the NCO becomes a sweep generator.
																 // USE ABOVE LINE FOR PARTS 1-4 OF LAB
																 // USE BELOW VALUE FOR PART 5
																 
					 fixed_frequency = 32'H0000_0000,  // make the NCO a sweep generator
	             sweep_rate = 32'd11,
                start_freq = 32'H0000_0000,
					 //start_freq = 32'H0400_0000, // 1/64 don't use doesn't work
					//start_freq = 32'H1000_0000, //1/16 don't use doesn't work either (wtf am I doing wrong?!)
              
				  span = 32'H8000_0000;// This span doesn't work with 
												//Max hold on the spectrum analyzer
												// as at f0 = 1/2 it blows up the
												//	spectrum with a power
			  // span = 32'H7000_0000 ;// This is 7/16
			  // span = 32'H7C00_0000; // This is 31/64 which is even closer to the desired 1/2
													// and won't cause the "explosion" in the spectrum analyzer
					 
    (* noprune *) reg [31:0] fo;   // frequency accumulator
    
    always @ ( posedge clk )
	 if (fixed_frequency == 32'H0) // make NCO a sweep generator
	 
        if ( ( fo[31] == 1'b1 ) || ( fo > start_freq + span ) )
            fo = start_freq;
        else
            fo = fo + sweep_rate;
				
	else
	         fo = fixed_frequency;
				

  
  
  
endmodule
