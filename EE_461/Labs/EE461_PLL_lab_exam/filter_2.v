module filter_2 (
	input clk,
	input signed [17:0] x,
	output reg signed [17:0] y 
	);
	
	reg signed [17:0] x1, x2; //1s17
	reg signed [35:0] x1_times_cosphi; 
	reg signed [17:0] adder_out; //2s16
	
	//declare registers
	always @ (posedge clk)
		x1 = x;
	always @ (posedge clk)
		x2 = x1;

	/*
		For half scale input, the max value is 1.7071
		Therefore, the output needs format 2s16
	*/
		
	//declare multipliers
	wire signed [17:0] a = -18'sd92682; //2s16 and equal to 2*cos(pi/4)
	always @ *
		x1_times_cosphi = x1 * a;
	
	
	//declare adders
	always @ *
		adder_out = x1_times_cosphi[34:17] + {x2[17], x2[17:1]};
	
	
	//shape the output
	always @ *
		y = {x[17], x[17:1]} + adder_out; //2s16






endmodule
