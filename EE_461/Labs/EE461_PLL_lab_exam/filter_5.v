module filter_5 (
	input clk,
	input signed [17:0] x,
	output reg signed [17:0] y 
);

//declarations
	reg signed [17:0] y1, y2;
	wire signed [17:0] a = 18'sd117965; // a = 0.9

// Stage 1 **********************
	reg signed [17:0] stage1Mid, stage1Midn_1;
	reg signed [35:0] stage1_times_a;
	
	
	//storage registers
	always @ (posedge clk)
		stage1Midn_1 = stage1Mid;
	
	//multipliers
	always @ *
		stage1_times_a = stage1Midn_1 * a;
	
	//adders
	always @ *
		stage1Mid = { {4{x[17]}}, x[17:4]} + stage1_times_a[34:17];
		
	always @ *
		y1 = stage1Mid + stage1Midn_1; //5s13


// Stage 2 **********************
	reg signed [17:0] y1n_1;

	//storage registers
	always @ (posedge clk)
		y1n_1 = {y1[17], y1[17:1]};
	
	//adders
	always @ *
		y2 = {y1[17], y1[17:1]} + y1n_1; //6s12
	


// Stage 3 **********************
	reg signed [17:0] yn_1;
	reg signed [35:0] yn_1_times_a;
	
	//storage registers
	always @ (posedge clk)
		yn_1 = y;
		
	//multipliers
	always @ *
		yn_1_times_a = yn_1 * a;
		
	//adders
	always @ *
		y = {{3{y2[17]}}, y2[17:3]} + yn_1_times_a[34:17]; //9s9
	





endmodule
