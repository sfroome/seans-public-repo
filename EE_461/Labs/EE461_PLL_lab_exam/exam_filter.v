module exam_filter (
          input clk,
			 input signed [17:0] x, //1s17
			 output reg signed [17:0] y
			         );
						
reg signed [35:0] y1_times_a1, y2_times_a2;
wire signed [17:0] a1, a2;

reg signed [17:0] delay1, delay2;

always@ (posedge clk)
	delay1 <= y;
	
always@ (posedge clk)
	delay2 <= delay1;
		 
assign a1 = -18'sd32768;
assign a2 = -18'sd85197;

always @ *
y1_times_a1 = delay1 * a1;

always @ *
y2_times_a2 = delay2 * a2;

always @ *
y = {x[17], x[17], x[17:2]} + y1_times_a1[34:17] + y2_times_a2[34:17]; 

endmodule
