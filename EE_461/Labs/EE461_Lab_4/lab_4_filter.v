module lab_4_filter (
          input clk,
		   input signed [17:0] x_in, //1s17
			input [17:0]SW,
		   output reg signed [17:0] y  ); //2s16 or 1s17?
			
/*
reg signed [17:0] b0, b1, b2, b3, b4, b5, b6, b7,
                  b8, b9, b10, b11, b12, b13, b14, b15;
reg signed [17:0] x0, x1, x2, x3, x4, x5, x6, x7, x8, x9,
                  x10, x11, x12, x13, x14, x15, x16, x17,
						x18, x19, 
						X20, X21, X2, X2, X2, X2, X2, 
						X2, X2, X2, 
*/
integer i;	
reg signed [17:0]	b[15:0];	// Array of sixteen 18 bit numbers.
reg signed [17:0]	x[30:0];	
reg signed [35:0] mult_out[15:0]; // Array of sixteen 36 bit numbers.
reg signed [17:0] sum_level_1[15:0];
reg signed [17:0] sum_level_2[7:0];
reg signed [17:0] sum_level_3[3:0];
reg signed [17:0] sum_level_4[1:0];
reg signed [17:0] sum_level_5;


always @ (posedge clk)
x[0] = { x_in[17], x_in[17:1]}; // sign extend input(BUT WHY)
//Array index on LHS of equation, Bits on RHS of equation.
// Yes it's weird.




always @ (posedge clk)
begin
for(i=1; i<31;i=i+1) // so x[1] to x[30]
//ie: All but array index 0 is handled by this
// For Loop.
x[i] <= x[i-1]; // NON BLOCKING ASSIGNMENT <=
// So at each clk edge, x[i] takes the previous 
// array index's value.
// eg: x[1] = x[0]
end


always @ *
for(i=0;i<=14;i=i+1)
sum_level_1[i] = x[i]+x[30-i];

always @ *
sum_level_1[15] = x[15];


// always @ (posedge clk)
always @ *
for(i=0;i<=15; i=i+1)
mult_out[i] = sum_level_1[i] * b[i];


 always @ *
for(i=0;i<=7;i=i+1)
sum_level_2[i] = mult_out[2*i][34:17] + mult_out[2*i+1][34:17];



always @ *
for(i=0;i<=3;i=i+1)
sum_level_3[i] = sum_level_2[2*i] + sum_level_2[2*i+1];
			


always @ *
for(i=0;i<=1;i=i+1)
sum_level_4[i] = sum_level_3[2*i] + sum_level_3[2*i+1];


always @ *
sum_level_5 = sum_level_4[0] + sum_level_4[1];



always @ (posedge clk)
//y = $signed({sum_level_5[17],sum_level_5[17:1]}); //Because 2s16 vs 1s17???
y = sum_level_5; //1s17??

//y = {sum_level_5[17], sum_level_5[17], sum_level_5[17:2]};

// Case Statement for SW[4:2]						 
always @ *
	case(SW[4:2])
		3'H0:  		begin// HANN 
						b[0] =   18'sd      0; // 
						b[1] =  -18'sd	    33; //
						b[2] =  -18'sd     98; // 
						b[3] =   18'sd      0; // 
						b[4] =   18'sd    444; // 
						b[5] =  	18'sd   1043; // 
						b[6] =   18'sd   1133; // 
						b[7] =   18'sd      0; // 
						b[8] =  -18'sd   2328; // 
						b[9] =  -18'sd   4551; // 
						b[10] = -18'sd   4425; // 
						b[11] =  18'sd      0; // 
						b[12] =  18'sd   8895; // 
						b[13] =  18'sd  19959; // 
						b[14] =  18'sd  29179; // 
						b[15] =  18'sd  32768; // 
						end
		
		3'H1: 		begin // HANN KAISER
						b[0] =  -18'sd    196; // 
						b[1] =  -18'sd	   458; //
						b[2] =  -18'sd   	490; // 
						b[3] =   18'sd      0; // 
						b[4] =   18'sd    969; // 
						b[5] =  	18'sd   1841; // 
						b[6] =   18'sd   1715; // 
						b[7] =   18'sd      0; // 
						b[8] =  -18'sd   2887; // 
						b[9] =  -18'sd   5286; // 
						b[10] = -18'sd   4887; // 
						b[11] =  18'sd      0; // 
						b[12] =  18'sd   9199; // 
						b[13] =  18'sd  20253; // 
						b[14] =  18'sd  29285; // 
						b[15] =   18'sd  32768; // 
						end
										
		3'H2: 		begin	//HAMMING
						b[0] =  -18'sd    157; // 
						b[1] =  -18'sd	   268; //
						b[2] =  -18'sd   	272; // 
						b[3] =   18'sd      0; // 
						b[4] =   18'sd    623; // 
						b[5] =  	18'sd   1293; // 
						b[6] =   18'sd   1304; // 
						b[7] =   18'sd      0; // 
						b[8] =  -18'sd   2478; // 
						b[9] =  -18'sd   4743; // 
						b[10] = -18'sd   4543; // 
						b[11] =  18'sd      0; // 
						b[12] =  18'sd   8970; // 
						b[13] =  18'sd  20031; // 
						b[14] =  18'sd  29205; // 
						b[15]=   18'sd  32768; // 
						end
			
		3'H3:			begin//HAMMING KAISER
						b[0] =  -18'sd     82; // 
						b[1] =  -18'sd	   238; //
						b[2] =  -18'sd    293; // 
						b[3] =   18'sd      0; // 
						b[4] =   18'sd    699; // 
						b[5] =  	18'sd   1420; // 
						b[6] =   18'sd   1399; // 
						b[7] =   18'sd      0; // 
						b[8] =  -18'sd   2567; // 
						b[9] =  -18'sd   4856; // 
						b[10] = -18'sd   4612; // 
						b[11] =  18'sd      0; // 
						b[12] =  18'sd   9012; // 
						b[13] =  18'sd  20071; // 
						b[14] =  18'sd  29219; // 
						b[15]=   18'sd  32768; // 
						end
							
		3'H4: 		begin  //BLACKMAN
						b[0] =  -18'sd      0; // 
						b[1] =  -18'sd	    12; //
						b[2] =  -18'sd     38; // 
						b[3] =   18'sd      0; // 
						b[4] =   18'sd    207; // 
						b[5] =  	18'sd    542; // 
						b[6] =   18'sd    658; // 
						b[7] =   18'sd      0; // 
						b[8] =  -18'sd   1661; // 
						b[9] =  -18'sd   3545; // 
						b[10] = -18'sd   3717; // 
						b[11] =  18'sd      0; // 
						b[12] =  18'sd   8351; // 
						b[13] =  18'sd  19407; // 
						b[14] =  18'sd  28975; // 
						b[15]=   18'sd  32768; // 	
						end
									
		3'H5: 		begin	//BLACKMAN KAISER
						b[0] =  -18'sd     11; // 
						b[1] =  -18'sd	    57; //
						b[2] =  -18'sd     97; // 
						b[3] =   18'sd      0; // 
						b[4] =   18'sd    345; // 
						b[5] =  	18'sd    811; // 
						b[6] =   18'sd    901; // 
						b[7] =   18'sd      0; // 
						b[8] =  -18'sd   1992; // 
						b[9] =  -18'sd   4043; // 
						b[10] = -18'sd   4068; // 
						b[11] =  18'sd      0; // 
						b[12] =  18'sd   8621; // 
						b[13] =  18'sd  19682; // 
						b[14] =  18'sd  29077; // 
						b[15]=   18'sd  32768; // 		
						end
											
		3'H6: 		begin // RECTANGULAR
						b[0] =  -18'sd   1967; // 
						b[1] =  -18'sd	  2980; //
						b[2] =  -18'sd   2269; // 
						b[3] =   18'sd      0; // 
						b[4] =   18'sd   2682; // 
						b[5] =  	18'sd   4172; // 
						b[6] =   18'sd   3278; // 
						b[7] =   18'sd      0; // 
						b[8] =  -18'sd   4215; // 
						b[9] =  -18'sd   6954; // 
						b[10] = -18'sd   5900; // 
						b[11] =  18'sd      0; // 
						b[12] =  18'sd   9834; // 
						b[13] =  18'sd  20861; // 
						b[14] =  18'sd  29502; // 
						b[15]=   18'sd  32768; // 	
						end
						
		3'H7: 		begin // PRELAB DESIGN 
						b[0] =   18'sd 	  0; 	// First three digits are zero simply because it's 
						b[1] =	18'sd 	  0;	// a N = 25 filter.
						b[2] =	18'sd 	  0; 
						b[3] =   18'sd      0; 
						b[4] =  -18'sd	   405; //
						b[5] =   18'sd      0; // 
						b[6] =   18'sd   1226; // 
						b[7] =   18'sd   	  0; // 
						b[8] =  -18'sd   2813; // 
						b[9] =   18'sd      0; // 
						b[10] =  -18'sd   5781; // 
						b[11] =   18'sd      0; // 
						b[12] =  -18'sd  12227; // 
						b[13] =  18'sd      0; // 
						b[14] =  18'sd  41136; // 
						b[15] =  18'sd  65536; // 
					 //b[13] =  18'sd  37822; // Since it's a length 24 filter. These are irrelevant.
				  	 //b[14] =  18'sd  53488; // 
					 //b[15]=   18'sd  59411; // 	
						end 
	endcase
	   

/*always @ *
   begin

   end
*/

/* for debugging
always@ *
for (i=0; i<=15; i=i+1)
if (i==15) % center coefficient
b[i] = 18'sd 131071; % almost 1 i.e. 1-2^(17)
else b[i] =18'sd0; % other than center coefficient
*/

/* for debugging
always@ *
for (i=0; i<=15; i=i+1)
 b[i] =18'sd 8192; % value of 1/16
*/
endmodule	
	