module filter_2 (
	input clk,
	input signed [17:0] x,
	output reg signed [17:0] y 
	);
	
	reg signed [17:0] xn_1, xn_2; //1s17
	reg signed [35:0] xn_1_times_a; 
	reg signed [17:0] adder_out; //2s16
	
	//declare registers
	always @ (posedge clk)
		xn_1 = x;
	always @ (posedge clk)
		xn_2 = xn_1;

	/*
		For half scale input, the max value is 1.7071
		Therefore, the output needs format 2s16
	*/
		
	//declare multipliers
	wire signed [17:0] a = -18'sd92682; //2s16
	always @ *
		xn_1_times_a = xn_1 * a;
	
	
	//declare adders
	always @ *
		adder_out = xn_1_times_a[34:17] + {xn_2[17], xn_2[17:1]};
	
	
	//shape the output
	always @ *
		y = {x[17], x[17:1]} + adder_out; //2s16






endmodule
