module filter_3 (
          input clk,
			 input signed [17:0] x,
			 output reg signed [17:0] y
			         );					

//assuming output only depends on max of entire system 
//max output value = 0.5537 

	wire signed [17:0] y1; //3s15
	
	//first stage of filter is filter_1 unmodified
	filter_1 stage1 (
		.clk(clk),
		.x(x),
		.y(y1)
	);
	
	reg signed [17:0] xn_1, xn_2; //3s15
	reg signed [35:0] xn_1_times_a; 
	reg signed [17:0] adder_out; //1s17
	
	//declare registers
	always @ (posedge clk)
		xn_1 = y1;
	always @ (posedge clk)
		xn_2 = xn_1;

		
	//declare multipliers
	wire signed [17:0] a = -18'sd92682; //2s16
	always @ *
		xn_1_times_a = xn_1 * a; //5s31
	
	
	//declare adders
	always @ *
		adder_out = xn_1_times_a[31:14] + {xn_2[15:0], 2'b00}; //1s17
	
	
	//shape the output
	always @ *
		y = {y1[15:0], 2'b00} + adder_out; //1s17
	

endmodule

