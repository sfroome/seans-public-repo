/*****************************************************
This 'should' create a system function with the following H(z)

h[n] = u[n] -u[n-N]
******************************************************/

module modified_system_under_test3 (
input clk, 
input signed [11:0] SUT_in, //1s11 in
output reg signed [17:0] SUT_out); // 1s17 out


//Declare Registers
reg signed [14:0] SUT_in_ext; // 4s11 SUT_in
reg signed [14:0] SUT_out_trunc; // 1s14 SUT_out before extending

// Declare The Chain of Registers
reg signed[14:0] h1,h2,h3,h4,h5,h6,h7; // This is going to be hell.

//Declare Adder Outputs
reg signed [14:0] h7_plus_SUT_in_ext,h1_plus_SUT_in_ext;



// The Registers.
always @ (posedge clk)
//h1 = SUT_in_ext;
h1 = h1_plus_SUT_in_ext;
always @ (posedge clk)
h2 = h1;
always @ (posedge clk)
h3 = h2;
always @ (posedge clk)
h4 = h3;
always @ (posedge clk)
h5 = h4; 
always @ (posedge clk)
h6 = h5;
always @ (posedge clk)
h7 = h6;

// Left Side Adder
always @ *
h1_plus_SUT_in_ext = h1 + SUT_in_ext; //SIGN EXTENDED SUT_IN YOU DUMBASS

// Right Side Adder
always @ *
h7_plus_SUT_in_ext = h1_plus_SUT_in_ext - h7; // really it's sut_in minus h7 but hey!

//Helps if I actually declare the value of SUT_out_trunc huh?
always @ *
SUT_out_trunc = h7_plus_SUT_in_ext;

// Sign Extend SUT_in
always @ *
SUT_in_ext = {SUT_in[11], SUT_in[11], SUT_in[11], SUT_in};


//Pad  SUT_out with zeroes
always @ *
SUT_out = {SUT_out_trunc, 3'b0}; // 14 bit to 17 bit ie: 1s14 to 1s17

endmodule
