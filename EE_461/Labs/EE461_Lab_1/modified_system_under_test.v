/*****************************************************
This 'should' create a system function with the following H(z)

H(z) = 0.5+0.5*z^(-1)

******************************************************/
module modified_system_under_test (
input clk, 
input signed [11:0] SUT_in, //1s11
output reg signed [17:0] SUT_out); //1s17


// Declaring constants
wire signed [11:0] a; // 0.5 (Way to leave this as a 24bit number)
wire signed [23:0] a1;

//Declaring Registers
reg signed [11:0] x1;
reg signed [23:0] multiplier_adder_out; // 2s22

assign a = 12'sd1024; // (aka 2^11)/2 since half of SUT_in would be 1024.
//assign a1 = 24'sd2097152; // aka 0.5*2^22

always @ (posedge clk)
x1 = SUT_in;

always @ *
multiplier_adder_out = a*SUT_in + a*x1; // This would be a 2s22 number due to having 
//multiplier_adder_out = a1 + a*x1;


always @ *
SUT_out = {multiplier_adder_out[22:5]};//], multiplier_adder_out[22:6]}; // trimmed from 2s22 to 1s17
// This gives 65504, which is 
// 1/2 of the max which would be 2^17-1
// Ie: (2^17-1)/2 = 65536



endmodule

// FOR A 1s11 NUMBER, 2047 IS INDEED CORRECT AND THIS MAKES SENSE AS 2^11 -1 is 2047
// FOR A 1s17 NUMBER the max aka max value for a signed 18bit
//would be 131071. We want half that or 65536.







