module system_under_test_delay (
input clk, 
input signed [11:0] SUT_in,
output reg signed [17:0] SUT_out);

reg signed [11:0] delay1, delay2;//, delay3;
//Pretty sure since I do not need the third delay.
// Signal tap would seem to agree seeing as you
//Need a register for the output.

always @ (posedge clk)
delay1 = SUT_in;
always @ (posedge clk)
delay2 = delay1;
always @ (posedge clk)
//delay3 = delay2;
//always @ (posedge clk) 
SUT_out = {delay2,6'b0}; // because 1s17 vs 1s11
endmodule
