module impulse_response_meas(
			input CLOCK_50,
			input [17:0]SW,
			input [3:0] KEY,
			//input [13:0]ADC_DA,
			//input [13:0]ADC_DB,
			output reg [3:0] LEDG,
			output reg [17:0] LEDR,
			output reg[13:0]DAC_DA,
			output reg [13:0]DAC_DB,
			// output	I2C_SCLK,
			// input		I2C_SDAT,
			//output	ADC_CLK_A,
			//output	ADC_CLK_B,
			//output	ADC_OEB_A,
			//output	ADC_OEB_B,
			// input 	ADC_OTR_A,
			// input 	ADC_OTR_B,
			output	DAC_CLK_A,
			output	DAC_CLK_B,
			output	DAC_MODE,
			output	DAC_WRT_A,
			output	DAC_WRT_B
			// ,input 	OSC_SMA_ADC4,
			// input 	SMA_DAC4
			 );
	reg clk;
	reg [11:0] NCO_freq, phase_ref,
			           quadrature_phase_out, phase_to_ROM;
	(* noprune *) reg [11:0] phase_out, staggered_phase_out;
	reg signed [13:0] signal_to_DAC;
	reg signed [29:0] multiplier_output;
	reg [35:0] PLL_acc, quad_phase;
		
	wire dwell_pulse;
	wire [17:0] sweep_gen_freq;
	wire signed [17:0] SUT_out;
	wire signed [11:0] NCO_1_out, NCO_2_out;
	/******************************
	*	generate the clock used for sampling ADC and
	*  driving the DAC, i.e. generate the sampling clock
	*/
	always @ (posedge CLOCK_50)
	clk = ~clk;
	// end generatating sampling clock


	/**********************************************************
	The Following is Sean's Code. Expect errors.
	***********************************************************
	Instantiate Sean's Register
	**********************************************************/
	(* noprune *) reg signed [11:0] periodic_impulse;
	(* noprune *) reg signed [17:0]	impulse_response;
	(* noprune *) reg unsigned[6:0] counter;
	reg unsigned counter_eq_zero;
	
	
	/**********************************************************
	Set up 7bit Counter 
	*********************************************************/

		always @ (posedge clk)
		if(counter <= 7'b111_1111)
			counter = counter + 7'b1;
		else
			counter = 7'b0;

	always @ *
		if (counter == 7'b111_1111)// || 7'd0 || 7'd1 || 7'd2 || 7'd3 || 7'd4 || 7'd5 || 7'd6)
			counter_eq_zero = 1'b1;
		else
			counter_eq_zero = 1'b0;		
	
	/****************************************
	Set up the logic for SUT_in
	****************************************/
	
	reg signed [11:0] SUT_in;
	always @ *
		if(counter_eq_zero == 1'b1)
			SUT_in = 12'sb0111_1111_1111;
		else
			SUT_in = 12'sb0;
	/****************************************
	Plop SUT_in into the modified SUT
	*****************************************/
	wire [1:0] case_select; 
	assign case_select = SW[4:3];
	//modified_system_under_test SUT_1
	//modified_system_under_test2 SUT_1
	//modified_system_under_test3 SUT_1
	//system_under_test_delay SUT_1
	//approx_brick_wall_filt SUT_1
	//approx_brick_wall_filt2 SUT_1
	Lab_2_filters SUT_1
		(.clk(clk),
		.x_in({SUT_in,6'b0}),
		.case_select(case_select),
		.y(SUT_out)); // These lines are for the brick wall filter(Which aren't working because fuck you quartus)
		//.SUT_in(SUT_in),
		//.SUT_out(SUT_out));		 
		
	/*****************************************
	Setup periodic_impulse and impulse_response
	******************************************/
	
	always @ (posedge clk)
		periodic_impulse = SUT_in;
	
	always @ (posedge clk)
//		impulse_response = SUT_out; 
		impulse_response = {SUT_out[17], SUT_out[17:1]}; // For Brick Wall Filter
																			// Because y is 2s16		
	/******************************************
	Modify signal_to_DAC so it equals SUT_out
	(Very easy modification)
	(Once you need to measure the phase response this becomes useless)
	******************************************/
	//always @ *
	//	signal_to_DAC = SUT_out[17:4];
								 
	/******[****************************
	End of code written by Sean
	************************************/
	
	/***********************************
			  Set up DACs
	*****************************/
			assign DAC_CLK_A = ~clk;
			assign DAC_CLK_B = ~clk;
			
			
			assign DAC_MODE = 1'b1; //treat DACs seperately
			
			assign DAC_WRT_A = ~clk;
			assign DAC_WRT_B = ~clk;
			
		always@ (posedge clk)// convert 1s13 format to 0u14
								//format and send it to DAC A
		DAC_DA = {~signal_to_DAC[13],
						signal_to_DAC[12:0]};		
		
				
		always@ (posedge clk) // DAC B is not used in this
					 // lab so makes it the same as DAC A
				 
			DAC_DB = {~signal_to_DAC[13],
						signal_to_DAC[12:0]} ;	
			/*  End DAC setup
			************************/
	
	/********************
	*	set up ADCs, which are not used in this lab 
	*/
	// Then why set them up if they're not used? - Sean
	
	//(* noprune *) reg [13:0] registered_ADC_A;
	//(* noprune *) reg [13:0] registered_ADC_B;
	
	//assign ADC_CLK_A = clk;
	//		assign ADC_CLK_B = clk;
			
	//		assign ADC_OEB_A = 1'b1;
	//		assign ADC_OEB_B = 1'b1;

			
	//		always@ (posedge clk)
	//			registered_ADC_A <= ADC_DA;
				
	//		always@ (posedge clk)
	//			registered_ADC_B <= ADC_DB;
				
			/*  End ADC setup
			************************/	
	
	
	/******************************
			Set up switches and LEDs
			*/
			always @ *
			LEDR = SW;
			always @ *
			LEDG = {dwell_pulse, KEY[2:0]};
	//		assign a = 24'sd2097152; // Should end up being a 2s22 number aka 0.5*2^22
	//end setting up switches and LEDs

	// instantiate the sweep generator	
		SweepGenerator sweep_gen_1(.clock(clk),
							  .sweep_rate_adj(3'b111),
							  .dwell_pulse(dwell_pulse),
							  .reset(~KEY[1]),
							  .lock_stop(~KEY[2]),
							  .lock_start(~KEY[3]),
							  .freq(sweep_gen_freq));
	// dwell pulse is assigned to LEDG[3] elsewhere so that it is not optimized out
	
							  
	//  make the phase accumulator for the NCOs 
	
	always @ (posedge clk)
	phase_ref = phase_ref + NCO_freq;
	
	
	
	// make data selector for phase_ref
	always @ (posedge clk)
	if (SW[0]==1'b0)
			NCO_freq = sweep_gen_freq[17:6];
	else  NCO_freq = {SW[17:14], 8'b0};

	// make the data selector for signal_to_DAC
	always @ *
	if (SW[1]==1'b0)
		signal_to_DAC = {NCO_1_out, 2'b0};
	else
	   signal_to_DAC = SUT_out[17:4];
		
	// make the phase locked loop
	always @ *
	multiplier_output = NCO_2_out * SUT_out;	

	always @ *
	quad_phase = { { 6{multiplier_output[29]} }, multiplier_output}
					  + PLL_acc;
	
	always @ (posedge clk)
	PLL_acc = quad_phase;
	
	always @ *
	quadrature_phase_out = quad_phase[35:24];
	
	always @ *
	phase_to_ROM = phase_ref + quadrature_phase_out;
	
	// end of making PLL
	
	// make the phase out, which does not to a pin
	// as its intendended destination is SignalTap
	always @ (posedge clk)
	phase_out = quadrature_phase_out + 12'b1100_0000_0000;
	
	always @ (negedge clk)
	staggered_phase_out = phase_out;
	
	// intantiate the phase-to-voltage ROM 
	// for NCOs 1 and 2
	ROM_for_12_x_12_NCO NCO_ROM_1 (
		  .address_a(phase_ref),
		  .address_b(phase_to_ROM),
		  .clock(clk),
		  .q_a(NCO_1_out),
		  .q_b(NCO_2_out));	
	
endmodule	