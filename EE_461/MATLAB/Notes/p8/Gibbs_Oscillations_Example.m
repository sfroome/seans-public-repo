clear all
% Why you can't just use a Rectangular filter for everything

% For M = 20;
fc = .2; %Cutoff frequency of LPF
M = 20;
n = 0:M;
h = 2*fc*sinc(2*fc*(n-M/2)); % Zee impulse response
Nf = 1024; %
f = [-Nf/2:Nf/2-1]/Nf; % Frequency range of interest
H = fftshift(fft(h,Nf)); 
A = H.*exp(j*2*pi*f*M/2); % H times the inverse of the phase gives A.
A20 = real(A);

% For M = 40;
M = 40;
n = 0:M;
h = 2*fc*sinc(2*fc*(n-M/2)); % Zee impulse response
Nf = 1024; %
f = [-Nf/2:Nf/2-1]/Nf; % Frequency range of interest
H = fftshift(fft(h,Nf)); 
A = H.*exp(j*2*pi*f*M/2); % H times the inverse of the phase gives A.
A40 = real(A);

% For M = 80;
M = 80;
n = 0:M;
h = 2*fc*sinc(2*fc*(n-M/2)); % Zee impulse response
Nf = 1024; %
f = [-Nf/2:Nf/2-1]/Nf; % Frequency range of interest
H = fftshift(fft(h,Nf)); 
A = H.*exp(j*2*pi*f*M/2); % H times the inverse of the phase gives A.
A80 = real(A);


% For M = 160;
M = 160;
n = 0:M;
h = 2*fc*sinc(2*fc*(n-M/2)); % Zee impulse response
Nf = 1024; %
f = [-Nf/2:Nf/2-1]/Nf; % Frequency range of interest
H = fftshift(fft(h,Nf)); 
A = H.*exp(j*2*pi*f*M/2); % H times the inverse of the phase gives A.
A160 = real(A);


figure(1)
clf
subplot(2,2,1)
plot(f,A20)
title('A(e^{j\omega}), M = 20')
grid
subplot(2,2,2)
plot(f,A40)
title('A(e^{j\omega}), M = 40')
grid
subplot(2,2,3)
plot(f,A80)
title('A(e^{j\omega}), M = 80')
xlabel('f (cycles/sample)')
grid
subplot(2,2,4)
plot(f,A160)
title('A(e^{j\omega}), M = 160')
xlabel('f (cycles/sample)')
grid

figure(2)
clf
M = 20;
n = 0:M;
h = 2*fc*sinc(2*fc*(n-M/2)); % Zee impulse response
Nf = 1024; %
f = [-Nf/2:Nf/2-1]/Nf; % Frequency range of interest
H = fftshift(fft(h,Nf)); 
A = H.*exp(j*2*pi*f*M/2); % H times the inverse of the phase gives A.
A = real(A);
plot(f,10*log10(A.^2))
title(['10log(|A(e^{j\omega})|^2), Filter Order = ',num2str(M)])
xlabel('f (cycles/sample)')
ylabel('dB')
grid


