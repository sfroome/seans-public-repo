clear all

fc = 0.2;
%M = 20;
M = 10;
nM = -M:M;

hideal = 2*fc*sinc(2*fc*(nM));
figure(1)
clf
subplot(3,1,1)
stem(nM,hideal)
title('h_{ideal}[n]')
grid
subplot(3,1,2)
%w=[zeros(1,10) ones(1,M+1) zeros(1,10)];
w=[zeros(1,5) ones(1,M+1) zeros(1,5)];
nw = [-5:15];
%nw = [-10:30];
stem(nw,w)
title('w[n]')
grid
subplot(3,1,3)
n=0:M;
h = 2*fc*sinc(2*fc*(n-M/2));
%hc = [zeros(1,10) h zeros(1,10)];
hc = [zeros(1,5) h zeros(1,5)];
%nS = -10:30;
nS = -5:15;
stem(nS,hc)
title('h[n]')
xlabel('n (samples)')
grid

figure(2)
clf
fs = -.5:0.001:.5;
Aideal = [zeros(1,300) ones(1,401) zeros(1,300)];
subplot(3,1,1)
plot(fs,Aideal)
title('A_{ideal}(e^{j\omega})')
axis([-.5,.5,0,1.2])
omega=-1:.01:1;
y=diric(omega*pi,21);
grid
subplot(3,1,2)
plot(omega/2,y*(M+1))
title('W(e^{j\omega})')
grid
subplot(3,1,3)
n=0:M;
h=2*fc*sinc(2*fc*(n-M/2));
Nf=1024;
f = [-Nf/2:Nf/2-1]/Nf;
H=fftshift(fft(h,Nf));
A=H.*exp(j*2*pi*f*M/2);
plot(f,real(A))
title('A(e^{j\omega})')
grid
xlabel('f (cycles/sample)')
