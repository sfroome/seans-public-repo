M = 2;
freq = [0 0.42 0.48 1];
mag = [0 1 0 0 ];
x = firpm(101, freq, mag);
y = downsample(x,M);

figure(1), clf
subplot(2,1,1)
stem([0:length(x)-1],x), grid
xlabel('n (samples)');
title('Input Sequence')
subplot(2,1,2)
stem([0:length(y)-1],y),grid
xlabel('n (samples)');
title(['Output Signal down-sampled by ', num2str(M)]);

figure(2), clf
subplot(2,1,1)
nstart = 40; nstop = 60;
stem([nstart:nstop],x(nstart+1:nstop+1)),grid
xlabel('n(samples)');
title('Zoomed-In Input Sequence')
subplot(2,1,2)
stem([nstart/M:nstop/M],y(nstart/M+1:nstop/M+1)),grid
xlabel('n (samples)')
title(['Zoomed-In Output Signal Down-sampled by ',num2str(M)]);

%Frequency Domain Party
figure(3), clf
[Xz, w] = freqz(x,1,512);
subplot(2,1,1)
plot(w/(2*pi),abs(Xz)); grid
xlabel('f (cycles/sample)');ylabel('Magnitude');
title('Input Spectrum')
[Yz,w] = freqz(y,1,512);
subplot(2,1,2)
plot(w/(2*pi),abs(Yz)); grid
xlabel('f (cycles/sample)'); ylabel('Magnitude');
title(['Output spectrum down-sampled by ', num2str(M)]);

y3 = downsample(x,3);
figure(4), clf % Aliasing
[Xz, w] = freqz(x, 1, 512);
subplot(2,1,1)
plot(w/(2*pi), abs(Xz)); grid
xlabel('f (cycles/sample)'); ylabel('Magnitude');
title('Input Spectrum');
[Y3z, w] = freqz(y3,1,512);
subplot(2,1,2)
plot(w/(2*pi),abs(Y3z)); grid
xlabel('f (cycles/samples)'); ylabel('Magnitude');
title(['Output spectrum (with aliasing) down-sampled by ', num2str(3)]);

figure(5), clf % Zoomed out Aliasing
[Xz, w] = freqz(x, 1, [0:.01:3*pi]);
subplot(2,1,1)
plot(w/(2*pi), abs(Xz)); grid
xlabel('f (cycles/samples)'); ylabel('Magnitude');
title('Input spectrum (zoomed out)');
[Y3z, w] = freqz(y3,1,[0:.01:3*pi]);
subplot(2,1,2)
plot(w/(2*pi),abs(Y3z)); grid
xlabel('f (cycles/sample)'); ylabel('Magnitude');
title(['Output spectrum (with aliasing) down-sampled ', num2str(3)]);