clear all

L = 3;

fh = 0.25;
fhM = 2*fh;
freq = [0 0.45 fhM 1];
mag = [0 1 0 0];
N = 100;
x = firpm(N-1,freq,mag);

xu = upsample(x,L);
figure(1), clf

subplot(2,1,1)
stem([0:length(x)-1],x), grid
title('Input Sequence')
xlabel('n (samples)');
subplot(2,1,2)
stem([0:length(xu)-1],xu), grid
xlabel('n (samples)');
title(['Input Signal Upsampled by', num2str(L)]);

figure(2), clf %Zoomed In
subplot(2,1,1)
nstart = .4*N; nstop=.6*N;
stem([nstart:nstop],x(nstart+1:nstop+1)),grid
xlabel('n (samples)');
title('Zoomed-In Input Sequence')
subplot(2,1,2)
stem([nstart*L:nstop*L],xu(nstart*L+1:nstop*L+1)), grid
xlabel('n (samples)');
title(['Zoomed-In Input Signal Upsampled by ', num2str(L)]);

[Xz, w] = freqz(x, 1);
figure(3)
subplot(2,1,1)
plot(w/(2*pi), abs(Xz)); grid
xlabel('f (cycles/sample)'); ylabel('Magnitude');
title('Input spectrum');
[Xuz, w] = freqz(xu, 1);
subplot(2,1,2)
plot(w/(2*pi), abs(Xuz)); grid
xlabel('f (cycles/sample)'); ylabel('Magnitude');
title(['Input spectrum up-sampled by ', num2str(L)]);

%Interpolation Filter
fp = fh/L;
fs = 0.5/L;
M = firpmord([fp fs]*2,[1 0], [0.0001 .0001]);
if M <= 3 % order cannot be less than 4
    M = 4;
elseif abs(floor(M/2)-M/2) > 0 % ORDER BE EVEN
    M = M+1;
end

display(['Filter order (M) = ', num2str(M)])
b = firpm(M,[0,fp,fs,0.5]*2,[L L 0 0]); % Filter have gain of L


%FREQUENCY RESPONSE
figure(4),clf
[H,w] = freqz(b,1,[0:.01:2*pi]);
plot(w/(2*pi),abs(H))
title(['Filter Frequency Response (L = ',num2str(L),')'])
ylabel('|H(e^{j\omega})|')
xlabel('f (cycles/sample)')
figure(5), clf
plot(w/(2*pi),20*log10(abs(H)))
title(['Filter Frequency Response (L=',num2str(L),')'])
ylabel('20log(H(e^{j\omega})|)')
xlabel('f (cycles/sample)')


xuz = [xu, zeros(1,M/2)];
y = filter(b,1,xuz);
y = y(M/2+1:end);

figure(6), clf %Spectrum of output y
[Yz,w] = freqz(y,1);
plot(w/(2*pi),abs(Yz)),grid
title(['Spectrum of Output Sequence (L = ', num2str(L),')'])
xlabel('f (cycles/sample)')

figure(7), clf % INTERPOLATED SEQUENCE
subplot(2,1,1)
stem([0:length(x)-1],x), grid
title('Input Sequence');
xlabel('Time Index n'); ylabel('Amplitude')
subplot(2,1,2)
stem([0:length(y)-1],y), grid
title(['Output Sequence interpolated by ', num2str(L)]);
xlabel('Time Index n'); ylabel('Amplitude');

figure(8), clf
%Zoomed in input and output
subplot(2,1,1)
nstart = .4*N; nstop=.6*N;
stem([nstart:nstop],x(nstart+1:nstop+1)), grid
xlabel('n (sample)');
title('Zoomed-In Input Sequence');
subplot(2,1,2)
stem([nstart*L:nstop*L],y(nstart*L+1:nstop*L+1)), grid
xlabel('n(samples');
title(['Zoomed-In Output Sequence Interpolated by ',num2str(L)]);
