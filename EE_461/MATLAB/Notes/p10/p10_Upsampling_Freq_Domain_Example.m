L = 3;

freq = [0 0.45 0.5 1];
mag = [0 1 0 0 ];
x = firpm(99, freq, mag);

y = upsample(x,L);

figure(1), clf
subplot(2,1,1)
stem(0:length(x)-1,x), grid
xlabel('n (samples)');
title('Input Sequence')
subplot(2,1,2)
stem([0:length(y)-1],y),grid
xlabel('n (samples)');
title(['Output Signal Upsampled by ', num2str(L)]);

figure(2), clf
subplot(2,1,1)
nstart = 40; nstop = 60;
stem([nstart:nstop],x(nstart+1:nstop+1)),grid
xlabel('n(samples)');
title('Zoomed-In Input Sequence')
subplot(2,1,2)
stem([nstart*L:nstop*L],y(nstart*L+1:nstop*L+1)),grid
xlabel('n (samples)')
title(['Zoomed-In Output Signal Upsampled by ',num2str(L)]);

%Frequency Domain Party
[Xz, w] = freqz(x,1,512);
figure(3)
subplot(2,1,1)
plot(w/(2*pi),abs(Xz)); grid
xlabel('f (cycles/sample)');ylabel('Magnitude');
title('Input Spectrum')
[Yz,w] = freqz(y,1,512);
subplot(2,1,2)
plot(w/(2*pi),abs(Yz)); grid
xlabel('f (cycles/sample)'); ylabel('Magnitude');
title(['Output spectrum up-sampled by ', num2str(L)]);