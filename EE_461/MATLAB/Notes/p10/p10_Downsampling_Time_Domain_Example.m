clear all
clf


figure(1)
N1 = 50;
f = 1/35;
M = 3;
n = 0:N1 -1;
m = 0:N1*M-1;
x = sin(2*pi*f*m);
y = downsample(x,M);
subplot(2,1,1)
stem(n,x(1:N1));
title('Input')
xlabel('n (samples)');
ylabel('Amplitude');
subplot(2,1,2)
stem(n,y);
title(['Output down-sampled by ',num2str(M)]);
xlabel('n(samples)');ylabel('Amplitude');

% The number of samples has in one period has decreased, 
%but the actual frequency of the sinusoid has increased