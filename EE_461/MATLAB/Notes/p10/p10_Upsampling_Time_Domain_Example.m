clear all
clf
figure(1)



N1 = 50;
f = 3/35;
L = 3;
n = 0:N1 -1;
x = sin(2*pi*f*n);
y = upsample(x,L);
subplot(2,1,1)
stem(n,x);
title('Input')
xlabel('n (samples)');
ylabel('Amplitude');
subplot(2,1,2)
stem(n,y(1:length(x)));
title(['Output up-sampled by ',num2str(L)]);
xlabel('n(samples)');ylabel('Amplitude');

% The number of samples has increased, however, this really just means that
% most of the samples are zero.