clear all
M = 2;
fh = .24;
fhM = 2*fh; 
freq = [0 0.42 fhM 1];
mag = [0 1 0 0];
N = 100;
x = firpm(N-1, freq, mag);

fp = fh;
fs = 0.5/M;
Mo = firpmord([fp fs]*2, [1 0], [0.0001 .0001]); 
if Mo <=3
    Mo=4;
elseif abs(floor(Mo/2)-Mo/2) >0 
    Mo = Mo+1;
end
display(['Filter order (Mo) = ', num2str(Mo)])
b = firpm(Mo,[0,fp,fs,0.5]*2,[1 1 0 0]);


figure(1), clf
[H,w]=freqz(b,1,[0:.01:2*pi]);
plot(w/(2*pi),abs(H))
title(['Filter Frequency Response (M =',num2str(M),')'])
ylabel('|H(e^{j\omega})|')
xlabel('f (cycles/sample)')
figure(2), clf
plot(w/(2*pi), 20*log10(abs(H)))
title(['Filter Frequency Response (M = ', num2str(M),')'])
ylabel('20log|H(e^{j\omega})|')
xlabel('f (cycles/sample)')

xz = [x,zeros(1,Mo/2)];
xf = filter(b,1,xz);
xf=xf(Mo/2+1:end);

y = downsample(xf,M);

figure(3), clf
subplot(2,1,1)
stem([0:length(x)-1],x), grid
xlabel('n (samples)');
title('Input Sequence')
subplot(2,1,2)
stem([0:length(y)-1],y), grid
xlabel('n (samples)');
title(['Input Signal Decimated by ', num2str(M)]);


figure(4), clf
subplot(2,1,1)
nstart = 0.4*N; nstop = 0.6*N;
stem([nstart:nstop], x(nstart+1:nstop+1)), grid
xlabel('n (samples');
title('Zoomed-In Input Sequence')
subplot(2,1,2)
stem([nstart/M:nstop/M],y(nstart/M+1:nstop/M+1)), grid
xlabel('n (samples)');
title(['Zoomed-In Output Sequence Decimated by ', num2str(M)]);


figure(5), clf
[Xz,w] = freqz(x,1,512);
subplot(2,1,1)
plot(w/(2*pi), abs(Xz)); grid
xlabel('f (cycles/sample)'); ylabel('Magnitude');
title('Input Spectrum');
[Yz,w]= freqz(y,1,512);
subplot(2,1,2)
plot(w/(2*pi), abs(Yz)); grid
xlabel('f (cycles/sample)'); ylabel('Magnitude');
title(['Output Spectrum decimated by ', num2str(M)]);
