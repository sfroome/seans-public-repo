deltap = 0.1;
deltas = 0.01;
wp = 0.2*pi;
ws = 0.3*pi;

% Least Squares Design
M = 32;
f = [0 wp ws pi]/(pi);
a = [1 1 0 0];
wght = [deltap^-1 deltas^-1];
wght = [1 deltap/deltas];
b = firls(M,f,a,wght);
[H,w] = freqz(b,1,[0:.01:pi]);


%Kaiser
wc = (0.2*pi+0.3*pi)/2;
A = -20*log10(min(deltas,deltap))
beta = 0.5842*(A-21)^.4 + 0.07886*(A-21); %21<=A<=50
Mk = (A-8)/(2.285*(ws-wp))
Mk = ceil(Mk); % M = 45
bk = fir1(Mk,wc/pi, 'low',kaiser(Mk+1,beta)); % wc is divided by pi
[Hk,w] = freqz(bk,1,[0:0.01:pi]);

% Magnitude Response
figure(1)
clf
plot(w/(2*pi),20*log10(abs(H)))
hold
plot(w/(2*pi),20*log10(abs(Hk)))
grid
legend('Least Squares','Kaiser')
xlabel('f (cycles/sample)')
ylabel('20log(|H(e^{j/omega})|)')
title('Filter Design Comparison: firls with Kaiser')


%Passband Ripple
figure(2)
clf
W = exp(-j*((-M/2)*w));
A = H.*W;
A = real(A);
plot(w,A)
grid
hold
Wk = exp(-j*((-Mk/2)*w));
Ak = Hk.*Wk;
Ak = real(Ak);
plot(w,Ak)
plot([0,wp],[1+deltap,1+deltap],'-k')
plot([0,wp],[1-deltap,1-deltap],'-k')
axis([0,ws,1-2*deltap,1+2*deltap])
legend('Least Squares','Kaiser')
title('Passband Ripple Comparison: firls with kaiser')
ylabel('A(e^{j\omega}')
xlabel('\omega(radians/sample)')


%Stopband Ripple
figure(3)
clf
plot(w,A)
grid
hold
plot(w,Ak)
plot([ws,pi],[deltas,deltas],'-k')
plot([ws,pi],[-deltas,-deltas],'-k')
axis([wp,pi,-2*deltas,2*deltas])
legend('Least Squares','Kaiser')
title('Stopband Ripple Comparison: firls with kaiser')
ylabel('A(e^{j\omega}')
xlabel('\omega(radians/sample)')


