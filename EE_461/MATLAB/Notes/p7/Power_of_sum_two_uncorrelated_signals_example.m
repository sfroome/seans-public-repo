clear All

Nd = 1000;
%Nd = 1000000;
x1 = rand(1,Nd)-.5; % zero mean signal
x2 = rand(1,Nd)-.5; % zero mean signal

%Check Signals have zero mean
x1mean = sum(x1)/Nd;
x2mean = sum(x2)/Nd;

mean = [x1mean; x2mean]
%pause

Pac_x1 = sum(x1.^2)/Nd;
Pac_x2 = sum(x2.^2)/Nd;

x = x1+x2;
Pac_x =  sum(x.^2)/Nd;
Pac_x_sum = Pac_x1 + Pac_x2;

Power_AC = [Pac_x ; Pac_x_sum]