clear all

N = 128;
Nd = 50*N;
n = 0:Nd-1;
f1 = 1/N;
x1 = cos(2*pi*(1/N)*n);; % zero mean signal
x2 = cos(2*pi*(1/N)*n + 2*pi/N);; % zero mean signal

%Check Signals have zero mean
x1mean = sum(x1)/Nd;
x2mean = sum(x2)/Nd;
mean = [x1mean; x2mean]
%pause

Pac_x1 = sum(x1.^2)/Nd;
Pac_x2 = sum(x2.^2)/Nd;

x = x1+x2;
Pac_x =  sum(x.^2)/Nd;
Pac_x_sum = Pac_x1 + Pac_x2;

Power_AC = [Pac_x ; Pac_x_sum]

Pac_cross = sum(2*x1.*x2)/Nd;
Power_AC_cross = [Pac_x;Pac_x_sum + Pac_cross]