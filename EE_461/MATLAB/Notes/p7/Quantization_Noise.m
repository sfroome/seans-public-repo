clear all

N = 32; %Sinusoid Period
n = 0:2*N-1;% two periods

x = cos(2*pi*n/N);
xt = floor(x*2^3)/2^3;
%xt = round(x*2^3)/2^3;
figure(1)
clf
subplot(2,1,1)
stem(n,x)
ylabel('Original Signal')
grid
subplot(2,1,2)
stem(n,xt)
ylabel('Quantized Signal')
xlabel('n (samples)')
grid

q = x-xt;
figure(2)
clf
stem(n,q)
title('Quantization Error')
xlabel('n (samples)')
grid

qf = fft(q)
figure(3)
clf
stem(n, abs(qf));
title('Spectral Content of the Quantization Error')
xlabel('frequency (x 1/64 cycles/sample)')
grid