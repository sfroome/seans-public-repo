% Comparing Statistical Models. 
%Quantization Example

xr = rand(1,1000000); % Random number
x = round(xr*2^10)*2^-10; % 1s10
xt = floor(x*2^8)*2^-8; % Truncate to 1s8.
q= x-xt; % Generate the Quantization Noise.

s = 2^-8;

figure(1)
clf
stem(q(1:100)/s)
title(['[q[n] (Actual System), mean = s*',num2str(mean(q)/s), ...
    ' variance = s^2*', num2str(var(q)/s^2)])
figure(2)
hist(q/s,100)
title(['p(A) (Actual System), mean = s*', num2str(mean(q)/s), ...
    ', variance = s^2*', num2str(var(q)/s^2)])
