n = 0:1/16:1;
w = 2*pi*n;
H = 0.5 +0.5*exp(i*-w);

figure (1)
%plot(w/(2*pi), 20*log10(abs(H)/1e-3));
plot(w,20*log10(abs(H)));
grid on
xlabel('n/pi samples');
ylabel('20log|H|');
title('Magnitude Response of H = 0.5 +0.5/z');

figure (2)
%plot(w/(2*pi), phase(H));
plot(w,phase(H));


figure (3)
%plot(w/(2*pi),exp(-i*w/2).*cos(w/2))

%figure(4)
freqz(H)