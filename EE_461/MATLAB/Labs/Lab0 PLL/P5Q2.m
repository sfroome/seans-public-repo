%Values taken from the Spectrum Analyzer 
%to verify the Formula given in the Lab Manual

f1 = 2.296e6;
f2 = 3.0220e6;
f3 = 4.519e6;
f4 = 5.631e6;
f5 = 6.680e6;

% Values in dBm (kD at 1/128)
P1 = -37.586;
P2 = -40.605; 
P3 = -43.332;
P4 = -45.267;
P5 = -46.630;

A1 = 10^(P1/10);
A2 = 10^(P2/10);
A3 = 10^(P3/10);
A4 = 10^(P4/10);
A5 = 10^(P5/10);

A1A = 2 * sqrt(A1)*2^(16);
A2A = 2 * sqrt(A2)*2^(16);
A3A = 2 * sqrt(A3)*2^(16);
A4A = 2 * sqrt(A4)*2^(16);
A5A = 2 * sqrt(A5)*2^(16);

x1=(pi*2^-7)/(2*abs(sin(pi*f1)))/2^16;
x2=(pi*2^-7)/(2*abs(sin(pi*f2)))/2^16;
x3=(pi*2^-7)/(2*abs(sin(pi*f3)))/2^16;
x4=(pi*2^-7)/(2*abs(sin(pi*f4)))/2^16;
x5=(pi*2^-7)/(2*abs(sin(pi*f5)))/2^16;

f0 = 3.126e6;
x0 = (pi*2^-7)/(2*abs(sin(pi*f0)))/2^16;
P0 = -40.536;
A0 = 10^(P0/10);
A0A = 2 * sqrt(A0)*2^(16);

f00 = 1.5630e6;
x00 = (pi*2^-7)/(2*abs(sin(pi*f00)))/2^16;
P00 = -34.475;
A00 = 10^(P00/10);
A00A = 2 * sqrt(A00)*2^(16);

%Values in dBm (kD at 1/1024)
P1a = -54.650;
P2a = -57.660; 
P3a = -59.526;
P4a = -61.835;
P5a = -63.255;

Delta_1 = P1-P1a; %17.0640
Delta_2 = P2-P2a; %17.0550
Delta_3 = P3-P3a; %16.1940
Delta_4 = P4-P4a; %16.5680
Delta_5 = P5-P5a; %16.6250