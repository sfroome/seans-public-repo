%10^(-34.280/20);  
%(pi*2^-7)/(2*abs(sin(pi*1.509e6/25e6)));

%x=10*log10(((pi*2^(-7))/(2*abs(sin(pi*1.509e6/25e6))))^2/1e-3);

%f = 1.509e6/25e6; %25MHz sampling frequency
f = 1.446/25;

denom = 2*sin(pi*f);
kD = 2^(-7);

num = pi*kD;

A_sq = (num/denom)^2;

Check_Val = 10*log10(A_sq)


x_for_model = -10; %dBm
y = -34; % dBm

Noise_Power = y - x_for_model