delta = 0:1/2000:1/2-1/2000;
detector_out_DC=(1/2)*sin(2*pi*(delta));
delta_phi = asin(2*detector_out_DC)/(2*pi)-pi/2;

plot(detector_out_DC,delta_phi)
grid on
    