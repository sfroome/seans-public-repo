f0 = 10e3:12.5e6;
kD = 2^-7;


A_Freq_Noise = (pi*kD)./(2*abs(sin(pi*2*f0)));

Pow_dB = 20*log10(A_Freq_Noise);

plot(f0,mean(Pow_dB))
