delta = 0:1/2000:1/2-1/2000;
detector_out_DC=(1/2)*sin(2*pi*(delta));
delta_phi = asin(2*detector_out_DC)/(2*pi)-1/4;

plot(detector_out_DC,delta_phi)
grid on

xlabel('Detector Out DC');
ylabel('Delta Phi');
title('DC Value of Detector Out Versus Delta Phi');

    