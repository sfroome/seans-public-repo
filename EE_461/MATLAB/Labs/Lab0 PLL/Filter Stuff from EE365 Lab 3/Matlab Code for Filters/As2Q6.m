%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% specify the numerator and denominator polynomials
% for H(z) = 1 / ( 1 - 2rcos(phi)z^(-1)+r^2z^(-2) )
r = 0.97; phi = pi/4; % parameters given in question
numerator_poly = [1, 2, 1];
denominator_poly = [1, -1.8, 0.81];
% end polynomial definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w = [0:1999]/2000*pi; % freq vector in radians/sample
H = freqz(numerator_poly, denominator_poly, w);
[max_gain, index_of_max] = max(abs(H));
freq_of_max = w(index_of_max); % in radians/sample
freq_of_max_cycles = freq_of_max/2/pi; % in cycles/sample
figure(1);
plot(w, abs(H)); grid;
title(['max gain = ', num2str(max_gain),'; ', ...
'frequency of max gain = ', num2str(freq_of_max), ...
' radians/sample or ', ...
num2str(freq_of_max_cycles), ' cycles/sample;']);
xlabel('freq in radians/sample');
ylabel('abs(H(e^{j w})');

%%%%%% FILTER 1 %%%%%%%%%%%%

r = 0.97; phi = pi/4; % parameters given in question
numerator_poly = [1];
denominator_poly = [1, -2*r*cos(phi), r^2];
% end polynomial definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w = [0:1999]/2000*pi; % freq vector in radians/sample
H = freqz(numerator_poly, denominator_poly, w);
[max_gain, index_of_max] = max(abs(H));
freq_of_max = w(index_of_max); % in radians/sample
freq_of_max_cycles = freq_of_max/2/pi; % in cycles/sample
figure(1);
plot(w, abs(H)); grid;
title(['max gain = ', num2str(max_gain),'; ', ...
'frequency of max gain = ', num2str(freq_of_max), ...
' radians/sample or ', ...
num2str(freq_of_max_cycles), ' cycles/sample;']);
xlabel('freq in radians/sample');
ylabel('abs(H(e^{j w})');


%%%%%%%%% FILTER 2 %%%%%%
r = 0.97; phi = pi/4; % parameters given in question

denominator_poly = [1];
numerator_poly = [1, -2*r*cos(phi), r^2];
% end polynomial definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w = [0:1999]/2000*pi; % freq vector in radians/sample
H = freqz(numerator_poly, denominator_poly, w);
[max_gain, index_of_max] = max(abs(H));
freq_of_max = w(index_of_max); % in radians/sample
freq_of_max_cycles = freq_of_max/2/pi; % in cycles/sample
figure(1);
plot(w, abs(H)); grid;
title(['max gain = ', num2str(max_gain),'; ', ...
'frequency of max gain = ', num2str(freq_of_max), ...
' radians/sample or ', ...
num2str(freq_of_max_cycles), ' cycles/sample;']);
xlabel('freq in radians/sample');
ylabel('abs(H(e^{j w})');



%%%%%%%%%%%%%%%%%% FILTER 3 %%%%%%%%%%%%%%%%%%%%%%%%%%

r = 0.97; phi = pi/4; % parameters given in question
numerator_poly = [1, -2*cos(phi), 1];
denominator_poly = [1, -2*r*cos(phi), r^2];
% end polynomial definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w = [0:1999]/2000*pi; % freq vector in radians/sample
H = freqz(numerator_poly, denominator_poly, w);
[max_gain, index_of_max] = max(abs(H));
freq_of_max = w(index_of_max); % in radians/sample
freq_of_max_cycles = freq_of_max/2/pi; % in cycles/sample
figure(1);
plot(w, abs(H)); grid;
title(['max gain = ', num2str(max_gain),'; ', ...
'frequency of max gain = ', num2str(freq_of_max), ...
' radians/sample or ', ...
num2str(freq_of_max_cycles), ' cycles/sample;']);
xlabel('freq in radians/sample');
ylabel('abs(H(e^{j w})');

%%% FILTER 4

r = 0.97; phi = pi/4; % parameters given in question

numerator_poly = [1, -2*cos(phi), 1];
denominator_poly = [1, -2*r*cos(phi), r^2];
% end polynomial definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w = [0:1999]/2000*pi; % freq vector in radians/sample
H = freqz(numerator_poly, denominator_poly, w);
[max_gain, index_of_max] = max(abs(H));
freq_of_max = w(index_of_max); % in radians/sample
freq_of_max_cycles = freq_of_max/2/pi; % in cycles/sample
figure(1);
plot(w, abs(H)); grid;
title(['max gain = ', num2str(max_gain),'; ', ...
'frequency of max gain = ', num2str(freq_of_max), ...
' radians/sample or ', ...
num2str(freq_of_max_cycles), ' cycles/sample;']);
xlabel('freq in radians/sample');
ylabel('abs(H(e^{j w})');



%%% FILTER 5
r = 0.97; phi = pi/4; % parameters given in question

numerator_poly = [1, 2, 1];
denominator_poly = [1, -1.8, 0.81];
% end polynomial definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w = [0:1999]/2000*pi; % freq vector in radians/sample
H = freqz(numerator_poly, denominator_poly, w);
[max_gain, index_of_max] = max(abs(H));
freq_of_max = w(index_of_max); % in radians/sample
freq_of_max_cycles = freq_of_max/2/pi; % in cycles/sample
figure(1);
plot(w, abs(H)); grid;
title(['max gain = ', num2str(max_gain),'; ', ...
'frequency of max gain = ', num2str(freq_of_max), ...
' radians/sample or ', ...
num2str(freq_of_max_cycles), ' cycles/sample;']);
xlabel('freq in radians/sample');
ylabel('abs(H(e^{j w})');