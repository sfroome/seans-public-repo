%%%%%% FILTER 2 %%%%%%%%%%%%
r = 0.9; phi = pi/4; % parameters given in question
denominator_poly = [1];
numerator_poly = [1, -2*r*cos(phi), r^2];
% end polynomial definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w = [0:1999]/2000*pi; % freq vector in radians/sample
H = freqz(numerator_poly, denominator_poly, w);
[max_gain, index_of_max] = max(abs(H));
max_gain_db = max(10*log10(abs(H).^2));
freq_of_max = w(index_of_max); % in radians/sample
freq_of_max_cycles = freq_of_max/2/pi; % in cycles/sample
figure(1);
plot(w/2/pi, 10*log10(abs(H).^2)); grid;
title('Filter 2');
xlabel('freq in cycles/sample');
ylabel('10log(|H|^2)');


figure (2)
plot(w/(2*pi), phase(H))



a=2*cos(phi)
round(a*2^17)

