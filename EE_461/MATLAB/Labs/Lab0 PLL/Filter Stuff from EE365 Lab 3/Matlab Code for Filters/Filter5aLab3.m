%%%%%% FILTER 5a %%%%%%%%%%%%
r = 0.9; phi = pi/4; % parameters given in question
numerator_poly = [1 1];
denominator_poly = [1];
% end polynomial definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w = [0:1999]/2000*pi; % freq vector in radians/sample
H = freqz(numerator_poly, denominator_poly, w);
[max_gain, index_of_max] = max(abs(H));
freq_of_max = w(index_of_max); % in radians/sample
freq_of_max_cycles = freq_of_max/2/pi; % in cycles/sample
figure(1);
plot(w/(2*pi), 10*log10(abs(H).^2)); grid;
title('Filter 5');
xlabel('freq in cycles/sample');
ylabel('10log(|H|^2)');


a = 0.9
round(0.9*2^17)