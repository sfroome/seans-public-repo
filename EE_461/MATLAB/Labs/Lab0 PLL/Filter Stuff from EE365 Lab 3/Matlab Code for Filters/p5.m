%10^(-34.280/20);  
%(pi*2^-7)/(2*abs(sin(pi*1.509e6/25e6)));

%x=10*log10(((pi*2^(-7))/(2*abs(sin(pi*1.509e6/25e6))))^2/1e-3);

%f = 1.509e6/25e6;
f = 1.446

denom = 2*abs(sin(pi*f));
kD = 2^(-7);

num = pi*kD;

A_sq = (num/denom)^2;

10*log10(A_sq/1e-3)