kD = 2^-7;
f0 = 10e3:10e3:12.5e6;
x = 2*abs(sin(2*pi*f0));

%H_ejw_inv = x*1/(pi*kD);
H_ejw = (pi*kD)./x;

%Pow = (H_ejw).^(2)/2;

Pow_dB = 10*log10(H_ejw/1e-3);

plot(f0, Pow_dB)