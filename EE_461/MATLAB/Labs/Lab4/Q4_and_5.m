clear all
clf


fp = 3/16;
fs = 5/16;
fc = (fp+fs)/2; 


dp = 0.003;
ds = dp;

A = -20*log10(dp);
%Since A = -50.45 dB, we want to use the Hann Window.

%beta = 0.5842*(A-21)^(.4) + 0.07886*(A-21);
beta = 0.1102*(A-8.7);

N = (A-8)/(2.285*2*pi*(fs-fp)) + 1; % Reminder M is the order of the filter
N = round(N);

M = N - 1;
n = 0:M;

win = kaiser(N, beta)';

hd = 2*fc*sinc(2*fc*(n-M/2));

h = hd.*win;

h_1s17 = round(h*2^17)'

figure(9)
clf
[H,w] = freqz(h, 1, [0:0.01:pi]);
plot(w/(2*pi), 20*log10(abs(H)))
title('Lowpass Filter, Custom Kaiser Design')
xlabel('f (cycles/sample)')
ylabel('20*log(|H(e^{j\omega})|)')
grid
