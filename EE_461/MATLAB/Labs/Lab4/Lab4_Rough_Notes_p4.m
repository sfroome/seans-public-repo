%Lab 4 Rough Notes Part 4

%BLACKMAN KAISER

% a), b), c),

% at 500 KHz, Gp is -6.850 dBm
%

% d) SO IT IS SUPPOSED TO BE -6.2 dB DOWN. FOR ALL OF THE FILTERS
% -6.267 dB down from Marker 1 is 2.645 MHz
% Fc is 3.145 MHz or 0.1258 cycles/sample

% e), f), g)
% Hooray for perfectly flat stopband that has no visual peaks because the
% noise floor is too high =D


% h)
% The approximated value for the peak in the first side lobe is -50.882 dB
% down from marker 1, at 5.043 MHz away from Marker 1.
% 
% With no attenuation, at 4.668 MHz away from Mk1, it's -50.957 dB

% i)
%HOORAY FOR NOISE FLOOR
% 4.668 MHz at -71.683 dB down from Marker 1

% j) Fpk_staho is 5.168 MHz (at -78.225 dBm)
%5.168/25 = .2067


% k) Roughly -112 KHz away and at 0.416 dB

% so 5.056 MHz  (.2022 cycles/sample)



% PERSONALLY DESIGNED FILTER
% It looks like garbage, but it's correct (Just less lobes than MATLAB indicates)!

% a), b), c)
% 500KHz Gp = -8.096 dBm (there's minimal passband ripple)

% d) -6.254 dB away from 500 KHz (is 5.886MHz relative to Marker 1
% Fc is 6.386 MHz or .2554 samples/cycle (theoretically about 1/4 in
% reality)


% e), f), g)
%(It's -16.009 dB below marker 1, and 9.465 MHz away as well)
% 


% h)
% Very little difference. -16.024 dB below, 9.465 MHz


% i)
% With no attenuation, 9.465 MHz awaym -15.698 dB below (all values
% relative to marker 1.

% j)
% Fpk_stahp is 9.965 MHz (.3986)

% k) Closest I can get is -0.021 dB apart, and -2.043 MHz away.
% Fstahp is 7.909 MHz or .3164 cycles per sample (designed to be .3125
% cycles per sample ie: 5/16)






%Q10

% Looking at the two Hann Windows, the Kaiser has a slightly narrower
% transition band, or at least a lower value for the stopband.

%
% The Hamming Kaiser equivalent also has a somewhat narrower transition
% band as well. (I think I  must have calculated or measured fs wrong for
% the Hamming Kaiser wrong?) (Probably the Hann)

% At 30 dB attenuation, Blackman is just a pain to measure, although
% noticeably has a narrower transition band as well


% Q11

% Maaaaybe?! It certainly met the fp and fs values, even if it's not a very
% good LPF (is it even meant to be an LPF?)



