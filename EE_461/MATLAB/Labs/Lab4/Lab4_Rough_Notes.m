% Lab 4 Rough Notes
% RECTANGULAR

% Need to do the prelab before I can properly do the lab.


%Steps 1 through 5
% I Setup the switches in a separate filter file.

%Step 6
%Rectangular filter so SW[4:2] = 3'b110 (aka 6)
%Remember to set SW[1] = 1'b1 or else you're simply passing a useless
%default signal (aka the NCO frequency, which when swept is just a almost
%flatline)
%i) -43.522 dBm at 4.319 MHz

% Step 7. Oh hey it mentions that!

%Step 8

% c)
%So Gp = -6.576 dBm ?? Wat. (I think I should be subtracting 5dBm from this
%value...)

% d)
%Relative to Marker 1, Frequency is approximately 2.629 MHz and -6.369 dB. (-6.36 dB below 500
%KHz)
% Basically Marker 2 is at 3.129 MHz and -12.930 dBm 
% So fc =  3.129/25 = .1252 cycles/sample (A bit bigger than the sample calculated value)

% e, f, g)
%It's around -15dBm although that's not really needed to be said (and it's
%supposed to be relative to marker 1, not marker 2 haha)

% h) Marker 3 is -22.182 dBm relative to marker 1 and 803 KHz away
% i) With 0 dB of input attenuation, it's -21.311 dBm

% j) f_pk_stop -28.748 dBm and at 3.932 MHz which is .1573 cycles/sample.
% f_pk_stop is the frequency of the first peak in the stopband. Aka it's
% half the width of the main lobe plus the cutoff frequency.

% k) Stop Band Corner Frequency 
% It's roughly at 3.537 MHz and -28.010 dBm (off by about -.132 dBm not
% accounting for DAC correction)
3.537/25; % = 0.1415


% HANN WINDOW
% c)
% Gp = -6.702 dBm at 500 KHz

% d) at 2.666 MHz away from Marker 1, and -6.515 dB away (correction is
% -.5314 dB)
% So Fc = 3.166 MHz and -13.212 dBm
% fc = .1266 cycles/sample

% e, f, g)
% HOW DO I MAKE SURE I STOP THE SWEEP WHEN I NEED TO?! AAAA
% It's largely guesswork making sure the sweep goes far enough into the
% stopband....

% h) 
%Well assuming I'm right Marker 3, relative to Marker 1 is -42.813 dB 
% 
% i) -43.522 dBm at 4.319 MHz (No Attenuation)

% j) -50.250 dBm at 4.819 MHz 
4.819/25 % = .1928 cycles/sample is fpk_stahp

% k) -344 KHz away from marker 3, abd -0.101 dB apart.
% Fstahp = 4.474 MHz or .1790 cycles/sample.



