clear all


fc = 1/8;
N =31;
M = N-1;
n = 0:M;

hd = 2*fc*sinc(2*fc*(n-M/2));

%Kaiser Equivalent Hann Calculations
A_Hann = 44;
beta_Hann = 3.86;

fs_minus_fp_Hann = (A_Hann-8)/(2.285*2*pi*(N-1));
fp_Hann = fc - fs_minus_fp_Hann/2;
fs_Hann = fc + fs_minus_fp_Hann/2;

%ws_Hann = fp_Hann*2*pi;
%wp_Hann = fs_Hann*2*pi;

win = kaiser(N, beta_Hann);
h_hann_kaiser = hd.*win.';


%Kaiser Equivalent Hamming Calculations
A_Hamming =53;
beta_Hamming = 4.86;

fs_minus_fp_Hamming = (A_Hamming-8)/(2.285*2*pi*(N-1));
fp_Hamming = fc - fs_minus_fp_Hamming/2;
fs_Hamming = fc + fs_minus_fp_Hamming/2;

%ws_Hamming = fp_Hamming*2*pi;
%wp_Hamming = fs_Hamming*2*pi;

win = kaiser(N, beta_Hamming);
h_hamming_kaiser = hd.*win.';

% Calculations for Blackman Kaiser Window
A_Blackman = 74;
beta_Blackman = 7.04;

fs_minus_fp_Blackman = (A_Blackman-8)/(2.285*2*pi*(N-1));
fp_Blackman = fc - fs_minus_fp_Blackman/2;
fs_Blackman = fc + fs_minus_fp_Blackman/2;

%ws_Blackman = fp_Blackman*2*pi;
%wp_Blackman = fs_Blackman*2*pi;

win = kaiser(N, beta_Blackman);
h_blackman_kaiser = hd.*win.';


[H_hd,w] = freqz(hd, 1, [0:0.01:pi]);

figure(5)
clf
plot(w/(2*pi), 20*log10(abs(H_hd)))

title('Lowpass Filter, hd[n]')
xlabel('f (cycles/sample)')
ylabel('20*log(|H(e^{j\omega})|)')
grid

figure(6)
clf
[H_hann_kaiser,w] = freqz(h_hann_kaiser, 1, [0:0.01:pi]);
plot(w/(2*pi), 20*log10(abs(H_hann_kaiser)))
title('Lowpass Filter, Hann Kaiser Window')
xlabel('f (cycles/sample)')
ylabel('20*log(|H(e^{j\omega})|)')
grid


figure(7)
clf
[H_hamming_kaiser,w] = freqz(h_hamming_kaiser, 1, [0:0.01:pi]);
plot(w/(2*pi), 20*log10(abs(H_hamming_kaiser)))
title('Lowpass Filter, Hamming Kaiser Window')
xlabel('f (cycles/sample)')
ylabel('20*log(|H(e^{j\omega})|)')
grid


figure(8)
clf
[H_blackman_kaiser,w] = freqz(h_blackman_kaiser, 1, [0:0.01:pi]);
plot(w/(2*pi), 20*log10(abs(H_blackman_kaiser)))
title('Lowpass Filter, Blackman Kaiser Window')
xlabel('f (cycles/sample)')
ylabel('20*log(|H(e^{j\omega})|)')
grid


h_hann__kaiser1s17 = round(h_hann_kaiser*2^17);
h_hamming_kaiser_1s17 = round(h_hamming_kaiser*2^17);
h_blackman_kaiser_1s17 = round(h_blackman_kaiser*2^17);