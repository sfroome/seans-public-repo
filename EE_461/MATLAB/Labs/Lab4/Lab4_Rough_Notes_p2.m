% LAB 4 ROUGH NOTES PART 2

%HAMMING

% a, b, c)
% Gp = -6.702 dBm

% d)
% marker 2 is 2.666 MHz away from Marker 1. and -6.589 dBm (so -6 + -
% .5314 approximately)
% So Marker 2 is at 3.166 MHz and -13.285 dBm (set as normal)
%

% e, f, g) 
% Marker 3 relative to marker 1, at 4.369 MHz away and 45.804 dB down
% 

% h) changing the sweep generator I get -47.571 dBm and 4.369 MHz 
%(marker 3 relvative to marker 1) 
% Marker 3 on its own is -53.341 dBm and 4.869 MHz


% i) -56.866 dBm (Okay something is off here...)
% (or -50.110 dBm marker 3 relative to 1). 
% Something is off with my Hamming gain values...)


% j) Fpk_stahp is 4.869 MHz = .1948 cycles/sample

% k)

% Roughly 4.4562 MHz (-57.239 dBm) (and -307 KHz, -.438 dB relative to marker 3) 
% so .1782 cycles per sample.

% Welp. Only 5 more to go.



%BLACKMAN

% a), b), and c)
%
%  Gp = -6.729 dBm at 500 MHz.
%

% d) Marker 2 Relative to Marker 1
% At 2.7 MHz and -6.762 dB down (relative to Marker 1 at 500 KHz)
% Fc = 3.2 MHz or 0.1280 cycles/sample. (-13.492 dBm)
%
%

% e), f), g) 
% Since I can't even see the lobes... I'm picking the end of the transtion
% band at 4.725 MHz (relative to marker 1, and -50.865 dB below)


% h)
%I  retook the measurement once I knew where the first lobe actually was,
%since I was off by almost 1 MHZ from where the first lobe actually was.
% so at 5.875 MHz (relative to 1), and it's still only around -51.105 dB below.

%i)
%
%With no Attenuation it's actually at -73.696 dB (relative to Marker 1)
% Also the lobe is much farther over at 5.875 MHz 
% j)
% Fpk_stahp is therefore at 6.375 MHz (and is at -80.365 dBm)
% 6.375/25 = .2550

% k) Marker 4 is -900 KHz away and -0.109 dB relative to Marker 3 
% Meaning fs is 5.475 MHz and is sitting at -80.474 dBm 
% 5.475/25 = 0.2190





