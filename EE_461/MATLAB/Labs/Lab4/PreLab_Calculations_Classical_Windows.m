% Calculations for Rectangular Window
clear all

N =  31;
fc = 1/8;
M = N-1;
n = 0:M;

hd = 2*fc*sinc(2*fc*(n-(N-1)/2));

%Main Lobe Width
MLW_Rectangular = 2/N;
%Frequency Fpk_stahp
fpk_stop_Rectangular = (1/2)*MLW_Rectangular + fc;
%Roll-Off Correction for the DAC
DAC_Correct_Rectangular = 20*log10(sin(pi*fpk_stop_Rectangular)/(pi*fpk_stop_Rectangular));
win = rectwin(N);
h_rectangular = hd.*win.';

%Calculations for Hann Window
MLW_Hann = 4/(N-1);
fpk_stop_Hann = (1/2)*MLW_Hann + fc;
DAC_Correct_Hann = 20*log10(sin(pi*fpk_stop_Hann)/(pi*fpk_stop_Hann));

win = hann(N);
h_hann = hd.*win.';

% Calculations for Hamming Window
%Main Lobe Width
MLW_Hamming = 4/(N-1);
%Frequency Fpk_stahp
fpk_stop_Hamming = (1/2)*MLW_Hamming + fc;
%Roll-Off Correction for the DAC
DAC_Correct_Hamming = 20*log10(sin(pi*fpk_stop_Hamming)/(pi*fpk_stop_Hamming));

win = hamming(N);
h_hamming = hd.*win.';



% Calculations for Blackman Window
%Main Lobe Width
MLW_Blackman = 6/(N-1);
%Frequency Fpk_stahp
fpk_stop_Blackman = (1/2)*MLW_Blackman + fc;
%Roll-Off Correction for the DAC
DAC_Correct_Blackman = 20*log10(sin(pi*fpk_stop_Blackman)/(pi*fpk_stop_Blackman));

win = blackman(N);
h_blackman = hd.*win.';


figure(1)
clf
[H_rectangular,w] = freqz(h_rectangular, 1, [0:0.01:pi]);
plot(w/(2*pi), 20*log10(abs(H_rectangular)))

title('Lowpass Filter, Rectangular')
xlabel('f (cycles/sample)')
ylabel('20*log(|H(e^{j\omega})|)')
grid

figure(2)
clf
[H_hann,w] = freqz(h_hann, 1, [0:0.01:pi]);
plot(w/(2*pi), 20*log10(abs(H_hann)))
title('Lowpass Filter, Hann Window')
xlabel('f (cycles/sample)')
ylabel('20*log(|H(e^{j\omega})|)')
grid


figure(3)
clf
[H_hamming,w] = freqz(h_hamming, 1, [0:0.01:pi]);
plot(w/(2*pi), 20*log10(abs(H_hamming)))
title('Lowpass Filter, Hamming Window')
xlabel('f (cycles/sample)')
ylabel('20*log(|H(e^{j\omega})|)')
grid


figure(4)
clf
[H_blackman,w] = freqz(h_blackman, 1, [0:0.01:pi]);
plot(w/(2*pi), 20*log10(abs(H_blackman)))
title('Lowpass Filter, Blackman Window')
xlabel('f (cycles/sample)')
ylabel('20*log(|H(e^{j\omega})|)')
grid

h_rectangular_1s17 = round(h_rectangular*2^17);
h_hann_1s17 = round(h_hann*2^17);
h_hamming_1s17 = round(h_hamming*2^17);
h_blackman_1s17 = round(h_blackman*2^17);
