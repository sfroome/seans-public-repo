%Lab 4 Rough Notes Part 3

% HANN KAISER

% a), b), c)

% At 500 KHz, Gp is about -6.746 dBm.

% d) (Not clear on the procedure, but anyway)
%  At 2.6250 MHz (relative to marker 1), -6.046 dB away
% 3.125/25 = 1/8 (Marker 2 is at 3.125 MHz "Normal" and at -12.767 dBm)



% e), f), g)
%
% Largest side lobe at 3.925 MHz (relative to marker 1)
% and -44.152 dB.
%

% h)
% With the sweep cut down slightly, I get 
% -44.922 dB below

% i)
% -45.960 dB below with 0 attenuation
% j) 4.425 MHz (at -52.684 dBm)
% 4.425/25=.1770
%
% k) Closest I can get is within -.413 dB, or -162.5 KHz away.
% but Fstahp is about 4.2625 MHz.
% 4.2625/25 = .1705 cycles/sample



% HAM ON A KAISER
% HAMMING KAISER

% a), b), and c)
%Gp = -6.733 dBm @ 500 KHz

%d) (Again, ignoring correction for now... and just taking the value 6dB
%down)
% -6.049 dBm down is 2.6250 MHz (marker 2 relative to 1)
% or 3.125 MHz exactly (1/8 cycles/sample)

% e), f), g)
 % Largest dBm value I could find was -48.710 dBm in the stopband.
 % at 4.175 MHz (relative to marker 1)


% h)
% Shifting over slightly, at -49.978 dB below at 4.2125 MHz 


% i)
% Shifting slightly again, max value is -53.194 dB at 4.1625 MHz.

% j)
% so Fpk_stahp is 4.6625 MHz (or .1865 cycles/sample)

% k)
%
% 0.063 dB away from Marker 3, or -162.5 KHz away
% so Fstahp is 4.5 MHz or 0.1800 cycles/sample



