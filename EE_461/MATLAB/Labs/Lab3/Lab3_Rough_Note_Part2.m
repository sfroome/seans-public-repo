%LAB 3 PART 2 ROUGH NOTES
%Steps 1 through 6

%You may need the Trace 2 from step 8 of Part 1. It never mentions to clear
%it before the end of Part 1.

% Step 3) 
% Trace 4 saved as MAX_HOLD. 
% Not sure what is being asked by "measure." It looks like a Low Pass filter with
% zeroes in it.
% It's not exactly asking for the frequency response of the filter at any
% particular frequency... so Imma just plot it, and plot the SUT in the
% next step too.

%Gold standard of couse is signed 18 bit. (1s17)
%SUT_out is as well

%Step 4)
% SUT looks similar just slightly... off.
%


%{
As shown from the local parameters and explained below.
//DATA_MASK = 18'H3FE00, // must be 18 bits,
	                          // initially truncate to keep 9 bits

	          // COEF_MASK = 18'H3FFFF, // must be 18 bits,
	                          // initially don't truncate
				  //NCO_1_FREQ = 12'H1A0; // Step 18 Part 1 of Lab 3
				 // NCO_1_FREQ = 12'H1A3; // must be 12 bits, 
				                // frequency in cycles/sample in 0u12 format
									 // Use this NCO for the rest of Part 1
											  
	         	DATA_MASK = 18'H3FFFF, // For Part 2
					COEF_MASK = 18'H3FE00, // For Part 2			
					NCO_1_FREQ = 12'H1A3; //  For Part 2	
					// In part 1, the Coefficients were not masked at all.
					// You can tell that by the fact that every single bit 
					// was anded with a 1. Only coefficient bits that are 0
					// would result in a bit being truncated/masked.
					
					//The data bits in Part one as stated above, had the 9 LSB's
					// truncated at the output. In part 2, none are truncated as stated.				
%}


% Step 5)
% It is definitively clear from the spectrum analyzer that truncating the
% coefficients has had an effect on the output of the SUT. The peak between fourth and
% fifth zeroes especially have been affected (ie: about 5.85MHz and
% 6.775MHz)

% Reminder Side lobes = Local maxima (aka the peaks)


% Step 6)
%{
First off (Matlab is bad at allowing for commenting out multiple lines at
once)

If the number of coefficients is 15
and the number of masks is 
18'H3FE00 compared to 18'H3FFFF
111_1111_1111_1111_1111

111_1111_1110_0000_0000

(Right each coeffiecient is 18 bits but there are 16 (0 thru 15) of them)
So literally 0 through 8 are zero.

Using that fourth side lobe as an example, it's zeroes are further apart
from eachother on each side of it compared to the gold standard. It also
has a larger amplitude compared to the gold standard.

The sixth lobe is simply shifted to the right slightly compared to the gold
standard, but it doesn't seem to have much of a difference in amplitude.

The second lobe does seem to have slightly more spaced out zeroes and does
also have a larger amplitude compared to the gold standard..

So yes. yes it does.

%}




