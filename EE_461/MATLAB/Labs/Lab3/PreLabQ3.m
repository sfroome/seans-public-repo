%Pre Lab Crap Q3



%3a
%Output is 1s8
%Would total power not simply be .275^2 the same as Q1? (See formulas in
%other script
s = 2^-8; % Assume it's already truncated and L is large

Pow_DC = (-s/2)^2
Pow_AC = s^2/12
Pow_Total = Pow_AC + Pow_DC

%3b
%Again see Q1 script. I could be completely wrong, but anyway...


Sqq1 = 2*Pow_AC/Fs