%LAB 3 PART 2 ROUGH NOTES 
%From Step 7 onwards.

% Step 7)

% It seems like the ripple is at least partially caused by the noise from
% the SUT, namely the truncation noise. Considering it cleans up somewhat
% after each sweep, that's probably the cause. I suspect if I switch SUT to
% view, and swept gold standard afterwards with the Math trace, 
%it would not change at all.

% Step 8 and 9 )
% The stopband is definitely worse. Afterall, for a Lowpass filter,
% controlling the passband ripple is far more important than the stopband
% ripple... seeing as it's the crap in the stopband you're filtering out
% anyway. As long as you are filtering out those frequencies... ripples
% aren't going to matter unless they're so bad that they get through the
% filter to the output of your thing whatever that may be.

% Step 10 and 11
% OH MY 0dB of attenuation is UGLY. It "blows up" at 3.125, and 12.5 and...
% there are false readings inside the passband as well.

% Step 12-14)
% I set the sweep to run between about 3.425 MHz and about 12.1625 MHz

% Steps 15-17

% It's MUCH cleaner. Probably because much of the noise energy is of course in both
% the passband and stopband, and it gets stopped when the sweep generator
% isn't sweeping at those frequencies. (I need to make sure I read up on
% how exactly this works).