%LAB 3 PART 1 ROUGH NOTES
% Page 9 has the high level circuit diagram
% Q4 - Remember to draw the filter later to include the bit masking AND
% operators at the input and at the output
%
% Q6 - 30dB attenuation and 0 to 12.5MHz span
% Q7 - SW[10]= 1'b0 sets Fs = 25MHz (sets the sweep generator to ON)
%
%
%
% Recall: LFSR - Linear-Feedback Shift Register 
%
%
%Q8
% a) -Power Reading at 1.5MHz (using MaxHoldTrace)
% P = -18.972 dBm (Ranges between -18.9 and -20dBm roughly)
% RBW = 120KHz
P0_dBm = -18.972 %dBm
RBW = 120e3 %Hz

RBW_dBm = 10*log10(120e3);

PSD = P0_dBm - RBW_dBm % dBm/Hz
% PSD = -69.7638 dBm

%Using Marker Function Band Interval Density 
%across 1MHz measurement interval I get PSD = -68.72dBm
%
%Q8b) wat? 
%
%
% -21.660 dBm @ 12.5MHz
% -15.416 dBm  @ 500KHz (too much noise at 0Hz)
% Must look at what sinc roll off is
% It's roughly 3.9dB. Last calculation gave a delta of about -4.2dB
% (One marker had to be at 863KHz though.)
%

%Q9
% a to c) Yes there is a null at 3.125 MHz
%
% d) Fs is now 1.56Mhz
%
% e) -21.791dBm at 1.56 MHz
%   -18.105dBm at 100 KHz    
%   Huh.... This is weird.
%
% f) NOISE MEASUREMENTS
% P0 at 150KHz = -18.618 dBm
P0_150_dBm = -18.618; %dBm
RBW_150 = 15e3; % KHz
RBW_150_dBm = 10*log10(RBW_150);

PSD_150 = P0_150_dBm - RBW_150_dBm % -60.3789dBm/Hz

delta = PSD - PSD_150 % -9.3849 dBm/Hz

%Using Marker Function and 100KHz span
% Measured PSD is -59.683 dBm/Hz

%Q9 g)

10*log10(8) % Does it drop by this  much? (Ie: 9dB ish)
%Yes. Yes it does.

%Q10
% Verify Settings Again
% 
% b) At 6.25MHz, The NCO noise has a power level of -42.95dBm
%   Whereas the Reference trace from the beginning has a power level 
%   of -18.48
%   So -42.95--18.48 = 24.47dBm 
% A similar reading at 1.5MHz yields a difference of 
% -42.91--17.72 = -25.19 dBm
% Difference should be -53+30 = 23dB below the reference which is 
% Approximately correct.

%Q11
% Aside from the odd peak, the noise does have about 5dBm of ripple.
% (Is between -40 dBm and -45dBm)

%Q12
% It just shifts up Trace One to the -10dBm line (and inverts it) 
% Math-wise, this makes sense.
% reference is about -20dBm, the noise is around -40ish. subtracting
% -30dBm from -20--40 = -20 gives around -50dBm. My numbers are far from
% perfect but they at least make sense.
%
% Actually... It would make sense that I initially subtracted it wrong. 
%
% Trace 1 is the noise. Trace 2 is saved flatline reference. Trace1-Trace2
% -30.1dBm does place the noise along the -50dBm line on the spectrum
% analyzer (as should be expected. I simply subtracted them backwards
% yesterday)


%Q13
% Trace 2: aka the Random Sequence
% 6.250 MHz -20.16dBm
%
%Trace 1: aka the Randomized NCO
% 6.250 MHz  -23.57dBm
%
-24.04 - -20.76; % Using Trace Average gives -3.28
%-2.75 %dBm. Ehhh close enough (This was using MAX HOLD for both)

%Another reading (using max hold) gives -23.17 -20.01 = -3.16
-23.17--20.01;
%Q14
%Just plot the Gold Standard
%Trace 4 is Gold


%Q15
%It "blows up" at around (3/4)*3.125. (I guessed 2.3438)
%Trace 1 is different because Noise screws with the frequency response
%especially at the the higher frequencies?
%Trace 5 is SUT (half scale)


%Q16
%NOC_Freq is 2.5625MHz??!
%Power out is -1.185dBm 
%Taking Trace1 - Trace4 -30.1 dBm
% aka SUT - Gold -- 30.1 dBm
%gives -24.050dBm 
%( Gold is the original Trace 2?!)
% "Output of" in this context is referring to the noise. 
%(Says "TRUNCATION NOISE IN THE SUT but is slightly 
%unclear if it's asking to connect NCO_output to 
%the SUT or to the SUT_Noise)
%Trace1 - Trace2 = -48.667 dBm
% SUT_Noise - Random_sequence -30.1 = -48.5 dBm.

%Q17 (Pg 13 - lots of important theory)
%NCO_1_FREQ = 12'H1A3
%12'b0001_1010_0011;
%b11 = 1 so
m = 11;
noise_sinusoid_spacing = 25e6/(2^(m+1)); % = 6.1035e3 Hz = 6.1035 kHz.

%Pretty sure Sinusoid Truncation Noise is the NCO_Noise

Sample_1 = 6.03025e6; % 6.030MHz at -54.205 dBm
Sample_2 = 6.03635e6; % 6.036MHz at -58.260 dBm
six_point_one_kilohertz = Sample_2 - Sample_1; %Oh snap it's right.


% Q18 
Fs = 25e6;
Truncation_Noise_Spacing_Q18 = Fs/2^7;
%Ie 195.31 kHz = 1.9531e5 Hz
%Theoretical Value
%NCO_FREQ appears to be 2.5375 MHz now


%Actual Value is:

Sample1_q18 = 2.5390e6; %2.54 MHz at -37.8 dBm
Sample2_q18 = 2.7345e6; %2.7345 MHz at -36.0 dBm

Sample2_q18 - Sample1_q18; % = 195500 or 195.5 KHz which is close enough.

%AND THAT IS PART 1 OF THE LAB


