%PRELAB CRAP%

%1a
%Total Power%

VFS = 0.275;
VFS_N = -VFS;

mean = (1/2)*VFS + (1/2)*VFS_N;

P_DC = mean^2;
P_AC = (1/2)*(VFS^2) + (1/2)*(VFS_N^2)

P = P_DC + P_AC; % ie: equal to VFS^2 = 0.275^2 = 0.0756 V^2  

Fs = 25e6; 

%1b
Sejw = 2*P_AC/Fs %6.05e-9 V^2/Hz

%1c

10*log10(Sejw/1e-3) %-52.1824 dBm
