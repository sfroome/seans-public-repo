%Question 2 of Prelab Crap

% NCO freq is 12'b0011_0101_0000 and is 0s12
%in other words

NCO_freq = 2^-3+2^-4+2^-6+2^-8 %  =0.207031250000000
%0s12

Fs = 25e6;
%2a
NCO_freq_cps = NCO_freq*2^12 % 848 cycles per sample
%2b
NCO_freq_Hz = NCO_freq*Fs % So  5.175781250000000e+06 Hz

%2c
%Right most bit is b7 (b0 is MSB on LHS) so
% To Calculate period of quantization noise
Period_q=2^(7+1) % = 256 samples

%2d
Period_q_sec = Period_q/Fs % = 1.024e-5 so 10.24 us

%2e 
%Fundamental frequency is reciprocal of period so

Fp = Fs/Period_q % = 9.765625e4 or 97.66kHz

%2f
%Spacing is simply Fp = 97.66KHz

%2g
%Much smaller than the spacings b/w the harmonics so Idk... 5KHz because
%That's what the example says to use (maybe 10KHz?)


