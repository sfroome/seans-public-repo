
H = 1/(1-0.99*exp(i*4*pi*0.2));
H_LPF_Phase = atan2(imag(H),real(H));
H_LPF_Mag = sqrt(dot(H,H));
H_LPF_DC = 1/(1-0.99*exp(0));

n = 0:10000;
y_mod_DC = -0.185*H_LPF_DC+H_LPF_Mag*1.85*sin(4*pi+0.2*n+0.1+H_LPF_Phase);

mean(y_mod_DC)