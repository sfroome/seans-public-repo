clear
theta_DC = 0.2; % For Question 3
%theta_DC = 0.1; % For Question 4
%theta_AC = pi/2;
theta_AC = 1;
fm = 0.01;

%n = 0:1/(2*pi*fm):10000; %Gets different value due to having far more
                          %samples than necessary.
n=0:10000;


theta_n = theta_DC + theta_AC*cos(2*pi*fm*n);
sin_theta_n = sin(theta_n);
mean_n = mean(sin_theta_n) % Approximation of the DC Component.

%beta = 0.5; %For AC= pi/2
beta = 0.75; %For AC = 1;
DC_component = theta_DC*beta