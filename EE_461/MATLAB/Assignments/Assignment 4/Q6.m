clear all;
%Am = 0.1; % For part F
Am = pi/2; % For part G
fm = 0.05;
n=0:10000;

Ar = 2;
f0 = 1;

x_n = (n + Am*cos(2*pi*fm*n))/(2*pi);

x_hat_n = (n + 0.1)/(2*pi);

Ref_sig = Ar*sin(2*pi*x_n);
Feed_sig = cos(2*pi*x_hat_n);

ymult = Ref_sig.*Feed_sig;

%abs(mean(ymult))
mean(sin(0.1+Am*cos(2*pi*fm*n)))

beta = 0.5;
DC_x = 0.1;

approximated_DC_Out = beta*DC_x;