fc = 1/8; % Cutoff Freq (cycles/samples)
N = 31; % # of Coefficients
M = N - 1; % Order of Filter
n = 0:M; % Sample indices

h = 2*fc*sinc(2*fc*(n - M/2));

%Convert the LPR to HPF using Technique 1
h = -h;
h(M/2+1) = 1+h(M/2+1);

figure(1)
clf
stem(n,h);
title ('HPF Impulse Response, h[n]')
xlabel('n (samples)')
grid on

figure(2)
clf
[H,w] =freqz(h,1);
plot(w/(2*pi),10*log10(H.*conj(H)))
ylabel('10 log{(|H(e^{j\omega})|^2}')
title('HPF Response')
grid on

A = H.*exp(j*w*M/2);
figure(3)
subplot(3,1,1)
plot(w/(2*pi),abs(H))
title('HPF Response')
grid on
subplot(3,1,2)
plot(w/(2*pi),real(A))
grid on
ylabel('A(e^{j\omega})')
subplot(3,1,3)
plot(w/(2*pi), -(M/2)*w)
ylabel('\theta(e^{j\omega})')
xlabel('frequency (cycles/sample)')
grid on