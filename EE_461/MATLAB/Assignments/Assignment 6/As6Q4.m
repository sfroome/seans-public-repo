

N = 31; % # of Coefficients
M = N - 1; % Order of Filter
n = 0:M; % Sample indices

% Cutoff Freq (cycles/samples)
fc1 = 1/8;
fc2 = 1/2;

h1 = 2*fc1*sinc(2*fc1*(n - M/2));
h2 = 2*fc2*sinc(2*fc2*(n - M/2));

h = h2 - h1;

figure(1)
clf
stem(n,h);
title ('LPF Impulse Response, h[n]')
xlabel('n (samples)')
grid on

figure(2)
clf
[H,w] =freqz(h,1);
subplot(3,1,1)
plot(w/(2*pi),abs(H))
ylabel('|H(e^{j\omega})|')
grid on
subplot(3,1,2)
plot(w/(2*pi),10*log10(abs(H).^2))
grid on
axis([0 0.5 -50 10])
ylabel('10 log{(|H(e^{j\omega})|^2)}')
subplot(3,1,3)
plot(w/(2*pi), unwrap(angle(H)))
ylabel('\angle H(e^{j\omega})')
xlabel('frequency (cycles/sample)')
grid on

