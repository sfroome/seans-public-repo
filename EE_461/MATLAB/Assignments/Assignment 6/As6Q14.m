clear all
clf

f11=0; % (cycles/sample)
f21= 1/6;
f12= 1/3;
f22= .5;
C1=1;
C2=.5;
M=80; % order of filter
n=0:M; % sample indices

h1=2*f21*sinc(2*f21*(n-M/2))-2*f11*sinc(2*f11*(n-M/2));
h2=2*f22*sinc(2*f22*(n-M/2))-2*f12*sinc(2*f12*(n-M/2));
h=C1*h1+C2*h2;
figure(1)
clf
stem(n,h)
title('Two Band Impulse Response, h[n]')
xlabel('n (samples)')
grid

figure(2)
clf
L=256;
hz=[h,zeros(1,L-length(h))];
H=fft(hz);
k=0:L-1;
% M is even - thus this a type 1 filter with a linear phase response of
% -(M/2)*2*pi*f, where f=k/L
W=exp(-j*(-(M/2)*2*pi)*k/L);
A=H.*W;
A=real(A);
plot(k/L-0.5,A)
ylabel('A(e^{j\omega})')
title('Two Band Amplitude Response')
xlabel('f (cycles/sample)')
grid

figure(3)
subplot(1,3,1)
plot(k/L-0.5,abs(H))

subplot(1,3,2)
plot(k/L-0.5,A)

subplot(1,3,3)
plot(k/L -.5, W);
