x1 = [1 2 3 2 1 0 -1 -2 -3 -2 -1 0];
x2 = [-1 -2 -3 -2 -1 0 1 2 3 2 1 0];

Nd = length(x1);

meanx1 = sum(x1)/Nd;
meanx2 = sum(x2)/Nd;

Rx1x2 = sum(x1.*x2);

Pac_x1 = sum(x1.^2)/Nd;
Pac_x2 = sum(x2.^2)/Nd;
x = x1 + x2;

Pac_x = sum(x.^2)/Nd;
Pac_x_sum = Pac_x1 +Pac_x2;
Power_AC = [Pac_x; Pac_x_sum]

%P1x = 0;
%for n = 1:Nd;
%    P1x = P1x + ((x1(n))*(x1(n)));
%end
%P1x = P1x*(1/12);
%P2x = 0;
%for n = 1:Nd;
%    P2x = P2x + ((x2(n))*(x2(n)));
%end
%P2x = (1/12)*P2x;
%P = P1x + P2x;