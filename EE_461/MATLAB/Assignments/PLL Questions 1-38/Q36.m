% Matlab script to find the magnitude response of H_Xhat(z)
% from values of k1 and k2
% define the frequency vector
w = pi*[0:10^6-1]/10^6; % 10^6 equally space point between 0 and pi
% coefficients of denominator polynomial
k1 = 9.169999999999456e-04 ;
k2 = 0;

a1 = -(2-k1*(1+k2));
a2 = 1-k1;
% location of zero
z1 = 1/(1+k2);
% generate the magnitude response in dB
H_Xhat =20*log10( abs( k1*(1+k2)*( exp(j*w) - z1 ) ./ ...
( exp(j*2*w) + a1*exp(j*w) + a2 ) ) );

plot(w, H_Xhat)