% Matlab script that computes and plot the signals
% in a Type 2 circuit for unit step and unit ramp inputs
%%%%%%%%%%%%%%%%%%%
% system parameters
k1 = 1000;
k2 = 1000;
%%%%%%%%%%%%%%%%%%%
% Matlab parameters
N = 10000; % length of input vector
%%%%%%%%%%%%%%%%%%%
% inputs
x_step = ones(1,N); % unit step
x_ramp = 1:N; % unit ramp
x_ramp = x_step + 2.5.*x_ramp;
%%%%%%%%%%%%%%%%%%%
% reserve space for vectors used for step input
e_step = zeros(1,N); % error signal
x_hat_step = zeros(1,N); % x_hat register (in accumulator)
y1_step = zeros(1,N); % input to the x_hat accumulator
y2_step = zeros(1,N); % input to other accumulator
y3_step = zeros(1,N); % register in second accumulator
%%%%%%%%%%%%%%%%%%%
% reserve space for vectors used for ramp input
e_ramp = zeros(1,N);
x_hat_ramp = zeros(1,N);
y1_ramp = zeros(1,N);
y2_ramp = zeros(1,N);
y3_ramp = zeros(1,N);
%%%%%%%%%%%%%%%%%%%
% initialize registers, i.e. set their values at n=0
x_hat_step(1) = 0; % value at n=0
x_hat_ramp(1) = 0; % value at n=0
y3_step(1) = 0; % value at n=0
y3_ramp(1) = 0; % value at n=0
%%%%%%%%%%%%%%%%%%%
% loop for sequential computation for step input
for n = 0:N-1 % index n is the sample number
% calculate the variables at sample n
e_step(n+1) = x_step(n+1)-x_hat_step(n+1);
y2_step(n+1) = e_step(n+1)*k1;
y1_step(n+1) = y2_step(n+1) + ...
    k2*(y2_step(n+1)+y3_step(n+1));
% calculate the register outputs at sample n+1
if n<N-1 % don�t compute outputs for sample N+1
x_hat_step(n+2) = x_hat_step(n+1) + y1_step(n+1);
y3_step(n+2) = y3_step(n+1)+ y2_step(n+1);
end
end
%%%%%%%%%%%%%%%%%%%
% loop for sequential computation for ramp input
for n = 0:N-1 % caculate variables at sample n
e_ramp(n+1) = x_ramp(n+1)-x_hat_ramp(n+1);
y2_ramp(n+1) = e_ramp(n+1)*k1;
y1_ramp(n+1) = y2_ramp(n+1) + ...
k2*(y2_ramp(n+1)+y3_ramp(n+1));
% calculate the register outputs at sample n+1
if n<N-1 % don�t compute registers for sample N+1
x_hat_ramp(n+2) = x_hat_ramp(n+1) + y1_ramp(n+1);
y3_ramp(n+2) = y3_ramp(n+1)+ y2_ramp(n+1);
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot the results
figure(1);
plot(0:N-1, y3_step,'-g^', 0:N-1, x_hat_step,'-rs', ...
0:N-1, y1_step,'-ko', 0:N-1, e_step,'-bd', ...
'LineWidth',2,'markersize',9);
xlabel('n'); grid;
legend({'y3[n]','xh[n]','y1[n]','e[n]'},'location','east');
title(['Type 2 responses to a step input with k_1 = ', ...
num2str(k1), ' and k_2 = ',num2str(k2)]);
print -depsc type_2_error_for_step_input;
figure(2);
plot(0:N-1, y3_ramp,'-g^', 0:N-1, y1_ramp,'-ko', ...
0:N-1, e_ramp,'-bd','LineWidth',2,'MarkerSize',9);
xlabel('n'); grid;
legend({'y3[n]','y1[n]','e[n]'},'location','northwest');
title(['Type 2 responses to a ramp input with k_1 = ', ...
num2str(k1), ' and k_2 = ',num2str(k2)]);
print -depsc type_2_error_for_ramp_input;
% END OF MATLAB SCRIPT
