clear all
clf

h=[1 0 -1];
M=length(h)-1;  % order of filter
beta=pi/2;  % antisymmetrical impulse response
alpha=M/2;
L=256; % length of fft
hz=[h zeros(1,L-length(h))]; % zero pad h[n] to length L
H=fft(hz);
k=0:L-1;

W=exp(-j*(beta-alpha*2*pi*k/L));
A=H.*W; % generate the amplitude response
A=real(A); % imag(A) is very close to zero so eliminate it
fp=k/L;  % frequency from 0 to 1-1/L cycles/sample
f=(fp - 1/2); % frequency from -0.5 to 0.5-1/L
plot(f,fftshift(A))
hold on
plot(f,fftshift(abs(H)),'r')
legend('A(e^{j\omega})', '|H(e^{j\omega})|')
xlabel('f (cycles/sample')
xlabel('f (cycles/sample)')