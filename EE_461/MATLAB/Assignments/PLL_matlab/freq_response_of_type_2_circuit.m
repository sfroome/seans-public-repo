
clear all;
% specify the location for a double pole on the real line
p = .999; % in the notes on PLL the symbol \rho is used for this

% specify locations for a pair of distinct poles on the real line

p1_real = p; % location of first distinct real pole is that of double pole
S = 33.3; % 
p2_real = 1 - (1-p)/S; % location second pole is S times closer
                          % to the unit circle

% specify the location for two pairs of complex poles
theta_1 = 1*(1-p); % angle of poles in radians
theta_2 = 2*(1-p); % angle of poles in radians
rho = p;           % magnitude of both poles
p1_complex_1 = rho * exp(j*theta_1); 
p2_complex_1 = conj(p1_complex_1);
p1_complex_2 = rho * exp(j*theta_2); 
p2_complex_2 = conj(p1_complex_2);

% find the locations of the zeros for the three sets of poles
z1 = 1 - (1-p)^2/(2*(1-p)); % zero for double pole
z1_real = 1 -  (1-p1_real)*(1-p2_real) / ( (1-p1_real) + (1-p2_real) );
                               % zero for distinct real poles
z1_complex_1 = 1 -  (1-p1_complex_1)*(1-p2_complex_1) / ...
              ( (1-p1_complex_1) + (1-p2_complex_1) );
                        % zero for complex poles with theta = theta_1
z1_complex_2 = 1 -  (1-p1_complex_2)*(1-p2_complex_2) / ...
              ( (1-p1_complex_2) + (1-p2_complex_2) );
                        % zero for complex poles with theta = theta_2
% find constants k1 and k2 for the three sets of poles
k1 = 1- p*p; % for double pole
k2 = (1-p)*(1-p)/k1; % for double pole
k1_real = 1- p1_real*p2_real;
k2_real = (1-p1_real)*(1-p2_real)/k1_real;
k1_complex_1 = 1- p1_complex_1*p2_complex_1;
k2_complex_1 = (1-p1_complex_1)*(1-p2_complex_1)/k1_complex_1;
k1_complex_2 = 1- p1_complex_2*p2_complex_2;
k2_complex_2 = (1-p1_complex_2)*(1-p2_complex_2)/k1_complex_2;
% freq responses
f = 0:0.00001:.5;
w = 2*pi*f;
w_scaled = w/(1-p);
H_x_hat_double = k1*(1+k2)*(exp(j*w) - z1) ./ (exp(j*w) - p).^2;
H_x_hat_real = k1_real*(1+k2_real)*(exp(j*w) - z1_real) ./ ...
              (exp(j*w) - p1_real) ./ (exp(j*w) - p2_real);
H_x_hat_complex_1 = k1_complex_1*(1+k2_complex_1)* ...
                    (exp(j*w) - z1_complex_1) ./ ...
                   (exp(j*w) - p1_complex_1) ./ (exp(j*w) - p2_complex_1);
H_x_hat_complex_2 = k1_complex_2*(1+k2_complex_2)* ...
                    (exp(j*w) - z1_complex_2) ./ ...
                   (exp(j*w) - p1_complex_2) ./ (exp(j*w) - p2_complex_2);               
               
H_e_double = (exp(j*w) - 1).^2 ./ (exp(j*w) - p).^2;
H_e_real = (exp(j*w) - 1).^2 ./ ...
                     (exp(j*w) - p1_real) ./ (exp(j*w) - p2_real);
H_e_complex_1 = (exp(j*w) - 1).^2 ./ ...
                (exp(j*w) - p1_complex_1) ./ (exp(j*w) - p2_complex_1);
H_e_complex_2 = (exp(j*w) - 1).^2 ./ ...
                (exp(j*w) - p1_complex_2) ./ (exp(j*w) - p2_complex_2);
            
figure(1); 
  plot(w, 20*log10(abs(H_x_hat_double)),'-k', ...
       w, 20*log10(abs(H_x_hat_real)),'--r', ...
       w, 20*log10(abs(H_x_hat_complex_1)),'-.b', ...
       w, 20*log10(abs(H_x_hat_complex_2)),':b', ...
       'LineWidth',2);
  axis([0,3,-70,5]);
  xlabel('\omega in radians/sample');
  ylabel('gain in dB');
  title('Feedback 0utput');
  grid;    
  legend(['douple at p = ', num2str(p)], ...
      ['p_1 = ', num2str(p1_real),'  p_2 = ' num2str(p2_real)], ...
      ['complex, \rho = ',num2str(p),' \theta = ', num2str(theta_1)], ...
      ['complex, \rho = ',num2str(p),' \theta = ', num2str(theta_2)], ...
         'location','northeast');
%  print -depsc H_xhat_response;
 figure(2); 
  plot(w_scaled, 20*log10(abs(H_x_hat_double)),'-k', ...
       w_scaled, 20*log10(abs(H_x_hat_real)),'--r', ...
       w_scaled, 20*log10(abs(H_x_hat_complex_1)),'-.b', ...
       w_scaled, 20*log10(abs(H_x_hat_complex_2)),':b', ...
       'LineWidth',2);
  axis([0,10,-15,4]);
  xlabel('\omega/(1-\rho)');
  ylabel('gain in dB');
  title('Feedback 0utput');
  grid;    
  legend(['douple at p = ', num2str(p)], ...
      ['p_1 = ', num2str(p1_real),'  p_2 = ' num2str(p2_real)], ...
      ['complex, \rho = ',num2str(p),' \theta = ', num2str(theta_1)], ...
      ['complex, \rho = ',num2str(p),' \theta = ', num2str(theta_2)], ...
         'location','northeast')   
% print -depsc H_xhat_pass_band_response
figure(3); 
  plot(w, 20*log10(abs(H_e_double)),'-k', ...
          w, 20*log10(abs(H_e_real)),'--r', ...
          w, 20*log10(abs(H_e_complex_1)),'-.b', ...
          w, 20*log10(abs(H_e_complex_2)),':b', ...
          'LineWidth',2);
  xlabel('\omega in radians/sample');
  ylabel('gain in dB');
  title('Error output');
  grid; axis([0,3,-18,2]);
  legend(['douple at p = ', num2str(p)], ...
      ['p_1 = ', num2str(p1_real),'  p_2 = ' num2str(p2_real)], ...
      ['complex, \rho = ',num2str(p),' \theta = ', num2str(theta_1)], ...
      ['complex, \rho = ',num2str(p),' \theta = ', num2str(theta_2)], ...
       'location', 'southeast');
 % print -depsc H_E_pass_band_response; 
figure(4); 
  plot(w_scaled, 20*log10(abs(H_e_double)),'-k', ...
          w_scaled, 20*log10(abs(H_e_real)),'--r', ...
          w_scaled, 20*log10(abs(H_e_complex_1)),'-.b', ...
          w_scaled, 20*log10(abs(H_e_complex_2)),':b', ...
          'LineWidth',2); axis normal;
   xlabel('\omega/(1-\rho)');
  ylabel('gain in dB');
  title('Error output');
  grid; axis([0,20,-3,2]);
  legend(['douple at p = ', num2str(p)], ...
      ['p_1 = ', num2str(p1_real),'  p_2 = ' num2str(p2_real)], ...
      ['complex, \rho = ',num2str(p),' \theta = ', num2str(theta_1)], ...
      ['complex, \rho = ',num2str(p),' \theta = ', num2str(theta_2)], ...
       'location', 'southeast');
 % print -depsc H_E_transition_band_response;
   
   
   
disp(['constants for double poles at ', num2str(rho)])
disp(['k1 = ', num2str(k1)])
disp(['k2 = ', num2str(k2)])

disp(['constants for distinct real poles at', num2str(p1_real), ... 
       ' and ', num2str(p2_real)])
disp(['k1 = ', num2str(k1_real)])
disp(['k2 = ', num2str(k2_real)])

disp(['constants for complex poles with \theta = ', num2str(theta_1), ...
       ' and \rho = ', num2str(rho)])
disp(['k1 = ', num2str(k1_complex_1)])
disp(['k2 = ', num2str(k2_complex_1)])

disp(['constants for complex poles with \theta = ', num2str(theta_2), ...
       ' and \rho = ', num2str(rho)])
disp(['k1 = ', num2str(k1_complex_2)])
disp(['k2 = ', num2str(k2_complex_2)])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% now find peak in H_x_hat as a function of S = (1-p2)/(1-p1)
p1_real = 0.99; % location of smallest distinct real pole 
H_x_hat_max = zeros(1,100); % reserve space for H_x_hat_max
for S = 1:100
p2_real = 1 - (1-p1_real)/S;
z1_real = 1 -  (1-p1_real)*(1-p2_real) / ( (1-p1_real) + (1-p2_real) );
k1_real = 1- p1_real*p2_real;
k2_real = (1-p1_real)*(1-p2_real)/k1_real;
H_x_hat_real_S = k1_real*(1+k2_real)*(exp(j*w) - z1_real) ./ ...
              (exp(j*w) - p1_real) ./ (exp(j*w) - p2_real);
H_x_hat_max(S) = max(abs(H_x_hat_real_S)); % save the maximum value
end
figure(5);  plot(1:S, 20*log10(H_x_hat_max),'-k','LineWidth',2); grid
xlabel('S'); ylabel('peak of |H_{xhat}(e^{j\omega})| in dB');
title(['p_1 = ', num2str(p1_real)])
% print -depsc peak_H_xhat_vs_S
 figure(6);  plot(1:10, 20*log10(H_x_hat_max(1:10)),'-k','LineWidth',2);
 grid; xlabel('S');  ylabel('peak of |H_{xhat}(e^{j\omega})| in dB');
title(['p_1 = ', num2str(p1_real)])
 % print -depsc zoomed_peak_H_xhat_vs_S
