% Matlab script for Example on Design of Type 2 circuit
% for an H_xhat(z) output
%%%%%%%%%%%%%%%%%%%
clear all
% system parameters for low pass filter
Rp = 1/2; % pass band ripple in dB
wp = 0.0070; % pass band corner
S = 10.5; % found from graph for Rp=1 dB wp = 0.003; 
% pass band corner in radians/sample
fo = 0.3; % carrier frequency in cycles/sample
% Matlab parameters
N = 4e5; % length of input vector for simulation
%%%%%%%%%%%%%%%%%%%
% caculate intial value for p1 using a double pole
p1_init_double = 1-wp/(Rp*exp(-1)+sqrt(2))
% p1_init_single = 1 - wp*sqrt(10^(-Rp/10)/(1-10^(-Rp/10))) % not used

guess_factor = 1; % this must be set to 1 to generate the response for
                 % the intial p1
%guess_factor = 1.68; % final result after cut-and-try
                    % comment this line out to generate initial response
p1 = 1 - guess_factor*(1-p1_init_double);
%p1 =1 - (1-p1_init_single)/guess_factor;

p2 = 1 - (1-p1)/S;
k1 = 1-p1*p2;
k2 = (1-p1)*(1-p2)/k1;
%
% inputs to the simulation
 x_ramp = fo*[0:N-1] + cos(0.00109*(0:N-1)) ;% carrier ramp + signal at peak
% x_ramp = fo*[0:N-1] + cos(wp*(0:N-1)) ;  % carrier ramp + signal at corner
% reserve space for vectors used in simulation
e_ramp = zeros(1,N); 
x_hat_ramp = zeros(1,N);                        
y1_ramp = zeros(1,N); 
y2_ramp = zeros(1,N); 
y3_ramp = zeros(1,N); 
%%%%%%%%%%%%%%%%%%%
% initialize registers, i.e. set their values at n=0
x_hat_ramp(1) = 0; % value at n=0
y3_ramp(1) = 0;    % value at n=0

   %%%%%%%%%%%%%%%%%%%
   % loop for sequential computation for ramp input
   for n = 0:N-1  % caculate variables at sample n
    e_ramp(n+1) = x_ramp(n+1)-x_hat_ramp(n+1);
    y2_ramp(n+1) = e_ramp(n+1)*k1;
    y1_ramp(n+1) = y2_ramp(n+1) + ...
                    k2*(y2_ramp(n+1)+y3_ramp(n+1));
    % calculate the register outputs at sample n+1 
    if n<N-1 % don't compute registers for sample N+1
    x_hat_ramp(n+2) = x_hat_ramp(n+1) + y1_ramp(n+1);
    y3_ramp(n+2) = y3_ramp(n+1)+ y2_ramp(n+1);
    end 
   end
% find peak output of e[n] in steady state
xhat_peak_dB = 20*log10(max(x_hat_ramp(end-10^3:end))) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% plot the results of simulation   
figure(1);
   plot(0:N-1, x_hat_ramp , 'LineWidth',2); axis normal;  grid;
   xlabel('n');  ylabel('xhat[n]');
   title(['x\_hat with the ramp:  f_o = ', num2str(fo), ...
          '  k_1 = ',   num2str(k1), '  k_2 = ',  num2str(k2)]);
 legend(['S = ', num2str(S),';','  R_p = ', num2str(Rp),' dB;', ...
              ' \omega_p = ', num2str(wp)],'location','southeast');
%   print -depsc example_xhat_with_ramp;
figure(2);
   plot(0:N-1, x_hat_ramp - fo*[0:N-1] , 'LineWidth',2); axis normal;  grid;
   xlabel('n');  ylabel('xhat[n]');
   title(['x\_hat with the Ramp Subtracted:'   ...
          '  k_1 = ',   num2str(k1), '  k_2 = ',  num2str(k2)]);
    legend(['S = ', num2str(S),';','  R_p = ', num2str(Rp),' dB;', ...
              ' \omega_p = ', num2str(wp)],'location','southeast');
%   print -depsc example_xhat_ramp_subtracted;
figure(3);
   plot(0:N-1, x_hat_ramp - fo*[0:N-1], 'LineWidth', 2 );
   axis([2e4,3e4,-1,1]);  axis normal; grid;
   xlabel('n');  ylabel('xhat[n]');
   title('Zoomed View in Steady State with Ramp Subtracted');
   legend(['S = ', num2str(S),';','  R_p = ', num2str(Rp),' dB;', ...
              ' \omega_p = ', num2str(wp)],'location','east');

%    print -depsc zoomed_xhat_example_corner_verification;
 figure(4);
   plot(0:N-1, x_hat_ramp - fo*[0:N-1], 'LineWidth', 2 );
   axis([2e5,3e5,-1.5,1.5]);  axis normal; grid;
   xlabel('n');  ylabel('xhat[n]');
   title('Zoomed View in Steady State with Ramp Subtracted');
   legend(['S = ', num2str(S),';','  R_p = ', num2str(Rp),' dB;', ...
              ' \omega_{peak} = 0.00109'],'location','east');
 %  print -depsc zoomed_xhat_example_peak_verification;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% now plot the magnitude response
% define the frequency vector
w = pi*[0:10^4]/10^4*wp;
% find coefficients of denominator polynomial from k1 and k2_nice
 a1 = -(2-k1*(1+k2));
 a2 = 1 - k1;
 % find location of zero
 z1 = 1/(1+k2);
% generate the frequency responses for nice and double pole designs
H_xhat = k1*(1+k2)*(exp(j*w)-z1) ./ (exp(j*2*w)+a1*exp(j*w) + a2);

if (guess_factor == 1)
% plot magnitude response for initial value of p1
figure(5);
    plot(w,20*log10(abs(H_xhat)),'-r', ...
        [0,wp, wp],[-Rp,-Rp, -Rp-2],'-k', [0,wp], [Rp,Rp], '-k', ...
        'LineWidth',2);
    grid;   axis normal;
    xlabel('\omega in radians/sample'); 
    ylabel('20log(|H_xhat(e^{j\omega})|)')
   title(['Using Initial value of p_1:  p_{1\_intial} = ', ...
      num2str(p1_init_double), ' and p_2 = ', num2str(p2)]);
    legend('filter response','pass-band specification','location','northeast');
    % print -depsc H_xhat_example_with_initial_p1
else
   % plot magnitude response for final value of p1
     figure(6);
    plot(w,20*log10(abs(H_xhat)),'-r', ...
        [0,wp, wp],[-Rp,-Rp, -Rp-2],'-k', [0,wp], [Rp,Rp], '-k', ...
        'LineWidth',2);
    grid;   axis normal;
    xlabel('\omega in radians/sample'); 
    ylabel('20log(|H_xhat(e^{j\omega})|)')
    title(['Using final value of p_1:  p_{1\_final} = ', ...
      num2str(p1), ' and p_2 = ', num2str(p2)]);
    legend('filter response','pass-band specification','location','northeast');
    % print -depsc H_xhat_example_with_final_p1
end
 % END OF MATLAB SCRIPT
 %%%%%%%%%%%%%%%%%%%%%%