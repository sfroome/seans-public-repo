clear all

delta_spec = 0.001;
%delta_spec = 0.005;
deltap = 0.1;

delta = delta_spec;
A = -20*log10(delta);

t_band1 = 0.4*pi-.25*pi;
t_band2 = .75*pi-.65*pi;

M = (A-8)/(2.285*(min(t_band1,t_band2)));
M %= 72.4381
M = 73;

beta = .1102*(A-8.7);

n = 0:M;
f1 = (0.25*pi+0.4*pi)/(2*2*pi);
f2 = (0.65*pi+0.75*pi)/(2*2*pi);

hd = 2*f2*sinc(2*f2*(n-M/2)) - 2*f1*sinc(2*f1*(n-M/2));

win = kaiser(M+1, beta)';
h = hd.*win;

L = 1024;
hz = [h,zeros(1,L-length(h))];
H = fft(hz);
k=0:L-1;
% M even so type I
% Phase response of -(M/2)*2*pi*f, f=k/L

W = exp(-j*(-(M/2)*2*pi)*k/L);
A = H.*W;
A=real(A);

figure(1), clf
plot(k/L-0.5,fftshift(A));
ylabel('A(e^{j\omega})')
title('Bandpass Amplitude Response using Hann Window')
xlabel('f (cycles/sample)')
grid


