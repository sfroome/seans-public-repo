clear all

delta_spec = 0.001;
delta = delta_spec; % Presumably in sample code because your actual delta will end up being lower 
%due to SB attenuation in your window.
wp = 0.4*pi;
ws = 0.5*pi;

A = -20*log10(delta);
A % = 60; So Ham on a Kaiser bun! (Hamming)

N = 1+ (A-8)/(2.285*2*pi*(ws/(2*pi)-wp/(2*pi)));
M = N - 1;
M % = 72.438136024319988
M = round(M);

beta = 0.1102*(A-8.7);

wc = ((wp+ws)/2);

h = fir1(M,wc/pi,'low',kaiser(M+1,beta));


[H,w]= freqz(h,1,[0:0.01:pi]);
W = exp(-j*(-M/2)*w);
A = H.*W;
A = real(A);

figure(1)
clf
stem([0:M],h);
title('Lowpass filter impulse rseponse')
xlabel('n (samples)')
figure(2)
clf
plot(w/(2*pi),A)
title(['Lowpass filter using a Kaiser window, M = ',num2str(M)])
xlabel('f (cycles/sample)')
ylabel('A(e^{j\omega})|)')
grid
figure(3)
clf
plot(w/(2*pi), 20*log10(abs(H)))
title(['Lowpass Filter using a Kaiser Window, M = ', num2str(M)])
xlabel('f (cycles/sample)')
ylabel('20log(|H(e^{j\omega})|')
grid