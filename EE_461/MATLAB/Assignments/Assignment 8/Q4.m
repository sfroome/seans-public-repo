format long
clear all
%{ Use these to comment out entire sections of code
%}

wp = pi/4;
ws = pi/8;

Gp = 1;
A = 44; % Because HANN window
delta = 10^(-44/20);
delta % = 0.0063096

M = 5.01*pi/(wp-ws);
M % = 40.0799999999
M = round(M);


n = 0:M;
f1 = ((ws+wp)/2)/(2*pi);
f2 = 0.5; % This needs to be nonzero to shift it.

hd = 2*f2*sinc(2*f2*(n-M/2))-2*f1*sinc(2*f1*(n-M/2)); 
%Generalized formula. For a LPF, f1 is of course zero.
%For a Bandpass, of course both will be nonzero.

hd = Gp*hd;
win = hann(M+1);
h = hd.*win.';


L = 1024;
hz = [h,zeros(1,L-length(h))];
H = fft(hz);
k=0:L-1;
% M even so type I
% Phase response of -(M/2)*2*pi*f, f=k/L

W = exp(-j*(-(M/2)*2*pi)*k/L);
A = H.*W;
A=real(A);

figure(1), clf
plot(k/L-0.5,fftshift(A));
ylabel('A(e^{j\omega})')
title('Highpass Amplitude Response using Hann Window')
xlabel('f (cycles/sample)')
grid
