clear all

wp = 0.7*pi;
ws = 0.8*pi; % Yes they're flipped.... but anyway

deltas = 0.0004;
deltap = 0.001;

A = -20*log10(min(deltas,deltap));
A % = 67.958800173440750

% For Regular Window Design, choose Blackman.
% For a Blackman Window

M = 9.19*pi/(ws-wp);
M % = -91.899999999999991
Mb = round(M);
wc = ((wp+ws)/2);
hb = fir1(Mb,wc/pi,'high',blackman(Mb+1));

[H, w] = freqz(hb,1,[0:0.01:pi]);
figure(1)
clf
plot(w/(2*pi),20*log10(abs(H)))
title(['Highpass filter using a Blackman window, M =', num2str(Mb)])
xlabel('f (cycles/sample)')
ylabel('20log(|H(e^{j\omega})|)')
grid

% For the Kaiser Design
M = (A-8)/(2.285*(ws-wp));
Mk = round(M);
M % = 83.525071592667715
beta = .1102*(A-8.7); %(You know A is going to be bigger than 50 so meh)
hk = fir1(Mk,wc/pi,'high',kaiser(Mk+1,beta));

[H, w] = freqz(hk, 1, [0:0.01:pi]);
figure(2)
clf
plot(w/(2*pi), 20*log10(abs(H)))
title(['Highpass Filter using a Kaiser Window, M = ', num2str(Mk)])
xlabel('f (cycles/sample)')
ylabel('20log(|H(e^{j\omega})|)')
grid

%Kaiser has a lower order, so it is the better choice.
% Blackman does have slightly better attenuation, but that is still
% at the cost of a higher order. 
