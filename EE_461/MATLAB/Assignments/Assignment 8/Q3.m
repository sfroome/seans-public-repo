format long
clear all

wp = pi/8;
ws = pi/4;

%deltap = 0.01;
%deltas = deltap;
delta_spec  = 0.01;
Gp = 2;
delta = delta_spec/Gp;

A = -20*log10(delta);

A %= 46.0206, so choose Hamming (Pg 22 of section 8 notes)

M = 6.27*pi/(ws-wp);
M % = 50.15999999999999999999999
M = round(M); % Must be integer.


n = 0:M;
f2 = ((wp+ws)/2)/(2*pi);
f1 = 0;

hd = 2*f2*sinc(2*f2*(n-M/2))-2*f1*sinc(2*f1*(n-M/2)); 
%Generalized formula. For a LPF, f1 is of course zero.
%For a Bandpass, of course both will be nonzero.

hd = Gp*hd;
win = hamming(M+1);
h = hd.*win.';


L = 1024;
hz = [h,zeros(1,L-length(h))];
H = fft(hz);
k=0:L-1;
% M even so type I
% Phase response of -(M/2)*2*pi*f, f=k/L

W = exp(-j*(-(M/2)*2*pi)*k/L);
A = H.*W;
A=real(A);

figure(1), clf
plot(k/L-0.5,fftshift(A));
ylabel('A(e^{j\omega})')
title('Lowpass Amplitude Response using Hamming Window')
xlabel('f (cycles/sample)')
grid

