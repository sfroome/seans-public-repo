clear all;
clf;

figure(1)
w = [.5:1:1000]/1000*pi
h1= [ -3566 -5403 -4114 0 4863 7564 5943 0 -7641 -12607 -10698 0 17829 37822 53488 59411];
N1 = 2*length(h1) - 1;
M1 = N1 - 1;
alpha1 = M1/2;
stem(h1)

figure(2)
w = [.5:1:1000]/1000*pi
h2 = [ -1967 -2980 -2269 0 2682 4172 3278 0 -4215 -6954 -5900 0 9834 20861 29502 32768];
N2 = 2*length(h2) - 1;
M2 = N2 - 1;
alpha2 = M2/2;
stem(h2)

figure(3)
w = [.5:1:1000]/1000*pi
h3 = [ 1967 2980 2269 0 -2682 -4172 -3278   0 4215 6954  5900  0 -9834 -20861 -29502 98304];
N3 = 2*length(h3) - 1;
M3 = N3 - 1;
alpha3 = M3/2;
stem(h3)

figure(4)
w = [.5:1:1000]/1000*pi
h4 = [ -3566 5403 -4114 -0 4863 -7564 5943 -0 -7641 12607 -10698 0 17829 -37822 53488 -59411];
N4 = 2*length(h4) - 1;
M4 = N4 - 1;
alpha4 = M4/2;
stem(h4)
